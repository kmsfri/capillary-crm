<?php

namespace App\Imports;

use App\Models\Customer;
use Maatwebsite\Excel\Concerns\ToCollection;
use Illuminate\Support\Collection;
use Helpers;


class CustomersImport implements ToCollection
{

    	
    	public $data;
    	
    	
    	public function collection(Collection $rows){
    	
    	
    	
    	
    	    $duplicates = [];
    	    $toInsert = [];

    	    foreach ($rows as $key=>$row) {
    	    
    	    	if(!empty($row[6])){
    	    		$row[6] = Helpers::convertFaToEnNumbersV2($row[6]);
    	    	}
    	    	
    	    	if(!empty($row[7])){
    	    		$row[7] = Helpers::convertFaToEnNumbersV2($row[7]);
    	    	}
    	    	
    	    	if(!empty($row[8])){
    	    		$row[8] = Helpers::convertFaToEnNumbersV2($row[8]);
    	    	}
    	    	
    	    	if(!empty($row[9])){
    	    		$row[9] = Helpers::convertFaToEnNumbersV2($row[9]);
    	    	}
    	    	
    	    	if(!empty($row[10])){
    	    		$row[10] = Helpers::convertFaToEnNumbersV2($row[10]);
    	    	}
    	    	
    	    	if(!empty($row[11])){
    	    		$row[11] = Helpers::convertFaToEnNumbersV2($row[11]);
    	    	}
    	    
    	    	
    	    
    	    
    	    	$row[9] = !empty($row[9])?("0".((int)($row[9])) ):"";
    	    	$row[10] = !empty($row[10])?("0".((int)($row[10])) ):"";
    	    	$row[11] = !empty($row[11])?("0".((int)($row[11])) ):"";
    	    	
    	    	$row[6] = !empty($row[6])?((int)($row[6]) ):"";
    	    	$row[7] = !empty($row[7])?((int)($row[7]) ):"";
    	    	$row[8] = !empty($row[8])?((int)($row[8]) ):"";
    	    
    	    
    		if(( (!empty($row[9]) && is_numeric($row[9])) || (!empty($row[6]) && is_numeric($row[6]))    )){
	    	
	    	
	    		//check duplicate mobile
	    		
	    		if(!empty($row[6]) && Customer::where('phone',$row[6])->exists()){
	    			$duplicates[] = $row[6];
	    		}else if(!empty($row[9]) && Customer::where('mobile',$row[9])->exists()){
	    			$duplicates[] = $row[9];
	    		}else if(!empty($row[9]) || !empty($row[6])){
	    		
	    			$toInsert[] = $row;
	    			
	    		}
	    	
	    	
	    	}
    	
	    }
	    
	    
	    
	    $data = [
	    	'failed_to_insert' => 0,
	    	'inserted' => 0,
	    	'duplicate_count' => count($duplicates),
	    	'duplicate_list' => $duplicates,
	    ];
	    
	    
	   
	    
	    if(count($duplicates)==0){

	    
	    	foreach($toInsert as $row){
	    	
	    	
	    	
	    		$newCustomer = new Customer([
				    'first_name'=>$row[0],
				    'last_name'=>$row[1],
				    'business_name'=>$row[2],
				    'state'=>$row[3],
				    'city'=>$row[4],
				    'address'=>$row[5],
				    'phone'=>$row[6],
				    'phone2'=>$row[7]??Null,
				    'phone3'=>$row[8]??Null,
				    'mobile'=>$row[9],
				    'mobile2'=>$row[10]??Null,
				    'mobile3'=>$row[11]??Null,
				    'creator_id'=>auth()->user()->id
				    
				]);
				
				
			$newCustomer->save();
				
				
			if($newCustomer){
				$data['inserted'] = $data['inserted']+1;
			}else{
				$data['failed_to_insert'] = $data['failed_to_insert']+1;
			}
				
	    	}
	    	
	    
	    }
	    
	    
	    
	    
	
	    
	    $this->data = $data;
	    
	    
	    
	    
	}
    	
    	
    	
    	
    	
    	
    	
}
