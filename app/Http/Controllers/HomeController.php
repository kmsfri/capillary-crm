<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Clue;

use App\Models\Customer;

use App\Models\Business;

use App\Models\Invoice;

use Helpers;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
		
		
		$permittedConditions = Helpers::getBusinessesAccessPermittedConditions(auth()->user());
		
		
		//begin count clues
		
        $clues = Clue::select('*')->where('id','<',0);


		if(auth()->user()->user_type=='ADMIN' || auth()->user()->user_type=='CSO'){
			//do nothing
			$clues = $clues->orWhere('id','>=',0);
		}else{

			foreach($permittedConditions as $key=>$condition){
				$toFireCondition1 = [];
				if(!empty($condition['ignore_cities']) && $condition['ignore_cities']){
					$toFireCondition1 = [
						'operand'=>'LIKE',
						'values' => '%'
					];
				}else{
					$toFireCondition1 = [
						'operand'=>'IN',
						'values' => $condition['city_ids']
					];
				}

				$toFireCondition2 = [];
				if(!empty($condition['ignore_creator_id']) && $condition['ignore_creator_id']){
					$toFireCondition2 = [
						'operand'=>'LIKE',
						'values' => '%'
					];
				}else{
					$toFireCondition2 = [
						'operand'=>'=',
						'values' => $condition['creator_id']
					];
				}
				$clues = $clues->orWhere(function($query) use($toFireCondition1,$toFireCondition2) {
					if($toFireCondition1['operand']=='IN'){
						$query->whereIn('city_id', $toFireCondition1['values'])->where('creator_user_id',$toFireCondition2['operand'],$toFireCondition2['values']);
					}else{
						$query->where('city_id',$toFireCondition1['operand'], $toFireCondition1['values'])->where('creator_user_id',$toFireCondition2['operand'],$toFireCondition2['values']);
					}
				});
			}
		}
        
 
 
 
		$permittedBusinessIDs = $clues->get()->pluck('id')->toArray();
		
		unset($clues);
 	
 	
        $cluesCount = Clue::select('*')->whereIn('id',$permittedBusinessIDs)->count();
		
		
		//end count clues
		
		
		
		
		
		//begin count businesses
		
		
        $businesses = Business::select('*')->where('id','<',0);


		if(auth()->user()->user_type=='ADMIN' || auth()->user()->user_type=='CSO'){
			//do nothing
			$businesses = $businesses->orWhere('id','>=',0);
		}else{
			foreach($permittedConditions as $key=>$condition){
				$toFireCondition1 = [];
				if(!empty($condition['ignore_cities']) && $condition['ignore_cities']){
					$toFireCondition1 = [
						'operand'=>'LIKE',
						'values' => '%'
					];
				}else{
					$toFireCondition1 = [
						'operand'=>'IN',
						'values' => $condition['city_ids']
					];
				}

				$toFireCondition2 = [];
				if(!empty($condition['ignore_creator_id']) && $condition['ignore_creator_id']){
					$toFireCondition2 = [
						'operand'=>'LIKE',
						'values' => '%'
					];
				}else{
					
					$toFireCondition2 = [
						'operand'=>'=',
						'values' => $condition['creator_id']
					];
				}
				$businesses = $businesses->orWhere(function($query) use($toFireCondition1,$toFireCondition2) {
					if($toFireCondition1['operand']=='IN'){
						$query->whereIn('city_id', $toFireCondition1['values'])->where('creator_id',$toFireCondition2['operand'],$toFireCondition2['values']);
					}else{
						$query->where('city_id',$toFireCondition1['operand'], $toFireCondition1['values'])->where('creator_id',$toFireCondition2['operand'],$toFireCondition2['values']);
					}
				});
			}
        }
 
 
 
		$permittedBusinessIDs = $businesses->get()->pluck('id')->toArray();
		
		unset($businesses);
		
		
        $businessesCount = Business::select('*')->whereIn('id',$permittedBusinessIDs)->count();
		
		
		
		//end count businesses
		
		
		
		
		
		
		
		//begin count customers
		
		
        $data = Customer::select('*')->where('id','<',0);


		if(auth()->user()->user_type=='ADMIN' || auth()->user()->user_type=='CSO'){
			//do nothing
			$data = $data->orWhere('id','>=',0);
		}else{
			
			foreach($permittedConditions as $key=>$condition){
				

				$toFireCondition2 = [];
				if(!empty($condition['ignore_creator_id']) && $condition['ignore_creator_id']){
					$toFireCondition2 = [
						'operand'=>'LIKE',
						'values' => '%'
					];
				}else{
					$toFireCondition2 = [
						'operand'=>'=',
						'values' => $condition['creator_id']
					];
				}
				if($toFireCondition2['operand']=="LIKE" && $toFireCondition2['values']=="%"){
					   //do nothing
					   $data = $data->orWhere(function($query) use($toFireCondition2) {
					$query->where('id','>=',0);
			   });
				}else{
				$data = $data->orWhere(function($query) use($toFireCondition2) {
					$query->where('creator_id',$toFireCondition2['operand'],$toFireCondition2['values']);
				});
				}
			}
		}
        
        
        $permittedCustomerIDs = $data->get()->pluck('id')->toArray();
 	
		unset($data);
 	
 	
        $customersCount = Customer::select('*')->whereIn('id',$permittedCustomerIDs)->count();
		
		
		//end count customers
		
		
		
		
		//begin count primary invoices
		
	
        $businesses = Business::select('*')->where('id','<',0);


		if(auth()->user()->user_type=='ADMIN' || auth()->user()->user_type=='CSO'){
			//do nothing
			$businesses = $businesses->orWhere('id','>=',0);
		}else{
			foreach($permittedConditions as $key=>$condition){
				$toFireCondition1 = [];
				if(!empty($condition['ignore_cities']) && $condition['ignore_cities']){
					$toFireCondition1 = [
						'operand'=>'LIKE',
						'values' => '%'
					];
				}else{
					$toFireCondition1 = [
						'operand'=>'IN',
						'values' => $condition['city_ids']
					];
				}

				$toFireCondition2 = [];
				if(!empty($condition['ignore_creator_id']) && $condition['ignore_creator_id']){
					$toFireCondition2 = [
						'operand'=>'LIKE',
						'values' => '%'
					];
				}else{
					$toFireCondition2 = [
						'operand'=>'=',
						'values' => $condition['creator_id']
					];
				}
				$businesses = $businesses->orWhere(function($query) use($toFireCondition1,$toFireCondition2) {
					if($toFireCondition1['operand']=='IN'){
						$query->whereIn('city_id', $toFireCondition1['values'])->where('creator_id',$toFireCondition2['operand'],$toFireCondition2['values']);
					}else{
						$query->where('city_id',$toFireCondition1['operand'], $toFireCondition1['values'])->where('creator_id',$toFireCondition2['operand'],$toFireCondition2['values']);
					}
				});
			}
		}

        $businessesIDs = $businesses->pluck('id')->toArray();



		$primaryInvoicesCount = Invoice::whereIn('business_id',$businessesIDs)
			->where('invoice_type','official')
			->where('approved_by_accountant',1)->count();
		
		//end count primary invoices
		
		
		
		
		
		
		
		
		
        return view('panel.pages.dashboard',compact('cluesCount','businessesCount','customersCount','primaryInvoicesCount'));
    }
}
