<?php


namespace App\Http\Controllers\Admin;



use Helpers;
use App\Models\City;
use App\Models\Business;
use App\Models\BusinessCredit;
use App\Models\CrmConfig;
use App\Models\Invoice;
use App\Models\Clue;
use App\Models\InstanceInvoice;
use App\Models\Product;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Rules\CheckBusinessPermission;
use niklasravnsborg\LaravelPdf\Facades\Pdf;

class InstanceController extends Controller

{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    function __construct()

    {

        $this->middleware('permission:invoice-list|invoice-create|invoice-edit|invoice-delete', ['only' => ['index', 'show']]);

        $this->middleware('permission:invoice-create', ['only' => ['create', 'store']]);

        $this->middleware('permission:invoice-edit', ['only' => ['edit', 'update']]);

        $this->middleware('permission:invoice-delete', ['only' => ['destroy']]);
        
        $this->middleware('permission:primary-invoice-list', ['only' => ['primaryInvoices']]);
        
        $this->middleware('permission:rejected-invoice-list', ['only' => ['rejectedInvoices']]);
        
        $this->middleware('permission:business-accounting-approve', ['only' => ['showAccountantApprovingForm','accountantApprovingInvoice']]);
        
        $this->middleware('permission:business-sales-specialist-approve', ['only' => ['showSalesSpecialistApprovingForm','salesSpecialistApprovingInvoice']]);
        
        $this->middleware('permission:management-bijak', ['only' => ['showCreateBijakform','uploadBijak']]);
        
        $this->middleware('permission:view-bijak', ['only' => ['showBijak']]);
        
        
        

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */





    public function index(Request $request)

    {

        $permittedConditions = Helpers::getBusinessesAccessPermittedConditions(auth()->user());

        $businesses = Business::select('*')->where('id','<',0);

        foreach($permittedConditions as $key=>$condition){
            $toFireCondition1 = [];
            if(!empty($condition['ignore_cities']) && $condition['ignore_cities']){
                $toFireCondition1 = [
                    'operand'=>'LIKE',
                    'values' => '%'
                ];
            }else{
                $toFireCondition1 = [
                    'operand'=>'IN',
                    'values' => $condition['city_ids']
                ];
            }

            $toFireCondition2 = [];
            if(!empty($condition['ignore_creator_id']) && $condition['ignore_creator_id']){
                $toFireCondition2 = [
                    'operand'=>'LIKE',
                    'values' => '%'
                ];
            }else{
                $toFireCondition2 = [
                    'operand'=>'=',
                    'values' => $condition['creator_id']
                ];
            }
            $businesses = $businesses->orWhere(function($query) use($toFireCondition1,$toFireCondition2) {
                if($toFireCondition1['operand']=='IN'){
                    $query->whereIn('city_id', $toFireCondition1['values'])->where('creator_id',$toFireCondition2['operand'],$toFireCondition2['values']);
                }else{
                    $query->where('city_id',$toFireCondition1['operand'], $toFireCondition1['values'])->where('creator_id',$toFireCondition2['operand'],$toFireCondition2['values']);
                }
            });
        }

        $businessesIDs = $businesses->pluck('id')->toArray();


	$invoices = Invoice::whereIn('business_id',$businessesIDs)
			->where('invoice_type','official')
			->where('approved_by_sales_specialist',0)
			->orWhere(function($query) {
			    $query->where('approved_by_sales_specialist', 1)->where('approved_by_accountant', 0);
			})
			->orderBy('created_at','DESC')->paginate(20);

        return view('panel.admin.invoices.index', compact('invoices'))
            ->with('i', (request()->input('page', 1) - 1) * 20);

    }
    
    
    
    
    public function primaryInvoices(Request $request)
    {

        $permittedConditions = Helpers::getBusinessesAccessPermittedConditions(auth()->user());

        $businesses = Business::select('*')->where('id','<',0);

        foreach($permittedConditions as $key=>$condition){
            $toFireCondition1 = [];
            if(!empty($condition['ignore_cities']) && $condition['ignore_cities']){
                $toFireCondition1 = [
                    'operand'=>'LIKE',
                    'values' => '%'
                ];
            }else{
                $toFireCondition1 = [
                    'operand'=>'IN',
                    'values' => $condition['city_ids']
                ];
            }

            $toFireCondition2 = [];
            if(!empty($condition['ignore_creator_id']) && $condition['ignore_creator_id']){
                $toFireCondition2 = [
                    'operand'=>'LIKE',
                    'values' => '%'
                ];
            }else{
                $toFireCondition2 = [
                    'operand'=>'=',
                    'values' => $condition['creator_id']
                ];
            }
            $businesses = $businesses->orWhere(function($query) use($toFireCondition1,$toFireCondition2) {
                if($toFireCondition1['operand']=='IN'){
                    $query->whereIn('city_id', $toFireCondition1['values'])->where('creator_id',$toFireCondition2['operand'],$toFireCondition2['values']);
                }else{
                    $query->where('city_id',$toFireCondition1['operand'], $toFireCondition1['values'])->where('creator_id',$toFireCondition2['operand'],$toFireCondition2['values']);
                }
            });
        }

        $businessesIDs = $businesses->pluck('id')->toArray();



		$invoices = Invoice::whereIn('business_id',$businessesIDs)
			->where('invoice_type','official')
			->where('approved_by_accountant',1)
			->orderBy('created_at','DESC')->paginate(20);

        return view('panel.admin.invoices.primary-invoices', compact('invoices'))
            ->with('i', (request()->input('page', 1) - 1) * 20);

    }
    
    
    
    public function rejectedInvoices(Request $request)
    {

        $permittedConditions = Helpers::getBusinessesAccessPermittedConditions(auth()->user());

        $businesses = Business::select('*')->where('id','<',0);

        foreach($permittedConditions as $key=>$condition){
            $toFireCondition1 = [];
            if(!empty($condition['ignore_cities']) && $condition['ignore_cities']){
                $toFireCondition1 = [
                    'operand'=>'LIKE',
                    'values' => '%'
                ];
            }else{
                $toFireCondition1 = [
                    'operand'=>'IN',
                    'values' => $condition['city_ids']
                ];
            }

            $toFireCondition2 = [];
            if(!empty($condition['ignore_creator_id']) && $condition['ignore_creator_id']){
                $toFireCondition2 = [
                    'operand'=>'LIKE',
                    'values' => '%'
                ];
            }else{
                $toFireCondition2 = [
                    'operand'=>'=',
                    'values' => $condition['creator_id']
                ];
            }
            $businesses = $businesses->orWhere(function($query) use($toFireCondition1,$toFireCondition2) {
                if($toFireCondition1['operand']=='IN'){
                    $query->whereIn('city_id', $toFireCondition1['values'])->where('creator_id',$toFireCondition2['operand'],$toFireCondition2['values']);
                }else{
                    $query->where('city_id',$toFireCondition1['operand'], $toFireCondition1['values'])->where('creator_id',$toFireCondition2['operand'],$toFireCondition2['values']);
                }
            });
        }

        $businessesIDs = $businesses->pluck('id')->toArray();



	$invoices = Invoice::whereIn('business_id',$businessesIDs)
			->where('invoice_type','official')
			->where('approved_by_sales_specialist',2)
			->orWhere('approved_by_accountant',2)
			
			->orderBy('created_at','DESC')->paginate(20);

        return view('panel.admin.invoices.rejected-invoices', compact('invoices'))
            ->with('i', (request()->input('page', 1) - 1) * 20);

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create(Request $request)

    {
    
        request()->validate([
            'clue_id' => ['required','integer','exists:clues,id'],
            'clue_level' => 'required',
        ]);
        
        if(empty($request->clue_id)){
        	$clue = Clue::where('id',$request->clue_id)->first();
        	if(empty($clue)){
        		abort(404);
        	}
        }

        $clue = Clue::findOrFail($request->clue_id);
        

        $products = Product::select(['id','product_name','has_priority', 'price', 'product_code'])->orderBy('has_priority','DESC')->get();

		$clueLevel = $request->clue_level;

        return view('panel.admin.instances.create', compact('clue','clueLevel','products'));

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)

    {

        request()->validate([
            'clue_id' => ['required','integer','exists:clues,id'],
			'clue_level' => 'required',
            'invoice_items.*' => 'required',
            'delivery_place_address' => 'required|max:255',
            'sell_method' => 'required|integer',
            'cheque_time' => 'required|integer',
            'invoice_description' => 'required|max:500',
			'file_path' => 'nullable|max:15360',
        ]);
		
		
		
		//check credit
		if($request->sell_method==3){ //sell method is Credit
		
			$remainedCreditAmount = BusinessCredit::where('business_id',$request->business_id)->sum('credit_amount');
			
			
			$invoiceSumAmount = 0;
			
			foreach($request->invoice_items as $invItem){
				$invoiceSumAmount = $invoiceSumAmount + $invItem['item_total_price'];
			}
			
			if(((int)$invoiceSumAmount)>((int)$remainedCreditAmount)){
				return redirect()->back()
					->withInput($request->input())
            		->with('danger', 'با توجه به نحوه فروش اعتباری، جمع مبالغ فاکتور از میزان اعتبار باقیمانده کسب و کار بیشتر است.');
			}
			
		
		}
		
		
		
		
		//BEGIN VALIDATE INVOICE ITEEMS
		$maxChequeDays = CrmConfig::where('config_key','MAX_CHEQUE_DAYS')->firstOrFail()->config_value;
		
		if($request->cheque_time > $maxChequeDays){
			return redirect()->back()
					->withInput($request->input())
            		->with('danger', 'حداکثر زمان چک میتواند '.$maxChequeDays.' باشد.');
		}
		
		$maxDiscountAmount = CrmConfig::where('config_key','MAX_DISCOUNT_AMOUNT')->firstOrFail()->config_value;
		$maxInvoiceItemAmount = CrmConfig::where('config_key','MAX_INVOICE_ITEM_AMOUNT')->firstOrFail()->config_value;
		
		$sumOfItemAmounts = 0;
		foreach($request->invoice_items as $invItem){
			if($invItem['discount']>$maxDiscountAmount){
				return redirect()->back()
					->withInput($request->input())
            		->with('danger', 'حداکثر میزان تخفیف میتواند '.$maxDiscountAmount.' باشد.');
			}
			
			if($invItem['item_total_price']>$maxInvoiceItemAmount){
				return redirect()->back()
					->withInput($request->input())
            		->with('danger', 'حداکثر مبلغ هر آیتم فاکتور میتواند '.$maxInvoiceItemAmount.' باشد.');
			}
			
			$sumOfItemAmounts = $sumOfItemAmounts + $invItem['item_total_price'];
		}
		
		
		$maxSumOfInvoiceItemsAmount = CrmConfig::where('config_key','MAX_SUM_OF_INVOICE_ITEMS_AMOUNT')->firstOrFail()->config_value;
		if($sumOfItemAmounts > $maxSumOfInvoiceItemsAmount){
			return redirect()->back()
					->withInput($request->input())
            		->with('danger', 'جمع مبالع آیتمهای هر فاکتور میتواند حداکثر '.$maxSumOfInvoiceItemsAmount.' باشد.');
		}
		//BEGIN VALIDATE INVOICE ITEEMS 
		
		
		
		
		
		



        $invoiceData = [];

        $invoiceData['invoice_items'] = json_encode($request->invoice_items, true);

        $invoiceData['delivery_place_address'] = $request->delivery_place_address;
        $invoiceData['sell_method'] = $request->sell_method;
        $invoiceData['cheque_time'] = $request->cheque_time;
        $invoiceData['invoice_description'] = $request->invoice_description;
        
        $invoiceData['invoice_type'] = 'official';





      
        $invoiceData['clue_id'] = $request->clue_id;
		
		$invoiceData['clue_level'] = $request->clue_level;

        $invoiceData['creator_id'] = auth()->user()->id;






		$invoiceData['file_path'] = Null;

		//upload file_path
        if (!empty($request->file_path)) {

            $uploadPath = '/uploads/instances';
            $toUploadFile = $request->file('file_path');
            $invoiceData['file_path'] = $this->uploadFile($toUploadFile, $uploadPath);

        }






        InstanceInvoice::create($invoiceData);


        return redirect()->route('clues.showCartable',['clue_id'=>$request->clue_id])
            ->with('success', 'فرم ارسال نمونه جدید با موفقیت ثبت شد.');

    }


    /**
     * Display the specified resource.
     *
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */

    public function show(Business $business)

    {

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */

    public function edit(Invoice $invoice, Request $request)
    {
    
		abort(403);
    
    	$request->request->add(['business_id' => $invoice->business_id]);
    
    
    	request()->validate([
            'business_id' => ['required','integer','exists:businesses,id', new CheckBusinessPermission()]
        ]);
    
    

        $invoice->invoice_items = !empty($invoice->invoice_items)?json_decode($invoice->invoice_items, true):[];
        
        $business = $invoice->business()->first();
        
        $products = Product::select(['id','product_name','has_priority', 'price', 'product_code'])->orderBy('has_priority','DESC')->get();

        return view('panel.admin.invoices.edit', compact('invoice','business', 'products'));

    }


    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, Invoice $invoice)
    {
		
		abort(403);
    
    	$request->request->add(['business_id' => $invoice->business_id]);
    
  
        request()->validate([

            'business_id' => ['required','integer','exists:businesses,id', new CheckBusinessPermission()],
            'invoice_items.*' => 'required',
            'delivery_place_address' => 'required|max:255',
            'sell_method' => 'required|integer',
            'cheque_time' => 'required|integer',
            'invoice_description' => 'required|max:500'
        ]);


        

        $invoiceData = [];

        $invoiceData['invoice_items'] = json_encode($request->invoice_items, true);

        $invoiceData['delivery_place_address'] = $request->delivery_place_address;
        $invoiceData['sell_method'] = $request->sell_method;
        $invoiceData['cheque_time'] = $request->cheque_time;
        $invoiceData['invoice_description'] = $request->invoice_description;

        $invoice->update($invoiceData);
        
        
        return redirect()->route('invoices.index')
            ->with('success', 'فاکتور مورد نظر با موفقیت ویرایش شد');
        




    }


    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */

    public function destroy(Invoice $invoice)

    {
		
		abort(403);

        $invoice->delete();


        return redirect()->route('invoices.index')
            ->with('success', 'فاکتور مورد نظر با موفقیت حذف شد');

    }


    public function getPdf(Request $request){


		
		$data = [];
        
        
        
        $invoice = InstanceInvoice::findOrFail($request->invoice_id);

		/*
        if($invoice->approved_by_accountant==1){
        	$data['title'] = 'صورت حساب فروش کالا و خدمات';
        }else{
        	$data['title'] = 'پیش فاکتور کالا و خدمات';
        }
		*/
		
		$data['title'] = 'فرم ارسال نمونه کالا';
        
			
	 
		$data['serial_number'] = $invoice->id;
		
		$data['created_at'] = \Morilog\Jalali\Jalalian::forge($invoice->created_at)->format('%A, %d %B %Y');
		
		$data['business_first_name'] = $invoice->clue()->first()->first_name;
		
		$data['business_last_name'] = $invoice->clue()->first()->last_name;
		
		$data['business_name'] = $invoice->clue()->first()->business_name;
		
		$data['business_address'] = $invoice->clue()->first()->address;
		
		$data['business_postal_code'] = $invoice->clue()->first()->postal_code;
		
		$data['business_phone_number'] = $invoice->clue()->first()->phone_number;
		
		$businessMobiles = json_decode($invoice->clue()->first()->mobiles,true);
		
		$data['business_mobile1'] = !empty($businessMobiles[0])?$businessMobiles[0]:'-';
		
		$data['business_mobile2'] = !empty($businessMobiles[1])?$businessMobiles[1]:'-';
		
		$data['invoice_items'] = json_decode($invoice->invoice_items,true);
		
		$data['invoice_creator'] = $invoice->creator()->first()->name;
		
		$data['invoice_registerar'] = $invoice->creator()->first()->name;
		
		$data['invoice_delivery_place_address'] = $invoice->delivery_place_address;
		
		
		
		$enums = config('enum');
			$sellMethods = $enums['sell_method'];
			
			$data['invoice_sell_method'] = !empty($sellMethods[$invoice->sell_method])?$sellMethods[$invoice->sell_method]:'-';
			
			$data['invoice_cheque_time'] = $invoice->cheque_time;
			
			$data['invoice_description'] = $invoice->invoice_description;
			
			
			
			//return view('panel.admin.invoices.pdf', compact('data'));
			
			$options = [];
			if($invoice->approved_by_accountant==1){
				$options = [ 
			  'format' => 'A4-L',
			  'orientation' => 'L'
			];
        }
        
        
        $pdf = Pdf::loadView('panel.admin.instances.pdf', compact('data'), [], $options);
        
        return $pdf->stream('instance_report.pdf');

    }
	
	
	
	
	
	
	
	
	
	private function uploadFile($file, $uploadPath)
    {
        $uploaded_file_dir_signature = "";

        if ($file == null) {
            $uploaded_file_dir_signature = "";
        } else {

            if ($file->isValid()) {
                $fileName = str_replace(' ', '', time()) . rand(1000, 9999) . '_' . $file->getClientOriginalName();
                $destinationPath = public_path() . $uploadPath;

                $file->move($destinationPath, $fileName);
                $uploaded_file_dir_signature = $uploadPath . '/' . $fileName;
//                    if($request->edit_id!=Null) {
//                        $to_remove_dir_signature = \App\Models\AgencyRequest::find($request->edit_id)->signature;
//                    }
            } else {
//                    $this->img_upload_error_msg = 'آپلود تصویر ناموفق بود';
//                    goto catch_block;
            }
        }

        return $uploaded_file_dir_signature;
    } 
    



}
