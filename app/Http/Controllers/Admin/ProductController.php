<?php


namespace App\Http\Controllers\Admin;


use App\Models\Product;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class ProductController extends Controller

{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    function __construct()

    {

        $this->middleware('permission:product-list|product-create|product-edit|product-delete', ['only' => ['index', 'show']]);

        $this->middleware('permission:product-create', ['only' => ['create', 'store']]);

        $this->middleware('permission:product-edit', ['only' => ['edit', 'update']]);

        $this->middleware('permission:product-delete', ['only' => ['destroy']]);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()

    {

        $products = Product::latest()->paginate(25);

        return view('panel.admin.products.index', compact('products'))
            ->with('i', (request()->input('page', 1) - 1) * 25);

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()

    {

        return view('panel.admin.products.create');

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)

    {

        request()->validate([

            'product_name' => 'required|max:191',
            'product_code' => 'required|max:191',
            'manufacturer' => 'required|max:191',
            'product_model' => 'required|max:191',
            'stock_count' => 'required|integer',
            'price' => 'required|integer',
            'has_priority' => 'required|integer',
            'details' => 'nullable|max:500',
            'descriptions' => 'nullable|max:500'


        ]);


        Product::create($request->all());


        return redirect()->route('products.index')
            ->with('success', 'محصول با موفقیت ایجاد شد.');

    }


    /**
     * Display the specified resource.
     *
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */

    public function show(Product $product)

    {

        return view('panel.admin.products.show', compact('product'));

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */

    public function edit(Product $product)

    {

        return view('panel.admin.products.edit', compact('product'));

    }


    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, Product $product)

    {

        request()->validate([

            'product_name' => 'required|max:191',
            'product_code' => 'required|max:191',
            'manufacturer' => 'required|max:191',
            'product_model' => 'required|max:191',
            'stock_count' => 'required|integer',
            'price' => 'required|integer',
            'has_priority' => 'required|integer',
            'details' => 'nullable|max:500',
            'descriptions' => 'nullable|max:500'

        ]);


        $product->update($request->all());


        return redirect()->route('products.index')
            ->with('success', 'محصول مورد نظر با موفقیت ویرایش شد');

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */

    public function destroy(Product $product)

    {

        $product->delete();


        return redirect()->route('products.index')
            ->with('success', 'محصول مورد نظر با موفقیت حذف شد');

    }
    
    
    
    
    
    public function updateAllProducts(Request $request){
    
    	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL,"http://192.168.40.21:9045/Service1.svc/GetAllProductInfo");
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_POST, 1);

	$data = json_encode([
	  'Password'=> 'Sepidar@123',
	  'Username'=> 'webuser'
	],true);

	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");

	$serverOutput = curl_exec($ch);

	curl_close($ch);
    
    
    	$sepidarPoductsList = json_decode($serverOutput,true);
    	foreach($sepidarPoductsList as $sepidarProduct){
    	
    		if(!(Product::where('sepidar_code',$sepidarProduct['Code'])->exists())){
    		
    			$newProductData = [
    				'product_name' => $sepidarProduct['Title'],
    				'product_code' => $sepidarProduct['Code'],
    				'sepidar_barcode' => $sepidarProduct['BarCode'],
    				'sepidar_code' => $sepidarProduct['Code'],
    				'sepidar_product_type' => $sepidarProduct['ProductType']   			
    			];
    			Product::create($newProductData);
    		
    		}else{
    			$product = Product::where('sepidar_code',$sepidarProduct['Code'])->firstOrFail();
    			
    			$newProductData = [
    				'product_name' => $sepidarProduct['Title'],
    				'sepidar_barcode' => $sepidarProduct['BarCode'],
    				'sepidar_product_type' => $sepidarProduct['ProductType']   			
    			];
    			
    			$product->update($newProductData);
    			
    		}
    		
    		
    		
    		
    		
    		
    		
    		
    		
    	
    		
    	}
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	//UPDATE FEE
    	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL,"http://192.168.40.21:9045/Service1.svc/GetAllPriceNote");
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_POST, 1);

	$data = json_encode([
	  'Password'=> 'Sepidar@123',
	  'Username'=> 'webuser'
	],true);

	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");

	$serverOutput = curl_exec($ch);

	curl_close($ch);
    
    
    	$sepidarPoductsList = json_decode($serverOutput,true);
    	
    	
    	foreach($sepidarPoductsList as $sepidarProduct){
    	
    		$product = Product::where('sepidar_code',$sepidarProduct['ItemCode'])->first();
    		
    		if(!$product){
    			continue;
    		}
    		
    		
    		$product->price = $sepidarProduct['Fee'];
    		$product->save();
    	
    		
    	}
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	//UPDATE Stock Count
    	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL,"http://192.168.40.21:9045/Service1.svc/GetAllSalableQuantity");
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_POST, 1);

	$data = json_encode([
	  'Password'=> 'Sepidar@123',
	  'Username'=> 'webuser'
	],true);

	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");

	$serverOutput = curl_exec($ch);

	curl_close($ch);
    
    
    	$sepidarPoductsList = json_decode($serverOutput,true);
    	
    	$productsStockCount = [];
    	foreach($sepidarPoductsList as $sepidarProduct){
    	
    		
    		
    		
    		$beforeValue = 0;
    		if(!empty($productsStockCount[$sepidarProduct['ItemCode']])){
    			$beforeValue = $productsStockCount[$sepidarProduct['ItemCode']];
    		}
    		
    		$productsStockCount[$sepidarProduct['ItemCode']] = $beforeValue+$sepidarProduct['Quantity'];
    		
    		
    	
    		
    	}
    	
    	
    	
    	foreach($productsStockCount as $productCode => $productQuantity){
    	
    		$product = Product::where('sepidar_code',$productCode)->first();
    		
    		if(!$product){
    			continue;
    		}
    		
    		
    		$product->stock_count = $productQuantity;
    		$product->save();
    		
    	
    	}
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	/*
	$allProducts = Product::where('sepidar_code','>',0)->get();
	foreach($allProducts as $product){
	
	
		//UPDATE Stock Count
	    	$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL,"http://192.168.40.21:9045/Service1.svc/CheckProduct");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POST, 1);

		$data = json_encode([
		  'Code' => $product->sepidar_code,
		  'Password'=> 'Sepidar@123',
		  'Username'=> 'webuser'
		],true);

		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");

		$stockCount = curl_exec($ch);

		curl_close($ch);
		
		
		
		$productToUpdate = Product::findOrFail($product->id);
    		
    		
    		$productToUpdate->stock_count = $stockCount;
    		$productToUpdate->save();
	    
	    
	    	
	
	
	}    	
    	
    	*/
    	
    	
    	
    	
 	echo "Job Done Successfully";   	
    	
    	
    	

    
    }

}
