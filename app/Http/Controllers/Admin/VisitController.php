<?php


namespace App\Http\Controllers\Admin;


use Helpers;
use Illuminate\Http\Request;
use App\Models\Business;
use App\Models\City;
use App\Models\VisitRoute;
use App\Models\DriverVisitRoute;
use App\Models\ClueProcessReport;
use App\Models\Invoice;
use App\Models\User;
use App\Models\CallTargetAlarm;
use App\Models\Clue;
use App\Models\Note;

use DB;

use App\Http\Controllers\Controller;

class VisitController extends Controller

{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    function __construct()

    {

        $this->middleware('permission:visit-set-daily-flow', ['only' => ['setDailyFlow']]);

        $this->middleware('permission:show-visitors', ['only' => ['showVisitors']]);
        
        
        $this->middleware('permission:show-drivers', ['only' => ['showDrivers']]);
        
        $this->middleware('permission:visit-set-driver-daily-flow', ['only' => ['setDriverDailyFlow']]);
        

        //$this->middleware('permission:city-edit', ['only' => ['edit', 'update']]);

        //$this->middleware('permission:city-delete', ['only' => ['destroy']]);

    }
    
    
    
    public function setDailyFlow(Request $request)
    {
    
    	
    
    	$visitorUser = User::findOrFail($request->user_id);
    	
    	
    	$visitRoute = Null;
    	$visitRouteAt = Null;
    	$visitRouteFlowChartOperatorsCount = 0;
    	
    	//get count of default flowchart operators
    	if(!empty($request->visit_at_date)){
    		$visitRouteAt = $request->visit_at_date;
    		$visitRoute = VisitRoute::where('visitor_user_id',$visitorUser->id)->where('visit_at_date',$request->visit_at_date)->first();
    		
    		$defaultVisitRoute = $visitRoute->visit_route;
    		if(!empty($defaultVisitRoute)){
    			$defaultVisitRoute = json_decode($defaultVisitRoute,true);
    			if(isset($defaultVisitRoute['operators']) && !empty($defaultVisitRoute['operators'])){
    			
    				$visitRouteFlowChartOperatorsCount = count($defaultVisitRoute['operators']);

    			}
    		}
    	}
    	
    	
    	
    
    
    	$permittedConditions = Helpers::getBusinessesAccessPermittedConditions(auth()->user());
    	
    	
    	
    	
    	
    	$permittedStateRegionCities = Helpers::getPermittedCities(auth()->user());

        $states = City::select('*');
        if(is_array($permittedStateRegionCities['permitted_states'])){
            $states = $states->whereIn('id',$permittedStateRegionCities['permitted_states']);
        }else if($permittedStateRegionCities['permitted_states']!='%'){
            $states = $states->whereIn('id',[]);
        }
        $states = $states->where('unit_type', 0)->where('unit_status', 1)->orderBy('unit_order', 'ASC')->get();
    	
    	
    	
    	
    	
    	
    	
    	

        $businesses = Business::select('*')->where('id','<',0);

        foreach($permittedConditions as $key=>$condition){
            $toFireCondition1 = [];
            if(!empty($condition['ignore_cities']) && $condition['ignore_cities']){
                $toFireCondition1 = [
                    'operand'=>'LIKE',
                    'values' => '%'
                ];
            }else{
                $toFireCondition1 = [
                    'operand'=>'IN',
                    'values' => $condition['city_ids']
                ];
            }

            $toFireCondition2 = [];
            if(!empty($condition['ignore_creator_id']) && $condition['ignore_creator_id']){
                $toFireCondition2 = [
                    'operand'=>'LIKE',
                    'values' => '%'
                ];
            }else{
                $toFireCondition2 = [
                    'operand'=>'=',
                    'values' => $condition['creator_id']
                ];
            }
            $businesses = $businesses->orWhere(function($query) use($toFireCondition1,$toFireCondition2) {
                if($toFireCondition1['operand']=='IN'){
                    $query->whereIn('city_id', $toFireCondition1['values'])->where('creator_id',$toFireCondition2['operand'],$toFireCondition2['values']);
                }else{
                    $query->where('city_id',$toFireCondition1['operand'], $toFireCondition1['values'])->where('creator_id',$toFireCondition2['operand'],$toFireCondition2['values']);
                }
            });
        }

        $businesses = $businesses->orderBy('created_at','DESC')->get();
    
    
        return view('panel.admin.visit.flow.set-daily-flow',compact('businesses','visitorUser','visitRouteAt','visitRoute','visitRouteFlowChartOperatorsCount','states'));

    }
    
    
    
    public function setDriverDailyFlow(Request $request)
    {
    
    	
    
    	$visitorUser = User::findOrFail($request->user_id);
    	
    	
    	$visitRoute = Null;
    	$visitRouteAt = Null;
    	$visitRouteFlowChartOperatorsCount = 0;
    	
    	//get count of default flowchart operators
    	if(!empty($request->visit_at_date)){
    		$visitRouteAt = $request->visit_at_date;
    		$visitRoute = DriverVisitRoute::where('visitor_user_id',$visitorUser->id)->where('visit_at_date',$request->visit_at_date)->first();
    		
    		$defaultVisitRoute = $visitRoute->visit_route;
    		if(!empty($defaultVisitRoute)){
    			$defaultVisitRoute = json_decode($defaultVisitRoute,true);
    			if(isset($defaultVisitRoute['operators']) && !empty($defaultVisitRoute['operators'])){
    			
    				$visitRouteFlowChartOperatorsCount = count($defaultVisitRoute['operators']);

    			}
    		}
    	}
    	
    	
    	
    
    
    	$permittedConditions = Helpers::getBusinessesAccessPermittedConditions(auth()->user());
    	
    	
    	
    	
    	
    	$permittedStateRegionCities = Helpers::getPermittedCities(auth()->user());

        $states = City::select('*');
        if(is_array($permittedStateRegionCities['permitted_states'])){
            $states = $states->whereIn('id',$permittedStateRegionCities['permitted_states']);
        }else if($permittedStateRegionCities['permitted_states']!='%'){
            $states = $states->whereIn('id',[]);
        }
        $states = $states->where('unit_type', 0)->where('unit_status', 1)->orderBy('unit_order', 'ASC')->get();
    	
    	
    	
    	
    	
    	
    	
    	

        $businesses = Business::select('*')->where('id','<',0);

        foreach($permittedConditions as $key=>$condition){
            $toFireCondition1 = [];
            if(!empty($condition['ignore_cities']) && $condition['ignore_cities']){
                $toFireCondition1 = [
                    'operand'=>'LIKE',
                    'values' => '%'
                ];
            }else{
                $toFireCondition1 = [
                    'operand'=>'IN',
                    'values' => $condition['city_ids']
                ];
            }

            $toFireCondition2 = [];
            if(!empty($condition['ignore_creator_id']) && $condition['ignore_creator_id']){
                $toFireCondition2 = [
                    'operand'=>'LIKE',
                    'values' => '%'
                ];
            }else{
                $toFireCondition2 = [
                    'operand'=>'=',
                    'values' => $condition['creator_id']
                ];
            }
            $businesses = $businesses->orWhere(function($query) use($toFireCondition1,$toFireCondition2) {
                if($toFireCondition1['operand']=='IN'){
                    $query->whereIn('city_id', $toFireCondition1['values'])->where('creator_id',$toFireCondition2['operand'],$toFireCondition2['values']);
                }else{
                    $query->where('city_id',$toFireCondition1['operand'], $toFireCondition1['values'])->where('creator_id',$toFireCondition2['operand'],$toFireCondition2['values']);
                }
            });
        }

        $businesses = $businesses->orderBy('created_at','DESC')->get();
    
    
        return view('panel.admin.visit.flow.set-driver-daily-flow',compact('businesses','visitorUser','visitRouteAt','visitRoute','visitRouteFlowChartOperatorsCount','states'));

    }
    
    
    
    
    
    
    public function showVisitors(Request $request){
    
    
    	$permittedUserIDs = [];
    	
    	$showAllSpecialists = false;
        if(auth()->user()->user_type=='SALES_SPECIALIST'){
            $permittedUserIDs = [auth()->user()->id];

        }else if(auth()->user()->user_type=='SALES_MANAGER'){
            $subUsers = auth()->user()->subUsers()->get();
            foreach($subUsers as $subUser){
                $permittedUserIDs[] = $subUser->id;
            }

        }else if(auth()->user()->user_type=='CSO'){
            $managerUsers = auth()->user()->subUsers()->get();
            foreach($managerUsers as $managerUser){
                foreach($managerUser->subUsers()->get() as $specialistUser){
                    $permittedUserIDs[] = $specialistUser->id;
                }
            }

        }else if(auth()->user()->user_type=='ADMIN'){
            $showAllSpecialists = true;
        }
    
    
    	$data = User::select('*')->where('user_type','SALES_SPECIALIST');
    	
    	if(!$showAllSpecialists){
    		$data = $data->whereIn('id',$permittedUserIDs);
    	}
    	
    	$data = $data->orderBy('id', 'DESC')->paginate(20);

        return view('panel.admin.visit.visitors.index', compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 20);
    
    
    }
    
    
    
    
    public function showDrivers(Request $request){
    
    
    	$permittedUserIDs = [];
    	
    	$showAllSpecialists = false;
        if(auth()->user()->user_type=='DRIVER'){
            $permittedUserIDs = [auth()->user()->id];

        }else if(auth()->user()->user_type=='STOCK'){
            $subUsers = auth()->user()->subUsers()->get();
            foreach($subUsers as $subUser){
                $permittedUserIDs[] = $subUser->id;
            }

        }else if(auth()->user()->user_type=='CSO'){
            $managerUsers = auth()->user()->subUsers()->get();
            foreach($managerUsers as $managerUser){
                foreach($managerUser->subUsers()->get() as $specialistUser){
                    $permittedUserIDs[] = $specialistUser->id;
                }
            }

        }else if(auth()->user()->user_type=='ADMIN'){
            $showAllSpecialists = true;
        }
    
    
    	$data = User::select('*')->where('user_type','DRIVER');
    	
    	if(!$showAllSpecialists){
    		$data = $data->whereIn('id',$permittedUserIDs);
    	}
    	
    	$data = $data->orderBy('id', 'DESC')->paginate(20);

        return view('panel.admin.visit.drivers.index', compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 20);
    
    
    }
    

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create(Request $request)

    {
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */

    public function storeDailyFlow(Request $request)
    {

        
	request()->validate([
	    'visitor_id' => 'required|integer|exists:users,id',
            'visit_flowchart_json' => 'required',
            'visit_flow_date' => 'required_without:visit_route_id|max:10',
            'visit_route_description' => 'nullable|max:1000',
            'visit_route_id' => 'nullable|integer|exists:visit_routes,id'
        ]);
        
        
        
        
        
        $data['visitor_user_id'] = $request->visitor_id;
        $data['creator_user_id'] = auth()->user()->id;
        $data['visit_route'] = $request->visit_flowchart_json;
        $data['visit_route_description'] = $request->visit_route_description;

        
        
        
        
        



	$errorMessage = Null;
	$successMessage = Null;
	if(!empty($request->visit_route_id)){
		$visitRoute = VisitRoute::findOrFail($request->visit_route_id);
		$visitRoute->visit_route = $request->visit_flowchart_json;
		$visitRoute->visit_route_description = $request->visit_route_description;
		
		$visitRoute->save();
		
		$successMessage = 'مسیر ویزیت با موفقیت ویرایش شد.';
        }else{
        
        	$flowDate = $request->visit_flow_date;
        	$flowDataExploded = explode('/',$request->visit_flow_date);
        
        	$data['visit_at_date'] = implode(
			'-',
			\Morilog\Jalali\CalendarUtils::toGregorian($flowDataExploded[0], $flowDataExploded[1], $flowDataExploded[2])
		);
        
        	//check date
        	$visitRoutesExists = VisitRoute::where('visitor_user_id',$request->visitor_id)->where('visit_at_date',$data['visit_at_date'])->exists();
        	if($visitRoutesExists){
        		$errorMessage = "قبلا برای این ویزیتور در تاریخ انتخاب شده، مسیر ویزیت تعریف شده است.";
        	}else{
        		VisitRoute::create($data);
        	
        	
        		$successMessage = 'مسیر ویزیت با موفقیت ایجاد شد.';
        	}
        	
        	
        
        }
        

	if(!empty($errorMessage)){
		return back()->with('danger', $errorMessage);
	}else{
		return redirect()->route('visit.show-users-daily-flow-list',$request->visitor_id)
		    ->with('success', $successMessage);
	}

        

    }
    
    
    
    
    
    
    public function storeDriverDailyFlow(Request $request)
    {

        
	request()->validate([
	    'visitor_id' => 'required|integer|exists:users,id',
            'visit_flowchart_json' => 'required',
            'visit_flow_date' => 'required_without:visit_route_id|max:10',
            'visit_route_description' => 'nullable|max:1000',
            'visit_route_id' => 'nullable|integer|exists:visit_routes,id'
        ]);
        
        
        
        
        
        $data['visitor_user_id'] = $request->visitor_id;
        $data['creator_user_id'] = auth()->user()->id;
        $data['visit_route'] = $request->visit_flowchart_json;
        $data['visit_route_description'] = $request->visit_route_description;

        
        
        
        
        



	$errorMessage = Null;
	$successMessage = Null;
	if(!empty($request->visit_route_id)){
		$visitRoute = DriverVisitRoute::findOrFail($request->visit_route_id);
		$visitRoute->visit_route = $request->visit_flowchart_json;
		$visitRoute->visit_route_description = $request->visit_route_description;
		
		$visitRoute->save();
		
		$successMessage = 'مسیر ویزیت با موفقیت ویرایش شد.';
        }else{
        
        	$flowDate = $request->visit_flow_date;
        	$flowDataExploded = explode('/',$request->visit_flow_date);
        
        	$data['visit_at_date'] = implode(
			'-',
			\Morilog\Jalali\CalendarUtils::toGregorian($flowDataExploded[0], $flowDataExploded[1], $flowDataExploded[2])
		);
        
        	//check date
        	$visitRoutesExists = DriverVisitRoute::where('visitor_user_id',$request->visitor_id)->where('visit_at_date',$data['visit_at_date'])->exists();
        	if($visitRoutesExists){
        		$errorMessage = "قبلا برای این راننده در تاریخ انتخاب شده، مسیر ویزیت تعریف شده است.";
        	}else{
        		DriverVisitRoute::create($data);
        	
        	
        		$successMessage = 'مسیر ویزیت با موفقیت ایجاد شد.';
        	}
        	
        	
        
        }
        

	if(!empty($errorMessage)){
		return back()->with('danger', $errorMessage);
	}else{
		return redirect()->route('visit.show-driver-users-daily-flow-list',$request->visitor_id)
		    ->with('success', $successMessage);
	}

        

    }


    /**
     * Display the specified resource.
     *
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */

    public function show(City $city)

    {
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */

    public function edit(City $city)

    {


      


    }


    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, City $city)

    {
       

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)

    {
       

    }
    
    
    
    
    public function userDailyFlowList(Request $request){
    	
    	
    	
    	$permittedUserIDs = [];
    	
    	$showAllSpecialists = false;
        if(auth()->user()->user_type=='SALES_SPECIALIST'){
            $permittedUserIDs = [auth()->user()->id];

        }else if(auth()->user()->user_type=='SALES_MANAGER'){
            $subUsers = auth()->user()->subUsers()->get();
            foreach($subUsers as $subUser){
                $permittedUserIDs[] = $subUser->id;
            }

        }else if(auth()->user()->user_type=='CSO'){
            $managerUsers = auth()->user()->subUsers()->get();
            foreach($managerUsers as $managerUser){
                foreach($managerUser->subUsers()->get() as $specialistUser){
                    $permittedUserIDs[] = $specialistUser->id;
                }
            }

        }else if(auth()->user()->user_type=='ADMIN'){
            $showAllSpecialists = true;
        }
        
        
        
        $visitorUser = User::select('*')->where('user_type','SALES_SPECIALIST')->where('id',$request->user_id);
    	
    	if(!$showAllSpecialists){
    		$visitorUser = $visitorUser->whereIn('id',$permittedUserIDs);
    	}
    	
    	$visitorUser= $visitorUser->firstOrFail();
        
    	
    	
    	$data = VisitRoute::select('*')->where('visitor_user_id',$request->user_id);
    	$data = $data->orderBy('id', 'DESC')->paginate(20);
    	

        return view('panel.admin.visit.flow.user-daily-flow-list', compact('data','visitorUser'))
            ->with('i', ($request->input('page', 1) - 1) * 20);
    
    }
    
    
    public function showDriverUserDailyFlowList(Request $request){
    	
    	$visitorUser = User::findOrFail($request->user_id);
    	
    	
    	$data = DriverVisitRoute::select('*')->where('visitor_user_id',$request->user_id);
    	$data = $data->orderBy('id', 'DESC')->paginate(20);
    	

        return view('panel.admin.visit.flow.driver-user-daily-flow-list', compact('data','visitorUser'))
            ->with('i', ($request->input('page', 1) - 1) * 20);
    
    }
    
    
    
    public function showVisitorDailyFlow(){
 	$now = \Carbon\Carbon::now();   
 	$nowInDate = $now->toDateString();
 	
 	
 	$visitRoute = VisitRoute::where('visitor_user_id',auth()->user()->id)->where('visit_at_date',$nowInDate)->first();
 	$visitRouteFlow = Null;
 	$visitRouteDescription = "";
 	
 	$visitGeoLocationsForShowOnMap = [];
	
	
	$visitRouteID = Null;
 	if(!empty($visitRoute)){
 	
	 	$visitRouteDescription = $visitRoute->visit_route_description;
	 	
	 	
	 	
	 	$visitRouteFlow=json_decode($visitRoute->visit_route);
	 	
	 	
	 	
	 	$visitingFlag = false;
	 	foreach($visitRouteFlow->operators as $operator){
	 	
	 	
	 			if($operator->properties->meta->type==2){ //geo location
	 	
	 				$visitGeoLocationsForShowOnMap[] = [
	 					'latitude' => $operator->properties->meta->latitude,
	 					'longitude' => $operator->properties->meta->longitude,
	 					'title' => $operator->properties->title,
	 				];
	 			
	 			}else if($operator->properties->meta->type==1){ //business
	 			
	 				$selectedBusiness = Business::findOrFail($operator->properties->meta->id);
	 			
	 			
	 				if(!empty($selectedBusiness->latitude) && !empty($selectedBusiness->longitude)){
		 				$visitGeoLocationsForShowOnMap[] = [
		 					'latitude' => $selectedBusiness->latitude,
		 					'longitude' => $selectedBusiness->longitude,
		 					'title' => $selectedBusiness->business_name."(".$selectedBusiness->first_name." ".$selectedBusiness->last_name.")",
		 				];
	 				}
	 			
	 			} 
	 	
	 	
	 	

		 		$status = "not_visited";
		 		$class ="not-visited";
		 		if(!empty($operator->properties->meta->exit_datetime)){
		 			$status = "visited";
		 			$class ="visited";
		 		}

		 		if(!$visitingFlag && $status=='not_visited'){ //set blue to the first visitable operator(in visiting)
		 		
		 			$status = "visiting";
		 			$class ="visiting";
		 			
		 			$visitingFlag = true;
		 		}
		 		
		 		$operator->properties->meta->visit_status = $status;
		 		$operator->properties->class=$class;
		 		
		 		
		 		//if($operator->properties->meta->type==1){
		 			$metaJsonEncoded = json_encode($operator->properties->meta,true);
		 			$operatorTitle = $operator->properties->title;
		 			$operator->properties->title = "<a href='#' onClick='showRouteElementModal($metaJsonEncoded,\"$operatorTitle\")'>".$operator->properties->title."</a>";
		 		//}else{
		 			//$businessID = $operator->properties->meta->id;
		 			//$operator->properties->title = $operator->properties->title;
		 			//'.route('invoices.create',['business_id'=>$businessID]).'
		 		//}
		 		
	 		
	 		
	 	}
 	
		$visitRouteID = $visitRoute->id;
 	}
 	
 	//$visitRoute->visit_route = json_encode($visitRouteFlow);
 	
 	
 	
 	
 	return view('panel.admin.visit.visitors.show-visitor-daily-flow',compact('visitRouteFlow','nowInDate','visitRouteDescription','visitRouteID','visitGeoLocationsForShowOnMap'));
 	
 	
    }
    
    
    
    
    public function showDriverVisitorDailyFlow(){
 	$now = \Carbon\Carbon::now();   
 	$nowInDate = $now->toDateString();
 	
 	
 	$visitRoute = DriverVisitRoute::where('visitor_user_id',auth()->user()->id)->where('visit_at_date',$nowInDate)->first();
 	$visitRouteFlow = Null;
 	$visitRouteDescription = "";
 	
 	$visitGeoLocationsForShowOnMap = [];
	
	
	$visitRouteID = Null;
 	if(!empty($visitRoute)){
 	
	 	$visitRouteDescription = $visitRoute->visit_route_description;
	 	
	 	
	 	
	 	$visitRouteFlow=json_decode($visitRoute->visit_route);
	 	
	 	
	 	
	 	$visitingFlag = false;
	 	foreach($visitRouteFlow->operators as $operator){
	 	
	 	
	 			if($operator->properties->meta->type==2){ //geo location
	 	
	 				$visitGeoLocationsForShowOnMap[] = [
	 					'latitude' => $operator->properties->meta->latitude,
	 					'longitude' => $operator->properties->meta->longitude,
	 					'title' => $operator->properties->title,
	 				];
	 			
	 			}else if($operator->properties->meta->type==1){ //business
	 			
	 				$selectedBusiness = Business::findOrFail($operator->properties->meta->id);
	 			
	 			
	 				if(!empty($selectedBusiness->latitude) && !empty($selectedBusiness->longitude)){
		 				$visitGeoLocationsForShowOnMap[] = [
		 					'latitude' => $selectedBusiness->latitude,
		 					'longitude' => $selectedBusiness->longitude,
		 					'title' => $selectedBusiness->business_name."(".$selectedBusiness->first_name." ".$selectedBusiness->last_name.")",
		 				];
	 				}
	 			
	 			} 
	 	
	 	
	 	

		 		$status = "not_visited";
		 		$class ="not-visited";
		 		if(!empty($operator->properties->meta->exit_datetime)){
		 			$status = "visited";
		 			$class ="visited";
		 		}

		 		if(!$visitingFlag && $status=='not_visited'){ //set blue to the first visitable operator(in visiting)
		 		
		 			$status = "visiting";
		 			$class ="visiting";
		 			
		 			$visitingFlag = true;
		 		}
		 		
		 		$operator->properties->meta->visit_status = $status;
		 		$operator->properties->class=$class;
		 		
		 		
		 		//if($operator->properties->meta->type==1){
		 			$metaJsonEncoded = json_encode($operator->properties->meta,true);
		 			$operatorTitle = $operator->properties->title;
		 			$operator->properties->title = "<a href='#' onClick='showRouteElementModal($metaJsonEncoded,\"$operatorTitle\")'>".$operator->properties->title."</a>";
		 		//}else{
		 			//$businessID = $operator->properties->meta->id;
		 			//$operator->properties->title = $operator->properties->title;
		 			//'.route('invoices.create',['business_id'=>$businessID]).'
		 		//}
		 		
	 		
	 		
	 	}
 	
		$visitRouteID = $visitRoute->id;
 	}
 	
 	//$visitRoute->visit_route = json_encode($visitRouteFlow);
 	
 	
 	
 	
 	return view('panel.admin.visit.drivers.show-visitor-daily-flow',compact('visitRouteFlow','nowInDate','visitRouteDescription','visitRouteID','visitGeoLocationsForShowOnMap'));
 	
 	
    }
    
    
    
    
    public function updateFlowOperator(Request $request){
    
    	request()->validate([
            'visit_route_id' => 'required|integer|exists:visit_routes,id',
            'operator_id' => 'required',
            'request_type' => 'required',
            'operator_description' => 'nullable|max:5000'
        ]);
        
        
        
        //TODO: LIMIT TO ACCESS TO OWN VISIT ROUTES
    	$visitRoute = VisitRoute::findOrFail($request->visit_route_id);
    	
    	
    	$visitRouteFlow=json_decode($visitRoute->visit_route);
    	
    	
    	
    	 
    	foreach($visitRouteFlow->operators as $key=>$operator){

	 	if($key==$request->operator_id){ 
	 	
	 		if($request->request_type=="set_entry_datetime"){
	 			$visitRouteFlow->operators->$key->properties->meta->entry_datetime = date('Y-m-d H:i:s', time());
	 		}else if($request->request_type=="set_exit_datetime"){
	 			$visitRouteFlow->operators->$key->properties->meta->exit_datetime = date('Y-m-d H:i:s', time());
	 		}else if($request->request_type=="redirect_to_show_geo_location"){
	 		
	 		
	 			$latitude = "";
	 			$longitude = "";
	 			
	 			if($visitRouteFlow->operators->$key->properties->meta->type==2){//location
		 			
		 			$latitude = $visitRouteFlow->operators->$key->properties->meta->latitude;
		 			$longitude = $visitRouteFlow->operators->$key->properties->meta->longitude;
	 			
	 			}else if($visitRouteFlow->operators->$key->properties->meta->type==1){ //business
	 			
	 				$selectedBusiness = Business::findOrFail($visitRouteFlow->operators->$key->properties->meta->id);
	 				
	 				$latitude = $selectedBusiness->latitude;
	 				$longitude = $selectedBusiness->longitude;
	 			}
	 			
	 			
	 			if(!empty($latitude) && !empty($longitude)){
	 				return redirect()->to('https://maps.google.com/?q='.$latitude.','.$longitude);
	 			}else{
	 				return back()->with('danger', "موقعیت مکانی مشخص نشده است");
	 			}
	 			
	 		
	 		
	 			
	 		}else if($request->request_type=="set_description"){
	 			//TODO: validate operator_description text
				
				if($this->isJson($request->operator_description)){
					
					$tempJsonDecoded = json_decode($request->operator_description,true);
					//$encodedJson = json_encode($tempJsonDecoded,true);
					
					
					
					$visitRouteFlow->operators->$key->properties->meta->visit_description = $tempJsonDecoded;
					
				}else{
					$visitRouteFlow->operators->$key->properties->meta->visit_description = $request->operator_description;
				}
				
				
	 		}else if($request->request_type=="redirect_to_create_invoice"){
	 			
	 			if($visitRouteFlow->operators->$key->properties->meta->type==2){//location
	 			
	 				//TODO: redirect with location filter parameter and show just in radius businesses in the destinition page
	 				return redirect()->route('businesses.index')
		    				->with('success', "برای صدور فاکتور میتوانید بر روی دکمه صدور فاکتور در هر ردیف کلیک کنید.");
	 			}else if($visitRouteFlow->operators->$key->properties->meta->type==1){ //business
	 				return redirect()->route('invoices.create',['business_id'=>$visitRouteFlow->operators->$key->properties->meta->id]);
	 			}
	 			
	 		}else if($request->request_type=="store_visit_file"){
	 			
	 			//upload image
				if (!empty($request->visit_image)) {
				    $uploadedBusinessImageFilesPath = [];
				    $uploadPath = '/uploads/visit/';
				    $uploadedFile = $this->uploadFile($request->visit_image, $uploadPath);

				    $visitImageDecodedArray = [];


				    if($this->isJson($visitRouteFlow->operators->$key->properties->meta->visit_image)){
					$visitImageDecodedArray = json_decode($visitRouteFlow->operators->$key->properties->meta->visit_image,true);
					$visitImageDecodedArray[] = $uploadedFile;
				    }else if(!empty($visitRouteFlow->operators->$key->properties->meta->visit_image)){
					$visitImageDecodedArray = [$visitRouteFlow->operators->$key->properties->meta->visit_image,$uploadedFile]; //for old version added(not json array)
				    }else{
					$visitImageDecodedArray = [$uploadedFile];
				    }
					
				   
				   
				    //$visitRouteFlow->operators->$key->properties->meta->visit_image = $uploadedFile;
				    $visitRouteFlow->operators->$key->properties->meta->visit_image = json_encode($visitImageDecodedArray,true);
				 
				    
				   
				}
	 			
	 		}
	 		
	 		
	 		
	 	
	 	}	 		
 		
 	}
 	
 	
 	$visitRoute->visit_route = json_encode($visitRouteFlow);
 	
    	$visitRoute->save();
    	
    	return redirect()->route('visit.visitor.show-daily-flow')
		    ->with('success', "مسیر ویزیت با موفقیت بروزرسانی شد.");
    
    }
    
    
    
    private function uploadFile($file, $uploadPath)
    {
        $uploaded_file_dir_signature = "";

        if ($file == null) {
            $uploaded_file_dir_signature = "";
        } else {

            if ($file->isValid()) {
                $fileName = str_replace(' ', '', time()) . rand(1000, 9999) . '_' . $file->getClientOriginalName();
                $destinationPath = public_path() . $uploadPath;

                $file->move($destinationPath, $fileName);
                $uploaded_file_dir_signature = $uploadPath . '/' . $fileName;
            } else {
//                    $this->img_upload_error_msg = 'آپلود تصویر ناموفق بود';
//                    goto catch_block;
            }
        }

        return $uploaded_file_dir_signature;
    }
    
    
    
    public function updateDriverFlowOperator(Request $request){
    
    	request()->validate([
            'visit_route_id' => 'required|integer|exists:driver_visit_routes,id',
            'operator_id' => 'required',
            'request_type' => 'required',
            'operator_description' => 'nullable|max:500'
        ]);
        
        
        
        //TODO: LIMIT TO ACCESS TO OWN VISIT ROUTES
    	$visitRoute = DriverVisitRoute::findOrFail($request->visit_route_id);
    	
    	
    	$visitRouteFlow=json_decode($visitRoute->visit_route);
    	
    	
    	
    	 
    	foreach($visitRouteFlow->operators as $key=>$operator){

	 	if($key==$request->operator_id){ 
	 	
	 		if($request->request_type=="set_entry_datetime"){
	 			$visitRouteFlow->operators->$key->properties->meta->entry_datetime = date('Y-m-d H:i:s', time());
	 		}else if($request->request_type=="set_exit_datetime"){
	 			$visitRouteFlow->operators->$key->properties->meta->exit_datetime = date('Y-m-d H:i:s', time());
	 		}else if($request->request_type=="redirect_to_show_geo_location"){
	 		
	 		
	 			$latitude = "";
	 			$longitude = "";
	 			
	 			if($visitRouteFlow->operators->$key->properties->meta->type==2){//location
		 			
		 			$latitude = $visitRouteFlow->operators->$key->properties->meta->latitude;
		 			$longitude = $visitRouteFlow->operators->$key->properties->meta->longitude;
	 			
	 			}else if($visitRouteFlow->operators->$key->properties->meta->type==1){ //business
	 			
	 				$selectedBusiness = Business::findOrFail($visitRouteFlow->operators->$key->properties->meta->id);
	 				
	 				$latitude = $selectedBusiness->latitude;
	 				$longitude = $selectedBusiness->longitude;
	 			}
	 			
	 			
	 			if(!empty($latitude) && !empty($longitude)){
	 				return redirect()->to('https://maps.google.com/?q='.$latitude.','.$longitude);
	 			}else{
	 				return back()->with('danger', "موقعیت مکانی مشخص نشده است");
	 			}
	 			
	 		
	 		
	 			
	 		}else if($request->request_type=="set_description"){
	 			//TODO: validate operator_description text
	 			$visitRouteFlow->operators->$key->properties->meta->visit_description = $request->operator_description;
	 		}else if($request->request_type=="redirect_to_create_invoice"){
	 			
	 			if($visitRouteFlow->operators->$key->properties->meta->type==2){//location
	 			
	 				//TODO: redirect with location filter parameter and show just in radius businesses in the destinition page
	 				return redirect()->route('businesses.index')
		    				->with('success', "برای صدور فاکتور میتوانید بر روی دکمه صدور فاکتور در هر ردیف کلیک کنید.");
	 			}else if($visitRouteFlow->operators->$key->properties->meta->type==1){ //business
	 				return redirect()->route('invoices.create',['business_id'=>$visitRouteFlow->operators->$key->properties->meta->id]);
	 			}
	 			
	 		}
	 		
	 		
	 		
	 	
	 	}	 		
 		
 	}
 	
 	
 	$visitRoute->visit_route = json_encode($visitRouteFlow);
 	
    	$visitRoute->save();
    	
    	return redirect()->route('visit.driver-visitor.show-daily-flow')
		    ->with('success', "مسیر ویزیت با موفقیت بروزرسانی شد.");
    
    }
	
	
	
	
	
	public function showUserDailyFlowReport(Request $request){
		
		
		if(empty($request->visit_at_date)){
			abort(404);
		}
		
		
		
		
		
		
		$permittedUserIDs = [];
	    	
	    	$showAllSpecialists = false;
		if(auth()->user()->user_type=='SALES_SPECIALIST'){
		    $permittedUserIDs = [auth()->user()->id];

		}else if(auth()->user()->user_type=='SALES_MANAGER'){
		    $subUsers = auth()->user()->subUsers()->get();
		    foreach($subUsers as $subUser){
		        $permittedUserIDs[] = $subUser->id;
		    }

		}else if(auth()->user()->user_type=='CSO'){
		    $managerUsers = auth()->user()->subUsers()->get();
		    foreach($managerUsers as $managerUser){
		        foreach($managerUser->subUsers()->get() as $specialistUser){
		            $permittedUserIDs[] = $specialistUser->id;
		        }
		    }

		}else if(auth()->user()->user_type=='ADMIN'){
		    $showAllSpecialists = true;
		}
		
		
		
		$visitorUser = User::select('*')->where('user_type','SALES_SPECIALIST')->where('id',$request->user_id);
	    	
	    	if(!$showAllSpecialists){
	    		$visitorUser = $visitorUser->whereIn('id',$permittedUserIDs);
	    	}
	    	
	    	$visitorUser= $visitorUser->firstOrFail();
		
		
		
		
		
		
		
    	
    	
		$visitRoute = VisitRoute::where('visitor_user_id',$visitorUser->id)->where('visit_at_date',$request->visit_at_date)->first();
		
		$visitRouteData = $visitRoute->visit_route;
		
		$userVisitRouteReportData = [];
		if(!empty($visitRouteData)){
			$visitRouteData = json_decode($visitRouteData,true);
			if(isset($visitRouteData['operators']) && !empty($visitRouteData['operators'])){
			
				foreach($visitRouteData['operators'] as $visitRouteOperator){
					
					$userVisitRouteReportData[] = [
						'title' => $visitRouteOperator['properties']['title'],
						'type' => $visitRouteOperator['properties']['meta']['type'],
						'business_id' => ($visitRouteOperator['properties']['meta']['type']==1)?$visitRouteOperator['properties']['meta']['id']:Null,
						'latitude' => ($visitRouteOperator['properties']['meta']['type']==2)?$visitRouteOperator['properties']['meta']['latitude']:Null,
						'longitude' => ($visitRouteOperator['properties']['meta']['type']==2)?$visitRouteOperator['properties']['meta']['longitude']:Null,
						'entry_datetime' => $visitRouteOperator['properties']['meta']['entry_datetime'],
						'exit_datetime' => $visitRouteOperator['properties']['meta']['exit_datetime'],
						'visit_description' => $visitRouteOperator['properties']['meta']['visit_description'],
						'visit_image' => !empty($visitRouteOperator['properties']['meta']['visit_image'])?$visitRouteOperator['properties']['meta']['visit_image']:Null,
					];
				}

			}
		}
		
		
		return view('panel.admin.visit.flow.user-daily-flow-report', compact('visitorUser','visitRoute','userVisitRouteReportData'));  
		
	}
	
	
	
	
	
	public function showDriverUserDailyFlowReport(Request $request){
		
		
		if(empty($request->visit_at_date)){
			abort(404);
		}
		
		
		$visitorUser = User::findOrFail($request->user_id);
    	
    	
		$visitRoute = DriverVisitRoute::where('visitor_user_id',$visitorUser->id)->where('visit_at_date',$request->visit_at_date)->first();
		
		$visitRouteData = $visitRoute->visit_route;
		
		$userVisitRouteReportData = [];
		if(!empty($visitRouteData)){
			$visitRouteData = json_decode($visitRouteData,true);
			if(isset($visitRouteData['operators']) && !empty($visitRouteData['operators'])){
			
				foreach($visitRouteData['operators'] as $visitRouteOperator){
					
					$userVisitRouteReportData[] = [
						'title' => $visitRouteOperator['properties']['title'],
						'type' => $visitRouteOperator['properties']['meta']['type'],
						'business_id' => ($visitRouteOperator['properties']['meta']['type']==1)?$visitRouteOperator['properties']['meta']['id']:Null,
						'latitude' => ($visitRouteOperator['properties']['meta']['type']==2)?$visitRouteOperator['properties']['meta']['latitude']:Null,
						'longitude' => ($visitRouteOperator['properties']['meta']['type']==2)?$visitRouteOperator['properties']['meta']['longitude']:Null,
						'entry_datetime' => $visitRouteOperator['properties']['meta']['entry_datetime'],
						'exit_datetime' => $visitRouteOperator['properties']['meta']['exit_datetime'],
						'visit_description' => $visitRouteOperator['properties']['meta']['visit_description'],
					];
				}

			}
		}
		
		
		return view('panel.admin.visit.flow.driver-user-daily-flow-report', compact('visitorUser','visitRoute','userVisitRouteReportData'));  
		
	}
	
	
	
	public function showSetSpecialistForm(Request $request){
	
		if(empty($request->user_id)){
			abort(404);
		}
		
		
		$visitorUser = User::findOrFail($request->user_id);
		
		$parentUser = User::find($visitorUser->parent_user_id);
	
		
		$specialistUsers = User::select('*')->where('user_type','SALES_MANAGER')->orderBy('id', 'DESC')->get();
		
		
	
		return view('panel.admin.visit.visitors.set-specialist-form', compact('visitorUser','parentUser','specialistUsers'));  
	
	}



	public function updateSpecialistUser(Request $request){
	
		request()->validate([
		    'visitor_user_id' => 'required|integer|exists:users,id',
		    'specialist_user_id' => 'required|integer|exists:users,id'
		]);
		
		
		$visitorUser = User::where('user_type','SALES_SPECIALIST')->where('id',$request->visitor_user_id)->firstOrFail();
		
		
		$specialistUser = User::where('user_type','SALES_MANAGER')->where('id',$request->specialist_user_id)->firstOrFail();
		
		$visitorUser->parent_user_id = $specialistUser->id;
		$visitorUser->save();
		
		
		return redirect()->back()
		    ->with('success', "اطلاعات مورد نظر با موفقیت ویرایش شد");
	
	
	}


	function isJson($string) {
	   json_decode($string);
	   return json_last_error() === JSON_ERROR_NONE;
	}












	public function showCartable(Request $request){
    
    	$businessID = $request->business_id;
    	
    	//TODO: just access to permitted businesses
    	$business = Business::findOrFail($businessID);
    	
    	$invoices = $business->invoices()->get();
    	
    	
    	$credits = $business->credits()->get();
    	
    	
    	$businessMobiles = json_decode($business->mobiles,true);
    	$businessPhones = [];
    	if(!empty($businessMobiles) && count($businessMobiles)>0){
    		foreach($businessMobiles as $businessMobile){
    		
    			if(!empty($businessMobile)){
    				$businessPhones[] = (int)Helpers::convertFaToEnNumbersV2($businessMobile);
    			}
    		
    		}
    	}
    	
    	if(!empty($business->phone_number)){
    		$businessPhones[] =  (int)Helpers::convertFaToEnNumbersV2($business->phone_number);
    	}
    	
    	$getCallsQuery = DB::connection('mysql_voip')->table('cdr');
    		
    	foreach($businessPhones as $businessPhone){
    		$getCallsQuery = $getCallsQuery->orWhere(function($query) use($businessPhone){
					    $query->where('src','LIKE', '%'.$businessPhone.'%')->orWhere('dst','LIKE','%'.$businessPhone.'%');
					});
    	}
	
		$getCallsQuery=$getCallsQuery->orderBy('calldate','DESC');
	
		$businessCalls=$getCallsQuery->paginate(20);
	
	
	
		//TODO: maybe will be limited to specialists
		$businessAlarms = CallTargetAlarm::where('business_id',$businessID)->get();
    	
    	
    	
    	//TODO: maybe will be limited to specialists
		$businessNotes = Note::where('business_id',$businessID)->orderBy('created_at','DESC')->get();
    	
    	
    	 return view('panel.admin.businesses.cartable', compact('business','invoices','credits','businessCalls','businessAlarms','businessNotes'));
    	
    	
    
    }






	public function showVisitorReport(Request $request){
	
		
		$visitorUser = User::where('id',$request->visitor_user_id)->where('user_type','SALES_SPECIALIST');
		
		if(auth()->user()->user_type=='SALES_SPECIALIST'){
			$visitorUser = $visitorUser->where('id',auth()->user()->id);
		}
		
		$visitorUser = $visitorUser->firstOrFail();
		
		
		$parentUser = NULL;
		if(!empty($visitorUser->parent_user_id)){
			$parentUser = User::where('id',$visitorUser->parent_user_id)->firstOrFail();
		}
		
		
		
		$fromDateGregorian = NULL;
        if(!empty($request->from_date)){
        
        	$fromDate = $request->from_date;
        	$fromDateGregorian=\Morilog\Jalali\Jalalian::fromFormat('Y/m/d', $fromDate)->toCarbon()->toDateTimeString();
        	
        
        }
		
		$toDateGregorian = NULL;
        if(!empty($request->to_date)){
        
        	$toDate = $request->to_date;
        	$toDateGregorian=\Morilog\Jalali\Jalalian::fromFormat('Y/m/d', $toDate)->toCarbon()->toDateTimeString();
        	
        
        }
		
		
		
		$clues = Clue::where('creator_user_id',$visitorUser->id);
		if(!empty($fromDateGregorian)){
			$clues = $clues->whereDate('created_at' , '>=',$fromDateGregorian);
		}
		if(!empty($toDateGregorian)){
			$clues = $clues->whereDate('created_at' , '<=',$toDateGregorian);
		}
		$clues = $clues->orderBy('created_at','DESC')->paginate(20,['*'],'clues_page');
		
		
		
		$invoices = Invoice::where('creator_id',$visitorUser->id);
		if(!empty($fromDateGregorian)){
			$invoices = $invoices->whereDate('created_at' , '>=',$fromDateGregorian);
		}
		if(!empty($toDateGregorian)){
			$invoices = $invoices->whereDate('created_at' , '<=',$toDateGregorian);
		}
		$invoices = $invoices->orderBy('created_at','DESC')->get();
		
		$businesses = Business::where('creator_id',$visitorUser->id);
		if(!empty($fromDateGregorian)){
			$businesses = $businesses->whereDate('created_at' , '>=',$fromDateGregorian);
		}
		if(!empty($toDateGregorian)){
			$businesses = $businesses->whereDate('created_at' , '<=',$toDateGregorian);
		}
		$businesses = $businesses->orderBy('created_at','DESC')->paginate(20,['*'],'businesses_page');
		
		
		
		$userCalls = [];
		
		if(!empty($visitorUser->voip_id)){
			$getCallsQuery = DB::connection('mysql_voip')->table('cdr');
				
			$getCallsQuery = $getCallsQuery->orWhere(function($query) use($visitorUser){
							$query->where('src','LIKE', '%'.$visitorUser->voip_id.'%')->orWhere('dst','LIKE','%'.$visitorUser->voip_id.'%');
						});
						
						
			if(!empty($fromDateGregorian)){
				$getCallsQuery = $getCallsQuery->whereDate('calldate' , '>=',$fromDateGregorian);
			}
			if(!empty($toDateGregorian)){
				$getCallsQuery = $getCallsQuery->whereDate('calldate' , '<=',$toDateGregorian);
			}
		
			$getCallsQuery=$getCallsQuery->orderBy('calldate','DESC');
		
			$visitorUser=$getCallsQuery->paginate(20,['*'],'calls_page');
		}
		
		
		///TODO: maybe will be limiteded
		$userAlarms = CallTargetAlarm::where('specialist_user_id',$visitorUser->id);
		if(!empty($fromDateGregorian)){
			$userAlarms = $userAlarms->whereDate('created_at' , '>=',$fromDateGregorian);
		}
		if(!empty($toDateGregorian)){
			$userAlarms = $userAlarms->whereDate('created_at' , '<=',$toDateGregorian);
		}
		$userAlarms = $userAlarms->paginate(20,['*'],'alarms_page');
    	
    	//TODO: maybe will be limiteded
		$userNotes = Note::where('creator_id',$visitorUser->id);
		if(!empty($fromDateGregorian)){
			$userNotes = $userNotes->whereDate('created_at' , '>=',$fromDateGregorian);
		}
		if(!empty($toDateGregorian)){
			$userNotes = $userNotes->whereDate('created_at' , '<=',$toDateGregorian);
		}
		$userNotes = $userNotes->orderBy('created_at','DESC')->paginate(20,['*'],'user_notes_page');
		
		
		//TODO: maybe will be limiteded
		$userProcessReports = ClueProcessReport::where('creator_id',$visitorUser->id);
		if(!empty($fromDateGregorian)){
			$userProcessReports = $userProcessReports->whereDate('created_at' , '>=',$fromDateGregorian);
		}
		if(!empty($toDateGregorian)){
			$userProcessReports = $userProcessReports->whereDate('created_at' , '<=',$toDateGregorian);
		}
		$userProcessReports = $userProcessReports->orderBy('created_at','DESC')->paginate(20,['*'],'clue_process_report_page');
		
		
		
		
		
		
		
		
		
		
		
		
		
		//begin clue charts
		
		
		
		
		$cluesTempObj = Clue::select('*');
		if(!empty($fromDateGregorian)){
			$cluesTempObj = $cluesTempObj->whereDate('created_at' , '>=',$fromDateGregorian);
		}
		if(!empty($toDateGregorian)){
			$cluesTempObj = $cluesTempObj->whereDate('created_at' , '<=',$toDateGregorian);
		}
		$cluesTempObj = $cluesTempObj->where('creator_user_id',$visitorUser->id);
		
		$chartOpenClosedClues = Clue::selectRaw('clue_closed, count(id) as clue_count');
		if(!empty($fromDateGregorian)){
			$chartOpenClosedClues = $chartOpenClosedClues->whereDate('created_at' , '>=',$fromDateGregorian);
		}
		if(!empty($toDateGregorian)){
			$chartOpenClosedClues = $chartOpenClosedClues->whereDate('created_at' , '<=',$toDateGregorian);
		}
		$chartOpenClosedClues = $chartOpenClosedClues->where('creator_user_id',$visitorUser->id);
		
		
		$chartStatusGroupedClues = Clue::selectRaw('clue_status, count(id) as clue_count');
		if(!empty($fromDateGregorian)){
			$chartStatusGroupedClues = $chartStatusGroupedClues->whereDate('created_at' , '>=',$fromDateGregorian);
		}
		if(!empty($toDateGregorian)){
			$chartStatusGroupedClues = $chartStatusGroupedClues->whereDate('created_at' , '<=',$toDateGregorian);
		}
		$chartStatusGroupedClues = $chartStatusGroupedClues->where('creator_user_id',$visitorUser->id);

 
 
		$minDate = Clue::select(DB::raw('min(created_at) AS min_created_at'));
		if(!empty($fromDateGregorian)){
			$minDate = $minDate->whereDate('created_at' , '>=',$fromDateGregorian);
		}
		if(!empty($toDateGregorian)){
			$minDate = $minDate->whereDate('created_at' , '<=',$toDateGregorian);
		}
		$minDate = $minDate->first();
		$fromDateGregorian = $minDate->min_created_at;
		$fromDateJalali = \Morilog\Jalali\Jalalian::forge($fromDateGregorian)->format('%Y/%m/%d');
 
        
        $maxDate =  Clue::select(DB::raw('max(created_at) AS max_created_at'));
		if(!empty($fromDateGregorian)){
			$maxDate = $maxDate->whereDate('created_at' , '>=',$fromDateGregorian);
		}
		if(!empty($toDateGregorian)){
			$maxDate = $maxDate->whereDate('created_at' , '<=',$toDateGregorian);
		}
		$maxDate = $maxDate->first();
		$toDateGregorian = $maxDate->max_created_at;
		$toDateJalali = \Morilog\Jalali\Jalalian::forge($toDateGregorian)->format('%Y/%m/%d');
        
        
        
        //BEGIN: calculate date periods
        $explodedDate = explode('/',$toDateJalali);
		$toCheckDate = $explodedDate[0].$explodedDate[1];
		
		$fromHoldYear = NULL;
		$fromHoldMonth = NULL;
		do{
			
			if(empty($fromHoldYear)){
				$explodedDate = explode('/',$fromDateJalali);
				$fromHoldYear = $explodedDate[0];
				$fromHoldMonth = $explodedDate[1]; 
			}else{
				if($fromHoldMonth<=11){
					$fromHoldMonth++;
				}else{
					$fromHoldMonth = 1;
					$fromHoldYear++;
				}
			}
			
			if($fromHoldMonth<10){
				$fromHoldMonth = "0".((int)$fromHoldMonth);
			}
			$datePeriods[] = $fromHoldYear."/".$fromHoldMonth;
			
			
		
		
		}while($toCheckDate>=$fromHoldYear.$fromHoldMonth);
		//END: calculate date periods
	
	
		$chart1DataArray = [];
		foreach($datePeriods as $key=>$datePeriod){
		
			$fromDateJalali = $datePeriod."/01";
			$toDateJalali = $datePeriod."/31";
			
			$fromDateGregorianTemp=\Morilog\Jalali\Jalalian::fromFormat('Y/m/d', $fromDateJalali)->toCarbon()->toDateTimeString();
			$toDateGregorianTemp=\Morilog\Jalali\Jalalian::fromFormat('Y/m/d', $toDateJalali)->toCarbon()->toDateTimeString();
			
			
			$tempObj = clone $cluesTempObj;
			
			$periodDate = $tempObj->whereDate('created_at' , '>=',$fromDateGregorianTemp)->whereDate('created_at' , '<=',$toDateGregorianTemp);
			$periodCount = $periodDate->count();
			
			if($periodCount>0){
				$chart1DataArray[$datePeriod] = $periodCount;
			}
			
			
			
		
		}
		
		
		
		
		$chartOpenClosedClues = $chartOpenClosedClues->groupBy('clue_closed')->get();
	
		$chartOpenClosedCluesDataArray = [];
		foreach($chartOpenClosedClues as $chartItem){
			$tempTitle = "باز";
			if($chartItem->clue_closed==1){
				$tempTitle = "بسته";
			}
			
			$chartOpenClosedCluesDataArray[] = [
				'title' => $tempTitle,
				'count' => $chartItem->clue_count
			];
		}
		
		
		
		$chartStatusGroupedClues = $chartStatusGroupedClues->groupBy('clue_status')->get();
		$chartStatusGroupedDataArray = [];
		
		$clueTitlesArr = [
			'clue' => 'ثبت سرنخ',
			'customer' => 'ثبت مشتری',
			'validation' => 'اعتبارسنجی',
			'sale' => 'فروش',
			'pre_invoice'=>'‍‍پیش فاکتور',
			'invoice'=>'فاکتور',
			'return_from_sale' => 'برگشت از فروش'
		];
		
					
		foreach($chartStatusGroupedClues as $chartItem){
			
			$tempTitle = $clueTitlesArr[$chartItem->clue_status];
			
			$chartStatusGroupedDataArray[] = [
				'title' => $tempTitle,
				'count' => $chartItem->clue_count
			];
		}
		
		
		//end clue charts
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		//begin get visitor flow ist
    	$visitorFlows = VisitRoute::select('*')->where('visitor_user_id',$visitorUser->id);
		if(!empty($fromDateGregorian)){
			$explodedDate = explode(' ',$fromDateGregorian);
			
			$visitorFlows = $visitorFlows->whereDate('visit_at_date' , '>=',$explodedDate[0]);
		}
		if(!empty($toDateGregorian)){
			$explodedDate = explode(' ',$toDateGregorian);
			
			$visitorFlows = $visitorFlows->whereDate('visit_at_date' , '<=',$explodedDate[0]);
		}
		
    	$visitorFlows = $visitorFlows->orderBy('id', 'DESC')->paginate(20,['*'],'visitor_flows_page');
		//end get visitor flow list
		
		
		
		
		return view('panel.admin.visit.visitors.report', compact('visitorUser','visitorFlows','parentUser','clues','invoices','businesses','userCalls','userNotes','userAlarms','chart1DataArray','chartOpenClosedCluesDataArray','chartStatusGroupedDataArray','userProcessReports'));
	
	}
  
  
	public function addNewNoteToVisitor(Request $request, $visitorUserID)
    {
		
		$visitorUser = User::where('id',$visitorUserID)->where('user_type','SALES_SPECIALIST')->firstOrFail();

		//TODO: validate to access own customer
        $this->validate($request, [

            'note_text' => 'required|max:1500',
            //'visitor_user_id' => 'required|exists:users,id'

        ]);



		$inputs = [
			'creator_id' => $visitorUser->id,
			'referenced_by_user' => auth()->user()->id,
			'note_text' => $request->note_text,
		];

        $note = Note::create($inputs);



        return redirect()->back()
            ->with('success', 'یادداشت با موفقیت ایجاد شد');

    }

}
