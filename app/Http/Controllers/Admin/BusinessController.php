<?php


namespace App\Http\Controllers\Admin;


use App\Models\Business;
use App\Models\BusinessCredit;
use Helpers;
use App\Models\City;
use App\Models\Customer;
use App\Models\Note;
use App\Models\Business_type;
use App\Models\CallTargetAlarm;
use Illuminate\Http\Request;
use DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;



use Excel;
use App\Exports\BusinessExport;

class BusinessController extends Controller

{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    function __construct()

    {

        $this->middleware('permission:business-list|business-create|business-edit|business-delete', ['only' => ['index', 'show','showBusinessesReport']]);

        $this->middleware('permission:business-create', ['only' => ['create', 'store']]);

        $this->middleware('permission:business-edit', ['only' => ['edit', 'update']]);

        $this->middleware('permission:business-delete', ['only' => ['destroy']]);
        
        $this->middleware('permission:business-accounting-approve', ['only' => ['showAccountantApprovingForm','accountantApprovingBusiness']]);
        
        $this->middleware('permission:business-sales-specialist-approve', ['only' => ['showSalesSpecialistApprovingForm','salesSpecialistApprovingBusiness']]);
        
        $this->middleware('permission:show-business-cartable', ['only' => ['showCartable','addNewNote']]);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */



    public function index(Request $request)

    {
    
    
    
    	
    
    

        $permittedConditions = Helpers::getBusinessesAccessPermittedConditions(auth()->user());

        $businesses = Business::select('*')->where('id','<',0);

        foreach($permittedConditions as $key=>$condition){
            $toFireCondition1 = [];
            if(!empty($condition['ignore_cities']) && $condition['ignore_cities']){
                $toFireCondition1 = [
                    'operand'=>'LIKE',
                    'values' => '%'
                ];
            }else{
                $toFireCondition1 = [
                    'operand'=>'IN',
                    'values' => $condition['city_ids']
                ];
            }

            $toFireCondition2 = [];
            if(!empty($condition['ignore_creator_id']) && $condition['ignore_creator_id']){
                $toFireCondition2 = [
                    'operand'=>'LIKE',
                    'values' => '%'
                ];
            }else{
				
                $toFireCondition2 = [
                    'operand'=>'=',
                    'values' => $condition['creator_id']
                ];
            }
            $businesses = $businesses->orWhere(function($query) use($toFireCondition1,$toFireCondition2) {
                if($toFireCondition1['operand']=='IN'){
                    $query->whereIn('city_id', $toFireCondition1['values'])->where('creator_id',$toFireCondition2['operand'],$toFireCondition2['values']);
                }else{
                    $query->where('city_id',$toFireCondition1['operand'], $toFireCondition1['values'])->where('creator_id',$toFireCondition2['operand'],$toFireCondition2['values']);
                }
            });
        }
        
 
 
 
 	$permittedBusinessIDs = $businesses->get()->pluck('id')->toArray();
 	
 	unset($businesses);
 	
 	
        $businesses = Business::select('*')->whereIn('id',$permittedBusinessIDs);
 
        
        //filters:
        if(!empty($request->city_id)){
        
        	$toFilterCityIDs = [];
        	
        	$toFilterCityIDs[] = $request->city_id;
        
        	$filterSelectedCity = City::findOrFail($request->city_id);
        	
        	$filterSelectedCityRegions = $filterSelectedCity->subsets()->get();
        		
		foreach($filterSelectedCityRegions as $filterSelectedRegion){
		
			$toFilterCityIDs[] = $filterSelectedRegion->id;
		
		}
	
		$businesses = $businesses->whereIn('city_id',$toFilterCityIDs);
        
        
        }else if(!empty($request->state_id)){
        	$toFilterCityIDs = [];
        	
        	$toFilterCityIDs[] = $request->state_id;
        	
        	$filterSelectedState = City::findOrFail($request->state_id);
        	$filterSelectedStateCities = $filterSelectedState->subsets()->get();
        	foreach($filterSelectedStateCities as $filterSelectedCity){
        		$toFilterCityIDs[] = $filterSelectedCity->id;
        		
        		$filterSelectedCityRegions = $filterSelectedCity->subsets()->get();
        		
        		foreach($filterSelectedCityRegions as $filterSelectedRegion){
        		
        			$toFilterCityIDs[] = $filterSelectedRegion->id;
        		
        		}
        	
        	}
        	
        	
        	$businesses = $businesses->whereIn('city_id',$toFilterCityIDs);
        	
        	
        }
        
        
        
        
        
        if(!empty($request->customer_degree)){
        
        	$businesses = $businesses->where('customer_degree',$request->customer_degree);
        
        }
        
        if(!empty($request->business_type_id)){
        
        	$businesses = $businesses->where('business_type_id',$request->business_type_id);
        
        }
        
        if(!empty($request->customer_recruitment_type)){
        
        	$businesses = $businesses->where('customer_recruitment_type',$request->customer_recruitment_type);
        
        }
        
        
        if(!empty($request->assets_estimate_rial)){
        
        
        	$businesses = $businesses->where('assets_estimate_rial',$request->assets_estimate_rial);
        
        }
        
        
        
        if(!empty($request->search_key)){
        
        	$searchKey = $request->search_key;
        
        	$businesses = $businesses->where(function($query) use($searchKey) {
		    $query->where('first_name','LIKE', '%'.$searchKey.'%')->orWhere('last_name','LIKE', '%'.$searchKey.'%')
		   	 ->orWhere('business_name','LIKE', '%'.$searchKey.'%')->orWhere('phone_number','LIKE', '%'.$searchKey.'%')
		   	 ->orWhere('mobiles','LIKE', '%'.$searchKey.'%');
		});
		
		
        

        
        }
		
		
		
		
		if(!empty($request->export_excel) && $request->export_excel==1){
			
			
			
			$businesses = $businesses->orderBy('created_at','DESC')->get();
			
			$dataToExport = [];
			foreach($businesses as $key=>$item){
				$dataToExport[] = [
					$item['first_name'],
					$item['last_name'],
					$item->getFullStateCityRegion(),
					$item['national_code'],
					$item['postal_code'],
					$item['phone_number'],
					$item['mobiles'],
					$item['working_personnel']
				
				];
				
			}
			
			return Excel::download(new BusinessExport($dataToExport), 'businesses.xlsx');
		}
        
        
        

        $businesses = $businesses->orderBy('created_at','DESC')->paginate(20);
        
        
        
        
        
        
        
        
        
        
        $permittedStateRegionCities = Helpers::getPermittedCities(auth()->user());

        $states = City::select('*');
        if(is_array($permittedStateRegionCities['permitted_states'])){
            $states = $states->whereIn('id',$permittedStateRegionCities['permitted_states']);
        }else if($permittedStateRegionCities['permitted_states']!='%'){
            $states = $states->whereIn('id',[]);
        }
        $states = $states->where('unit_type', 0)->where('unit_status', 1)->orderBy('unit_order', 'ASC')->get();

        $cities = [];
        if(!empty($request->state_id)){
        	//TODO: just show permitted cities
            $cities = City::where('state_id',$request->state_id)->where('unit_status',1)->orderBy('unit_order','ASC')->get();
        }

        
        
        $enums = config('enum');
        $businessTypes = Business_type::where('business_type_status',1)->orderBy('business_type_order','ASC')->get();

        return view('panel.admin.businesses.index', compact('businesses','states','cities','enums','businessTypes'))
            ->with('i', (request()->input('page', 1) - 1) * 20);

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create(Request $request)

    {

        $permittedStateRegionCities = Helpers::getPermittedCities(auth()->user());

        $states = City::select('*');
        if(is_array($permittedStateRegionCities['permitted_states'])){
            $states = $states->whereIn('id',$permittedStateRegionCities['permitted_states']);
        }else if($permittedStateRegionCities['permitted_states']!='%'){
            $states = $states->whereIn('id',[]);
        }
        $states = $states->where('unit_type', 0)->where('unit_status', 1)->orderBy('unit_order', 'ASC')->get();

        $cities = [];
        $regions = [];
        if(old('state_id')){
            $cities = City::where('state_id',old('state_id'))->where('unit_status',1)->orderBy('unit_order','ASC')->get();

            if(old('city_id')){
                $regions = City::where('state_id',old('city_id'))->where('unit_status',1)->orderBy('unit_order','ASC')->get();
            }
        }
        
        
        $customerData = [];
        if(!empty($request->customer_id)){
        	$customer = Customer::findOrFail($request->customer_id);
        	$customerData['first_name'] = $customer->first_name;
        	$customerData['last_name'] = $customer->last_name;
        	$customerData['business_name'] = $customer->business_name;
        	$customerData['address'] = $customer->address;
        	$customerData['phone_number'] = $customer->phone;
        	$customerData['mobiles'] = [$customer->mobile,''];
        	$customerData['customer_id'] = $customer->id;
        	
        }
        
        
        
        $enums = config('enum');
	$businessTypes = Business_type::where('business_type_status',1)->orderBy('business_type_order','ASC')->get();

        return view('panel.admin.businesses.create', compact('states','cities','regions','enums','businessTypes','customerData'));

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)

    {


        request()->validate([

            'first_name' => 'required|max:20',
            'last_name' => 'required|max:20',
            'business_name' => 'required|max:100',
            'state_id' => 'required|integer|exists:cities,id',
            'city_id' => 'required|integer|exists:cities,id',
            'region_id' => 'nullable|integer|exists:cities,id',
            'address' => 'required|max:255',
            'shipping_name' => 'max:100',
            'national_code' => 'nullable|numeric',
            'postal_code' => 'nullable|numeric',
            'phone_number' => 'nullable|numeric',
            'mobiles.*' => 'nullable',
            'latitude' => 'max:50',
            'longitude' => 'max:50',
            'buy_method' => 'nullable|integer',
            'business_ownership' => 'nullable|integer',
            'ownership_doc_file' => 'nullable|image|max:15360',
            'visit_card_file' => 'nullable|image|max:15360',
            'business_license_file' => 'nullable|image|max:15360',

            'activity_amount' => 'nullable|integer',
            'working_personnel' => 'nullable|integer',
            'assets_estimate_rial' => 'nullable|integer',
            'assets_estimate_box' => 'nullable|integer',
            'neighbors.*' => '',
            'partners.*' => '',
            'business_cheque_file' => 'nullable|image|max:15360',
            'business_images.*' => 'nullable|image|max:15360',
            'suggested_credit_limit' => 'nullable|integer',
            'business_description' => 'max:500',

            'sell_approved_by_seller' => 'nullable|integer',
            
            'no_validation_required' => 'nullable|integer',
            
            
            
            'business_type_id' => 'nullable|integer',
            'customer_degree' => 'nullable|max:10',
            'customer_legal_type'=>'nullable',
            'customer_recruitment_type' => 'nullable',
            
            'copartner_name' => 'nullable|max: 255',
            'copartner_phone' => 'nullable|max:100',
            
            'birth_date' => 'nullable|max:10',
            
            
            'customer_id' => 'nullable|integer|exists:customers,id'

        ]);


        $businessData = [];


        //upload business_images and set file_pathes to $businessData
        if (!empty($request->business_images)) {
            $uploadedBusinessImageFilesPath = [];
            $uploadPath = '/uploads/businesses/business_image_files';
            $businessImagesArray = is_array($request->business_images) ? $request->business_images : [$request->business_images];
            foreach ($businessImagesArray as $businessImage) {
                $toUploadFile = $businessImage;
                $uploadedBusinessImageFilesPath[] = $this->uploadBusinessFile($toUploadFile, $uploadPath);
            }


            $businessData['business_images'] = json_encode($uploadedBusinessImageFilesPath, true);

        }


        //upload ownership_doc_file_path and set file_pathes to $businessData
        if (!empty($request->ownership_doc_file)) {

            $uploadPath = '/uploads/businesses/ownership_doc_files';
            $toUploadFile = $request->file('ownership_doc_file');
            $businessData['ownership_doc_file_path'] = $this->uploadBusinessFile($toUploadFile, $uploadPath);

        }


        //upload visit_card_file_path and set file_pathes to $businessData
        if (!empty($request->visit_card_file)) {

            $uploadPath = '/uploads/businesses/visit_card_files';
            $toUploadFile = $request->file('visit_card_file');
            $businessData['visit_card_file_path'] = $this->uploadBusinessFile($toUploadFile, $uploadPath);

        }


        //upload business_license_file_path and set file_pathes to $businessData
        if (!empty($request->business_license_file)) {

            $uploadPath = '/uploads/businesses/business_license_files';
            $toUploadFile = $request->file('business_license_file');
            $businessData['business_license_file_path'] = $this->uploadBusinessFile($toUploadFile, $uploadPath);

        }


        //upload business_cheque_file_path and set file_pathes to $businessData
        if (!empty($request->business_cheque_file)) {

            $uploadPath = '/uploads/businesses/business_cheque_files';
            $toUploadFile = $request->file('business_cheque_file');
            $businessData['business_cheque_file_path'] = $this->uploadBusinessFile($toUploadFile, $uploadPath);

        }


        $businessData['partners'] = json_encode($request->partners, true);
        $businessData['neighbors'] = json_encode($request->neighbors, true);
        $businessData['mobiles'] = json_encode($request->mobiles, true);


        $businessData['first_name'] = $request->first_name;
        $businessData['last_name'] = $request->last_name;
        $businessData['business_name'] = $request->business_name;


        if (!empty($request->region_id)) {
            $businessData['city_id'] = $request->region_id;
        } else if (!empty($request->city_id)) {
            $businessData['city_id'] = $request->city_id;
        } else if (!empty($request->state_id)) {
            $businessData['city_id'] = $request->state_id;
        }

        $businessData['address'] = $request->address;
        $businessData['shipping_name'] = $request->shipping_name;
        $businessData['national_code'] = $request->national_code;
        $businessData['postal_code'] = $request->postal_code;
        $businessData['phone_number'] = $request->phone_number;
        $businessData['latitude'] = $request->latitude;
        $businessData['longitude'] = $request->longitude;
        $businessData['buy_method'] = $request->buy_method;
        $businessData['business_ownership'] = $request->business_ownership;
        $businessData['activity_amount'] = $request->activity_amount;
        $businessData['working_personnel'] = $request->working_personnel;
        $businessData['assets_estimate_rial'] = $request->assets_estimate_rial;
        $businessData['assets_estimate_box'] = $request->assets_estimate_box;
        $businessData['suggested_credit_limit'] = $request->suggested_credit_limit;
        $businessData['business_description'] = $request->business_description;
        $businessData['sell_approved_by_seller'] = $request->sell_approved_by_seller;
        
        $businessData['no_validation_required'] = $request->no_validation_required;
        
        
        $businessData['business_type_id'] = $request->business_type_id;
        $businessData['customer_degree'] = $request->customer_degree;
        $businessData['customer_legal_type'] = $request->customer_legal_type;
        $businessData['customer_recruitment_type'] = $request->customer_recruitment_type;
        
        
        $businessData['copartner_name'] = $request->copartner_name;
        $businessData['copartner_phone'] = $request->copartner_phone;
        
        $businessData['birth_date'] = $request->birth_date;
        
        


        $businessData['creator_id'] = auth()->user()->id;
        
        if(!empty($request->customer_id)){
        	$businessData['customer_id'] = $request->customer_id;
        }
        
        Business::create($businessData);


        return redirect()->route('businesses.index')
            ->with('success', 'کسب و کار جدید با موفقیت افزوده شد');

    }


    /**
     * Display the specified resource.
     *
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */

    public function show(Business $business)

    {


        $business->mobiles = json_decode($business->mobiles, true);

        $business->neighbors = json_decode($business->neighbors, true);
        $business->partners = json_decode($business->partners, true);
        $business->business_images = json_decode($business->business_images, true);



	$enums = config('enum');
	$businessTypes = Business_type::where('business_type_status',1)->orderBy('business_type_order','ASC')->get();

        return view('panel.admin.businesses.show', compact('business','enums','businessTypes'));

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */

    public function edit(Business $business)
    {

        $permittedStateRegionCities = Helpers::getPermittedCities(auth()->user());

        $states = City::select('*');
        if(is_array($permittedStateRegionCities['permitted_states'])){
            $states = $states->whereIn('id',$permittedStateRegionCities['permitted_states']);
        }else if($permittedStateRegionCities['permitted_states']!='%'){
            $states = $states->whereIn('id',[]);
        }
        $states = $states->where('unit_type', 0)->where('unit_status', 1)->orderBy('unit_order', 'ASC')->get();
        $cities = [];
        $regions = [];

        $storedCityID = $business->city_id;
        //detect state_id, city_id, region_id
        $storedCity = City::find($business->city_id);
        if ($storedCity->unit_type == 0) { //is state(means that the city and region didnt chosen)
            $business->state_id = $storedCityID;
            $business->city_id = [];
            $business->region_id = [];

            $cities = City::select('*');
            if(is_array($permittedStateRegionCities['permitted_cities'])){
                $cities = $cities->whereIn('id',$permittedStateRegionCities['permitted_cities']);
            }else if($permittedStateRegionCities['permitted_cities']!='%'){
                $cities = $cities->whereIn('id',[]);
            }
            $cities = $cities->where('state_id', $business->state_id)->where('unit_status', 1)->orderBy('unit_order', 'ASC')->get();

        } else if ($storedCity->unit_type == 1) {//is city(means that the region didnt chosen)
            $business->state_id = $storedCity->state_id;
            $business->city_id = $storedCityID;
            $business->region_id = [];


            $cities = City::select('*');
            if(is_array($permittedStateRegionCities['permitted_cities'])){
                $cities = $cities->whereIn('id',$permittedStateRegionCities['permitted_cities']);
            }else if($permittedStateRegionCities['permitted_cities']!='%'){
                $cities = $cities->whereIn('id',[]);
            }
            $cities = $cities->where('state_id', $business->state_id)->where('unit_status', 1)->orderBy('unit_order', 'ASC')->get();


            $regions = City::select('*');
            if(is_array($permittedStateRegionCities['permitted_regions'])){
                $regions = $regions->whereIn('id',$permittedStateRegionCities['permitted_regions']);
            }else if($permittedStateRegionCities['permitted_regions']!='%'){
                $regions = $regions->whereIn('id',[]);
            }
            $regions = $cities->where('state_id', $business->city_id)->where('unit_status', 1)->orderBy('unit_order', 'ASC')->get();


        } else if ($storedCity->unit_type == 2) {//is region(means that all of state and city and region selected)
            $business->state_id = City::find($storedCity->state_id)->parent()->first()->id;
            $business->city_id = $storedCity->state_id;
            $business->region_id = $storedCityID;

            $cities = City::select('*');
            if(is_array($permittedStateRegionCities['permitted_cities'])){
                $cities = $cities->whereIn('id',$permittedStateRegionCities['permitted_cities']);
            }else if($permittedStateRegionCities['permitted_cities']!='%'){
                $cities = $cities->whereIn('id',[]);
            }
            $cities = $cities->where('state_id', $business->state_id)->where('unit_status', 1)->orderBy('unit_order', 'ASC')->get();


            $regions = City::select('*');
            if(is_array($permittedStateRegionCities['permitted_regions'])){
                $regions = $regions->whereIn('id',$permittedStateRegionCities['permitted_regions']);
            }else if($permittedStateRegionCities['permitted_regions']!='%'){
                $regions = $regions->whereIn('id',[]);
            }

            $regions = $regions->where('state_id', $business->city_id)->where('unit_status', 1)->orderBy('unit_order', 'ASC')->get();

        }


        $business->mobiles = !empty($business->mobiles)?json_decode($business->mobiles, true):[];
        $business->neighbors = !empty($business->neighbors)?json_decode($business->neighbors, true):[];

        $business->partners = !empty($business->partners)?json_decode($business->partners, true):[];

        $business->business_images = !empty($business->business_images)?json_decode($business->business_images, true):[];
        
        
        $enums = config('enum');
	$businessTypes = Business_type::where('business_type_status',1)->orderBy('business_type_order','ASC')->get();

        return view('panel.admin.businesses.edit', compact('business', 'states', 'cities', 'regions','enums','businessTypes'));

    }


    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, Business $business)
    {


        request()->validate([

            'first_name' => 'required|max:20',
            'last_name' => 'required|max:20',
            'business_name' => 'required|max:100',
            'state_id' => 'required|integer|exists:cities,id',
            'city_id' => 'required|integer|exists:cities,id',
            'region_id' => 'nullable|integer|exists:cities,id',
            'address' => 'required|max:255',
            'shipping_name' => 'max:100',
            'national_code' => 'nullable|numeric',
            'postal_code' => 'nullable|numeric',
            'phone_number' => 'nullable|numeric',
            'mobiles.*' => 'nullable',
            'latitude' => 'max:50',
            'longitude' => 'max:50',
            'buy_method' => 'nullable|integer',
            'business_ownership' => 'nullable|integer',
            'ownership_doc_file' => 'nullable|image|max:15360',
            'visit_card_file' => 'nullable|image|max:15360',
            'business_license_file' => 'nullable|image|max:15360',

            'activity_amount' => 'nullable|integer',
            'working_personnel' => 'nullable|integer',
            'assets_estimate_rial' => 'nullable|integer',
            'assets_estimate_box' => 'nullable|integer',
            'neighbors.*' => '',
            'partners.*' => '',
            'business_cheque_file' => 'nullable|image|max:15360',
            'business_images.*' => 'nullable|image|max:15360',
            'suggested_credit_limit' => 'nullable|integer',
            'business_description' => 'nullable|max:500',

            'sell_approved_by_seller' => 'nullable|integer',
            
            'no_validation_required' => 'nullable|integer',
            
            'business_type_id' => 'nullable|integer',
            'customer_degree' => 'nullable|max:10',
            'customer_legal_type'=>'nullable',
            'customer_recruitment_type' => 'nullable',
            
            'copartner_name' => 'nullable|max:255',
            'copartner_phone' => 'nullable|max:100',
            
            'birth_date' => 'nullable|max:10'

        ]);


        $businessData = [];


        //upload business_images and set file_pathes to $businessData
        if (!empty($request->business_images)) {
            $uploadedBusinessImageFilesPath = [];
            $uploadPath = '/uploads/businesses/business_image_files';
            $businessImagesArray = is_array($request->business_images) ? $request->business_images : [$request->business_images];
            foreach ($businessImagesArray as $businessImage) {
                $toUploadFile = $businessImage;
                $uploadedBusinessImageFilesPath[] = $this->uploadBusinessFile($toUploadFile, $uploadPath);
            }


            $businessData['business_images'] = json_encode($uploadedBusinessImageFilesPath, true);

        }


        //upload ownership_doc_file_path and set file_pathes to $businessData
        if (!empty($request->ownership_doc_file)) {

            $uploadPath = '/uploads/businesses/ownership_doc_files';
            $toUploadFile = $request->file('ownership_doc_file');
            $businessData['ownership_doc_file_path'] = $this->uploadBusinessFile($toUploadFile, $uploadPath);

        }


        //upload visit_card_file_path and set file_pathes to $businessData
        if (!empty($request->visit_card_file)) {

            $uploadPath = '/uploads/businesses/visit_card_files';
            $toUploadFile = $request->file('visit_card_file');
            $businessData['visit_card_file_path'] = $this->uploadBusinessFile($toUploadFile, $uploadPath);

        }


        //upload business_license_file_path and set file_pathes to $businessData
        if (!empty($request->business_license_file)) {

            $uploadPath = '/uploads/businesses/business_license_files';
            $toUploadFile = $request->file('business_license_file');
            $businessData['business_license_file_path'] = $this->uploadBusinessFile($toUploadFile, $uploadPath);

        }


        //upload business_cheque_file_path and set file_pathes to $businessData
        if (!empty($request->business_cheque_file)) {

            $uploadPath = '/uploads/businesses/business_cheque_files';
            $toUploadFile = $request->file('business_cheque_file');
            $businessData['business_cheque_file_path'] = $this->uploadBusinessFile($toUploadFile, $uploadPath);

        }


        $businessData['partners'] = json_encode($request->partners, true);
        $businessData['neighbors'] = json_encode($request->neighbors, true);
        $businessData['mobiles'] = json_encode($request->mobiles, true);


        $businessData['first_name'] = $request->first_name;
        $businessData['last_name'] = $request->last_name;
        $businessData['business_name'] = $request->business_name;


        if (!empty($request->region_id)) {
            $businessData['city_id'] = $request->region_id;
        } else if (!empty($request->city_id)) {
            $businessData['city_id'] = $request->city_id;
        } else if (!empty($request->state_id)) {
            $businessData['city_id'] = $request->state_id;
        }

        $businessData['address'] = $request->address;
        $businessData['shipping_name'] = $request->shipping_name;
        $businessData['national_code'] = $request->national_code;
        $businessData['postal_code'] = $request->postal_code;
        $businessData['phone_number'] = $request->phone_number;
        $businessData['latitude'] = $request->latitude;
        $businessData['longitude'] = $request->longitude;
        $businessData['buy_method'] = $request->buy_method;
        $businessData['business_ownership'] = $request->business_ownership;
        $businessData['activity_amount'] = $request->activity_amount;
        $businessData['working_personnel'] = $request->working_personnel;
        $businessData['assets_estimate_rial'] = $request->assets_estimate_rial;
        $businessData['assets_estimate_box'] = $request->assets_estimate_box;
        $businessData['suggested_credit_limit'] = $request->suggested_credit_limit;
        $businessData['business_description'] = $request->business_description;
        $businessData['sell_approved_by_seller'] = $request->sell_approved_by_seller;
        
        $businessData['no_validation_required'] = $request->no_validation_required;
        
        
        
        $businessData['business_type_id'] = $request->business_type_id;
        $businessData['customer_degree'] = $request->customer_degree;
        $businessData['customer_legal_type'] = $request->customer_legal_type;
        $businessData['customer_recruitment_type'] = $request->customer_recruitment_type;
        
        $businessData['copartner_name'] = $request->copartner_name;
        $businessData['copartner_phone'] = $request->copartner_phone;
        
        
        $businessData['birth_date'] = $request->birth_date;
        


        $business->update($businessData);


        return redirect()->route('businesses.index')
            ->with('success', 'کسب و کار با موفقیت ویرایش شد');


    }


    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */

    public function destroy(Business $business)

    {

        $business->delete();


        return redirect()->route('businesses.index')
            ->with('success', 'کسب و کار مورد نظر با موفقیت حذف شد');

    }


    private function uploadBusinessFile($file, $uploadPath)
    {
        $uploaded_file_dir_signature = "";

        if ($file == null) {
            $uploaded_file_dir_signature = "";
        } else {

            if ($file->isValid()) {
                $fileName = str_replace(' ', '', time()) . rand(1000, 9999) . '_' . $file->getClientOriginalName();
                $destinationPath = public_path() . $uploadPath;

                $file->move($destinationPath, $fileName);
                $uploaded_file_dir_signature = $uploadPath . '/' . $fileName;
//                    if($request->edit_id!=Null) {
//                        $to_remove_dir_signature = \App\Models\AgencyRequest::find($request->edit_id)->signature;
//                    }
            } else {
//                    $this->img_upload_error_msg = 'آپلود تصویر ناموفق بود';
//                    goto catch_block;
            }
        }

        return $uploaded_file_dir_signature;
    }
    
    
    
    
    public function showAccountantApprovingForm(Request $request){
    
    
    	//TODO: limit to own businesses
    	$business = Business::findOrFail($request->business_id);
    	
    	
    	if($business->approved_by_sales_specialist==0){
    		return redirect()->back()
            		->with('danger', 'تایید ابتدا باید توسط کارشناس فروش انجام شود');
    	}
    	
    	
    	
    	return view('panel.admin.accountant.businesses.approve', compact('business'));
    
    }
    
    
    public function accountantApprovingBusiness(Request $request){
    
    	
    	
    	
    	request()->validate([

	    'business_id' => 'required|exists:businesses,id',
            'accountant_approving_description' => 'required|max:1000',
            'approved_by_accountant' => 'required|integer',


        ]);
        
        //TODO: should add transaction:
        
        
        $business = Business::findOrFail($request->business_id);
    	
    	
    	$business->accountant_approving_description = $request->accountant_approving_description;
    	$business->approved_by_accountant = $request->approved_by_accountant;
    	
    	$business->save();
		
		
		
		if($request->approved_by_accountant==1){
			$creditAmount = !empty($business->suggested_credit_limit)?$business->suggested_credit_limit:0;
			$creditData = [
				'business_id' => $business->id,
				'credit_amount' => $creditAmount,
				'invoice_id' => Null,
				'credit_description' => "اعتبار اولیه",
			];
			
			BusinessCredit::create($creditData);
		}
		
		
		
		
    	
    	return redirect()->route('businesses.index')
            ->with('success', 'تایید/عدم تایید کسب و کار با موفقیت ثبت شد');
    
    }
    
    
    public function showSalesSpecialistApprovingForm(Request $request){
    
    
    	//TODO: limit to own businesses
    	$business = Business::findOrFail($request->business_id);
    	
    	
    	return view('panel.admin.accountant.businesses.approve-by-sales-specialist', compact('business'));
    
    }
    
    
    public function salesSpecialistApprovingBusiness(Request $request){
    
    	
    	
    	
    	request()->validate([

	    'business_id' => 'required|exists:businesses,id',
            'sales_specialist_approving_description' => 'required|max:1000',
            'approved_by_sales_specialist' => 'required|integer',


        ]);
        
        
        
        
        $business = Business::findOrFail($request->business_id);
    	
    	
    	$business->sales_specialist_approving_description = $request->sales_specialist_approving_description;
    	$business->approved_by_sales_specialist = $request->approved_by_sales_specialist;
    	
    	$business->save();
    	
    	return redirect()->route('businesses.index')
            ->with('success', 'تایید/عدم تایید کسب و کار با موفقیت ثبت شد');
    
    }
	
	
	
	
	
	
	
	
	
	
	
	public function showBusinessesReport(Request $request){
    
	
	
	
		$permittedConditions = Helpers::getBusinessesAccessPermittedConditions(auth()->user());

        //$businesses = Business::select(DB::raw('count(id) as `data`'), DB::raw('YEAR(created_at) year, MONTH(created_at) month'))->where('id','<',0);
        $businesses = Business::select('*')->where('id','<',0);

        foreach($permittedConditions as $key=>$condition){
            $toFireCondition1 = [];
            if(!empty($condition['ignore_cities']) && $condition['ignore_cities']){
                $toFireCondition1 = [
                    'operand'=>'LIKE',
                    'values' => '%'
                ];
            }else{
                $toFireCondition1 = [
                    'operand'=>'IN',
                    'values' => $condition['city_ids']
                ];
            }

            $toFireCondition2 = [];
            if(!empty($condition['ignore_creator_id']) && $condition['ignore_creator_id']){
                $toFireCondition2 = [
                    'operand'=>'LIKE',
                    'values' => '%'
                ];
            }else{
                $toFireCondition2 = [
                    'operand'=>'=',
                    'values' => $condition['creator_id']
                ];
            }
            $businesses = $businesses->orWhere(function($query) use($toFireCondition1,$toFireCondition2) {
                if($toFireCondition1['operand']=='IN'){
                    $query->whereIn('city_id', $toFireCondition1['values'])->where('creator_id',$toFireCondition2['operand'],$toFireCondition2['values']);
                }else{
                    $query->where('city_id',$toFireCondition1['operand'], $toFireCondition1['values'])->where('creator_id',$toFireCondition2['operand'],$toFireCondition2['values']);
                }
            });
        }
        
 
        
        
        
        //filters:
        if(!empty($request->city_id)){
        
        	$toFilterCityIDs = [];
        	
        	$toFilterCityIDs[] = $request->city_id;
        
        	$filterSelectedCity = City::findOrFail($request->city_id);
        	
        	$filterSelectedCityRegions = $filterSelectedCity->subsets()->get();
        		
		foreach($filterSelectedCityRegions as $filterSelectedRegion){
		
			$toFilterCityIDs[] = $filterSelectedRegion->id;
		
		}
	
		$businesses = $businesses->whereIn('city_id',$toFilterCityIDs);
        
        
        }else if(!empty($request->state_id)){
        	$toFilterCityIDs = [];
        	
        	$toFilterCityIDs[] = $request->state_id;
        	
        	$filterSelectedState = City::findOrFail($request->state_id);
        	$filterSelectedStateCities = $filterSelectedState->subsets()->get();
        	foreach($filterSelectedStateCities as $filterSelectedCity){
        		$toFilterCityIDs[] = $filterSelectedCity->id;
        		
        		$filterSelectedCityRegions = $filterSelectedCity->subsets()->get();
        		
        		foreach($filterSelectedCityRegions as $filterSelectedRegion){
        		
        			$toFilterCityIDs[] = $filterSelectedRegion->id;
        		
        		}
        	
        	}
        	
        	
        	$businesses = $businesses->whereIn('city_id',$toFilterCityIDs);
        	
        	
        }
        
        
        
        
        
        if(!empty($request->customer_degree)){
        
        	$businesses = $businesses->where('customer_degree',$request->customer_degree);
        
        }
        
        if(!empty($request->business_type_id)){
        
        	$businesses = $businesses->where('business_type_id',$request->business_type_id);
        
        }
        
        if(!empty($request->customer_recruitment_type)){
        
        	$businesses = $businesses->where('customer_recruitment_type',$request->customer_recruitment_type);
        
        }
        
        
        if(!empty($request->assets_estimate_rial)){
        
        
        	$businesses = $businesses->where('assets_estimate_rial',$request->assets_estimate_rial);
        
        }
		
		
	if(!empty($request->from_date)){
        
        	$fromDate = $request->from_date;
        	$fromDateGregorian=\Morilog\Jalali\Jalalian::fromFormat('Y/m/d', $fromDate)->toCarbon()->toDateTimeString();
        	
        	$businesses = $businesses->whereDate('created_at' , '>=',$fromDateGregorian);
        	
        	
        	$fromDateJalali = $request->from_date;
        
        }else{
        	$minDate = Business::select(DB::raw('min(created_at) AS min_created_at'))->first();
        	$fromDateGregorian = $minDate->min_created_at;
        	$fromDateJalali = \Morilog\Jalali\Jalalian::forge($fromDateGregorian)->format('%Y/%m/%d');
        }
        
        if(!empty($request->to_date)){
        
        	$toDate = $request->to_date;
        	$toDateGregorian=\Morilog\Jalali\Jalalian::fromFormat('Y/m/d', $toDate)->toCarbon()->toDateTimeString();
        
        	$businesses = $businesses->whereDate('created_at' , '<=',$toDateGregorian);
        	
        	$toDateJalali = $request->to_date;
        
        }else{
        
        	$maxDate = Business::select(DB::raw('max(created_at) AS max_created_at'))->first();
        	$toDateGregorian = $maxDate->max_created_at;
        	$toDateJalali = \Morilog\Jalali\Jalalian::forge($toDateGregorian)->format('%Y/%m/%d');
        
        }
        
        
        
        //BEGIN: calculate date periods
        $datePeriods = [];
        if($request->report_period==1){//Daily
        
        
        	$explodedDate = explode('/',$toDateJalali);
		$toCheckDate = $explodedDate[0].$explodedDate[1].$explodedDate[2];
		$fromHoldYear = NULL;
		$fromHoldMonth = NULL;
		$fromHoldDay = NULL;
		do{
			
			if(empty($fromHoldYear)){
				$explodedDate = explode('/',$fromDateJalali);
				$fromHoldYear = $explodedDate[0];
				$fromHoldMonth = $explodedDate[1]; 
				$fromHoldDay = $explodedDate[2]; 
			}else{
				if($fromHoldDay<=30){
					$fromHoldDay++;
				}else if($fromHoldMonth<=11){
					$fromHoldDay=1;
					$fromHoldMonth++;
				}else{
					$fromHoldDay=1;
					$fromHoldMonth = 1;
					$fromHoldYear++;
				}
			}
			
			
			if($fromHoldMonth<10){
				$fromHoldMonth = "0".((int)$fromHoldMonth);
			}
			
			if($fromHoldDay<10){
				$fromHoldDay = "0".((int)$fromHoldDay);
			}
			
			$datePeriods[] = $fromHoldYear."/".$fromHoldMonth."/".$fromHoldDay;
			
			
		
		
		}while($toCheckDate>($fromHoldYear.$fromHoldMonth.$fromHoldDay));
        
        
		
	}else{//Monthly
	
		$explodedDate = explode('/',$toDateJalali);
		$toCheckDate = $explodedDate[0].$explodedDate[1];
		
		$fromHoldYear = NULL;
		$fromHoldMonth = NULL;
		do{
			
			if(empty($fromHoldYear)){
				$explodedDate = explode('/',$fromDateJalali);
				$fromHoldYear = $explodedDate[0];
				$fromHoldMonth = $explodedDate[1]; 
			}else{
				if($fromHoldMonth<=11){
					$fromHoldMonth++;
				}else{
					$fromHoldMonth = 1;
					$fromHoldYear++;
				}
			}
			
			if($fromHoldMonth<10){
				$fromHoldMonth = "0".((int)$fromHoldMonth);
			}
			$datePeriods[] = $fromHoldYear."/".$fromHoldMonth;
			
			
		
		
		}while($toCheckDate>=$fromHoldYear.$fromHoldMonth);
		
	}
	//END: calculate date periods
	
	
	
	$chart1DataArray = [];
	foreach($datePeriods as $key=>$datePeriod){
	
		$fromDateJalali = NULL;
		$toDateJalali = NULL;
		if($request->report_period==1){//Daily
			$fromDateJalali = $datePeriod;
			$toDateJalali = $datePeriod;
		}else{
			$fromDateJalali = $datePeriod."/01";
			$toDateJalali = $datePeriod."/31";
		}
		
		$fromDateGregorian=\Morilog\Jalali\Jalalian::fromFormat('Y/m/d', $fromDateJalali)->toCarbon()->toDateTimeString();
		$toDateGregorian=\Morilog\Jalali\Jalalian::fromFormat('Y/m/d', $toDateJalali)->toCarbon()->toDateTimeString();
		
		
		$tempObj = clone $businesses;
		
		$periodDate = $tempObj->whereDate('created_at' , '>=',$fromDateGregorian)->whereDate('created_at' , '<=',$toDateGregorian);
		$periodCount = $periodDate->count();
		
		if($periodCount>0){
			$chart1DataArray[$datePeriod] = $periodCount;
		}
		
		
		
	
	}
	
	
        
	//$businesses = $businesses->groupBy('year','month');
        //$businesses = $businesses->get();
		
        
        
		
		
        
        
        
        $permittedStateRegionCities = Helpers::getPermittedCities(auth()->user());

        $states = City::select('*');
        if(is_array($permittedStateRegionCities['permitted_states'])){
            $states = $states->whereIn('id',$permittedStateRegionCities['permitted_states']);
        }else if($permittedStateRegionCities['permitted_states']!='%'){
            $states = $states->whereIn('id',[]);
        }
        $states = $states->where('unit_type', 0)->where('unit_status', 1)->orderBy('unit_order', 'ASC')->get();

        $cities = [];
        if(!empty($request->state_id)){
        	//TODO: just show permitted cities
            $cities = City::where('state_id',$request->state_id)->where('unit_status',1)->orderBy('unit_order','ASC')->get();
        }

        
        
        $enums = config('enum');
        $businessTypes = Business_type::where('business_type_status',1)->orderBy('business_type_order','ASC')->get();
		
		
		
		
        
 
        return view('panel.admin.businesses.report', compact('businesses','states','cities','enums','businessTypes','chart1DataArray'));
    	
    	
    	
    	
    	
    	
    }
    
    
    
    
    
    public function showCartable(Request $request){
    
    	$businessID = $request->business_id;
    	
    	//TODO: just access to permitted businesses
    	$business = Business::findOrFail($businessID);
    	
    	$invoices = $business->invoices()->get();
    	
    	
    	$credits = $business->credits()->get();
    	
    	
    	$businessMobiles = json_decode($business->mobiles,true);
    	$businessPhones = [];
    	if(!empty($businessMobiles) && count($businessMobiles)>0){
    		foreach($businessMobiles as $businessMobile){
    		
    			if(!empty($businessMobile)){
    				$businessPhones[] = (int)Helpers::convertFaToEnNumbersV2($businessMobile);
    			}
    		
    		}
    	}
    	
    	if(!empty($business->phone_number)){
    		$businessPhones[] =  (int)Helpers::convertFaToEnNumbersV2($business->phone_number);
    	}
    	
    	$getCallsQuery = DB::connection('mysql_voip')->table('cdr');
    		
    	foreach($businessPhones as $businessPhone){
    		$getCallsQuery = $getCallsQuery->orWhere(function($query) use($businessPhone){
					    $query->where('src','LIKE', '%'.$businessPhone.'%')->orWhere('dst','LIKE','%'.$businessPhone.'%');
					});
    	}
	
		$getCallsQuery=$getCallsQuery->orderBy('calldate','DESC');
	
		$businessCalls=$getCallsQuery->paginate(20);
	
	
	
		//TODO: maybe will be limited to specialists
		$businessAlarms = CallTargetAlarm::where('business_id',$businessID)->get();
    	
    	
    	
    	//TODO: maybe will be limited to specialists
		$businessNotes = Note::where('business_id',$businessID)->orderBy('created_at','DESC')->get();
    	
    	
    	 return view('panel.admin.businesses.cartable', compact('business','invoices','credits','businessCalls','businessAlarms','businessNotes'));
    	
    	
    
    }
    
    
    
    
    public function addNewNote(Request $request)
    {

	//TODO: validate to access own customer
        $this->validate($request, [

            'note_text' => 'required|max:1500',
            'business_id' => 'required|exists:businesses,id'

        ]);



        $business = Business::findOrFail($request->business_id);

	$inputs = [
		'creator_id' => auth()->user()->id,
		'business_id' => $business->id,
		'note_text' => $request->note_text,
	];

        $note = Note::create($inputs);



        return redirect()->back()
            ->with('success', 'یادداشت با موفقیت ایجاد شد');

    }
    
    
    

}
