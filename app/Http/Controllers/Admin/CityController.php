<?php


namespace App\Http\Controllers\Admin;


use App\Models\City;
use Helpers;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class CityController extends Controller

{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    function __construct()

    {

        $this->middleware('permission:city-list|city-create|city-edit|city-delete', ['only' => ['index', 'show']]);

        $this->middleware('permission:city-create', ['only' => ['create', 'store']]);

        $this->middleware('permission:city-edit', ['only' => ['edit', 'update']]);

        $this->middleware('permission:city-delete', ['only' => ['destroy']]);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {

//        $unitType = !empty($request->unit_type)?$request->unit_type:0;

        $unitParentID = !empty($request->parent)?$request->parent:Null;

        $unitParent = Null;
        $unitType = 0;
        if(!empty($unitParentID)){
            $unitParent = City::find($unitParentID);
            $unitType = $unitParent->unit_type+1;
        }



        $data = City::where('unit_type',$unitType);

        if(!empty($unitParentID)){
            $data = $data->where('state_id',$unitParentID);
        }

        $data = $data->orderBy('unit_order', 'ASC')->paginate(50);



        return view('panel.admin.cities.index', compact('data','unitType', 'unitParent'))
            ->with('i', ($request->input('page', 1) - 1) * 50);

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create(Request $request)

    {

        $unitType = !empty($request->unit_type)?$request->unit_type:Null;
        $parent_id = !empty($request->parent_id)?$request->parent_id:Null;

        $unitParent = Null;
        if(!empty($parent_id)){
            $unitParent = City::find($parent_id);
        }



        return view('panel.admin.cities.create', compact('unitType','unitParent'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {

        $this->validate($request, [

            'unit_parent' => 'nullable|integer|exists:cities,id',
            'unit_name' => 'required|max:50',
            'unit_order' => 'required|integer',
            'unit_status' => 'required|integer',

        ]);


        $parentUnit = Null;
        if(!empty($request->unit_parent)){
            $parentUnit = City::find($request->unit_parent);
        }

        $toStoreData = [
            'unit_name'=>$request->unit_name,
            'state_id'=>$request->unit_parent,
            'unit_type'=>!empty($parentUnit)?($parentUnit->unit_type+1):0,
            'unit_order'=>$request->unit_order,
            'unit_status'=>$request->unit_status,
        ];



        $city = City::create($toStoreData);



        return redirect()->route('cities.index',['parent'=>$request->unit_parent])
            ->with('success', 'واحد جدید با موفقیت اضافه شد');


    }


    /**
     * Display the specified resource.
     *
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */

    public function show(City $city)

    {
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */

    public function edit(City $city)

    {


        return view('panel.admin.cities.edit', compact('city'));


    }


    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, City $city)

    {
        $this->validate($request, [

            'unit_name' => 'required|max:50',
            'unit_order' => 'required|integer',
            'unit_status' => 'required|integer',

        ]);


        $dataToUpdate = [
            'unit_name' => $request->unit_name,
            'unit_order' => $request->unit_order,
            'unit_status' => $request->unit_status,
        ];


        $city->update($dataToUpdate);


        return redirect()->route('cities.index',['parent'=>$city->state_id])
            ->with('success', 'واحد با موفقیت ویرایش شد');

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)

    {
        City::find($id)->delete();

        return redirect()->route('cities.index')
            ->with('success', 'واحد با موفقیت حذف شد');

    }



    public function getCities(Request $request){

        $this->validate($request, [
            'r_id' => 'required|exists:cities,id',
        ]);

        $permittedStateRegionCities = Helpers::getPermittedCities(auth()->user());
        $permittedStateIDs = is_array($permittedStateRegionCities['permitted_states'])?$permittedStateRegionCities['permitted_states']:[];
        $permittedCityIDs = is_array($permittedStateRegionCities['permitted_cities'])?$permittedStateRegionCities['permitted_cities']:[];
        $permittedRegionIDs = is_array($permittedStateRegionCities['permitted_regions'])?$permittedStateRegionCities['permitted_regions']:[];
        $permittedAllCitiesIDs = array_merge($permittedStateIDs,$permittedCityIDs,$permittedRegionIDs);


        $cities=City::select('id','state_id','unit_name','latitude','longitude')
            ->where('state_id','=',$request->r_id);

        if(auth()->user()->user_type!='ADMIN' && auth()->user()->user_type!='SALES_ACCOUNTANT' && auth()->user()->user_type!='CSO' && auth()->user()->user_type!='STOCK'){
            $cities = $cities->whereIn('id',$permittedAllCitiesIDs);
        }


        $cities = $cities->where('unit_status','=',1)
            ->orderBy('unit_order' , 'ASC')
            ->orderBy('created_at')->get();

        foreach ($cities as $key=>$ct){
            $cities[$key]->title=$ct->unit_name;
        }

        return response()->json($cities);
    }

}
