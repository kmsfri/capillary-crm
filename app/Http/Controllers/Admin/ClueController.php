<?php


namespace App\Http\Controllers\Admin;


use App\Models\Clue;

use App\Models\InstanceInvoice;

use App\Models\Customer;

use App\Models\Business;

use App\Models\Invoice;

use App\Models\Note;

use App\Models\ClueProcessReport;

use App\Models\Campaign;

use App\Models\Product;

use App\Models\CallTargetAlarm;

use App\Models\JobClass;

use App\Models\User;

use App\Models\ClueSteps;

use App\Models\MarketingList;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Helpers;

use Illuminate\Database\Eloquent\Builder;

use DB;

use App\Models\City;

use Excel;
use App\Exports\ClueExport;

class ClueController extends Controller

{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    function __construct()

    {

        $this->middleware('permission:clue-list|clue-create|clue-edit|clue-delete', ['only' => ['index', 'show']]);

        $this->middleware('permission:clue-create', ['only' => ['create', 'store']]);

        $this->middleware('permission:clue-edit', ['only' => ['edit', 'update']]);

        $this->middleware('permission:clue-delete', ['only' => ['destroy']]);
		
		$this->middleware('permission:show-clue-cartable', ['only' => ['showCartable','addNewNote']]);
		
		$this->middleware('permission:view-clues-report', ['only' => ['showCluesReport']]);
		
		$this->middleware('permission:merge-clues', ['only' => ['mergeToAnotherClue','mergeConfirmToAnotherClue']]);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request){
		
		
		$permittedConditions = Helpers::getBusinessesAccessPermittedConditions(auth()->user());
		
        $clues = Clue::select('*');
		if(auth()->user()->user_type=='ADMIN' || auth()->user()->user_type=='CSO'){
			//do nothing
			$clues = $clues->orWhere('id','>=',0);
		}else{
			
			foreach($permittedConditions as $key=>$condition){
				$toFireCondition1 = [];
				if(empty($condition['ignore_cities']) || !$condition['ignore_cities']){
					$toFireCondition1 = [
						'operand'=>'IN',
						'values' => $condition['city_ids']
					];
				}

				$toFireCondition2 = [];
				if(empty($condition['ignore_creator_id']) || !$condition['ignore_creator_id']){
					$toFireCondition2 = [
						'operand'=>'IN',
						'values' => $condition['creator_id']
					];
				}
				
				
				if(!empty($toFireCondition1)){
					$clues = $clues->whereIn('city_id', $toFireCondition1['values']);
				}
				
				if(!empty($toFireCondition2)){
					//$clues = $clues->whereIn('creator_user_id', $toFireCondition2['values']);
				}
				
			}
			
		}
 
 
		$permittedCluesIDs = $clues->get()->pluck('id')->toArray();
		
		
		unset($clues);
 	


		$clues = Clue::select('clues.*')->whereIn('id',$permittedCluesIDs);
 
        
		
		if(!empty($request->region_id)){

	
			$clues = $clues->where('city_id',$request->region_id);
        
        
        }else if(!empty($request->city_id)){
        
        	$toFilterCityIDs = [];
        	
        	$toFilterCityIDs[] = $request->city_id;
        
        	$filterSelectedCity = City::findOrFail($request->city_id);
        	
        	$filterSelectedCityRegions = $filterSelectedCity->subsets()->get();
        		
			foreach($filterSelectedCityRegions as $filterSelectedRegion){
			
				$toFilterCityIDs[] = $filterSelectedRegion->id;
			
			}
	
			$clues = $clues->whereIn('city_id',$toFilterCityIDs);
        
        
        }else if(!empty($request->state_id)){
        	$toFilterCityIDs = [];
        	
        	$toFilterCityIDs[] = $request->state_id;
        	
        	$filterSelectedState = City::findOrFail($request->state_id);
        	$filterSelectedStateCities = $filterSelectedState->subsets()->get();
        	foreach($filterSelectedStateCities as $filterSelectedCity){
        		$toFilterCityIDs[] = $filterSelectedCity->id;
        		
        		$filterSelectedCityRegions = $filterSelectedCity->subsets()->get();
        		
        		foreach($filterSelectedCityRegions as $filterSelectedRegion){
        		
        			$toFilterCityIDs[] = $filterSelectedRegion->id;
        		
        		}
        	
        	}
        	
        	
        	$clues = $clues->whereIn('city_id',$toFilterCityIDs);
        	
        	
        }
        
		
		
		
        
     
        
        
        if(!empty($request->clue_title)){
        
        	$searchKey = $request->clue_title;
        
			$clues = $clues->where(function($query) use($searchKey) {
				$query->where('clue_title','LIKE', '%'.$searchKey.'%');
			});
		
        
        }
		
		
		if(!empty($request->first_name)){
        
        	$searchKey = $request->first_name;
        
			$clues = $clues->where(function($query) use($searchKey) {
				$query->where('first_name','LIKE', '%'.$searchKey.'%');
			});
		
        
        }
		
		
		if(!empty($request->last_name)){
        
        	$searchKey = $request->last_name;
        
			$clues = $clues->where(function($query) use($searchKey) {
				$query->where('last_name','LIKE', '%'.$searchKey.'%');
			});
		
        
        }
		
		
		if(!empty($request->business_name)){
        
        	$searchKey = $request->business_name;
        
			$clues = $clues->where(function($query) use($searchKey) {
				$query->where('business_name','LIKE', '%'.$searchKey.'%');
			});
		
        
        }
		
		
		if(!empty($request->address)){
        
        	$searchKey = $request->address;
        
			$clues = $clues->where(function($query) use($searchKey) {
				$query->where('address','LIKE', '%'.$searchKey.'%');
			});
		
        
        }
		
		
		
		if(!empty($request->phone)){
        
        	$searchKey = (int)$request->phone;
        
			$clues = $clues->where(function($query) use($searchKey) {
				$query->where('phone','LIKE', '%'.$searchKey.'%')
						->orWhere('phone','LIKE', '%'.$searchKey.'%')
						->orWhere('phone2','LIKE', '%'.$searchKey.'%')
						->orWhere('phone3','LIKE', '%'.$searchKey.'%')
						->orWhere('mobile','LIKE', '%'.$searchKey.'%')
						->orWhere('mobile2','LIKE', '%'.$searchKey.'%')
						->orWhere('mobile3','LIKE', '%'.$searchKey.'%');
			});
		
        
        }
		
		
		
		
		if(!empty($request->creator_user_id)){
        
        	$clues = $clues->where('creator_user_id',$request->creator_user_id);
		
        
        }
		
		
		
		if(!empty($request->campaign_id)){
        
        	$clues = $clues->where('campaign_id',$request->campaign_id);
		
        
        }



		if(!empty($request->clue_closed)){
        
			$searchKey = ($request->clue_closed=="open")?0:1;
		
        	$clues = $clues->where('clue_closed',$searchKey);
		
        
        }
		
		
		
		if(!empty($request->clue_status)){
        
			$searchKey = $request->clue_status;
		
        	$clues = $clues->where('clue_status',$searchKey);
		
        
        }
		
		
		if(!empty($request->clue_product_id)){
        
    
			foreach($request->clue_product_id as $toSearchProduct){
				
				$clues = $clues->whereHas('products', function (Builder $query) use($toSearchProduct) {
					// Query the pivot table
					
					$query->where('product_id', $toSearchProduct);
					
					
				});
				
			}
        	

/*
join('clue_products', 'clues.id', '=', 'clue_products.clue_id')
							 ->whereIn('clue_products.',);
			
	*/	
        
        }
		
		
		if(!empty($request->export_excel) && $request->export_excel==1){
			
			
			
			$clues = $clues->orderBy('created_at','DESC')->get();
			
			$dataToExport = [];
			foreach($clues as $key=>$item){
				$dataToExport[] = [
					$item['clue_title'],
					$item['first_name'],
					$item['last_name'],
					$item['business_name'],
					!empty($item->city()->first())?$item->city()->first()->unit_name:'',
					$item['address'],
					$item['phone'],
					$item['phone2'],
					$item['phone3'],
					$item['mobile'],
					$item['mobile2'],
					$item['mobile3'],
					$item['customer_description'],
					$item->created_at,
					!empty($item->creator()->first())?$item->creator()->first()->name:''
				
				];
				
			}
			
			return Excel::download(new ClueExport($dataToExport), 'clues.xlsx');
		}
        
        
        

        $clues = $clues->orderBy('clues.created_at','DESC')->paginate(20);
        
        
		//begin get permitted cities
		$permittedStateRegionCities = Helpers::getPermittedCities(auth()->user());

        $states = City::select('*');
        if(is_array($permittedStateRegionCities['permitted_states'])){
            $states = $states->whereIn('id',$permittedStateRegionCities['permitted_states']);
        }else if($permittedStateRegionCities['permitted_states']!='%'){
            $states = $states->whereIn('id',[]);
        }
        $states = $states->where('unit_type', 0)->where('unit_status', 1)->orderBy('unit_order', 'ASC')->get();

        $cities = [];
		$regions = [];
        if(!empty($request->state_id)){
        	//TODO: just show permitted cities
            $cities = City::where('state_id',$request->state_id)->where('unit_status',1)->orderBy('unit_order','ASC')->get();
			if(!empty($request->city_id)){
                $regions = City::where('state_id',$request->city_id)->where('unit_status',1)->orderBy('unit_order','ASC')->get();
            }
        }
		
		//end get permitted cities

		
		
		
		
		
		
		$creatorUsers = [];
		if(auth()->user()->user_type=="ADMIN" || auth()->user()->user_type=="CSO" || auth()->user()->user_type=="SALES_ACCOUNTANT"){
			
			$creatorUsers = User::orderBy('name','ASC')->get();
			
		}else if(auth()->user()->user_type=="SALES_MANAGER"){
			
			$creatorUsers = User::where('id',auth()->user()->id)->orWhere('parent_user_id',auth()->user()->id)->orderBy('name','ASC')->get();
			
		}else{
			$creatorUsers = User::where('id',auth()->user()->id)->orderBy('name','ASC')->get();
		}
		
		
		
		
		
		$campaignList = Campaign::orderBy('id','ASC')->get();
		
		
		
		$clueClosedStatuses = [
		
			'open' => 'باز',
			'closed' => 'بسته',
		
		];
		
		
		
		
		
		$clueStatuses = [
		
			'clue' => 'ایجاد سرنخ',
			'validation' => 'اعتبارسنجی',
			'pre_invoice' => 'ثبت مشتری(صدور پیش فاکتور)',
			'invoice' => 'ثبت مشتری(صدور فاکتور)',
		
		];
		
		
		$productsList = Product::orderBy('has_priority','DESC')->orderBy('id','ASC')->get();
		
		
        return view('panel.admin.clues.index', compact('productsList','clues','states','cities','regions','creatorUsers','campaignList','clueClosedStatuses','clueStatuses'))
            ->with('i', (request()->input('page', 1) - 1) * 20);

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()

    {
		
		
		
		
		
		
		//begin get permitted cities
		
		
		$permittedStateRegionCities = Helpers::getPermittedCities(auth()->user());

        $states = City::select('*');
        if(is_array($permittedStateRegionCities['permitted_states'])){
            $states = $states->whereIn('id',$permittedStateRegionCities['permitted_states']);
        }else if($permittedStateRegionCities['permitted_states']!='%'){
            $states = $states->whereIn('id',[]);
        }
        $states = $states->where('unit_type', 0)->where('unit_status', 1)->orderBy('unit_order', 'ASC')->get();

        $cities = [];
        $regions = [];
        if(old('state_id')){
            $cities = City::where('state_id',old('state_id'))->where('unit_status',1)->orderBy('unit_order','ASC')->get();

            if(old('city_id')){
                $regions = City::where('state_id',old('city_id'))->where('unit_status',1)->orderBy('unit_order','ASC')->get();
            }
        }
		
		
		//end get permitted cities
		
		
		
		
		
		
		$campaigns = Campaign::all();
		
		
		
		
		
		
		

        return view('panel.admin.clues.create',compact('states','cities','regions','campaigns'));

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)

    {
		
		
		
		
		
		
		
		
		
		
		

        request()->validate([

            'clue_title' => 'required|max:191',
            'clue_closed' => 'required|integer',
			
			
			'first_name' => 'nullable|max:255',
            'last_name' => 'required_without:business_name|max:255',
            'business_name' => 'required_without:last_name|max:255',
            'state_id' => 'required|integer|exists:cities,id',
            'city_id' => 'required|integer|exists:cities,id',
            'region_id' => 'nullable|integer|exists:cities,id',
            'address' => 'nullable|max:255',
            'phone' => 'nullable|required_without_all:mobile,mobile2,mobile3,phone2,phone3|max:255|unique:clues,phone|unique:clues,phone2|unique:clues,phone3',
            'phone2' => 'nullable|required_without_all:mobile,mobile2,mobile3,phone,phone3|max:255|unique:clues,phone|unique:clues,phone2|unique:clues,phone3',
            'phone3' => 'nullable|required_without_all:mobile,mobile2,mobile3,phone,phone2|max:255|unique:clues,phone|unique:clues,phone2|unique:clues,phone3',
            'mobile' => 'nullable|required_without_all:phone,phone2,phone3,mobile2,mobile3|max:255|unique:clues,mobile|unique:clues,mobile2|unique:clues,mobile3',
            'mobile2' => 'nullable|required_without_all:phone,phone2,phone3,mobile,mobile3|max:255|unique:clues,mobile|unique:clues,mobile2|unique:clues,mobile3',
            'mobile3' => 'nullable|required_without_all:phone,phone2,phone3,mobile,mobile2|max:255|unique:clues,mobile|unique:clues,mobile2|unique:clues,mobile3',
			
			'campaign_id' => 'nullable|integer|exists:campaigns,id',

        ]);
		
		
		
		


		$data = [
		
			
		
			'clue_title' => $request->clue_title,
			'clue_closed' => $request->clue_closed,
			'creator_user_id' => auth()->user()->id,
			
			
			
			'first_name' => $request->first_name,
			'last_name' => $request->last_name,
			'business_name' => $request->business_name,
			'address' => $request->address,
			'phone' => $request->phone,
			'phone' => $request->phone,
			'phone3' => $request->phone3,
			'mobile' => $request->mobile,
			'mobile2' => $request->mobile2,
			'mobile3' => $request->mobile3,
			
			'campaign_id' => $request->campaign_id
			
			
		
		];






		if (!empty($request->region_id)) {
            $data['city_id'] = $request->region_id;
        } else if (!empty($request->city_id)) {
            $data['city_id'] = $request->city_id;
        } else if (!empty($request->state_id)) {
            $data['city_id'] = $request->state_id;
        }




        Clue::create($data);


        return redirect()->route('clues.index')
            ->with('success', 'سرنخ جدید با موفقیت ایجاد شد.');

    }


    /**
     * Display the specified resource.
     *
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */

    public function show(Product $product)

    {

        //return view('panel.admin.products.show', compact('product'));

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */

    public function edit(Clue $clue)

    {
		
		$clueID = $clue->id;
		unset($clue);
		
		
		$clue = CLUE::select('*');
        if(auth()->user()->user_type=='SALES_SPECIALIST'){
            //$clue = $clue->where('creator_user_id',auth()->user()->id);
        }
        $clue = $clue->where('id',$clueID)->first();
		
		
		
		
		//begin get permitted cities
		
		
		$permittedStateRegionCities = Helpers::getPermittedCities(auth()->user());

        $states = City::select('*');
        if(is_array($permittedStateRegionCities['permitted_states'])){
            $states = $states->whereIn('id',$permittedStateRegionCities['permitted_states']);
        }else if($permittedStateRegionCities['permitted_states']!='%'){
            $states = $states->whereIn('id',[]);
        }
		
        $states = $states->where('unit_type', 0)->where('unit_status', 1)->orderBy('unit_order', 'ASC')->get();
        $cities = [];
        $regions = [];

        $storedCityID = $clue->city_id;
        //detect state_id, city_id, region_id
		
		
        $storedCity = City::find($clue->city_id);
		if(!empty($clue->city_id)){
			if ($storedCity->unit_type == 0) { //is state(means that the city and region didnt chosen)
				$clue->state_id = $storedCityID;
				$clue->city_id = [];
				$clue->region_id = [];

				$cities = City::select('*');
				if(is_array($permittedStateRegionCities['permitted_cities'])){
					$cities = $cities->whereIn('id',$permittedStateRegionCities['permitted_cities']);
				}else if($permittedStateRegionCities['permitted_cities']!='%'){
					$cities = $cities->whereIn('id',[]);
				}
				$cities = $cities->where('state_id', $clue->state_id)->where('unit_status', 1)->orderBy('unit_order', 'ASC')->get();

			} else if ($storedCity->unit_type == 1) {//is city(means that the region didnt chosen)
				
				$clue->state_id = $storedCity->state_id;
				$clue->city_id = $storedCityID;
				$clue->region_id = [];


				$cities = City::select('*');
				if(is_array($permittedStateRegionCities['permitted_cities'])){
					$cities = $cities->whereIn('id',$permittedStateRegionCities['permitted_cities']);
				}else if($permittedStateRegionCities['permitted_cities']!='%'){
					$cities = $cities->whereIn('id',[]);
				}
				
				$cities = $cities->where('state_id', $clue->state_id)->where('unit_status', 1)->orderBy('unit_order', 'ASC')->get();


				$regions = City::select('*');
				if(is_array($permittedStateRegionCities['permitted_regions'])){
					$regions = $regions->whereIn('id',$permittedStateRegionCities['permitted_regions']);
				}else if($permittedStateRegionCities['permitted_regions']!='%'){
					$regions = $regions->whereIn('id',[]);
				}
				
				$regions = $regions->where('state_id', $clue->city_id)->where('unit_status', 1)->orderBy('unit_order', 'ASC')->get();


			} else if ($storedCity->unit_type == 2) {//is region(means that all of state and city and region selected)
				
			
				$clue->state_id = City::find($storedCity->state_id)->parent()->first()->id;
				$clue->city_id = $storedCity->state_id;
				$clue->region_id = $storedCityID;

				$cities = City::select('*');
				if(is_array($permittedStateRegionCities['permitted_cities'])){
					$cities = $cities->whereIn('id',$permittedStateRegionCities['permitted_cities']);
				}else if($permittedStateRegionCities['permitted_cities']!='%'){
					$cities = $cities->whereIn('id',[]);
				}
				$cities = $cities->where('state_id', $clue->state_id)->where('unit_status', 1)->orderBy('unit_order', 'ASC')->get();


				$regions = City::select('*');
				if(is_array($permittedStateRegionCities['permitted_regions'])){
					$regions = $regions->whereIn('id',$permittedStateRegionCities['permitted_regions']);
				}else if($permittedStateRegionCities['permitted_regions']!='%'){
					$regions = $regions->whereIn('id',[]);
				}

				$regions = $regions->where('state_id', $clue->city_id)->where('unit_status', 1)->orderBy('unit_order', 'ASC')->get();

			}
			
		}	
		//end get permitted cities
		
		
		
		$campaigns = Campaign::all();
		
		

        return view('panel.admin.clues.edit', compact('clue','states','cities','regions','campaigns'));

    }


    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, Clue $clue)

    {

        request()->validate([

			'clue_title' => 'required|max:191',
            
            'clue_closed' => 'required|integer',

			
			'first_name' => 'nullable|max:255',
            'last_name' => 'required_without:business_name|max:255',
            'business_name' => 'required_without:last_name|max:255',
            'state' => 'nullable|max:191',
            'city' => 'nullable|max:191',
            'business_name' => 'required_without:last_name|max:255',
            'state_id' => 'nullable|integer|exists:cities,id',
            'city_id' => 'nullable|integer|exists:cities,id',
            'region_id' => 'nullable|integer|exists:cities,id',
            'address' => 'nullable|max:255',
            'phone' => 'nullable|required_without_all:mobile,mobile2,mobile3,phone2,phone3|max:255|unique:clues,phone,'.$clue->id.'|unique:clues,phone2,'.$clue->id.'|unique:clues,phone3,'.$clue->id,
            'phone2' => 'nullable|required_without_all:mobile,mobile2,mobile3,phone,phone3|max:255|unique:clues,phone,'.$clue->id.'|unique:clues,phone2,'.$clue->id.'|unique:clues,phone3,'.$clue->id,
            'phone3' => 'nullable|required_without_all:mobile,mobile2,mobile3,phone,phone2|max:255|unique:clues,phone,'.$clue->id.'|unique:clues,phone2,'.$clue->id.'|unique:clues,phone3,'.$clue->id,
            'mobile' => 'nullable|required_without_all:phone,phone2,phone3,mobile2,mobile3|max:255|unique:clues,mobile,'.$clue->id.'|unique:clues,mobile2,'.$clue->id.'|unique:clues,mobile3,'.$clue->id,
            'mobile2' => 'nullable|required_without_all:phone,phone2,phone3,mobile,mobile3|max:255|unique:clues,mobile,'.$clue->id.'|unique:clues,mobile2,'.$clue->id.'|unique:clues,mobile3,'.$clue->id,
            'mobile3' => 'nullable|required_without_all:phone,phone2,phone3,mobile,mobile2|max:255|unique:clues,mobile,'.$clue->id.'|unique:clues,mobile2,'.$clue->id.'|unique:clues,mobile3,'.$clue->id,
			
			
			'campaign_id' => 'nullable|integer|exists:campaigns,id',

        ]);
		

		
		
		$data = [
		
			
		
			'clue_title' => $request->clue_title,
			'clue_closed' => $request->clue_closed,
			'last_updated_user_id' => auth()->user()->id,
			
			
			
			'first_name' => $request->first_name,
			'last_name' => $request->last_name,
			'business_name' => $request->business_name,
			'state' => $request->state,
			'city' => $request->city,
			'address' => $request->address,
			'phone' => $request->phone,
			'phone' => $request->phone,
			'phone3' => $request->phone3,
			'mobile' => $request->mobile,
			'mobile2' => $request->mobile2,
			'mobile3' => $request->mobile3,
			
			
			'campaign_id' => $request->campaign_id
			
			
		
		];






		if (!empty($request->region_id)) {
            $data['city_id'] = $request->region_id;
        } else if (!empty($request->city_id)) {
            $data['city_id'] = $request->city_id;
        } else if (!empty($request->state_id)) {
            $data['city_id'] = $request->state_id;
        }
		
		
		
		
		


        $clue->update($data);


        return redirect()->route('clues.index')
            ->with('success', 'سرنخ مورد نظر با موفقیت ویرایش شد.');

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */

    public function destroy(Clue $clue)

    {

        $clue->delete();


        return redirect()->route('clues.index')
            ->with('success', 'سرنخ مورد نظر با موفقیت حذف شد.');

    }
	
	
	
	
	
	
	
	
	
	
	
	
	public function showCartable(Request $request){
    
    	$clueID = $request->clue_id;
    	
    	//TODO: just access to permitted businesses
		
    	$clue = Clue::findOrFail($clueID);
    	
		
		
		$cluePhoneNumbers = [];
		
		
		
		if(!empty($clue->phone)){
			$cluePhoneNumbers[] = (int)Helpers::convertFaToEnNumbersV2($clue->phone);
		}
		
		if(!empty($clue->phone2)){
			$cluePhoneNumbers[] = (int)Helpers::convertFaToEnNumbersV2($clue->phone2);
		}
		
		if(!empty($clue->phone3)){
			$cluePhoneNumbers[] = (int)Helpers::convertFaToEnNumbersV2($clue->phone3);
		}
		
		if(!empty($clue->mobile)){
			$cluePhoneNumbers[] = (int)Helpers::convertFaToEnNumbersV2($clue->mobile);
		}
		
		if(!empty($clue->mobile2)){
			$cluePhoneNumbers[] = (int)Helpers::convertFaToEnNumbersV2($clue->mobile2);
		}
		
		if(!empty($clue->mobile3)){
			$cluePhoneNumbers[] = (int)Helpers::convertFaToEnNumbersV2($clue->mobile3);
		}
		
		if(!empty($clue->sepidar_phone)){
			$cluePhoneNumbers[] = (int)Helpers::convertFaToEnNumbersV2($clue->sepidar_phone);
		}
		
		
		
		$customer = Null;
		if(!empty($clue->customer_id)){
			
			
			$customer = Customer::find($clue->customer_id);
			
			
			
    	
			if(!empty($customer->phone)){
				$cluePhoneNumbers[] = (int)Helpers::convertFaToEnNumbersV2($customer->phone);
			}
			
			if(!empty($customer->phone2)){
				$cluePhoneNumbers[] = (int)Helpers::convertFaToEnNumbersV2($customer->phone2);
			}
			
			if(!empty($customer->phone3)){
				$cluePhoneNumbers[] = (int)Helpers::convertFaToEnNumbersV2($customer->phone3);
			}
			
			if(!empty($customer->mobile)){
				$cluePhoneNumbers[] = (int)Helpers::convertFaToEnNumbersV2($customer->mobile);
			}
			
			if(!empty($customer->mobile2)){
				$cluePhoneNumbers[] = (int)Helpers::convertFaToEnNumbersV2($customer->mobile2);
			}
			
			if(!empty($customer->mobile3)){
				$cluePhoneNumbers[] = (int)Helpers::convertFaToEnNumbersV2($customer->mobile3);
			}
			
			if(!empty($customer->sepidar_phone)){
				$cluePhoneNumbers[] = (int)Helpers::convertFaToEnNumbersV2($customer->sepidar_phone);
			}
			
			
			
			
		}
		
		
		$business = Null;
		if(!empty($clue->business_id)){
			
			
			$business = Business::find($clue->business_id);
			
			
			
			
			//begin get business calls
			
			$businessMobiles = json_decode($business->mobiles,true);
			if(!empty($businessMobiles) && count($businessMobiles)>0){
				foreach($businessMobiles as $businessMobile){
				
					if(!empty($businessMobile)){
						$cluePhoneNumbers[] = (int)Helpers::convertFaToEnNumbersV2($businessMobile);
					}
				
				}
			}
			
			if(!empty($business->phone_number)){
				$cluePhoneNumbers[] =  (int)Helpers::convertFaToEnNumbersV2($business->phone_number);
			}
			
			
			
			//end get business calls
			
			
			
		}
		
		
		$clueCalls = [];
		if(!empty($cluePhoneNumbers) && count($cluePhoneNumbers)>0){
			$getCallsQuery = DB::connection('mysql_voip')->table('cdr');
				
			foreach($cluePhoneNumbers as $businessPhone){
				$getCallsQuery = $getCallsQuery->orWhere(function($query) use($businessPhone){
							$query->where('src','LIKE', '%'.$businessPhone.'%')->orWhere('dst','LIKE','%'.$businessPhone.'%');
						});
			}
		
			$getCallsQuery=$getCallsQuery->orderBy('calldate','DESC');
			
			$clueCalls=$getCallsQuery->paginate(20);
		}
		
    	
    	
    	
    	//TODO: maybe will be limited to specialists
		$clueAlarms = CallTargetAlarm::where('clue_id',$clueID)->get();
	
	
	

		//TODO: maybe will be limiteded
		$clueNotes = Note::where('clue_id',$clueID)->orderBy('created_at','DESC')->paginate(20,['*'],'user_notes_page');
		
		
		//TODO: maybe will be limiteded
		$clueProcessReports = clueProcessReport::where('clue_id',$clueID)->orderBy('created_at','DESC')->paginate(20,['*'],'clue_process_report_page');
		
		
		
		
		
		
		
		
		
		
		$permittedConditions = Helpers::getBusinessesAccessPermittedConditions(auth()->user());
		
		
		//begin get all businesses list
		
		$businesses = Business::select('*')->where('id','<',0);

		$customers = Customer::select('*')->where('id','<',0);

        foreach($permittedConditions as $key=>$condition){
            $toFireCondition1 = [];
            if(!empty($condition['ignore_cities']) && $condition['ignore_cities']){
                $toFireCondition1 = [
                    'operand'=>'LIKE',
                    'values' => '%'
                ];
            }else{
                $toFireCondition1 = [
                    'operand'=>'IN',
                    'values' => $condition['city_ids']
                ];
            }

            $toFireCondition2 = [];
            if(!empty($condition['ignore_creator_id']) && $condition['ignore_creator_id']){
                $toFireCondition2 = [
                    'operand'=>'LIKE',
                    'values' => '%'
                ];
            }else{
                $toFireCondition2 = [
                    'operand'=>'=',
                    'values' => $condition['creator_id']
                ];
            }
            $businesses = $businesses->orWhere(function($query) use($toFireCondition1,$toFireCondition2) {
                if($toFireCondition1['operand']=='IN'){
                    $query->whereIn('city_id', $toFireCondition1['values'])->where('creator_id',$toFireCondition2['operand'],$toFireCondition2['values']);
                }else{
                    $query->where('city_id',$toFireCondition1['operand'], $toFireCondition1['values'])->where('creator_id',$toFireCondition2['operand'],$toFireCondition2['values']);
                }
            });
			
			
			
			if($toFireCondition2['operand']=="LIKE" && $toFireCondition2['values']=="%"){
			    //do nothing
			    $customers = $customers->orWhere(function($query) use($toFireCondition2) {
					$query->where('id','>=',0);
				});
            }else{
				$customers = $customers->orWhere(function($query) use($toFireCondition2) {
					$query->where('creator_id',$toFireCondition2['operand'],$toFireCondition2['values']);
				});
            }
			
			
			
			
			
			
			
			
			
			
        }
        
 
 
 
		$permittedBusinessIDs = $businesses->get()->pluck('id')->toArray();
 	
		unset($businesses);
 	
 	
        $businesses = Business::select('*')->whereIn('id',$permittedBusinessIDs);
 
        $businesses = $businesses->orderBy('created_at','DESC')->get();
		
		
		
		
		
		
		$permittedCustomerIDs = $customers->get()->pluck('id')->toArray();
 	
	
	
		unset($customers);
 	
        $customers = Customer::select('*')->whereIn('id',$permittedCustomerIDs);
		
		$customers = $customers->orderBy('created_at','DESC')->get();
		
		
		
		//end get all businesses list
		
		
		
		
		
		
		
		
		
		
		//begin get all pre-invoices
		//TODO: maybe wrong
		$preInvoices = Invoice::whereIn('business_id',$permittedBusinessIDs)
			->where('invoice_type','official')
			->where('approved_by_accountant',0)
			->orderBy('created_at','DESC')->get();
			
			
		//end get all pre-invoices
		
		
		//begin get all invoices
		//TODO: maybe wrong
		$invoices = Invoice::whereIn('business_id',$permittedBusinessIDs)
			->where('invoice_type','official')
			->where('approved_by_accountant',1)
			->orderBy('created_at','DESC')->get();
			
			
		//end get all invoices
		
		
		
		
		
		
		
		
		
		
		
		//begin marketing list
		
		
		$jobClasses = JobClass::orderBy('job_class_order','ASC')->get();
		
		
		$legalStatuses = [
			'real' => 'حقیقی',
			'legal' => 'حقوقی'
		];
		
		$activityLevels = [
			'bonakdar' => 'بنکدار',
			'khordeh_foroosh' => 'خرده فروش',
			'masraf_konandeh' => 'مصرف کننده'
		];
		
		$clueMarketingList = MarketingList::where('clue_id',$clue->id)->get();
		
		//end marketing list
		
		
		
		
		
		//begin get instances
		$instances['customer'] = InstanceInvoice::where('clue_id',$clue->id)->where('clue_level','customer')->get();
		$instances['validation'] = InstanceInvoice::where('clue_id',$clue->id)->where('clue_level','validation')->get();
		$instances['sale'] = InstanceInvoice::where('clue_id',$clue->id)->where('clue_level','sale')->get();
		//end get instances
		
		
		
    	
		$productsList = Product::orderBy('has_priority','DESC')->orderBy('id','ASC')->get();
		
		
		$clueProducts = $clue->products()->get()->pluck('id')->toArray();
		
    	
    	return view('panel.admin.clues.cartable', compact('productsList','clueProducts','clueNotes','clueProcessReports','clueAlarms','clueCalls','customers','business','customer','clue','businesses','invoices','preInvoices','jobClasses','legalStatuses','activityLevels','clueMarketingList','instances'));
    	
    	
    
    }
    
    
    
    
    public function addNewNote(Request $request)
    {

		//TODO: validate to access own customer
        $this->validate($request, [

            'note_text' => 'required|max:1500',
            'clue_id' => 'required|exists:clues,id'

        ]);



        $clue = Clue::findOrFail($request->clue_id);

		$inputs = [
			'creator_id' => auth()->user()->id,
			'clue_id' => $clue->id,
			'note_text' => $request->note_text,
		];

        $note = Note::create($inputs);



        return redirect()->back()
            ->with('success', 'یادداشت با موفقیت ایجاد شد');

    }
	
	
	
	public function addNewReportProcess(Request $request)
    {

		//TODO: validate to access own customer
        $this->validate($request, [

            'process_text' => 'required|max:1500',
            'clue_id' => 'required|exists:clues,id'

        ]);



        $clue = Clue::findOrFail($request->clue_id);

		$inputs = [
			'creator_id' => auth()->user()->id,
			'clue_id' => $clue->id,
			'process_text' => $request->process_text,
		];

        $clueProcessReports = ClueProcessReport::create($inputs);



        return redirect()->back()
            ->with('success', 'توضیحات بازدید سرنخ با موفقیت ثبت شد');

    }
	
	
	
	
	public function updateClueProcessReport(Request $request){
		
		
		request()->validate([
			'clue_process_report_id' => 'required|integer|exists:clue_process_reports,id',
            'cartable_row_process_report_text' => 'required|max:1500'
        ]);
		
		$clueProcessReport = clueProcessReport::where('id',$request->clue_process_report_id)->where('creator_id',auth()->user()->id)->firstOrFail();
		
		
		$clueProcessReport->process_text = $request->cartable_row_process_report_text;
		$clueProcessReport->save();
		
		
		return redirect()->back()
            ->with('success', 'متن گزارش بازدید سرنخ با موفقیت ویرایش شد');
		
		
	}
	
	
	
	
	public function updateClueCustomerStatus(Request $request){
		
		
		//TODO: validate to access to clue edit
		//TODO: validate access to the clue
		//TODO: validate step edit
		
        request()->validate([

            
            'clue_id' => 'required|integer|exists:clues,id',
            'customer_id' => 'required|integer|exists:customers,id',


        ]);
		
		
		$clue = CLUE::select('*');
        if(auth()->user()->user_type=='SALES_SPECIALIST'){
            $clue = $clue->where('creator_user_id',auth()->user()->id);
        }
        $clue = $clue->where('id',$request->clue_id)->firstOrFail();
		
		
		$data = [
			'customer_id' => $request->customer_id,
			'clue_status' => 'customer',
		];


        $clue->update($data);
		
		
		
		$updateDescription = "شناسه مشتری، توسط کاربر با شناسه ".auth()->user()->id." به ".$request->customer_id." تغییر پیدا کرد.";
		$this->updateClueSteps($clue->id, auth()->user()->id, 'customer',$updateDescription,$request->customer_id);
		
		


        return redirect()->route('clues.showCartable',['clue_id'=>$clue->id])
            ->with('success', 'وضعیت سرنخ با موفقیت ویرایش شد');
		
		
		
	}
	
	
	private function updateClueSteps($clueID, $updaterUser, $stepKey, $updateDescription, $updatedValue){
		
		
		$clueStepsInput['clue_id'] = $clueID;
		$clueStepsInput['step_key'] = $stepKey;
		$clueStepsInput['updated_by'] = $updaterUser;
		$clueStepsInput['update_description'] = $updateDescription;
		$clueStepsInput['updated_value'] = $updatedValue;

        $clueStep = ClueSteps::create($clueStepsInput);
		
		return true;
		
	}
	
	
	
	public function updateClueBusinessStatus(Request $request){
		
		
		//TODO: validate to access to clue edit
		//TODO: validate access to the clue
		//TODO: validate step edit
		
        request()->validate([

            
            'clue_id' => 'required|integer|exists:clues,id',
            'business_id' => 'required|integer|exists:businesses,id',


        ]);
		
		
		$clue = CLUE::select('*');
        if(auth()->user()->user_type=='SALES_SPECIALIST'){
            $clue = $clue->where('creator_user_id',auth()->user()->id);
        }
        $clue = $clue->where('id',$request->clue_id)->firstOrFail();
		
		
		$data = [
			'business_id' => $request->business_id,
			'clue_status' => 'validation',
		];


        $clue->update($data);
		
		
		$updateDescription = "شناسه کسب و کار(اعتبارسنجی)، توسط کاربر با شناسه ".auth()->user()->id." به ".$request->business_id." تغییر پیدا کرد.";
		$this->updateClueSteps($clue->id, auth()->user()->id, 'validation',$updateDescription,$request->business_id);


        return redirect()->route('clues.showCartable',['clue_id'=>$clue->id])
            ->with('success', 'وضعیت سرنخ با موفقیت ویرایش شد');
		
		
		
	}
	
	
	
	
	public function updateCluePreInvoiceStatus(Request $request){
		
		
		//TODO: validate to access to clue edit
		//TODO: validate access to the clue
		//TODO: validate step edit
		
        request()->validate([

            
            'clue_id' => 'required|integer|exists:clues,id',
            'pre_invoice_id' => 'required|integer|exists:invoices,id',


        ]);
		
		
		$clue = CLUE::select('*');
        if(auth()->user()->user_type=='SALES_SPECIALIST'){
            $clue = $clue->where('creator_user_id',auth()->user()->id);
        }
        $clue = $clue->where('id',$request->clue_id)->firstOrFail();
		
		
		$data = [
			'pre_invoice_id' => $request->pre_invoice_id,
			'clue_status' => 'sale',
		];
		
        $clue->update($data);
		
		
		$updateDescription = "شناسه پیش فاکتور، توسط کاربر با شناسه ".auth()->user()->id." به ".$request->pre_invoice_id." تغییر پیدا کرد.";
		$this->updateClueSteps($clue->id, auth()->user()->id, 'sale',$updateDescription,$request->pre_invoice_id);


        return redirect()->route('clues.showCartable',['clue_id'=>$clue->id])
            ->with('success', 'وضعیت سرنخ با موفقیت ویرایش شد');
		
		
		
	}
	
	
	
	
	
	public function updateClueInvoiceStatus(Request $request){
		
		
		//TODO: validate to access to clue edit
		//TODO: validate access to the clue
		//TODO: validate step edit
		
        request()->validate([

            
            'clue_id' => 'required|integer|exists:clues,id',
            'invoice_id' => 'required|integer|exists:invoices,id',


        ]);
		
		
		$clue = CLUE::select('*');
        if(auth()->user()->user_type=='SALES_SPECIALIST'){
            $clue = $clue->where('creator_user_id',auth()->user()->id);
        }
        $clue = $clue->where('id',$request->clue_id)->firstOrFail();
		
		
		$data = [
			'invoice_id' => $request->invoice_id,
			'clue_status' => 'return_from_sale',
		];


        $clue->update($data);
		
		
		$updateDescription = "شناسه فاکتور، توسط کاربر با شناسه ".auth()->user()->id." به ".$request->invoice_id." تغییر پیدا کرد.";
		$this->updateClueSteps($clue->id, auth()->user()->id, 'return_from_sale',$updateDescription,$request->invoice_id);


        return redirect()->route('clues.showCartable',['clue_id'=>$clue->id])
            ->with('success', 'وضعیت سرنخ با موفقیت ویرایش شد');
		
		
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public function showCluesReport(Request $request){
    
	
	
	
		$permittedConditions = Helpers::getBusinessesAccessPermittedConditions(auth()->user());

        //$businesses = Business::select(DB::raw('count(id) as `data`'), DB::raw('YEAR(created_at) year, MONTH(created_at) month'))->where('id','<',0);
        $clues = Clue::select('*')->where('id','<',0);
		
		$chartOpenClosedClues = Clue::selectRaw('clue_closed, count(id) as clue_count')->where('id','<',0);
		
		
		$chartStatusGroupedClues = Clue::selectRaw('clue_status, count(id) as clue_count')->where('id','<',0);
		
		$allClues = Clue::selectRaw('id')->where('id','<',0);

        foreach($permittedConditions as $key=>$condition){
            $toFireCondition1 = [];
            if(!empty($condition['ignore_cities']) && $condition['ignore_cities']){
                $toFireCondition1 = [
                    'operand'=>'LIKE',
                    'values' => '%'
                ];
            }else{
                $toFireCondition1 = [
                    'operand'=>'IN',
                    'values' => $condition['city_ids']
                ];
            }

            $toFireCondition2 = [];
            if(!empty($condition['ignore_creator_id']) && $condition['ignore_creator_id']){
                $toFireCondition2 = [
                    'operand'=>'LIKE',
                    'values' => '%'
                ];
            }else{
                $toFireCondition2 = [
                    'operand'=>'=',
                    'values' => $condition['creator_id']
                ];
            }
            $clues = $clues->orWhere(function($query) use($toFireCondition1,$toFireCondition2) {
                if($toFireCondition1['operand']=='IN'){
                    $query->whereIn('city_id', $toFireCondition1['values'])->where('creator_user_id',$toFireCondition2['operand'],$toFireCondition2['values']);
                }else{
                    $query->where('city_id',$toFireCondition1['operand'], $toFireCondition1['values'])->where('creator_user_id',$toFireCondition2['operand'],$toFireCondition2['values']);
                }
            });
			
			$chartOpenClosedClues = $chartOpenClosedClues->orWhere(function($query) use($toFireCondition1,$toFireCondition2) {
                if($toFireCondition1['operand']=='IN'){
                    $query->whereIn('city_id', $toFireCondition1['values'])->where('creator_user_id',$toFireCondition2['operand'],$toFireCondition2['values']);
                }else{
                    $query->where('city_id',$toFireCondition1['operand'], $toFireCondition1['values'])->where('creator_user_id',$toFireCondition2['operand'],$toFireCondition2['values']);
                }
            });
			
			
			$chartStatusGroupedClues = $chartStatusGroupedClues->orWhere(function($query) use($toFireCondition1,$toFireCondition2) {
                if($toFireCondition1['operand']=='IN'){
                    $query->whereIn('city_id', $toFireCondition1['values'])->where('creator_user_id',$toFireCondition2['operand'],$toFireCondition2['values']);
                }else{
                    $query->where('city_id',$toFireCondition1['operand'], $toFireCondition1['values'])->where('creator_user_id',$toFireCondition2['operand'],$toFireCondition2['values']);
                }
            });
			
			$allClues = $allClues->orWhere(function($query) use($toFireCondition1,$toFireCondition2) {
                if($toFireCondition1['operand']=='IN'){
                    $query->whereIn('city_id', $toFireCondition1['values'])->where('creator_user_id',$toFireCondition2['operand'],$toFireCondition2['values']);
                }else{
                    $query->where('city_id',$toFireCondition1['operand'], $toFireCondition1['values'])->where('creator_user_id',$toFireCondition2['operand'],$toFireCondition2['values']);
                }
            });
			
			
			
			
			
			
        }
        
 
        
        
        
        //filters:
        if(!empty($request->city_id)){
        
        	$toFilterCityIDs = [];
        	
        	$toFilterCityIDs[] = $request->city_id;
        
        	$filterSelectedCity = City::findOrFail($request->city_id);
        	
        	$filterSelectedCityRegions = $filterSelectedCity->subsets()->get();
					
			foreach($filterSelectedCityRegions as $filterSelectedRegion){
			
				$toFilterCityIDs[] = $filterSelectedRegion->id;
			
			}
		
			$clues = $clues->whereIn('city_id',$toFilterCityIDs);
			
			$chartOpenClosedClues = $chartOpenClosedClues->whereIn('city_id',$toFilterCityIDs);
			
			$chartStatusGroupedClues = $chartStatusGroupedClues->whereIn('city_id',$toFilterCityIDs);
			
			$allClues = $allClues->whereIn('city_id',$toFilterCityIDs);
			
			
			
			
			
        
        
        }else if(!empty($request->state_id)){
        	$toFilterCityIDs = [];
        	
        	$toFilterCityIDs[] = $request->state_id;
        	
        	$filterSelectedState = City::findOrFail($request->state_id);
        	$filterSelectedStateCities = $filterSelectedState->subsets()->get();
        	foreach($filterSelectedStateCities as $filterSelectedCity){
        		$toFilterCityIDs[] = $filterSelectedCity->id;
        		
        		$filterSelectedCityRegions = $filterSelectedCity->subsets()->get();
        		
        		foreach($filterSelectedCityRegions as $filterSelectedRegion){
        		
        			$toFilterCityIDs[] = $filterSelectedRegion->id;
        		
        		}
        	
        	}
        	
        	
        	$clues = $clues->whereIn('city_id',$toFilterCityIDs);
			
			$chartOpenClosedClues = $chartOpenClosedClues->whereIn('city_id',$toFilterCityIDs);
			
			$chartStatusGroupedClues = $chartStatusGroupedClues->whereIn('city_id',$toFilterCityIDs);
			
			
			$allClues = $allClues->whereIn('city_id',$toFilterCityIDs);
			
			
        	
        	
        }
        
        
		

		if(!empty($request->from_date)){
        
        	$fromDate = $request->from_date;
        	$fromDateGregorian=\Morilog\Jalali\Jalalian::fromFormat('Y/m/d', $fromDate)->toCarbon()->toDateTimeString();
        	
        	$clues = $clues->whereDate('created_at' , '>=',$fromDateGregorian);
			
			$chartOpenClosedClues = $chartOpenClosedClues->whereDate('created_at' , '>=',$fromDateGregorian);
			
			$chartStatusGroupedClues = $chartStatusGroupedClues->whereDate('created_at' , '>=',$fromDateGregorian);
			
			$allClues = $allClues->whereDate('created_at' , '>=',$fromDateGregorian);
			
			
        	
        	
        	$fromDateJalali = $request->from_date;
        
        }else{
        	$minDate = Clue::select(DB::raw('min(created_at) AS min_created_at'))->first();
        	$fromDateGregorian = $minDate->min_created_at;
        	$fromDateJalali = \Morilog\Jalali\Jalalian::forge($fromDateGregorian)->format('%Y/%m/%d');
        }
        
        if(!empty($request->to_date)){
        
        	$toDate = $request->to_date;
        	$toDateGregorian=\Morilog\Jalali\Jalalian::fromFormat('Y/m/d', $toDate)->toCarbon()->toDateTimeString();
        
        	$clues = $clues->whereDate('created_at' , '<=',$toDateGregorian);
			
			$chartOpenClosedClues = $chartOpenClosedClues->whereDate('created_at' , '<=',$toDateGregorian);
			
			$chartStatusGroupedClues = $chartStatusGroupedClues->whereDate('created_at' , '<=',$toDateGregorian);
			
			
			$allClues = $allClues->whereDate('created_at' , '<=',$toDateGregorian);
			
			
			
			
        	
        	$toDateJalali = $request->to_date;
        
        }else{
        
        	$maxDate =  Clue::select(DB::raw('max(created_at) AS max_created_at'))->first();
        	$toDateGregorian = $maxDate->max_created_at;
        	$toDateJalali = \Morilog\Jalali\Jalalian::forge($toDateGregorian)->format('%Y/%m/%d');
        
        }
        
        
        
        //BEGIN: calculate date periods
        $datePeriods = [];
        if($request->report_period==1){//Daily
        
			
			$explodedDate = explode('/',$toDateJalali);
			$toCheckDate = $explodedDate[0].$explodedDate[1].$explodedDate[2];
			$fromHoldYear = NULL;
			$fromHoldMonth = NULL;
			$fromHoldDay = NULL;
			do{
				
				if(empty($fromHoldYear)){
					$explodedDate = explode('/',$fromDateJalali);
					$fromHoldYear = $explodedDate[0];
					$fromHoldMonth = $explodedDate[1]; 
					$fromHoldDay = $explodedDate[2]; 
				}else{
					if($fromHoldDay<=30){
						$fromHoldDay++;
					}else if($fromHoldMonth<=11){
						$fromHoldDay=1;
						$fromHoldMonth++;
					}else{
						$fromHoldDay=1;
						$fromHoldMonth = 1;
						$fromHoldYear++;
					}
				}
				
				
				if($fromHoldMonth<10){
					$fromHoldMonth = "0".((int)$fromHoldMonth);
				}
				
				if($fromHoldDay<10){
					$fromHoldDay = "0".((int)$fromHoldDay);
				}
				
				$datePeriods[] = $fromHoldYear."/".$fromHoldMonth."/".$fromHoldDay;
				
				
			
			
			}while($toCheckDate>($fromHoldYear.$fromHoldMonth.$fromHoldDay));
        
        
		
		}else{//Monthly
	
			$explodedDate = explode('/',$toDateJalali);
			$toCheckDate = $explodedDate[0].$explodedDate[1];
			
			$fromHoldYear = NULL;
			$fromHoldMonth = NULL;
			do{
				
				if(empty($fromHoldYear)){
					$explodedDate = explode('/',$fromDateJalali);
					$fromHoldYear = $explodedDate[0];
					$fromHoldMonth = $explodedDate[1]; 
				}else{
					if($fromHoldMonth<=11){
						$fromHoldMonth++;
					}else{
						$fromHoldMonth = 1;
						$fromHoldYear++;
					}
				}
				
				if($fromHoldMonth<10){
					$fromHoldMonth = "0".((int)$fromHoldMonth);
				}
				$datePeriods[] = $fromHoldYear."/".$fromHoldMonth;
				
				
			
			
			}while($toCheckDate>=$fromHoldYear.$fromHoldMonth);
		
		}
		//END: calculate date periods
	
	
		$chart1DataArray = [];
		foreach($datePeriods as $key=>$datePeriod){
		
			$fromDateJalali = NULL;
			$toDateJalali = NULL;
			if($request->report_period==1){//Daily
				$fromDateJalali = $datePeriod;
				$toDateJalali = $datePeriod;
			}else{
				$fromDateJalali = $datePeriod."/01";
				$toDateJalali = $datePeriod."/31";
			}
			
			$fromDateGregorian=\Morilog\Jalali\Jalalian::fromFormat('Y/m/d', $fromDateJalali)->toCarbon()->toDateTimeString();
			$toDateGregorian=\Morilog\Jalali\Jalalian::fromFormat('Y/m/d', $toDateJalali)->toCarbon()->toDateTimeString();
			
			
			$tempObj = clone $clues;
			
			$periodDate = $tempObj->whereDate('created_at' , '>=',$fromDateGregorian)->whereDate('created_at' , '<=',$toDateGregorian);
			$periodCount = $periodDate->count();
			
			if($periodCount>0){
				$chart1DataArray[$datePeriod] = $periodCount;
			}
			
			
			
		
		}
		
		
		
		
		$chartOpenClosedClues = $chartOpenClosedClues->groupBy('clue_closed')->get();
	
		$chartOpenClosedCluesDataArray = [];
		foreach($chartOpenClosedClues as $chartItem){
			$tempTitle = "باز";
			if($chartItem->clue_closed==1){
				$tempTitle = "بسته";
			}
			
			$chartOpenClosedCluesDataArray[] = [
				'title' => $tempTitle,
				'count' => $chartItem->clue_count
			];
		}
		
		
		
		$chartStatusGroupedClues = $chartStatusGroupedClues->groupBy('clue_status')->get();
		$chartStatusGroupedDataArray = [];
		
		$clueTitlesArr = [
			'clue' => 'ثبت سرنخ',
			'customer' => 'ثبت مشتری',
			'validation' => 'اعتبارسنجی',
			'sale' => 'فروش',
			'pre_invoice'=>'‍‍پیش فاکتور',
			'invoice'=>'فاکتور',
			'return_from_sale' => 'برگشت از فروش'
		];
		
					
		foreach($chartStatusGroupedClues as $chartItem){
			
			$tempTitle = $clueTitlesArr[$chartItem->clue_status];
			
			$chartStatusGroupedDataArray[] = [
				'title' => $tempTitle,
				'count' => $chartItem->clue_count
			];
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
        
        $permittedStateRegionCities = Helpers::getPermittedCities(auth()->user());

        $states = City::select('*');
        if(is_array($permittedStateRegionCities['permitted_states'])){
            $states = $states->whereIn('id',$permittedStateRegionCities['permitted_states']);
        }else if($permittedStateRegionCities['permitted_states']!='%'){
            $states = $states->whereIn('id',[]);
        }
        $states = $states->where('unit_type', 0)->where('unit_status', 1)->orderBy('unit_order', 'ASC')->get();

        $cities = [];
        if(!empty($request->state_id)){
        	//TODO: just show permitted cities
            $cities = City::where('state_id',$request->state_id)->where('unit_status',1)->orderBy('unit_order','ASC')->get();
        }


 




		$changeToSaleStatusRate = 0;
		
		
		$allCluesIDs = $allClues->get()->pluck('id')->toArray();
		$allRegisteredCluesCount = count($allCluesIDs);
		
		
		$allChangedToSaleClues = ClueSteps::select('clue_id')->whereIn('clue_id',$allCluesIDs)->where('step_key','sale')->groupBy('clue_id')->get();
		$allChangedToSaleCluesCount = $allChangedToSaleClues->count();
		
		
		$changeToSaleStatusRate = round(((100/$allRegisteredCluesCount)*$allChangedToSaleCluesCount),2);
		
		


        
		
		
       
 
        return view('panel.admin.clues.report', compact('clues','states','cities','chart1DataArray','chartOpenClosedCluesDataArray','chartStatusGroupedDataArray','allRegisteredCluesCount','allChangedToSaleCluesCount','changeToSaleStatusRate'));
    	
    	
    	
    	
    	
    	
    }
	
	
	
	
	
	public function updateClueMarketingList(Request $request){
		
		
		//TODO: validate to access own customer
        $this->validate($request, [

            'clue_id' => 'required|integer|exists:clues,id',
            'job_class_id' => 'required|exists:job_classes,id',
			'legal_status' => 'required|max:20',
			'activity_level' => 'required|max:20',
			'introduce_method' => 'nullable|max:191'

        ]);



        $clue = Clue::findOrFail($request->clue_id);

		$inputs = [
			'created_by' => auth()->user()->id,
			'clue_id' => $clue->id,
			'job_class_id' => $request->job_class_id,
			'legal_status' => $request->legal_status,
			'legal_status' => $request->legal_status,
			'activity_level' => $request->activity_level,
			'introduce_method' => $request->introduce_method,
		];

        $marketingList = MarketingList::create($inputs);



        return redirect()->back()
            ->with('success', 'آیتم فهرست بازاریابی با موفقیت اضافه شد');
		
	}
	
	
	
	
	public function updateClueDescription(Request $request)

    {

	//TODO: validate to access own customer
        $this->validate($request, [

            'customer_description' => 'required|max:5000',
            'clue_id' => 'required|exists:clues,id'

        ]);



        $clue = Clue::select('*');
        if(auth()->user()->user_type=='SALES_SPECIALIST'){
            $clue = $clue->where('creator_user_id',auth()->user()->id);
        }
        $clue = $clue->where('id',$request->clue_id)->firstOrFail();


	$clue->customer_description = $request->customer_description;
	$clue->save();



        return redirect()->back()
            ->with('success', 'توضیحات با موفقیت ویرایش شد.');

    }
	
	
	
	
	public function updateClueProducts(Request $request)

    {

	//TODO: validate to access own customer
        $this->validate($request, [

            'clue_products.*' => 'required|integer|exists:products,id',
            'clue_id' => 'required|exists:clues,id'

        ]);
		
		//TODO: to limit access
		$clue = CLUE::select('*');
        if(auth()->user()->user_type=='SALES_SPECIALIST'){
            $clue = $clue->where('creator_user_id',auth()->user()->id);
        }
        $clue = $clue->where('id',$request->clue_id)->firstOrFail();
		
		
		$productIDs = $request->clue_products;
		
		
		$clue->products()->sync($productIDs);




        return redirect()->back()
            ->with('success', 'محصولات مرتبط با سرنخ با موفقیت ویرایش شدند');

    }
    
    
    
	
	
	
	public function mergeToAnotherClue(Request $request){
		$baseClue = Clue::findOrFail($request->base_clue_id);
		$toMergeClue = Clue::findOrFail($request->to_merge_clue_id);
		
		$toAddClueNotes =  Note::where('clue_id',$toMergeClue->id)->get();
		$toAddClueAlarms =  CallTargetAlarm::where('clue_id',$toMergeClue->id)->get();
		
		return view('panel.admin.clues.merge-clues', compact('baseClue','toMergeClue','toAddClueNotes','toAddClueAlarms'));
			
			
		
	}
	
	
	
	public function mergeConfirmToAnotherClue(Request $request){
		
		$baseClue = Clue::findOrFail($request->base_clue_id);
		$toMergeClue = Clue::findOrFail($request->to_merge_clue_id);
		
		$toAddClueNotes =  Note::where('clue_id',$toMergeClue->id)->get();
		$toAddClueAlarms =  CallTargetAlarm::where('clue_id',$toMergeClue->id)->get();
		
		
		
		
		
		
		DB::beginTransaction();

		try {
			
			//ADD all notes and alarms to base CLUE
			foreach($toAddClueNotes as $toAddNote){
				
				
				$note = Clone $toAddNote;
				
				$note->clue_id = $baseClue->id;
				$note->save();
				
			}
			
			
			//TODO remove to_merge CLUE
			foreach($toAddClueAlarms as $toAddAlarm){
				
				
				$alarm = Clone $toAddAlarm;
				
				$alarm->clue_id = $baseClue->id;
				$alarm->save();
				
			}
			
			
			$toMergeClue->delete();

			DB::commit();
			// all good
		} catch (\Exception $e) {
			DB::rollback();
			
			return redirect()->route('clues.showCartable',['clue_id'=>$baseClue->id])
				->with('danger', 'عملیات ادغام سرنخ‌ها انجام نشد.');
		}
		
		


        return redirect()->route('clues.showCartable',['clue_id'=>$baseClue->id])
            ->with('success', 'ادغام سرنخ‌ها با موفقیت انجام شد.');
		
		
		
	}
	
	
	
	
    
}
