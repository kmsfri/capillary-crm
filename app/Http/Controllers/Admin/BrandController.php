<?php


namespace App\Http\Controllers\Admin;


use App\Models\Brand;

use App\Models\Business;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class BrandController extends Controller

{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    function __construct()

    {

        $this->middleware('permission:brand-list|brand-create|brand-edit|brand-delete', ['only' => ['index', 'show']]);

        $this->middleware('permission:brand-create', ['only' => ['create', 'store']]);

        $this->middleware('permission:brand-edit', ['only' => ['edit', 'update']]);

        $this->middleware('permission:brand-delete', ['only' => ['destroy']]);
		
		$this->middleware('permission:management-business-brands', ['only' => ['editBusinessBrands','showBusinessBrands']]);
		
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()

    {

        $brands = Brand::latest()->paginate(5);

        return view('panel.admin.brands.index', compact('brands'))
            ->with('i', (request()->input('page', 1) - 1) * 5);

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()

    {

        return view('panel.admin.brands.create');

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)

    {

        request()->validate([

            'brand_name' => 'required|max:191',
			'manufacturer' => 'required|max:191',
            'descriptions' => 'nullable|max:500'
          

        ]);


        Brand::create($request->all());


        return redirect()->route('brands.index')
            ->with('success', 'برند با موفقیت ایجاد شد.');

    }


    /**
     * Display the specified resource.
     *
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */

    public function show(Brand $brand)

    {

        return view('panel.admin.brands.show', compact('brand'));

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */

    public function edit(Brand $brand)

    {

        return view('panel.admin.brands.edit', compact('brand'));

    }


    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, Brand $brand)

    {

        request()->validate([

            'brand_name' => 'required|max:191',
			'manufacturer' => 'required|max:191',
            'descriptions' => 'nullable|max:500'
          

        ]);


        $brand->update($request->all());


        return redirect()->route('brands.index')
            ->with('success', 'برند مورد نظر با موفقیت ویرایش شد');

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */

    public function destroy(Brand $brand)

    {

        $brand->delete();


        return redirect()->route('brands.index')
            ->with('success', 'برند مورد نظر با موفقیت حذف شد');

    }
	
	
	public function editBusinessBrands(Request $request){
		
		//TODO: validate access to business_id
		
		$business = Business::findOrFail($request->business_id);
		
		$brands = Brand::select('*')->get();

		$businessBrandsMaster = $business->brands()->get();
		$businessBrands = [];
		$businessBrandsIDs = [];
		foreach($businessBrandsMaster as $bbm){
			$businessBrands[$bbm->pivot->brand_id]  = [
				'id' => $bbm->pivot->brand_id,
				'price' => $bbm->pivot->price,
			];
			
			$businessBrandsIDs[] = $bbm->pivot->brand_id;

		}
		
		return view('panel.admin.brands.edit-business-brands', compact('business','brands','businessBrands','businessBrandsIDs'));
		
	}
	
	
	public function updateBusinessBrands(Request $request){
		
		request()->validate([

            'business_id' => 'required|integer|exists:businesses,id',
            'brand_ids.*' => 'required|integer|exists:brands,id',
			'brand_price.*' => 'nullable|integer',

        ]);
		
		$data = [];

		
		$business =  Business::findOrFail($request->business_id);
		
		
		$data = [];
		if(!empty($request->brand_ids)){
			foreach($request->brand_ids as $key=>$brandID){
				
				$data[$brandID] = ['price'=>$request->brand_price[$key]];
			
			}
		}
		

		
		
		
		$business->brands()->sync($data);
		
		
		return redirect()->back()

            ->with('success','قیمت برندهای فروشگاه با موفقیت ثبت شد');
		
	}
	
	

}
