<?php


namespace App\Http\Controllers\Admin;


use App\Models\Campaign;

use App\Models\User;

use App\Models\Clue;

use Helpers;

use DB;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class CampaignController extends Controller

{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    function __construct()

    {

        $this->middleware('permission:campaign-list|campaign-create|campaign-edit|campaign-delete', ['only' => ['index', 'show']]);

        $this->middleware('permission:campaign-create', ['only' => ['create', 'store']]);

        $this->middleware('permission:campaign-edit', ['only' => ['edit', 'update']]);

        $this->middleware('permission:campaign-delete', ['only' => ['destroy']]);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()

    {

        $campaigns = Campaign::latest()->paginate(25);

        return view('panel.admin.campaigns.index', compact('campaigns'))
            ->with('i', (request()->input('page', 1) - 1) * 25);

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()

    {

        return view('panel.admin.campaigns.create');

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)

    {

        request()->validate([

            'campaign_title' => 'required|max:191',
            'campaign_description' => 'required|max:1000',
            'start_at' => 'required|max:20',
            'end_at' => 'required|max:20',
            'campaign_source' => 'required|max:255',


        ]);
		
		
		$dateExploded = explode('/',$request->start_at);
        $startAt = implode(
			'-',
			\Morilog\Jalali\CalendarUtils::toGregorian($dateExploded[0], $dateExploded[1], $dateExploded[2])
		);
		
		
		$endExploded = explode('/',$request->end_at);
        $endAt = implode(
			'-',
			\Morilog\Jalali\CalendarUtils::toGregorian($endExploded[0], $endExploded[1], $endExploded[2])
		);
		
		
		$tempData = $request->all();
		unset($tempData['start_at']);
		unset($tempData['end_at']);
		
		$data = array_merge($tempData,[
			'start_at' => $startAt,
			'end_at' => $endAt,
			'creator_user_id' => auth()->user()->id,
		]);


        Campaign::create($data);


        return redirect()->route('campaigns.index')
            ->with('success', 'کمپین با موفقیت ایجاد شد.');

    }


    /**
     * Display the specified resource.
     *
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */

    public function show(Campaign $campaign)

    {

        return view('panel.admin.campaigns.show', compact('campaign'));

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */

    public function edit(Campaign $campaign)

    {
		$tempDateExploded = explode(' ',$campaign->start_at);
		$tempDateExploded = explode('-',$tempDateExploded[0]);
		$campaign->start_at = implode('/',\Morilog\Jalali\CalendarUtils::toJalali($tempDateExploded[0],$tempDateExploded[1],$tempDateExploded[2] ) );
		
		
		$tempDateExploded = explode(' ',$campaign->end_at);
		$tempDateExploded = explode('-',$tempDateExploded[0]);
		$campaign->end_at = implode('/',\Morilog\Jalali\CalendarUtils::toJalali($tempDateExploded[0],$tempDateExploded[1],$tempDateExploded[2] ) );
		

        return view('panel.admin.campaigns.edit', compact('campaign'));

    }


    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, Campaign $campaign)

    {

        request()->validate([

            'campaign_title' => 'required|max:191',
            'campaign_description' => 'required|max:1000',
            'start_at' => 'required|max:20',
            'end_at' => 'required|max:20',
            'campaign_source' => 'required|max:255',

        ]);
		
		
		$dateExploded = explode('/',$request->start_at);
        $startAt = implode(
			'-',
			\Morilog\Jalali\CalendarUtils::toGregorian($dateExploded[0], $dateExploded[1], $dateExploded[2])
		);
		
		
		$endExploded = explode('/',$request->end_at);
        $endAt = implode(
			'-',
			\Morilog\Jalali\CalendarUtils::toGregorian($endExploded[0], $endExploded[1], $endExploded[2])
		);
		
		$tempData = $request->all();
		unset($tempData['start_at']);
		unset($tempData['end_at']);
		
		$data = array_merge($tempData,[
			'start_at' => $startAt,
			'end_at' => $endAt,
			'last_updated_by' => auth()->user()->id,
		]);


        $campaign->update($data);


        return redirect()->route('campaigns.index')
            ->with('success', 'کمپین مورد نظر با موفقیت ویرایش شد');

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */

    public function destroy(Campaign $campaign)

    {

        $campaign->delete();


        return redirect()->route('campaigns.index')
            ->with('success', 'کمپین مورد نظر با موفقیت حذف شد');

    }
	
	
	
	
	
	
	
	
	
	
	
	public function showCampaignReport(Request $request){
	
		
		$permittedConditions = Helpers::getBusinessesAccessPermittedConditions(auth()->user());
		
		
		$campaign = Campaign::findOrFail($request->campaign_id);
		
		$creatorUser = NULL;
		if(!empty($campaign->creator_user_id)){
			$creatorUser = User::find($campaign->creator_user_id);
		}
		
		$lastUpdatorUser = NULL;
		if(!empty($campaign->last_updated_by)){
			$lastUpdatorUser = User::find($campaign->last_updated_by);
		}
		
		
	
		
		$clues = Clue::select('*')->where('id','<',0);
		if(auth()->user()->user_type=='ADMIN' || auth()->user()->user_type=='CSO'){
			//do nothing
			$clues = $clues->orWhere('id','>=',0);
		}else{
			foreach($permittedConditions as $key=>$condition){
				$toFireCondition1 = [];
				if(!empty($condition['ignore_cities']) && $condition['ignore_cities']){
					$toFireCondition1 = [
						'operand'=>'LIKE',
						'values' => '%'
					];
				}else{
					$toFireCondition1 = [
						'operand'=>'IN',
						'values' => $condition['city_ids']
					];
				}

				$toFireCondition2 = [];
				if(!empty($condition['ignore_creator_id']) && $condition['ignore_creator_id']){
					$toFireCondition2 = [
						'operand'=>'LIKE',
						'values' => '%'
					];
				}else{
					$toFireCondition2 = [
						'operand'=>'=',
						'values' => $condition['creator_id']
					];
				}
				$clues = $clues->orWhere(function($query) use($toFireCondition1,$toFireCondition2) {
					if($toFireCondition1['operand']=='IN'){
						$query->whereIn('city_id', $toFireCondition1['values'])->where('creator_user_id',$toFireCondition2['operand'],$toFireCondition2['values']);
					}else{
						$query->where('city_id',$toFireCondition1['operand'], $toFireCondition1['values'])->where('creator_user_id',$toFireCondition2['operand'],$toFireCondition2['values']);
					}
				});
			}
			
		}
		
		$permittedIDs = $clues->get()->pluck('id')->toArray();
		
		unset($clues);
		
		$clues = Clue::select('*')->whereIn('id',$permittedIDs)->where('campaign_id',$campaign->id);
		
		if(!empty($request->clue_closed)){
        
			$searchKey = ($request->clue_closed=="open")?0:1;
		
        	$clues = $clues->where('clue_closed',$searchKey);
		
        
        }
		
		
		
		if(!empty($request->clue_status)){
        
			$searchKey = $request->clue_status;
		
        	$clues = $clues->where('clue_status',$searchKey);
		
        
        }
		
		$permittedIDs = $clues->get()->pluck('id')->toArray();
		
		
		
		
		unset($clues);
 	
		
		
		
		
		
		//begin clue charts
		$cluesTempObj = Clue::select('*')->whereIn('id',$permittedIDs);
		
		$chartOpenClosedClues = Clue::selectRaw('clue_closed, count(id) as clue_count')->whereIn('id',$permittedIDs);
		
		
		$chartStatusGroupedClues = Clue::selectRaw('clue_status, count(id) as clue_count')->whereIn('id',$permittedIDs);

 
 
		$minDate = Clue::select(DB::raw('min(created_at) AS min_created_at'))->whereIn('id',$permittedIDs)->first();
		$fromDateGregorian = $minDate->min_created_at;
		$fromDateJalali = \Morilog\Jalali\Jalalian::forge($fromDateGregorian)->format('%Y/%m/%d');
 
        
        $maxDate =  Clue::select(DB::raw('max(created_at) AS max_created_at'))->whereIn('id',$permittedIDs)->first();
		$toDateGregorian = $maxDate->max_created_at;
		$toDateJalali = \Morilog\Jalali\Jalalian::forge($toDateGregorian)->format('%Y/%m/%d');
        
        
        
        //BEGIN: calculate date periods
        $explodedDate = explode('/',$toDateJalali);
		$toCheckDate = $explodedDate[0].$explodedDate[1];
		
		$fromHoldYear = NULL;
		$fromHoldMonth = NULL;
		do{
			
			if(empty($fromHoldYear)){
				$explodedDate = explode('/',$fromDateJalali);
				$fromHoldYear = $explodedDate[0];
				$fromHoldMonth = $explodedDate[1]; 
			}else{
				if($fromHoldMonth<=11){
					$fromHoldMonth++;
				}else{
					$fromHoldMonth = 1;
					$fromHoldYear++;
				}
			}
			
			if($fromHoldMonth<10){
				$fromHoldMonth = "0".((int)$fromHoldMonth);
			}
			$datePeriods[] = $fromHoldYear."/".$fromHoldMonth;
			
			
		
		
		}while($toCheckDate>=$fromHoldYear.$fromHoldMonth);
		//END: calculate date periods
	
	
		$chart1DataArray = [];
		foreach($datePeriods as $key=>$datePeriod){
		
			$fromDateJalali = $datePeriod."/01";
			$toDateJalali = $datePeriod."/31";
			
			$fromDateGregorian=\Morilog\Jalali\Jalalian::fromFormat('Y/m/d', $fromDateJalali)->toCarbon()->toDateTimeString();
			$toDateGregorian=\Morilog\Jalali\Jalalian::fromFormat('Y/m/d', $toDateJalali)->toCarbon()->toDateTimeString();
			
			
			$tempObj = clone $cluesTempObj;
			
			$periodDate = $tempObj->whereDate('created_at' , '>=',$fromDateGregorian)->whereDate('created_at' , '<=',$toDateGregorian);
			$periodCount = $periodDate->count();
			
			if($periodCount>0){
				$chart1DataArray[$datePeriod] = $periodCount;
			}
			
			
			
		
		}
		
		
		
		
		$chartOpenClosedClues = $chartOpenClosedClues->groupBy('clue_closed')->get();
	
		$chartOpenClosedCluesDataArray = [];
		foreach($chartOpenClosedClues as $chartItem){
			$tempTitle = "باز";
			if($chartItem->clue_closed==1){
				$tempTitle = "بسته";
			}
			
			$chartOpenClosedCluesDataArray[] = [
				'title' => $tempTitle,
				'count' => $chartItem->clue_count
			];
		}
		
		
		
		$chartStatusGroupedClues = $chartStatusGroupedClues->groupBy('clue_status')->get();
		$chartStatusGroupedDataArray = [];
		
		$clueTitlesArr = [
			'clue' => 'ثبت سرنخ',
			'customer' => 'ثبت مشتری',
			'validation' => 'اعتبارسنجی',
			'sale' => 'فروش',
			'pre_invoice'=>'‍‍پیش فاکتور',
			'invoice'=>'فاکتور',
			'return_from_sale' => 'برگشت از فروش'
		];
		
					
		foreach($chartStatusGroupedClues as $chartItem){
			
			$tempTitle = $clueTitlesArr[$chartItem->clue_status];
			
			$chartStatusGroupedDataArray[] = [
				'title' => $tempTitle,
				'count' => $chartItem->clue_count
			];
		}
		
		
		//end clue charts
		
		
		
		
		
		
		
		
		$clues = Clue::select('*')->whereIn('id',$permittedIDs)->paginate(20);
		
		
		
		
		$clueClosedStatuses = [
		
			'open' => 'باز',
			'closed' => 'بسته',
		
		];
		
		
		
		
		
		$clueStatuses = [
		
			'clue' => 'ایجاد سرنخ',
			'validation' => 'اعتبارسنجی',
			'pre_invoice' => 'ثبت مشتری(صدور پیش فاکتور)',
			'invoice' => 'ثبت مشتری(صدور فاکتور)',
		
		];
		
		
		
		return view('panel.admin.campaigns.report', compact('campaign','lastUpdatorUser','creatorUser','clues','chart1DataArray','chartOpenClosedCluesDataArray','chartStatusGroupedDataArray','clueStatuses','clueClosedStatuses'));
	
	}
    
    
    
    
    

}
