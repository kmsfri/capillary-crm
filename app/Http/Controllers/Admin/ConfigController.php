<?php


namespace App\Http\Controllers\Admin;


use App\Models\CrmConfig;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class ConfigController extends Controller

{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    function __construct()

    {

        $this->middleware('permission:configs-edit', ['only' => ['edit', 'update']]);

     
    }

   

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */

    public function edit(Request $request)
    {
		
		$data['max_cheque_days'] = CrmConfig::where('config_key','MAX_CHEQUE_DAYS')->firstOrFail()->config_value;
		
		$data['max_discount_amount'] = CrmConfig::where('config_key','MAX_DISCOUNT_AMOUNT')->firstOrFail()->config_value;
		
		$data['max_invoice_item_amount'] = CrmConfig::where('config_key','MAX_INVOICE_ITEM_AMOUNT')->firstOrFail()->config_value;
		
		$data['max_sum_of_invoice_items_amount'] = CrmConfig::where('config_key','MAX_SUM_OF_INVOICE_ITEMS_AMOUNT')->firstOrFail()->config_value;
		

        return view('panel.admin.configs.edit', compact('data'));

    }


    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request)

    {

        request()->validate([

            'max_cheque_days' => 'required|integer',
			'max_discount_amount' => 'required|integer',
            'max_invoice_item_amount' => 'required|integer',
            'max_sum_of_invoice_items_amount' => 'required|integer'

        ]);
		
		//TODO: ADD TRANSACTION:

		$crmConfig1 = CrmConfig::where('config_key','MAX_CHEQUE_DAYS')->firstOrFail();
		$crmConfig1->config_value=$request->max_cheque_days;
		$crmConfig1->save();
		
		$crmConfig2 = CrmConfig::where('config_key','MAX_DISCOUNT_AMOUNT')->firstOrFail();
		$crmConfig2->config_value=$request->max_discount_amount;
		$crmConfig2->save();
		
		
		$crmConfig3 = CrmConfig::where('config_key','MAX_INVOICE_ITEM_AMOUNT')->firstOrFail();
		$crmConfig3->config_value=$request->max_invoice_item_amount;
		$crmConfig3->save();
		
		$crmConfig4 = CrmConfig::where('config_key','MAX_SUM_OF_INVOICE_ITEMS_AMOUNT')->firstOrFail();
		$crmConfig4->config_value=$request->max_sum_of_invoice_items_amount;
		$crmConfig4->save();
        

        return redirect()->route('configs.edit')
            ->with('success', 'تنظیمات مربوط به قیمتها با موفقیت ویرایش شد');

    }



}
