<?php


namespace App\Http\Controllers\Admin;


use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Models\Customer;

use App\Models\CallTargetAlarm;

use App\Models\Note;

use App\Models\Clue;

use Spatie\Permission\Models\Role;

use DB;

use Hash;

use Helpers;

use Illuminate\Support\Arr;


use Excel;

use App\Imports\CustomersImport;


use App\Exports\CustomersExport;



class CustomerController extends Controller

{


    function __construct()

    {

        $this->middleware('permission:customer-list|customer-create|customer-edit|customer-delete', ['only' => ['index', 'show','showCustomersReport']]);

        $this->middleware('permission:customer-create', ['only' => ['create', 'store']]);

        $this->middleware('permission:customer-edit', ['only' => ['edit', 'update','updateCustomerDescription']]);

        $this->middleware('permission:customer-delete', ['only' => ['destroy']]);
        
        $this->middleware('permission:show-customer-cartable', ['only' => ['showCartable','addNewNote']]);

    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)

    {
    
    
    
    	$permittedConditions = Helpers::getBusinessesAccessPermittedConditions(auth()->user());

        $data = Customer::select('*')->where('id','<',0);

        foreach($permittedConditions as $key=>$condition){
            

            $toFireCondition2 = [];
            if(!empty($condition['ignore_creator_id']) && $condition['ignore_creator_id']){
                $toFireCondition2 = [
                    'operand'=>'LIKE',
                    'values' => '%'
                ];
            }else{
                $toFireCondition2 = [
                    'operand'=>'=',
                    'values' => $condition['creator_id']
                ];
            }
            if($toFireCondition2['operand']=="LIKE" && $toFireCondition2['values']=="%"){
            	   //do nothing
            	   $data = $data->orWhere(function($query) use($toFireCondition2) {
		        $query->where('id','>=',0);
		   });
            }else{
		    $data = $data->orWhere(function($query) use($toFireCondition2) {
		        $query->where('creator_id',$toFireCondition2['operand'],$toFireCondition2['values']);
		    });
            }
        }
        
        
        $permittedCustomerIDs = $data->get()->pluck('id')->toArray();
 	
		unset($data);
 	
 	
        $data = Customer::select('*')->whereIn('id',$permittedCustomerIDs);
        
        if(!empty($request->search_key)){
        
        	$searchKey = $request->search_key;
        
        	$data = $data->where(function($query) use($searchKey) {
		    $query->where('first_name','LIKE', '%'.$searchKey.'%')->orWhere('last_name','LIKE', '%'.$searchKey.'%')
		   	 ->orWhere('business_name','LIKE', '%'.$searchKey.'%')->orWhere('state','LIKE', '%'.$searchKey.'%')
		   	 ->orWhere('city','LIKE', '%'.$searchKey.'%')->orWhere('address','LIKE', '%'.$searchKey.'%')
		   	 ->orWhere('phone','LIKE', '%'.$searchKey.'%')->orWhere('phone2','LIKE', '%'.$searchKey.'%')->orWhere('phone3','LIKE', '%'.$searchKey.'%')
		   	 ->orWhere('mobile','LIKE', '%'.$searchKey.'%')->orWhere('mobile2','LIKE', '%'.$searchKey.'%')->orWhere('mobile3','LIKE', '%'.$searchKey.'%');
		});
		
		
        

        
        }
        
        
        
        if(!empty($request->state)){
        
        	$searchKey = $request->state;
        
        	$data = $data->where('state','LIKE','%'.$searchKey.'%');
		
        }
        
        
        if(!empty($request->city)){
        
        	$searchKey = $request->city;
        
        	$data = $data->where('city','LIKE','%'.$searchKey.'%');
		
        }
        
        
    
		if(!empty($request->export_excel) && $request->export_excel==1){
			
			
			
			//die('disabled');
			/*
			$allAlarms = Customer::whereNotNull('business_name')->where('business_name','!=','')
						->where('id','>=',12000)->where('id','<=',14000)->get();
			foreach($allAlarms as $customer){
				
//				$customer = Customer::find($alarm->customer_id);
				if(!empty($customer)){
					
					
					$equalentClue = Clue::Where(function($query) use($customer) {
												$query->whereNotNull('phone')->where('phone','!=','')->where('phone',$customer->phone);
											})
											->orWhere(function($query) use($customer) {
												$query->whereNotNull('phone2')->where('phone2','!=','')->where('phone2',$customer->phone2);
											})
											->orWhere(function($query) use($customer) {
												$query->whereNotNull('phone3')->where('phone3','!=','')->where('phone3',$customer->phone3);
											})
											->orWhere(function($query) use($customer) {
												$query->whereNotNull('mobile')->where('mobile','!=','')->where('mobile',$customer->mobile);
											})
											->orWhere(function($query) use($customer) {
												$query->whereNotNull('mobile2')->where('mobile2','!=','')->where('mobile2',$customer->mobile2);
											})
											->orWhere(function($query) use($customer) {
												$query->whereNotNull('mobile3')->where('mobile3','!=','')->where('mobile3',$customer->mobile3);
											})
											->orWhere(function($query) use($customer) {
												$query->whereNotNull('sepidar_phone')->where('sepidar_phone','!=','')->where('sepidar_phone',$customer->sepidar_phone);
											})
											->orWhere(function($query) use($customer) {
												$query->whereNotNull('sepidar_code')->where('sepidar_code','!=','')->where('sepidar_code',$customer->sepidar_code);
											})
											->first();
											
											
					if(!empty($equalentClue) && ($equalentClue->business_name== $equalentClue->last_name)){
						$equalentClue->business_name=$customer->business_name;
						$equalentClue->save();
					}
					
				}
				
			}
			
			
			
			die("done");
			
			*/
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			$data = $data->orderBy('created_at','DESC')->get();
			
			$dataToExport = [];
			foreach($data as $key=>$item){
				
				
				$examinedCity = \App\Models\City::where('unit_name','LIKE','%'.trim($item['city']).'%')->where('unit_type',1)->first();
				$cityID = '';
				$cityName = '';
				$stateName = '';
				if(!empty($examinedCity)){
					$cityID = $examinedCity->id;
					$cityName = $examinedCity->unit_name;
					$stateName = $examinedCity->parent()->first()->unit_name;
				}
				
				$dataToExport[] = [
					$item['first_name'],
					$item['last_name'],
					$item['business_name'],
					$item['state'],
					$item['city'],
					$item['address'],
					$item['phone1'],
					$item['phone2'],
					$item['phone3'],
					$item['mobile1'],
					$item['mobile2'],
					$item['mobile3'],
					$item['customer_description'],
					$item['sepidar_code'],
					$item['sepidar_phone'],
					$cityID,
					$stateName,
					$cityName,
					'',
					''
					
					
				
				];
				
			}
			
			return Excel::download(new CustomersExport($dataToExport), 'customers.xlsx');
		}
		
    

        $data = $data->orderBy('created_at','DESC')->paginate(20);
    
    
    
    
    
/*
        if(auth()->user()->user_type=='SALES_SPECIALIST'){
            $data = $data->where('creator_id',auth()->user()->id);
        }
        $data = $data->orderBy('id', 'DESC')->paginate(20);
*/
        return view('panel.admin.customers.index', compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 20);

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()

    {

		die("این بخش غیرفعال شده است. موارد جدید را در بخش سرنخ وارد نمایید");

        return view('panel.admin.customers.create');

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)

    {

        $this->validate($request, [

            'first_name' => 'nullable|max:255',
            'last_name' => 'required_without:business_name|max:255',
            'business_name' => 'required_without:last_name|max:255',
            'city' => 'nullable|max:255',
            'address' => 'nullable|max:255',
            'phone' => 'nullable|required_without_all:mobile,mobile2,mobile3,phone2,phone3|max:255|unique:customers,phone|unique:customers,phone2|unique:customers,phone3',
            'phone2' => 'nullable|required_without_all:mobile,mobile2,mobile3,phone,phone3|max:255|unique:customers,phone|unique:customers,phone2|unique:customers,phone3',
            'phone3' => 'nullable|required_without_all:mobile,mobile2,mobile3,phone,phone2|max:255|unique:customers,phone|unique:customers,phone2|unique:customers,phone3',
            'mobile' => 'nullable|required_without_all:phone,phone2,phone3,mobile2,mobile3|max:255|unique:customers,mobile|unique:customers,mobile2|unique:customers,mobile3',
            'mobile2' => 'nullable|required_without_all:phone,phone2,phone3,mobile,mobile3|max:255|unique:customers,mobile|unique:customers,mobile2|unique:customers,mobile3',
            'mobile3' => 'nullable|required_without_all:phone,phone2,phone3,mobile,mobile2|max:255|unique:customers,mobile|unique:customers,mobile2|unique:customers,mobile3',

        ]);


        $input = $request->all();

        $input['creator_id'] = auth()->user()->id;

        $customer = Customer::create($input);



        return redirect()->route('customers.index')
            ->with('success', 'مشتری جدید با موفقیت اضافه شد');

    }


    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */

    public function show($id)

    {

        $customer = Customer::find($id);

        return view('panel.admin.customers.show', compact('customer'));

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */

    public function edit($id)

    {

        $customer = Customer::select('*');
        if(auth()->user()->user_type=='SALES_SPECIALIST'){
            $customer = $customer->where('creator_id',auth()->user()->id);
        }
        $customer = $customer->where('id',$id)->first();



        return view('panel.admin.customers.edit', compact('customer'));

    }


    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)

    {

        $this->validate($request, [
        
        
            'first_name' => 'nullable|max:255',
            'last_name' => 'required_without:business_name|max:255',
            'business_name' => 'required_without:last_name|max:255',
            'city' => 'nullable|max:255',
            'address' => 'nullable|max:255',
            'phone' => 'nullable|required_without_all:mobile,mobile2,mobile3,phone2,phone3|max:255|unique:customers,phone,'.$id.'|unique:customers,phone2,'.$id.'|unique:customers,phone3,'.$id,
            'phone2' => 'nullable|required_without_all:mobile,mobile2,mobile3,phone,phone3|max:255|unique:customers,phone,'.$id.'|unique:customers,phone2,'.$id.'|unique:customers,phone3,'.$id,
            'phone3' => 'nullable|required_without_all:mobile,mobile2,mobile3,phone,phone2|max:255|unique:customers,phone,'.$id.'|unique:customers,phone2,'.$id.'|unique:customers,phone3,'.$id,
            'mobile' => 'nullable|required_without_all:phone,phone2,phone3,mobile2,mobile3|max:255|unique:customers,mobile,'.$id.'|unique:customers,mobile2,'.$id.'|unique:customers,mobile3,'.$id,
            'mobile2' => 'nullable|required_without_all:phone,phone2,phone3,mobile,mobile3|max:255|unique:customers,mobile,'.$id.'|unique:customers,mobile2,'.$id.'|unique:customers,mobile3,'.$id,
            'mobile3' => 'nullable|required_without_all:phone,phone2,phone3,mobile,mobile2|max:255|unique:customers,mobile,'.$id.'|unique:customers,mobile2,'.$id.'|unique:customers,mobile3,'.$id,

        ]);


        $input = $request->all();




        $customer = Customer::select('*');
        if(auth()->user()->user_type=='SALES_SPECIALIST'){
            $customer = $customer->where('creator_id',auth()->user()->id);
        }
        $customer = $customer->where('id',$id)->first();



        $customer->update($input);





        return redirect()->route('customers.index')
            ->with('success', 'مشتری با موفقیت ویرایش شد');

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {

        $customer = Customer::select('*');
        if(auth()->user()->user_type=='SALES_SPECIALIST'){
            $customer = $customer->where('creator_id',auth()->user()->id);
        }
        $customer = $customer->where('id',$id)->first();

        $customer->delete();

        return redirect()->route('customers.index')
            ->with('success', 'مشتری با موفقیت حذف شد');

    }
    
    
    
    public function uploadExcelFile(Request $request){
    
    	ini_set('memory_limit', '-1');
    	 
    	$import = new CustomersImport;
	Excel::import($import, $request->excel_file);
	
	
    	
	$message = "تعداد ".
			$import->data['inserted']
		   ." مشتری با موفقیت اضافه شد. تعداد ".
		   $import->data['failed_to_insert']." مشتری با خطا مواجه شد و تعداد ".
		   $import->data['duplicate_count']." مشتری با شماره های زیر تکراری بودند: ".implode(' , ',$import->data['duplicate_list']);



	return redirect()->route('customers.index')
            ->with('success', $message);

    	
    }
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public function showCustomersReport(Request $request){
    
    
		$permittedConditions = Helpers::getBusinessesAccessPermittedConditions(auth()->user());

        //$data = Customer::select(DB::raw('count(id) as `data`'), DB::raw('YEAR(created_at) year, MONTH(created_at) month'))->where('id','<',0);
        $data = Customer::select('*')->where('id','<',0);

        foreach($permittedConditions as $key=>$condition){
            

            $toFireCondition2 = [];
            if(!empty($condition['ignore_creator_id']) && $condition['ignore_creator_id']){
                $toFireCondition2 = [
                    'operand'=>'LIKE',
                    'values' => '%'
                ];
            }else{
                $toFireCondition2 = [
                    'operand'=>'=',
                    'values' => $condition['creator_id']
                ];
            }
            $data = $data->orWhere(function($query) use($toFireCondition2) {
                $query->where('creator_id',$toFireCondition2['operand'],$toFireCondition2['values']);
            });
        }
		
		
	if(!empty($request->from_date)){
        
        	$fromDate = $request->from_date;
        	$fromDateGregorian=\Morilog\Jalali\Jalalian::fromFormat('Y/m/d', $fromDate)->toCarbon()->toDateTimeString();
        	
        	$data = $data->whereDate('created_at' , '>=',$fromDateGregorian);
        	
        	$fromDateJalali = $request->from_date;
        	
        
        }else{
        	$minDate = Customer::select(DB::raw('min(created_at) AS min_created_at'))->first();
        	$fromDateGregorian = $minDate->min_created_at;
        	$fromDateJalali = \Morilog\Jalali\Jalalian::forge($fromDateGregorian)->format('%Y/%m/%d');
        }
        
        if(!empty($request->to_date)){
        
        	$toDate = $request->to_date;
        	$toDateGregorian=\Morilog\Jalali\Jalalian::fromFormat('Y/m/d', $toDate)->toCarbon()->toDateTimeString();
        
        	$data = $data->whereDate('created_at' , '<=',$toDateGregorian);
        	
        	$toDateJalali = $request->to_date;
        	
        }else{
        	$maxDate = Customer::select(DB::raw('max(created_at) AS max_created_at'))->first();
        	$toDateGregorian = $maxDate->max_created_at;
        	$toDateJalali = \Morilog\Jalali\Jalalian::forge($toDateGregorian)->format('%Y/%m/%d');
        	
        }
		
		
		
		
		
		
		
		
		
		
		
		
	//BEGIN: calculate date periods
        $datePeriods = [];
        if($request->report_period==1){//Daily
        
        
        	$explodedDate = explode('/',$toDateJalali);
		$toCheckDate = $explodedDate[0].$explodedDate[1].$explodedDate[2];
		$fromHoldYear = NULL;
		$fromHoldMonth = NULL;
		$fromHoldDay = NULL;
		do{
			
			if(empty($fromHoldYear)){
				$explodedDate = explode('/',$fromDateJalali);
				$fromHoldYear = $explodedDate[0];
				$fromHoldMonth = $explodedDate[1]; 
				$fromHoldDay = $explodedDate[2]; 
			}else{
				if($fromHoldDay<=30){
					$fromHoldDay++;
				}else if($fromHoldMonth<=11){
					$fromHoldDay=1;
					$fromHoldMonth++;
				}else{
					$fromHoldDay=1;
					$fromHoldMonth = 1;
					$fromHoldYear++;
				}
			}
			
			
			if($fromHoldMonth<10){
				$fromHoldMonth = "0".((int)$fromHoldMonth);
			}
			
			if($fromHoldDay<10){
				$fromHoldDay = "0".((int)$fromHoldDay);
			}
			
			$datePeriods[] = $fromHoldYear."/".$fromHoldMonth."/".$fromHoldDay;
			
			
		
		
		}while($toCheckDate>($fromHoldYear.$fromHoldMonth.$fromHoldDay));
        
        
		
	}else{//Monthly
	
		$explodedDate = explode('/',$toDateJalali);
		$toCheckDate = $explodedDate[0].$explodedDate[1];
		
		$fromHoldYear = NULL;
		$fromHoldMonth = NULL;
		do{
			
			if(empty($fromHoldYear)){
				$explodedDate = explode('/',$fromDateJalali);
				$fromHoldYear = $explodedDate[0];
				$fromHoldMonth = $explodedDate[1]; 
			}else{
				if($fromHoldMonth<=11){
					$fromHoldMonth++;
				}else{
					$fromHoldMonth = 1;
					$fromHoldYear++;
				}
			}
			
			if($fromHoldMonth<10){
				$fromHoldMonth = "0".((int)$fromHoldMonth);
			}
			$datePeriods[] = $fromHoldYear."/".$fromHoldMonth;
			
			
		
		
		}while($toCheckDate>=$fromHoldYear.$fromHoldMonth);
		
	}
	//END: calculate date periods
	
	
	
	$chart1DataArray = [];
	foreach($datePeriods as $key=>$datePeriod){
	
		$fromDateJalali = NULL;
		$toDateJalali = NULL;
		if($request->report_period==1){//Daily
			$fromDateJalali = $datePeriod;
			$toDateJalali = $datePeriod;
		}else{
			$fromDateJalali = $datePeriod."/01";
			$toDateJalali = $datePeriod."/31";
		}
		
		$fromDateGregorian=\Morilog\Jalali\Jalalian::fromFormat('Y/m/d', $fromDateJalali)->toCarbon()->toDateTimeString();
		$toDateGregorian=\Morilog\Jalali\Jalalian::fromFormat('Y/m/d', $toDateJalali)->toCarbon()->toDateTimeString();
		
		
		$tempObj = clone $data;
		
		$periodDate = $tempObj->whereDate('created_at' , '>=',$fromDateGregorian)->whereDate('created_at' , '<=',$toDateGregorian);
		$periodCount = $periodDate->count();
		
		if($periodCount>0){
			$chart1DataArray[$datePeriod] = $periodCount;
		}
		
		
		
	
	}
		
		
		
		
		
		
		
	$data = [];	
		
		
		

        //$data = $data->groupBy('year','month')->get();
    
    
  

        return view('panel.admin.customers.report', compact('data','chart1DataArray'));
    	
    	
    	
    	
    	
    	
    }
    
    
    
    
    
    
    
    
    
    
    
    
    public function showCartable(Request $request){
    
    	$customerID = $request->customer_id;
    	
    	//TODO: just access to permitted custoemrs
    	$customer = Customer::findOrFail($customerID);
    	
    	 
    	
    	$customerPhones = [];
    	
    	if(!empty($customer->phone)){
    		$customerPhones[] = (int)Helpers::convertFaToEnNumbersV2($customer->phone);
    	}
    	
    	if(!empty($customer->phone2)){
    		$customerPhones[] = (int)Helpers::convertFaToEnNumbersV2($customer->phone2);
    	}
    	
    	if(!empty($customer->phone3)){
    		$customerPhones[] = (int)Helpers::convertFaToEnNumbersV2($customer->phone3);
    	}
    	
    	if(!empty($customer->mobile)){
    		$customerPhones[] = (int)Helpers::convertFaToEnNumbersV2($customer->mobile);
    	}
    	
    	if(!empty($customer->mobile2)){
    		$customerPhones[] = (int)Helpers::convertFaToEnNumbersV2($customer->mobile2);
    	}
    	
    	if(!empty($customer->mobile3)){
    		$customerPhones[] = (int)Helpers::convertFaToEnNumbersV2($customer->mobile3);
    	}
    	
    	if(!empty($customer->sepidar_phone)){
    		$customerPhones[] = (int)Helpers::convertFaToEnNumbersV2($customer->sepidar_phone);
    	}
    	
    	$getCallsQuery = DB::connection('mysql_voip')->table('cdr')->where('src','<',0);
    		
    	foreach($customerPhones as $customerPhone){
			if(!empty($customerPhone) && $customerPhone>0){
				$getCallsQuery = $getCallsQuery->orWhere(function($query) use($customerPhone){
							$query->where('src','LIKE', '%'.$customerPhone.'%')->orWhere('dst','LIKE','%'.$customerPhone.'%');
						});
			}
    	}
	
	$getCallsQuery=$getCallsQuery->orderBy('calldate','DESC');
	
	$customerCalls=$getCallsQuery->paginate(20);
	
	
	
	//TODO: maybe will be limited to specialists
	$customerAlarms = CallTargetAlarm::where('customer_id',$customerID)->get();
	
	
	//TODO: maybe will be limited to specialists
	$customerNotes = Note::where('customer_id',$customerID)->orderBy('created_at','DESC')->get();
    	
    	
    	
    	 return view('panel.admin.customers.cartable', compact('customer','customerCalls','customerAlarms','customerNotes'));
    	
    	
    
    }
    
    
    
    
    
    
    public function updateCustomerDescription(Request $request)

    {

	//TODO: validate to access own customer
        $this->validate($request, [

            'customer_description' => 'required|max:5000',
            'customer_id' => 'required|exists:customers,id'

        ]);



        $customer = Customer::select('*');
        if(auth()->user()->user_type=='SALES_SPECIALIST'){
            $customer = $customer->where('creator_id',auth()->user()->id);
        }
        $customer = $customer->where('id',$request->customer_id)->firstOrFail();


	$customer->customer_description = $request->customer_description;
	$customer->save();



        return redirect()->back()
            ->with('success', 'توضیحات مشتری با موفقیت ویرایش شد');

    }
    
    
    
    
    
    public function addNewNote(Request $request)
    {

	//TODO: validate to access own customer
        $this->validate($request, [

            'note_text' => 'required|max:1500',
            'customer_id' => 'required|exists:customers,id'

        ]);



        $customer = Customer::select('*');
        if(auth()->user()->user_type=='SALES_SPECIALIST'){
            $customer = $customer->where('creator_id',auth()->user()->id);
        }
        $customer = $customer->where('id',$request->customer_id)->firstOrFail();

	$inputs = [
		'creator_id' => auth()->user()->id,
		'customer_id' => $customer->id,
		'note_text' => $request->note_text,
	];

        $note = Note::create($inputs);



        return redirect()->back()
            ->with('success', 'یادداشت با موفقیت ایجاد شد');

    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    public function updateAllCustomers(Request $request){
    
    	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL,"http://192.168.40.21:9045/Service1.svc/GetAllCustomerInfo");
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_POST, 1);

	$data = json_encode([
	  'Password'=> 'Sepidar@123',
	  'Username'=> 'webuser'
	],true);

	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");

	$serverOutput = curl_exec($ch);

	curl_close($ch);
    
    
    	$sepidarCustomersList = json_decode($serverOutput,true);
    	foreach($sepidarCustomersList as $sepidarCustomer){
    	
    		if(!(Customer::where('sepidar_phone',$sepidarCustomer['Phone'])->exists())){
    		
    			$extraData = '';
    			
    			if(!empty($sepidarCustomer['EconomicCode'])){
    			
    				$extraData = 'کد اقتصادی: '.$sepidarCustomer['EconomicCode'];
    			}
    			
    			if(!empty($sepidarCustomer['IdentificationCode'])){
    			
    				$extraData .= ' کد شناسایی: '.$sepidarCustomer['IdentificationCode'];
    			}
    		
    			$newCustomerData = [
    				'address' => $sepidarCustomer['Address'],
    				'sepidar_code' => $sepidarCustomer['Code'],
    				'first_name' => $sepidarCustomer['Name'],
    				'last_name' => $sepidarCustomer['LastName'],
    				'sepidar_phone' => $sepidarCustomer['Phone'],
    				'business_name' => $sepidarCustomer['Title'],
    				'customer_description' => $extraData,
    				'is_sepidar_user' => 1,			
    			];
    			Customer::create($newCustomerData);
    		
    		}
    	
    		
    	}
    	
    	
    	
    	
    	
    	
    
    }
    
    
    
    

}
