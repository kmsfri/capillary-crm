<?php


namespace App\Http\Controllers\Admin;


use Helpers;
use Illuminate\Http\Request;
use App\Models\Commission;
use App\Models\CommissionMonth;
use App\Models\User;

use DB;

use App\Http\Controllers\Controller;

class CommissionController extends Controller

{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    function __construct()

    {

        $this->middleware('permission:set-monthly-commision', ['only' => ['setMonthlyCommission']]);

        $this->middleware('permission:show-users-commission-list', ['only' => ['userCommissionsList']]);
        

    }
	
	
	public function destroy(Request $request)
    {
		
		$commission = Commission::findOrFail($request->commission_id);
		
		$userCommissionMonthObj = CommissionMonth::findOrFail($commission->user_commission_month_id);

        $commission->delete();
		
		
		$monthSumSaleAmount = Commission::where('user_commission_month_id',$userCommissionMonthObj->id)->sum('sale_amount');
		$monthCommissionAmount = Commission::where('user_commission_month_id',$userCommissionMonthObj->id)->sum('commission_amount');
		$monthCommissionVisitorAmount = Commission::where('user_commission_month_id',$userCommissionMonthObj->id)->sum('visitor_amount');
		$monthCommissionSpecialistAmount = Commission::where('user_commission_month_id',$userCommissionMonthObj->id)->sum('specialist_amount');
		//$monthCommissionAmount = ($monthSumSaleAmount>0)?$this->calculateCommission($monthSumSaleAmount):0;
		
		
		$userCommissionMonthObj->sum_sale_amount = $monthSumSaleAmount;
		$userCommissionMonthObj->commission_amount = $monthCommissionAmount;
		$userCommissionMonthObj->visitor_amount = $monthCommissionVisitorAmount;
		$userCommissionMonthObj->specialist_amount = $monthCommissionSpecialistAmount;
		
		$userCommissionMonthObj->save();


        return redirect()->route('commission.show-users-month-commission-list',['commission_month_id'=>$userCommissionMonthObj->id])
            ->with('success', 'مبلغ کمیسیون مورد نظر با موفقیت حذف شد!');

    }
    
    
    
    public function setMonthlyCommission(Request $request)
    {
    
    	
    
    	$user = User::findOrFail($request->user_id);
    	
    	
    	$commission = Null;
    	$commissionDate = Null;
		
    	if(!empty($request->commission_date)){
    		$commissionDate = $request->commission_date;
    		$commission = Commission::where('user_id',$user->id)->where('commission_month',$request->commission_date)->first();
    		
 
    	}
    	
    	
    	
    
    
    	$permittedConditions = Helpers::getBusinessesAccessPermittedConditions(auth()->user());
    	
    	
    	
    	
    
        return view('panel.admin.commission.set-monthly-commission',compact('commission','user','commissionDate',));

    }
    
    
    private function calculateCommission($value){
		
		$calculated1 = 0;
		if($value<750000000){
			$calculated1 = $value*0.5/100;
		}else{
			$calculated1 = 750000000*0.5/100;
		}
		
		$calculated2 = 0;
		if($value<1500000000){
			
			if($value<750000000){
				$calculated2 = 0;
			}else{
				$calculated2 = ($value - 750000000)*0.3/100;
			}
			
		}else{
			$calculated2 = 750000000*0.3/100;
		}
		
		
		$calculated3 = 0;
		if($value>1500000000){
			$calculated3 = ($value-1500000000) * 0.1/100;
		}else{
			$calculated3 = 0;
		}
		
		
		return ($calculated1+$calculated2+$calculated3);
		
		
		
	}
    
    public function storeMonthlyCommission(Request $request)
    {

        
		request()->validate([
			'user_id' => 'required|integer|exists:users,id',
            'sale_amount' => 'required|numeric',
            'commission_month' => 'required|max:12',
            'commission_note' => 'nullable|max:1000',
			
            'customer_code' => 'nullable|max:20',
            'customer_name' => 'nullable|max:100',
            'sale_type' => 'required|max:15',
            'settlement_date' => 'nullable|max:15',
            'visitor_amount_percent' => 'required|integer',
            'specialist_amount_percent' => 'required|integer',
            
        ]);
		
		
		//check percents to be 100
        if(($request->visitor_amount_percent+$request->specialist_amount_percent)!=100){
			return redirect()->back()
                    ->withInput($request->input())
                    ->withErrors(['errors' => 'جمع مقادیر درصد حتما باید برابر با 100 باشد.']);
		}
        
        
        
		$commissionDate = $request->commission_month;
		$explodedCommissionDate = explode('/',$commissionDate);
		
		$commissionMonth = $explodedCommissionDate[0].'/'.$explodedCommissionDate[1];
		$invoiceFullJalaliDate = $explodedCommissionDate[0].'/'.$explodedCommissionDate[1].'/'.$explodedCommissionDate[2];
		
		
		
		$userCommissionMonthObj = CommissionMonth::where('user_id',$request->user_id)->where('commission_month',$commissionMonth)->first();
		if(empty($userCommissionMonthObj)){
			$userCommissionMonthObj = New CommissionMonth();
			
			$userCommissionMonthObj->commission_month = $commissionMonth;
			$userCommissionMonthObj->user_id = $request->user_id;
			$userCommissionMonthObj->save();
			
			$userCommissionMonthObj = CommissionMonth::where('user_id',$request->user_id)->where('commission_month',$commissionMonth)->first();
		}
		
		
		
		
		
		
		
		
		$specialistPercent = $request->specialist_amount_percent;
		$visitorPercent = $request->visitor_amount_percent;
		
		$fcommissionAmount = 0;			
		$specialistAmount = 0;
		$visitorAmount = 0;
		
		
		if($request->sale_type=='discount' || $request->sale_type=='internet'){
			
			$fcommissionAmount = 0;			
			$specialistAmount = 0;
			$visitorAmount = 0;
			
		}else if($request->sale_type=='returned'){
			
			$fcommissionAmount = ($request->sale_amount>0)?$this->calculateCommission($request->sale_amount):0;
			$fcommissionAmount = $fcommissionAmount*(-1);
			
			$specialistAmount = ($fcommissionAmount/100)*$specialistPercent;
			$specialistAmount = $specialistAmount*(-1);
			
			$visitorAmount = ($fcommissionAmount/100)*$visitorPercent;
			$visitorAmount = $visitorAmount*(-1);
			
		}else{
			
			$fcommissionAmount = ($request->sale_amount>0)?$this->calculateCommission($request->sale_amount):0;
			
			$specialistAmount = ($fcommissionAmount/100)*$specialistPercent;
			$visitorAmount = ($fcommissionAmount/100)*$visitorPercent;
			
		}
		
		
		$data = [];
        
        $data['creator_user_id'] = auth()->user()->id;
        $data['user_commission_month_id'] = $userCommissionMonthObj->id;
        $data['sale_amount'] = $request->sale_amount;
		$data['commission_amount'] = $fcommissionAmount;
		$data['commission_note'] = $request->commission_note;
		$data['customer_code'] = $request->customer_code;
		$data['customer_name'] = $request->customer_name;
		$data['sale_type'] = $request->sale_type;
		$data['settlement_date'] = $request->settlement_date;
		$data['visitor_amount_percent'] = $request->visitor_amount_percent;
		$data['specialist_amount_percent'] = $request->specialist_amount_percent;
		$data['visitor_amount'] = $visitorAmount;
		$data['specialist_amount'] = $specialistAmount;
		$data['invoice_date'] = $invoiceFullJalaliDate;
		
		
		
        
		Commission::create($data);
        
		
		
		$monthSumSaleAmount = Commission::where('user_commission_month_id',$userCommissionMonthObj->id)->sum('sale_amount');
		$monthCommissionAmount = Commission::where('user_commission_month_id',$userCommissionMonthObj->id)->sum('commission_amount');
		$monthCommissionVisitorAmount = Commission::where('user_commission_month_id',$userCommissionMonthObj->id)->sum('visitor_amount');
		$monthCommissionSpecialistAmount = Commission::where('user_commission_month_id',$userCommissionMonthObj->id)->sum('specialist_amount');
		//$monthCommissionAmount = ($monthSumSaleAmount>0)?$this->calculateCommission($monthSumSaleAmount):0;
		
		
		$userCommissionMonthObj->sum_sale_amount = $monthSumSaleAmount;
		$userCommissionMonthObj->commission_amount = $monthCommissionAmount;
		$userCommissionMonthObj->visitor_amount = $monthCommissionVisitorAmount;
		$userCommissionMonthObj->specialist_amount = $monthCommissionSpecialistAmount;
		
		$userCommissionMonthObj->save();
		
		
		
		
        $successMessage = 'اطلاعات فروش جدید با موفقیت افزوده شد.';
		
		return redirect()->route('commission.show-users-month-commission-list',['commission_month_id'=>$userCommissionMonthObj->id])
		    ->with('success', $successMessage);
		
		/*
		return redirect()->route('commission.show-users-monthly-commission-list',$request->user_id)
		    ->with('success', $successMessage);
			*/
		
		

    }
    
    
    
    
    
    
    
    public function userMonthCommissionsList(Request $request){
    	
    	
    	
    	$permittedUserIDs = [];
    	
    	$showAllSpecialists = false;
        if(auth()->user()->user_type=='SALES_SPECIALIST'){
            $permittedUserIDs = [auth()->user()->id];

        }else if(auth()->user()->user_type=='SALES_MANAGER'){
            $subUsers = auth()->user()->subUsers()->get();
            foreach($subUsers as $subUser){
                $permittedUserIDs[] = $subUser->id;
            }
			
			$permittedUserIDs[] = [auth()->user()->id];

        }else if(auth()->user()->user_type=='CSO'){
            $managerUsers = auth()->user()->subUsers()->get();
            foreach($managerUsers as $managerUser){
                foreach($managerUser->subUsers()->get() as $specialistUser){
                    $permittedUserIDs[] = $specialistUser->id;
                }
            }
			
			$permittedUserIDs[] = [auth()->user()->id];

        }else if(auth()->user()->user_type=='ADMIN'){
            $showAllSpecialists = true;
        }
		
		
		$commissionMonth = CommissionMonth::select('*')->where('id',$request->commission_month_id);
		
		if(!$showAllSpecialists){
			$commissionMonth = $commissionMonth->whereIn('user_id',$permittedUserIDs);
		}
		$commissionMonth = $commissionMonth->first();
		
		
		$commissions = Commission::where('user_commission_month_id',$commissionMonth->id)->orderBy('created_at','DESC')->get();
        
        
		
		$user = User::where('id',$commissionMonth->user_id)->first();
      

        return view('panel.admin.commission.user-month-commission-list', compact('commissions','commissionMonth','user'))
            ->with('i', ($request->input('page', 1) - 1) * 20);
    
    }
	
	
	
	public function userCommissionsMonthsList(Request $request){
    	
    	
		
		
		
		
		$fromDateGregorian = NULL;
        if(!empty($request->from_date)){
        
        	$fromDate = $request->from_date;
        	$fromDateGregorian=\Morilog\Jalali\Jalalian::fromFormat('Y/m/d', $fromDate)->toCarbon()->toDateTimeString();
        	
        
        }
		
		$toDateGregorian = NULL;
        if(!empty($request->to_date)){
        
        	$toDate = $request->to_date;
        	$toDateGregorian=\Morilog\Jalali\Jalalian::fromFormat('Y/m/d', $toDate)->toCarbon()->toDateTimeString();
        	
        
        }
		
		
		
    	
    	$permittedUserIDs = [];
    	
    	$showAllSpecialists = false;
        if(auth()->user()->user_type=='SALES_SPECIALIST'){
            $permittedUserIDs = [auth()->user()->id];

        }else if(auth()->user()->user_type=='SALES_MANAGER'){
            $subUsers = auth()->user()->subUsers()->get();
            foreach($subUsers as $subUser){
                $permittedUserIDs[] = $subUser->id;
            }
			
			$permittedUserIDs[] = [auth()->user()->id];

        }else if(auth()->user()->user_type=='CSO'){
            $managerUsers = auth()->user()->subUsers()->get();
            foreach($managerUsers as $managerUser){
                foreach($managerUser->subUsers()->get() as $specialistUser){
                    $permittedUserIDs[] = $specialistUser->id;
                }
            }
			
			$permittedUserIDs[] = [auth()->user()->id];

        }else if(auth()->user()->user_type=='ADMIN'){
            $showAllSpecialists = true;
        }
        
        
        
        $user = User::select('*')->where('user_type','SALES_MANAGER')->where('id',$request->user_id);
    	
    	if(!$showAllSpecialists){
    		$user = $user->whereIn('id',$permittedUserIDs);
    	}
    	
    	$user= $user->firstOrFail();
        
    	
    	
    	$data = CommissionMonth::select('*');
		
		if(!empty($fromDateGregorian)){
			$data = $data->whereDate('created_at' , '>=',$fromDateGregorian);
		}
		if(!empty($toDateGregorian)){
			$data = $data->whereDate('created_at' , '<=',$toDateGregorian);
		}
		
		
		$data = $data->where('user_id',$request->user_id);
    	$data = $data->orderBy('commission_month', 'DESC')->paginate(20);
    	
		

        return view('panel.admin.commission.show-user-commission-months-list', compact('data','user'))
            ->with('i', ($request->input('page', 1) - 1) * 20);
    
    }
	
	
	function editCommission(){
		abort(403);
	}
    
    
    
}
