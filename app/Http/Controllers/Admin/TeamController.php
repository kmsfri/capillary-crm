<?php


namespace App\Http\Controllers\Admin;


use App\Models\Team;

use App\Models\User;

use App\Models\Invoice;

use App\Models\Business;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class TeamController extends Controller

{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    function __construct()

    {

        $this->middleware('permission:team-management', ['only' => ['index', 'show','showTeamReport']]);

        $this->middleware('permission:team-management', ['only' => ['create', 'store']]);

        $this->middleware('permission:team-management', ['only' => ['edit', 'update']]);

        $this->middleware('permission:team-management', ['only' => ['destroy']]);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()

    {

        $teams = Team::latest()->paginate(15);

        return view('panel.admin.teams.index', compact('teams'))
            ->with('i', (request()->input('page', 1) - 1) * 5);

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()

    {

        return view('panel.admin.teams.create');

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)

    {

        request()->validate([

            'team_name' => 'required|max:191',
            'target_invoices_count' => 'required|integer',
            'target_invoices_amount' => 'required|integer',
            'target_businesses_count' => 'required|integer',
            'team_description' => 'required|max:1000',
            'date_from' => 'required|max:10',
            'date_to' => 'required|max:10',
            


        ]);
        
        
        $fromDateExploded = explode('/',$request->date_from);
        $fromDate = implode(
			'-',
			\Morilog\Jalali\CalendarUtils::toGregorian($fromDateExploded[0], $fromDateExploded[1], $fromDateExploded[2])
		);
		
	$toDateExploded = explode('/',$request->date_to);
        $toDate = implode(
			'-',
			\Morilog\Jalali\CalendarUtils::toGregorian($toDateExploded[0], $toDateExploded[1], $toDateExploded[2])
		);
        
        	
        
        
        $data = [
        	'team_name' => $request->team_name,
        	'target_invoices_count' => $request->target_invoices_count,
        	'target_invoices_amount' => $request->target_invoices_amount,
        	'target_businesses_count' => $request->target_businesses_count,
        	'team_description' => $request->team_description,
        	'date_from' => $fromDate,
        	'date_to' => $toDate,
        	'creator_id' => auth()->user()->id,
        	
        ];
        

        Team::create($data);


        return redirect()->route('teams.index')
            ->with('success', 'تیم جدید با موفقیت ایجاد شد.');

    }


    /**
     * Display the specified resource.
     *
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */

    public function show(Team $team)

    {

        return view('panel.admin.teams.show', compact('team'));

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */

    public function edit(Team $team)

    {

        return view('panel.admin.teams.edit', compact('team'));

    }


    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, Team $team)

    {
    
    
    
    	request()->validate([

            'team_name' => 'required|max:191',
            'target_invoices_count' => 'required|integer',
            'target_invoices_amount' => 'required|integer',
            'target_businesses_count' => 'required|integer',
            'team_description' => 'required|max:1000',
            'date_from' => 'required|max:10',
            'date_to' => 'required|max:10',
            


        ]);
        
        
        $fromDateExploded = explode('/',$request->date_from);
        $fromDate = implode(
			'-',
			\Morilog\Jalali\CalendarUtils::toGregorian($fromDateExploded[0], $fromDateExploded[1], $fromDateExploded[2])
		);
		
	$toDateExploded = explode('/',$request->date_to);
        $toDate = implode(
			'-',
			\Morilog\Jalali\CalendarUtils::toGregorian($toDateExploded[0], $toDateExploded[1], $toDateExploded[2])
		);
        
        	
        
        
        $data = [
        	'team_name' => $request->team_name,
        	'target_invoices_count' => $request->target_invoices_count,
        	'target_invoices_amount' => $request->target_invoices_amount,
        	'target_businesses_count' => $request->target_businesses_count,
        	'team_description' => $request->team_description,
        	'date_from' => $fromDate,
        	'date_to' => $toDate,
        	'creator_id' => auth()->user()->id,
        	
        ];
    
    
    
    


        $team->update($data);


        return redirect()->route('teams.index')
            ->with('success', 'تیم مورد نظر با موفقیت ویرایش شد');

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */

    public function destroy(Team $team)

    {

        $team->delete();


        return redirect()->route('teams.index')
            ->with('success', 'تیم مورد نظر با موفقیت حذف شد');

    }
    
    
    
    
    
    public function createTeamMembers(Request $request){
    
    
    
    	$team = Team::findOrFail($request->team_id);
    	$teamMembers = $team->members()->get()->pluck('id')->toArray();
    	
    	$users = User::where('user_type','SALES_MANAGER')->orWhere('user_type','SALES_SPECIALIST')->get();
    
    
        return view('panel.admin.teams.members.create-members',compact('team','teamMembers','users'));
    
    
    
    }  
    
    
    
    
    public function storeTeamMembers(Request $request){
    
    
    	request()->validate([

            'team_id' => 'required|integer|exists:teams,id',
            'member_user_id.*' => 'required|integer|exists:users,id',
            

        ]);
    
    
    
    	$team = Team::findOrFail($request->team_id);
    	
    	$team->members()->sync($request->member_user_id);
    	
    	
    	return redirect()->route('teams.index')
            ->with('success', 'تیم مورد نظر با موفقیت ویرایش شد');
    
    
    } 
	
	
	
	
	
	public function showTeamReport(Request $request){
		
		
		$team = Team::findOrFail($request->team_id);
		
		$targetInvoicesCount = $team->target_invoices_count;
		
		$targetInvoicesAmount = $team->target_invoices_amount;
		
		$targetBusinessesCount = $team->target_businesses_count;
		
		
		$teamMemberUserIDs = $team->members()->get()->pluck('id')->toArray();
		
		
		
		
		
		$data['registeredInvoicesCount'] = Invoice::whereIn('creator_id',$teamMemberUserIDs)->count();
		
		
		$registeredInvoicesAmount = 0;
		$registeredInvoices = Invoice::whereIn('creator_id',$teamMemberUserIDs)->get();
		foreach($registeredInvoices as $invc){
			
			$decodedInvoiceItems = json_decode($invc['invoice_items'],true);
			foreach($decodedInvoiceItems as $invcItem){
				$registeredInvoicesAmount = $registeredInvoicesAmount+$invcItem['item_total_price'];
			}
			
		}
		
		$data['registeredInvoicesAmount'] = $registeredInvoicesAmount;
		
		
		
		$data['registeredBusinessesCount'] = Business::whereIn('creator_id',$teamMemberUserIDs)->count();
		
		
		
		return view('panel.admin.teams.report', compact('data','team'));
		
		
		
		
		
	}
    
    
    

}
