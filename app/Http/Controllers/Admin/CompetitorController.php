<?php


namespace App\Http\Controllers\Admin;


use App\Models\BusinessCompetitor;

use App\Models\InstanceInvoice;

use App\Models\Country;

use App\Models\Product;

use App\Models\Brand;


use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Helpers;

use DB;

use App\Models\City;

class CompetitorController extends Controller

{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    function __construct()

    {

        $this->middleware('permission:competitor-list|competitor-create|competitor-edit|competitor-delete', ['only' => ['index', 'show']]);

        $this->middleware('permission:competitor-create', ['only' => ['create', 'store']]);

        $this->middleware('permission:competitor-edit', ['only' => ['edit', 'update']]);

        $this->middleware('permission:competitor-delete', ['only' => ['destroy']]);
		
		$this->middleware('permission:view-competitors-report', ['only' => ['showCompetitorsReport']]);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()

    {
		
		
		
		
		
		
		
 	
        $competitors = BusinessCompetitor::select('*');
 
        
        if(!empty($request->search_key)){
        
        	$searchKey = $request->search_key;
        
        	$competitors = $competitors->where(function($query) use($searchKey) {
				$query->where('competitor_name','LIKE', '%'.$searchKey.'%');
			});
		
		
        

        
        }
        
        
        $competitors = $competitors->orderBy('created_at','DESC')->paginate(20);
        
        

        return view('panel.admin.competitors.index', compact('competitors'))
            ->with('i', (request()->input('page', 1) - 1) * 25);

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()

    {
		
		$products = Product::all();
		$brands = Brand::all();
		$countries = Country::all();
	
		
        return view('panel.admin.competitors.create',compact('products','brands','countries'));

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)

    {
		

        request()->validate([

            'product_title' => 'required|max:191',
            'product_id' => 'required|integer|exists:products,id',
			'brand_id' => 'required|integer|exists:brands,id',
			'country_id' => 'required|integer|exists:countries,id',
			'company_price' => 'nullable|numeric',
			'market_price' => 'nullable|numeric',
			'checkout_period' => 'nullable|integer',
			'market_stock_count' => 'nullable|integer',
			
			'market_stock_percent' => 'nullable|integer',
			'manufacturer_stock_count' => 'nullable|integer',
			'manufacturer_stock_percent' => 'nullable|integer',
			'importer_stock_count' => 'nullable|integer',
			'importer_stock_percent' => 'nullable|integer',
			
        ]);
		
		
		
		


		$data = [
		
			
		
			'product_title' => $request->product_title,
			'product_id' => $request->product_id,
			
			'brand_id' => $request->brand_id,
			'country_id' => $request->country_id,
			'company_price' => $request->company_price,
			'market_price' => $request->market_price,
			'checkout_period' => $request->checkout_period,
			'market_stock_count' => $request->market_stock_count,
			'market_stock_percent' => $request->market_stock_percent,
			'manufacturer_stock_count' => $request->manufacturer_stock_count,
			'manufacturer_stock_percent' => $request->manufacturer_stock_percent,
			
			'importer_stock_count' => $request->importer_stock_count,
			'importer_stock_percent' => $request->importer_stock_percent,
			
			'created_by' => auth()->user()->id,
			
			
			
		
		];







        BusinessCompetitor::create($data);


        return redirect()->route('competitors.index')
            ->with('success', 'رکورد جدید با موفقیت اضافه شد.');

    }


    /**
     * Display the specified resource.
     *
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */

    public function show(BusinessCompetitor $competitor)

    {

        //return view('panel.admin.products.show', compact('product'));

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */

    public function edit(BusinessCompetitor $competitor)

    {
		
		$competitorID = $competitor->id;
		unset($competitor);
		
		
		$competitor = BusinessCompetitor::select('*');
        if(auth()->user()->user_type=='SALES_SPECIALIST'){
            $competitor = $competitor->where('created_by',auth()->user()->id);
        }
        $competitor = $competitor->where('id',$competitorID)->first();
		
		$products = Product::all();
		$brands = Brand::all();
		$countries = Country::all();
		

        return view('panel.admin.competitors.edit', compact('competitor','products','brands','countries'));

    }


    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, BusinessCompetitor $competitor)

    {

        request()->validate([

			'product_title' => 'required|max:191',
            'product_id' => 'required|integer|exists:products,id',
			'brand_id' => 'required|integer|exists:brands,id',
			'country_id' => 'required|integer|exists:countries,id',
			'company_price' => 'nullable|numeric',
			'market_price' => 'nullable|numeric',
			'checkout_period' => 'nullable|integer',
			'market_stock_count' => 'nullable|integer',
			
			'market_stock_percent' => 'nullable|integer',
			'manufacturer_stock_count' => 'nullable|integer',
			'manufacturer_stock_percent' => 'nullable|integer',
			'importer_stock_count' => 'nullable|integer',
			'importer_stock_percent' => 'nullable|integer',

        ]);
		

		
		
		$data = [
		
			'product_title' => $request->product_title,
			'product_id' => $request->product_id,
			
			'brand_id' => $request->brand_id,
			'country_id' => $request->country_id,
			'company_price' => $request->company_price,
			'market_price' => $request->market_price,
			'checkout_period' => $request->checkout_period,
			'market_stock_count' => $request->market_stock_count,
			'market_stock_percent' => $request->market_stock_percent,
			'manufacturer_stock_count' => $request->manufacturer_stock_count,
			'manufacturer_stock_percent' => $request->manufacturer_stock_percent,
			
			'importer_stock_count' => $request->importer_stock_count,
			'importer_stock_percent' => $request->importer_stock_percent,
			
			'last_updated_by' => auth()->user()->id,
			
			
		
		];



		


        $competitor->update($data);


        return redirect()->route('competitors.index')
            ->with('success', 'رکورد مورد نظر با موفقیت ویرایش شد.');

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */

    public function destroy(BusinessCompetitor $competitor)

    {

        $competitor->delete();


        return redirect()->route('competitors.index')
            ->with('success', 'رکورد مورد نظر با موفقیت حذف شد.');

    }
	
	
	
	
	
	
	
	
	
	
	
	public function showCompetitorsReport(Request $request){
    
	
		$competitors = BusinessCompetitor::select('*')->where('id','>',0);
	
	
	
		if(!empty($request->from_date)){
        
        	$fromDate = $request->from_date;
        	$fromDateGregorian=\Morilog\Jalali\Jalalian::fromFormat('Y/m/d', $fromDate)->toCarbon()->toDateTimeString();
        	
        	$competitors = $competitors->whereDate('created_at' , '>=',$fromDateGregorian);
			
        	
        	$fromDateJalali = $request->from_date;
        
        }else{
        	$minDate = BusinessCompetitor::select(DB::raw('min(created_at) AS min_created_at'))->first();
        	$fromDateGregorian = $minDate->min_created_at;
        	$fromDateJalali = \Morilog\Jalali\Jalalian::forge($fromDateGregorian)->format('%Y/%m/%d');
        }
        
        if(!empty($request->to_date)){
        
        	$toDate = $request->to_date;
        	$toDateGregorian=\Morilog\Jalali\Jalalian::fromFormat('Y/m/d', $toDate)->toCarbon()->toDateTimeString();
        
        	$competitors = $competitors->whereDate('created_at' , '<=',$toDateGregorian);
			
			
			
        	$toDateJalali = $request->to_date;
        
        }else{
        
        	$maxDate =  BusinessCompetitor::select(DB::raw('max(created_at) AS max_created_at'))->first();
        	$toDateGregorian = $maxDate->max_created_at;
        	$toDateJalali = \Morilog\Jalali\Jalalian::forge($toDateGregorian)->format('%Y/%m/%d');
        
        }
		
		
		
		
		
		if(!empty($request->product_id)){
        
        
        	$competitors = $competitors->where('product_id' , $request->product_id);
			
        
        }
		
		
		if(!empty($request->brand_id)){
        
        
        	$competitors = $competitors->where('brand_id' , $request->brand_id);
			
        
        }
		
		
		if(!empty($request->country_id)){
        
        
        	$competitors = $competitors->where('country_id' , $request->country_id);
			
        
        }
		
		
		
		
		
		
		//BEGIN: calculate date periods
        $datePeriods = [];
        if($request->report_period==1){//Daily
        
			
			$explodedDate = explode('/',$toDateJalali);
			$toCheckDate = $explodedDate[0].$explodedDate[1].$explodedDate[2];
			$fromHoldYear = NULL;
			$fromHoldMonth = NULL;
			$fromHoldDay = NULL;
			do{
				
				if(empty($fromHoldYear)){
					$explodedDate = explode('/',$fromDateJalali);
					$fromHoldYear = $explodedDate[0];
					$fromHoldMonth = $explodedDate[1]; 
					$fromHoldDay = $explodedDate[2]; 
				}else{
					if($fromHoldDay<=30){
						$fromHoldDay++;
					}else if($fromHoldMonth<=11){
						$fromHoldDay=1;
						$fromHoldMonth++;
					}else{
						$fromHoldDay=1;
						$fromHoldMonth = 1;
						$fromHoldYear++;
					}
				}
				
				
				if($fromHoldMonth<10){
					$fromHoldMonth = "0".((int)$fromHoldMonth);
				}
				
				if($fromHoldDay<10){
					$fromHoldDay = "0".((int)$fromHoldDay);
				}
				
				$datePeriods[] = $fromHoldYear."/".$fromHoldMonth."/".$fromHoldDay;
				
				
			
			
			}while($toCheckDate>($fromHoldYear.$fromHoldMonth.$fromHoldDay));
        
        
		
		}else{//Monthly
	
			$explodedDate = explode('/',$toDateJalali);
			$toCheckDate = $explodedDate[0].$explodedDate[1];
			
			$fromHoldYear = NULL;
			$fromHoldMonth = NULL;
			do{
				
				if(empty($fromHoldYear)){
					$explodedDate = explode('/',$fromDateJalali);
					$fromHoldYear = $explodedDate[0];
					$fromHoldMonth = $explodedDate[1]; 
				}else{
					if($fromHoldMonth<=11){
						$fromHoldMonth++;
					}else{
						$fromHoldMonth = 1;
						$fromHoldYear++;
					}
				}
				
				if($fromHoldMonth<10){
					$fromHoldMonth = "0".((int)$fromHoldMonth);
				}
				$datePeriods[] = $fromHoldYear."/".$fromHoldMonth;
				
				
			
			
			}while($toCheckDate>=$fromHoldYear.$fromHoldMonth);
		
		}
		//END: calculate date periods

		
		
		
		
		
		
		
		
		$chart1DataArray = []; //based on company_price
		$chart2DataArray = []; //based on market_price
		$chart3DataArray = []; //based on market_stock_percent
		$chart4DataArray = []; //based on manufacturer_stock_percent
		$chart5DataArray = []; //based on importer_stock_percent
		
		
		
		foreach($datePeriods as $key=>$datePeriod){
		
			$fromDateJalali = NULL;
			$toDateJalali = NULL;
			if($request->report_period==1){//Daily
				$fromDateJalali = $datePeriod;
				$toDateJalali = $datePeriod;
			}else{
				$fromDateJalali = $datePeriod."/01";
				$toDateJalali = $datePeriod."/31";
			}
			
			$fromDateGregorian=\Morilog\Jalali\Jalalian::fromFormat('Y/m/d', $fromDateJalali)->toCarbon()->toDateTimeString();
			$toDateGregorian=\Morilog\Jalali\Jalalian::fromFormat('Y/m/d', $toDateJalali)->toCarbon()->toDateTimeString();
			
			
			$tempObj = clone $competitors;
			
			$periodDate = $tempObj->whereDate('created_at' , '>=',$fromDateGregorian)->whereDate('created_at' , '<=',$toDateGregorian);
			
			$periodCount = $periodDate->avg('company_price');
			
			if($periodCount>0){
				$chart1DataArray[$datePeriod] = $periodCount;
			}else{
				$chart1DataArray[$datePeriod] = 0;
			}
			
			
			
			
			
			
			$tempObj = clone $competitors;
			
			$periodDate = $tempObj->whereDate('created_at' , '>=',$fromDateGregorian)->whereDate('created_at' , '<=',$toDateGregorian);
			$periodCount = $periodDate->avg('market_price');
			
			if($periodCount>0){
				$chart2DataArray[$datePeriod] = $periodCount;
			}else{
				$chart2DataArray[$datePeriod] = 0;
			}
			
			
			
			$tempObj = clone $competitors;
			
			$periodDate = $tempObj->whereDate('created_at' , '>=',$fromDateGregorian)->whereDate('created_at' , '<=',$toDateGregorian);
			$periodCount = $periodDate->avg('market_stock_percent');
			
			if($periodCount>0){
				$chart3DataArray[$datePeriod] = $periodCount;
			}else{
				$chart3DataArray[$datePeriod] = 0;
			}
			
			
			
			
			$tempObj = clone $competitors;
			
			$periodDate = $tempObj->whereDate('created_at' , '>=',$fromDateGregorian)->whereDate('created_at' , '<=',$toDateGregorian);
			$periodCount = $periodDate->avg('manufacturer_stock_percent');
			
			if($periodCount>0){
				$chart4DataArray[$datePeriod] = $periodCount;
			}else{
				$chart4DataArray[$datePeriod] = 0;
			}
			
			
			
			$tempObj = clone $competitors;
			
			$periodDate = $tempObj->whereDate('created_at' , '>=',$fromDateGregorian)->whereDate('created_at' , '<=',$toDateGregorian);
			$periodCount = $periodDate->avg('importer_stock_percent');
			
			if($periodCount>0){
				$chart5DataArray[$datePeriod] = $periodCount;
			}else{
				$chart5DataArray[$datePeriod] = 0;
			}
			
			
			
		
		}
		
		
		
		
		
		
		$products = Product::all();
		$brands = Brand::all();
		$countries = Country::all();
		
		
		
		
		
		return view('panel.admin.competitors.report', compact('competitors','chart1DataArray','chart2DataArray','chart3DataArray','chart4DataArray','chart5DataArray','products','brands','countries'));
		
		
		
		
		
		
		
		
		
		
	
	
	}
    
    
    
    
}
