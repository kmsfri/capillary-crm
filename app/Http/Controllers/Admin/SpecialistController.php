<?php


namespace App\Http\Controllers\Admin;


use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Models\User;

use App\Models\Business;

use App\Models\Customer;

use App\Models\SpecialistCallTarget;

use App\Models\CallTargetAlarm;

use App\Models\Clue;

use App\Models\Invoice;

use App\Models\Note;

use Spatie\Permission\Models\Role;

use DB;

use Hash;

use Illuminate\Support\Arr;

use Helpers;


class SpecialistController extends Controller

{

    function __construct()

    {

        $this->middleware('permission:specialists-list', ['only' => ['index', 'show']]);
        
        $this->middleware('permission:show-call-targets-list', ['only' => ['showCallTargetsList']]);
        
        $this->middleware('permission:specialist-edit-call-targets', ['only' => ['createCallTarget','editCallTargets','updateCallTargets']]);
        
        $this->middleware('permission:specialist-show-call-target-details', ['only' => ['showCallTargetDetails']]);
        
        $this->middleware('permission:view-voip-calls-report', ['only' => ['showVoipCallsReport']]);
        
        $this->middleware('permission:access-daily-call-targets-list-for-specialists', ['only' => ['showSpecialistDailyCallTargetsList']]);
		
        $this->middleware('permission:access-to-alarms-cartable', ['only' => ['showAlarmsList','seenAlarm','showSetAlarmFormForCustomerCallTarget','storeAlarmForCustomerCallTarget']]); 
        
        
        

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)

    {

        $permittedUserIDs = [];
    	
    	$showAllSpecialists = false;
        if(auth()->user()->user_type=='SALES_SPECIALIST'){
            $permittedUserIDs = [auth()->user()->id];

        }else if(auth()->user()->user_type=='SALES_MANAGER'){
            $subUsers = auth()->user()->subUsers()->get();
            foreach($subUsers as $subUser){
                $permittedUserIDs[] = $subUser->id;
            }

        }
        /*else if(auth()->user()->user_type=='CSO'){
            $managerUsers = auth()->user()->subUsers()->get();
            foreach($managerUsers as $managerUser){
                foreach($managerUser->subUsers()->get() as $specialistUser){
                    $permittedUserIDs[] = $specialistUser->id;
                }
            }

        }
        */
        else if(auth()->user()->user_type=='ADMIN' || auth()->user()->user_type=='CSO'){
            $showAllSpecialists = true;
        }
    
    
    	$data = User::select('*')->where('user_type','SALES_MANAGER');
    	
    	if(!$showAllSpecialists){
    		$data = $data->whereIn('id',$permittedUserIDs);
    	}
    	
    	$data = $data->orderBy('id', 'DESC')->paginate(20);

        return view('panel.admin.specialists.index', compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 20);

    }
    
    
    
    
    public function showCallTargetsList(Request $request){
    	//TODO: set limit to access just permitted IDs
    	
    	
    	$specialistUser = User::findOrFail($request->user_id);
    	$data = SpecialistCallTarget::where('specialist_user_id',$request->user_id)->orderBy('target_date', 'DESC')->paginate(20);
    	
    	$dataArray = [];
    	foreach($data as $callTarget){
    	
    	
    		
    		//BEGIN: CALCULATE VOIP CALLS COUNT
    		$callTargetBusinessIDs = json_decode($callTarget->target_calls_list,true);
    		$phoneNumbers = [];
    		foreach($callTargetBusinessIDs as $businessID){
    		
    			$business = Business::findOrFail($businessID);

    			
    			if(!empty($business->phone_number) && mb_strlen($business->phone_number,'utf-8')>5){
    				$phoneNumbers[] = (int)(Helpers::convertFaToEnNumbersV2($business->phone_number));  //(int) for remove leading zero
    			}
    			
    			$mobiles = json_decode($business->mobiles,true);
    			foreach($mobiles as $mobile){
    				if(!empty($mobile) && mb_strlen($mobile,'utf-8')>5){
    					$phoneNumbers[] = (int)(Helpers::convertFaToEnNumbersV2($mobile)); //(int) for remove leading zero
    				}
    				
    			}
    			
    		
    		}
    		
    		
    		$specialistUser = User::findOrFail($callTarget->specialist_user_id);
    		
    		
    		
    		$getCallsQuery = 'select count(*) AS cnt from cdr where src='.$specialistUser->voip_id." AND disposition='ANSWERED' AND DATE(calldate) = '".$callTarget->target_date."' AND dst REGEXP '";
    		foreach($phoneNumbers as $key => $phoneNumber){
    			if($key>0){
    				$getCallsQuery = $getCallsQuery.'|';
    			}
    			$getCallsQuery = $getCallsQuery.$phoneNumber;
    		}
    		$getCallsQuery = $getCallsQuery."'";
    		

    		$callsCount = DB::connection('mysql_voip')->select($getCallsQuery);
    		$callsCount = $callsCount[0]->cnt;
    		//END: CALCULATE VOIP CALLS COUNT
    		
    		$callTarget['voip_calls_count'] = $callsCount;
    	
    	
    	
    	
    	
    		$dataArray[] = [
    			'id' => $callTarget->id,
    			'specialist_user_id' => $callTarget->specialist_user_id,
    			'creator_user_id' => $callTarget->creator_user_id,
    			'target_date' => $callTarget->target_date,
    			'target_calls_list' => $callTarget->target_calls_list,
    			'target_call_description' => $callTarget->target_call_description,
    			'created_at' => $callTarget->created_at,
    			'updated_at' => $callTarget->updated_at,
    		];
    	}
    	
    	
    	return view('panel.admin.specialists.call_targets.index', compact('data','specialistUser'))
            ->with('i', ($request->input('page', 1) - 1) * 20);
    }
    
    
    
    
    public function createCallTarget(Request $request){
    
    
    	$specialistUser = User::findOrFail($request->user_id);
    	
    	
    	
    	
    	
    	
    	$callTarget = Null;
    	$callTargetAt = Null;
    	//$callTargetFlowChartOperatorsCount = 0;
    	
    	//get count of default flowchart operators
    	if(!empty($request->target_date)){
    		$callTargetAt = $request->target_date;
    		$callTarget = SpecialistCallTarget::where('specialist_user_id',$specialistUser->id)->where('target_date',$request->target_date)->first();
    		
    		$callTarget->target_calls_list = json_decode($callTarget->target_calls_list);
    	}
    	
    
    
    	$permittedConditions = Helpers::getBusinessesAccessPermittedConditions(auth()->user());

        $businesses = Business::select('*')->where('id','<',0);

        foreach($permittedConditions as $key=>$condition){
            $toFireCondition1 = [];
            if(!empty($condition['ignore_cities']) && $condition['ignore_cities']){
                $toFireCondition1 = [
                    'operand'=>'LIKE',
                    'values' => '%'
                ];
            }else{
                $toFireCondition1 = [
                    'operand'=>'IN',
                    'values' => $condition['city_ids']
                ];
            }

            $toFireCondition2 = [];
            if(!empty($condition['ignore_creator_id']) && $condition['ignore_creator_id']){
                $toFireCondition2 = [
                    'operand'=>'LIKE',
                    'values' => '%'
                ];
            }else{
                $toFireCondition2 = [
                    'operand'=>'=',
                    'values' => $condition['creator_id']
                ];
            }
            $businesses = $businesses->orWhere(function($query) use($toFireCondition1,$toFireCondition2) {
                if($toFireCondition1['operand']=='IN'){
                    $query->whereIn('city_id', $toFireCondition1['values'])->where('creator_id',$toFireCondition2['operand'],$toFireCondition2['values']);
                }else{
                    $query->where('city_id',$toFireCondition1['operand'], $toFireCondition1['values'])->where('creator_id',$toFireCondition2['operand'],$toFireCondition2['values']);
                }
            });
        }

        $businesses = $businesses->orderBy('created_at','DESC')->get();
        
        
        return view('panel.admin.specialists.call_targets.create-call-target',compact('businesses','specialistUser','callTargetAt','callTarget'));
    
    
    
    }
    
    
    
    
    
    
    public function storeCallTarget(Request $request){
    
    	request()->validate([
	    'specialist_user_id' => 'required|integer|exists:users,id',
            'business_id.*' => 'required|integer|exists:businesses,id',
            'target_date' => 'required_without:call_target_id|max:10',
            'target_call_description' => 'nullable|max:1000',
            'call_target_id' => 'nullable|integer|exists:specialist_call_targets,id'
        ]);
        
        
        
        
        
        $data['specialist_user_id'] = $request->specialist_user_id;
        $data['creator_user_id'] = auth()->user()->id;
        $data['target_calls_list'] = json_encode($request->business_id);
        $data['target_call_description'] = $request->target_call_description;

        
        
        
        
        



	$errorMessage = Null;
	$successMessage = Null;
	if(!empty($request->call_target_id)){
		$callTarget = SpecialistCallTarget::findOrFail($request->call_target_id);
		$callTarget->target_calls_list = json_encode($request->business_id);
		$callTarget->target_call_description = $request->target_call_description;
		
		$callTarget->save();
		
		$successMessage = 'لیست تارگت تماسهای روزانه با موفقیت ویرایش شد.';
        }else{
        
        	$targetDate = $request->target_date;
        	$targetDataExploded = explode('/',$request->target_date);
        
        	$data['target_date'] = implode(
			'-',
			\Morilog\Jalali\CalendarUtils::toGregorian($targetDataExploded[0], $targetDataExploded[1], $targetDataExploded[2])
		);
        
        	//check date
        	$targetCallExists = SpecialistCallTarget::where('specialist_user_id',$request->specialist_user_id)->where('target_date',$data['target_date'])->exists();
        	if($targetCallExists){
        		$errorMessage = "قبلا برای این کارشناس در تاریخ انتخاب شده، تارگت تماس روزانه تعریف شده است.";
        	}else{
        	
        		SpecialistCallTarget::create($data);
        	
        	
        		$successMessage = 'لیست تارگت تماس روزانه برای کارشناس مورد نظر با موفقیت ایجاد شد.';
        	}
        	
        	
        
        }
        

        

	if(!empty($errorMessage)){
		return back()->with('danger', $errorMessage);
	}else{
		return redirect()->route('specialists.show-call-targets-list',$request->specialist_user_id)
		    ->with('success', $successMessage);
	}
    
    
    
    }
    
    
    
    
    public function showCallTargetDetails(Request $request){
    	
    	
    	//TODO: just permitted date shoud be show
    	
    	$callTarget = SpecialistCallTarget::where('id',$request->call_target_id)->first();
    	
    	$targetDate = \Morilog\Jalali\Jalalian::forge($callTarget->target_date)->format('%A, %d %B %Y');
    	
    	$specialistUser = User::where('id',$callTarget->specialist_user_id)->first();
    	
    	$data['target_title'] = 'لیست تماسهای کاربر: '.$specialistUser->name.' در تاریخ: '.$targetDate;
    	
    	$targetBusinesses = json_decode($callTarget->target_calls_list);
    	$businesses = Business::whereIn('id',$targetBusinesses)->get();
    	
    	
    	foreach($businesses as $key=>$business){
    	
    	
    		//BEGIN: CALCULATE VOIP CALLS COUNT
    		$callTargetBusinessIDs = json_decode($callTarget->target_calls_list,true);
    		
    		$phoneNumbers = [];
    		if(!empty($business->phone_number) && mb_strlen($business->phone_number,'utf-8')>5){
			$phoneNumbers[] = (int)(Helpers::convertFaToEnNumbersV2($business->phone_number));  //(int) for remove leading zero
		}
		
		$mobiles = json_decode($business->mobiles,true);
		foreach($mobiles as $mobile){
			if(!empty($mobile) && mb_strlen($mobile,'utf-8')>5){
				$phoneNumbers[] = (int)(Helpers::convertFaToEnNumbersV2($mobile)); //(int) for remove leading zero
			}
			
		}
    		
    		$getCallsQuery = 'select sum(duration) as sum_durations from cdr where src='.$specialistUser->voip_id." AND disposition='ANSWERED' AND DATE(calldate) = '".$callTarget->target_date."' AND dst REGEXP '";
    		
    		foreach($phoneNumbers as $key => $phoneNumber){
    			if($key>0){
    				$getCallsQuery = $getCallsQuery.'|';
    			}
    			$getCallsQuery = $getCallsQuery.$phoneNumber;
    		}
    		$getCallsQuery = $getCallsQuery."'";

    		$voipCalls = DB::connection('mysql_voip')->select($getCallsQuery);
    		$sumCallDurations = $voipCalls[0]->sum_durations;
    		//END: CALCULATE VOIP CALLS COUNT
    		
    		
    		$business->voip_sum_call_durations = !empty($sumCallDurations)?$sumCallDurations:0;
    	
    	
    	
    	}

    	
    	
    	
    	
    	
    	
    	
    	$data['target_businesses'] = $businesses;
    	
    	
    	
    	return view('panel.admin.specialists.call_targets.call-target-details',compact('data'));
    	
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()

    {



    }


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)

    {


    }


    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */

    public function show($id)

    {


    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */

    public function edit($id)

    {


    }


    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)

    {


    }


    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)

    {

    }
    
    
    
    
    public function showSpecialistDailyCallTargetsList(Request $request){
    
    
    	$now = new \DateTime();
	$today = $now->format('Y-m-d');

    	$callTarget = SpecialistCallTarget::where('specialist_user_id',auth()->user()->id)->where('target_date',$today)->first();
    	
    	$specialistUser = User::where('id',auth()->user()->id)->first();
    	
    	$targetDate = \Morilog\Jalali\Jalalian::forge($today)->format('%A, %d %B %Y');
    	
    	$data['target_title'] = 'لیست تماسهای کاربر: '.$specialistUser->name.' در تاریخ: '.$targetDate;
    	
    	$data['target_businesses'] = Null;
    	
    	$callTargetID = Null;
    	
    	if(!empty($callTarget)){
    	
	    	
	    	
	    	
	    	$targetBusinesses = json_decode($callTarget->target_calls_list);
	    	$data['target_businesses'] = Business::whereIn('id',$targetBusinesses)->get();
	    	
	    	$callTargetID = $callTarget->id;
    	}
    	return view('panel.admin.specialists.call_targets.specialist-daily-call-targets-list',compact('data','callTargetID'));
    
    }
    
    
    
    public function showSetAlarmFormForCustomerCallTarget(Request $request){
    
    	
    	$callTargetID = $request->call_target_id;
    	$businessID = $request->business_id;
    	$customerID = $request->customer_id;
    	$clueID = $request->clue_id;
    	
    	/*
    	if(empty($callTargetID) || empty($businessID)){
    		abort(404);
    	}
    	*/
    	
    	$pageTitle = "ایجاد آلارم جدید ";
    	
    	$callTarget = Null;
    	if(!empty($callTargetID)){
    		$callTarget = SpecialistCallTarget::where('specialist_user_id',auth()->user()->id)
    					->where('id',$callTargetID)->firstOrFail();
    	}
    	
    					
    	$business = Null;			
    	if(!empty($businessID)){
    		$business = Business::findOrFail($businessID);
    		
    		$pageTitle .= "برای کسب و کار ".$business->business_name."(".$business->first_name." ".$business->last_name.")";
    		
    	}
    	
    	
    	$customer = Null;			
    	if(!empty($customerID)){
    		$customer = Customer::findOrFail($customerID);
    		
    		$pageTitle .= "برای مشتری ".$customer->business_name."(".$customer->first_name." ".$customer->last_name.")";
    	}
		
		
		$clue = Null;			
    	if(!empty($clueID)){
    		$clue = Clue::findOrFail($clueID);
    		
    		$pageTitle .= "برای سرنخ ".$clue->business_name."(".$clue->first_name." ".$clue->last_name.")";
    	}
    					
    					
    	return view('panel.admin.specialists.call_targets.create-call-target-alarm',compact('callTargetID','business','customer','pageTitle','clue'));
    	
    }
    
    public function storeAlarmForCustomerCallTarget(Request $request){
    
    	request()->validate([
	    'alarm_at' => 'required|max:20',
	    'call_target_id' => 'nullable|exists:specialist_call_targets,id',
	    'business_id' => 'nullable|exists:businesses,id',
	    'customer_id' => 'nullable|exists:customers,id',
	    'clue_id' => 'nullable|exists:clues,id',
	    'alarm_description' => 'nullable|max:1024',
        ]);
        
        $callTarget = Null;
		if(!empty($request->call_target_id)){
			$callTarget = SpecialistCallTarget::findOrFail($request->call_target_id);
		}
    	
    	
    	
    	
    	$explodedAlarmAt = explode(' ',$request->alarm_at);
    	$explodedAlarmAtDate = explode("/",$explodedAlarmAt[0]);
    	
    	$alarmDateGregorian = \Morilog\Jalali\CalendarUtils::toGregorian($explodedAlarmAtDate[0], $explodedAlarmAtDate[1], $explodedAlarmAtDate[2]);
    	
    	if(strlen($alarmDateGregorian[1])==1){
    		$alarmDateGregorian[1] = "0".$alarmDateGregorian[1];
    	}
    	
    	if(strlen($alarmDateGregorian[2])==1){
    		$alarmDateGregorian[2] = "0".$alarmDateGregorian[2];
    	}
    	
    	$gregorianAlarmFullDateTime = implode("-",$alarmDateGregorian)." ".$explodedAlarmAt[1];
    
    	$specialistUserID = Null;
    	if(!empty($callTarget)){
    		$specialistUserID = $callTarget->specialist_user_id;
    	}else{
    		$specialistUserID = auth()->user()->id;
    	}
    	
    	
    	
    
    	$data = [
    		'call_target_id' => !empty($callTarget)?$callTarget->id:Null,
    		'specialist_user_id' => $specialistUserID,
    		'business_id' => $request->business_id,
    		'customer_id' => $request->customer_id,
    		'clue_id' => $request->clue_id,
    		'alarm_at' => $gregorianAlarmFullDateTime,
    		'alarm_description' => $request->alarm_description,
    		'is_seen' => 0,
    		
    	];
    	
    	
    	
    	CallTargetAlarm::create($data);
     
    
		if(!empty($request->return_url)){
			return redirect($request->return_url)
				->with('success', 'آلارم با موفقیت ایجاد شد');
		}else{
			return redirect()->route('specialists.showAlarmsList')
				->with('success', 'آلارم با موفقیت ایجاد شد');
		}
		
    	
    	
    	
    	
    
    }
	
	
	
	
	public function showAlarmsList(Request $request){
		
	
		$fromDate = Null;
		if(empty($request->from_date)){
			$request->from_date = $date = \Morilog\Jalali\Jalalian::forge('now - 30 days')->format('%Y/%m/%d');
		}
		
		
		$fromDateGregorian=\Morilog\Jalali\Jalalian::fromFormat('Y/m/d', $request->from_date)->toCarbon()->toDateTimeString();
		
		
		
		$data = \App\Models\CallTargetAlarm::where('specialist_user_id',auth()->user()->id)->where('alarm_at' , '>=',$fromDateGregorian);
		
		
		
		
		if(!empty($request->to_date)){
			$toDateGregorian=\Morilog\Jalali\Jalalian::fromFormat('Y/m/d', $request->to_date)->toCarbon()->toDateTimeString();	
			$data = $data->where( 'alarm_at', '<=', $toDateGregorian );
		}
		
		
		
		
		
		
		
		
		$todayBegin = date('Y-m-d')." 00:00:00";
		$todayEnd = date('Y-m-d')." 23:59:59";
		
		$todayData = \App\Models\CallTargetAlarm::where('specialist_user_id',auth()->user()->id)->where('alarm_at' , '>=',$todayBegin);
		
		$todayData = $todayData->where( 'alarm_at', '<=', $todayEnd );
		
		
		
		
		
		
		
		
		
		
		if(isset($request->seen_type) && $request->seen_type!=""){
        
			
			$data = $data->where('is_seen' ,$request->seen_type);
			
			$todayData = $todayData->where('is_seen' ,$request->seen_type);
			
			
		}
		
		if(!empty($request->overdue)){
        
        		$operator = '';
			if($request->overdue==1){
			
				$operator = '>';
			
			
			}elseif($request->overdue==2){
			
				$operator = '<=';
				
			}
			
			
			$data = $data->whereDate('alarm_at', $operator, date('Y-m-d H:i:s'));
			$todayData = $todayData->whereDate('alarm_at', $operator, date('Y-m-d H:i:s'));
			
			
			
			
		}
		
		if(!empty($request->alarm_description)){
        
			
			$data = $data->where('alarm_description','LIKE' ,'%'.$request->alarm_description.'%');
			$todayData = $todayData->where('alarm_description','LIKE' ,'%'.$request->alarm_description.'%');
			
			
		}
		
		$data = $data->orderByRaw('ABS (alarm_at - NOW()) ASC')->get();
		
		$todayData = $todayData->orderByRaw('ABS (alarm_at - NOW()) ASC')->get();
		
		return view('panel.admin.specialists.call_targets.alarms',compact('data','todayData'));
		
	}
	
	
	
	public function seenAlarm(Request $request){
		
		//TODO: validate own alarm_id
		
		
		$callTargetAlarms = \App\Models\CallTargetAlarm::findOrFail($request->alarm_id);
		
		$callTargetAlarms->is_seen = 1;
		$callTargetAlarms->save();
		
		
		return redirect()->back()
		    ->with('success', "وضعیت آلارم مورد نظر تغییر پیدا کرد.");
		
	}
	
	public function seenAlarmInNotifications(Request $request){
		
		//TODO: validate own alarm_id
		
		
		$callTargetAlarms = \App\Models\CallTargetAlarm::findOrFail($request->alarm_id);
		
		$callTargetAlarms->is_seen_in_notifications = 1;
		$callTargetAlarms->save();
		
		
		return true;
		
	}
	
	
	
	
	
	
	
	public function showVoipCallsReport(Request $request){
	
	
	
		$voipSpecialistUsers = User::whereNotNull('voip_id')->orderBy('id','DESC')->get();
		
		$voipSpecialistUserIDs = $voipSpecialistUsers->pluck('voip_id')->toArray();
	
	
    		$getCallsQuery = DB::connection('mysql_voip')->table('cdr');
    		
    		
    		$getCallsQuery = $getCallsQuery->where(function($query) use($voipSpecialistUserIDs){
						    $query->whereIn('src', $voipSpecialistUserIDs)->orWhereIn('dst',$voipSpecialistUserIDs);
						});
    		
    		
		if(!empty($request->from_date)){
			$fromDateGregorian=\Morilog\Jalali\Jalalian::fromFormat('Y/m/d', $request->from_date)->toCarbon()->toDateTimeString();
			
			$getCallsQuery=$getCallsQuery->whereDate('callDate','>=',$fromDateGregorian);
		}
		
		
		
		if(!empty($request->to_date)){
			$toDateGregorian=\Morilog\Jalali\Jalalian::fromFormat('Y/m/d', $request->to_date)->toCarbon()->toDateTimeString();
			$getCallsQuery=$getCallsQuery->whereDate('callDate','<=',$toDateGregorian);
		}
		
		
		if(!empty($request->src_number)){
			$getCallsQuery=$getCallsQuery->where('src',$request->src_number);
		}
		
		if(!empty($request->dst_number)){
			$getCallsQuery=$getCallsQuery->where('dst','LIKE','%'.$request->dst_number.'%');
		}
		
		if(!empty($request->disposition)){
			$getCallsQuery=$getCallsQuery->where('disposition',$request->disposition);
		}
		
		
		$getCallsQuery=$getCallsQuery->orderBy('calldate','DESC');
    		
    		
    		$callsReportData=$getCallsQuery->paginate(20);
    	
    		
    		
    		return view('panel.admin.specialists.call_targets.voip-calls-report',compact('voipSpecialistUsers','callsReportData'));
    		
    		
	
	}
	
	
	
	
	
	
	
	
	
	public function showSpecialistReport(Request $request){
	
		
		$specialistUser = User::where('id',$request->specialist_user_id)->where('user_type','SALES_MANAGER');
		
		if(auth()->user()->user_type=='SALES_MANAGER'){
			$specialistUser = $specialistUser->where('id',auth()->user()->id);
		}
		
		$specialistUser = $specialistUser->firstOrFail();
		
		
		$subUsersCount = User::where('id',$specialistUser->id)->first()->subUsers()->count();
		
		
		
		$fromDateGregorian = NULL;
        if(!empty($request->from_date)){
        
        	$fromDate = $request->from_date;
        	$fromDateGregorian=\Morilog\Jalali\Jalalian::fromFormat('Y/m/d', $fromDate)->toCarbon()->toDateTimeString();
        	
        	//$chart1Data = $chart1Data->whereDate('created_at' , '>=',$fromDateGregorian);
        	
        	//$chart2Data = $chart2Data->whereDate('created_at' , '>=',$fromDateGregorian);
        	
        	
        	//$fromDateJalali = $request->from_date;
        
        }
		
		$toDateGregorian = NULL;
        if(!empty($request->to_date)){
        
        	$toDate = $request->to_date;
        	$toDateGregorian=\Morilog\Jalali\Jalalian::fromFormat('Y/m/d', $toDate)->toCarbon()->toDateTimeString();
        	
        	//$chart1Data = $chart1Data->whereDate('created_at' , '>=',$fromDateGregorian);
        	
        	//$chart2Data = $chart2Data->whereDate('created_at' , '>=',$fromDateGregorian);
        	
        	
        	//$fromDateJalali = $request->from_date;
        
        }
		
		
		
		$clues = Clue::where('creator_user_id',$specialistUser->id);
		if(!empty($fromDateGregorian)){
			$clues = $clues->whereDate('created_at' , '>=',$fromDateGregorian);
		}
		if(!empty($toDateGregorian)){
			$clues = $clues->whereDate('created_at' , '<=',$toDateGregorian);
		}
		$clues = $clues->orderBy('created_at','DESC')->paginate(20,['*'],'clues_page');
		
		$invoices = Invoice::where('creator_id',$specialistUser->id);
		if(!empty($fromDateGregorian)){
			$invoices = $invoices->whereDate('created_at' , '>=',$fromDateGregorian);
		}
		if(!empty($toDateGregorian)){
			$invoices = $invoices->whereDate('created_at' , '<=',$toDateGregorian);
		}
		$invoices = $invoices->orderBy('created_at','DESC')->get();
		
		
		$businesses = Business::where('creator_id',$specialistUser->id);
		if(!empty($fromDateGregorian)){
			$businesses = $businesses->whereDate('created_at' , '>=',$fromDateGregorian);
		}
		if(!empty($toDateGregorian)){
			$businesses = $businesses->whereDate('created_at' , '<=',$toDateGregorian);
		}
		$businesses = $businesses->orderBy('created_at','DESC')->paginate(20,['*'],'businesses_page');
		
		
		
		$userCalls = [];
		
		if(!empty($specialistUser->voip_id)){
			$getCallsQuery = DB::connection('mysql_voip')->table('cdr');
				
			$getCallsQuery = $getCallsQuery->where(function($query) use($specialistUser){
							$query->where('src','LIKE', '%'.$specialistUser->voip_id.'%')->orWhere('dst','LIKE','%'.$specialistUser->voip_id.'%');
						});
						
			if(!empty($fromDateGregorian)){
				$getCallsQuery = $getCallsQuery->whereDate('calldate' , '>=',$fromDateGregorian);
			}
			if(!empty($toDateGregorian)){
				$getCallsQuery = $getCallsQuery->whereDate('calldate' , '<=',$toDateGregorian);
			}
		
			$getCallsQuery=$getCallsQuery->orderBy('calldate','DESC');
		
			$userCalls=$getCallsQuery->paginate(20,['*'],'calls_page');

			

		}
		
		
		
		
		//TODO: maybe will be limiteded
		$userAlarms = CallTargetAlarm::where('specialist_user_id',$specialistUser->id);
		if(!empty($fromDateGregorian)){
			$userAlarms = $userAlarms->whereDate('created_at' , '>=',$fromDateGregorian);
		}
		if(!empty($toDateGregorian)){
			$userAlarms = $userAlarms->whereDate('created_at' , '<=',$toDateGregorian);
		}
		$userAlarms = $userAlarms->paginate(20,['*'],'alarms_page');
    	
    	//TODO: maybe will be limiteded
		$userNotes = Note::where('creator_id',$specialistUser->id);
		if(!empty($fromDateGregorian)){
			$userNotes = $userNotes->whereDate('created_at' , '>=',$fromDateGregorian);
		}
		if(!empty($toDateGregorian)){
			$userNotes = $userNotes->whereDate('created_at' , '<=',$toDateGregorian);
		}
		$userNotes = $userNotes->orderBy('created_at','DESC')->paginate(20,['*'],'user_notes_page');
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		//begin clue charts
		
		
		
		
		$cluesTempObj = Clue::select('*');
		if(!empty($fromDateGregorian)){
			$cluesTempObj = $cluesTempObj->whereDate('created_at' , '>=',$fromDateGregorian);
		}
		if(!empty($toDateGregorian)){
			$cluesTempObj = $cluesTempObj->whereDate('created_at' , '<=',$toDateGregorian);
		}
		$cluesTempObj = $cluesTempObj->where('creator_user_id',$specialistUser->id);
		
		$chartOpenClosedClues = Clue::selectRaw('clue_closed, count(id) as clue_count');
		if(!empty($fromDateGregorian)){
			$chartOpenClosedClues = $chartOpenClosedClues->whereDate('created_at' , '>=',$fromDateGregorian);
		}
		if(!empty($toDateGregorian)){
			$chartOpenClosedClues = $chartOpenClosedClues->whereDate('created_at' , '<=',$toDateGregorian);
		}
		$chartOpenClosedClues = $chartOpenClosedClues->where('creator_user_id',$specialistUser->id);
		
		
		$chartStatusGroupedClues = Clue::selectRaw('clue_status, count(id) as clue_count');
		if(!empty($fromDateGregorian)){
			$chartStatusGroupedClues = $chartStatusGroupedClues->whereDate('created_at' , '>=',$fromDateGregorian);
		}
		if(!empty($toDateGregorian)){
			$chartStatusGroupedClues = $chartStatusGroupedClues->whereDate('created_at' , '<=',$toDateGregorian);
		}
		$chartStatusGroupedClues = $chartStatusGroupedClues->where('creator_user_id',$specialistUser->id);

 
 
		$minDate = Clue::select(DB::raw('min(created_at) AS min_created_at'));
		if(!empty($fromDateGregorian)){
			$minDate = $minDate->whereDate('created_at' , '>=',$fromDateGregorian);
		}
		if(!empty($toDateGregorian)){
			$minDate = $minDate->whereDate('created_at' , '<=',$toDateGregorian);
		}
		$minDate = $minDate->first();
		$fromDateGregorian = $minDate->min_created_at;
		$fromDateJalali = \Morilog\Jalali\Jalalian::forge($fromDateGregorian)->format('%Y/%m/%d');
 
        
        $maxDate =  Clue::select(DB::raw('max(created_at) AS max_created_at'));
		if(!empty($fromDateGregorian)){
			$maxDate = $maxDate->whereDate('created_at' , '>=',$fromDateGregorian);
		}
		if(!empty($toDateGregorian)){
			$maxDate = $maxDate->whereDate('created_at' , '<=',$toDateGregorian);
		}
		$maxDate = $maxDate->first();
		$toDateGregorian = $maxDate->max_created_at;
		$toDateJalali = \Morilog\Jalali\Jalalian::forge($toDateGregorian)->format('%Y/%m/%d');
        
        
        
        //BEGIN: calculate date periods
        $explodedDate = explode('/',$toDateJalali);
		$toCheckDate = $explodedDate[0].$explodedDate[1];
		
		$fromHoldYear = NULL;
		$fromHoldMonth = NULL;
		do{
			
			if(empty($fromHoldYear)){
				$explodedDate = explode('/',$fromDateJalali);
				$fromHoldYear = $explodedDate[0];
				$fromHoldMonth = $explodedDate[1]; 
			}else{
				if($fromHoldMonth<=11){
					$fromHoldMonth++;
				}else{
					$fromHoldMonth = 1;
					$fromHoldYear++;
				}
			}
			
			if($fromHoldMonth<10){
				$fromHoldMonth = "0".((int)$fromHoldMonth);
			}
			$datePeriods[] = $fromHoldYear."/".$fromHoldMonth;
			
			
		
		
		}while($toCheckDate>=$fromHoldYear.$fromHoldMonth);
		//END: calculate date periods
	
	
		$chart1DataArray = [];
		foreach($datePeriods as $key=>$datePeriod){
		
			$fromDateJalali = $datePeriod."/01";
			$toDateJalali = $datePeriod."/31";
			
			$fromDateGregorian=\Morilog\Jalali\Jalalian::fromFormat('Y/m/d', $fromDateJalali)->toCarbon()->toDateTimeString();
			$toDateGregorian=\Morilog\Jalali\Jalalian::fromFormat('Y/m/d', $toDateJalali)->toCarbon()->toDateTimeString();
			
			
			$tempObj = clone $cluesTempObj;
			
			$periodDate = $tempObj->whereDate('created_at' , '>=',$fromDateGregorian)->whereDate('created_at' , '<=',$toDateGregorian);
			$periodCount = $periodDate->count();
			
			if($periodCount>0){
				$chart1DataArray[$datePeriod] = $periodCount;
			}
			
			
			
		
		}
		
		
		
		
		$chartOpenClosedClues = $chartOpenClosedClues->groupBy('clue_closed')->get();
	
		$chartOpenClosedCluesDataArray = [];
		foreach($chartOpenClosedClues as $chartItem){
			$tempTitle = "باز";
			if($chartItem->clue_closed==1){
				$tempTitle = "بسته";
			}
			
			$chartOpenClosedCluesDataArray[] = [
				'title' => $tempTitle,
				'count' => $chartItem->clue_count
			];
		}
		
		
		
		$chartStatusGroupedClues = $chartStatusGroupedClues->groupBy('clue_status')->get();
		$chartStatusGroupedDataArray = [];
		
		$clueTitlesArr = [
			'clue' => 'ثبت سرنخ',
			'customer' => 'ثبت مشتری',
			'validation' => 'اعتبارسنجی',
			'sale' => 'فروش',
			'pre_invoice'=>'‍‍پیش فاکتور',
			'invoice'=>'فاکتور',
			'return_from_sale' => 'برگشت از فروش'
		];
		
					
		foreach($chartStatusGroupedClues as $chartItem){
			
			$tempTitle = $clueTitlesArr[$chartItem->clue_status];
			
			$chartStatusGroupedDataArray[] = [
				'title' => $tempTitle,
				'count' => $chartItem->clue_count
			];
		}
		
		
		//end clue charts
		
		
		
		
		return view('panel.admin.specialists.report', compact('specialistUser','subUsersCount','userAlarms','clues','invoices','businesses','userCalls','userNotes','chart1DataArray','chartOpenClosedCluesDataArray','chartStatusGroupedDataArray'));
	
	}
  
  
	public function addNewNoteToSpecialist(Request $request, $specialistUserID)
    {
		
		$specialistUser = User::where('id',$specialistUserID)->where('user_type','SALES_MANAGER')->firstOrFail();

		//TODO: validate to access own customer
        $this->validate($request, [

            'note_text' => 'required|max:1500',
            //'visitor_user_id' => 'required|exists:users,id'

        ]);



		$inputs = [
			'creator_id' => $specialistUser->id,
			'referenced_by_user' => auth()->user()->id,
			'note_text' => $request->note_text,
		];

        $note = Note::create($inputs);



        return redirect()->back()
            ->with('success', 'یادداشت با موفقیت ایجاد شد');

    }
	
	
	public function updateNote(Request $request){
		
		
		request()->validate([
			'note_id' => 'required|integer|exists:notes,id',
            'cartable_row_note_text' => 'required|max:1500'
        ]);
		
		$note = Note::where('id',$request->note_id)->where('creator_id',auth()->user()->id)->firstOrFail();
		
		
		$note->note_text = $request->cartable_row_note_text;
		$note->save();
		
		
		return redirect()->back()
            ->with('success', 'یادداشت با موفقیت ویرایش شد');
		
		
	}
	
	

}

