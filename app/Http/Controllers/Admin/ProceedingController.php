<?php


namespace App\Http\Controllers\Admin;


use App\Models\Proceeding;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class ProceedingController extends Controller

{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    function __construct()

    {

        $this->middleware('permission:proceeding-list|proceeding-create|proceeding-edit|proceeding-delete', ['only' => ['index', 'show']]);

        $this->middleware('permission:proceeding-create', ['only' => ['create', 'store']]);

        $this->middleware('permission:proceeding-edit', ['only' => ['edit', 'update']]);

        $this->middleware('permission:proceeding-delete', ['only' => ['destroy']]);

    }

    /** 
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()

    {

        $proceedings = Proceeding::latest()->paginate(5);

        return view('panel.admin.proceedings.index', compact('proceedings'))
            ->with('i', (request()->input('page', 1) - 1) * 5);

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()

    {

        return view('panel.admin.proceedings.create');

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
	 
	 
	private function uploadFile($file, $uploadPath)
    {
        $uploaded_file_dir_signature = "";

        if ($file == null) {
            $uploaded_file_dir_signature = "";
        } else {

            if ($file->isValid()) {
                $fileName = str_replace(' ', '', time()) . rand(1000, 9999) . '_' . $file->getClientOriginalName();
                $destinationPath = public_path() . $uploadPath;

                $file->move($destinationPath, $fileName);
                $uploaded_file_dir_signature = $uploadPath . '/' . $fileName;
//                    if($request->edit_id!=Null) {
//                        $to_remove_dir_signature = \App\Models\AgencyRequest::find($request->edit_id)->signature;
//                    }
            } else {
//                    $this->img_upload_error_msg = 'آپلود تصویر ناموفق بود';
//                    goto catch_block;
            }
        }

        return $uploaded_file_dir_signature;
    } 

    public function store(Request $request)

    {

        request()->validate([

            'proceeding_title' => 'required|max:191',
            'hold_at' => 'required|max:20',
            'hold_description' => 'nullable|max:500',
			'hold_file' => 'required|max:15360',


        ]);
		
		
		$holdAtExploded = explode('/',$request->hold_at);
        $holdAt = implode(
			'-',
			\Morilog\Jalali\CalendarUtils::toGregorian($holdAtExploded[0], $holdAtExploded[1], $holdAtExploded[2])
		);
		
		
		$data = [
			'proceeding_title' => $request->proceeding_title,
            'hold_at' => $holdAt,
            'hold_description' => $request->hold_description,
		];
		
		//upload hold_file and set file_pathes to $businessData
        if (!empty($request->hold_file)) {

            $uploadPath = '/uploads/proceedings';
            $toUploadFile = $request->file('hold_file');
            $data['file_path'] = $this->uploadFile($toUploadFile, $uploadPath);

        }


        Proceeding::create($data);


        return redirect()->route('proceedings.index')
            ->with('success', 'صورتجلسه با موفقیت اضافه شد');

    }


    /**
     * Display the specified resource.
     *
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */

    public function show(Proceeding $proceeding)

    {

        return view('panel.admin.proceedings.show', compact('proceeding'));

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */

    public function edit(Proceeding $proceeding)
    {
		abort(403);
        return view('panel.admin.proceedings.edit', compact('proceeding'));

    }


    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, Proceeding $proceeding)

    {

        request()->validate([

            'proceeding_title' => 'required|max:191',
            'hold_at' => 'required|max:20',
            'hold_description' => 'nullable|max:500',
			'hold_file' => 'required|max:15360',


        ]);
		
		
		$holdAtExploded = explode('/',$request->hold_at);
        $holdAt = implode(
			'-',
			\Morilog\Jalali\CalendarUtils::toGregorian($holdAtExploded[0], $holdAtExploded[1], $holdAtExploded[2])
		);
		
		
		$data = [
			'proceeding_title' => $request->proceeding_title,
            'hold_at' => $holdAt,
            'hold_description' => $request->hold_description,
		];
		
		//upload hold_file and set file_pathes to $businessData
        if (!empty($request->hold_file)) {

            $uploadPath = '/uploads/proceedings';
            $toUploadFile = $request->file('hold_file');
            $data['file_path'] = $this->uploadFile($toUploadFile, $uploadPath);

        }


        $proceeding->update($data);


        return redirect()->route('proceedings.index')
            ->with('success', 'صورتجلسه مورد نظر با موفقیت ویرایش شد');

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */

    public function destroy(Proceeding $proceeding)

    {

        $proceeding->delete();


        return redirect()->route('proceedings.index')
            ->with('success', 'صورتجلسه مورد نظر با موفقیت حذف شد');

    }

}
