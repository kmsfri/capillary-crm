<?php


namespace App\Http\Controllers\Admin;


use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Models\User;

use App\Models\City;
use App\Models\UserCity;
use App\Models\VisitRoute;

use App\Models\UserGeoPath;

use Spatie\Permission\Models\Role;

use DB;

use Hash;

use Illuminate\Support\Arr;

use Helpers;


class UserController extends Controller

{

    function __construct()

    {

        $this->middleware('permission:user-list|user-create|user-edit|user-delete', ['only' => ['index', 'show']]);

        $this->middleware('permission:user-create', ['only' => ['create', 'store']]);

        $this->middleware('permission:user-edit', ['only' => ['edit', 'update']]);

        $this->middleware('permission:user-delete', ['only' => ['destroy']]);
        
        
        $this->middleware('permission:users-cities-management', ['only' => ['editUserCities','addCityToUser','removeUserCity']]);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)

    {

        $data = User::orderBy('id', 'DESC')->paginate(5);

        return view('panel.admin.users.index', compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 5);

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()

    {

        $roles = Role::pluck('name', 'name')->all();

        return view('panel.admin.users.create', compact('roles'));

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)

    {

        $this->validate($request, [

            'name' => 'required',
            'user_type' => 'required',

            'email' => 'required|email|unique:users,email',

            'password' => 'required|same:confirm-password',

            'roles' => 'required',
			
			'voip_id' => 'nullable|max:20',
			
			
			'is_active' => 'nullable|integer'

        ]);


        $input = $request->all();

        $input['password'] = Hash::make($input['password']);


        $user = User::create($input);

        $user->assignRole($request->input('roles'));


        return redirect()->route('users.index')
            ->with('success', 'کاربر جدید با موفقیت اضافه شد');

    }


    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */

    public function show($id)

    {

        $user = User::find($id);

        return view('panel.admin.users.show', compact('user'));

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */

    public function edit($id)

    {

        $user = User::find($id);

        $roles = Role::pluck('name', 'name')->all();

        $userRole = $user->roles->pluck('name', 'name')->all();


        return view('panel.admin.users.edit', compact('user', 'roles', 'userRole'));

    }


    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)

    {

        $this->validate($request, [

            'name' => 'required',
            'user_type' => 'required',
            'email' => 'required|email|unique:users,email,' . $id,

            'password' => 'same:confirm-password',

            'roles' => 'required',
			
			'voip_id' => 'nullable',
			
			'is_active' => 'nullable|integer'

        ]);


        $input = $request->all();

        if (!empty($input['password'])) {

            $input['password'] = Hash::make($input['password']);

        } else {

            $input = Arr::except($input, array('password'));

        }


        $user = User::find($id);

        $user->update($input);

        DB::table('model_has_roles')->where('model_id', $id)->delete();


        $user->assignRole($request->input('roles'));


        return redirect()->route('users.index')
            ->with('success', 'کاربر با موفقیت ویرایش شد');

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)

    {

        User::find($id)->delete();

        return redirect()->route('users.index')
            ->with('success', 'کاربر با موفقیت حذف شد');

    }
    
    
    
    
    
    
    
    public function editUserCities(Request $request){
    
    
    	//TODO: limit to edit just permitted users
    
    
    	$user = User::where('id',$request->user_id)->firstOrFail();
    
    	$userRegions = $user->cities()->get();
    	
    	
    	
    	$permittedStateRegionCities = Helpers::getPermittedCities(auth()->user());

        $states = City::select('*');
        if(is_array($permittedStateRegionCities['permitted_states'])){
            $states = $states->whereIn('id',$permittedStateRegionCities['permitted_states']);
        }else if($permittedStateRegionCities['permitted_states']!='%'){
            $states = $states->whereIn('id',[]);
        }
        $states = $states->where('unit_type', 0)->where('unit_status', 1)->orderBy('unit_order', 'ASC')->get();
	$cities = [];
	$regions = [];
    	
    	
    	return view('panel.admin.users.edit-user-cities', compact('user','userRegions','states','cities','regions'));
    	
    
    }
    
    
    public function addCityToUser(Request $request){
    
    
    	//TODO: limit to edit just permitted users
    
    	$this->validate($request, [

            'user_id' => 'required|integer|exists:users,id',
            'region_id' => 'nullable|exists:cities,id',
            'city_id' => 'nullable|exists:cities,id'


        ]);
        
        
        $user = User::where('id',$request->user_id)->firstOrFail();
        
        
        $regionIDsToAdd = [];
        
        
        if(!empty($request->region_id)){
        
        	$region = City::where('id',$request->region_id)->where('unit_type',2)->first();

		
        
        	$regionIDsToAdd[] = $region->id;
        }elseif(!empty($request->city_id)){
        
        	
			
        	$city = City::findOrFail($request->city_id);

		
        	
        	$regionIDsToAdd = $city->subsets()->where('unit_type',2)->get()->pluck('id')->toArray();
        
        
        }elseif(!empty($request->state_id)){
       
        	
			$state = City::findOrFail($request->state_id);
			
			$cityIDs = $state->subsets()->where('unit_type',1)->get()->pluck('id')->toArray();
			
			$regionIDsToAdd = [];
			
			foreach($cityIDs as $cityID){
				$city = City::findOrFail($cityID);
				$regionIDsToAdd = array_merge($regionIDsToAdd,$city->subsets()->where('unit_type',2)->get()->pluck('id')->toArray());
			}
			
        	
        
        
        }else{
        
        
			return redirect()->back()
					->with('danger', 'انتخاب استان اجباری می باشد');
			
        
        }
        
        
        foreach($regionIDsToAdd as $regionIDToAdd){
        	if(!$user->cities()->where('city_id',$regionIDToAdd)->exists()){
        		$user->cities()->attach($regionIDToAdd);
        	}
        }
        
        

        
        
        return redirect()->back()
	    		->with('success', 'دسترسی مناطق کاربر '.$user->name." با موفقیت ویرایش شد.");
        
        
    
    }
    
    
    
    
    public function removeUserCity(Request $request){
    
    	//TODO: limit to edit just permitted users
    
    	$this->validate($request, [

            'user_id' => 'required|integer|exists:users,id',
            'region_id' => 'nullable|exists:cities,id',


        ]);
        
        
        $user = User::findOrFail($request->user_id);
        
		
		UserCity::where('user_id',$user->id)->where('city_id',$request->region_id)->delete();
		
        //$user->cities()->where('city_id',$request->region_id)->delete();
        
        
        return redirect()->back()
	    		->with('success', 'دسترسی مناطق کاربر '.$user->name." با موفقیت ویرایش شد.");
        
    
    
    }
	
	
	
	public function removeUserCityMass(Request $request){
    
    	//TODO: limit to edit just permitted users
    
    	$this->validate($request, [

            'user_id' => 'required|integer|exists:users,id',
            'region_id.*' => 'nullable|exists:cities,id',


        ]);
		
		$user = User::findOrFail($request->user_id);
        
        UserCity::where('user_id',$user->id)->whereIn('city_id',$request->region_id)->delete();
        
        return redirect()->back()
	    		->with('success', 'دسترسی مناطق کاربر '.$user->name." با موفقیت ویرایش شد.");
        
    
    
    }
	
	
	
	
	public function updateGeoPath(Request $request){
		
		$user = User::findOrFail($request->user_id);
		
		$latitude = $request->latitude;
		$longitude = $request->longitude;
		
		
		$latestOpenPath = UserGeoPath::where('user_id',$user->id)->whereNull('end_at')->firstOrFail();
		
		
		$path = $latestOpenPath->path_points;
		
		$pathArray = json_decode($path,true);
		$lastLocation = !empty($pathArray)?last($pathArray):[];
		
		$toBeStore = true;
		if(!empty($lastLocation) && !empty($lastLocation['lat']) && !empty($lastLocation['lng'])){
			$distanceToPreviousPoint = $this->distance($lastLocation['lat'],$lastLocation['lng'],$latitude,$longitude,"K"); //in kilometer
			if($distanceToPreviousPoint<=0.3){
				$toBeStore = false;
			}
		}
		
		if($toBeStore){
			$now = date('Y-m-d H:i:s', time());
			
			$dataArray = [
				'lat'=>$latitude,
				'lng'=>$longitude,
				'time' => $now
			];
			
			$pathArray[] = $dataArray;

			$latestOpenPath->path_points = json_encode($pathArray);
			
			$latestOpenPath->save();
		}
		return true;
		
		
	}
	
	
	
	public function showUserWalkedPaths(Request $request){
		
		
		$permittedUserIDs = [];
    	
    	$showAllSpecialists = false;
        if(auth()->user()->user_type=='SALES_SPECIALIST'){
            $permittedUserIDs = [auth()->user()->id];

        }else if(auth()->user()->user_type=='SALES_MANAGER'){
            $subUsers = auth()->user()->subUsers()->get();
            foreach($subUsers as $subUser){
                $permittedUserIDs[] = $subUser->id;
            }

        }else if(auth()->user()->user_type=='CSO'){
            $managerUsers = auth()->user()->subUsers()->get();
            foreach($managerUsers as $managerUser){
                foreach($managerUser->subUsers()->get() as $specialistUser){
                    $permittedUserIDs[] = $specialistUser->id;
                }
            }

        }else if(auth()->user()->user_type=='ADMIN'){
            $showAllSpecialists = true;
        }
    
    
    	$user = User::select('*')->where('id',$request->user_id);
    	
    	if(!$showAllSpecialists){
    		$user = $user->whereIn('id',$permittedUserIDs);
    	}
		
		
		$user = $user->firstOrFail();
		
		
		$walkedPaths = UserGeoPath::where('user_id',$user->id)->orderBy('created_at','DESC')->paginate(20);
		
		return view('panel.admin.users.walked-paths', compact('user','walkedPaths'));
		
		
	}
	
	
	public function showWalkedPath(Request $request){
		
		
		
		//TODO: add permission limitation
		$walkedPath = UserGeoPath::where('id',$request->path_id)->firstOrFail();
		
		
		
		
		$permittedUserIDs = [];
    	
    	$showAllSpecialists = false;
        if(auth()->user()->user_type=='SALES_SPECIALIST'){
            $permittedUserIDs = [auth()->user()->id];

        }else if(auth()->user()->user_type=='SALES_MANAGER'){
            $subUsers = auth()->user()->subUsers()->get();
            foreach($subUsers as $subUser){
                $permittedUserIDs[] = $subUser->id;
            }

        }else if(auth()->user()->user_type=='CSO'){
            $managerUsers = auth()->user()->subUsers()->get();
            foreach($managerUsers as $managerUser){
                foreach($managerUser->subUsers()->get() as $specialistUser){
                    $permittedUserIDs[] = $specialistUser->id;
                }
            }

        }else if(auth()->user()->user_type=='ADMIN'){
            $showAllSpecialists = true;
        }
    
    
    	$user = User::select('*')->where('id',$walkedPath->user_id);
    	
    	if(!$showAllSpecialists){
    		$user = $user->whereIn('id',$permittedUserIDs);
    	}
		
		
		$user = $user->firstOrFail();
		
		
		
		
		
		
		
		$walkedPathPoints = !empty($walkedPath->path_points)?json_decode($walkedPath->path_points,true):[];
		
		$locationsArray = [];
		foreach($walkedPathPoints as $point){
			
			$jalaliDate = \Morilog\Jalali\Jalalian::forge($point['time'])->format('%A, %d %B %Y در ساعت H:i:s');
			$locationsArray[] = [
				'lat' => $point['lat'],
				'lng' => $point['lng'],
				'title' => $jalaliDate,
			];
		}
		
		
		
		
		
		
		
		
		$startAtPath = $walkedPath->start_at;
		
		$startDatePath = explode(" ",$startAtPath)[0];
		
		
		

		
		
		//get routes:
		$visitRoute = VisitRoute::where('visitor_user_id',$walkedPath->user_id)->where('visit_at_date',$startDatePath)->first();
		
		$visitRouteData = NULL;
		if(!empty($visitRoute->visit_route)){
			$visitRouteData = $visitRoute->visit_route;
		}
		
		$userVisitRouteData = [];
		if(!empty($visitRouteData)){
			$visitRouteData = json_decode($visitRouteData,true);
			if(isset($visitRouteData['operators']) && !empty($visitRouteData['operators'])){
			
				foreach($visitRouteData['operators'] as $visitRouteOperator){
					
					if($visitRouteOperator['properties']['meta']['type']==2){
						$userVisitRouteData[] = [
							'title' => $visitRouteOperator['properties']['title'],
							'latitude' => ($visitRouteOperator['properties']['meta']['type']==2)?$visitRouteOperator['properties']['meta']['latitude']:Null,
							'longitude' => ($visitRouteOperator['properties']['meta']['type']==2)?$visitRouteOperator['properties']['meta']['longitude']:Null,
							'entry_datetime' => $visitRouteOperator['properties']['meta']['entry_datetime'],
							'exit_datetime' => $visitRouteOperator['properties']['meta']['exit_datetime']
						];
					}
				}

			}
		}
		
		
		
		//userVisitRouteData
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		return view('panel.admin.users.show-user-walked-path',compact('user','walkedPath','locationsArray','userVisitRouteData'));
		
		
	}
	
	
	public function toggleWalkingStatus(){
		
		$userID = auth()->user()->id;
		
		$user = User::findOrFail($userID);
		
		
		$walkedPath = UserGeoPath::where('user_id',$userID)->latest()->first();
		
		if(!empty($walkedPath) && empty($walkedPath->end_at)){
			$walkedPath->end_at = \Carbon\Carbon::now();
			$walkedPath->save();
			
		}else{
			
			$newWalkedPath = new UserGeoPath();
			
			$newWalkedPath->start_at = \Carbon\Carbon::now();
			$newWalkedPath->user_id = $userID;
			
			$newWalkedPath->save();
			
		}
		
		
		return redirect()->back();
		
	}
	
	
	private function distance($lat1, $lon1, $lat2, $lon2, $unit) {
	  if (($lat1 == $lat2) && ($lon1 == $lon2)) {
		return 0;
	  }
	  else {
		$theta = $lon1 - $lon2;
		$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
		$dist = acos($dist);
		$dist = rad2deg($dist);
		$miles = $dist * 60 * 1.1515;
		$unit = strtoupper($unit);

		if ($unit == "K") {
		  return ($miles * 1.609344);
		} else if ($unit == "N") {
		  return ($miles * 0.8684);
		} else {
		  return $miles;
		}
	  }
	}

}

