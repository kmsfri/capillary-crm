<?php


namespace App\Http\Controllers\Admin;



use Helpers;
use App\Models\City;
use App\Models\Business;
use App\Models\BusinessCredit;
use App\Models\CrmConfig;
use App\Models\Invoice;
use App\Models\Product;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Rules\CheckBusinessPermission;
use niklasravnsborg\LaravelPdf\Facades\Pdf;

class UnofficialInvoiceController extends Controller

{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    function __construct()

    {

        $this->middleware('permission:invoice-list|invoice-create|invoice-edit|invoice-delete', ['only' => ['index', 'show']]);

        $this->middleware('permission:invoice-create', ['only' => ['create', 'store']]);

        $this->middleware('permission:invoice-edit', ['only' => ['edit', 'update']]);

        $this->middleware('permission:invoice-delete', ['only' => ['destroy']]);
        
        $this->middleware('permission:primary-invoice-unofficial-list', ['only' => ['primaryInvoices']]);
        
        $this->middleware('permission:rejected-invoice-unofficial-list', ['only' => ['rejectedInvoices']]);
        
        $this->middleware('permission:business-accounting-approve', ['only' => ['showAccountantApprovingForm','accountantApprovingInvoice']]);
        
         $this->middleware('permission:business-sales-specialist-approve', ['only' => ['showSalesSpecialistApprovingForm','salesSpecialistApprovingInvoice']]);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */





    public function index(Request $request)

    {

        $permittedConditions = Helpers::getBusinessesAccessPermittedConditions(auth()->user());

        $businesses = Business::select('*')->where('id','<',0);

        foreach($permittedConditions as $key=>$condition){
            $toFireCondition1 = [];
            if(!empty($condition['ignore_cities']) && $condition['ignore_cities']){
                $toFireCondition1 = [
                    'operand'=>'LIKE',
                    'values' => '%'
                ];
            }else{
                $toFireCondition1 = [
                    'operand'=>'IN',
                    'values' => $condition['city_ids']
                ];
            }

            $toFireCondition2 = [];
            if(!empty($condition['ignore_creator_id']) && $condition['ignore_creator_id']){
                $toFireCondition2 = [
                    'operand'=>'LIKE',
                    'values' => '%'
                ];
            }else{
                $toFireCondition2 = [
                    'operand'=>'=',
                    'values' => $condition['creator_id']
                ];
            }
            $businesses = $businesses->orWhere(function($query) use($toFireCondition1,$toFireCondition2) {
                if($toFireCondition1['operand']=='IN'){
                    $query->whereIn('city_id', $toFireCondition1['values'])->where('creator_id',$toFireCondition2['operand'],$toFireCondition2['values']);
                }else{
                    $query->where('city_id',$toFireCondition1['operand'], $toFireCondition1['values'])->where('creator_id',$toFireCondition2['operand'],$toFireCondition2['values']);
                }
            });
        }

        $businessesIDs = $businesses->pluck('id')->toArray();


	$invoices = Invoice::whereIn('business_id',$businessesIDs)
			->where('invoice_type','unofficial')
			->where('approved_by_sales_specialist',0)
			->orWhere(function($query) {
			    $query->where('approved_by_sales_specialist', 1)->where('approved_by_accountant', 0);
			})
			->orderBy('created_at','DESC')->paginate(20);

        return view('panel.admin.invoices.unofficial.index', compact('invoices'))
            ->with('i', (request()->input('page', 1) - 1) * 20);

    }
    
    
    
    
    public function primaryInvoices(Request $request)
    {

        $permittedConditions = Helpers::getBusinessesAccessPermittedConditions(auth()->user());

        $businesses = Business::select('*')->where('id','<',0);

        foreach($permittedConditions as $key=>$condition){
            $toFireCondition1 = [];
            if(!empty($condition['ignore_cities']) && $condition['ignore_cities']){
                $toFireCondition1 = [
                    'operand'=>'LIKE',
                    'values' => '%'
                ];
            }else{
                $toFireCondition1 = [
                    'operand'=>'IN',
                    'values' => $condition['city_ids']
                ];
            }

            $toFireCondition2 = [];
            if(!empty($condition['ignore_creator_id']) && $condition['ignore_creator_id']){
                $toFireCondition2 = [
                    'operand'=>'LIKE',
                    'values' => '%'
                ];
            }else{
                $toFireCondition2 = [
                    'operand'=>'=',
                    'values' => $condition['creator_id']
                ];
            }
            $businesses = $businesses->orWhere(function($query) use($toFireCondition1,$toFireCondition2) {
                if($toFireCondition1['operand']=='IN'){
                    $query->whereIn('city_id', $toFireCondition1['values'])->where('creator_id',$toFireCondition2['operand'],$toFireCondition2['values']);
                }else{
                    $query->where('city_id',$toFireCondition1['operand'], $toFireCondition1['values'])->where('creator_id',$toFireCondition2['operand'],$toFireCondition2['values']);
                }
            });
        }

        $businessesIDs = $businesses->pluck('id')->toArray();



	$invoices = Invoice::whereIn('business_id',$businessesIDs)
			->where('invoice_type','unofficial')
			->where('approved_by_accountant',1)
			->orderBy('created_at','DESC')->paginate(20);

        return view('panel.admin.invoices.unofficial.primary-invoices', compact('invoices'))
            ->with('i', (request()->input('page', 1) - 1) * 20);

    }
    
    
    
    public function rejectedInvoices(Request $request)
    {

        $permittedConditions = Helpers::getBusinessesAccessPermittedConditions(auth()->user());

        $businesses = Business::select('*')->where('id','<',0);

        foreach($permittedConditions as $key=>$condition){
            $toFireCondition1 = [];
            if(!empty($condition['ignore_cities']) && $condition['ignore_cities']){
                $toFireCondition1 = [
                    'operand'=>'LIKE',
                    'values' => '%'
                ];
            }else{
                $toFireCondition1 = [
                    'operand'=>'IN',
                    'values' => $condition['city_ids']
                ];
            }

            $toFireCondition2 = [];
            if(!empty($condition['ignore_creator_id']) && $condition['ignore_creator_id']){
                $toFireCondition2 = [
                    'operand'=>'LIKE',
                    'values' => '%'
                ];
            }else{
                $toFireCondition2 = [
                    'operand'=>'=',
                    'values' => $condition['creator_id']
                ];
            }
            $businesses = $businesses->orWhere(function($query) use($toFireCondition1,$toFireCondition2) {
                if($toFireCondition1['operand']=='IN'){
                    $query->whereIn('city_id', $toFireCondition1['values'])->where('creator_id',$toFireCondition2['operand'],$toFireCondition2['values']);
                }else{
                    $query->where('city_id',$toFireCondition1['operand'], $toFireCondition1['values'])->where('creator_id',$toFireCondition2['operand'],$toFireCondition2['values']);
                }
            });
        }

        $businessesIDs = $businesses->pluck('id')->toArray();



	$invoices = Invoice::whereIn('business_id',$businessesIDs)
			->where('invoice_type','unofficial')
			->where('approved_by_sales_specialist',2)
			->orWhere('approved_by_accountant',2)
			->orderBy('created_at','DESC')->paginate(20);

        return view('panel.admin.invoices.unofficial.rejected-invoices', compact('invoices'))
            ->with('i', (request()->input('page', 1) - 1) * 20);

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create(Request $request)

    {
    
        request()->validate([
            'business_id' => ['nullable','integer','exists:businesses,id', new CheckBusinessPermission()],
            'customer_id' => 'required_without:business_id|integer|exists:customers,id',
        ]);
        
        if(empty($request->business_id) && !empty($request->customer_id)){
        	$business = Business::where('customer_id',$request->customer_id)->first();
        	if(empty($business)){
        		return redirect()->route('businesses.create',['customer_id'=>$request->customer_id])
            			->with('warning', 'برای صدور پیش فاکتور ابتدا باید اطلاعات مربوط به کسب و کار مورد نظر را تکمیل کنید!');
        	}
        }else{
        	$business = Business::findOrFail($request->business_id);
        	
        	if($business->approved_by_sales_specialist!=1){
        		return view('panel.admin.invoices.unofficial.not-approved-by-sales-specialist', compact('business'));
        	}else if($business->approved_by_accountant!=1 && ($business->buy_method==1 || $business->buy_method==2)){
        		return view('panel.admin.invoices.unofficial.not-approved', compact('business'));
        	}
        }
        

        
        

        $products = Product::select(['id','product_name','has_priority', 'price', 'product_code'])->orderBy('has_priority','DESC')->get();

        return view('panel.admin.invoices.unofficial.create', compact('business','products'));

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)

    {




        request()->validate([
            'business_id' => ['required','integer','exists:businesses,id', new CheckBusinessPermission()],
            'invoice_items.*' => 'required',
            'delivery_place_address' => 'required|max:255',
            'sell_method' => 'required|integer',
            'cheque_time' => 'required|integer',
            'invoice_description' => 'required|max:500'
        ]);
		
		
		//check credit
		if($request->sell_method==3){ //sell method is Credit
		
			$remainedCreditAmount = BusinessCredit::where('business_id',$request->business_id)->sum('credit_amount');
			
			
			$invoiceSumAmount = 0;
			
			foreach($request->invoice_items as $invItem){
				$invoiceSumAmount = $invoiceSumAmount + $invItem['item_total_price'];
			}
			
			if(((int)$invoiceSumAmount)>((int)$remainedCreditAmount)){
				return redirect()->back()
					->withInput($request->input())
            		->with('danger', 'با توجه به نحوه فروش اعتباری، جمع مبالغ فاکتور از میزان اعتبار باقیمانده کسب و کار بیشتر است.');
			}
			
		
		}
		
		
		
		
		//BEGIN VALIDATE INVOICE ITEEMS
		$maxChequeDays = CrmConfig::where('config_key','MAX_CHEQUE_DAYS')->firstOrFail()->config_value;
		
		if($request->cheque_time > $maxChequeDays){
			return redirect()->back()
					->withInput($request->input())
            		->with('danger', 'حداکثر زمان چک میتواند '.$maxChequeDays.' باشد.');
		}
		
		$maxDiscountAmount = CrmConfig::where('config_key','MAX_DISCOUNT_AMOUNT')->firstOrFail()->config_value;
		$maxInvoiceItemAmount = CrmConfig::where('config_key','MAX_INVOICE_ITEM_AMOUNT')->firstOrFail()->config_value;
		
		$sumOfItemAmounts = 0;
		foreach($request->invoice_items as $invItem){
			if($invItem['discount']>$maxDiscountAmount){
				return redirect()->back()
					->withInput($request->input())
            		->with('danger', 'حداکثر میزان تخفیف میتواند '.$maxDiscountAmount.' باشد.');
			}
			
			if($invItem['item_total_price']>$maxInvoiceItemAmount){
				return redirect()->back()
					->withInput($request->input())
            		->with('danger', 'حداکثر مبلغ هر آیتم فاکتور میتواند '.$maxInvoiceItemAmount.' باشد.');
			}
			
			$sumOfItemAmounts = $sumOfItemAmounts + $invItem['item_total_price'];
		}
		
		
		$maxSumOfInvoiceItemsAmount = CrmConfig::where('config_key','MAX_SUM_OF_INVOICE_ITEMS_AMOUNT')->firstOrFail()->config_value;
		if($sumOfItemAmounts > $maxSumOfInvoiceItemsAmount){
			return redirect()->back()
					->withInput($request->input())
            		->with('danger', 'جمع مبالع آیتمهای هر فاکتور میتواند حداکثر '.$maxSumOfInvoiceItemsAmount.' باشد.');
		}
		//BEGIN VALIDATE INVOICE ITEEMS




        $invoiceData = [];

        $invoiceData['invoice_items'] = json_encode($request->invoice_items, true);

        $invoiceData['delivery_place_address'] = $request->delivery_place_address;
        $invoiceData['sell_method'] = $request->sell_method;
        $invoiceData['cheque_time'] = $request->cheque_time;
        $invoiceData['invoice_description'] = $request->invoice_description;
        
        $invoiceData['invoice_type'] = 'unofficial';





      
        $invoiceData['business_id'] = $request->business_id;

        $invoiceData['creator_id'] = auth()->user()->id;


        Invoice::create($invoiceData);


        return redirect()->route('invoices.unofficial.index')
            ->with('success', 'فاکتور مورد نظر با موفقیت صادر شد');

    }


    /**
     * Display the specified resource.
     *
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */

    public function show(Business $business)

    {

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */

    public function edit(Invoice $invoice, Request $request)
    {
    
    dd($invoice);
    
    	$request->request->add(['business_id' => $invoice->business_id]);
    
    
    	request()->validate([
            'business_id' => ['required','integer','exists:businesses,id', new CheckBusinessPermission()]
        ]);
    
    

        $invoice->invoice_items = !empty($invoice->invoice_items)?json_decode($invoice->invoice_items, true):[];
        
        $business = $invoice->business()->first();
        
        $products = Product::select(['id','product_name','has_priority', 'price', 'product_code'])->orderBy('has_priority','DESC')->get();

        return view('panel.admin.invoices.unofficial.edit', compact('invoice','business', 'products'));

    }


    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, Invoice $invoice)
    {
    
    
    	$request->request->add(['business_id' => $invoice->business_id]);
    
  
        request()->validate([

            'business_id' => ['required','integer','exists:businesses,id', new CheckBusinessPermission()],
            'invoice_items.*' => 'required',
            'delivery_place_address' => 'required|max:255',
            'sell_method' => 'required|integer',
            'cheque_time' => 'required|integer',
            'invoice_description' => 'required|max:500'
        ]);


        

        $invoiceData = [];

        $invoiceData['invoice_items'] = json_encode($request->invoice_items, true);

        $invoiceData['delivery_place_address'] = $request->delivery_place_address;
        $invoiceData['sell_method'] = $request->sell_method;
        $invoiceData['cheque_time'] = $request->cheque_time;
        $invoiceData['invoice_description'] = $request->invoice_description;

        $invoice->update($invoiceData);
        
        
        return redirect()->route('invoices.unofficial.index')
            ->with('success', 'فاکتور مورد نظر با موفقیت ویرایش شد');
        




    }


    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */

    public function destroy(Invoice $invoice)

    {

        $invoice->delete();


        return redirect()->route('invoices.unofficial.index')
            ->with('success', 'فاکتور مورد نظر با موفقیت حذف شد');

    }


    public function getPdf(Request $request){


	

	$data = [];
        
        
        
        $invoice = Invoice::findOrFail($request->invoice_id);

        
      
        
        if($invoice->approved_by_accountant==1){
        	$data['title'] = 'صورت حساب فروش کالا و خدمات';
        }else{
        	$data['title'] = 'پیش فاکتور کالا و خدمات';
        }
        
        
 
 	$data['serial_number'] = $invoice->id;
 	
 	$data['created_at'] = \Morilog\Jalali\Jalalian::forge($invoice->created_at)->format('%A, %d %B %Y');
 	
 	$data['business_first_name'] = $invoice->business()->first()->first_name;
 	
 	$data['business_last_name'] = $invoice->business()->first()->last_name;
 	
 	$data['business_name'] = $invoice->business()->first()->business_name;
 	
 	$data['business_address'] = $invoice->business()->first()->address;
 	
 	$data['business_postal_code'] = $invoice->business()->first()->postal_code;
 	
 	$data['business_phone_number'] = $invoice->business()->first()->phone_number;
 	
 	$businessMobiles = json_decode($invoice->business()->first()->mobiles,true);
 	
 	$data['business_mobile1'] = !empty($businessMobiles[0])?$businessMobiles[0]:'-';
 	
 	$data['business_mobile2'] = !empty($businessMobiles[1])?$businessMobiles[1]:'-';
 	
 	$data['invoice_items'] = json_decode($invoice->invoice_items,true);
 	
 	$data['invoice_creator'] = $invoice->creator()->first()->name;
 	
 	$data['invoice_registerar'] = $invoice->creator()->first()->name;
 	
 	$data['invoice_delivery_place_address'] = $invoice->delivery_place_address;
 	
 	
 	
 	$enums = config('enum');
        $sellMethods = $enums['sell_method'];
        
        $data['invoice_sell_method'] = !empty($sellMethods[$invoice->sell_method])?$sellMethods[$invoice->sell_method]:'-';
        
        $data['invoice_cheque_time'] = $invoice->cheque_time;
        
        $data['invoice_description'] = $invoice->invoice_description;
        
        
        
        //return view('panel.admin.invoices.pdf', compact('data'));
        
        $options = [];
        if($invoice->approved_by_accountant==1){
        	$options = [ 
		  'format' => 'A4-L',
		  'orientation' => 'L'
		];
        }
        
        
        $pdf = Pdf::loadView('panel.admin.invoices.pdf', compact('data'), [], $options);
        
        return $pdf->stream('report.pdf');

    }
    
    
    
    
    
    public function showAccountantApprovingForm(Request $request){
    	//TODO: limit to own businesses
    	//TODO: limit to just approve 0 fields
    	$invoice = Invoice::findOrFail($request->invoice_id);
    	if($invoice->approved_by_sales_specialist==0){
    		return redirect()->back()
            		->with('danger', 'تایید ابتدا باید توسط کارشناس فروش انجام شود');
    	}
    	
    	
    	return view('panel.admin.accountant.invoices.unofficial.approve', compact('invoice'));
    }
    
    
    public function accountantApprovingInvoice(Request $request){
    
    	//TODO: limit to own businesses
    	//TODO: limit to just approve 0 fields
    
    	request()->validate([

	    'invoice_id' => 'required|exists:invoices,id',
            'accountant_approving_description' => 'required|max:1000',
            'approved_by_accountant' => 'required|integer',


        ]);
        
        
        
        
        $invoice = Invoice::findOrFail($request->invoice_id);
    	if($invoice->approved_by_sales_specialist==0){
    		return redirect()->back()
            		->with('danger', 'تایید ابتدا باید توسط کارشناس فروش انجام شود');
    	}
		
		
		
		
		//check credit
		if($invoice->sell_method=='CREDIT' && $request->approved_by_accountant==1){ //sell method is Credit and approved by accountant
		
			$remainedCreditAmount = BusinessCredit::where('business_id',$invoice->business_id)->sum('credit_amount');
			
			
			$invoiceSumAmount = 0;
			
			$decodedInvoiceItems = json_decode($invoice->invoice_items,true);
			
			foreach($decodedInvoiceItems as $invItem){
				$invoiceSumAmount = $invoiceSumAmount + $invItem['item_total_price'];
			}
	
			if(((int)$invoiceSumAmount)>((int)$remainedCreditAmount)){
				return redirect()->back()
					->withInput($request->input())
            		->with('danger', 'با توجه به نحوه فروش اعتباری، جمع مبالغ فاکتور از میزان اعتبار باقیمانده کسب و کار بیشتر است.');
			}
			
			
			
			
			
			$negativeCreditData = [
				'business_id' => $invoice->business_id,
				'credit_amount' => '-'.$invoiceSumAmount,
				'invoice_id' => $invoice->id,
				'credit_description' => 'تایید فاکتور با شناسه '.$invoice->id." توسط: ".auth()->user()->id,
			];
			BusinessCredit::create($negativeCreditData);
			
		
		}
		
		
		
		
		
		
		
		
		
    	
    	
    	$invoice->accountant_approving_description = $request->accountant_approving_description;
    	$invoice->approved_by_accountant = $request->approved_by_accountant;
    	
    	$invoice->save();
    	
    	return redirect()->route('invoices.unofficial.index')
            ->with('success', 'تایید/عدم تایید پیش فاکتور با موفقیت ثبت شد');
    }
    
    
    
    
    
     public function showSalesSpecialistApprovingForm(Request $request){
    	//TODO: limit to own businesses
    	//TODO: limit to just approve 0 fields
    	$invoice = Invoice::findOrFail($request->invoice_id);
    	
    	
    	return view('panel.admin.accountant.invoices.unofficial.approve-by-sales-specialist', compact('invoice'));
    }
    
    
    public function salesSpecialistApprovingInvoice(Request $request){
    
    	//TODO: limit to own businesses
    	//TODO: limit to just approve 0 fields
    
    	request()->validate([

	    'invoice_id' => 'required|exists:invoices,id',
            'approved_by_sales_specialist' => 'required|integer',
            'sales_specialist_approving_description' => 'required|max:1000',


        ]);
        
        
        
        
        $invoice = Invoice::findOrFail($request->invoice_id);
    	
    	
    	$invoice->sales_specialist_approving_description = $request->sales_specialist_approving_description;
    	$invoice->approved_by_sales_specialist = $request->approved_by_sales_specialist;
    	
    	$invoice->save();
    	
    	return redirect()->route('invoices.unofficial.index')
            ->with('success', 'تایید/عدم تایید پیش فاکتور با موفقیت ثبت شد');
    }




}
