<?php


namespace App\Http\Controllers\Admin;


use App\Models\Marketing;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class MarketingController extends Controller

{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    function __construct()

    {

        $this->middleware('permission:marketing-list|marketing-create|marketing-edit|marketing-delete', ['only' => ['index', 'show']]);

        $this->middleware('permission:marketing-create', ['only' => ['create', 'store']]);

        $this->middleware('permission:marketing-edit', ['only' => ['edit', 'update']]);

        $this->middleware('permission:marketing-delete', ['only' => ['destroy']]);

    }

    /** 
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()

    {

        $marketings = Marketing::latest()->paginate(5);

        return view('panel.admin.marketing.index', compact('marketings'))
            ->with('i', (request()->input('page', 1) - 1) * 5);

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()

    {

        return view('panel.admin.marketing.create');

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
	 
	 
	private function uploadFile($file, $uploadPath)
    {
        $uploaded_file_dir_signature = "";

        if ($file == null) {
            $uploaded_file_dir_signature = "";
        } else {

            if ($file->isValid()) {
                $fileName = str_replace(' ', '', time()) . rand(1000, 9999) . '_' . $file->getClientOriginalName();
                $destinationPath = public_path() . $uploadPath;

                $file->move($destinationPath, $fileName);
                $uploaded_file_dir_signature = $uploadPath . '/' . $fileName;
//                    if($request->edit_id!=Null) {
//                        $to_remove_dir_signature = \App\Models\AgencyRequest::find($request->edit_id)->signature;
//                    }
            } else {
//                    $this->img_upload_error_msg = 'آپلود تصویر ناموفق بود';
//                    goto catch_block;
            }
        }

        return $uploaded_file_dir_signature;
    } 

    public function store(Request $request)

    {

        request()->validate([

            'marketing_title' => 'required|max:191',
            'do_at' => 'required|max:20',
            'marketing_description' => 'nullable|max:500',
			'marketing_file' => 'required|max:15360',


        ]);
		
		
		$holdAtExploded = explode('/',$request->do_at);
        $holdAt = implode(
			'-',
			\Morilog\Jalali\CalendarUtils::toGregorian($holdAtExploded[0], $holdAtExploded[1], $holdAtExploded[2])
		);
		
		
		$data = [
			'marketing_title' => $request->marketing_title,
            'do_at' => $holdAt,
            'marketing_description' => $request->marketing_description,
		];
		
		//upload marketing_file and set file_pathes to $businessData
        if (!empty($request->marketing_file)) {

            $uploadPath = '/uploads/marketing';
            $toUploadFile = $request->file('marketing_file');
            $data['file_path'] = $this->uploadFile($toUploadFile, $uploadPath);

        }


        Marketing::create($data);


        return redirect()->route('marketing.index')
            ->with('success', 'آیتم بازاریابی جدید با موفقیت اضافه شد');

    }


    /**
     * Display the specified resource.
     *
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */

    public function show(Marketing $marketing)

    {

        return view('panel.admin.marketing.show', compact('marketing'));

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */

    public function edit(Marketing $marketing)
    {
		abort(403);
        return view('panel.admin.marketing.edit', compact('marketing'));

    }


    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, Marketing $marketing)

    {

        request()->validate([

            'marketing_title' => 'required|max:191',
            'do_at' => 'required|max:20',
            'marketing_description' => 'nullable|max:500',
			'marketing_file' => 'nullable|max:15360',



        ]);
		
		
		$holdAtExploded = explode('/',$request->do_at);
        $holdAt = implode(
			'-',
			\Morilog\Jalali\CalendarUtils::toGregorian($holdAtExploded[0], $holdAtExploded[1], $holdAtExploded[2])
		);
		
		
		$data = [
			'marketing_title' => $request->marketing_title,
            'do_at' => $holdAt,
            'marketing_description' => $request->marketing_description,
		];
		
		//upload marketing_file and set file_pathes to $businessData
        if (!empty($request->marketing_file)) {

            $uploadPath = '/uploads/marketing';
            $toUploadFile = $request->file('marketing_file');
            $data['file_path'] = $this->uploadFile($toUploadFile, $uploadPath);

        }


        $marketing->update($data);


        return redirect()->route('marketing.index')
            ->with('success', 'آیتم مارکنینگ مورد نظر با موفقیت ویرایش شد');

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */

    public function destroy(Marketing $marketing)

    {

        $marketing->delete();


        return redirect()->route('marketing.index')
            ->with('success', 'آیتم مارکتینگ مورد نظر با موفقیت حذف شد');

    }

}
