<?php


namespace App\Http\Controllers\Admin;


use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Models\City;
use App\Models\Business_type;
use App\Models\Business;
use App\Models\Customer;
use App\Models\SmsHistory;
use App\Rules\CheckBusinessPermission;


use Spatie\Permission\Models\Role;

use DB;

use Hash;

use Helpers;

use Illuminate\Support\Arr;


use Excel;
use Kavenegar;


class SmsController extends Controller

{


    function __construct()

    {

        $this->middleware('permission:access-sms-panel', ['only' => ['index', 'show']]);


    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function history(Request $request)

    {
    
    
    
    	$data = SmsHistory::select('*');

        $data = $data->orderBy('created_at','DESC')->paginate(20);
    
    
    

        return view('panel.admin.sms.history', compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 20);

    }
    
    
    
    public function showSendForm(Request $request)
    {
    
    
    
    
    	$permittedConditions = Helpers::getBusinessesAccessPermittedConditions(auth()->user());

        $businesses = Business::select('*')->where('id','<',0);

        foreach($permittedConditions as $key=>$condition){
            $toFireCondition1 = [];
            if(!empty($condition['ignore_cities']) && $condition['ignore_cities']){
                $toFireCondition1 = [
                    'operand'=>'LIKE',
                    'values' => '%'
                ];
            }else{
                $toFireCondition1 = [
                    'operand'=>'IN',
                    'values' => $condition['city_ids']
                ];
            }

            $toFireCondition2 = [];
            if(!empty($condition['ignore_creator_id']) && $condition['ignore_creator_id']){
                $toFireCondition2 = [
                    'operand'=>'LIKE',
                    'values' => '%'
                ];
            }else{
                $toFireCondition2 = [
                    'operand'=>'=',
                    'values' => $condition['creator_id']
                ];
            }
            $businesses = $businesses->orWhere(function($query) use($toFireCondition1,$toFireCondition2) {
                if($toFireCondition1['operand']=='IN'){
                    $query->whereIn('city_id', $toFireCondition1['values'])->where('creator_id',$toFireCondition2['operand'],$toFireCondition2['values']);
                }else{
                    $query->where('city_id',$toFireCondition1['operand'], $toFireCondition1['values'])->where('creator_id',$toFireCondition2['operand'],$toFireCondition2['values']);
                }
            });
        }
        
        //filters:
        if(!empty($request->city_id)){
        
        	$toFilterCityIDs = [];
        	
        	$toFilterCityIDs[] = $request->city_id;
        
        	$filterSelectedCity = City::findOrFail($request->city_id);
        	
        	$filterSelectedCityRegions = $filterSelectedCity->subsets()->get();
        		
		foreach($filterSelectedCityRegions as $filterSelectedRegion){
		
			$toFilterCityIDs[] = $filterSelectedRegion->id;
		
		}
	
		$businesses = $businesses->whereIn('city_id',$toFilterCityIDs);
        
        
        }else if(!empty($request->state_id)){
        	$toFilterCityIDs = [];
        	
        	$toFilterCityIDs[] = $request->state_id;
        	
        	$filterSelectedState = City::findOrFail($request->state_id);
        	$filterSelectedStateCities = $filterSelectedState->subsets()->get();
        	foreach($filterSelectedStateCities as $filterSelectedCity){
        		$toFilterCityIDs[] = $filterSelectedCity->id;
        		
        		$filterSelectedCityRegions = $filterSelectedCity->subsets()->get();
        		
        		foreach($filterSelectedCityRegions as $filterSelectedRegion){
        		
        			$toFilterCityIDs[] = $filterSelectedRegion->id;
        		
        		}
        	
        	}
        	
        	
        	$businesses = $businesses->whereIn('city_id',$toFilterCityIDs);
        	
        	
        }
        
        
        if(!empty($request->customer_degree)){
        
        	$businesses = $businesses->where('customer_degree',$request->customer_degree);
        
        }
        
        if(!empty($request->business_type_id)){
        
        	$businesses = $businesses->where('business_type_id',$request->business_type_id);
        
        }
        
        if(!empty($request->customer_recruitment_type)){
        
        	$businesses = $businesses->where('customer_recruitment_type',$request->customer_recruitment_type);
        
        }
        
        

        $contacts = $businesses->orderBy('created_at','DESC')->get()->pluck('mobiles');
    
    
    	$contactsToSendSMS =[];
    	foreach($contacts as $contact){
    		$decodedContact = json_decode($contact);
    		
    		foreach($decodedContact as $toSendSMSContact){
    		
    			if(!empty($toSendSMSContact)){
    			
    				$toSendSMSContact = Helpers::convertFaToEnNumbersV2($toSendSMSContact);
    			
    				if(preg_match("/^09[0-9]{9}$/", $toSendSMSContact)) {
				   $contactsToSendSMS[] = $toSendSMSContact; 
				}
    			
    			
    				
    			}
    		
    			
    		
    		}
    		
    		
    	}
    
    	$contactsToSendSMS = array_unique($contactsToSendSMS);
    
    
    
    
    
    
    
    
    
    	$permittedStateRegionCities = Helpers::getPermittedCities(auth()->user());

        $states = City::select('*');
        if(is_array($permittedStateRegionCities['permitted_states'])){
            $states = $states->whereIn('id',$permittedStateRegionCities['permitted_states']);
        }else if($permittedStateRegionCities['permitted_states']!='%'){
            $states = $states->whereIn('id',[]);
        }
        $states = $states->where('unit_type', 0)->where('unit_status', 1)->orderBy('unit_order', 'ASC')->get();

        $cities = [];
        if(!empty($request->state_id)){
        	//TODO: just show permitted cities
            $cities = City::where('state_id',$request->state_id)->where('unit_status',1)->orderBy('unit_order','ASC')->get();
        }

        
        
        $enums = config('enum');
        $businessTypes = Business_type::where('business_type_status',1)->orderBy('business_type_order','ASC')->get();

    
    
    
    
    
    
    
    
    	$data = [];

        return view('panel.admin.sms.send', compact('data','states','cities','enums','businessTypes','contactsToSendSMS'));

    }
    
    
    
    public function sendSMS(Request $request){
    
    	$this->validate($request, [

            'contacts.*' => 'required',
            'sms_text' => 'required'
        ]);



	$contactsToSendSMS = array_unique($request->contacts);
	$smsTextMessage = $request->sms_text;
	
	
	
	
	
	//Send SMS by call API
	
	
	
	//Call SMS API

	$resultMessage = "";
	$resultStatus = "success";
	$sentStatus = 0;
	try{
		    $sender = "1000200300200";
		    $message = $smsTextMessage;
		    $receptor = $contactsToSendSMS;
		    //$receptor = array("09396147736");
		    $result = Kavenegar::Send($sender,$receptor,$message);
		    if($result){
			$resultMessage = "پیام با موفقیت ارسال شد";
			$resultStatus = "success";
			$sentStatus = 1;
		    }else{
		    	$resultMessage = "خطای کاوه نگار: ارسال پیامک با مشکل مواجه شد";
			$resultStatus = "danger";
		    }
		}
		catch(\Kavenegar\Exceptions\ApiException $e){
			
			$resultMessage = "خطای کاوه نگار: ".$e->getMessage();
			$resultStatus = "danger";
		    // در صورتی که خروجی وب سرویس 200 نباشد این خطا رخ می دهد
		    echo $e->errorMessage();
		}
		catch(\Kavenegar\Exceptions\HttpException $e){
		    // در زمانی که مشکلی در برقرای ارتباط با وب سرویس وجود داشته باشد این خطا رخ می دهد
		    	$resultMessage = "خطای کاوه نگار: ".$e->getMessage();
			$resultStatus = "danger";
		    echo $e->errorMessage();
		}
	
	
	
	
	

        $data = [
        	'sms_text' => $smsTextMessage,
        	'contact_mobiles' => json_encode($contactsToSendSMS),
        	'creator_user_id' => auth()->user()->id,
        	'sent_status' => $sentStatus,
        ];

        $smsHistory = SmsHistory::create($data);


        return redirect()->route('sms.history')
            ->with($resultStatus, $resultMessage);
    
    }
    
    
    
    
    
    public function sendSpecialistInfo(Request $request){
    
    	request()->validate([
            'business_id' => ['nullable','integer','exists:businesses,id', new CheckBusinessPermission()],
            'customer_id' => 'required_without:business_id|integer|exists:customers,id',
        ]);
        
        
        $recieverMobile = Null;
        if(!empty($request->business_id)){
        	
        	$business = Business::findOrFail($request->business_id);
        	
        	$mobiles = $business->mobiles;
        	$mobiles = json_decode($mobiles,true);
        	
        	if(!empty($mobiles[0])){
        		$recieverMobile = $mobiles[0];
        	}
        	
        	
        
        }else if(!empty($request->customer_id)){
        	
        	$customer = Customer::findOrFail($request->customer_id);
        	
        	if(!empty($customer->mobile)){
        		$recieverMobile = $customer->mobile;
        	}
        
        }else{
        	abort(404);
        }
        
        
        if(empty($recieverMobile)){ //TODO: validate mobile number too
        
        	return redirect()->route('customers.index')
            		->with('danger', "شماره موبایل مشتری یافت نشد.");
        
        
        }else{
        
        	$smsTextMessage = "با سلام. مشخصات کارشناس فروش: ".auth()->user()->name.". لازیو";
       
       
       	$resultMessage = "";
       	$resultStatus = "success";
       	try{
		    $sender = "1000200300200";
		    $message = $smsTextMessage;
		    $receptor = array($recieverMobile);
		    //$receptor = array("09396147736");
		    $result = Kavenegar::Send($sender,$receptor,$message);
		    if($result){
			$resultMessage = "اطلاعات کارشناس فروش با موفقیت برای مشتری ارسال شد";
       		$resultStatus = "success";
		    }else{
		    	$resultMessage = "ارسال پیامک با مشکل مواجه شد";
       		$resultStatus = "danger";
		    }
		}
		catch(\Kavenegar\Exceptions\ApiException $e){
			
			$resultMessage = "خطای کاوه نگار: ".$e->getMessage();
       		$resultStatus = "danger";
		    // در صورتی که خروجی وب سرویس 200 نباشد این خطا رخ می دهد
		    echo $e->errorMessage();
		}
		catch(\Kavenegar\Exceptions\HttpException $e){
		    // در زمانی که مشکلی در برقرای ارتباط با وب سرویس وجود داشته باشد این خطا رخ می دهد
		    $resultMessage = "خطای کاوه نگار: ".$e->getMessage();
       		$resultStatus = "danger";
		    echo $e->errorMessage();
		}
       	
       	
       	
       	
       	
       	
       	
       	
       	
       	
       	
       	
       	
       	
       	return redirect()->back()
            		->with($resultStatus, $resultMessage);
        
        }
    
    }
    


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()

    {


        return view('panel.admin.customers.create');

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)

    {

        $this->validate($request, [

            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'business_name' => 'required|max:255',
            'city' => 'required|max:255',
            'address' => 'required|max:255',
            'phone' => 'required|max:255|unique:customers,phone',
            'mobile' => 'required|max:255|unique:customers,mobile',

        ]);


        $input = $request->all();

        $input['creator_id'] = auth()->user()->id;

        $customer = Customer::create($input);



        return redirect()->route('customers.index')
            ->with('success', 'مشتری جدید با موفقیت اضافه شد');

    }


    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */

    public function show($id)

    {

        $customer = Customer::find($id);

        return view('panel.admin.customers.show', compact('customer'));

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */

    public function edit($id)

    {

        $customer = Customer::select('*');
        if(auth()->user()->user_type=='SALES_SPECIALIST'){
            $customer = $customer->where('creator_id',auth()->user()->id);
        }
        $customer = $customer->where('id',$id)->first();



        return view('panel.admin.customers.edit', compact('customer'));

    }


    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)

    {

        $this->validate($request, [

            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'business_name' => 'required|max:255',
            'city' => 'required|max:255',
            'address' => 'required|max:255',
            'phone' => 'required|max:255',
            'mobile' => 'required|max:255',

        ]);


        $input = $request->all();




        $customer = Customer::select('*');
        if(auth()->user()->user_type=='SALES_SPECIALIST'){
            $customer = $customer->where('creator_id',auth()->user()->id);
        }
        $customer = $customer->where('id',$id)->first();



        $customer->update($input);





        return redirect()->route('customers.index')
            ->with('success', 'مشتری با موفقیت ویرایش شد');

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {

        $customer = Customer::select('*');
        if(auth()->user()->user_type=='SALES_SPECIALIST'){
            $customer = $customer->where('creator_id',auth()->user()->id);
        }
        $customer = $customer->where('id',$id)->first();

        $customer->delete();

        return redirect()->route('customers.index')
            ->with('success', 'مشتری با موفقیت حذف شد');

    }
    
    
    
    public function uploadExcelFile(Request $request){
    
    	ini_set('memory_limit', '-1');
    	 
    	$import = new CustomersImport;
	Excel::import($import, $request->excel_file);
	
	
    	
	$message = "تعداد ".
			$import->data['inserted']
		   ." مشتری با موفقیت اضافه شد. تعداد ".
		   $import->data['failed_to_insert']." مشتری با خطا مواجه شد و تعداد ".
		   $import->data['duplicate_count']." مشتری با شماره های زیر تکراری بودند: ".implode(' , ',$import->data['duplicate_list']);



	return redirect()->route('customers.index')
            ->with('success', $message);

    	
    }







}
