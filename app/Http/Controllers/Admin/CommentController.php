<?php


namespace App\Http\Controllers\Admin;


use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Models\CustomerComment;

use App\Models\CustomerCommentCategory;

use Spatie\Permission\Models\Role;

use DB;

use Hash;

use Helpers;

use Illuminate\Support\Arr;

use Excel;



class CommentController extends Controller

{


    function __construct()

    {

        $this->middleware('permission:comment-list|comment-create|comment-edit|comment-delete', ['only' => ['index', 'show','showCommentsReport']]);

        $this->middleware('permission:comment-create', ['only' => ['create', 'store']]);

        $this->middleware('permission:comment-edit', ['only' => ['edit', 'update']]);

        $this->middleware('permission:comment-delete', ['only' => ['destroy']]);

    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)

    {
    
    
    
    	$permittedConditions = Helpers::getBusinessesAccessPermittedConditions(auth()->user());

        $data = CustomerComment::select('*')->where('id','<',0);

        foreach($permittedConditions as $key=>$condition){
            

            $toFireCondition2 = [];
            if(!empty($condition['ignore_creator_id']) && $condition['ignore_creator_id']){
                $toFireCondition2 = [
                    'operand'=>'LIKE',
                    'values' => '%'
                ];
            }else{
                $toFireCondition2 = [
                    'operand'=>'=',
                    'values' => $condition['creator_id']
                ];
            }
            $data = $data->orWhere(function($query) use($toFireCondition2) {
                $query->where('creator_id',$toFireCondition2['operand'],$toFireCondition2['values']);
            });
        }

        $data = $data->orderBy('created_at','DESC')->paginate(20);
    

        return view('panel.admin.comments.index', compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 20);

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()

    {
		
	$commentCategories = CustomerCommentCategory::where('is_active',1)->orderBy('category_order','ASC')->get();

        return view('panel.admin.comments.create',compact('commentCategories'));

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)

    {

        $this->validate($request, [

            'customer_id' => 'nullable|integer|exists:customers,id',
            'comment_category_id' => 'required|integer|exists:customer_comment_categories,id',
            'comment_text' => 'required|max:1000',
            'customer_phone' => 'required|max:50',
            
        ]);


        $input = $request->all();

        $input['creator_id'] = auth()->user()->id;

        $comment = CustomerComment::create($input);



        return redirect()->route('comments.index')
            ->with('success', 'شکایت/پیشنهاد جدید با موفقیت اضافه شد');

    }


    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */

    public function show($id)

    {

        $comment = CustomerComment::find($id);

        return view('panel.admin.comments.show', compact('comment'));

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */

    public function edit($id)

    {

        $comment = CustomerComment::select('*');
        if(auth()->user()->user_type=='SALES_SPECIALIST'){
            $comment = $comment->where('creator_id',auth()->user()->id);
        }
        $comment = $comment->where('id',$id)->first();


	$commentCategories = CustomerCommentCategory::where('is_active',1)->orderBy('category_order','ASC')->get();

        return view('panel.admin.comments.edit', compact('comment','commentCategories'));

    }


    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)

    {

        $this->validate($request, [
            
            'customer_id' => 'nullable|integer|exists:customers,id',
            'comment_category_id' => 'required|integer|exists:customer_comment_categories,id',
            'comment_text' => 'required|max:1000',
            'customer_phone' => 'required|max:50',
            

        ]);


        $input = $request->all();




        $comment = CustomerComment::select('*');
        if(auth()->user()->user_type=='SALES_SPECIALIST'){
            $comment = $comment->where('creator_id',auth()->user()->id);
        }
        $comment = $comment->where('id',$id)->first();



        $comment->update($input);





        return redirect()->route('comments.index')
            ->with('success', 'شکایت/پیشنهاد با موفقیت ویرایش شد');

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {

        $comment = CustomerComment::select('*');
        if(auth()->user()->user_type=='SALES_SPECIALIST'){
            $comment = $comment->where('creator_id',auth()->user()->id);
        }
        $comment = $comment->where('id',$id)->first();

        $comment->delete();

        return redirect()->route('comments.index')
            ->with('success', 'شکایت/پیشنهاد با موفقیت حذف شد');

    }
    
    
    public function showCommentsReport(Request $request){
    
    
    
    	
    	$permittedConditions = Helpers::getBusinessesAccessPermittedConditions(auth()->user());


	$chart1Data = CustomerComment::select('*')->where('id','<',0);
        
        
        $chart2Data = CustomerComment::select(DB::raw('count(id) as `data`'),DB::raw('comment_category_id as `comment_category_id`'))->where('id','<',0);

        foreach($permittedConditions as $key=>$condition){
            

            $toFireCondition2 = [];
            if(!empty($condition['ignore_creator_id']) && $condition['ignore_creator_id']){
                $toFireCondition2 = [
                    'operand'=>'LIKE',
                    'values' => '%'
                ];
            }else{
                $toFireCondition2 = [
                    'operand'=>'=',
                    'values' => $condition['creator_id']
                ];
            }
            $chart1Data = $chart1Data->orWhere(function($query) use($toFireCondition2) {
                $query->where('creator_id',$toFireCondition2['operand'],$toFireCondition2['values']);
            });
            
            $chart2Data = $chart2Data->orWhere(function($query) use($toFireCondition2) {
                $query->where('creator_id',$toFireCondition2['operand'],$toFireCondition2['values']);
            });
            
            
            
        }
        
        
        //filters:
        if(!empty($request->comment_category_id)){
	
		$chart1Data = $chart1Data->where('comment_category_id',$request->comment_category_id);
		
		$chart2Data = $chart2Data->where('comment_category_id',$request->comment_category_id);
        
        
        }
        
        
        if(!empty($request->from_date)){
        
        	$fromDate = $request->from_date;
        	$fromDateGregorian=\Morilog\Jalali\Jalalian::fromFormat('Y/m/d', $fromDate)->toCarbon()->toDateTimeString();
        	
        	//$chart1Data = $chart1Data->whereDate('created_at' , '>=',$fromDateGregorian);
        	
        	$chart2Data = $chart2Data->whereDate('created_at' , '>=',$fromDateGregorian);
        	
        	
        	$fromDateJalali = $request->from_date;
        
        }else{
        	$minDate = CustomerComment::select(DB::raw('min(created_at) AS min_created_at'))->first();
        	$fromDateGregorian = $minDate->min_created_at;
        	$fromDateJalali = \Morilog\Jalali\Jalalian::forge($fromDateGregorian)->format('%Y/%m/%d');
        }
        
        if(!empty($request->to_date)){
        
        	$toDate = $request->to_date;
        	$toDateGregorian=\Morilog\Jalali\Jalalian::fromFormat('Y/m/d', $toDate)->toCarbon()->toDateTimeString();
        
        	//$chart1Data = $chart1Data->whereDate('created_at' , '<=',$toDateGregorian);
        	
        	$chart2Data = $chart2Data->whereDate('created_at' , '<=',$toDateGregorian);
        	
        	
        	$toDateJalali = $request->to_date;
        
        }else{
        	$maxDate = CustomerComment::select(DB::raw('max(created_at) AS max_created_at'))->first();
        	$toDateGregorian = $maxDate->max_created_at;
        	$toDateJalali = \Morilog\Jalali\Jalalian::forge($toDateGregorian)->format('%Y/%m/%d');
        	
        }
        
        
        
        $datePeriods = [];
        if($request->report_period==1){//Daily
        
        
        	$explodedDate = explode('/',$toDateJalali);
		$toCheckDate = $explodedDate[0].$explodedDate[1].$explodedDate[2];
		$fromHoldYear = NULL;
		$fromHoldMonth = NULL;
		$fromHoldDay = NULL;
		do{
			
			if(empty($fromHoldYear)){
				$explodedDate = explode('/',$fromDateJalali);
				$fromHoldYear = $explodedDate[0];
				$fromHoldMonth = $explodedDate[1]; 
				$fromHoldDay = $explodedDate[2]; 
			}else{
				if($fromHoldDay<=30){
					$fromHoldDay++;
				}else if($fromHoldMonth<=11){
					$fromHoldDay=1;
					$fromHoldMonth++;
				}else{
					$fromHoldDay=1;
					$fromHoldMonth = 1;
					$fromHoldYear++;
				}
			}
			
			
			if($fromHoldMonth<10){
				$fromHoldMonth = "0".((int)$fromHoldMonth);
			}
			
			if($fromHoldDay<10){
				$fromHoldDay = "0".((int)$fromHoldDay);
			}
			
			$datePeriods[] = $fromHoldYear."/".$fromHoldMonth."/".$fromHoldDay;
			
			
		
		
		}while($toCheckDate>($fromHoldYear.$fromHoldMonth.$fromHoldDay));
        
        
		
	}else{//Monthly
	
		$explodedDate = explode('/',$toDateJalali);
		$toCheckDate = $explodedDate[0].$explodedDate[1];
		
		$fromHoldYear = NULL;
		$fromHoldMonth = NULL;
		do{
			
			if(empty($fromHoldYear)){
				$explodedDate = explode('/',$fromDateJalali);
				$fromHoldYear = $explodedDate[0];
				$fromHoldMonth = $explodedDate[1]; 
			}else{
				if($fromHoldMonth<=11){
					$fromHoldMonth++;
				}else{
					$fromHoldMonth = 1;
					$fromHoldYear++;
				}
			}
			
			if($fromHoldMonth<10){
				$fromHoldMonth = "0".((int)$fromHoldMonth);
			}
			$datePeriods[] = $fromHoldYear."/".$fromHoldMonth;
			
			
		
		
		}while($toCheckDate>=$fromHoldYear.$fromHoldMonth);
		
	}
	
	
	$chart1DataArray = [];
	foreach($datePeriods as $key=>$datePeriod){
	
		$fromDateJalali = NULL;
		$toDateJalali = NULL;
		if($request->report_period==1){//Daily
			$fromDateJalali = $datePeriod;
			$toDateJalali = $datePeriod;
		}else{
			$fromDateJalali = $datePeriod."/01";
			$toDateJalali = $datePeriod."/31";
		}
		
		$fromDateGregorian=\Morilog\Jalali\Jalalian::fromFormat('Y/m/d', $fromDateJalali)->toCarbon()->toDateTimeString();
		$toDateGregorian=\Morilog\Jalali\Jalalian::fromFormat('Y/m/d', $toDateJalali)->toCarbon()->toDateTimeString();
		
		
		$tempObj = clone $chart1Data;
		
		$periodDate = $tempObj->whereDate('created_at' , '>=',$fromDateGregorian)->whereDate('created_at' , '<=',$toDateGregorian);
		$periodCount = $periodDate->count();
		
		if($periodCount>0){
			$chart1DataArray[$datePeriod] = $periodCount;
		}
		
		
	
	}
        
      
        
        
        $chart2Data = $chart2Data->groupBy('comment_category_id');
        
        
   

        $chart2Data = $chart2Data->get();
        
        $chart2DataArray = [];
	foreach($chart2Data as $key=>$chart2Element){
	
		if($chart2Element->data>0){
			
			$commentCategory = CustomerCommentCategory::findOrFail($chart2Element->comment_category_id);
			$chart2DataArray[$commentCategory->category_text] = $chart2Element->data;
		
		}
	
	}
	
        
        
        $commentCategories = CustomerCommentCategory::where('is_active',1)->orderBy('category_order','ASC')->get();
    
 
        return view('panel.admin.comments.report', compact('commentCategories','chart1DataArray','chart2DataArray','chart2Data'));
    	
    	
    	
    	
    	
    	
    }
    
    
  

}
