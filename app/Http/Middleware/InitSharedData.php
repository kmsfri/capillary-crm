<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Auth\Guard;
use \App\Models\UserGeoPath;


class InitSharedData
{




    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @param  string|null  ...$guards
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
     
     
    public function handle($request, Closure $next) {
    
	
		
    	if(auth()->user()){
    		$user = auth()->user();
    		
    		//$nowDateTime = \Carbon\Carbon::now()->toDateTimeString();
    		
    		$callTargetAlarms = \App\Models\CallTargetAlarm::where('specialist_user_id',$user->id)->where( 'alarm_at', '<=', \Carbon\Carbon::now() )->where('is_seen',0)->orderBy('alarm_at','DESC')->take(10)->get();
    		$request->call_target_alarms_list_total_count = \App\Models\CallTargetAlarm::where('specialist_user_id',$user->id)->where( 'alarm_at', '<=', \Carbon\Carbon::now() )->where('is_seen',0)->orderBy('alarm_at','DESC')->count();
			
    		$alarmsList = [];
    		
			
    		foreach($callTargetAlarms as $callTargetAlarm){
    		
    			$businessTitle = '';
    			$businessID = Null;	
    		
    			$business = \App\Models\Business::find($callTargetAlarm->business_id);
    			if(!empty($business)){
    				$businessTitle = $business->business_name;
    				$businessID = $business->id;
    			}
    			
    			$alarmsList[] = [
    				'call_target_alarm_id' => $callTargetAlarm->id,
    				'business_title' => $businessTitle ,
    				'business_id' => $businessID,
    				'customer_id' => $callTargetAlarm->customer_id,
    				'clue_id' => $callTargetAlarm->clue_id,
    				'call_target_alarm_description' => $callTargetAlarm->alarm_description,
    				'call_target_alarm_at' => $callTargetAlarm->alarm_at,
    			];
    		}
    		
    		$request->call_target_alarms_list = $alarmsList;
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			$currentDate = \Carbon\Carbon::now()->format('Y-m-d');
			$notificationAlarms = \App\Models\CallTargetAlarm::where('specialist_user_id',$user->id)->where( 'alarm_at', '<=', $currentDate." 23:59:59" )->where( 'alarm_at', '>=', $currentDate." 00:00:00" )->where('is_seen',0)->where('is_seen_in_notifications',0)->orderBy('alarm_at','DESC')->take(5)->get();
    		
    		$notificationsList = [];
    		
			
    		foreach($notificationAlarms as $notifyItem){
    		
    			$businessTitle = '';
    			$businessID = Null;	
    		
    			$business = \App\Models\Business::find($notifyItem->business_id);
    			if(!empty($business)){
    				$businessTitle = $business->business_name;
    				$businessID = $business->id;
    			}
    			
    			$notificationsList[] = [
    				'call_target_alarm_id' => $notifyItem->id,
    				'business_title' => $businessTitle ,
    				'business_id' => $businessID,
    				'alarm_description' => $notifyItem->alarm_description,
    				'clue_id' => $notifyItem->clue_id,
    				'call_target_alarm_description' => $notifyItem->alarm_description,
    				'call_target_alarm_at' => $notifyItem->alarm_at,
    			];
    		}
    		
    		$request->notificationsList = $notificationsList;
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			$now = \Carbon\Carbon::now();   
			$nowInDate = $now->toDateString();
			
			$request->current_day_walkings_count = UserGeoPath::where('user_id',$user->id)->whereDate('start_at',$nowInDate)->whereNotNull('end_at')->count();
    		$request->allowed_daily_walkings_count = $user->allowed_daily_walkings_count;
			
			$walkedPath = UserGeoPath::where('user_id',$user->id)->whereNull('end_at')->latest('id')->first();
			$request->current_walked_path = $walkedPath;
			
			
    	}
    
      return $next($request);
    }
    /* 
    public function handle(Request $request, Closure $next, ...$guards)
    {
    
    	dd('dddd');
    
        $guards = empty($guards) ? [null] : $guards;

        foreach ($guards as $guard) {
            if (Auth::guard($guard)->check()) {
                return redirect(RouteServiceProvider::HOME);
            }
        }

        return $next($request);
    }
    */
}
