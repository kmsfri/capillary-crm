<?php



namespace App\Models;



use Illuminate\Database\Eloquent\Factories\HasFactory;

use Illuminate\Database\Eloquent\Model;



class VisitRoute extends Model

{

    use HasFactory;



    /**

     * The attributes that are mass assignable.

     *

     * @var array

     */

    protected $fillable = [

        'visitor_user_id', 'creator_user_id', 'visit_at_date', 'visit_route', 'visit_route_description', 'visit_route_note'

    ];



    public function creator()
    {
        return $this->belongsTo(User::class,'creator_user_id');
    }
    
    
    
    public function visitor()
    {
        return $this->belongsTo(User::class,'visitor_user_id');
    }

}
