<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;

use Illuminate\Database\Eloquent\Model;


class Business_type extends Model

{

    use HasFactory;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     
     
    protected $table = "business_types";

    protected $fillable = [
    ];


    public function city()
    {
        return $this->hasMany(Business::class);
    }



}
