<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;

use Illuminate\Database\Eloquent\Model;


class Business extends Model

{

    use HasFactory;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [

        'business_images', 'ownership_doc_file_path', 'visit_card_file_path', 'business_license_file_path', 'business_cheque_file_path', 'partners', 'neighbors', 'mobiles', 'first_name', 'last_name', 'business_name', 'city_id',
        'address', 'shipping_name', 'national_code', 'postal_code', 'phone_number', 'latitude', 'longitude', 'buy_method', 'business_ownership', 'activity_amount', 'working_personnel', 'assets_estimate_rial', 'assets_estimate_box', 'suggested_credit_limit', 'business_description', 'sell_approved_by_seller', 'no_validation_required','approved_by_accountant','accountant_approving_description','approved_by_sales_specialist','sales_specialist_approving_description', 'creator_id',
        
        'business_type_id',
            'customer_degree',
            'customer_legal_type',
            'customer_recruitment_type',
            'copartner_name',
            'copartner_phone',
            'birth_date',
            'customer_id'

    ];


    public function city()
    {
        return $this->belongsTo(City::class);
    }
    
    
    public function businessType()
    {
        return $this->belongsTo(Business_type::class);
    }
    
    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }


    public function getFullStateCityRegion()
    {
        $level1 = $this->belongsTo(City::class, 'city_id')->first();
		if(empty($level1)){
			return '';
		}
        $fullStateCityRegionName = $level1->unit_name;

        $level2 = $level1->parent()->first();
        if (!empty($level2)) {
            $fullStateCityRegionName = $level2->unit_name . " - " . $fullStateCityRegionName;

            $level3 = $level2->parent()->first();
            if (!empty($level3)) {
                $fullStateCityRegionName = $level3->unit_name . " - " . $fullStateCityRegionName;
            }

        }

        return $fullStateCityRegionName;
    }
	 
	
	
	public function brands()
    {
        return $this->belongsToMany(Brand::class,'business_brands','business_id','brand_id')->withPivot('price');
    }


	public function invoices()
    {
        return $this->hasMany(Invoice::class,'business_id');
    }
    
    
    public function credits()
    {
        return $this->hasMany(BusinessCredit::class,'business_id');
    }
    
    
    public function creator()
    {
        return $this->belongsTo(User::class,'creator_id');
    }


}
