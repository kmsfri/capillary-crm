<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;

use Illuminate\Database\Eloquent\Model;


class InstanceInvoice extends Model

{

    use HasFactory;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [

        'clue_id',
        'creator_id',
		'clue_level',
		'file_path',
        'delivery_place_address',
        'sell_method',
        'cheque_time',
        'invoice_description',
        'invoice_items',
        'approved_by_accountant',
		'accountant_approving_description',
		'approved_by_sales_specialist',
		'sales_specialist_approving_description',
		'invoice_type'
        
    ];


    public function clue()
    {
        return $this->belongsTo(Clue::class);
    }


    public function creator()
    {
        return $this->belongsTo(User::class);
    }
    
    public function bijaks()
    {
        return $this->hasMany(Bijak::class);
    }


}
