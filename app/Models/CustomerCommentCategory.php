<?php



namespace App\Models;



use Illuminate\Database\Eloquent\Factories\HasFactory;

use Illuminate\Database\Eloquent\Model;



class CustomerCommentCategory extends Model

{

    use HasFactory;



    /**

     * The attributes that are mass assignable.

     *

     * @var array

     */

    protected $fillable = [

        'category_text', 'category_order', 'is_active'

    ];



    public function comments()
    {
        return $this->hasMany(CustomerComment::class,'customer_category_id');
    }

}
