<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;

use Illuminate\Database\Eloquent\Model;


class Invoice extends Model

{

    use HasFactory;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [

        'business_id',
        'creator_id',
        'delivery_place_address',
        'sell_method',
        'cheque_time',
        'invoice_description',
        'invoice_items',
        'approved_by_accountant',
	'accountant_approving_description',
	'approved_by_sales_specialist',
	'sales_specialist_approving_description',
	'invoice_type'
        
    ];


    public function business()
    {
        return $this->belongsTo(Business::class);
    }


    public function creator()
    {
        return $this->belongsTo(User::class);
    }
    
    public function bijaks()
    {
        return $this->hasMany(Bijak::class);
    }


}
