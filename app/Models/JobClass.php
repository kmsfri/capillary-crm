<?php



namespace App\Models;



use Illuminate\Database\Eloquent\Factories\HasFactory;

use Illuminate\Database\Eloquent\Model;



class JobClass extends Model

{

    use HasFactory;
 


    /**

     * The attributes that are mass assignable.

     *

     * @var array

     */

    protected $fillable = [

        'job_class_title','job_class_order', 'job_class_status'

    ];
    

	
	
	

}
