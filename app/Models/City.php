<?php



namespace App\Models;



use Illuminate\Database\Eloquent\Factories\HasFactory;

use Illuminate\Database\Eloquent\Model;



class City extends Model

{

    use HasFactory;



    /**

     * The attributes that are mass assignable.

     *

     * @var array

     */

    protected $fillable = [
        'unit_name',
        'state_id',
        'unit_type',
        'unit_order',
        'unit_status'
    ];


    public function parent()
    {
        return $this->belongsTo(City::class, 'state_id');
    }
    
    
    public function subsets()
    {
        return $this->hasMany(City::class, 'state_id');
    }

}
