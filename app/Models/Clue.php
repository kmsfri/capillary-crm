<?php



namespace App\Models;



use Illuminate\Database\Eloquent\Factories\HasFactory;

use Illuminate\Database\Eloquent\Model;



class Clue extends Model

{

    use HasFactory;
 


    /**

     * The attributes that are mass assignable.

     *

     * @var array

     */

    protected $fillable = [

        'clue_title','first_name','last_name','business_name','state','city','city_id','campaign_id','address','phone','phone2','phone3','mobile','mobile2','mobile3','customer_description','clue_status','clue_closed', 'customer_id', 'business_id','pre_invoice_id','invoice_id','creator_user_id','is_sepidar_user','sepidar_phone','sepidar_code','last_updated_user_id'

    ];
    
    
    public function creator()
    {
        return $this->belongsTo(User::class,'creator_user_id');
    }
	
	
	public function customer()
    {
        return $this->belongsTo(Customer::class,'customer_id');
    }
	
	
	
	public function business()
    {
        return $this->belongsTo(Business::class,'business_id');
    }
	
	public function preInvoice()
    {
        return $this->belongsTo(Invoice::class,'pre_invoice_id');
    }
	
	public function invoice()
    {
        return $this->belongsTo(Invoice::class,'invoice_id');
    }
	
	public function lastUpdaterUser()
    {
        return $this->belongsTo(User::class,'last_updated_user_id');
    }
	
	
	public function getFullStateCityRegion()
    {
        $level1 = $this->belongsTo(City::class, 'city_id')->first();
        $fullStateCityRegionName = $level1->unit_name;

        $level2 = $level1->parent()->first();
        if (!empty($level2)) {
            $fullStateCityRegionName = $level2->unit_name . " - " . $fullStateCityRegionName;

            $level3 = $level2->parent()->first();
            if (!empty($level3)) {
                $fullStateCityRegionName = $level3->unit_name . " - " . $fullStateCityRegionName;
            }

        }

        return $fullStateCityRegionName;
    }
	
	public function city()
    {
        return $this->belongsTo(City::class);
    }
	
	
	public function products()
    {
        return $this->belongsToMany(Product::class,'clue_products','clue_id','product_id');
    }

}
