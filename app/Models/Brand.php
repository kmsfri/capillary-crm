<?php



namespace App\Models;



use Illuminate\Database\Eloquent\Factories\HasFactory;

use Illuminate\Database\Eloquent\Model;



class Brand extends Model

{

    use HasFactory;



    /**

     * The attributes that are mass assignable.

     *

     * @var array

     */

    protected $fillable = [

        'brand_name', 'manufacturer', 'descriptions'

    ];
	
	
	public function businesses()
    {
        return $this->belongsToMany(Business::class,'business_brands','brand_id','business_id')->withPivot('price');
    }

}
