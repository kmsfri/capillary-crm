<?php



namespace App\Models;



use Illuminate\Database\Eloquent\Factories\HasFactory;

use Illuminate\Database\Eloquent\Model;



class ClueSteps extends Model

{

    use HasFactory;



    /**

     * The attributes that are mass assignable.

     *

     * @var array

     */

    protected $fillable = [

        'clue_id' , 'step_key', 'updated_by', 'update_description', 'updated_value'

    ];



    public function updaterUser()
    {
        return $this->belongsTo(User::class,'updated_by');
    }
    

	
	
	public function clue()
    {
        return $this->belongsTo(Clue::class,'clue_id');
    }

}
