<?php



namespace App\Models;



use Illuminate\Database\Eloquent\Factories\HasFactory;

use Illuminate\Database\Eloquent\Model;



class TeamMember extends Model

{

    use HasFactory;



    /**

     * The attributes that are mass assignable.

     *

     * @var array

     */

    protected $fillable = [

        'team_id', 'member_user_id'

    ];
    
    
    public function team()
    {
        return $this->belongsTo(Team::class,'team_id');
    }
    
    public function user() 
    {
        return $this->belongsTo(User::class,'user_id');
    }
    

}
