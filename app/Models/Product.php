<?php



namespace App\Models;



use Illuminate\Database\Eloquent\Factories\HasFactory;

use Illuminate\Database\Eloquent\Model;



class Product extends Model

{

    use HasFactory;



    /**

     * The attributes that are mass assignable.

     *

     * @var array

     */

    protected $fillable = [

        'product_name', 'product_code', 'manufacturer', 'product_model', 'stock_count', 'price', 'has_priority', 'details', 'descriptions','sepidar_barcode','sepidar_code','sepidar_product_type'

    ];
	
	
	public function clues()
    {
        return $this->belongsToMany(Clue::class,'clue_products','product_id','clue_id');
    }

}
