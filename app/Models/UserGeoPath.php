<?php



namespace App\Models;



use Illuminate\Database\Eloquent\Factories\HasFactory;

use Illuminate\Database\Eloquent\Model;



class UserGeoPath extends Model

{

    use HasFactory;
	
	protected $table = 'user_geo_paths';
 


    /**

     * The attributes that are mass assignable.

     *

     * @var array

     */

    protected $fillable = [

        'start_at','end_at', 'user_id', 'path_points','path_description'

    ];
    
    
    public function owner()
    {
        return $this->belongsTo(User::class,'user_id');
    }

}
