<?php



namespace App\Models;



use Illuminate\Database\Eloquent\Factories\HasFactory;

use Illuminate\Database\Eloquent\Model;



class CustomerComment extends Model

{

    use HasFactory;



    /**

     * The attributes that are mass assignable.

     *

     * @var array

     */

    protected $fillable = [

        'customer_id', 'comment_category_id', 'comment_text','customer_phone','creator_id'

    ];



    public function customer()
    {
        return $this->belongsTo(Customer::class,'customer_id');
    }
    
    public function category()
    {
        return $this->belongsTo(CustomerCommentCategory::class,'comment_category_id');
    }
    
    public function creator()
    {
        return $this->belongsTo(User::class,'creator_id');
    }

}
