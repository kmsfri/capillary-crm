<?php



namespace App\Models;



use Illuminate\Database\Eloquent\Factories\HasFactory;

use Illuminate\Database\Eloquent\Model;



class BusinessCompetitor extends Model

{

    use HasFactory;
 


    /**

     * The attributes that are mass assignable.

     *

     * @var array

     */

    protected $fillable = [

        'product_title','product_id', 'brand_id', 'country_id','company_price','market_price','checkout_period','market_stock_count','market_stock_percent','manufacturer_stock_count','manufacturer_stock_percent'
		,'importer_stock_count','importer_stock_percent','created_by','last_updated_by'

    ];
    
    
    public function creator()
    {
        return $this->belongsTo(User::class,'created_by');
    }
	
	public function product()
    {
        return $this->belongsTo(Product::class,'product_id');
    }
	
	public function brand()
    {
        return $this->belongsTo(Brand::class,'brand_id');
    }
	
	public function country()
    {
        return $this->belongsTo(Brand::class,'country_id');
    }
	
	public function lastUpdater()
    {
        return $this->belongsTo(User::class,'last_updated_by');
    }

}
