<?php



namespace App\Models;



use Illuminate\Database\Eloquent\Factories\HasFactory;

use Illuminate\Database\Eloquent\Model;



class Commission extends Model

{
	
	
	protected $table = "user_commissions";

    use HasFactory;



    /**

     * The attributes that are mass assignable.

     *

     * @var array

     */

    protected $fillable = [

        'creator_user_id','user_commission_month_id', 'sale_amount', 'commission_note','commission_amount','customer_code','customer_name','sale_type','settlement_date','visitor_amount_percent','specialist_amount_percent','visitor_amount','specialist_amount','invoice_date'

    ];



    public function creator()
    {
        return $this->belongsTo(User::class,'creator_user_id');
    }
    
    
    /*
    public function commissionOwner()
    {
        return $this->belongsTo(User::class,'user_id');
    }
	*/

}
