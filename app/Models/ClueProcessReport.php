<?php



namespace App\Models;



use Illuminate\Database\Eloquent\Factories\HasFactory;

use Illuminate\Database\Eloquent\Model;



class ClueProcessReport extends Model

{

    use HasFactory;



    /**

     * The attributes that are mass assignable.

     *

     * @var array

     */

    protected $fillable = [

        'creator_id' , 'clue_id', 'process_text'

    ];



    public function creator()
    {
        return $this->belongsTo(User::class,'creator_id');
    }
    

	
	
	public function clue()
    {
        return $this->belongsTo(Clue::class,'clue_id');
    }

}
