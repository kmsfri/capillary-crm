<?php



namespace App\Models;



use Illuminate\Database\Eloquent\Factories\HasFactory;

use Illuminate\Database\Eloquent\Model;



class CommissionMonth extends Model

{
	
	
	protected $table = "user_commission_months";

    use HasFactory;



    /**

     * The attributes that are mass assignable.

     *

     * @var array

     */

    protected $fillable = [

        'commission_month','user_id','sum_sale_amount','commission_amount','visitor_amount','specialist_amount'

    ];



    
    
    public function commissionOwner()
    {
        return $this->belongsTo(User::class,'user_id');
    }
	

}
