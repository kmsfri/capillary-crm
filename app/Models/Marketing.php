<?php



namespace App\Models;



use Illuminate\Database\Eloquent\Factories\HasFactory;

use Illuminate\Database\Eloquent\Model;



class Marketing extends Model

{

    use HasFactory;
 

	protected $table="marketing";


    /**

     * The attributes that are mass assignable.

     *

     * @var array

     */

    protected $fillable = [

        'marketing_title', 'do_at', 'marketing_description','file_path'

    ];

}
