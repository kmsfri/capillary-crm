<?php



namespace App\Models;



use Illuminate\Database\Eloquent\Factories\HasFactory;

use Illuminate\Database\Eloquent\Model;



class Campaign extends Model

{

    use HasFactory;



    /**

     * The attributes that are mass assignable.

     *

     * @var array

     */

    protected $fillable = [

        'campaign_title', 'campaign_description', 'start_at', 'end_at', 'campaign_source', 'creator_user_id', 'last_updated_by'

    ];
	
	
	public function creator()
    {
        return $this->belongsTo(User::class,'creator_user_id');
    }
	
	
	public function lastUpdator()
    {
        return $this->belongsTo(User::class,'last_updated_by');
    }

}
