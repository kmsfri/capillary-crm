<?php



namespace App\Models;



use Illuminate\Database\Eloquent\Factories\HasFactory;

use Illuminate\Database\Eloquent\Model;



class SpecialistCallTarget extends Model

{

    use HasFactory;



    /**

     * The attributes that are mass assignable.

     *

     * @var array

     */

    protected $fillable = [

        'specialist_user_id', 'creator_user_id', 'target_date', 'target_calls_list', 'target_call_description'

    ];



    public function creator()
    {
        return $this->belongsTo(User::class,'creator_user_id');
    }
    
    
    
    public function specialist()
    {
        return $this->belongsTo(User::class,'specialist_user_id');
    }

}
