<?php



namespace App\Models;



use Illuminate\Database\Eloquent\Factories\HasFactory;

use Illuminate\Database\Eloquent\Model;



class Team extends Model

{

    use HasFactory;



    /**

     * The attributes that are mass assignable.

     *

     * @var array

     */

    protected $fillable = [

        'team_name', 'target_invoices_count', 'target_invoices_amount', 'target_businesses_count','team_description','date_to','date_from', 'creator_id'

    ];
    
    
    public function members()
    {
        return $this->belongsToMany(User::class,'team_members','team_id','member_user_id');
    }

}
