<?php



namespace App\Models;



use Illuminate\Database\Eloquent\Factories\HasFactory;

use Illuminate\Database\Eloquent\Model;



class SmsHistory extends Model

{

    use HasFactory;



    /**

     * The attributes that are mass assignable.

     *

     * @var array

     */


    protected $table = "sms_history";


    protected $fillable = [

        'sms_text', 'contact_mobiles', 'sent_time','creator_user_id','sent_status'

    ];



    public function creator()
    {
        return $this->belongsTo(User::class,'creator_user_id');
    }

}
