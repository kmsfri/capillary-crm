<?php



namespace App\Models;



use Illuminate\Database\Eloquent\Factories\HasFactory;

use Illuminate\Database\Eloquent\Model;



class MarketingList extends Model

{

    use HasFactory;
 
	
	protected $table = "clue_marketing_list";

    /**

     * The attributes that are mass assignable.

     *

     * @var array

     */

    protected $fillable = [

        'clue_id', 'job_class_id','legal_status','activity_level','introduce_method','created_byIndex'

    ];
    
    
    public function creator()
    {
        return $this->belongsTo(User::class,'created_by');
    }
	
	public function jobClass()
    {
        return $this->belongsTo(JobClass::class,'job_class_id');
    }
	
	
	

}
