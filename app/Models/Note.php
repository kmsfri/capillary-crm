<?php



namespace App\Models;



use Illuminate\Database\Eloquent\Factories\HasFactory;

use Illuminate\Database\Eloquent\Model;



class Note extends Model

{

    use HasFactory;



    /**

     * The attributes that are mass assignable.

     *

     * @var array

     */

    protected $fillable = [

        'creator_id','referenced_by_user', 'business_id', 'customer_id', 'clue_id', 'note_text'

    ];



    public function creator()
    {
        return $this->belongsTo(User::class,'creator_id');
    }
    
	
	public function referencedBy()
    {
        return $this->belongsTo(User::class,'referenced_by_user');
    }
    
    public function business()
    {
        return $this->belongsTo(Business::class,'business_id');
    }
    
    
    public function customer()
    {
        return $this->belongsTo(Customer::class,'customer_id');
    }
	
	
	public function clue()
    {
        return $this->belongsTo(Clue::class,'clue_id');
    }

}
