<?php



namespace App\Models;



use Illuminate\Database\Eloquent\Factories\HasFactory;

use Illuminate\Database\Eloquent\Model;



class Customer extends Model

{

    use HasFactory;



    /**

     * The attributes that are mass assignable.

     *

     * @var array

     */

    protected $fillable = [

        'first_name', 'last_name', 'business_name','state', 'city', 'address', 'phone', 'phone2', 'phone3', 'mobile', 'mobile2', 'mobile3', 'customer_description', 'creator_id'
        ,'is_sepidar_user','sepidar_phone','sepidar_code'

    ];



    public function creator()
    {
        return $this->belongsTo(User::class,'creator_id');
    }

}
