<?php



namespace App\Models;



use Illuminate\Database\Eloquent\Factories\HasFactory;

use Illuminate\Database\Eloquent\Model;



class CallTargetAlarm extends Model

{

    use HasFactory;



    /**

     * The attributes that are mass assignable.

     *

     * @var array

     */

    protected $fillable = [

        'call_target_id', 'specialist_user_id', 'business_id', 'customer_id','clue_id', 'alarm_at', 'alarm_description', 'is_seen', 'is_seen_in_notifications'

    ];



    public function specialist()
    {
        return $this->belongsTo(User::class,'specialist_user_id');
    }
    
    
    
    public function callTarget()
    {
        return $this->belongsTo(SpecialistCallTarget::class,'call_target_id');
    }
    
    public function business()
    {
        return $this->belongsTo(Business::class,'business_id');
    }
    
    
    public function customer()
    {
        return $this->belongsTo(Customer::class,'customer_id');
    }
	
	public function clue()
    {
        return $this->belongsTo(Clue::class,'clue_id');
    }

}
