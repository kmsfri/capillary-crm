<?php



namespace App\Models;



use Illuminate\Database\Eloquent\Factories\HasFactory;

use Illuminate\Database\Eloquent\Model;



class Proceeding extends Model

{

    use HasFactory;
 


    /**

     * The attributes that are mass assignable.

     *

     * @var array

     */

    protected $fillable = [

        'proceeding_title', 'hold_at', 'hold_description','file_path'

    ];

}
