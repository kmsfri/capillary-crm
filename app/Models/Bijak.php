<?php



namespace App\Models;



use Illuminate\Database\Eloquent\Factories\HasFactory;

use Illuminate\Database\Eloquent\Model;



class Bijak extends Model

{

    use HasFactory;
 


    /**

     * The attributes that are mass assignable.

     *

     * @var array

     */

    protected $fillable = [

        'invoice_id','bijak_title', 'bijak_date', 'bijak_description','file_path','created_by'

    ];
    
    
    public function creator()
    {
        return $this->belongsTo(User::class,'created_by');
    }

}
