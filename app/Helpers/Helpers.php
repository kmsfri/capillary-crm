<?php


use App\Models\City;

class Helpers
{
	
	
    function convertFaToEnNumbersV2($string) {
	    $persian = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];
	    $arabic = ['٠', '١', '٢', '٣', '٤', '٥', '٦', '٧', '٨', '٩'];

	    $num = range(0, 9);
	    $convertedPersianNums = str_replace($persian, $num, $string);
	    $englishNumbersOnly = str_replace($arabic, $num, $convertedPersianNums);
	    
	    return $englishNumbersOnly;
    }
	
    public static function convertFaToEn($string) {
	return strtr($string, array('۰'=>'0', '۱'=>'1', '۲'=>'2', '۳'=>'3', '۴'=>'4', '۵'=>'5', '۶'=>'6', '۷'=>'7', '۸'=>'8', '۹'=>'9', '٠'=>'0', '١'=>'1', '٢'=>'2', '٣'=>'3', '٤'=>'4', '٥'=>'5', '٦'=>'6', '٧'=>'7', '٨'=>'8', '٩'=>'9'));
    }


    public static function getPermittedCities($user){

        $permittedRegions = [];
        if($user->user_type=='SALES_SPECIALIST' || $user->user_type=='DRIVER'){
            $permittedRegions = $user->cities()->get()->pluck('id')->toArray();

        }else if($user->user_type=='SALES_MANAGER'){
            $subUsers = $user->subUsers()->get();
            foreach($subUsers as $subUser){
                $permittedRegions = array_merge($permittedRegions,$subUser->cities()->get()->pluck('id')->toArray());
            }

        }
        /*else if($user->user_type=='CSO'){
            $managerUsers = $user->subUsers()->get();
            foreach($managerUsers as $managerUser){
                foreach($managerUser->subUsers()->get() as $specialistUser){
                    $permittedRegions = array_merge($permittedRegions,$specialistUser->cities()->get()->pluck('id')->toArray());
                }
            }

        }
        */
        else if($user->user_type=='ADMIN' || $user->user_type=='SALES_ACCOUNTANT' || $user->user_type=='CSO' || $user->user_type=='STOCK'){
            $permittedRegions = '%';
        }


        $permittedCities = [];
        if(is_array($permittedRegions)) {
            foreach ($permittedRegions as $permittedRegion) {
                $permittedCities[] = City::find($permittedRegion)->parent()->first()->id;
            }
        }


        $permittedStates = [];
        if(is_array($permittedCities)) {
            foreach ($permittedCities as $permittedCity) {
                $permittedStates[] = City::find($permittedCity)->parent()->first()->id;
            }
        }



        return [
            'permitted_states' => ($user->user_type=='ADMIN' || $user->user_type=='SALES_ACCOUNTANT' || $user->user_type=='CSO' || $user->user_type=='STOCK')?'%':$permittedStates,
            'permitted_cities' => ($user->user_type=='ADMIN' || $user->user_type=='SALES_ACCOUNTANT' || $user->user_type=='CSO' || $user->user_type=='STOCK')?'%':$permittedCities,
            'permitted_regions' => ($user->user_type=='ADMIN' || $user->user_type=='SALES_ACCOUNTANT' || $user->user_type=='CSO' || $user->user_type=='STOCK')?'%':$permittedRegions,
        ];

    }


    public static function make_slug($string, $separator = '-')
    {
        $string = trim($string);
        $string = mb_strtolower($string, 'UTF-8');
        $string = preg_replace("/[^a-z0-9_\s-ءاآؤئبپتثجچحخدذرزژسشصضطظعغفقكکگلمنوهی]/u", '', $string);
        $string = preg_replace("/[\s-_]+/", ' ', $string);
        $string = preg_replace("/[\s_]/", $separator, $string);

        return $string;
    }


    public static function space2Dash($string)
    {
        $string = trim($string);
        $string = str_replace(" ", "-", $string);
        $string = str_replace("_", "-", $string);

        return $string;
    }

    public static function br2nl($string)
    {
        return preg_replace('/\<br(\s*)?\/?\>/i', "", $string);
    }

    protected static function div($a, $b)
    {
        return (int)($a / $b);
    }


    public static function convert_date_j_to_g($timestamp, $str)
    {
        $temp = explode(" ", $timestamp);
        $timestamp = $temp[0];
        list($j_y, $j_m, $j_d) = explode('/', $timestamp);

        $g_days_in_month = array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
        $j_days_in_month = array(31, 31, 31, 31, 31, 31, 30, 30, 30, 30, 30, 29);


        $jy = (int)($j_y) - 979;
        $jm = (int)($j_m) - 1;
        $jd = (int)($j_d) - 1;

        $j_day_no = 365 * $jy + self::div($jy, 33) * 8 + self::div($jy % 33 + 3, 4);

        for ($i = 0; $i < $jm; ++$i)
            $j_day_no += $j_days_in_month[$i];

        $j_day_no += $jd;
        $g_day_no = $j_day_no + 79;

        $gy = 1600 + 400 * self::div($g_day_no, 146097); /* 146097 = 365*400 + 400/4 - 400/100 + 400/400 */
        $g_day_no = $g_day_no % 146097;

        $leap = true;
        if ($g_day_no >= 36525) { /* 36525 = 365*100 + 100/4 */
            $g_day_no--;
            $gy += 100 * self::div($g_day_no, 36524); /* 36524 = 365*100 + 100/4 - 100/100 */
            $g_day_no = $g_day_no % 36524;

            if ($g_day_no >= 365)
                $g_day_no++;
            else
                $leap = false;

        }

        $gy += 4 * self::div($g_day_no, 1461); /* 1461 = 365*4 + 4/4 */
        $g_day_no %= 1461;

        if ($g_day_no >= 366) {
            $leap = false;

            $g_day_no--;
            $gy += self::div($g_day_no, 365);
            $g_day_no = $g_day_no % 365;
        }

        for ($i = 0; $g_day_no >= $g_days_in_month[$i] + ($i == 1 && $leap); $i++)
            $g_day_no -= $g_days_in_month[$i] + ($i == 1 && $leap);
        $gm = $i + 1;
        $gd = $g_day_no + 1;
        if ($str) return $gy . '-' . $gm . '-' . $gd;
        return array($gy, $gm, $gd);
    }


    public static function convert_date_g_to_j($timestamp, $str)
    {

        if ($timestamp == Null || $timestamp == '') {
            return '0000/00/00';
        }
        $temp = explode(" ", $timestamp);
        $timestamp = $temp[0];
        list($g_y, $g_m, $g_d) = explode('-', $timestamp);

        $g_days_in_month = array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
        $j_days_in_month = array(31, 31, 31, 31, 31, 31, 30, 30, 30, 30, 30, 29);

        $gy = $g_y - 1600;
        $gm = $g_m - 1;
        $gd = $g_d - 1;

        $g_day_no = 365 * $gy + self::div($gy + 3, 4) - self::div($gy + 99, 100) + self::div($gy + 399, 400);

        for ($i = 0; $i < $gm; ++$i)
            $g_day_no += $g_days_in_month[$i];
        if ($gm > 1 && (($gy % 4 == 0 && $gy % 100 != 0) || ($gy % 400 == 0)))
            /* leap and after Feb */
            $g_day_no++;
        $g_day_no += $gd;

        $j_day_no = $g_day_no - 79;

        $j_np = self::div($j_day_no, 12053); /* 12053 = 365*33 + 32/4 */
        $j_day_no = $j_day_no % 12053;

        $jy = 979 + 33 * $j_np + 4 * self::div($j_day_no, 1461); /* 1461 = 365*4 + 4/4 */

        $j_day_no %= 1461;

        if ($j_day_no >= 366) {
            $jy += self::div($j_day_no - 1, 365);
            $j_day_no = ($j_day_no - 1) % 365;
        }

        for ($i = 0; $i < 11 && $j_day_no >= $j_days_in_month[$i]; ++$i)
            $j_day_no -= $j_days_in_month[$i];
        $jm = $i + 1;
        $jd = $j_day_no + 1;
        if ($str) return $jy . '/' . $jm . '/' . $jd;
        return array($jy, $jm, $jd);
    }


    public static function get_equal_str_month($month_order)
    {
        $month = [
            1 => "فروردین",
            2 => "اردیبهشت",
            3 => "خرداد",
            4 => "تیر",
            5 => "مرداد",
            6 => "شهریور",
            7 => "مهر",
            8 => "آبان",
            9 => "آذر",
            10 => "دی",
            11 => "بهمن",
            12 => "اسفند",
        ];

        return $month[$month_order];
    }


    public static function getRelationsInArray($data, $rel_name, $parent_fields = array())
    {
        $data = $data->toArray();
        $resp = array();
        foreach ($data as $key => $d) {
            foreach ($parent_fields as $v) {
                foreach ($d[$rel_name] as $key2 => $c) {
                    $d[$rel_name][$key2][$v] = $d[$v];
                }
            }
            $resp = array_merge($resp, $d[$rel_name]);
        }
        return $resp;
    }



    public static function getBusinessesAccessPermittedConditions($user){
		//creator_id limition is disabled

        $permittedConditions = [];


        if($user->user_type=='SALES_SPECIALIST' || $user->user_type=='DRIVER'){
			
			
			$subUserRegions = $subUser->cities()->get()->pluck('id')->toArray();
			$subUserCities = City::whereIn('id',$subUserRegions)->get()->pluck('state_id')->toArray();
			$subUserStates = City::whereIn('id',$subUserCities)->get()->pluck('state_id')->toArray();
			
			$toFilterCityIDs = array_merge($subUserRegions, $subUserCities, $subUserStates);
			
			$permittedConditions[] = [
					'ignore_creator_id'=>true,
                    'city_ids' => $toFilterCityIDs,
            ];

            $permittedConditions[] = [

                'creator_id' => [$user->id, $user->parent_user_id],
                'city_ids' => [],
                'ignore_cities'=>true,

            ];

        }else if($user->user_type=='SALES_MANAGER'){
            $subUsers = $user->subUsers()->get();
            foreach($subUsers as $subUser){
				
				$subUserRegions = $subUser->cities()->get()->pluck('id')->toArray();
				$subUserCities = City::whereIn('id',$subUserRegions)->get()->pluck('state_id')->toArray();
				$subUserStates = City::whereIn('id',$subUserCities)->get()->pluck('state_id')->toArray();
				
				$toFilterCityIDs = array_merge($subUserRegions, $subUserCities, $subUserStates);
				
                $permittedConditions[] = [
                    //'creator_id' => $subUser->id,
					'ignore_creator_id'=>true,
                    'city_ids' => $toFilterCityIDs,
                ];

            }


			$extraUsers = [];
			foreach($subUsers as $subUser){
				
                $extraUsers[] = $subUser->id;

            }

            $permittedConditions[] = [

                'creator_id' => array_merge([$user->id],$extraUsers),
                'city_ids' => [],
                'ignore_cities'=>true,

            ];
			
			

        }else if($user->user_type=='ADMIN' || $user->user_type=='SALES_ACCOUNTANT' || $user->user_type=='CSO' || $user->user_type=='STOCK'){
            $permittedConditions = [
                [
                    'ignore_creator_id'=>true,
                    'ignore_cities'=>true,
                    'creator_id' => '%',
                    'city_ids' => '%'
                ]
            ];
        }
        

        return $permittedConditions;

    }



}
