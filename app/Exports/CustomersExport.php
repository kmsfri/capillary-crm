<?php

  

namespace App\Exports;

  

use Maatwebsite\Excel\Concerns\FromCollection;

use Maatwebsite\Excel\Concerns\WithHeadings;

  

class CustomersExport implements FromCollection, WithHeadings

{

    protected $data;

  

    /**

     * Write code on Method

     *

     * @return response()

     */

    public function __construct($data)

    {

        $this->data = $data;

    }

  

    /**

     * Write code on Method

     *

     * @return response()

     */

    public function collection()

    {

        return collect($this->data);

    }

  

    /**

     * Write code on Method

     *

     * @return response()

     */

    public function headings() :array

    {

        return [

            'نام',

            'نام خانوادگی',

            'نام فروشگاه',
			
			'استان',
			
			'شهر',
			
			'آدرس',
			
			'تلفن ۱',
			
			'تلفن ۲',
			
			'تلفن ۳',
			
			'موبایل ۱',
			
			'موبایل ۲',
			
			'موبایل ۳',
			
			'توضیحات',
			
			'کد سپیدار',
			
			'تلفن در سپیدار',
			
			'شناسه شهر',
			
			'نام استان',
			
			'نام شهر',
			
			'نام استان درست',
			
			'نام شهر درست'
        ];

    }

}