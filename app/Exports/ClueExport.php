<?php

  

namespace App\Exports;

  

use Maatwebsite\Excel\Concerns\FromCollection;

use Maatwebsite\Excel\Concerns\WithHeadings;

  

class ClueExport implements FromCollection, WithHeadings

{

    protected $data;

  

    /**

     * Write code on Method

     *

     * @return response()

     */

    public function __construct($data)

    {

        $this->data = $data;

    }

  

    /**

     * Write code on Method

     *

     * @return response()

     */

    public function collection()

    {

        return collect($this->data);

    }

  

    /**

     * Write code on Method

     *

     * @return response()

     */

    public function headings() :array

    {

        return [

            'عنوان سرنخ',

            'نام',

            'نام خانوادگی',
			
			'نام فروشکاه',
			
			'شهر',
			
			'آدرس',
			
			'تلفن ۱',
			
			'تلفن ۲',
			
			'تلفن ۳',
			
			'موبایل ۱',
			
			'موبایل ۲',
			
			'موبایل ۳',
			
			'توضیحات سرنخ',
			
			'تاریخ ایجاد',
			
			'ایجاد شده توسط'
        ];

    }

}