<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserBusinessConfirmTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_business_confirm', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->nullable()->default(null);
            $table->unsignedBigInteger('business_id')->nullable()->default(null);
            $table->string('confirm_description')->nullable();
            $table->timestamps();
        });


        Schema::table('user_business_confirm', function(Blueprint $table) {
            $table->foreign('business_id', 'fk_business_id_user_business_confirm')->references('id')->on('businesses')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('user_id', 'fk_user_id_user_business_confirm')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_business_confirm');
    }
}
