<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_cities', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->nullable()->default(null);
            $table->unsignedBigInteger('city_id')->nullable()->default(null);


        });

        Schema::table('users_cities', function(Blueprint $table) {
            $table->foreign('city_id', 'fk_city_id_users_cities')->references('id')->on('cities')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('user_id', 'fk_user_id_users_cities')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_cities');
    }
}
