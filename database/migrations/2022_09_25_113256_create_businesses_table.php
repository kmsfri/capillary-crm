<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBusinessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('businesses', function (Blueprint $table) {
            $table->id();

            $table->string('first_name');
            $table->string('last_name');
            $table->string('business_name');
            $table->unsignedBigInteger('city_id')->nullable()->default(null);
            $table->text('address');
            $table->string('shipping_name');
            $table->string('national_code', 15);
            $table->string('postal_code', 15);
            $table->string('mobiles')->comment('in json format');
            $table->string('latitude', 30);
            $table->string('longitude', 30);
            $table->tinyInteger('buy_method')->comment('0: cash - 1: cheque');
            $table->tinyInteger('business_ownership')->comment('0: own - 1: rent');
            $table->string('ownership_doc_file_path');
            $table->string('business_license_file_path')->default(Null);
            $table->unsignedInteger('activity_amount')->nullable()->default(Null)->comment('based on day');
            $table->unsignedInteger('working_personnel')->nullable()->default(Null)->comment('based on the number of people');
            $table->unsignedBigInteger('assets_estimate_rial')->nullable()->default(Null)->comment('Based on Rial worth');
            $table->unsignedInteger('assets_estimate_box')->nullable()->default(Null)->comment('based on the number of cartoons available');
            $table->string('neighbors',500)->comment('json keys: name, phones, address');
            $table->string('partners',500)->comment('json keys: name, products, phones');
            $table->string('business_cheque_file_path');
            $table->string('business_images',500)->comment('json keys: img_path');
            $table->unsignedBigInteger('suggested_credit_limit')->nullable()->default(Null)->comment('Based on Rial');
            $table->text('business_description');
            $table->tinyInteger('sell_approved_by_seller')->nullable()->comment('0: not approved - 1: approved');
            $table->tinyInteger('sell_approved_by_critic')->nullable()->comment('0: not approved - 1: approved');
            $table->tinyInteger('sell_approved_by_accountant')->nullable()->comment('0: not approved - 1: approved');
            $table->tinyInteger('sell_approved_by_sales_manager')->nullable()->comment('0: not approved - 1: approved');
            $table->tinyInteger('sell_approved_by_general_manager')->nullable()->comment('0: not approved - 1: approved');

            $table->unsignedBigInteger('credit_amount')->nullable()->default(Null)->comment('Based on Rial');

            $table->timestamps();

        });


        Schema::table('businesses', function(Blueprint $table) {
            $table->foreign('city_id', 'fk_city_id_businesses')->references('id')->on('cities')->onUpdate('cascade')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('businesses');
    }
}
