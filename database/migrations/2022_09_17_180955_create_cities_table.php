<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cities', function (Blueprint $table) {
            $table->id();

            $table->string('unit_name');
            $table->unsignedBigInteger('state_id')->nullable()->default(Null);
            $table->unsignedTinyInteger('unit_type')->comment('0: state - 1:city - 2:region');
            $table->unsignedTinyInteger('unit_order')->default(0);
            $table->boolean('unit_status')->default(true);


            $table->timestamps();


            $table->foreign('state_id')
                ->references('id')->on('cities')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cities');
    }
}
