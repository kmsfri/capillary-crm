<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{route('showDashboard')}}" class="brand-link">
        <img src="{{asset('panel/static/img/laziologo2.png')}}" alt="AdminLTE Logo" class="brand-image elevation-3"
             style="background-color: white; width: 50px; border-radius: 1px;">
        <span class="brand-text font-weight-light">پنل مدیریت</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <div>
            <!-- Sidebar user panel (optional) -->
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                <div class="image">
                    <img src="{{asset('panel/static/img/user_icon.png')}}" class="img-circle elevation-2" alt="User Image">
                </div>
                <div class="info">
                    <a href="#" class="d-block">{{auth()->user()->name}}</a>
                </div>
            </div>

            <!-- Sidebar Menu -->
            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                    <!-- Add icons to the links using the .nav-icon class
                         with font-awesome or any other icon font library -->



			@can('show-visitor-cartable-report')
			<li class="nav-item has-treeview">
			
				<a href="{{route('visit.show-visitor-report', auth()->user()->id)}}" class="nav-link">
					<i class="nav-icon fa fa-pie-chart"></i>
					کارتابل
				</a>
			</li>
			@endcan
			
			
			@can('show-specialist-cartable-report')
			<li class="nav-item has-treeview">
			
				<a href="{{route('specialists.show-specialist-report', auth()->user()->id)}}" class="nav-link">
					<i class="nav-icon fa fa-pie-chart"></i>
					کارتابل
				</a>
			</li>
			@endcan

			@can('show-drivers-daily-visit')
			<li class="nav-item has-treeview">
				<a href="{{route('visit.driver-visitor.show-daily-flow')}}" class="nav-link">
					<i class="nav-icon fa fa-pie-chart"></i>
					ویزیت روزانه راننده
				</a>
			</li>
			@endcan
		
			
			
			
			
			@can('show-daily-visit')
			<li class="nav-item has-treeview">
		                <a href="#" class="nav-link">
		                    <i class="nav-icon fa fa-pie-chart"></i>
		                    <p>
		                        ویزیت
		                        <i class="right fa fa-angle-left"></i>
		                    </p>
		                </a>
		                
		                @can('show-daily-visit')
		                <ul class="nav nav-treeview">
		                    <li class="nav-item">
		                        <a href="{{route('visit.visitor.show-daily-flow')}}" class="nav-link">
		                            <i class="fa fa-circle-o nav-icon"></i>
		                            <p>ویزیت روزانه</p>
		                        </a>
		                    </li>

		                </ul>
		                @endcan
		                
		                @can('show-visit-lists-to-visitor')
		                <ul class="nav nav-treeview">
		                    <li class="nav-item">
		                        <a href="{{route('visit.show-users-daily-flow-list',auth()->user()->id)}}" class="nav-link">
		                            <i class="fa fa-circle-o nav-icon"></i>
		                            <p>گزارش ویزیت ها</p>
		                        </a>
		                    </li>

		                </ul>
						
						
						 <ul class="nav nav-treeview">
		                    <li class="nav-item">
		                        <a href="{{route('users.showUserWalkedPaths',auth()->user()->id)}}" class="nav-link">
		                            <i class="fa fa-circle-o nav-icon"></i>
		                            <p>لیست پیمایش ها</p>
		                        </a>
		                    </li>

		                </ul>
						
						
						
						
						
		                @endcan
						
                    	</li>
			@endcan
			
			
			
			
			
			
			
			
			
			
                    
                    
			@can('access-daily-call-targets-list-for-specialists')
			<li class="nav-item has-treeview">
				<a href="{{route('specialists.show-specialist-daily-call-targets-list')}}" class="nav-link"> 
					<i class="nav-icon fa fa-pie-chart"></i>
					لیست تارگت تماس روزانه
				</a>
			</li>
			@endcan
			
			@can('access-to-alarms-cartable')
			<li class="nav-item has-treeview">
				<a href="{{route('specialists.showAlarmsList')}}" class="nav-link">
					<i class="nav-icon fa fa-pie-chart"></i>
					کارتابل آلارم ها
				</a>
			</li>
			@endcan
			


		    
                    <li class="nav-item has-treeview">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fa fa-dashboard"></i>
                            <p>
                                کاربران
                                <i class="right fa fa-angle-left"></i>
                            </p>
                        </a>

                        <ul class="nav nav-treeview">
                        
                            @can('user-list')
                            <li class="nav-item">
                                <a href="{{route('users.index')}}" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>مدیریت کاربران</p>
                                </a>
                            </li>
                            @endcan
                            
                            @can('show-visitors')
                            <li class="nav-item">
                                <a href="{{route('visit.show-visitors')}}" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>ویزیتورها</p>
                                </a>
                            </li>
                            @endcan
                            
                            @can('show-drivers')
                            <li class="nav-item">
                                <a href="{{route('visit.show-drivers')}}" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>راننده ها</p>
                                </a>
                            </li>
                            @endcan
                            
                            @can('role-list')
                            <li class="nav-item">
                                <a href="{{route('roles.index')}}" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>مدیریت مجوزها</p>
                                </a>
                            </li>
                            @endcan
                            
                            @can('specialists-list')
                            <li class="nav-item">
                                <a href="{{route('specialists.index')}}" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>کارشناسان فروش</p>
                                </a>
                            </li>
                            @endcan
                            
                            @can('view-voip-calls-report')
                            <li class="nav-item">
                                <a href="{{route('specialists.show-voip-calls-report')}}" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>گزارش VOIP</p>
                                </a>
                            </li>
                            @endcan
                            
                            @can('team-management')
                            <li class="nav-item">
                                <a href="{{route('teams.index')}}" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>مدیریت تیم ها</p>
                                </a>
                            </li>
                            @endcan

                        </ul>

                    </li>
                    
                    
                    
                    
                    

                    <li class="nav-item has-treeview">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fa fa-pie-chart"></i>
                            <p>
                                کسب و کارها
                                <i class="right fa fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{route('businesses.index')}}" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>مدیریت کسب و کارها</p>
                                </a>
                            </li>
							
							<li class="nav-item">
                                <a href="{{route('businesses.showBusinessesReport')}}" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>گزارش کسب و کارها</p>
                                </a>
                            </li>
							


                        </ul>

                        
                        <!--
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{route('invoices.index',['invoice_type'=>'RETURNED'])}}" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>فاکتورهای مرجوعی</p>
                                </a>
                            </li>

                        </ul>
                        
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{route('invoices.index',['invoice_type'=>'DAMAGED'])}}" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>فاکتورهای ضایعاتی</p>
                                </a>
                            </li>

                        </ul>
                        -->
                    </li>
					
					
					
					
					
					
                    
					
					<li class="nav-item has-treeview">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fa fa-pie-chart"></i>
                            <p>
                                سرنخ ها
                                <i class="right fa fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                           
							
							<li class="nav-item">
                                <a href="{{route('clues.index')}}" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>مدیریت سرنخ ها</p>
                                </a>
                            </li>
							
							<li class="nav-item">
                                <a href="{{route('clues.showCluesReport')}}" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>گزارش سرنخ ها</p>
                                </a>
                            </li>
							
				

                        </ul>

                    </li>
					
					
					<li class="nav-item has-treeview">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fa fa-pie-chart"></i>
                            <p>
                                کمپین ها
                                <i class="right fa fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                           
							
							<li class="nav-item">
                                <a href="{{route('campaigns.index')}}" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>مدیریت کمپین ها</p>
                                </a>
                            </li>

							
				

                        </ul>

                    </li>
                    
                    
                    
                    <li class="nav-item has-treeview">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fa fa-pie-chart"></i>
                            <p>
                                فاکتورهای رسمی
                                <i class="right fa fa-angle-left"></i>
                            </p>
                        </a>
                        
                        
                        @can('invoice-list')
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{route('invoices.index')}}" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>پیش فاکتورها</p>
                                </a>
                            </li>

                        </ul>
                        @endcan
                        
                        
                        @can('primary-invoice-list')
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{route('invoices.show-primary-invoices')}}" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>فاکتورهای اصلی</p>
                                </a>
                            </li>

                        </ul>
                        @endcan
                        
                        
                        @can('rejected-invoice-list')
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{route('invoices.show-rejected-invoices')}}" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>فاکتورهای رد شده</p>
                                </a>
                            </li>

                        </ul>
                        @endcan
                     
                    </li>
                    
                    
                    
                    <li class="nav-item has-treeview">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fa fa-pie-chart"></i>
                            <p>
                                فاکتورهای غیررسمی
                                <i class="right fa fa-angle-left"></i>
                            </p>
                        </a>
                        
                        
                        @can('invoice-unofficial-list')
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{route('invoices.unofficial.index')}}" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>پیش فاکتورها</p>
                                </a>
                            </li>

                        </ul>
                        @endcan
                        
                        
                        @can('primary-invoice-unofficial-list')
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{route('invoices.unofficial.show-primary-invoices')}}" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>فاکتورهای اصلی</p>
                                </a>
                            </li>

                        </ul>
                        @endcan
                        
                        
                        @can('rejected-invoice-unofficial-list')
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{route('invoices.unofficial.show-rejected-invoices')}}" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>فاکتورهای رد شده</p>
                                </a>
                            </li>

                        </ul>
                        @endcan
                     
                    </li>
                    
                    
                    
                    <li class="nav-item has-treeview">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fa fa-pie-chart"></i>
                            <p>
                                فاکتورهای مرجوعی
                                <i class="right fa fa-angle-left"></i>
                            </p>
                        </a>
                        
                        
                        @can('invoice-list')
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{route('invoices.returned.index')}}" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>فاکتورهای درخواست شده</p>
                                </a>
                            </li>

                        </ul>
                        @endcan
                        
                        
                        @can('primary-invoice-returned-list')
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{route('invoices.returned.show-primary-invoices')}}" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>فاکتورهای تایید شده</p>
                                </a>
                            </li>

                        </ul>
                        @endcan
                        
                        
                        @can('rejected-invoice-returned-list')
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{route('invoices.returned.show-rejected-invoices')}}" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>فاکتورهای رد شده</p>
                                </a>
                            </li>

                        </ul>
                        @endcan
                     
                    </li>            


					@can('customer-list')
                    <li class="nav-item has-treeview">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fa fa-pie-chart"></i>
                            <p>
                                مشتریان
                                <i class="right fa fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{route('customers.index')}}" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>مدیریت مشتریان</p>
                                </a>
                            </li>

                        </ul>
                    </li>
					@endcan


                    <li class="nav-item has-treeview">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fa fa-pie-chart"></i>
                            <p>
                                پیکربندی
                                <i class="right fa fa-angle-left"></i>
                            </p>
                        </a>

                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{route('cities.index')}}" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>شهر و استان</p>
                                </a>
                            </li>

                        </ul>

                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{route('products.index')}}" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>مدیریت محصولات</p>
                                </a>
                            </li>

                        </ul>
						
						
						<ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{route('brands.index')}}" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>مدیریت برندها</p>
                                </a>
                            </li>
 
                        </ul>
						@can('configs-edit')
						<ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{route('configs.edit')}}" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>تاریخ چک و قیمتها</p>
                                </a>
                            </li>

                        </ul>
						@endcan
                    </li>
                    
                    
                    
                    @can('access-sms-panel')
                    <li class="nav-item has-treeview">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fa fa-pie-chart"></i>
                            <p>
                                پنل ارسال SMS
                                <i class="right fa fa-angle-left"></i>
                            </p>
                        </a>

                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{route('sms.showSendForm')}}" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>ارسال گروهی</p>
                                </a>
                            </li>

                        </ul>
                        
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{route('sms.history')}}" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>تاریخچه ارسال ها</p>
                                </a>
                            </li>

                        </ul>

                    </li>
                    @endcan
                    
                    
                    
                    @can('comment-list')
                    <li class="nav-item has-treeview">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fa fa-pie-chart"></i>
                            <p>
                                شکایات و پیشنهادات
                                <i class="right fa fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{route('comments.index')}}" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>مدیریت</p>
                                </a>
                            </li>


                        </ul>
                    </li>
                    @endcan
                    
                    <li class="nav-item has-treeview">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fa fa-pie-chart"></i>
                            <p>
                                گزارشات جامع
                                <i class="right fa fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{route('comments.showCommentsReport')}}" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>شکایات و پیشنهادات</p>
                                </a>
                            </li>
                            
                            
                            
                            <li class="nav-item">
                                <a href="{{route('customers.showCustomersReport')}}" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>مشتریان</p>
                                </a>
                            </li>
       

                        </ul>
                    </li>
					
					
					@can('proceeding-list')
                    <li class="nav-item has-treeview">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fa fa-pie-chart"></i>
                            <p>
                                صورتجلسه ها
                                <i class="right fa fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{route('proceedings.index')}}" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>مدیریت صورتجلسه ها</p>
                                </a>
                            </li>


                        </ul>
                    </li>
                    @endcan
					
					
                    <li class="nav-item has-treeview">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fa fa-pie-chart"></i>
                            <p>
                                فهرست بازاریابی
                                <i class="right fa fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{route('marketing.index')}}" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>مدیریت فهرست بازاریابی</p>
                                </a>
                            </li>


                        </ul>
                    </li>
					
					
					<li class="nav-item has-treeview">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fa fa-pie-chart"></i>
                            <p>
                                فهرست رقبا
                                <i class="right fa fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{route('competitors.index')}}" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>مدیریت فهرست رقبا</p>
                                </a>
                            </li>
							
							
							<li class="nav-item">
                                <a href="{{route('competitors.showCompetitorsReport')}}" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>گزارش رقبا</p>
                                </a>
                            </li>


                        </ul>
                    </li>
					
					
					@can('show-users-commission-list')
					@if(auth()->user()->user_type=='SALES_MANAGER')
					<li class="nav-item has-treeview">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fa fa-pie-chart"></i>
                            <p>
                                کمیسیون فروش ماهانه
                                <i class="right fa fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{route('commission.show-users-commission-months-list',['user_id'=>auth()->user()->id])}}" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>لیست ثبت شده</p>
                                </a>
                            </li>
							
							
							<li class="nav-item">
                                <a href="{{route('commission.set-monthly-commision',['user_id'=>auth()->user()->id])}}" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>ثبت کمیسیون فروش ماهانه</p>
                                </a>
                            </li>


                        </ul>
                    </li>
					@endif
					@endcan
                    

                </ul>
            </nav>
            <!-- /.sidebar-menu -->
        </div>
    </div>
    <!-- /.sidebar -->
</aside>
