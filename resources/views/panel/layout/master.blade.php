<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>پنل مدیریت | لازیو | LAZIO</title>
    
    
    @yield('additional_js_top')

    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="{{asset('panel/plugins/font-awesome/css/font-awesome.min.css')}}">
    <!-- IonIcons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('panel/dist/css/adminlte.min.css')}}">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

    <!-- bootstrap rtl -->
    <link rel="stylesheet" href="{{asset('panel/dist/css/bootstrap-rtl.min.css')}}">
    
    <link rel="stylesheet" href="{{asset('datepicker/jalalidatepicker.min.css')}}">
    
    
    <!-- template rtl version -->
    <link rel="stylesheet" href="{{asset('panel/dist/css/custom-style.css?param=2')}}">



    @yield('additional_js_top2')


    @yield('additional_css')

	@php
	$randBackgroundImageURL = 'https://crmmap.sepidarang.com/panel/static/img/lazio/img'.rand(1,10).'.jpg';
	@endphp

    <style>
        /* Show red borders when filled, but invalid */
        .required{
            border: solid thin hsl(0, 76%, 50%);
        }



        @media (max-width: 575px) {
            .content-wrapper{
                padding: 15px;
            }
        }
        
        
        
       tbody tr.success{
	  background-color: #dff0d8 !important;
	}

	tbody tr.success:hover{
	  background-color: #d0e9c6 !important;
	}

	tbody tr.danger{
	  background-color: #f2dede !important;
	}

	tbody tr.danger:hover{
	  background-color: #ebcccc !important;
	}


	tbody tr.info{
	  background-color: #d9edf7 !important;
	}

	tbody tr.info:hover{
	  background-color: #c4e3f3 !important;
	}


	
	
	.blinking{
  animation: 1s blink ease infinite;
  width: 20px;
  height: 20px;
  border-radius: 100%
}

.blinking-green {
    animation: 1s blink-green ease infinite;
  width: 20px;
  height: 20px;
  border-radius: 100%
}


@-webkit-keyframes "blink-green" {
  from, to {
    opacity: 0;
    background-color: green;
  }
  50% {
    opacity: 1;
  }
}

@-webkit-keyframes "blink" {
  0% {
    opacity: 0;
    background-color: red;
  }
  25% {
    opacity: 1;
    background-color: red;
  }
   
    50% {
    opacity: 0;
    background-color: darkorange; 
  }
  
  75%{
    opacity: 1;
    background-color: darkorange; 
  }
  100%{
    opacity: 0;
    background-color: darkorange;
  }
}


.h2, h2{
	font-size: 1.7rem;
}
.form-control{
	font-size: 0.9rem;
}
body{
	font-size: 0.9rem;
}
.btn{
	font-size: 0.9rem;
}

.content-wrapper .content-container table.table *{
	font-size: 13px;
}

.content-wrapper::before{
	
	
	content: "";
	background-image: url(https://crmmap.sepidarang.com/panel/static/img/lazio/img5.jpg);
	#background-image: url({{$randBackgroundImageURL}});
	background-size: 100% auto;
	position: fixed;
	top: 0px;
	right: 0px;
	bottom: 0px;
	left: 0px;
	opacity: 0.6;
	
}


.main-sidebar::before{
	

	content: "";
	background-image: url(https://crmmap.sepidarang.com/panel/static/img/lazio/img1.jpg);
	background-size: auto 100%;
	position: fixed;
	top: 0;
	right: 0px;
	bottom: 0px;
	left: 0px;
	opacity: 0.1;
	background-position: top;
	
}

.sidebar-dark-primary {
  background-color: #0f3b66;
}

.content-container{
	background-color: white;
	position: relative;
}

.card{
	background-color: transparent;
}

.highcharts-background{
	opacity: 0;
}

.main-sidebar .nav-link{
	z-index: 99999999;
}

.toasts{
	width: 400px;
	position: fixed;
	left: 0;
	z-index: 9999999;
}
.toast{
	background: #626262db;
	color: white;
	margin-left: 15px;
	margin-top: 7px;
	padding: 10px;
	border-radius: 5px;
}

.toast .btn-toast{
	
}

    </style>

</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to to the body tag
to get the desired effect
|---------------------------------------------------------|
|LAYOUT OPTIONS | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition sidebar-mini">
<div class="wrapper">

	@if(!empty(request()->notificationsList) && count(request()->notificationsList)>0)
	<div class="toasts">
		@foreach(request()->notificationsList as $notifyItem)
		<div class="toast" id="notify_toast_{{$notifyItem['call_target_alarm_id']}}">
			
			<button type="button" class="close float-left" aria-label="Close" onclick="$(this).parent().fadeToggle('slow','linear')">
			  <span aria-hidden="true">&times;</span>
			</button>
					
		
			<div class="row">
				<div class="col-lg-12">
					<p>
						{{$notifyItem['alarm_description']}}
					</p>
				</div>
			</div>
			<div class="row mt-1">
				<div class="col-lg-12">
					<button class="btn btn-sm btn-danger btn-toast" onclick="dontShowNotification({{$notifyItem['call_target_alarm_id']}})">دیگر نمایش نده</button>
					
					@if(!empty($notifyItem['clue_id']))
						<a href="{{ route('clues.showCartable',['clue_id'=>$notifyItem['clue_id']]) }}" class="btn btn-sm btn-info btn-toast" target="_blank">مشاهده سرنخ</a>
					@elseif(!empty($notifyItem['business_id']))
						<a href="{{ route('businesses.showCartable',['business_id'=>$notifyItem['business_id']]) }}" class="btn btn-sm btn-info btn-toast" target="_blank">مشاهده کسب و کار</a>
					@endif
					
					
				</div>
			</div>
		</div>
		@endforeach
		
		<div class="toast">
			
			<button type="button" class="close float-left" aria-label="Close" onclick="$(this).parent().fadeToggle('slow','linear')">
			  <span aria-hidden="true">&times;</span>
			</button>
					
			<div class="row">
				<div class="col-lg-12">
					<a style="width: 100%" href="{{ route('specialists.showAlarmsList') }}" class="btn btn-sm btn-warning btn-toast" target="_blank">مشاهده همه نوتیفیکیشن‌ها</a>
					
					
				</div>
			</div>
		</div>
		
	</div>
	@endif

    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="{{route('showDashboard')}}" class="nav-link">خانه</a>
            </li>
            <li class="nav-item d-sm-inline-block">
				@if(request()->current_day_walkings_count>=request()->allowed_daily_walkings_count)
					<a href="#" onclick="alert('در طول هر شبانه‌روز مجاز به {{request()->allowed_daily_walkings_count}} بار زدن اتمام پیمایش هستید. اگر مایل هستید پیمایش دیگری را ثبت کنید، با پشتیبانی تماس بگیرید.')" class="btn btn-sm btn-default mt-1">ثبت پیمایش</a>
				@elseif(empty(request()->current_walked_path))
					<a href="{{route('users.toggleWalkingStatus')}}" class="btn btn-sm btn-success mt-1">ثبت پیمایش</a>
				@else
					<a href="#" onclick="endWalkingProcess()" class="btn btn-sm btn-danger pull-right ml-3 mt-1">اتمام پیمایش</a>
					<div class="blinking-green pull-right mt-2 ml-1"></div>
					<span class="pull-right mt-2">
					<small>
					در حال ثبت پیمایش...
					</small>
					</span>
					
				@endif
			
                
            </li>


        </ul>

        <!-- SEARCH FORM -->
<!--
        <form class="form-inline ml-3">
            <div class="input-group input-group-sm">
                <input class="form-control form-control-navbar" type="search" placeholder="جستجو" aria-label="Search">
                <div class="input-group-append">
                    <button class="btn btn-navbar" type="submit">
                        <i class="fa fa-search"></i>
                    </button>
                </div>
            </div>
        </form>
-->

        <!-- Right navbar links -->
        <ul class="navbar-nav mr-auto">
            <!-- Messages Dropdown Menu -->
			<!--
            <li class="nav-item dropdown">
                <a class="nav-link" data-toggle="dropdown" href="#">
                    <i class="fa fa-comments-o"></i>
                    <span class="badge badge-danger navbar-badge">3</span>
                </a>
                <div class="dropdown-menu dropdown-menu-lg dropdown-menu-left">
                    <a href="#" class="dropdown-item">
                        <div class="media">
                            <img src="{{asset('panel/dist/img/user1-128x128.jpg')}}" alt="User Avatar" class="img-size-50 ml-3 img-circle">
                            <div class="media-body">
                                <h3 class="dropdown-item-title">
                                    کاربر تست ۱
                                    <span class="float-left text-sm text-danger"><i class="fa fa-star"></i></span>
                                </h3>
                                <p class="text-sm">متن تیکت اول...</p>
                                <p class="text-sm text-muted"><i class="fa fa-clock-o mr-1"></i> 4 ساعت قبل</p>
                            </div>
                        </div>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item">
                        <div class="media">
                            <img src="{{asset('panel/dist/img/user8-128x128.jpg')}}" alt="User Avatar" class="img-size-50 img-circle ml-3">
                            <div class="media-body">
                                <h3 class="dropdown-item-title">
                                    کاربر تست ۲
                                    <span class="float-left text-sm text-muted"><i class="fa fa-star"></i></span>
                                </h3>
                                <p class="text-sm">متن تیکت دوم...</p>
                                <p class="text-sm text-muted"><i class="fa fa-clock-o mr-1"></i> 4 ساعت قبل</p>
                            </div>
                        </div>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item">
                        <div class="media">
                            <img src="{{asset('panel/dist/img/user3-128x128.jpg')}}" alt="User Avatar" class="img-size-50 img-circle ml-3">
                            <div class="media-body">
                                <h3 class="dropdown-item-title">
                                    سارا وکیلی
                                    <span class="float-left text-sm text-warning"><i class="fa fa-star"></i></span>
                                </h3>
                                <p class="text-sm">متن تیکت سوم...</p>
                                <p class="text-sm text-muted"><i class="fa fa-clock-o mr-1"></i>4 ساعت قبل</p>
                            </div>
                        </div>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item dropdown-footer">مشاهده همه تیکت‌ها</a>
                </div>
            </li>
			-->
            <!-- Notifications Dropdown Menu -->
            <li class="nav-item dropdown">
                <a class="nav-link" data-toggle="dropdown" href="#">
                    <i class="fa fa-bell-o"></i>
                    
                    @if(!empty(request()->call_target_alarms_list_total_count) && request()->call_target_alarms_list_total_count>0)
                    <span class="badge badge-warning navbar-badge">{{request()->call_target_alarms_list_total_count}}</span>
                    @endif
                </a>
                <div class="dropdown-menu dropdown-menu-lg dropdown-menu-left">
                
                    @if(!empty(request()->call_target_alarms_list) && count(request()->call_target_alarms_list)>0)
		            <span class="dropdown-item dropdown-header">آخرین آلارم‌ها</span>
		            <div class="dropdown-divider"></div>
		            @foreach(request()->call_target_alarms_list as $callTargetAlarm)
					@php
					$callAlarmURL = "#";
					if(!empty($callTargetAlarm['business_id'])){
						$callAlarmURL = route('businesses.showCartable',$callTargetAlarm['business_id']);
					}else if(!empty($callTargetAlarm['customer_id'])){
						$callAlarmURL = route('customers.showCartable',$callTargetAlarm['customer_id']);
					}else if(!empty($callTargetAlarm['clue_id'])){
						$callAlarmURL = route('clues.showCartable',$callTargetAlarm['clue_id']);
					}
					@endphp
				    <a href="{{$callAlarmURL}}" class="dropdown-item" target="_blank" style="white-space: normal; display: inline-block" title="{{$callTargetAlarm['call_target_alarm_description']}}">
				        <i class="fa fa-envelope ml-2"></i>
						<span>
						{{$callTargetAlarm['call_target_alarm_description']}}
						</span>
				        <span class="float-left text-muted text-sm">{{ \Morilog\Jalali\Jalalian::forge($callTargetAlarm['call_target_alarm_at'])->ago() }}</span>
				    </a>
				    <div class="dropdown-divider"></div>
		            @endforeach
		            <a href="{{route('specialists.showAlarmsList')}}" class="dropdown-item dropdown-footer">مشاهده همه نوتیفیکیشن</a>
                    @else
                    <a href="#" class="dropdown-item">
                    	هیچ آلارم مشاهده نشده ای ندارید
                    </a>
                    @endif
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">خروج
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                    @csrf
                </form>
            </li>
        </ul>
    </nav>
    <!-- /.navbar -->

    @include('panel.layout.sections.side-menu')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <div class="content-container p-sm-5" style="background-color: #fffffff0;">
            @yield('main_content')
            <p class="text-center text-primary"><small>Developed by test.com</small></p>
        </div>
    </div>
    <!-- /.content-wrapper -->

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->


</div>
<!-- ./wrapper -->

<!-- Main Footer -->
<footer class="main-footer" style="position: relative; background-color: #343a40 ;color: white; text-align: left">
    <!-- Default to the left -->
    <strong>Copyright &copy; 2024</strong>
</footer>

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="{{asset('panel/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap -->
<script src="{{asset('panel/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- AdminLTE -->
<script src="{{asset('panel/dist/js/adminlte.js')}}"></script>

<!-- OPTIONAL SCRIPTS -->
<script src="{{asset('panel/plugins/chart.js/Chart.min.js')}}"></script>
<script src="{{asset('panel/dist/js/demo.js')}}"></script>
<script src="{{asset('panel/dist/js/pages/dashboard3.js')}}"></script>

@yield('additional_js_bottom')

<script>
    $('[type="submit"]').on('click', function () {
        // this adds 'required' class to all the required inputs under the same <form> as the submit button
        $(this)
            .closest('form')
            .find('[required]')
            .addClass('required');
    });


    $('input, textarea').on('keypress', function (e) {
        if($(this).val()!==''){
            $(this).removeClass('required');
        }
    });

    $('select').on('change', function (e) {
        if($(this).val()!==''){
            $(this).removeClass('required');
        }
    });


</script>



<script src="{{asset('datepicker/jalalidatepicker.min.js')}}"></script>



<script>
jalaliDatepicker.startWatch({
    minDate: "attr",
    maxDate: "attr",
    time: false,
    changeMonthRotateYear: true,
    showTodayBtn: true,
    showEmptyBtn: true,
    topSpace: 10,
    bottomSpace: 30,
    zIndex: 99999,
    marginRight: "100px",
    dayRendering(opt,input){
        return {
            isHollyDay:opt.day==1
        }
    }
});

$(document).ready(function() {
	
	
	
	// Check if geolocation is supported by the browser
	if ("geolocation" in navigator) {
	
	
		var lat="";
		var lng="";
		
			
			
			
			
			
			
			
		setInterval(intervalFunction = function()  {
			
			
			navigator.geolocation.getCurrentPosition(function(location) {
				lat = location.coords.latitude;
				lng = location.coords.longitude;
				
				

				$.ajax({
					url: '{{route('users.updateGeoPath',auth()->user()->id)}}',
					type: 'GET',
					cache:false,
					data: {"user_id":{{auth()->user()->id}}, "latitude":lat, "longitude":lng },
					success: function(data){

					   //alert(data);
					},
					error: function(data){
						//alert(data.responseText);
						
					}
				});
				
				return lat;
			},
			function(){
				@if(!empty(request()->current_walked_path))
					alert("دستگاه یا مرورگر شما قابلیت ثبت موفعیت مکانی را ندارد. لطفا دستگاه یا مرورگر خود را تغییر دهید، در غیر این صورت دکمه 'اتمام پیمایش' را بزنید!");
				@endif
			});
			
			
			
			
			
		
			
		}, 30 * 1000); // 60 * 1000 milsec

		intervalFunction();		
		
		
		
		
		
	}else{
		@if(!empty(request()->current_walked_path))
			alert("دستگاه یا مرورگر شما قابلیت ثبت موفعیت مکانی را ندارد. لطفا دستگاه یا مرورگر خود را تغییر دهید، در غیر این صورت دکمه 'اتمام پیمایش' را بزنید!");
		@endif
	}
	
	
	
	
	


});



function dontShowNotification(alarmID){
	
	$.ajax({
		url: '{{route('specialists.seenAlarmInNotifications')}}',
		type: 'GET',
		cache:false,
		data: {"alarm_id":alarmID },
		success: function(data){

		   //alert(data);
		   $('#notify_toast_'+alarmID).fadeToggle('slow');
		   
		},
		error: function(data){
			alert('خطا');
			
		}
	});
}



function endWalkingProcess(){
	
	
	
	var varMaxAllowedWalkingsCount = {{request()->allowed_daily_walkings_count}};
	if (confirm("توجه! شما در طول هر روز فقط "+varMaxAllowedWalkingsCount+" بار  مجاز به اتمام پیمایش هستید. در صورت اتمام پیمایش، شما امروز مجاز به شروع پیمایش جدید نخواهید بود.") == true) {
		
		var endWalkingProcessURL = "{{route('users.toggleWalkingStatus')}}";
		
		// Simulate an HTTP redirect:
		window.location.replace(endWalkingProcessURL);

	} else {
	  return;
	}
	
}


//document.getElementById("aaa").addEventListener("jdp:change", function (e) { console.log(e) });
</script>

@yield('overwrite_datepicker_js')



</body>
</html>
