@extends('panel.layout.master')
@section('additional_css')
    <style>
        .dashboard-sepid-arang-logo-container img{
            max-height: 43vh;
            max-width: 100%;
        }
		
		.small-box > .small-box-footer{
			color: #9d9d9d;
		}
    </style>
@stop
@section('main_content')

    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">داشبورد</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-left">
                        <li class="breadcrumb-item"><a href="#">خانه</a></li>
                        <li class="breadcrumb-item active">داشبورد</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
	
	<div class="container-fluid">
		<!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>{{$cluesCount}}</h3>

              <p>سرنخ ها</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <a href="{{route('clues.index')}}" class="small-box-footer">مشاهده لیست <i class="fa fa-arrow-circle-left"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>{{$businessesCount}}<sup style="font-size: 20px"></sup></h3>

              <p>کسب و کارها</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="{{route('businesses.index')}}" class="small-box-footer">مشاهده لیست <i class="fa fa-arrow-circle-left"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>{{$customersCount}}</h3>

              <p>مشتریان</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="{{route('customers.index')}}" class="small-box-footer">مشاهده لیست <i class="fa fa-arrow-circle-left"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>{{$primaryInvoicesCount}}</h3>

              <p>فاکتورهای رسمی</p>
            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
            <a href="{{route('invoices.show-primary-invoices')}}" class="small-box-footer">مشاهده لیست <i class="fa fa-arrow-circle-left"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>
	  
	  </div>
	
		@php
		$randNum1 = rand(1,10);
		$randNum2 = rand(1,10);
		@endphp
	
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body text-center dashboard-sepid-arang-logo-container">
							@php
								$filePath1 = "panel/static/img/lazio/img".$randNum1.".jpg";
								$filePath2 = "panel/static/img/lazio/img".$randNum2.".jpg";
							@endphp	
							
							<img style="width: 30%; opacity:0.9; border-radius: 5px" src="{{asset($filePath1)}}" class="img">
						
                            <img src="{{asset('panel/static/img/sepid-arang.jpg')}}" class="img">
													
							<img style="width: 30%; opacity:0.9; border-radius: 5px" src="{{asset($filePath2)}}" class="img">
                        </div>
                    </div>
                    <!-- /.card -->

                </div>

            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content -->



@endsection
