@extends('panel.layout.master')
@section('additional_js_top')

@endsection
@section('main_content')

@if ($message = Session::get('danger'))

        <div class="alert alert-danger">

            <p>{{ $message }}</p>

        </div>

    @endif

    <div class="row mb-md-5">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2>کمیسیون ماهانه برای کاربر: {{$user->name}}</h2>

            </div>

            <div class="pull-left">

                <a class="btn btn-primary" href="{{ route('showDashboard') }}"> بازگشت</a>

            </div>

        </div>

    </div>


    @if (count($errors) > 0)

        <div class="alert alert-danger">

            <strong>خطا!</strong> در مقادیر ورودی ایراداتی وجود دارد.<br><br>

            <ul>

                @foreach ($errors->all() as $error)

                    <li>{{ $error }}</li>

                @endforeach

            </ul>

        </div>

    @endif



    {!! Form::open(array('route' => 'commission.store-monthly-commission','method'=>'POST','id'=>'mainForm')) !!}

    <input type="hidden" name="user_id" value="{{$user->id}}">
    <input type="hidden" name="commission_id" value="{{!empty($commission)?$commission->id:''}}">
    <div class="row">
  
		<div class="col-xs-12 col-sm-12 col-md-6 form-input-container">

			<div class="form-group">

				<strong>در تاریخ:</strong>

				<input data-jdp type="text" name="commission_month" required {{(!empty($commissionDate)?'disabled':'')}} id="commission_month" value="{{old('commission_month',(!empty($commissionDate)?$commissionDate:'') )}}" class="form-control" placeholder="">
				
				@if(!empty($commissionDate))
					<input type="hidden" name="commission_month" value="{{$commissionDate}}">
				@endif


			</div>

		</div>
		
		
		<div class="col-xs-12 col-sm-12 col-md-6 form-input-container">

			<div class="form-group">

				<strong>مبلغ فروش:</strong>

				<input type="text" class="form-control" name="sale_amount" id="sale_amount" placeholder="" required value="{{ old('sale_amount',(!empty($commission)?$commission->sale_amount:'')) }}">

			</div>

		</div>
	
    </div>
	
	
	
	
	<div class="row">
  
		<div class="col-xs-12 col-sm-12 col-md-6 form-input-container">

			<div class="form-group">

				<strong>کد مشتری:</strong>

				<input type="text" class="form-control" name="customer_code" placeholder="" value="{{ old('customer_code',(!empty($commission)?$commission->customer_code:'')) }}">



			</div>

		</div>
		
		
		<div class="col-xs-12 col-sm-12 col-md-6 form-input-container">

			<div class="form-group">

				<strong>نام مشتری:</strong>

				<input type="text" class="form-control" name="customer_name" placeholder="" value="{{ old('customer_name',(!empty($commission)?$commission->customer_name:'')) }}">

			</div>

		</div>
	
    </div>
	
	
	
	
	<div class="row">
  
		<div class="col-xs-12 col-sm-12 col-md-6 form-input-container">

			<div class="form-group">

				<strong>نوع فروش:</strong>

				<select required  name='sale_type' id='sale_type' class='form-control pull-right'>
					<option @if(old('sale_type', isset($commission->sale_type) ? $commission->sale_type : '')=='commitment') selected @endif value="commitment" >فروش تعهدی</option>
					<option @if(old('sale_type', isset($commission->sale_type) ? $commission->sale_type : '')=='cash') selected @endif value="cash" >فروش نقدی</option>
					<option @if(old('sale_type', isset($commission->sale_type) ? $commission->sale_type : '')=='returned') selected @endif value="returned" >فاکتور برگشتی</option>
					<option @if(old('sale_type', isset($commission->sale_type) ? $commission->sale_type : '')=='discount') selected @endif value="discount" >تخفیف</option>
					<option @if(old('sale_type', isset($commission->sale_type) ? $commission->sale_type : '')=='internet') selected @endif value="internet" >فروش اینترنتی</option>
				</select>



			</div>

		</div>
		
		
		<div class="col-xs-12 col-sm-12 col-md-6 form-input-container">

			<div class="form-group">

				<strong>تاریخ تسویه:</strong>

				<select required  name='settlement_date' id='settlement_date' class='form-control pull-right'>
					<option @if(old('settlement_date', isset($commission->settlement_date) ? $commission->settlement_date : '')=='allowed') selected @endif value="allowed" >مجاز</option>
					<option @if(old('settlement_date', isset($commission->settlement_date) ? $commission->settlement_date : '')=='illegal') selected @endif value="illegal" >غیرمجاز</option>
					<option @if(old('settlement_date', isset($commission->settlement_date) ? $commission->settlement_date : '')=='returned') selected @endif value="returned" >فاکتور برگشتی</option>
					<option @if(old('settlement_date', isset($commission->settlement_date) ? $commission->settlement_date : '')=='internet') selected @endif value="internet" >اینترنتی</option>
				</select>
				
			</div>

		</div>
	
    </div>
	
	
	
	
	
	






	<div class="row">
  
		<div class="col-xs-12 col-sm-12 col-md-6 form-input-container">

			<div class="form-group">

				<strong>پورسانت کارشناس فروش(درصد):</strong>

				<input type="text" class="form-control" maxlength="3" name="specialist_amount_percent" id="specialist_amount_percent" placeholder="" required value="{{ old('specialist_amount_percent',(!empty($commission)?$commission->specialist_amount_percent:'70')) }}">



			</div>

		</div>
		
		
		<div class="col-xs-12 col-sm-12 col-md-6 form-input-container">

			<div class="form-group">

				<strong>پورسانت ویزیتور(درصد):</strong>

				<input type="text" class="form-control" maxlength="3" name="visitor_amount_percent" id="visitor_amount_percent" placeholder="" required value="{{ old('visitor_amount_percent',(!empty($commission)?$commission->visitor_amount_percent:'30')) }}">

			</div>

		</div>
	
    </div>











	
	
    	
    <div class="row">

        
        
        
        
        <div class="col-xs-12 col-sm-12 col-md-12 form-input-container">

        	<div class="form-group">

            	<strong>توضیحات:</strong>

            	<textarea class="form-control" style="height:100px" name="commission_note" placeholder="">{{ old('commission_note',(!empty($commission)?$commission->commission_note:'')) }}</textarea>

        	</div>

		</div>

        
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">

            <button type="submit" class="btn btn-primary">ثبت</button>

        </div>

    </div>
	
	
	
	 <div class="row">

        
        
        
        
        <div class="col-xs-12 col-sm-12 col-md-12 form-input-container mt-5">

			<h5>توضیحات:</h5>
        	<p>
				به ازای فاکتورهای تخفیف، برای کارشناس فروش و ویزیتور هیچ پورسانتی محاسبه نمی‌شود
			</p>
			<p>
				به ازای فاکتورهای برگشتی، پورسانت محاسبه شده از مجموع پورسانت‌های کارشناس فروش و ویزیتور کسر خواهد شد
			</p>
			
			<p>
				به ازای فاکتورهای فروش اینترنتی، برای کارشناس فروش و ویزیتور هیچ پورسانتی محاسبه نمی‌شود
			</p>
			<p>
				برای فاکتورهای فروش تعهدی و فروش نقدی، برای کارشناس فروش و ویزیتور طبق درصد وارد شده پورسانت محاسبه خواهد شد
			</p>
			<p>
				جمع درصدهای سهم کارشناس فروش و ویزیتور حتما باید عدد 100 باشد
			</p>

		</div>

        
   

    </div>

    {!! Form::close() !!}



	<script>
	
	document.querySelector("#specialist_amount_percent").addEventListener("keypress", function (evt) {
		if (evt.which < 48 || evt.which > 57)
		{
			evt.preventDefault();
		}
	});
	
	document.querySelector("#visitor_amount_percent").addEventListener("keypress", function (evt) {
		if (evt.which < 48 || evt.which > 57)
		{
			evt.preventDefault();
		}
	});
	
	document.querySelector("#sale_amount").addEventListener("keypress", function (evt) {
		if (evt.which < 48 || evt.which > 57)
		{
			evt.preventDefault();
		}
	});
	
	
	</script>

@endsection
