@extends('panel.layout.master')
@section('main_content')

    <div class="row mb-md-5">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2>کمیسیون های ماهانه ثبت شده برای کاربر: {{$user->name}} در ماه {{$commissionMonth->commission_month}}</h2>

            </div>

            <div class="pull-left">

                

            </div>

        </div>

    </div>


    @if ($message = Session::get('success'))

        <div class="alert alert-success">

            <p>{{ $message }}</p>

        </div>

    @endif
    
    
    


    <table class="table table-bordered">

        <tr>

            <th>ردیف</th>

            <th>ایجاد کننده</th>
            
            <th>زمان ثبت</th>
			
			<th>مشتری</th>
			
			<th>تاریخ فاکتور</th>
			
			<th>نوع فروش</th>
			
			<th>تاریخ تسویه</th>
			
			<th>مبلغ فروش</th>
			
			<th>خالص</th>
			
			<th>سهم ویزیتور</th>
			
			<th>سهم کارشناس</th>
			
			<th>جمع پورسانت</th>
			
			
			
			<th>توضیحات</th>
			
			@can('set-monthly-commision')
            <th>عملیات</th>
            @endcan

        </tr>

		@php
		$saleTypes = [
			'commitment'=>'فروش تعهدی',
			'cash'=>'فروش نقدی',
			'returned'=>'فروش برگشتی',
			'discount'=>'تخفیف',
			'internet'=>'فروش اینترنتی'
		];
		
		$settlementDates = [
			'allowed'=>'مجاز',
			'illegal'=>'غیرمجاز',
			'returned'=>'فاکتور برگشتی',
			'internet'=>'اینترنتی'
		];
		@endphp

        @foreach ($commissions as $key => $commission)

            <tr>

                <td>{{ ++$i }}</td>
				
                <td>{{ $commission->creator()->first()->name }}</td>
                
                <td>{{ \Morilog\Jalali\Jalalian::forge($commission->created_at)->format('%A, %d %B %Y') }}</td>
				
				<td>کد مشتری: {{ $commission->customer_code??'-' }}
					</br>
					نام مشتری: {{ $commission->customer_name??'-' }}
				</td>
				
				<td>{{ $commission->invoice_date??'-' }}</td>
				
				<td>{{ $saleTypes[$commission->sale_type]??'-' }}</td>
				
				<td>{{ $settlementDates[$commission->settlement_date]??'-' }}</td>
				
				<td>{{ number_format($commission->sale_amount) }}</td>
				
				<td>{{ number_format($commission->commission_amount) }}</td>
				
				<td>{{ number_format($commission->visitor_amount) }}
					</br>
					<span class="badge badge-info">
					{{ number_format($commission->visitor_amount_percent) }}%
					</span>
				</td>
				
				<td>{{ number_format($commission->specialist_amount) }}
					</br>
					<span class="badge badge-info">
					{{ number_format($commission->specialist_amount_percent) }}%
					</span>
					
				</td>
				
				<td>{{ number_format($commission->visitor_amount+$commission->specialist_amount) }}
					</br>
					<span class="badge badge-info">
					100%
					</span>
				</td>

				

				<td>{{ $commission->commission_note }}</td>
				
				@can('set-monthly-commision')
                <td>
				
					<form action="{{ route('commission.destroy',$commission->id) }}" method="POST">
						@csrf
					
						@method('DELETE')

						<button type="submit" class="btn btn-danger" onClick=' confirm("آیا مطمئنید؟"); '>حذف</button>
					</form>
				
	             <!--<a class="btn btn-warning" href="{{ route('commission.edit-commission',['commission_id'=>$commission->id]) }}">ویرایش</a>-->
                </td>
                @endcan

            </tr>

        @endforeach

    </table>


 

@endsection
