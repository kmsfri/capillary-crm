@extends('panel.layout.master')
@section('main_content')
    <div class="row mb-md-5">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2>کمیسیون های ماهانه ثبت شده برای کاربر: {{$user->name}}</h2>

            </div>

            <div class="pull-left">

                

            </div>

        </div>

    </div>


    @if ($message = Session::get('success'))

        <div class="alert alert-success">

            <p>{{ $message }}</p>

        </div>

    @endif
    
    
    


    <table class="table table-bordered">

        <tr>

            <th>ردیف</th>

            <th>ماه</th>

            <th>ایجاد کننده</th>
            
            <th>زمان ایجاد</th>
			
			<th>مبلغ فروش</th>
			
			<th>مبلغ کمیسیون</th>
			
			<th>سهم کارشناس</th>
			
			<th>سهم ویزیتور</th>
			
			<th>توضیحات</th>
			
			@can('set-monthly-commision')
            <th width="280px">عملیات</th>
            @endcan

        </tr>

        @foreach ($data as $key => $commission)

            <tr>

                <td>{{ ++$i }}</td>

                <td>{{ $commission->commission_month }}</td>

                <td>{{ $commission->creator()->first()->name }}</td>
                
                <td>{{ \Morilog\Jalali\Jalalian::forge($commission->created_at)->format('%A, %d %B %Y') }}</td>
				
				<td>{{ number_format($commission->sale_amount) }}</td>
				
				<td>{{ number_format($commission->commission_amount) }}</td>
				
				<td>{{ number_format($commission->specialist_amount) }}</td>
				<td>{{ number_format($commission->visitor_amount) }}</td>
				
				<td>{{ $commission->commission_note }}</td>
				
				@can('set-monthly-commision')
                <td>
				@csrf

					<form action="{{ route('commission.destroy',$commission->id) }}" method="POST">
						@csrf
					
						@method('DELETE')

						<button type="submit" class="btn btn-danger" onClick=' confirm("آیا مطمئنید؟"); '>حذف</button>
					</form>
					<!--<a class="btn btn-warning" href="{{ route('commission.set-monthly-commision',['user_id'=>$commission->user_id,'commission_date'=>$commission->commission_month]) }}">ویرایش</a>-->
					
                </td>
                @endcan

            </tr>

        @endforeach

    </table>


    {!! $data->links() !!}



@endsection
