@extends('panel.layout.master')
@section('main_content')
    <div class="row mb-md-5">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2>کمیسیون های ماهانه ثبت شده برای کاربر: {{$user->name}}</h2>

            </div>

            <div class="pull-left">

                

            </div>

        </div>

    </div>


    @if ($message = Session::get('success'))

        <div class="alert alert-success">

            <p>{{ $message }}</p>

        </div>

    @endif
	
	
	<div class="row">    
    <div class="col-lg-12 mb-md-5">
        <form action="{{route('commission.show-users-commission-months-list',$user->id)}}" method="GET">
        <div class="row">
	    
            
            
     
            
            
            <div class="col-xs-12 col-sm-12 col-md-2 form-input-container">

                <div class="form-group">

                    <strong>از تاریخ:</strong>

                    <input data-jdp type="text" name="from_date" id="from_date" value="{{ old('from_date',request()->from_date) }}" class="form-control" placeholder="">

                </div>

            </div>
            
            
            <div class="col-xs-12 col-sm-12 col-md-2 form-input-container">

                <div class="form-group">

                    <strong>تا تاریخ:</strong>
                    
                    <input data-jdp type="text" name="to_date" id="to_date" value="{{ old('to_date',request()->to_date) }}" class="form-control" placeholder="">

                </div>

            </div>
 
            
	
        </div>
        
        <div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 form-input-container">
			<div class="pull-left">
			<button type="submit" class="btn btn-success">فیلتر</button>
			</div>
		</div>
        </div>
        </form>
    </div>
	</div>
    
    
    


    <table class="table table-bordered">

        <tr>

            <th>ردیف</th>

            <th>ماه</th>
            
			<th>مجموع مبالغ</th>
			
			<th>مجموع کمیسیون</th>
			
			<th>مجموع سهم کارشناس</th>
			
			<th>مجموع سهم ویزیتور</th>
			
			<th>زمان ایجاد</th>
			


        </tr>

        @foreach ($data as $key => $commission)

            <tr>

                <td>{{ ++$i }}</td>

                <td>
				<a href="{{route('commission.show-users-month-commission-list',$commission->id)}}">
				{{ $commission->commission_month }}
				</a>
				</td> 

				
				<td>{{ number_format($commission->sum_sale_amount) }}</td>
				
				<td>{{ number_format($commission->commission_amount) }}</td>
				<td>{{ number_format($commission->visitor_amount) }}</td>
				<td>{{ number_format($commission->specialist_amount) }}</td>
				
				<td>{{ \Morilog\Jalali\Jalalian::forge($commission->created_at)->format('%A, %d %B %Y') }}</td>
				
				@can('set-monthly-commision')
				<!--
                <td>
	             <a class="btn btn-warning" href="{{ route('commission.set-monthly-commision',['user_id'=>$commission->user_id,'commission_date'=>$commission->commission_month]) }}">ویرایش</a>
                </td>
				-->
                @endcan

            </tr>

        @endforeach

    </table>


    {!! $data->links() !!}



@endsection
