@extends('panel.layout.master')
@section('main_content')
    <div class="row mb-md-5">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2>کمیسیون های ماهانه ثبت شده برای کاربر: {{$user->name}}</h2>

            </div>

            <div class="pull-left">

                

            </div>

        </div>

    </div>


    @if ($message = Session::get('success'))

        <div class="alert alert-success">

            <p>{{ $message }}</p>

        </div>

    @endif
    
    
    


    <table class="table table-bordered">

        <tr>

            <th>ردیف</th>

            <th>ماه</th>
            
			<th>مجموع مبالغ</th>
			
			<th>کمیسیون</th>
			
			<th>مجموع سهم کارشناس</th>
			
			<th>مجموع سهم ویزیتور</th>
			
			<th>زمان ایجاد</th>
			


        </tr>

        @foreach ($data as $key => $commission)

            <tr>

                <td>{{ ++$i }}</td>

                <td>
				<a href="{{route('commission.show-users-month-commission-list',$commission->id)}}">
				{{ $commission->commission_month }}
				</a>
				</td> 

				
				<td>{{ number_format($commission->sum_sale_amount) }}</td>
				
				<td>{{ number_format($commission->commission_amount) }}</td>
				
				<td>{{ number_format($commission->specialist_amount) }}</td>
				
				<td>{{ number_format($commission->visitor_amount) }}</td>
				
				<td>{{ \Morilog\Jalali\Jalalian::forge($commission->created_at)->format('%A, %d %B %Y') }}</td>
				
				@can('set-monthly-commision')
				<!--
                <td>
	             <a class="btn btn-warning" href="{{ route('commission.set-monthly-commision',['user_id'=>$commission->user_id,'commission_date'=>$commission->commission_month]) }}">ویرایش</a>
                </td>
				-->
                @endcan

            </tr>

        @endforeach

    </table>


    {!! $data->links() !!}



@endsection
