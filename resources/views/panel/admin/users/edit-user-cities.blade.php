@extends('panel.layout.master')
@section('additional_css')

<style>
	
	.region-card{
		cursor: pointer;
	}
	
	.region-card.checked{
		right: -3px;
		left: -3px;
		width: calc(100% + 6px);
		top: -3px;
		bottom: -3px;
		height: calc(94%);
		background-color: #e6e6e6;
	}
	
</style>

@endsection
@section('main_content')

	


    <div class="row mb-md-5">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2>ویرایش دسترسی مناطق برای کاربر: {{$user->name}}</h2>

            </div>

            <div class="pull-left">

                <a class="btn btn-primary" href="{{ route('users.index') }}"> بازگشت</a>

            </div>

        </div>

    </div>


    @if (count($errors) > 0)

        <div class="alert alert-danger">

            <strong>خطا!</strong> در مقادیر ورودی ایراداتی وجود دارد.<br><br>

            <ul>

                @foreach ($errors->all() as $error)

                    <li>{{ $error }}</li>

                @endforeach

            </ul>

        </div>

    @endif
    
    
    @if ($message = Session::get('success'))

        <div class="alert alert-success">

            <p>{{ $message }}</p>

        </div>

    @endif
    
    @if ($message = Session::get('danger'))

        <div class="alert alert-danger">

            <p>{{ $message }}</p>

        </div>

    @endif
    
    
    
    
    
    <div class="row">    
    <div class="col-lg-12 mb-md-5">
        <form action="{{route('users.add-city-to-user')}}" method="GET">
        
        
        <input type="hidden" name="user_id" value="{{$user->id}}">
        
        <div class="row">
	    
	    
	    
	    
	    
	    
	    <div class="col-xs-12 col-sm-12 col-md-3 form-input-container">

                <div class="form-group">

                    <strong>استان(*):</strong>

                    <select required  name='state_id' id='state_id' class='form-control pull-right' onChange="change_list('state_id','city_id','loading_gif_cities','{{route('getCities')}}',['region_id'])"> 				<!-- TODO: init vars -->
                        <option {{(!old('state_id', isset($business->state_id) ? $business->state_id : '')? 'selected' : '')}} value="">انتخاب کنید</option>
                        @foreach($states as $state)
                            <option @if(old('state_id', isset($business->state_id) ? $business->state_id : '')==$state->id) selected @endif value="{{$state->id}}" >{{$state->unit_name}}</option>
                        @endforeach
                    </select>

                </div>

            </div>



            <div class="col-xs-12 col-sm-12 col-md-3 form-input-container">

                <div class="form-group">

                    <strong>شهر:</strong>

                    <select  name='city_id' id='city_id' class='form-control pull-right' onChange="change_list('city_id','region_id','loading_gif_regions','{{route('getCities')}}')">
                        @if($cities)
                            <option {{(!old('city_id', isset($business->city_id) ? $business->city_id : '')? 'selected' : '')}} value="">انتخاب کنید</option>
                            @foreach($cities as $city)
                                <option @if(old('city_id', isset($business->city_id) ? $business->city_id : '')==$city->id) selected @endif value="{{$city->id}}" >{{$city->unit_name}}</option>
                            @endforeach
                        @endif
                    </select>

                </div>

            </div>


            <div class="col-xs-12 col-sm-12 col-md-3 form-input-container">

                <div class="form-group">

                    <strong>منطقه:</strong>

                    <select  name='region_id' id='region_id' class='form-control pull-right'>
                        @if($regions)
                            <option {{(!old('region_id', isset($business->region_id) ? $business->region_id : '')? 'selected' : '')}} value="">انتخاب کنید</option>
                            @foreach($regions as $region)
                                <option @if(old('region_id', isset($business->region_id) ? $business->region_id : '')==$region->id) selected @endif value="{{$region->id}}" >{{$region->unit_name}}</option>
                            @endforeach
                        @endif
                    </select>

                </div>

            </div>
            
            
            <div class="col-xs-12 col-sm-12 col-md-2 form-input-container">

                <div class="form-group">

                    <strong>&nbsp;</strong>

                    <button type="submit" class="btn btn-success" href="{{ route('users.add-city-to-user') }}">افزودن</button>

                </div>

            </div>
	    
	    
	    
	    
	    
	    

	
	
        </div>
        

        </form>
    </div>
</div>
    
    
    
    
    
    
    
    
    


    <div class="row">
    
    
    	@foreach($userRegions as $userRegion)
    	
    		<div class="col-xs-12 col-sm-12 col-md-3">
    		
    		
			<div class="card region-card" onclick="toggleRegionCard(this)">
			  <div class="card-body">
			  
				<div class="float-right">

					<p>{{$userRegion->unit_name}}</p>
					
					@php
					$city = $userRegion->parent()->first();
					$state = $city->parent()->first();
					@endphp
    					<p class="card-subtitle mb-2 text-muted">
    					
    					{{$city->unit_name}} - {{$state->unit_name}}
    					
    					</p>

					

				</div>

				<div class="float-left">
				

					<form action="{{ route('users.remove-user-city') }}" method="POST">

						<input type="hidden" name="user_id" value="{{$user->id}}">
						<input type="hidden" name="region_id" value="{{$userRegion->id}}">

						@csrf

						@method('DELETE')

						

					    <button type="submit" class="btn btn-sm btn-danger">حذف</button>
						
						
						<div class="form-check-inline">
						  <label class="form-check-label">
							<input type="checkbox" form="massDeletionForm" class="form-check-input region-card-checkbox hide" name="region_id[]" value="{{$userRegion->id}}">
						  </label>
						</div>

						
					</form>
				
				</div>
			  
			  
			  </div>
			</div>
    		

		    

		</div>
    	
    	@endforeach

        



    </div>


	<div class="row mt-5">
		<div class="col-xs-12 col-sm-12 col-md-3">
			<form id="massDeletionForm" action="{{ route('users.remove-user-city-mass') }}" method="POST">
			
				@method('DELETE')
			
				{!! csrf_field() !!}
				
				<input type="hidden" name="user_id" value="{{$user->id}}">
			
				<button type="submit" class="btn btn-danger">حذف چندتایی</button>
			
			</form>
		</div>
	</div>


@endsection

@section('additional_js_bottom')


<script>




function change_list(master_el_id,child_el_id,loading_gif_id,ajax_req_url,depend_element_ids = []){

    var e = document.getElementById(master_el_id);
    r_id = e.options[e.selectedIndex].value;

    if(r_id==""){
        $('#'+child_el_id).html('');
        return;
    }

    $('#'+master_el_id).prop('disabled', true);
    //$('#'+loading_gif_id).css('display', 'block'); //TODO: enable loadings

    token= $('body').find("input[name='_token']").val();

    $.ajax({
        url: ajax_req_url,
        type: 'GET',
        data: {"r_id":r_id, "_token":token },
        success: function(data){

            $('#'+child_el_id).html('');
            html_str='<option value="" >انتخاب کنید</option>';
            $('#'+child_el_id).append(html_str);
            $.each(data, function( index, value ) {
                html_str='<option value="'+value.id+'" >'+value.title+'</option>';
                $('#'+child_el_id).append(html_str);
            });

            depend_element_ids.forEach(function(){
                $('#'+depend_element_ids).empty();
            });

            $('#'+master_el_id).prop('disabled', false);
            //$('#'+loading_gif_id).css('display', 'none');
        },
        error: function(data){
            alert(data.responseText);
            alert('دریافت اطلاعات با خطا مواجه شد!');
            $('#'+master_el_id).prop('disabled', false);
            //$('#'+loading_gif_id).css('display', 'none');
        }
    });

}




function toggleRegionCard(obj){
	if($(obj).find('.region-card-checkbox').prop('checked')==true){
		$(obj).removeClass('checked');
		$(obj).find('.region-card-checkbox').prop('checked',false);
	}else{
		$(obj).addClass('checked');
		$(obj).find('.region-card-checkbox').prop('checked',true);
		
	}
}
</script>

@endsection
