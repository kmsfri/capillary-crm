@extends('panel.layout.master')
@section('main_content')
    <div class="row mb-md-5">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2>مسیرهای پیمایش شده توسط کاربر: {{$user->name}}({{$user->email}})</h2>

            </div>


        </div>

    </div>


    @if ($message = Session::get('success'))

        <div class="alert alert-success">

            <p>{{ $message }}</p>

        </div>

    @endif
    
    @if ($message = Session::get('danger'))

        <div class="alert alert-danger">

            <p>{{ $message }}</p>

        </div>

    @endif


    <table class="table table-bordered">

        <tr>

            <th>ردیف</th>

            <th>زمان شروع</th>
            <th>زمان پایان</th>
            
            <th width="280px">عملیات</th>

        </tr>
		@php
			$i=0;
		@endphp
        @foreach ($walkedPaths as $key => $path)

            <tr>

                <td>{{ ++$i }}</td>

				
				<td>{{ \Morilog\Jalali\Jalalian::forge($path->start_at)->format('%A, %d %B %Y ساعت H:i:s') }}</td>


				@if(!empty($path->end_at))
					<td>{{ \Morilog\Jalali\Jalalian::forge($path->end_at)->format('%A, %d %B %Y') }}</td>
				@else
					<td>در حال پیمایش</td>
				@endif

                <td><a href="{{route('users.showWalkedPath',$path->id)}}" target="_blank">مشاهده مسیر</a></td>


            </tr>

        @endforeach

    </table>


    {!! $walkedPaths->render() !!}



@endsection
