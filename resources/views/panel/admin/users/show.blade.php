@extends('panel.layout.master')
@section('main_content')

    <div class="row mb-md-5">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2> نمایش اطلاعات کاربر</h2>

            </div>

            <div class="pull-left">

                <a class="btn btn-primary" href="{{ route('users.index') }}"> بازگشت</a>

            </div>

        </div>

    </div>


    <div class="row">

        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>نام و نام خانوادگی:</strong>

                {{ $user->name }}

            </div>

        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>ایمیل:</strong>

                {{ $user->email }}

            </div>

        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>نقش ها:</strong>

                @if(!empty($user->getRoleNames()))

                    @foreach($user->getRoleNames() as $v)

                        <label class="badge badge-success">{{ $v }}</label>

                    @endforeach

                @endif

            </div>

        </div>

    </div>

@endsection
