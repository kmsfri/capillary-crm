@extends('panel.layout.master')
@section('additional_js_top')

@endsection

@section('additional_css')
<link rel = "stylesheet" href = "{{asset('css/leaflet.css')}}" />

<style>
#map{
	height: 600px;
}
</style>
@stop
@section('main_content')

@if ($message = Session::get('danger'))

        <div class="alert alert-danger">

            <p>{{ $message }}</p>

        </div>

    @endif

    <div class="row mb-md-5">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h3>مسیر پیمایش شده توسط کاربر: {{$user->name}}({{$user->email}})</h3>
				</br>
				<h5>تاریخ شروع پیمایش: {{ \Morilog\Jalali\Jalalian::forge($walkedPath->start_at)->format('%A, %d %B %Y ساعت H:i:s') }}</h5>
				<h5>تاریخ پایان پیمایش: {{ !empty($walkedPath->end_at)?\Morilog\Jalali\Jalalian::forge($walkedPath->end_at)->format('%A, %d %B %Y ساعت H:i:s'):'در حال پیمایش' }}</h5>



				<p>
					توضیح: نشان‌های آبی‌رنگ مربوط به مسیرهای ویزیت روزانه تعیین شده توسط کارشناس فروش هستند و نشان‌های قرمز رنگ مربوط به مسیر واقعی پیمایش شده توسط ویزیتور هستند.
				</p>
            </div>

            <div class="pull-left">

                <a class="btn btn-primary" href="{{ route('users.showUserWalkedPaths',$user->id) }}"> بازگشت</a>

            </div>

        </div>

    </div>
    


	@if ($message = Session::get('success'))

		<div class="alert alert-success">

		    <p>{{ $message }}</p>

		</div>

	@endif
    


    @if (count($errors) > 0)

        <div class="alert alert-danger">

            <strong>خطا!</strong> در مقادیر ورودی ایراداتی وجود دارد.<br><br>

            <ul>

                @foreach ($errors->all() as $error)

                    <li>{{ $error }}</li>

                @endforeach

            </ul>

        </div>

    @endif



    
    

    
    
    <div class="row">
        
	<div class="col-xs-12 col-sm-12 col-md-12 mt-md-3 mb-md-5">

		<div class="form-group">

		    <div id="map"></div>

		</div>

	</div>

    </div>



    
    


@endsection
@section('additional_js_bottom')
<script src = "{{asset('js/leaflet.js')}}"></script>

<!--<script src = "{{asset('panel/plugins/jquery-flowchart-master/jquery-ui.min.js')}}"></script>-->


    <script type="text/javascript">
	
		

		
			
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	//$(document).ready(function(){

            <?php
            	$mapCenter = [
            		'lat' => '35.709165',
            		'lng' => '51.371681',
            	];
            	
            	
		
            	if(!empty($locationsArray) && !empty($locationsArray[0]['lat']) && !empty($locationsArray[0]['lng'])){
            		$mapCenter = [
						'lat' => $locationsArray[0]['lat'],
						'lng' => $locationsArray[0]['lng'],
					];
            	}
            	
            ?>
			
			
			var userVisitRouteData = {!! json_encode($userVisitRouteData) !!};
            
            
            var visitGeoLocationsForShowOnMap = {!! json_encode($locationsArray) !!};


            // Creating map options




            let centerLat = '<?php echo $mapCenter['lat']; ?>';
            let centerLng = '<?php echo $mapCenter['lng']; ?>';



			pathCoords = connectTheDots(visitGeoLocationsForShowOnMap);

			
			window.onload=function(){
				
				var map = L.map("map");
				L.tileLayer("http://{s}.tile.osm.org/{z}/{x}/{y}.png").addTo(map);
				map.setView([centerLat, centerLng], 11);
				var myPolyline = L.polyline(pathCoords).addTo(map);
				
				
				
				
				
				var marker;

				
				visitGeoLocationsForShowOnMap.forEach(function(item){
				
					let latlng = {lat:item.lat, lng:item.lng};

					marker = L.marker(latlng,{title: item.title}).addTo(map).bindPopup(item.title);;
				});
				
				
				
				
				const markerIcon = L.icon({
				  iconSize: [25, 41],
				  iconAnchor: [10, 41],
				  popupAnchor: [2, -40],
				  // specify the path here
				  iconUrl: "https://unpkg.com/leaflet@1.5.1/dist/images/marker-icon.png",
				  shadowUrl: "https://unpkg.com/leaflet@1.5.1/dist/images/marker-shadow.png"
				});
				
				
				userVisitRouteData.forEach(function(item){
				
					
					let latlng = {lat: item.latitude, lng: item.longitude};
					marker = L.marker(latlng,
					
					{
						draggable: true, // Make the icon dragable
						title: item.title, // Add a title
						opacity: 0.7,
						icon: markerIcon // here assign the markerIcon var
					} // Adjust the opacity
					
					).addTo(map).bindPopup(item.title);
					
					
				});
				
				
				
				
			}
			
			
			
			


            

        //});
		
		
		
		
		
		
		
		function connectTheDots(data){
			var c = [];
			for(i in data) {
				var x = parseFloat(data[i].lat);
				var y = parseFloat(data[i].lng);
				c.push([x, y]);
			}
			return c;
		}
				
		
		
		
		
		
		
		
	</script>
	

	
	
@endsection
