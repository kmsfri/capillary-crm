@extends('panel.layout.master')
@section('main_content')

    <div class="row mb-md-5">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2>ویرایش کاربر</h2>

            </div>

            <div class="pull-left">

                <a class="btn btn-primary" href="{{ route('users.index') }}"> بازگشت</a>

            </div>

        </div>

    </div>


    @if (count($errors) > 0)

        <div class="alert alert-danger">

            <strong>خطا!</strong> در مقادیر ورودی ایراداتی وجود دارد.<br><br>

            <ul>

                @foreach ($errors->all() as $error)

                    <li>{{ $error }}</li>

                @endforeach

            </ul>

        </div>

    @endif


    {!! Form::model($user, ['method' => 'PATCH','route' => ['users.update', $user->id]]) !!}

    <div class="row">

        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>نام و نام خانوادگی:</strong>

                {!! Form::text('name', null, array('placeholder' => '','class' => 'form-control')) !!}

            </div>

        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>ایمیل:</strong>

                {!! Form::text('email', null, array('placeholder' => '','class' => 'form-control')) !!}

            </div>

        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>نوع کاربر:</strong>

                {!! Form::select('user_type', ['ADMIN'=>'ADMIN','CSO'=>'CSO','SALES_MANAGER'=>'SALES_MANAGER','SALES_SPECIALIST'=>'SALES_SPECIALIST','SALES_ACCOUNTANT'=>'SALES_ACCOUNTANT',
                'STOCK'=>'STOCK','DRIVER'=>'DRIVER'],$user->user_type, array('class' => 'form-control')) !!}

            </div>

        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>رمز عبور:</strong>

                {!! Form::password('password', array('placeholder' => '','class' => 'form-control')) !!}

            </div>

        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>تکرار رمز عبور:</strong>

                {!! Form::password('confirm-password', array('placeholder' => '','class' => 'form-control')) !!}

            </div>

        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>نقش:</strong>

                {!! Form::select('roles[]', $roles,$userRole, array('class' => 'form-control','multiple')) !!}

            </div>

        </div>
		
		<div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>شماره VOIP:</strong>

                {!! Form::text('voip_id', null, array('placeholder' => '','class' => 'form-control')) !!}

            </div>

        </div>
		
		
		<div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>وضعیت:</strong>

                {!! Form::select('is_active', ['1'=>'فعال','0'=>'غیرفعال'],$user->is_active, array('class' => 'form-control')) !!}

            </div>

        </div>


        <div class="col-xs-12 col-sm-12 col-md-12 text-center">

            <button type="submit" class="btn btn-primary">ثبت</button>

        </div>

    </div>

    {!! Form::close() !!}



@endsection
