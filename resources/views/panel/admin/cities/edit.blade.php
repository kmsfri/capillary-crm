@extends('panel.layout.master')
@section('main_content')

    <div class="row mb-md-5">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2>ویرایش
                    @if($city->unit_type==0)
                        استان
                    @elseif($city->unit_type==1)
                        شهر
                    @else
                        منطقه
                    @endif
                </h2>

            </div>

            <div class="pull-left">

                <a class="btn btn-primary" href="{{ route('cities.index') }}"> بازگشت</a>

            </div>

        </div>

    </div>


    @if (count($errors) > 0)

        <div class="alert alert-danger">

            <strong>خطا!</strong> در مقادیر ورودی ایراداتی وجود دارد.<br><br>

            <ul>

                @foreach ($errors->all() as $error)

                    <li>{{ $error }}</li>

                @endforeach

            </ul>

        </div>

    @endif


    {!! Form::model($city, ['method' => 'PATCH','route' => ['cities.update', $city->id]]) !!}

    <div class="row">

        <div class="col-xs-12 col-sm-12 col-md-4">

            <div class="form-group">

                <strong>عنوان:</strong>

                {!! Form::text('unit_name', null, array('placeholder' => '','class' => 'form-control')) !!}

            </div>

        </div>

        <div class="col-xs-12 col-sm-12 col-md-4">

            <div class="form-group">

                <strong>ترتیب:</strong>

                <select  name='unit_order' id='unit_order' class='form-control pull-right' >
                    @for($i=1; $i<=50; $i++)
                        <option @if(old('unit_order', isset($city->unit_order) ? $city->unit_order : '')==$i) selected @endif value="{{$i}}" >{{$i}}</option>
                    @endfor
                </select>

            </div>

        </div>

        <div class="col-xs-12 col-sm-12 col-md-4">

            <div class="form-group">

                <strong>وضعیت:</strong>

                <select  name='unit_status' id='unit_status' class='form-control pull-right' >
                    <option @if(old('unit_status', isset($city->unit_status) ? $city->unit_status : '')==0) selected @endif value="0" >غیرفعال</option>
                    <option @if(old('unit_status', isset($city->unit_status) ? $city->unit_status : '')==1) selected @endif value="1" >فعال</option>
                </select>

            </div>

        </div>


        <div class="col-xs-12 col-sm-12 col-md-12 text-center">

            <button type="submit" class="btn btn-primary">ثبت</button>

        </div>

    </div>

    {!! Form::close() !!}



@endsection
