@extends('panel.layout.master')
@section('main_content')
    <div class="row mb-md-5">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2>مدیریت
                @if($unitType==0)
                    استانها
                @elseif($unitType==1)
                    شهرها
                @else
                    مناطق
                @endif

                @if(!empty($unitParent))
                    (زیرمجموعه {{$unitParent->unit_name}})
                @endif
                </h2>

            </div>

            <div class="pull-left">

                <a class="btn btn-success" href="{{ route('cities.create',['unit_type'=>$unitType, 'parent_id'=>!empty($unitParent)?$unitParent->id:Null]) }}"> ایجاد
                    @if($unitType==0)
                        استان
                    @elseif($unitType==1)
                        شهر
                    @else
                        منطقه
                    @endif
                    جدید</a>

            </div>

        </div>

    </div>


    @if ($message = Session::get('success'))

        <div class="alert alert-success">

            <p>{{ $message }}</p>

        </div>

    @endif


    <table class="table table-bordered">

        <tr>

            <th>ردیف</th>

            <th>عنوان</th>

            <th>ترتیب</th>

            <th>وضعیت</th>

            <th width="280px">عملیات</th>

        </tr>

        @foreach ($data as $key => $unit)

            <tr>

                <td>{{ ++$i }}</td>

                <td>
                @if($unitParent && $unitParent->unit_type==1)
                        {{ $unit->unit_name }}
                @else
                    <a href="{{route('cities.index',['parent'=>$unit->id])}}">{{ $unit->unit_name }}</a>
                @endif
                </td>

                <td>{{ $unit->unit_order }}</td>
                <td>{!!  ($unit->unit_status==1)?'<label class="badge badge-success">فعال</label>':'<label class="badge badge-danger">غیرفعال</label>' !!}</td>


                <td>

{{--                    <a class="btn btn-info" href="{{ route('cities.show',$unit->id) }}">نمایش</a>--}}

                    <a class="btn btn-primary" href="{{ route('cities.edit',$unit->id) }}">ویرایش</a>

                    {!! Form::open(['method' => 'DELETE','route' => ['cities.destroy', $unit->id],'style'=>'display:inline']) !!}

                    {!! Form::submit('حذف', ['class' => 'btn btn-danger']) !!}

                    {!! Form::close() !!}

                </td>

            </tr>

        @endforeach

    </table>


    {!! $data->render() !!}



@endsection
