@extends('panel.layout.master')
@section('additional_css')


<style>

.create-invoice-col a{
  display:block;
}

</style>
    
@stop

@section('additional_js_top2')

<script src="{{asset('highcharts/highcharts.js')}}"></script>
<script src="{{asset('highcharts/modules/exporting.js')}}"></script>
<script src="{{asset('highcharts/modules/export-data.js')}}"></script>
<script src="{{asset('highcharts/modules/accessibility.js')}}"></script>

@endsection
@section('main_content')


    <div class="row mb-md-5">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2>گزارش کمپین: {{$campaign->campaign_title}}</h2>

            </div>

            <div class="pull-left">


            </div>

        </div>

    </div>
    
    
    @if ($message = Session::get('success'))

        <div class="alert alert-success">

            <p>{{ $message }}</p>

        </div>

    @endif
    
    @if ($message = Session::get('danger'))

        <div class="alert alert-danger">

            <p>{{ $message }}</p>

        </div>

    @endif
    
    
	
	
   
<!-- BEGIN BUSINESS INFORMATION -->


<div class="row mt-3 mb-3">

<div class="col-lg-12">


<div class="card border-success">
<div class="card-header">اطلاعات کمپین</div>
<div class="card-body">
				
				
				
				
				
				
				
				
				
				
			
				
<table class="table table-bordered">



<tr>
	<td colspan="2">
		<strong>عنوان:</strong>
                {{ $campaign->campaign_title }}
	</td>
</tr>

<tr>
	
	<td colspan="2">
		<strong>توضیحات:</strong>
		{{ $campaign->campaign_description }}
                
	</td>
	
</tr>



<tr>
	
	<td>
		<strong>تاریخ شروع:</strong>
		{{\Morilog\Jalali\Jalalian::forge($campaign->start_at)->format('%A, %d %B %Y در ساعت H:i:s')}}
                
	</td>
	
	<td>
		<strong>تاریخ پایان:</strong>
		{{\Morilog\Jalali\Jalalian::forge($campaign->end_at)->format('%A, %d %B %Y در ساعت H:i:s')}}
                
	</td>
</tr>



<tr>
	
	<td>
		<strong>ایجاد کننده:</strong>
		{{!empty($creatorUser)?$creatorUser->name."(".$creatorUser->email.")":'-'}}
                
	</td>
	
	<td>
		<strong>آخرین آپدیت توسط:</strong>
		{{!empty($lastUpdatorUser)?$lastUpdatorUser->name."(".$lastUpdatorUser->email.")":'-'}}
                
	</td>
</tr>

<tr>
	
	<td colspan="2">
		<strong>منبع کمپین:</strong>
		{{$campaign->campaign_source}}
                
	</td>

</tr>


<tr>
	<td>
		<strong>زمان ایجاد کمپین:</strong>
                {{\Morilog\Jalali\Jalalian::forge($campaign->created_at)->format('%A, %d %B %Y در ساعت H:i:s')}}
	</td>
	<td>
		<strong>زمان آخرین ویرایش کمپین:</strong>
                {{\Morilog\Jalali\Jalalian::forge($campaign->updated_at)->format('%A, %d %B %Y در ساعت H:i:s')}}
	</td>
</tr>



</table>
				
							
	
	
</div>
</div>

</div>

</div>


<!-- END CUSTOMER INFORMATION -->
   
   
   


<!--BEGIN FILTERS -->
<div class="row mt-3 mb-3">

<div class="col-lg-12">


<div class="card border-default">
<div class="card-header">فیلتر
</div>
<div class="card-body">
<div class="row">    
    <div class="col-lg-12 mb-md-5">
        <form action="{{route('campaigns.showCampaignsReport',$campaign->id)}}" method="GET">
        <div class="row">
	    
			
			<div class="col-xs-12 col-sm-12 col-md-2 form-input-container">

				<div class="form-group">

					<strong>وضعیت سرنخ:</strong>

					<select  name='clue_status' class='form-control pull-right'> 
					<option {{(!old('clue_status', isset(request()->clue_status) ? request()->clue_status : '')? 'selected' : '')}} value="">انتخاب کنید</option>
					@foreach($clueStatuses as $key=>$clueStatusTitle)
						<option @if(old('clue_status', isset(request()->clue_status) ? request()->clue_status : '')==$key) selected @endif value="{{$key}}" >{{$clueStatusTitle}}</option>
					@endforeach
					</select>

				</div>

			</div>
			
			
			<div class="col-xs-12 col-sm-12 col-md-2 form-input-container">

				<div class="form-group">

					<strong>نتیجه سرنخ:</strong>

					<select  name='clue_closed' class='form-control pull-right'> 
					<option {{(!old('clue_closed', isset(request()->clue_closed) ? request()->clue_closed : '')? 'selected' : '')}} value="">انتخاب کنید</option>
					@foreach($clueClosedStatuses as $key=>$statusTitle)
						<option @if(old('clue_closed', isset(request()->clue_closed) ? request()->clue_closed : '')==$key) selected @endif value="{{$key}}" >{{$statusTitle}}</option>
					@endforeach
					</select>

				</div>

			</div>
			
			
			
            
	
	
        </div>
        
        <div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 form-input-container">
			<div class="pull-left">
			<button type="submit" class="btn btn-success" href="{{ route('campaigns.showCampaignsReport',$campaign->id) }}">فیلتر</button>
			</div>
			

			
			
		</div>
        </div>
        </form>
    </div>
</div>

</div>
</div>
</div>
</div>


<!--END FILTERS -->	





<!-- BEGIN CLUES -->




<div class="row mt-3 mb-3">

<div class="col-lg-12">


<div class="card border-default">
<div class="card-header">سرنخ های مربوط به این کمپین
({{count($clues)}} سرنخ)
</div>
<div class="card-body">
				
				
				
				
@if(count($clues)==0)
	<p class="card-text">هیچ سرنخی یافت نشد</p>
@else				
				
	<table class="table table-bordered">

        <tr>

            <th>ردیف</th>

            <th>عنوان سرنخ</th>
            <th>وضعیت</th>
			<th>نتیجه</th>
            <th>کاربر ایجاد کننده</th>
			<th>تاریخ ایجاد</th>
			<th>تاریخ آخرین بروزرسانی</th>
            
          
        </tr>

		@php
		$i=0;
		@endphp
        @foreach ($clues as $key => $clue)

            <tr>

                <td>{{ ++$i }}</td>

                <td>
                
                
					<a target="_blank" class="" href="{{ route('clues.showCartable',['clue_id'=>$clue->id]) }}">
					
					
					{{ $clue->clue_title }}
					
					
					</a>
                
                </td>
				
				
				
				<td>
				@php
					
					$clueTitlesArr = [
						'clue' => 'ثبت سرنخ',
						'customer' => 'ثبت مشتری',
						'validation' => 'اعتبارسنجی',
						'sale' => 'فروش',
						'pre_invoice'=>'‍‍پیش فاکتور',
						'invoice'=>'فاکتور',
						'return_from_sale' => 'برگشت از فروش'
					];
					$clueStatusTitle = $clueTitlesArr[$clue->clue_status];
				@endphp
				{{ $clueStatusTitle }}
				</td>
				
				
				
				<td>
				@php
					
					$clueClosedArr = [
						0 => 'باز',
						1 => 'بسته',
					];
					$clueClosedTitle = $clueClosedArr[$clue->clue_closed];
				@endphp
				@php
					$class="badge badge-success";
					if($clue->clue_closed==1){
						$class="badge badge-danger";
					}
				@endphp
				<span class="{{$class}}">
				{{ $clueClosedTitle }}
				</span>
				</td>
				
				
				
				<td>
                {{$clue->creator()->first()->name;}}
                </td>
				
				<td>{{ \Morilog\Jalali\Jalalian::forge($clue->created_at)->format('%A, %d %B %Y در ساعت H:i:s') }}</td>
				
				<td>{{ !empty($clue->updated_at)?(\Morilog\Jalali\Jalalian::forge($clue->updated_at)->format('%A, %d %B %Y در ساعت H:i:s')):'-' }}</td>
				
				
				
  

            </tr>

        @endforeach

    </table>
	{!! $clues->links() !!}		
				
@endif			
				
				
				
	
	
</div>
</div>

</div>

</div>










<!-- END CLUES -->











<!-- BEGIN CLUE CHARTS -->




<div class="row mt-3 mb-3">

<div class="col-lg-12">


<div class="card border-default">
<div class="card-header">گزارش آماری سرنخ های کمپین</div>
<div class="card-body">
				
				
				
	<div class="row mt-5">   
		<div class="col-lg-12 mb-md-5">
		<figure class="highcharts-figure">
			<div id="container"></div>
			
		</figure>
	   </div>
	</div>


	<div class="row mt-5">   
	   <div class="col-xs-12 col-sm-12 col-md-6">
		<figure class="highcharts-figure">
			<div id="chartClosedClues"></div>
			
		</figure>
	   </div>
	   
	   
	   <div class="col-xs-12 col-sm-12 col-md-6">
		<figure class="highcharts-figure">
			<div id="chartClueStatusGrouped"></div>
			
		</figure>
	   </div>
	   
	   
	   
	   
	</div>			
	
	
</div>
</div>

</div>

</div>










<!-- END CLUE CHARTS -->




















<!-- BEGIN CALLS HISTORY -->










   
   
    
    
    
    
  






@endsection





@section('additional_js_bottom')



<script type="text/javascript">
Highcharts.chart('container', {
    chart: {
        type: 'column',
		plotShadow: true
    },
    title: {
        text: 'نمودار تعداد سرنخ های ثبت شده'
    },
    xAxis: {
        categories: [
        		"تاریخ"
        		],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'تعداد سرنخ ها'
        }
    },
    tooltip: {
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: [
    
    	@foreach($chart1DataArray as $key=>$chartData)
    		{
    			name: '{{$key}}',
    			data: [{{$chartData}}]
    		}
    		@if($key!=array_key_last($chart1DataArray))
    		,
    		@endif
    	@endforeach
    
    ]
});













Highcharts.chart('chartClosedClues', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: 0,
        plotShadow: true
    },
	
    title: {
        text: 'نتیجه سرنخ ها(باز/بسته)',
        align: 'center'
        //verticalAlign: 'middle',
        //y: 60
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    accessibility: {
        point: {
            valueSuffix: '%'
        }
    },
    plotOptions: {
        pie: {
            dataLabels: {
                enabled: true,
                distance: -50,
                style: {
                    fontWeight: 'bold',
                    color: 'white',
					fontSize: "16px"
                }
            },
            startAngle: -90,
            endAngle: 90,
            center: ['50%', '75%'],
            size: '110%'
        }
    },
    series: [{
        type: 'pie',
        name: 'Browser share',
        innerSize: '40%',
        data: [
		
			@foreach($chartOpenClosedCluesDataArray as $key=>$chartData)
    		[
    			'{{$chartData["title"]}}', {{$chartData["count"]}}
    		]
    		@if($key!=array_key_last($chartOpenClosedCluesDataArray))
    		,
    		@endif
			@endforeach
		
	
        ]
    }]
});


















Highcharts.chart('chartClueStatusGrouped', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: true,
        type: 'pie'
    },
    title: {
        text: 'نمودار سرنخ ها بر اساس وضعیت',
        align: 'center'
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    accessibility: {
        point: {
            valueSuffix: '%'
        }
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.percentage:.1f} %',
				style: {
					fontSize: "14px"
                }
            }
        }
    },
    series: [{
        name: 'وضعیت سرنخ ها',
        colorByPoint: true,
        data: [
		
		@foreach($chartStatusGroupedDataArray as $key=>$chartData)
    		{
    			name: '{{$chartData["title"]}}',
    			y: {{$chartData["count"]}}
    		}
    		@if($key!=array_key_last($chartStatusGroupedDataArray))
    		,
    		@endif
    	@endforeach
		
		/*
		{
            name: 'Chrome',
            y: 70.67,
            sliced: true,
            selected: true
        }
		*/]
    }]
});


</script>


@endsection
