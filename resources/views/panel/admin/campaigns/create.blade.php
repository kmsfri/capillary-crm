@extends('panel.layout.master')
@section('main_content')

    <div class="row">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2>افزودن کمپین جدید</h2>

            </div>

            <div class="pull-left">

                <a class="btn btn-primary" href="{{ route('campaigns.index') }}"> بازگشت</a>

            </div>

        </div>

    </div>


    @if ($errors->any())

        <div class="alert alert-danger">

            <strong>خطا!</strong> در مقادیر ورودی ایراداتی وجود دارد.<br><br>

            <ul>

                @foreach ($errors->all() as $error)

                    <li>{{ $error }}</li>

                @endforeach

            </ul>

        </div>

    @endif


    <form action="{{ route('campaigns.store') }}" method="POST">

        @csrf


        <div class="row">
        
        
        
		<div class="col-xs-12 col-sm-12 col-md-12">

		    <div class="form-group">

			<strong>نام کمپین:</strong>

			{!! Form::text('campaign_title', old('campaign_title'), array('placeholder' => '','class' => 'form-control')) !!}

		    </div>

		</div>
		
		
		<div class="col-xs-12 col-sm-12 col-md-6 form-input-container">

			<div class="form-group">

				<strong>تاریخ شروع:</strong>

				<input data-jdp type="text" name="start_at" value="{{ old('start_at',($campaign->start_at??'') ) }}" class="form-control" placeholder="">

			</div>

		</div>
		
		
		<div class="col-xs-12 col-sm-12 col-md-6 form-input-container">

			<div class="form-group">

				<strong>تاریخ پایان:</strong>

				<input data-jdp type="text" name="end_at" value="{{ old('end_at',($campaign->end_at??'') ) }}" class="form-control" placeholder="">

			</div>

		</div>
		
	

		

		
		
		
		<div class="col-xs-12 col-sm-12 col-md-6 mt-md-12">

			<div class="form-group">

				<strong>توضیحات:</strong>

				<textarea class="form-control" name="campaign_description" placeholder="" >{{ old('campaign_description',($campaign->campaign_description??'') ) }}</textarea>

			</div>

		</div>
		
		
		
		<div class="col-xs-12 col-sm-12 col-md-6 mt-md-12">

			<div class="form-group">

				<strong>منبع کمپین:</strong>

				<textarea class="form-control" name="campaign_source" placeholder="" >{{old('campaign_source',($campaign->campaign_source??''))}}</textarea>

			</div>

		</div>
		
        
        

            <div class="col-xs-12 col-sm-12 col-md-12 text-center">

                <button type="submit" class="btn btn-primary">ثبت</button>

            </div>

        </div>


    </form>



@endsection

