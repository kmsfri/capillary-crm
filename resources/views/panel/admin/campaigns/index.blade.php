@extends('panel.layout.master')
@section('main_content')


    <div class="row">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2>مدیریت کمپین ها</h2>

            </div>

            <div class="pull-left">

                @can('campaign-create')

                    <a class="btn btn-success" href="{{ route('campaigns.create') }}"> ایجاد کمپین جدید</a>

                @endcan

            </div>

        </div>

    </div>


    @if ($message = Session::get('success'))

        <div class="alert alert-success">

            <p>{{ $message }}</p>

        </div>

    @endif


    <table class="table table-bordered">

        <tr>

            <th>ردیف</th>

            <th>نام کمپین</th>

            <th>تاریخ شروع</th>
            
            <th>تاریخ پایان</th>
            
            <th>ایجاد شده توسط</th>
            
            <th>آخرین آپدیت توسط</th>
			
			<th>زمان ایجاد</th>
            

            <th width="280px">عملیات</th>

        </tr>

        @foreach ($campaigns as $campaign)

            <tr>

                <td>{{ ++$i }}</td>

                <td><a href="{{route('campaigns.showCampaignsReport',$campaign->id)}}" target="_blank">{{ $campaign->campaign_title }}</a></td>

                <td>{{ \Morilog\Jalali\Jalalian::forge($campaign->start_at)->format('%A, %d %B %Y') }}</td>
				
				<td>{{ \Morilog\Jalali\Jalalian::forge($campaign->end_at)->format('%A, %d %B %Y') }}</td>
				
				 <td>{{ !empty($campaign->creator()->first()->name)?$campaign->creator()->first()->name:'-' }}</td>
				 
				 <td>{{ !empty($campaign->lastUpdator()->first())?$campaign->lastUpdator()->first()->name:'-' }}</td>
				 
				 <td>{{ \Morilog\Jalali\Jalalian::forge($campaign->created_at)->format('%A, %d %B %Y در ساعت H:i:s') }}</td>
                

                <td>

                    <form action="{{ route('campaigns.destroy',$campaign->id) }}" method="POST">

                        @can('campaign-edit')

                            <a class="btn btn-primary" href="{{ route('campaigns.edit',$campaign->id) }}">ویرایش</a>

                        @endcan


                        @csrf

                        @method('DELETE')

                        @can('campaign-delete')

                            <button type="submit" class="btn btn-danger">حذف</button>

                        @endcan

                    </form>

                </td>

            </tr>

        @endforeach

    </table>


    {!! $campaigns->links() !!}



@endsection
