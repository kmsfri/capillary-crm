@extends('panel.layout.master')
@section('main_content')
    <div class="row mb-md-5">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2>مدیریت رقبا</h2>

            </div>

            <div class="pull-left">

                <a class="btn btn-success" href="{{ route('competitors.create') }}"> ایجاد آیتم جدید</a>

            </div>

        </div>

    </div>


    @if ($message = Session::get('success'))

        <div class="alert alert-success">

            <p>{{ $message }}</p>

        </div>

    @endif
    
    @if ($message = Session::get('danger'))

        <div class="alert alert-danger">

            <p>{{ $message }}</p>

        </div>

    @endif
    
    
    
    
    
    <div class="row">    
    <div class="col-lg-12 mb-md-5">
        <form action="{{route('competitors.index')}}" method="GET">
        <div class="row">
	    
            
            
            <div class="col-xs-12 col-sm-12 col-md-4 form-input-container">

                <div class="form-group">

                    <strong>عنوان:</strong>

                    <input type="text" name="product_title" id="product_title" value="{{ old('product_title',request()->product_title) }}" class="form-control" placeholder="بخشی از عنوان مورد جستجو">

                </div>

            </div>
            
	
	
        </div>
        
        <div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 form-input-container">
			<div class="pull-left">
			<button type="submit" class="btn btn-success" href="{{ route('competitors.index') }}">فیلتر</button>
			</div>
			
			
			
			<div class="pull-left ml-3">
				<button type="button" class="btn btn-warning" onclick="exportExcel()">خروجی اکسل</button>
				<script>
					function exportExcel(){
						const urlParams = new URLSearchParams(window.location.search);

						urlParams.set('export_excel', 1);

						window.location.search = urlParams;
					}
				</script>
			</div>
			
			
		</div>
        </div>
        </form>
    </div>
</div>
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    


    <table class="table table-bordered">

        <tr>

            <th>ردیف</th>

            <th>عنوان</th>
            <th>نام کالا</th>
			<th>برند</th>
			<th>کشور</th>
            <th>کاربر ایجاد کننده</th>
			<th>تاریخ ایجاد</th>
			<th>کاربر ویرایش کننده</th>
			<th>تاریخ آخرین بروزرسانی</th>
            
            
            <th width="280px">عملیات</th>

        </tr>

        @foreach ($competitors as $key => $competitor)

            <tr>

                <td>{{ ++$i }}</td>

                <td>
					{{ $competitor->product_title }}
				</td>

				<td>
					{{ (!empty($competitor->product()->first())?$competitor->product()->first()->product_name:'-') }}
				</td>
				
				<td>
					{{ (!empty($competitor->brand()->first())?$competitor->brand()->first()->brand_name:'-') }}
				</td>
				
				<td>
					{{ (!empty($competitor->country()->first())?$competitor->country()->first()->country_name." - ".$competitor->country()->first()->country_code:'-') }}
				</td>
				
	
				<td>
                {{$competitor->creator()->first()->name;}}
                </td>
				
				<td>{{ \Morilog\Jalali\Jalalian::forge($competitor->created_at)->format('%A, %d %B %Y در ساعت H:i:s') }}</td>
				
				
				
				<td>
                {{!empty($competitor->lastUpdater()->first())?$competitor->lastUpdater()->first()->name:'-';}}
                </td>
				
				<td>{{ !empty($competitor->updated_at)?(\Morilog\Jalali\Jalalian::forge($competitor->updated_at)->format('%A, %d %B %Y در ساعت H:i:s')):'-' }}</td>
				
				
				
                

			
                <td>
                
                    <a class="btn btn-primary" href="{{ route('competitors.edit',$competitor->id) }}">ویرایش</a>

                    {!! Form::open(['method' => 'DELETE','route' => ['competitors.destroy', $competitor->id],'style'=>'display:inline']) !!}

                    {!! Form::submit('حذف', ['class' => 'btn btn-danger']) !!}

                    {!! Form::close() !!}

                </td>

            </tr>

        @endforeach

    </table>


    {!! $competitors->appends(Request::all())->render() !!}



@endsection
