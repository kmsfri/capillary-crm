@extends('panel.layout.master')
@section('main_content')

    <div class="row mb-md-5">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2>ویرایش رکورد</h2>

            </div>

            <div class="pull-left">

                <a class="btn btn-primary" href="{{ route('competitors.index') }}"> بازگشت</a>

            </div>

        </div>

    </div>


    @if (count($errors) > 0)

        <div class="alert alert-danger">

            <strong>خطا!</strong> در مقادیر ورودی ایراداتی وجود دارد.<br><br>

            <ul>

                @foreach ($errors->all() as $error)

                    <li>{{ $error }}</li>

                @endforeach

            </ul>

        </div>

    @endif



    {!! Form::model($competitor, ['method' => 'PATCH','route' => ['competitors.update', $competitor->id]]) !!}

    <div class="row mb-5">


		<div class="col-xs-12 col-sm-12 col-md-12 form-input-container">

			<div class="form-group">

				<strong>عنوان کالا:</strong>

				<input type="text" name="product_title" value="{{ old('product_title',$competitor->product_title)??'' }}" class="form-control" placeholder="">

			</div>

		</div>
		
		
		
		<div class="col-xs-12 col-sm-12 col-md-4 form-input-container">

			<div class="form-group">

				<strong>محصول:</strong>

				<select  name='product_id' id='product_id' class='form-control pull-right'>
					<option {{(!old('product_id', isset($competitor->product_id) ? $competitor->product_id : '')? 'selected' : '')}} value="">انتخاب کنید</option>
					@foreach($products as $product)
						<option @if(old('product_id', isset($competitor->product_id) ? $competitor->product_id : '')==$product->id) selected @endif value="{{$product->id}}" >{{$product->product_name}} - کد: {{$product->product_code}}</option>
					@endforeach
				</select>

			</div>

		</div>
		
		<div class="col-xs-12 col-sm-12 col-md-4 form-input-container">

			<div class="form-group">

				<strong>برند:</strong>

				<select  name='brand_id' id='brand_id' class='form-control pull-right'>
					<option {{(!old('brand_id', isset($competitor->brand_id) ? $competitor->brand_id : '')? 'selected' : '')}} value="">انتخاب کنید</option>
					@foreach($brands as $brand)
						<option @if(old('brand_id', isset($competitor->brand_id) ? $competitor->brand_id : '')==$brand->id) selected @endif value="{{$brand->id}}" >{{$brand->brand_name}}</option>
					@endforeach
				</select>

			</div>

		</div>
		
		
		
		<div class="col-xs-12 col-sm-12 col-md-4 form-input-container">

			<div class="form-group">

				<strong>کشور تولید کننده:</strong>

				<select  name='country_id' id='country_id' class='form-control pull-right'>
					<option {{(!old('country_id', isset($competitor->country_id) ? $competitor->country_id : '')? 'selected' : '')}} value="">انتخاب کنید</option>
					@foreach($countries as $country)
						<option @if(old('country_id', isset($competitor->country_id) ? $competitor->country_id : '')==$country->id) selected @endif value="{{$country->id}}" >{{$country->country_name}} - {{$country->country_code}}</option>
					@endforeach
				</select>

			</div>

		</div>
		
		
		
		
		<div class="col-xs-12 col-sm-12 col-md-4">

            <div class="form-group">

                <strong>قیمت شرکت:</strong>

                {!! Form::text('company_price', old('company_price',$competitor->company_price), array('placeholder' => '','class' => 'form-control')) !!}

            </div>

        </div>
		
		
		<div class="col-xs-12 col-sm-12 col-md-4">

            <div class="form-group">

                <strong>قیمت در بازار:</strong>

                {!! Form::text('market_price', old('market_price',$competitor->market_price), array('placeholder' => '','class' => 'form-control')) !!}

            </div>

        </div>
		
		
		<div class="col-xs-12 col-sm-12 col-md-4">

            <div class="form-group">

                <strong>مدت تسویه:</strong>

                {!! Form::text('checkout_period', old('checkout_period',$competitor->checkout_period), array('placeholder' => '','class' => 'form-control')) !!}

            </div>

        </div>
		
		
		<div class="col-xs-12 col-sm-12 col-md-6">

            <div class="form-group">

                <strong>میزان موجودی در بازار(تعداد):</strong>

                {!! Form::text('market_stock_count', old('market_stock_count',$competitor->market_stock_count), array('placeholder' => '','class' => 'form-control')) !!}

            </div>

        </div>
		
		<div class="col-xs-12 col-sm-12 col-md-6">

            <div class="form-group">

                <strong>میزان موجودی در بازار(درصد):</strong>

                {!! Form::text('market_stock_percent', old('market_stock_percent',$competitor->market_stock_percent), array('placeholder' => '','class' => 'form-control')) !!}

            </div>

        </div>
		
		
		
		<div class="col-xs-12 col-sm-12 col-md-6">

            <div class="form-group">

                <strong>میزان موجودی شرکت تولید کننده(تعداد):</strong>

                {!! Form::text('manufacturer_stock_count', old('manufacturer_stock_count',$competitor->manufacturer_stock_count), array('placeholder' => '','class' => 'form-control')) !!}

            </div>

        </div>
		
		<div class="col-xs-12 col-sm-12 col-md-6">

            <div class="form-group">

                <strong>میزان موجودی شرکت کننده(درصد):</strong>

                {!! Form::text('manufacturer_stock_percent', old('manufacturer_stock_percent',$competitor->manufacturer_stock_percent), array('placeholder' => '','class' => 'form-control')) !!}

            </div>

        </div>
		
		
		
		<div class="col-xs-12 col-sm-12 col-md-6">

            <div class="form-group">

                <strong>میزان موجودی شرکت وارد کننده(تعداد):</strong>

                {!! Form::text('importer_stock_count', old('importer_stock_count',$competitor->importer_stock_count), array('placeholder' => '','class' => 'form-control')) !!}

            </div>

        </div>
		
		<div class="col-xs-12 col-sm-12 col-md-6">

            <div class="form-group">

                <strong>میزان موجودی شرکت وارد کننده(درصد):</strong>

                {!! Form::text('importer_stock_percent', old('importer_stock_percent',$competitor->importer_stock_percent), array('placeholder' => '','class' => 'form-control')) !!}

            </div>

        </div>
		
		

        
        

        <div class="col-xs-12 col-sm-12 col-md-12 text-center">

            <button type="submit" class="btn btn-primary">ثبت</button>

        </div>

    </div>

    {!! Form::close() !!}






	<script>




        function change_list(master_el_id,child_el_id,loading_gif_id,ajax_req_url,depend_element_ids = []){

            var e = document.getElementById(master_el_id);
            r_id = e.options[e.selectedIndex].value;

            if(r_id==""){
                $('#'+child_el_id).html('');
                return;
            }

            $('#'+master_el_id).prop('disabled', true);
            //$('#'+loading_gif_id).css('display', 'block'); //TODO: enable loadings

            token= $('body').find("input[name='_token']").val();

            $.ajax({
                url: ajax_req_url,
                type: 'GET',
                data: {"r_id":r_id, "_token":token },
                success: function(data){

                    $('#'+child_el_id).html('');
                    html_str='<option value="" >انتخاب کنید</option>';
                    $('#'+child_el_id).append(html_str);
                    $.each(data, function( index, value ) {
                        html_str='<option value="'+value.id+'" >'+value.title+'</option>';
                        $('#'+child_el_id).append(html_str);
                    });

                    depend_element_ids.forEach(function(){
                        $('#'+depend_element_ids).empty();
                    });

                    $('#'+master_el_id).prop('disabled', false);
                    //$('#'+loading_gif_id).css('display', 'none');
                },
                error: function(data){
                    alert(data.responseText);
                    alert('دریافت اطلاعات با خطا مواجه شد!');
                    $('#'+master_el_id).prop('disabled', false);
                    //$('#'+loading_gif_id).css('display', 'none');
                }
            });

        }
    </script>










@endsection
