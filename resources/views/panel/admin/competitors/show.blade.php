@extends('panel.layout.master')
@section('main_content')

    <div class="row mb-md-5">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2> نمایش اطلاعات مشتری</h2>

            </div>

            <div class="pull-left">

                <a class="btn btn-primary" href="{{ route('customers.index') }}"> بازگشت</a>

            </div>

        </div>

    </div>


    <div class="row">

        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>نام:</strong>

                {{ $customer->first_name }}

            </div>

        </div>


        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>نام خانوادگی:</strong>

                {{ $customer->last_name }}

            </div>

        </div>


        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>فروشگاه:</strong>

                {{ $customer->business_name }}

            </div>

        </div>


        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>شهر:</strong>

                {{ $customer->city }}

            </div>

        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>آدرس:</strong>

                {{ $customer->address }}

            </div>

        </div>


        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>تلفن:</strong>

                {{ $customer->phone }}

            </div>

        </div>


        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>موبایل:</strong>

                {{ $customer->mobile }}

            </div>

        </div>


        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>کاربر ایجاد کننده:</strong>

                {{ $customer->creator()->first()->name }}({{ $customer->creator()->first()->email }})

            </div>

        </div>


        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>تاریخ و زمان ایجاد:</strong>

                {{ $customer->created_at }}

            </div>

        </div>


        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>تاریخ و زمان آخرین ویرایش:</strong>

                {{ $customer->updated_at }}

            </div>

        </div>

    </div>

@endsection
