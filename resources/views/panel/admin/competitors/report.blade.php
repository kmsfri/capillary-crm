@extends('panel.layout.master')
@section('additional_js_top2')

<script src="{{asset('highcharts/highcharts.js')}}"></script>
<script src="{{asset('highcharts/modules/exporting.js')}}"></script>
<script src="{{asset('highcharts/modules/export-data.js')}}"></script>
<script src="{{asset('highcharts/modules/accessibility.js')}}"></script>

@endsection
@section('main_content')
    <div class="row mb-md-5">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2>گزارش جامع آماری رقبا</h2>

            </div>

           

        </div>

    </div>


    @if ($message = Session::get('success'))

        <div class="alert alert-success">

            <p>{{ $message }}</p>

        </div>

    @endif
    
    @if ($message = Session::get('danger'))

        <div class="alert alert-danger">

            <p>{{ $message }}</p>

        </div>

    @endif
    
    
    
    <div class="row">    
    <div class="col-lg-12 mb-md-5">
        <form action="{{route('competitors.showCompetitorsReport')}}" method="GET">
        <div class="row">
	    
            
            
            
            <div class="col-xs-12 col-sm-12 col-md-2 form-input-container">

                <div class="form-group">

                    <strong>از تاریخ:</strong>

                    <input data-jdp type="text" name="from_date" id="from_date" value="{{ old('from_date',request()->from_date) }}" class="form-control" placeholder="">

                </div>

            </div>
            
            
            <div class="col-xs-12 col-sm-12 col-md-2 form-input-container">

                <div class="form-group">

                    <strong>تا تاریخ:</strong>
                    
                    <input data-jdp type="text" name="to_date" id="to_date" value="{{ old('to_date',request()->to_date) }}" class="form-control" placeholder="">

                </div>

            </div>
            
			
			
			
			<div class="col-xs-12 col-sm-12 col-md-2 form-input-container">

				<div class="form-group">

					<strong>محصول:</strong>

					<select  name='product_id' id='product_id' class='form-control pull-right' > 
					<option {{(!old('product_id', isset(request()->product_id) ? request()->product_id : '')? 'selected' : '')}} value="">انتخاب کنید</option>
					@foreach($products as $product)
						<option @if(old('product_id', isset(request()->product_id) ? request()->product_id : '')==$product->id) selected @endif value="{{$product->id}}" >{{$product->product_name}}</option>
					@endforeach
					</select>

				</div>

			</div>
			
			
			<div class="col-xs-12 col-sm-12 col-md-2 form-input-container">

				<div class="form-group">

					<strong>برند:</strong>

					<select  name='brand_id' id='brand_id' class='form-control pull-right' > 
					<option {{(!old('brand_id', isset(request()->brand_id) ? request()->brand_id : '')? 'selected' : '')}} value="">انتخاب کنید</option>
					@foreach($brands as $brand)
						<option @if(old('brand_id', isset(request()->brand_id) ? request()->brand_id : '')==$brand->id) selected @endif value="{{$brand->id}}" >{{$brand->brand_name}}</option>
					@endforeach
					</select>

				</div>

			</div>
	
	
			<div class="col-xs-12 col-sm-12 col-md-2 form-input-container">

				<div class="form-group">

					<strong>کشور تولید کننده:</strong>

					<select  name='country_id' id='country_id' class='form-control pull-right' > 
					<option {{(!old('country_id', isset(request()->country_id) ? request()->country_id : '')? 'selected' : '')}} value="">انتخاب کنید</option>
					@foreach($countries as $country)
						<option @if(old('country_id', isset(request()->country_id) ? request()->country_id : '')==$country->id) selected @endif value="{{$country->id}}" >{{$country->country_name}} - {{$country->country_code}}</option>
					@endforeach
					</select>

				</div>

			</div>	

		
			
			<div class="col-xs-12 col-sm-12 col-md-2 form-input-container">

				<div class="form-group">

					<strong>بازه زمانی:</strong>

					<select  name='report_period' id='report_period' class='form-control pull-right' autocomplete='off'>
					 <option @if(old('report_period', isset(request()->report_period) ? request()->report_period : '')==2) selected @endif value="2" >ماهانه</option>
					 <option @if(old('report_period', isset(request()->report_period) ? request()->report_period : '')==1) selected @endif value="1" >روزانه</option>
					</select>

				</div>

			</div>
				
				
				
				
			
			
			
	
        </div>
        
        <div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 form-input-container">
				<div class="pull-left">
				<button type="submit" class="btn btn-success">دریافت گزارش</button>
				</div>
			</div>
        </div>
        </form>
    </div>
</div>


  
<div class="row mt-5">   
    <div class="col-lg-12 mb-md-5">
    <figure class="highcharts-figure">
		<div id="chart1container"></div>
		
	</figure>
   </div>
</div>



<div class="row mt-5">   
    <div class="col-lg-12 mb-md-5">
    <figure class="highcharts-figure">
		<div id="chart2container"></div>
		
	</figure>
   </div>
</div>


@endsection


@section('additional_js_bottom')

<script type="text/javascript">









Highcharts.chart('chart1container', {

    title: {
        text: 'نمودار قیمت رقبا&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
        align: 'right'
    },

    subtitle: {
        text: 'میانگین قیمت در بازه های زمانی(مقایسه قیمت بازار و قیمت شرکت)',
        align: 'right'
    },

    yAxis: {
        title: {
            text: 'قیمت محصول'
        }
    },
	xAxis: {
		type: 'datetime',
		categories: [
		@foreach($chart1DataArray as $key=>$chartData)
				'{{$key}}'
				@if($key!=array_key_last($chart1DataArray))
				,
				@endif
		@endforeach
		]
    },
	
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
    },



    series: [{
        name: 'قیمت شرکت',
        data: [
			@foreach($chart1DataArray as $key=>$chartData)
				{{$chartData}}
				@if($key!=array_key_last($chart1DataArray))
				,
				@endif
			@endforeach
		
		]
    },
	{
        name: 'قیمت در بازار',
        data: [
			@foreach($chart2DataArray as $key=>$chartData)
				{{$chartData}}
				@if($key!=array_key_last($chart1DataArray))
				,
				@endif
			@endforeach
		
		]
    }
	
	
	],

    responsive: {
        rules: [{
            condition: {
                maxWidth: 500
            },
            chartOptions: {
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                }
            }
        }]
    }

});






Highcharts.chart('chart2container', {

    title: {
        text: 'نمودار موجودی بازار&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
        align: 'right'
    },

    subtitle: {
        text: 'موجودی بازار در بازه های زمانی مختلف(با مقایسه موجودی بازار و موجودی تولید کننده و موجودی وارد  کننده)',
        align: 'right'
    },

    yAxis: {
        title: {
            text: 'موجودی بر اساس درصد'
        }
    },
	xAxis: {
		type: 'datetime',
		categories: [
		@foreach($chart3DataArray as $key=>$chartData)
				'{{$key}}'
				@if($key!=array_key_last($chart1DataArray))
				,
				@endif
		@endforeach
		]
    },
	
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
    },



    series: [{
        name: 'موجودی در بازار',
        data: [
			@foreach($chart3DataArray as $key=>$chartData)
				{{$chartData}}
				@if($key!=array_key_last($chart1DataArray))
				,
				@endif
			@endforeach
		
		]
    },
	{
        name: 'موجودی تولید کننده',
        data: [
			@foreach($chart4DataArray as $key=>$chartData)
				{{$chartData}}
				@if($key!=array_key_last($chart1DataArray))
				,
				@endif
			@endforeach
		
		]
    },
	{
        name: 'موجودی وارد کننده',
        data: [
			@foreach($chart5DataArray as $key=>$chartData)
				{{$chartData}}
				@if($key!=array_key_last($chart1DataArray))
				,
				@endif
			@endforeach
		
		]
    }
	
	
	],

    responsive: {
        rules: [{
            condition: {
                maxWidth: 500
            },
            chartOptions: {
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                }
            }
        }]
    }

});














</script>


@endsection



