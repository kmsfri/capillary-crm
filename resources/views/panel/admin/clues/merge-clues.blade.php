@extends('panel.layout.master')
@section('additional_css')


<style>

.create-invoice-col a{
  display:block;
}


.select2-container--default .select2-selection--multiple .select2-selection__choice{
	
	color: #424242;
	
	
}


</style>

<link rel="stylesheet" href="{{asset('css/bootstrap-steps.min.css')}}">

<style>
.steps{
	direction: ltr !important;
}
.step-content{
	min-width: 10rem;
}

.step-circle::before {
	width: calc(10rem + 1rem - 1.5rem);
}
.step-text{
		font-size: 10px;
}
</style>

    
@stop
@section('main_content')


    <div class="row mb-md-5">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2>ادغام کارتابل سرنخ {{$toMergeClue->id}} با کارتابل سرنخ {{$baseClue->id}}</h2>

            </div>

            <div class="pull-left">


            </div>

        </div>

    </div>
	
    
    
    @if ($message = Session::get('success'))

        <div class="alert alert-success">

            <p>{{ $message }}</p>

        </div>

    @endif
    
    @if ($message = Session::get('danger'))

        <div class="alert alert-danger">

            <p>{{ $message }}</p>

        </div>

    @endif
	
	
	
	@if (count($errors) > 0)

        <div class="alert alert-danger">

            <strong>خطا!</strong> در مقادیر ورودی ایراداتی وجود دارد.<br><br>

            <ul>

                @foreach ($errors->all() as $error)

                    <li>{{ $error }}</li>

                @endforeach

            </ul>

        </div>

    @endif
    
    
    
    
    
    
<!-- BEGIN CLUE INFORMATION -->


<div class="row mt-3 mb-3">

<div class="col-lg-12">


<div class="card border-success">
<div class="card-header">توضیحات ادغام</div>
<div class="card-body">
						
	<blockquote class="blockquote mt-2">	
		<p>
			یادداشت‌ها و آلارم‌های سرنخ:
		</p>
	</blockquote>
	
	<blockquote class="blockquote mt-1">	
		<p>
		با شناسه‌ی: <span class="bg-danger text-white pl-2 pr-2">{{$toMergeClue->id}}</span> و عنوان: <span class="bg-danger text-white pl-2 pr-2">{{$toMergeClue->clue_title}}</span>
		</p>
	</blockquote>
	
	
	<blockquote class="blockquote mt-1">	
		<p>
		به سرنخ با شناسه‌ی: <span class="bg-danger text-white pl-2 pr-2">{{$baseClue->id}}</span> و عنوان: <span class="bg-danger text-white pl-2 pr-2">{{$toMergeClue->clue_title}}</span>
		</p>
	</blockquote>
	
	
	<blockquote class="blockquote mt-1">	
		<p>
		منتقل شده و سرنخ با شناسه <span class="bg-danger text-white pl-2 pr-2">{{$toMergeClue->id}}</span> به طور کامل حذف خواهد شد.
		</p>
	</blockquote>
	
	
	
	
	
	
</div>
</div>

</div>

</div>


<!-- END CLUE INFORMATION -->






<!-- BEGIN ALARMS -->






<div class="row mt-3 mb-3">

<div class="col-lg-12">


<div class="card border-success">
<div class="card-header">آلارم‌های منتقل شونده</div>
<div class="card-body">
				
				
				
				
				
				
				
				
				
				
@if(count($toAddClueAlarms)==0)
	<p class="card-text">هیچ آلارمی برای انتقال وجود ندارد</p>
@else				
				
<table class="table table-bordered">

<tr>
	<th>توضیحات</th>
	<td>تاریخ یادآوری</td>
	<td>تاریخ ثبت</td>
</tr>





@foreach ($toAddClueAlarms as $alarm)

	@php
		$rowClass = "";
		if($alarm->is_seen==1){

			$rowClass = 'success';

		}elseif(\Morilog\Jalali\Jalalian::forge($alarm->alarm_at)->isPast()){
			$rowClass = 'danger';
		}
	@endphp



    <tr class="{{$rowClass}}">



	<td>{{$alarm->alarm_description}}</td>


	<td>{{ \Morilog\Jalali\Jalalian::forge($alarm->alarm_at)->format('%A, %d %B %Y در ساعت H:i:s') }}</td>
	
	
	<td>{{ \Morilog\Jalali\Jalalian::forge($alarm->created_at)->format('%A, %d %B %Y در ساعت H:i:s') }}</td>


    </tr>

@endforeach

</table>
				
				
@endif			
				
				
				
	
	
</div>
</div>

</div>

</div>





<!-- END ALARMS -->




<!-- BEGIN CUSTOMER NOTES -->


<div class="row mt-3 mb-3">

<div class="col-lg-12">


<div class="card border-success">
<div class="card-header">یادداشت‌های منتقل شونده</div>
<div class="card-body">
				

	
	
	
	
	
	
	
	
	
					
	@if(count($toAddClueNotes)==0)
		<p class="card-text text-center">هیچ یادداشتی برای انتقال وجود ندارد.</p>
	@else				
					
	<table class="table table-bordered mt-5">

	<tr>
		<th style="width:66%">متن یادداشت</th>
		
		<td style="width:10%">ارجاع توسط</td>
		<td style="width:12%">تاریخ ثبت</td>
		<td style="width:12%">تاریخ آخرین ویرایش</td>
	</tr>





	@foreach ($toAddClueNotes as $note)


	    <tr>



		<td>
		
		{{$note->note_text}}
		
		</td>
		

		
		<td>{{ !empty($note->referencedBy()->first())?$note->referencedBy()->first()->name:
				(
					!empty($note->creator()->first())?$note->creator()->first()->name:'-'
				)}}</td>


		<td>{{ \Morilog\Jalali\Jalalian::forge($note->created_at)->format('%Y/%m/%d H:i:s') }}</td>
		
		<td>{{ \Morilog\Jalali\Jalalian::forge($note->updated_at)->format('%Y/%m/%d H:i:s') }}</td>


	    </tr>

	@endforeach

	</table>
	
					
					
	@endif	
	
	
	
	
	
	
	
	
	
	
					
							
	
	
</div>
</div>

</div>

</div>


<!-- END CUSTOMER NOTES -->









<!-- BEGIN MERGE -->


<div class="row mt-3 mb-3">

<div class="col-lg-12">


<div class="card border border-danger">
<div class="card-header bg-danger text-white">تایید ادغام</div>
<div class="card-body">
				
	<form action="{{ route('clues.merge.confirm') }}" method="POST">
			<input type="hidden" name="base_clue_id" value="{{$baseClue->id}}">
			<input type="hidden" name="to_merge_clue_id" value="{{$toMergeClue->id}}">
        	@csrf

			
			<div class="col-xs-12 col-sm-12 col-md-12 text-center">
				@can('merge-clues')
				<button type="submit" class="btn btn-danger" onclick="return confirm('آیا مطمئنید؟?');">تایید ادغام</button>
				@else
				<button type="button" disabled class="btn btn-danger">تایید ادغام</button>
				@endcan

			</div>
			
			
			
			<blockquote class="blockquote text-center mt-5">
			  <p class="mb-0">لطفا در ادغام سرنخ‌ها بسیار دقت کنید. سرنخ ادغام شده به صورت کامل حذف شده و قابل برگشت نیست.</p>
			</blockquote>
		
	</form>
	
	
					
							
	
	
</div>
</div>

</div>

</div>


<!-- END MERGE -->


















@endsection
