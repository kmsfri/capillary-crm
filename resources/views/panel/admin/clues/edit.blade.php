@extends('panel.layout.master')
@section('main_content')

    <div class="row mb-md-5">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2>ویرایش سرنخ</h2>

            </div>

            <div class="pull-left">

                <a class="btn btn-primary" href="{{ route('clues.index') }}"> بازگشت</a>

            </div>

        </div>

    </div>


    @if (count($errors) > 0)

        <div class="alert alert-danger">

            <strong>خطا!</strong> در مقادیر ورودی ایراداتی وجود دارد.<br><br>

            <ul>

                @foreach ($errors->all() as $error)

                    <li>{{ $error }}</li>

                @endforeach

            </ul>

        </div>

    @endif


    {!! Form::model($clue, ['method' => 'PATCH','route' => ['clues.update', $clue->id]]) !!}

    <div class="row">

        <div class="col-xs-12 col-sm-12 col-md-12 form-input-container">

			<div class="form-group">

				<strong>عنوان سرنخ:</strong>

				<input type="text" name="clue_title" value="{{ old('clue_title',$clue->clue_title)??'' }}" class="form-control" placeholder="">

			</div>

		</div>




		



		<div class="col-xs-12 col-sm-12 col-md-6 form-input-container">

			<div class="form-group">

				<strong>نتیجه سرنخ:</strong>
				<select  name='clue_closed' id='clue_closed' class='form-control pull-right'>
					<option value="" >انتخاب کنید</option>
					<option @if(old('clue_closed', isset($clue->clue_closed) ? $clue->clue_closed : '')=='1') selected @endif value="1" >بسته</option>
					<option @if(old('clue_closed', isset($clue->clue_closed) ? $clue->clue_closed : '')=='0') selected @endif value="0" >باز</option>
				</select>

			</div>
		</div>
		
		<div class="col-xs-12 col-sm-12 col-md-6 form-input-container">

			<div class="form-group">

				<strong>کمپین مرتبط:</strong>

				<select  name='campaign_id' id='campaign_id' class='form-control pull-right'>
					<option {{(!old('campaign_id', isset($clue->campaign_id) ? $clue->campaign_id : '')? 'selected' : '')}} value="">انتخاب کنید</option>
					@foreach($campaigns as $campaign)
						<option @if(old('campaign_id', isset($clue->campaign_id) ? $clue->campaign_id : '')==$campaign->id) selected @endif value="{{$campaign->id}}" >{{$campaign->campaign_title}}</option>
					@endforeach
				</select>

			</div>

		</div>
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		<div class="col-xs-12 col-sm-12 col-md-4">

            <div class="form-group">

                <strong>نام:</strong>

                {!! Form::text('first_name', old('first_name',$clue->first_name), array('placeholder' => '','class' => 'form-control')) !!}

            </div>

        </div>
        <div class="col-xs-12 col-sm-12 col-md-4">

            <div class="form-group">

                <strong>نام خانوادگی:</strong>

                {!! Form::text('last_name', old('last_name',$clue->last_name), array('placeholder' => '','class' => 'form-control')) !!}

            </div>

        </div>
		<div class="col-xs-12 col-sm-12 col-md-4">

            <div class="form-group">

                <strong>فروشگاه:</strong>

                {!! Form::text('business_name', old('business_name',$clue->business_name), array('placeholder' => '','class' => 'form-control')) !!}

            </div>

        </div>
        
		
		<div class="col-xs-12 col-sm-12 col-md-6">

            <div class="form-group">

                <strong>استان(متنی):</strong>

                {!! Form::text('state', old('state',$clue->state), array('placeholder' => '','class' => 'form-control')) !!}

            </div>

        </div>
		
		<div class="col-xs-12 col-sm-12 col-md-6">

            <div class="form-group">

                <strong>شهر(متنی):</strong>

                {!! Form::text('city', old('city',$clue->city), array('placeholder' => '','class' => 'form-control')) !!}

            </div>

        </div>
		
		
		
		
		
			@php
			$existsFlag1 = false;
			foreach($states as $state){
				if($state->id==$clue->city_id){
					$existsFlag1 = true;
					break;
				}
			}
			
			$existsFlag2 = false;
			foreach($cities as $city){
				if($city->id==$clue->city_id){
					$existsFlag2 = true;
					break;
				}
			}
			
			$existsFlag3 = false;
			foreach($regions as $region){
				if($region->id==$clue->city_id){
					$existsFlag3 = true;
					break;
				}
			}
			@endphp
			
			@if($existsFlag1 || $existsFlag2 || $existsFlag3)
			
			<div class="col-xs-12 col-sm-12 col-md-4 form-input-container">

                <div class="form-group">

                    <strong>استان:</strong>

                    <select  name='state_id' id='state_id' class='form-control pull-right' onChange="change_list('state_id','city_id','loading_gif_cities','{{route('getCities')}}',['region_id'])" required>

                        <option {{(!old('state_id', isset($clue->state_id) ? $clue->state_id : '')? 'selected' : '')}} value="">انتخاب کنید</option>
                        @foreach($states as $state)
                            <option @if(old('state_id', isset($clue->state_id) ? $clue->state_id : '')==$state->id) selected @endif value="{{$state->id}}" >{{$state->unit_name}}</option>
                        @endforeach
                    </select>

                </div>

            </div>



            <div class="col-xs-12 col-sm-12 col-md-4 form-input-container">

                <div class="form-group">

                    <strong>شهر:</strong>

                    <select  name='city_id' id='city_id' class='form-control pull-right' onChange="change_list('city_id','region_id','loading_gif_regions','{{route('getCities')}}')" required>
                        <option {{(!old('city_id', isset($clue->city_id) ? $clue->city_id : '')? 'selected' : '')}} value="">انتخاب کنید</option>
                        @foreach($cities as $city)
                            <option @if(old('city_id', isset($clue->city_id) ? $clue->city_id : '')==$city->id) selected @endif value="{{$city->id}}" >{{$city->unit_name}}</option>
                        @endforeach
                    </select>

                </div>

            </div>


            <div class="col-xs-12 col-sm-12 col-md-4 form-input-container">

                <div class="form-group">

                    <strong>منطقه:</strong>

                    <select  name='region_id' id='region_id' class='form-control pull-right'>

                        <option {{(!old('region_id', isset($clue->region_id) ? $clue->region_id : '')? 'selected' : '')}} value="">انتخاب کنید</option>
                        @foreach($regions as $region)
                            <option @if(old('region_id', isset($clue->region_id) ? $clue->region_id : '')==$region->id) selected @endif value="{{$region->id}}" >{{$region->unit_name}}</option>
                        @endforeach


                    </select>

                </div>

            </div>
			@else
			<div class="col-xs-12 col-sm-12 col-md-12 form-input-container">

                <div class="form-group">

                    <strong>شهر - استان - منطقه(عدم دسترسی ویرایش):</strong>
					<input type="text" disabled value="{{ $clue->getFullStateCityRegion() }}" class="form-control" placeholder="-">
				</div>
			</div>
			@endif
		
		
		
		
		
		
		
		
		
		
		
		
		
		<div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>آدرس:</strong>

                {!! Form::text('address', old('address',$clue->address), array('placeholder' => '','class' => 'form-control')) !!}

            </div>

        </div>

        <div class="col-xs-12 col-sm-12 col-md-4">

            <div class="form-group">

                <strong>تلفن ۱:</strong>

                {!! Form::text('phone', old('phone',$clue->phone), array('placeholder' => '','class' => 'form-control')) !!}

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-4">

            <div class="form-group">

                <strong>تلفن ۲:</strong>

                {!! Form::text('phone2', old('phone2',$clue->phone2), array('placeholder' => '','class' => 'form-control')) !!}

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-4">

            <div class="form-group">

                <strong>تلفن ۳:</strong>

                {!! Form::text('phone3', old('phone3',$clue->phone3), array('placeholder' => '','class' => 'form-control')) !!}

            </div>

        </div>

        <div class="col-xs-12 col-sm-12 col-md-4">

            <div class="form-group">

                <strong>موبایل:</strong>

                {!! Form::text('mobile', old('mobile',$clue->mobile), array('placeholder' => '','class' => 'form-control')) !!}

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-4">

            <div class="form-group">

                <strong>موبایل ۲:</strong>

                {!! Form::text('mobile2', old('mobile2',$clue->mobile2), array('placeholder' => '','class' => 'form-control')) !!}

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-4">

            <div class="form-group">

                <strong>موبایل ۳:</strong>

                {!! Form::text('mobile3', old('mobile3',$clue->mobile3), array('placeholder' => '','class' => 'form-control')) !!}

            </div>

        </div>
		
		
		
		
		
		
		
		
		<div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>تلفن سپیدار:</strong>

                {!! Form::text('sepidar_phone', old('sepidar_phone',$clue->sepidar_phone), array('placeholder' => '','class' => 'form-control')) !!}

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>کد سپیدار:</strong>

                {!! Form::text('sepidar_code', old('sepidar_code',$clue->sepidar_code), array('placeholder' => '','class' => 'form-control')) !!}

            </div>

        </div>
		
		
		
		
		
		
		
		
		
		
		
		
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
			@if($existsFlag1 || $existsFlag2 || $existsFlag3)
            <button type="submit" class="btn btn-primary">ثبت</button>
			@else
			<div class="alert alert-danger">
				<small>
			  <strong>اخطار!</strong> برای ویرایش سرنخ، به استان و شهر و منطقه آن دسترسی ندارید.
			  </small>
			</div>
			@endif
        </div>

    </div>

    {!! Form::close() !!}







	<script>




        function change_list(master_el_id,child_el_id,loading_gif_id,ajax_req_url,depend_element_ids = []){

            var e = document.getElementById(master_el_id);
            r_id = e.options[e.selectedIndex].value;

            if(r_id==""){
                $('#'+child_el_id).html('');
                return;
            }

            $('#'+master_el_id).prop('disabled', true);
            //$('#'+loading_gif_id).css('display', 'block'); //TODO: enable loadings

            token= $('body').find("input[name='_token']").val();

            $.ajax({
                url: ajax_req_url,
                type: 'GET',
                data: {"r_id":r_id, "_token":token },
                success: function(data){

                    $('#'+child_el_id).html('');
                    html_str='<option value="" >انتخاب کنید</option>';
                    $('#'+child_el_id).append(html_str);
                    $.each(data, function( index, value ) {
                        html_str='<option value="'+value.id+'" >'+value.title+'</option>';
                        $('#'+child_el_id).append(html_str);
                    });

                    depend_element_ids.forEach(function(){
                        $('#'+depend_element_ids).empty();
                    });

                    $('#'+master_el_id).prop('disabled', false);
                    //$('#'+loading_gif_id).css('display', 'none');
                },
                error: function(data){
                    alert(data.responseText);
                    alert('دریافت اطلاعات با خطا مواجه شد!');
                    $('#'+master_el_id).prop('disabled', false);
                    //$('#'+loading_gif_id).css('display', 'none');
                }
            });

        }
    </script>









@endsection
