@extends('panel.layout.master')
@section('main_content')

    <div class="row mb-md-5">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2>ایجاد سرنخ جدید</h2>

            </div>

            <div class="pull-left">

                <a class="btn btn-primary" href="{{ route('clues.index') }}"> بازگشت</a>

            </div>

        </div>

    </div>


    @if (count($errors) > 0)

        <div class="alert alert-danger">

            <strong>خطا!</strong> در مقادیر ورودی ایراداتی وجود دارد.<br><br>

            <ul>

                @foreach ($errors->all() as $error)

                    <li>{{ $error }}</li>

                @endforeach

            </ul>

        </div>

    @endif



    {!! Form::open(array('route' => 'clues.store','method'=>'POST', 'id'=>'clue_form')) !!}

    <div class="row mb-5">


		<div class="col-xs-12 col-sm-12 col-md-12 form-input-container">

			<div class="form-group">

				<strong>عنوان سرنخ:</strong>

				<input type="text" name="clue_title" value="{{ old('clue_title')??'' }}" class="form-control" placeholder="">

			</div>

		</div>
		
		
		<div class="col-xs-12 col-sm-12 col-md-6 form-input-container">

			<div class="form-group">

				<strong>نتیجه سرنخ:</strong>

				<select  name='clue_closed' id='clue_closed' class='form-control pull-right'>
					<option value="" >انتخاب کنید</option>
					<option @if(old('clue_closed')=='1') selected @endif value="1" >بسته</option>
					<option @if(old('clue_closed')=='0') selected @endif value="0" >باز</option>
				</select>

			</div>

		</div>
		
		
		<div class="col-xs-12 col-sm-12 col-md-6 form-input-container">

			<div class="form-group">

				<strong>کمپین مرتبط:</strong>

				<select  name='campaign_id' id='campaign_id' class='form-control pull-right'>
					<option {{(!old('campaign_id', isset($clue->campaign_id) ? $clue->campaign_id : '')? 'selected' : '')}} value="">انتخاب کنید</option>
					@foreach($campaigns as $campaign)
						<option @if(old('campaign_id', isset($clue->campaign_id) ? $clue->campaign_id : '')==$campaign->id) selected @endif value="{{$campaign->id}}" >{{$campaign->campaign_title}}</option>
					@endforeach
				</select>

			</div>

		</div>
		
		
		
		
		
		
		
		<div class="col-xs-12 col-sm-12 col-md-4">

            <div class="form-group">

                <strong>نام:</strong>

                {!! Form::text('first_name', old('first_name'), array('placeholder' => '','class' => 'form-control')) !!}

            </div>

        </div>
		
		
		<div class="col-xs-12 col-sm-12 col-md-4">

            <div class="form-group">

                <strong>نام خانوادگی:</strong>

                {!! Form::text('last_name', old('last_name'), array('placeholder' => '','class' => 'form-control')) !!}

            </div>

        </div><div class="col-xs-12 col-sm-12 col-md-4">

            <div class="form-group">

                <strong>فروشگاه:</strong>

                {!! Form::text('business_name', old('business_name'), array('placeholder' => '','class' => 'form-control')) !!}

            </div>

        </div>
        
        
        <div class="col-xs-12 col-sm-12 col-md-4 form-input-container">

			<div class="form-group">

				<strong>استان(*):</strong>

				<select required  name='state_id' id='state_id' class='form-control pull-right' onChange="change_list('state_id','city_id','loading_gif_cities','{{route('getCities')}}',['region_id'])"> 				<!-- TODO: init vars -->
					<option {{(!old('state_id', isset($business->state_id) ? $business->state_id : '')? 'selected' : '')}} value="">انتخاب کنید</option>
					@foreach($states as $state)
						<option @if(old('state_id', isset($business->state_id) ? $business->state_id : '')==$state->id) selected @endif value="{{$state->id}}" >{{$state->unit_name}}</option>
					@endforeach
				</select>

			</div>

		</div>



		<div class="col-xs-12 col-sm-12 col-md-4 form-input-container">

			<div class="form-group">

				<strong>شهر(*):</strong>

				<select required  name='city_id' id='city_id' class='form-control pull-right' onChange="change_list('city_id','region_id','loading_gif_regions','{{route('getCities')}}')">
					@if($cities)
						<option {{(!old('city_id', isset($business->city_id) ? $business->city_id : '')? 'selected' : '')}} value="">انتخاب کنید</option>
						@foreach($cities as $city)
							<option @if(old('city_id', isset($business->city_id) ? $business->city_id : '')==$city->id) selected @endif value="{{$city->id}}" >{{$city->unit_name}}</option>
						@endforeach
					@endif
				</select>

			</div>

		</div>


		<div class="col-xs-12 col-sm-12 col-md-4 form-input-container">

			<div class="form-group">

				<strong>منطقه:</strong>

				<select  name='region_id' id='region_id' class='form-control pull-right'>
					@if($regions)
						<option {{(!old('region_id', isset($business->region_id) ? $business->region_id : '')? 'selected' : '')}} value="">انتخاب کنید</option>
						@foreach($regions as $region)
							<option @if(old('region_id', isset($business->region_id) ? $business->region_id : '')==$region->id) selected @endif value="{{$region->id}}" >{{$region->unit_name}}</option>
						@endforeach
					@endif
				</select>

			</div>

		</div>

        <div class="col-xs-12 col-sm-12 col-md-12 form-input-container">

            <div class="form-group">

                <strong>آدرس:</strong>

                {!! Form::text('address', old('address'), array('placeholder' => '','class' => 'form-control')) !!}

            </div>

        </div>

        <div class="col-xs-12 col-sm-12 col-md-4 ">

            <div class="form-group">

                <strong>تلفن ۱:</strong>

                {!! Form::text('phone', old('phone'), array('placeholder' => '','class' => 'form-control')) !!}

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-4">

            <div class="form-group">

                <strong>تلفن ۲:</strong>

                {!! Form::text('phone2', old('phone2'), array('placeholder' => '','class' => 'form-control')) !!}

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-4 form-input-container">

            <div class="form-group">

                <strong>تلفن ۳:</strong>

                {!! Form::text('phone3', old('phone3'), array('placeholder' => '','class' => 'form-control')) !!}

            </div>

        </div>

        <div class="col-xs-12 col-sm-12 col-md-4 form-input-container">

            <div class="form-group">

                <strong>موبایل ۱:</strong>

                {!! Form::text('mobile', old('mobile'), array('placeholder' => '','class' => 'form-control')) !!}

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-4 form-input-container">

            <div class="form-group">

                <strong>موبایل ۲:</strong>

                {!! Form::text('mobile2', old('mobile2'), array('placeholder' => '','class' => 'form-control')) !!}

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-4 form-input-container">

            <div class="form-group">

                <strong>موبایل ۳:</strong>

                {!! Form::text('mobile3', old('mobile3'), array('placeholder' => '','class' => 'form-control')) !!}

            </div>

        </div>
		
		
		


		



		

        

        
        
        

        
        

        <div class="col-xs-12 col-sm-12 col-md-12 text-center">

            <button type="submit" id="submit_btn" class="btn btn-primary">ثبت</button>

        </div>

    </div>

    {!! Form::close() !!}






	<script>




        function change_list(master_el_id,child_el_id,loading_gif_id,ajax_req_url,depend_element_ids = []){

            var e = document.getElementById(master_el_id);
            r_id = e.options[e.selectedIndex].value;

            if(r_id==""){
                $('#'+child_el_id).html('');
                return;
            }

            $('#'+master_el_id).prop('disabled', true);
            //$('#'+loading_gif_id).css('display', 'block'); //TODO: enable loadings

            token= $('body').find("input[name='_token']").val();

            $.ajax({
                url: ajax_req_url,
                type: 'GET',
                data: {"r_id":r_id, "_token":token },
                success: function(data){

                    $('#'+child_el_id).html('');
                    html_str='<option value="" >انتخاب کنید</option>';
                    $('#'+child_el_id).append(html_str);
                    $.each(data, function( index, value ) {
                        html_str='<option value="'+value.id+'" >'+value.title+'</option>';
                        $('#'+child_el_id).append(html_str);
                    });

                    depend_element_ids.forEach(function(){
                        $('#'+depend_element_ids).empty();
                    });

                    $('#'+master_el_id).prop('disabled', false);
                    //$('#'+loading_gif_id).css('display', 'none');
                },
                error: function(data){
                    alert(data.responseText);
                    alert('دریافت اطلاعات با خطا مواجه شد!');
                    $('#'+master_el_id).prop('disabled', false);
                    //$('#'+loading_gif_id).css('display', 'none');
                }
            });

        }
		
		
		
		$('#clue_form').bind('submit', function (e) {
			var button = $('#submit_btn');

			// Disable the submit button while evaluating if the form should be submitted
			button.prop('disabled', true);

			var valid = true;    

			// Do stuff (validations, etc) here and set
			// "valid" to false if the validation fails

			if (!valid) { 
				// Prevent form from submitting if validation failed
				e.preventDefault();

				// Reactivate the button if the form was not submitted
				button.prop('disabled', false);
			}
		});
    </script>










@endsection
