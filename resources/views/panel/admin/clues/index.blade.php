@extends('panel.layout.master')
@section('additional_css')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />

<style>


.select2-container--default .select2-selection--multiple .select2-selection__choice{
	
	color: #424242;
	
	
}


</style>
@endsection
@section('main_content')
    <div class="row mb-md-5">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2>مدیریت سرنخ ها</h2>

            </div>

            <div class="pull-left">

                <a class="btn btn-success" href="{{ route('clues.create') }}"> ایجاد سرنخ جدید</a>

            </div>

        </div>

    </div>


    @if ($message = Session::get('success'))

        <div class="alert alert-success">

            <p>{{ $message }}</p>

        </div>

    @endif
    
    @if ($message = Session::get('danger'))

        <div class="alert alert-danger">

            <p>{{ $message }}</p>

        </div>

    @endif
    
    
    
    
    
<div class="row">    
    <div class="col-lg-12 mb-md-5">
        <form action="{{route('clues.index')}}" method="GET">
        <div class="row">
	    
            
            
            <div class="col-xs-12 col-sm-12 col-md-2 form-input-container">

                <div class="form-group">

                    <strong>عنوان:</strong>

                    <input type="text" name="clue_title" id="clue_title" value="{{ old('clue_title',request()->clue_title) }}" class="form-control" placeholder="بخشی از عنوان سرنخ">

                </div>

            </div>
			
			
			<div class="col-xs-12 col-sm-12 col-md-2 form-input-container">

                <div class="form-group">

                    <strong>نام:</strong>

                    <input type="text" name="first_name" id="first_name" value="{{ old('first_name',request()->first_name) }}" class="form-control" placeholder="بخشی از نام">

                </div>

            </div>
			
			<div class="col-xs-12 col-sm-12 col-md-2 form-input-container">

                <div class="form-group">

                    <strong>نام خانوادگی:</strong>

                    <input type="text" name="last_name" id="last_name" value="{{ old('last_name',request()->last_name) }}" class="form-control" placeholder="بخشی از نام خانوادگی">

                </div>

            </div>
			
			
			<div class="col-xs-12 col-sm-12 col-md-2 form-input-container">

                <div class="form-group">

                    <strong>نام فروشگاه:</strong>

                    <input type="text" name="business_name" id="business_name" value="{{ old('business_name',request()->business_name) }}" class="form-control" placeholder="بخشی از نام فروشگاه">

                </div>

            </div>
			
			
			<div class="col-xs-12 col-sm-12 col-md-2 form-input-container">

                <div class="form-group">

                    <strong>آدرس:</strong>

                    <input type="text" name="address" id="address" value="{{ old('address',request()->address) }}" class="form-control" placeholder="بخشی از آدرس">

                </div>

            </div>
			
			
			
			<div class="col-xs-12 col-sm-12 col-md-2 form-input-container">

                <div class="form-group">

                    <strong>تلفن یا موبایل:</strong>

                    <input type="text" name="phone" id="phone" value="{{ old('phone',request()->phone) }}" class="form-control" placeholder="بخشی از تلفن یا موبایل">

                </div>

            </div>
			
			
			
			
			<div class="col-xs-12 col-sm-12 col-md-2 form-input-container">

				<div class="form-group">

					<strong>استان:</strong>

					<select  name='state_id' id='state_id' class='form-control pull-right' onChange="change_list('state_id','city_id','loading_gif_cities','{{route('getCities')}}',['region_id'])"> 
					<option {{(!old('state_id', isset(request()->state_id) ? request()->state_id : '')? 'selected' : '')}} value="">انتخاب کنید</option>
					@foreach($states as $state)
						<option @if(old('state_id', isset(request()->state_id) ? request()->state_id : '')==$state->id) selected @endif value="{{$state->id}}" >{{$state->unit_name}}</option>
					@endforeach
					</select>

				</div>

			</div>
		
		
			<div class="col-xs-12 col-sm-12 col-md-2 form-input-container">

				<div class="form-group">

					<strong>شهر:</strong>

					<select  name='city_id' id='city_id' class='form-control pull-right' onChange="change_list('city_id','region_id','loading_gif_regions','{{route('getCities')}}')">
					@if($cities)
						<option {{(!old('city_id', isset(request()->city_id) ? request()->city_id : '')? 'selected' : '')}} value="">انتخاب کنید</option>
						@foreach($cities as $city)
						<option @if(old('city_id', isset(request()->city_id) ? request()->city_id : '')==$city->id) selected @endif value="{{$city->id}}" >{{$city->unit_name}}</option>
						@endforeach
					@endif
					</select>

				</div>

			</div>
			

			<div class="col-xs-12 col-sm-12 col-md-2 form-input-container">

				<div class="form-group">

					<strong>منطقه:</strong>

					<select  name='region_id' id='region_id' class='form-control pull-right'>
						@if($regions)
							<option {{(!old('region_id', isset(request()->region_id) ? request()->region_id : '')? 'selected' : '')}} value="">انتخاب کنید</option>
							@foreach($regions as $region)
								<option @if(old('region_id', isset(request()->region_id) ? request()->region_id : '')==$region->id) selected @endif value="{{$region->id}}" >{{$region->unit_name}}</option>
							@endforeach
						@endif
					</select>

				</div>

			</div>

			
			<div class="col-xs-12 col-sm-12 col-md-2 form-input-container">

				<div class="form-group">

					<strong>ایجاد کننده:</strong>

					<select  name='creator_user_id' class='form-control pull-right'> 
					<option {{(!old('creator_user_id', isset(request()->creator_user_id) ? request()->creator_user_id : '')? 'selected' : '')}} value="">انتخاب کنید</option>
					@foreach($creatorUsers as $creatorUser)
						<option @if(old('creator_user_id', isset(request()->creator_user_id) ? request()->creator_user_id : '')==$creatorUser->id) selected @endif value="{{$creatorUser->id}}" >{{$creatorUser->name}}({{$creatorUser->email}})</option>
					@endforeach
					</select>

				</div>

			</div>
			
			
			<div class="col-xs-12 col-sm-12 col-md-2 form-input-container">

				<div class="form-group">

					<strong>کمپین مرتبط:</strong>

					<select  name='campaign_id' class='form-control pull-right'> 
					<option {{(!old('campaign_id', isset(request()->campaign_id) ? request()->campaign_id : '')? 'selected' : '')}} value="">انتخاب کنید</option>
					@foreach($campaignList as $campaign)
						<option @if(old('campaign_id', isset(request()->campaign_id) ? request()->campaign_id : '')==$campaign->id) selected @endif value="{{$campaign->id}}" >{{$campaign->campaign_title}}</option>
					@endforeach
					</select>

				</div>

			</div>
			
			
			
			<div class="col-xs-12 col-sm-12 col-md-2 form-input-container">

				<div class="form-group">

					<strong>وضعیت سرنخ:</strong>

					<select  name='clue_status' class='form-control pull-right'> 
					<option {{(!old('clue_status', isset(request()->clue_status) ? request()->clue_status : '')? 'selected' : '')}} value="">انتخاب کنید</option>
					@foreach($clueStatuses as $key=>$clueStatusTitle)
						<option @if(old('clue_status', isset(request()->clue_status) ? request()->clue_status : '')==$key) selected @endif value="{{$key}}" >{{$clueStatusTitle}}</option>
					@endforeach
					</select>

				</div>

			</div>
			
			
			<div class="col-xs-12 col-sm-12 col-md-2 form-input-container">

				<div class="form-group">

					<strong>نتیجه سرنخ:</strong>

					<select  name='clue_closed' class='form-control pull-right'> 
					<option {{(!old('clue_closed', isset(request()->clue_closed) ? request()->clue_closed : '')? 'selected' : '')}} value="">انتخاب کنید</option>
					@foreach($clueClosedStatuses as $key=>$statusTitle)
						<option @if(old('clue_closed', isset(request()->clue_closed) ? request()->clue_closed : '')==$key) selected @endif value="{{$key}}" >{{$statusTitle}}</option>
					@endforeach
					</select>

				</div>

			</div>
			
			
			
			
			<div class="col-xs-12 col-sm-12 col-md-12 form-input-container">

				<div class="form-group">

					<strong>محصول مرتبط با سرنخ:</strong>


					<select class="js-basic-multiple" name="clue_product_id[]" multiple="multiple" style="width:100%;">
						@foreach($productsList as $productItem)
							@php
							$selected = '';
							if(isset(request()->clue_product_id) && in_array($productItem->id,request()->clue_product_id)){
								$selected = 'selected="selected"';
							}
							@endphp
						
							<option {{$selected}} value="{{$productItem->id}}">{{$productItem->product_name}}(کد: {{$productItem->product_code}})</option>
						@endforeach
					</select>



				</div>

			</div>
			
			
			
            
	
	
        </div>
        
        <div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 form-input-container">
			<div class="pull-left">
			<button type="submit" class="btn btn-success" href="{{ route('clues.index') }}">فیلتر</button>
			</div>
			
			<div class="pull-left ml-3">
				<button type="button" class="btn btn-warning" onclick="exportExcel()">خروجی اکسل</button>
				<script>
					function exportExcel(){
						const urlParams = new URLSearchParams(window.location.search);

						urlParams.set('export_excel', 1);

						window.location.search = urlParams;
					}
				</script>
			</div>
			
			
		</div>
        </div>
        </form>
    </div>
</div>
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

	<div class="table-responsive">
    <table class="table table-bordered">

        <tr>

            <th>ردیف</th>

            <th>عنوان سرنخ</th>
            <th>وضعیت</th>
			<th>نتیجه</th>
			@if(auth()->user()->user_type=='SALES_MANAGER')
				<th>مسئول</th>
			@else
				<th>کاربر ایجاد کننده</th>
			@endif
			<th>تاریخ ایجاد</th>
			<th>تاریخ آخرین بروزرسانی</th>
			@can('set-alarm-on-customers')
            <th>آلارم</th>
			@endcan
            
            <th width="280px">عملیات</th>

        </tr>

        @foreach ($clues as $key => $clue)

            <tr>

                <td>{{ ++$i }}</td>

                <td>
                
                
					<a target="_blank" class="" href="{{ route('clues.showCartable',['clue_id'=>$clue->id]) }}">
					
					
					{{ $clue->clue_title }}
					
					
					</a>
                
                </td>
				
				
				
				<td>
				@php
					
					$clueTitlesArr = [
						'clue' => 'ثبت سرنخ',
						'customer' => 'ثبت مشتری',
						'validation' => 'اعتبارسنجی',
						'sale' => 'فروش',
						'pre_invoice'=>'‍‍پیش فاکتور',
						'invoice'=>'فاکتور',
						'return_from_sale' => 'برگشت از فروش'
					];
					$clueStatusTitle = $clueTitlesArr[$clue->clue_status];
				@endphp
				{{ $clueStatusTitle }}
				</td>
				
				
				
				<td>
				@php
					
					$clueClosedArr = [
						0 => 'باز',
						1 => 'بسته',
					];
					$clueClosedTitle = $clueClosedArr[$clue->clue_closed];
				@endphp
				@php
					$class="badge badge-success";
					if($clue->clue_closed==1){
						$class="badge badge-danger";
					}
				@endphp
				<span class="{{$class}}">
				{{ $clueClosedTitle }}
				</span>
				</td>
				
				
				
				<td>
				
				@if(auth()->user()->user_type=='SALES_MANAGER')
					{{auth()->user()->name}}
				@elseif(!empty($clue->creator()->first()))
					{{$clue->creator()->first()->name;}}
				@elseif($clue->is_sepidar_user)
					کاربر سپیدار
				@else
					-
				@endif
				
                </td>
				
				<td>{{ \Morilog\Jalali\Jalalian::forge($clue->created_at)->format('%A, %d %B %Y در ساعت H:i:s') }}</td>
				
				<td>{{ !empty($clue->updated_at)?(\Morilog\Jalali\Jalalian::forge($clue->updated_at)->format('%A, %d %B %Y در ساعت H:i:s')):'-' }}</td>
				
				
				@can('set-alarm-on-customers')
				<td>
					<a href="{{ route('specialists.set-alarm-to-specialist-daily-call-target',['clue_id'=>$clue->id,'return_url'=>request()->getRequestUri()]) }}" onclick="return confirm('آیا مطمئنید؟')">تنظیم آلارم</a>
				</td>
				@endcan
                

			
                <td>
                
                    <a class="btn btn-primary" href="{{ route('clues.edit',$clue->id) }}">ویرایش</a>

                    {!! Form::open(['method' => 'DELETE','route' => ['clues.destroy', $clue->id],'style'=>'display:inline']) !!}

                    {!! Form::submit('حذف', ['class' => 'btn btn-danger']) !!}

                    {!! Form::close() !!}

                </td>

            </tr>

        @endforeach

    </table>
	</div>

    {!! $clues->appends(Request::all())->render() !!}



@endsection


@section('additional_js_bottom')

<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>


<script>

$(document).ready(function() {
    $('.js-basic-multiple').select2();
});


</script>


<script>
function change_list(master_el_id,child_el_id,loading_gif_id,ajax_req_url,depend_element_ids = []){

    var e = document.getElementById(master_el_id);
    r_id = e.options[e.selectedIndex].value;

    if(r_id==""){
        $('#'+child_el_id).html('');
        return;
    }

    $('#'+master_el_id).prop('disabled', true);
    //$('#'+loading_gif_id).css('display', 'block'); //TODO: enable loadings

    token= $('body').find("input[name='_token']").val();

    $.ajax({
        url: ajax_req_url,
        type: 'GET',
        data: {"r_id":r_id, "_token":token },
        success: function(data){

            $('#'+child_el_id).html('');
            html_str='<option value="" >انتخاب کنید</option>';
            $('#'+child_el_id).append(html_str);
            $.each(data, function( index, value ) {
                html_str='<option value="'+value.id+'" >'+value.title+'</option>';
                $('#'+child_el_id).append(html_str);
            });

            depend_element_ids.forEach(function(){
                $('#'+depend_element_ids).empty();
            });

            $('#'+master_el_id).prop('disabled', false);
            //$('#'+loading_gif_id).css('display', 'none');
        },
        error: function(data){
            alert(data.responseText);
            alert('دریافت اطلاعات با خطا مواجه شد!');
            $('#'+master_el_id).prop('disabled', false);
            //$('#'+loading_gif_id).css('display', 'none');
        }
    });

}
</script>
@endsection