@extends('panel.layout.master')
@section('additional_js_top2')

<script src="{{asset('highcharts/highcharts.js')}}"></script>
<script src="{{asset('highcharts/modules/exporting.js')}}"></script>
<script src="{{asset('highcharts/modules/export-data.js')}}"></script>
<script src="{{asset('highcharts/modules/accessibility.js')}}"></script>

@endsection
@section('main_content')
    <div class="row mb-md-5">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2>گزارش جامع آماری سرنخ ها</h2>

            </div>

           

        </div>

    </div>


    @if ($message = Session::get('success'))

        <div class="alert alert-success">

            <p>{{ $message }}</p>

        </div>

    @endif
    
    @if ($message = Session::get('danger'))

        <div class="alert alert-danger">

            <p>{{ $message }}</p>

        </div>

    @endif
    
    
    
    <div class="row">    
    <div class="col-lg-12 mb-md-5">
        <form action="{{route('clues.showCluesReport')}}" method="GET">
        <div class="row">
	    
            
            
            
            <div class="col-xs-12 col-sm-12 col-md-2 form-input-container">

                <div class="form-group">

                    <strong>از تاریخ:</strong>

                    <input data-jdp type="text" name="from_date" id="from_date" value="{{ old('from_date',request()->from_date) }}" class="form-control" placeholder="">

                </div>

            </div>
            
            
            <div class="col-xs-12 col-sm-12 col-md-2 form-input-container">

                <div class="form-group">

                    <strong>تا تاریخ:</strong>
                    
                    <input data-jdp type="text" name="to_date" id="to_date" value="{{ old('to_date',request()->to_date) }}" class="form-control" placeholder="">

                </div>

            </div>
            
			
			
			
			<div class="col-xs-12 col-sm-12 col-md-2 form-input-container">

				<div class="form-group">

					<strong>استان:</strong>

					<select  name='state_id' id='state_id' class='form-control pull-right' onChange="change_list('state_id','city_id','loading_gif_cities','{{route('getCities')}}',['region_id'])"> 
					<option {{(!old('state_id', isset(request()->state_id) ? request()->state_id : '')? 'selected' : '')}} value="">انتخاب کنید</option>
					@foreach($states as $state)
						<option @if(old('state_id', isset(request()->state_id) ? request()->state_id : '')==$state->id) selected @endif value="{{$state->id}}" >{{$state->unit_name}}</option>
					@endforeach
					</select>

				</div>

			</div>
	
	
			<div class="col-xs-12 col-sm-12 col-md-2 form-input-container">

				<div class="form-group">

					<strong>شهر:</strong>

					<select  name='city_id' id='city_id' class='form-control pull-right'>
					@if($cities)
						<option {{(!old('city_id', isset(request()->city_id) ? request()->city_id : '')? 'selected' : '')}} value="">انتخاب کنید</option>
						@foreach($cities as $city)
						<option @if(old('city_id', isset(request()->city_id) ? request()->city_id : '')==$city->id) selected @endif value="{{$city->id}}" >{{$city->unit_name}}</option>
						@endforeach
					@endif
					</select>

				</div>

			</div>
	

		
			
			<div class="col-xs-12 col-sm-12 col-md-2 form-input-container">

				<div class="form-group">

					<strong>بازه زمانی:</strong>

					<select  name='report_period' id='report_period' class='form-control pull-right' autocomplete='off'>
					 <option @if(old('report_period', isset(request()->report_period) ? request()->report_period : '')==2) selected @endif value="2" >ماهانه</option>
					 <option @if(old('report_period', isset(request()->report_period) ? request()->report_period : '')==1) selected @endif value="1" >روزانه</option>
					</select>

				</div>

			</div>
				
				
				
				
			
			
			
	
        </div>
        
        <div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 form-input-container">
				<div class="pull-left">
				<button type="submit" class="btn btn-success">دریافت گزارش</button>
				</div>
			</div>
        </div>
        </form>
    </div>
</div>


<div class="row mt-5">   
    <div class="col-lg-12 mb-md-5">
    
		<ul class="list-unstyled">
		  <li>تعداد کل سرنخ‌های ثبت شده: {{$allRegisteredCluesCount}}</li>
		  <li>تعداد وضعیت‌های تبدیل شده به فروش: {{$allChangedToSaleCluesCount}}</li>
		  <li>نرخ تبدیل سرنخ‌های بالقوه به بالفعل: {{$changeToSaleStatusRate}}%</li>
		</ul>
	
   </div>
</div>

  
<div class="row mt-5">   
    <div class="col-lg-12 mb-md-5">
    <figure class="highcharts-figure">
		<div id="container"></div>
		
	</figure>
   </div>
</div>


<div class="row mt-5">   
   <div class="col-xs-12 col-sm-12 col-md-6">
    <figure class="highcharts-figure">
		<div id="chartClosedClues"></div>
		
	</figure>
   </div>
   
   
   <div class="col-xs-12 col-sm-12 col-md-6">
    <figure class="highcharts-figure">
		<div id="chartClueStatusGrouped"></div>
		
	</figure>
   </div>
   
   
   
   
</div>

@endsection


@section('additional_js_bottom')
<script>
function change_list(master_el_id,child_el_id,loading_gif_id,ajax_req_url,depend_element_ids = []){

    var e = document.getElementById(master_el_id);
    r_id = e.options[e.selectedIndex].value;

    if(r_id==""){
        $('#'+child_el_id).html('');
        return;
    }

    $('#'+master_el_id).prop('disabled', true);
    //$('#'+loading_gif_id).css('display', 'block'); //TODO: enable loadings

    token= $('body').find("input[name='_token']").val();

    $.ajax({
        url: ajax_req_url,
        type: 'GET',
        data: {"r_id":r_id, "_token":token },
        success: function(data){

            $('#'+child_el_id).html('');
            html_str='<option value="" >انتخاب کنید</option>';
            $('#'+child_el_id).append(html_str);
            $.each(data, function( index, value ) {
                html_str='<option value="'+value.id+'" >'+value.title+'</option>';
                $('#'+child_el_id).append(html_str);
            });

            depend_element_ids.forEach(function(){
                $('#'+depend_element_ids).empty();
            });

            $('#'+master_el_id).prop('disabled', false);
            //$('#'+loading_gif_id).css('display', 'none');
        },
        error: function(data){
            alert(data.responseText);
            alert('دریافت اطلاعات با خطا مواجه شد!');
            $('#'+master_el_id).prop('disabled', false);
            //$('#'+loading_gif_id).css('display', 'none');
        }
    });

}


</script>



<script type="text/javascript">
Highcharts.chart('container', {
    chart: {
        type: 'column',
		plotShadow: true
    },
    title: {
        text: 'نمودار تعداد سرنخ های ثبت شده'
    },
    xAxis: {
        categories: [
        		"تاریخ"
        		],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'تعداد سرنخ ها'
        }
    },
    tooltip: {
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: [
    
    	@foreach($chart1DataArray as $key=>$chartData)
    		{
    			name: '{{$key}}',
    			data: [{{$chartData}}]
    		}
    		@if($key!=array_key_last($chart1DataArray))
    		,
    		@endif
    	@endforeach
    
    ]
});













Highcharts.chart('chartClosedClues', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: 0,
        plotShadow: true
    },
	
    title: {
        text: 'نتیجه سرنخ ها(باز/بسته)',
        align: 'center'
        //verticalAlign: 'middle',
        //y: 60
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    accessibility: {
        point: {
            valueSuffix: '%'
        }
    },
    plotOptions: {
        pie: {
            dataLabels: {
                enabled: true,
                distance: -50,
                style: {
                    fontWeight: 'bold',
                    color: 'white',
					fontSize: "16px"
                }
            },
            startAngle: -90,
            endAngle: 90,
            center: ['50%', '75%'],
            size: '110%'
        }
    },
    series: [{
        type: 'pie',
        name: 'Browser share',
        innerSize: '40%',
        data: [
		
			@foreach($chartOpenClosedCluesDataArray as $key=>$chartData)
    		[
    			'{{$chartData["title"]}}', {{$chartData["count"]}}
    		]
    		@if($key!=array_key_last($chartOpenClosedCluesDataArray))
    		,
    		@endif
			@endforeach
		
	
        ]
    }]
});


















Highcharts.chart('chartClueStatusGrouped', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: true,
        type: 'pie'
    },
    title: {
        text: 'نمودار سرنخ ها بر اساس وضعیت',
        align: 'center'
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    accessibility: {
        point: {
            valueSuffix: '%'
        }
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.percentage:.1f} %',
				style: {
					fontSize: "14px"
                }
            }
        }
    },
    series: [{
        name: 'وضعیت سرنخ ها',
        colorByPoint: true,
        data: [
		
		@foreach($chartStatusGroupedDataArray as $key=>$chartData)
    		{
    			name: '{{$chartData["title"]}}',
    			y: {{$chartData["count"]}}
    		}
    		@if($key!=array_key_last($chartStatusGroupedDataArray))
    		,
    		@endif
    	@endforeach
		
		/*
		{
            name: 'Chrome',
            y: 70.67,
            sliced: true,
            selected: true
        }
		*/]
    }]
});


</script>


@endsection



