@extends('panel.layout.master')
@section('additional_css')


<style>

.create-invoice-col a{
  display:block;
}


.select2-container--default .select2-selection--multiple .select2-selection__choice{
	
	color: #424242;
	
	
}


</style>

<link rel="stylesheet" href="{{asset('css/bootstrap-steps.min.css')}}">

<style>
.steps{
	direction: ltr !important;
}
.step-content{
	min-width: 10rem;
}

.step-circle::before {
	width: calc(10rem + 1rem - 1.5rem);
}
.step-text{
		font-size: 10px;
}
</style>

    
@stop
@section('main_content')


    <div class="row mb-md-5">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2>کارتابل سرنخ : {{$clue->clue_title}}</h2>

            </div>

            <div class="pull-left">


            </div>

        </div>

    </div>
	

	<div class="row mb-md-5">

        <div class="col-lg-12 margin-tb">

            <ul class="steps">
			  <li class="step {{($clue->clue_status=='clue')?'step-error':'step-success'}}">
				<div class="step-content">
				  <span class="step-circle">1</span>
				  <span class="step-text">ثبت سرنخ(مشتری بالقوه)</span>
				</div>
			  </li>
			  <li class="step 
								
								@if($clue->clue_status=='validation')
									step-error
								@elseif($clue->clue_status=='sale' || $clue->clue_status=='return_from_sale')
									step-success
								@else
									
								@endif
								">
				<div class="step-content">
				  <span class="step-circle">2</span>
				  <span class="step-text">اعتبارسنجی(مشتری بالقوه)</span>
				</div>
			  </li>
			  <li class="step">
				<div class="step-content
				
						@if($clue->clue_status=='sale')
							step-error
						@elseif($clue->clue_status=='return_from_sale')
							step-success
						@else
							
						@endif
				
				">
				  <span class="step-circle">3</span>
				  <span class="step-text">فروش(مشتری بالفعل)</span>
				</div>
			  </li>
			  
			  <li class="step">
				<div class="step-content
				
						@if($clue->clue_status=='return_from_sale' && $clue->clue_closed==0)
							step-error
						@elseif($clue->clue_status=='return_from_sale' && $clue->clue_closed==1)
							step-success
						@else
							
						@endif
				
				">
				  <span class="step-circle">3</span>
				  <span class="step-text">برگشت از فروش(مشتری بالفعل)</span>
				</div>
			  </li>
			  
			  
			</ul>

        </div>

    </div>
	
	
	
    
    
    
    @if ($message = Session::get('success'))

        <div class="alert alert-success">

            <p>{{ $message }}</p>

        </div>

    @endif
    
    @if ($message = Session::get('danger'))

        <div class="alert alert-danger">

            <p>{{ $message }}</p>

        </div>

    @endif
	
	
	
	@if (count($errors) > 0)

        <div class="alert alert-danger">

            <strong>خطا!</strong> در مقادیر ورودی ایراداتی وجود دارد.<br><br>

            <ul>

                @foreach ($errors->all() as $error)

                    <li>{{ $error }}</li>

                @endforeach

            </ul>

        </div>

    @endif
    
    
    
    
    
    
<!-- BEGIN CLUE INFORMATION -->


<div class="row mt-3 mb-3">

<div class="col-lg-12">


<div class="card border-success">
<div class="card-header">اطلاعات سرنخ</div>
<div class="card-body">
						
				
<table class="table table-bordered">



<tr>
	<td>
		<strong>عنوان سرنخ:</strong>
                {{ $clue->clue_title }}
	</td>
	<td>
		<strong>وضعیت سرنخ:</strong>
				@php
					
					$clueTitlesArr = [
						'clue' => 'ثبت سرنخ',
						'customer' => 'ثبت مشتری',
						'validation' => 'اعتبارسنجی',
						'sale' => 'فروش',
						'pre_invoice'=>'‍‍پیش فاکتور',
						'invoice'=>'فاکتور',
						'return_from_sale' => 'برگشت از فروش'
					];
					$clueStatusTitle = $clueTitlesArr[$clue->clue_status];
				@endphp
				{{ $clueStatusTitle }}
	</td>
</tr>


<tr>
	<td>
		<strong>نتیجه سرنخ:</strong>
                @php
					
					$clueClosedArr = [
						0 => 'باز',
						1 => 'بسته',
					];
					$clueClosedTitle = $clueClosedArr[$clue->clue_closed];
				@endphp
				
				@php
					$class="badge badge-success";
					if($clue->clue_closed==1){
						$class="badge badge-danger";
					}
				@endphp
				<span class="{{$class}}">
				{{ $clueClosedTitle }}
				</span>
	</td>
	
	<td>
		شناسه: {{$clue->id}}
	</td>
</tr>





<tr>
	<td>
		<strong>نام:</strong>
                {{ $clue->first_name }}
	</td>
	<td>
		<strong>نام خانوادگی:</strong>
                {{ $clue->last_name }}
	</td>
</tr>

<tr>
	<td colspan="2">
		<strong>نام فروشگاه:</strong>
                {{ $clue->business_name }}
	</td>
</tr>


<tr>
	<td colspan="2">
		<strong>آدرس کامل:</strong>
                {{ !empty($clue->city_id)?$clue->getFullStateCityRegion():'' }}
				-
				{{ $clue->address }}
	</td>
</tr>

<tr>
	<td>
		<strong>تلفن ثابت ۱:</strong>
                {{ $clue->phone }}
	</td>
	<td>
		<strong>تلفن ثابت ۲:</strong>
                {{ $clue->phone2 }}
	</td>
</tr>


<tr>
	<td>
		<strong>تلفن ثابت ۳:</strong>
                {{ $clue->phone3 }}
	</td>
	<td>
		<strong>موبایل ۱:</strong>
                {{ $clue->mobile }}
	</td>
</tr>

<tr>
	<td>
		<strong>موبایل ۲:</strong>
                {{ $clue->mobile2 }}
	</td>
	<td>
		<strong>موبایل ۳:</strong>
                {{ $clue->mobile3 }}
	</td>
</tr>







<tr>
	<td >
		<strong>
		@if(auth()->user()->user_type=='SALES_MANAGER')
			مسئول:
		@else
			کاربر ایجاد کننده:
		@endif
		</strong>
		
		@if(auth()->user()->user_type=='SALES_MANAGER')
			{{auth()->user()->name}}
		@elseif(!empty($clue->creator()->first()))
			{{$clue->creator()->first()->name."(".$clue->creator()->first()->email.")";}}
		@elseif($clue->is_sepidar_user)
			کاربر سپیدار
		@else
			-
		@endif

	</td>
	
	<td >
		<strong>آخرین ویرایش سرنخ توسط:</strong>
		
		@php
				if(!empty($clue->lastUpdaterUser()->first())){
					echo $clue->lastUpdaterUser()->first()->name."(".$clue->lastUpdaterUser()->first()->email.")";
				}else{
					echo "-";
				}
                
        @endphp
	</td>
</tr>


<tr>
	<td>
		<strong>زمان ایجاد:</strong>
                {{\Morilog\Jalali\Jalalian::forge($clue->created_at)->format('%A, %d %B %Y در ساعت H:i:s')}}
	</td>
	<td>
		<strong>زمان آخرین ویرایش:</strong>
                {{\Morilog\Jalali\Jalalian::forge($clue->updated_at)->format('%A, %d %B %Y در ساعت H:i:s')}}
	</td>
</tr>



</table>
				
							
	
	
</div>
</div>

</div>

</div>


<!-- END CLUE INFORMATION -->





<!-- BEGIN CUSTOMER DESCRIPTION UPDATE -->


<div class="row mt-3 mb-3">

<div class="col-lg-12">


<div class="card border-success">
<div class="card-header">توضیحات کلی سرنخ</div>
<div class="card-body">
				
	<form action="{{ route('clues.updateClueDescription') }}" method="POST">
        	@csrf
        	<input type="hidden" name="clue_id" value="{{$clue->id}}">
		<div class="form-group">

			<textarea class="form-control" name="customer_description" placeholder="" rows="8">{{old('customer_description',$clue->customer_description)}}</textarea>

		</div>
		
		<div class="col-xs-12 col-sm-12 col-md-12 text-center">

			<button type="submit" class="btn btn-primary">ثبت تغییرات</button>

		</div>
		
	</form>
					
							
	
	
</div>
</div>

</div>

</div>


<!-- END CUSTOMER DESCRIPTION UPDATE -->




<!-- BEGIN CLUE PRODUCTS -->


<div class="row mt-3 mb-3">

<div class="col-lg-12">


<div class="card border-success">
<div class="card-header">محصولات مرتبط با این سرنخ</div>
<div class="card-body">
				
	<form action="{{ route('clues.updateClueProducts') }}" method="POST">
        	@csrf
        	<input type="hidden" name="clue_id" value="{{$clue->id}}">
		<div class="form-group">

			<select class="js-basic-multiple" name="clue_products[]" multiple="multiple" style="width:100%;">
				@foreach($productsList as $productItem)
					@php
					$selected = '';
					if(in_array($productItem->id,$clueProducts)){
						$selected = 'selected="selected"';
					}
					@endphp
				
					<option {{$selected}} value="{{$productItem->id}}">{{$productItem->product_name}}(کد: {{$productItem->product_code}})</option>
				@endforeach
			</select>


		</div>
		
		<div class="col-xs-12 col-sm-12 col-md-12 text-center">

			<button type="submit" class="btn btn-primary">ثبت محصولات</button>

		</div>
		
	</form>
					
							
	
	
</div>
</div>

</div>

</div>


<!-- END CLUE PRODUCTS -->



<!-- BEGIN CLUE PROCESS REPORT -->


<div class="row mt-3 mb-3">

<div class="col-lg-12">


<div class="card border-success">
<div class="card-header">توضیحات بازدید سرنخ</div>
<div class="card-body">
				
	<form action="{{ route('clues.addNewReportProcess') }}" method="POST">
        	@csrf
        	<input type="hidden" name="clue_id" value="{{$clue->id}}">
			<div class="form-group">

				<textarea class="form-control" name="process_text" placeholder="" rows="5">{{old('process_text')}}</textarea>

			</div>
			
			<div class="col-xs-12 col-sm-12 col-md-12 text-center">

				<button type="submit" class="btn btn-primary">ثبت توضیحات بازدید</button>

			</div>
		
	</form>
	
	
	
	
	
	
	
	
	
					
	@if(count($clueProcessReports)==0)
		<p class="card-text text-center">هنوز هیچ توضیحات بازدیدی برای این سرنخ ثبت نشده است.</p>
	@else				
					
	<table class="table table-bordered mt-5">

	<tr>
		<th style="width:66%">متن توضیحات بازدید</th>
		
		<td style="width:12%">تاریخ ثبت</td>
		<td style="width:12%">تاریخ آخرین ویرایش</td>
	</tr>





	@foreach ($clueProcessReports as $processReport)


	    <tr>



		<td>
		
		@if($processReport->creator()->first()->id==auth()->user()->id)
			<div class="form-group">
				<form action="{{route('clues.updateClueProcessReport',$clue->id)}}" method="POST">
					@csrf
					<input type="hidden" name="clue_process_report_id" value="{{$processReport->id}}">
					<textarea name="cartable_row_process_report_text" class="form-control">{{$processReport->process_text}}</textarea>
					<button type="submit" class="btn btn-success btn-small pull-left mt-1">بروزرسانی</button>
				</form>
			</div>
		@else
			{{$processReport->process_text}}
		@endif
		
		</td>
		

		

		<td>{{ \Morilog\Jalali\Jalalian::forge($processReport->created_at)->format('%Y/%m/%d H:i:s') }}</td>
		
		<td>{{ \Morilog\Jalali\Jalalian::forge($processReport->updated_at)->format('%Y/%m/%d H:i:s') }}</td>


	    </tr>

	@endforeach

	</table>
	{!! $clueProcessReports->links() !!}
					
					
	@endif	
	
	
	
	
	
	
	
	
	
	
					
							
	
	
</div>
</div>

</div>

</div>


<!-- END CLUE PROCESS REPORT -->







<!-- BEGIN CUSTOMER INFORMATION -->


<div class="row mt-3 mb-3">

<div class="col-lg-12">


<div class="card border-success">
<div class="card-header">فهرست بازاریابی</div>
<div class="card-body">
					
		
					
		@if(!empty($clueMarketingList) && count($clueMarketingList)>0)
			
			 <table class="table table-bordered mb-5">
			  <thead>
				<tr>
				  <th>صنف</th>
				  <th>شخص</th>
				  <th>سطح فعالیت</th>
				  <th>نحوه آشنایی</th>
				</tr>
			  </thead>
			  <tbody>
				@foreach($clueMarketingList as $clueMarketingItem)
				<tr>
				  <td>
					{{$clueMarketingItem->jobClass()->first()->job_class_title}}
				  </td>
				  <td>
					@php
						
						$valuesArray = [
							'real' => 'حقیقی',
							'legal' => 'حقوقی'
						];
						$selectedValueTitle = $valuesArray[$clueMarketingItem->legal_status];
					@endphp
					{{ $selectedValueTitle }}
				  </td>
				  <td>
					@php
						
						$valuesArray = [
							'bonakdar' => 'بنکدار',
							'khordeh_foroosh' => 'خرده فروش',
							'masraf_konandeh' => 'مصرف کننده'
						];
						$selectedValueTitle = $valuesArray[$clueMarketingItem->activity_level];
					@endphp
					{{ $selectedValueTitle }}
				  </td>
				  <td>
					{{$clueMarketingItem->introduce_method}}
				  </td>
				</tr>
				@endforeach
				
			  </tbody>
			</table> 
		
		
			
		
		
			
			
		@endif
		
		
		
		
		
		
		
		@if($clue->clue_closed==0)
		
	
			<form action="{{ route('clues.updateClueMarketingList') }}" method="POST" class="mt-5">
				@csrf
				<input type="hidden" name="clue_id" value="{{$clue->id}}">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-3">
					
						
								<div class="form-group">
									<strong>صنف:</strong>
									<select  name='job_class_id' id='job_class_id' class='form-control pull-right'>
										<option value="" >انتخاب کنید</option>
										@foreach($jobClasses as $jobClass)
											<option @if(old('job_class_id')==$jobClass->id) selected @endif value="{{$jobClass->id}}" >{{$jobClass->job_class_title}}</option>
										@endforeach
									</select>

								</div>
								
							
					
					</div>
					
					<div class="col-xs-12 col-sm-12 col-md-3">
					
						
								<div class="form-group">
									<strong>شخص:</strong>
									<select  name='legal_status' id='legal_status' class='form-control pull-right'>
										@foreach($legalStatuses as $key=>$legalStatus)
											<option @if(old('legal_status')==$key) selected @endif value="{{$key}}" >{{$legalStatus}}</option>
										@endforeach
									</select>

								</div>
								
							
					
					</div>
					
					
					<div class="col-xs-12 col-sm-12 col-md-3">
					
						
								<div class="form-group">
									<strong>سطح فعالیت:</strong>
									<select  name='activity_level' id='activity_level' class='form-control pull-right'>
										<option value="" >انتخاب کنید</option>
										@foreach($activityLevels as $key=>$activityLevelItem)
											<option @if(old('activity_level')==$key) selected @endif value="{{$key}}" >{{$activityLevelItem}}</option>
										@endforeach
									</select>

								</div>
								
							
					
					</div>
					
					
					
				
					
					<div class="col-xs-12 col-sm-12 col-md-3">
					
						
								<div class="form-group">
									<strong>نحوه آشنایی:</strong>

									<input type="text" name="introduce_method" value="{{old('introduce_method')}}" class="form-control" placeholder="">
								</div>
								
							
					
					</div>
					
					
					
					<div class="col-xs-12 col-sm-12 col-md-3 text-right" style="padding-top:15px">
						<button type="submit" class="btn btn-primary">اضافه کردن آیتم فهرست بازاریابی</button>

					</div>
				</div>
			</form>
			
			
			
		
	
			
		@elseif($clue->clue_closed==1)
			<div class="row">
				<div class="col-lg-12 text-center">
					<p class="card-text">نتیجه این سرنخ به بسته تغییر یافته است. امکان بروزرسانی این آیتم وجود ندارد.</p>
				</div>
			</div>
				
			
		@endif
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
</div>
</div>

</div>

</div>


<!-- END CUSTOMER INFORMATION -->





















<!-- BEGIN BUSINESS INFORMATION -->


<div class="row mt-3 mb-3">

<div class="col-lg-12">


<div class="card border-success">
<div class="card-header">مرحله اعتبارسنجی(کسب و کار مرتبط با سرنخ)</div>
<div class="card-body">
					
		@if(!empty($clue->business_id))
		<div class="row">
			<div class="col-lg-12">
				<a target="_blank" class="" href="{{ route('businesses.showCartable',['business_id'=>$business->id]) }}">
					{{$business->business_name}}({{ $business->first_name.' '.$business->last_name }})
				</a>
			</div>
		</div>
		@endif
		
		
		
		@if(($clue->clue_status=='validation' || $clue->clue_status=='clue' ) && $clue->clue_closed==0)
		
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-6">
				
					<form id="updateBusinessStatusForm" action="{{ route('clues.updateClueBusinessStatus') }}" method="POST">
							@csrf
							<input type="hidden" name="clue_id" value="{{$clue->id}}">
							<div class="form-group">

								<select  name='business_id' id='business_id' class='form-control pull-right'>
									<option value="" >انتخاب کنید</option>
									@foreach($businesses as $businessItem)
										<option @if(old('business_id', isset($clue->business_id) ? $clue->business_id : '' )==$businessItem->id) selected @endif value="{{$businessItem->id}}" >{{$businessItem->business_name}}({{$businessItem->first_name}} {{$businessItem->last_name}})</option>
									@endforeach
								</select>

							</div>
							
							
						
					</form>
				
				</div>
				
				<div class="col-xs-12 col-sm-12 col-md-6 text-center" style="padding-top:15px">
					<button type="submit" class="btn btn-primary" form="updateBusinessStatusForm">بروزرسانی کسب و کار مرتبط با سرنخ</button>

				</div>
			</div>
			
		
	
			
		@elseif($clue->clue_closed==1)
			<div class="row">
				<div class="col-lg-12 text-center">
					<p class="card-text">نتیجه این سرنخ به بسته تغییر یافته است. امکان بروزرسانی این آیتم وجود ندارد.</p>
				</div>
			</div>
				
			
		@endif		









		<div class="card-body mt-5">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 text-center">
					<h5>نمونه های ارسال شده</h5>
				</div>
			</div>
			
			
			
			
			
			
			
			
			@if(!empty($instances['validation']) && count($instances['validation'])>0)
			
			 <table class="table table-bordered mb-5">
			  <thead>
				<tr>
				  <th>ردیف</th>
				  <th>ثبت کننده</th>
				  <th>تاریخ ثبت</th>
				  <th class="text-center">فرم ارسال نمونه</th>
				  <th class="text-center">فایل ضمیمه</th>
				</tr>
			  </thead>
			  <tbody>
				@php
					$i=0;
				@endphp
				@foreach($instances['validation'] as $instance)
				<tr>
				  <td>{{ ++$i }}</td>
				  <td>{{ $instance->creator()->first()->name }}</td>           
				  <td>{{ \Morilog\Jalali\Jalalian::forge($instance->created_at)->format('%A, %d %B %Y در ساعت H:i:s') }}</td>
				  
				  <td class="text-center">
					<a class="btn btn-warning" href="{{ route('instances.get-pdf',['invoice_id'=>$instance->id]) }}" target="_blank">دریافت فایل نمونه</a>
				  </td>
				  
				  <td class="text-center">
					@if(!empty($instance->file_path))
						<a class="btn btn-danger" href="{{ asset($instance->file_path) }}">دریافت فایل ضمیمه</a>
					@else
						-
					@endif
				  </td>
				  
				</tr>
				@endforeach
				
			  </tbody>
			</table> 
		
		
			
		@else
		
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 text-center">
				<p>هیچ نمونه ای در این مرحله ارسال نشده است</p>
			</div>
		</div>	
		
		@endif
			
			
			
			
			
			
			
			
			
			
			
			@if($clue->clue_closed==1)
			<div class="row">
				<div class="col-lg-12 text-center">
					<p class="card-text">نتیجه این سرنخ به بسته تغییر یافته است. امکان ارسال نمونه جدید وجود ندارد.</p>
				</div>
			</div>
			@else
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 text-right" style="padding-top:15px">
					<a href="{{route('instances.create',['clue_id'=>$clue->id,'clue_level'=>'validation'])}}" target="_blank">ارسال نمونه</a>

				</div>
			</div>
			@endif
		</div>




		
	
	
</div>
</div>

</div>

</div>


<!-- END BUSINESS INFORMATION -->






























<!-- BEGIN PRE_INVOICE INFORMATION -->


<div class="row mt-3 mb-3">

<div class="col-lg-12">


<div class="card border-success">
<div class="card-header">مرحله ثبت مشتری(پیش فاکتور مرتبط با سرنخ)</div>
<div class="card-body">
					
		@if(!empty($clue->pre_invoice_id))
		<div class="row">
			<div class="col-lg-12">
				
				<a class="btn btn-warning" target="_blank" href="{{ route('invoices.get-pdf',['invoice_id'=>$clue->pre_invoice_id]) }}">دریافت فایل پیش فاکتور</a>
				
			</div>
		</div>
		@endif
		
		
		
		@if((   ($clue->clue_status=='sale' && empty($clue->invoice_id) ) || ($clue->clue_status=='validation' && !empty($clue->business_id)) ) && $clue->clue_closed==0)
		 
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-6">
				
					<form id="updatePreInvoiceStatusForm" action="{{ route('clues.updateCluePreInvoiceStatus') }}" method="POST">
							@csrf
							<input type="hidden" name="clue_id" value="{{$clue->id}}">
							<div class="form-group">

								<select  name='pre_invoice_id' id='pre_invoice_id' class='form-control pull-right'>
									<option value="" >انتخاب کنید</option>
									@foreach($preInvoices as $preInvoiceItem)
										<option @if(old('pre_invoice_id', isset($clue->pre_invoice_id) ? $clue->pre_invoice_id : '' )==$preInvoiceItem->id) selected @endif value="{{$preInvoiceItem->id}}" >شناسه {{$preInvoiceItem->id}}</option>
									@endforeach
								</select>

							</div>
							
							
						
					</form>
				
				</div>
				
				<div class="col-xs-12 col-sm-12 col-md-6 text-center" style="padding-top:15px">
					<button type="submit" class="btn btn-primary" form="updatePreInvoiceStatusForm">بروزرسانی پیش فاکتور مرتبط با سرنخ</button>

				</div>
			</div>
			
		
	
			
		@elseif($clue->clue_closed==1)
			<div class="row">
				<div class="col-lg-12 text-center">
					<p class="card-text">نتیجه این سرنخ به بسته تغییر یافته است. امکان بروزرسانی این آیتم وجود ندارد.</p>
				</div>
			</div>
				
			
		@endif	









		<div class="card-body mt-5">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 text-center">
					<h5>نمونه های ارسال شده</h5>
				</div>
			</div>
			
			
			
			
			
			
			@if(!empty($instances['sale']) && count($instances['sale'])>0)
			
			 <table class="table table-bordered mb-5">
			  <thead>
				<tr>
				  <th>ردیف</th>
				  <th>ثبت کننده</th>
				  <th>تاریخ ثبت</th>
				  <th class="text-center">فرم ارسال نمونه</th>
				  <th class="text-center">فایل ضمیمه</th>
				</tr>
			  </thead>
			  <tbody>
				@php
					$i=0;
				@endphp
				@foreach($instances['sale'] as $instance)
				<tr>
				  <td>{{ ++$i }}</td>
				  <td>{{ $instance->creator()->first()->name }}</td>           
				  <td>{{ \Morilog\Jalali\Jalalian::forge($instance->created_at)->format('%A, %d %B %Y در ساعت H:i:s') }}</td>
				  
				  <td class="text-center">
					<a class="btn btn-warning" href="{{ route('instances.get-pdf',['invoice_id'=>$instance->id]) }}" target="_blank">دریافت فایل نمونه</a>
				  </td>
				  
				  <td class="text-center">
					@if(!empty($instance->file_path))
						<a class="btn btn-danger" href="{{ asset($instance->file_path) }}">دریافت فایل ضمیمه</a>
					@else
						-
					@endif
				  </td>
				  
				</tr>
				@endforeach
				
			  </tbody>
			</table> 
		
		
			
		@else
		
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 text-center">
				<p>هیچ نمونه ای در این مرحله ارسال نشده است</p>
			</div>
		</div>	
		
		@endif
			
			
			
			
			
			@if($clue->clue_closed==1)
			<div class="row">
				<div class="col-lg-12 text-center">
					<p class="card-text">نتیجه این سرنخ به بسته تغییر یافته است. امکان ارسال نمونه جدید وجود ندارد.</p>
				</div>
			</div>
			@else
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 text-right" style="padding-top:15px">
					<a href="{{route('instances.create',['clue_id'=>$clue->id,'clue_level'=>'sale'])}}" target="_blank">ارسال نمونه</a>

				</div>
			</div>
			@endif
		</div>



		
	
	
</div>
</div>

</div>

</div>


<!-- END PRE_INVOICE INFORMATION -->




<!-- BEGIN INVOICE INFORMATION -->


<div class="row mt-3 mb-3">

<div class="col-lg-12">


<div class="card border-success">
<div class="card-header">مرحله ثبت مشتری(فاکتور مرتبط با سرنخ)</div>
<div class="card-body">
					
		@if(!empty($clue->invoice_id))
		<div class="row">
			<div class="col-lg-12">
				
				<a class="btn btn-warning" target="_blank" href="{{ route('invoices.get-pdf',['invoice_id'=>$clue->invoice_id]) }}">دریافت فایل فاکتور</a>
				
			</div>
		</div>
		@endif
		
		
		
		@if((   ($clue->clue_status=='sale' && !empty($clue->pre_invoice_id) ) || ($clue->clue_status=='return_from_sale' && !empty($clue->invoice_id)) ) && $clue->clue_closed==0)
		
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-6">
				
					<form id="updateInvoiceStatusForm" action="{{ route('clues.updateClueInvoiceStatus') }}" method="POST">
							@csrf
							<input type="hidden" name="clue_id" value="{{$clue->id}}">
							<div class="form-group">

								<select  name='invoice_id' id='invoice_id' class='form-control pull-right'>
									<option value="" >انتخاب کنید</option>
									@foreach($invoices as $invoiceItem)
										<option @if(old('invoice_id', isset($clue->invoice_id) ? $clue->invoice_id : '' )==$invoiceItem->id) selected @endif value="{{$invoiceItem->id}}" >شناسه {{$invoiceItem->id}}</option>
									@endforeach
								</select>

							</div>
							
							
						
					</form>
				
				</div>
				
				<div class="col-xs-12 col-sm-12 col-md-6 text-center" style="padding-top:15px">
					<button type="submit" class="btn btn-primary" form="updateInvoiceStatusForm">بروزرسانی فاکتور مرتبط با سرنخ</button>

				</div>
			</div>
			
		
	
			
		@elseif($clue->clue_closed==1)
			<div class="row">
				<div class="col-lg-12 text-center">
					<p class="card-text">نتیجه این سرنخ به بسته تغییر یافته است. امکان بروزرسانی این آیتم وجود ندارد.</p>
				</div>
			</div>
				
			
		@endif	



		<div class="card-body mt-5">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 text-center">
					<h5>نمونه های ارسال شده</h5>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 text-center">
					<p>هیچ نمونه ای در این مرحله ارسال نشده است</p>
				</div>
			</div>
			@if($clue->clue_closed==1)
			<div class="row">
				<div class="col-lg-12 text-center">
					<p class="card-text">نتیجه این سرنخ به بسته تغییر یافته است. امکان ارسال نمونه جدید وجود ندارد.</p>
				</div>
			</div>
			@else
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 text-right" style="padding-top:15px">
					<a href="{{route('instances.create',['clue_id'=>$clue->id,'clue_level'=>'sale'])}}" target="_blank">ارسال نمونه</a>

				</div>
			</div>
			@endif
		</div>
	
	
</div>
</div>

</div>

</div>


<!-- END PRE_INVOICE INFORMATION -->






























<!-- BEGIN CALLS HISTORY -->













<div class="row mt-3 mb-3">

<div class="col-lg-12">


<div class="card border-success">
<div class="card-header">سوابق تماسها</div>
<div class="card-body">
				
				
				
				
				
				
				
				
				
				
@if(count($clueCalls)==0)
	<p class="card-text">هیچ تماسی برای شماره تلفن های مشتری و کسب و کار مرتبط با این سرنخ ثبت نشده است.</p>
@else				
				
<table class="table table-bordered">

<tr>
    <th>تاریخ تماس</th>
    <th>داخلی تماس گیرنده</th>
    <th>نام تماس گیرنده</th>
    <th>مقصد</th>
    <th>مدت زمان</th>
    <th>وضعیت پاسخ</th>

</tr>



@php
$dispositionValues = [
	'NO ANSWER' => ['title'=>'پاسخ داده نشده','class'=>'warning'],
	'BUSY' => ['title'=>'مشغول','class'=>'info'],
	'ANSWERED' => ['title'=>'پاسخ داده شده','class'=>'success'],
	'CONGESTION' => ['title'=>'ترافیک','class'=>'default'],
	'FAILED' => ['title'=>'ناموفق','class'=>'danger']
];
@endphp


@foreach ($clueCalls as $call)

    <tr>

        <td>{{\Morilog\Jalali\Jalalian::forge($call->calldate)->format('%A, %d %B %Y در ساعت H:i:s')}}</td>
        <td>{{$call->src}}</td>
        <td>
        @php
        $crmUser = \App\Models\User::where('voip_id',$call->src)->first();
        @endphp
        @if(!empty($crmUser->name))
        {{$crmUser->name}}
        @else
        {{$call->src}}
        @endif
        </td>
        <td>
        
        
        @php
        $crmUser = \App\Models\User::where('voip_id',$call->dst)->first();
        @endphp
        @if(!empty($crmUser->name))
        {{$crmUser->name}}
        @else
        {{$call->dst}}
        @endif
        
        </td>
        <td>{{$call->duration}} ثانیه</td>
        <td><label class="badge badge-{{$dispositionValues[$call->disposition]['class']}}">{{$dispositionValues[$call->disposition]['title']}}</label></td>	

    </tr>

@endforeach

</table>
				
				
@endif			
				
				
				
	
	
</div>
</div>

</div>

</div>





<!-- BEGIN ALARMS -->






<div class="row mt-3 mb-3">

<div class="col-lg-12">


<div class="card border-success">
<div class="card-header">سوابق آلارمها</div>
<div class="card-body">
				
				
				
				
				
				
				
				
				
				
@if(count($clueAlarms)==0)
	<p class="card-text">هیچ آلارمی برای این سرنخ ثبت نشده است</p>
@else				
				
<table class="table table-bordered">

<tr>
	<th>توضیحات</th>
	<td>تاریخ یادآوری</td>
	<td>تاریخ ثبت</td>
</tr>





@foreach ($clueAlarms as $alarm)

	@php
		$rowClass = "";
		if($alarm->is_seen==1){

			$rowClass = 'success';

		}elseif(\Morilog\Jalali\Jalalian::forge($alarm->alarm_at)->isPast()){
			$rowClass = 'danger';
		}
	@endphp



    <tr class="{{$rowClass}}">



	<td>{{$alarm->alarm_description}}</td>


	<td>{{ \Morilog\Jalali\Jalalian::forge($alarm->alarm_at)->format('%A, %d %B %Y در ساعت H:i:s') }}</td>
	
	
	<td>{{ \Morilog\Jalali\Jalalian::forge($alarm->created_at)->format('%A, %d %B %Y در ساعت H:i:s') }}</td>


    </tr>

@endforeach

</table>
				
				
@endif			
				
				
				
	
	
</div>
</div>

</div>

</div>





<!-- END ALARMS -->




<!-- BEGIN CUSTOMER NOTES -->


<div class="row mt-3 mb-3">

<div class="col-lg-12">


<div class="card border-success">
<div class="card-header">یادداشتهای سرنخ</div>
<div class="card-body">
				
	<form action="{{ route('clues.addNewNote') }}" method="POST">
        	@csrf
        	<input type="hidden" name="clue_id" value="{{$clue->id}}">
			<div class="form-group">

				<textarea class="form-control" name="note_text" placeholder="" rows="5">{{old('note_text')}}</textarea>

			</div>
			
			<div class="col-xs-12 col-sm-12 col-md-12 text-center">

				<button type="submit" class="btn btn-primary">ثبت یادداشت</button>

			</div>
		
	</form>
	
	
	
	
	
	
	
	
	
					
	@if(count($clueNotes)==0)
		<p class="card-text text-center">هنوز هیچ یادداشتی برای این سرنخ ثبت نشده است.</p>
	@else				
					
	<table class="table table-bordered mt-5">

	<tr>
		
		<th>ردیف</th>
	
		<th style="width:66%">متن یادداشت</th>
		
		<td style="width:10%">ارجاع توسط</td>
		<td style="width:12%">تاریخ ثبت</td>
		<td style="width:12%">تاریخ آخرین ویرایش</td>
	</tr>




	@php
	$counter = 0;
	@endphp
	@foreach ($clueNotes as $note)


	    <tr>

		
		<td>{{++$counter}}</td>

		<td>
		
		@if($note->creator()->first()->id==auth()->user()->id)
			<div class="form-group">
				<form action="{{route('specialists.updateNote',auth()->user()->id)}}" method="POST">
					@csrf
					<input type="hidden" name="note_id" value="{{$note->id}}">
					<textarea name="cartable_row_note_text" class="form-control">{{$note->note_text}}</textarea>
					<button type="submit" class="btn btn-success btn-small pull-left mt-1">بروزرسانی</button>
				</form>
			</div>
		@else
			{{$note->note_text}}
		@endif
		
		</td>
		

		
		<td>{{ !empty($note->referencedBy()->first())?$note->referencedBy()->first()->name:
				(
					!empty($note->creator()->first())?$note->creator()->first()->name:'-'
				)}}</td>


		<td>{{ \Morilog\Jalali\Jalalian::forge($note->created_at)->format('%Y/%m/%d H:i:s') }}</td>
		
		<td>{{ \Morilog\Jalali\Jalalian::forge($note->updated_at)->format('%Y/%m/%d H:i:s') }}</td>


	    </tr>

	@endforeach

	</table>
	{!! $clueNotes->links() !!}
					
					
	@endif	
	
	
	
	
	
	
	
	
	
	
					
							
	
	
</div>
</div>

</div>

</div>


<!-- END CUSTOMER NOTES -->









<!-- BEGIN MERGE -->


<div class="row mt-3 mb-3">

<div class="col-lg-12">


<div class="card border border-danger">
<div class="card-header bg-danger text-white">ادغام سرنخ‌ها</div>
<div class="card-body">
				
	<form action="{{ route('clues.merge') }}" method="POST">
			<input type="hidden" name="base_clue_id" value="{{$clue->id}}">
        	@csrf
			<div class="form-group">

				<input type="text" name="to_merge_clue_id" value="{{old('to_merge_clue_id')}}" class="form-control" placeholder="شناسه سرنخ دوم(این سرنخ حذف خواهد شد و یادداشت ها و آلارم های آن به سرنخ فعلی منتقل خواهد شد)">

				
			</div>
			
			<div class="col-xs-12 col-sm-12 col-md-12 text-center">
				@can('merge-clues')
				<button type="submit" class="btn btn-danger" onclick="return confirm('آیا مطمئنید؟?');">ادغام سرنخ‌ها</button>
				@else
				<button type="button" disabled class="btn btn-danger">ادغام سرنخ‌ها</button>
				@endcan

			</div>
			
			
			
			<blockquote class="blockquote text-center mt-5">
			  <p class="mb-0">لطفا در ادغام سرنخ‌ها بسیار دقت کنید. سرنخ ادغام شده به صورت کامل حذف شده و قابل برگشت نیست.</p>
			  <footer class="blockquote-footer">این قابلیت فقط توسط کاربر ادمین قابل انجام است.</footer>
			</blockquote>
		
	</form>
	
	
					
							
	
	
</div>
</div>

</div>

</div>


<!-- END MERGE -->


















@endsection



@section('additional_js_bottom')

<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>


<script>

$(document).ready(function() {
    $('.js-basic-multiple').select2();
});


</script>

@endsection