@extends('panel.layout.master')
@section('main_content')
    <div class="row mb-md-5">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2>تاریخچه ارسالها</h2>

            </div>

            <div class="pull-left">

                <a class="btn btn-success" href="{{ route('sms.showSendForm') }}"> ارسال SMS</a>

            </div>

        </div>

    </div>

    @if ($message = Session::get('danger'))

        <div class="alert alert-danger">

            <p>{{ $message }}</p>

        </div>

    @endif

    @if ($message = Session::get('success'))

        <div class="alert alert-success">

            <p>{{ $message }}</p>

        </div>

    @endif


    <table class="table table-bordered">

        <tr>

            <th>ردیف</th>

            <th>متن پیام</th>
            <th>ارسال کننده</th>
            <th>زمان ارسال</th>
        </tr>

        @foreach ($data as $key => $sms)

            <tr>

                <td>{{ ++$i }}</td>

                <td>{{ substr($sms->sms_text,0,70)."..." }}</td>

                
                <td>{{ $sms->creator()->first()->name }}</td>
                
                <td>{{ \Morilog\Jalali\Jalalian::forge($sms->sent_time)->format('%A, %d %B %Y در ساعت H:i:s') }}</td>
                
                

            </tr>

        @endforeach

    </table>


    {!! $data->render() !!}



@endsection
