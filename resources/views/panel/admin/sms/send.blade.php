@extends('panel.layout.master')

@section('additional_css')


<link href="{{asset('select2/css/select2.min.css')}}" rel="stylesheet" />
    

<style>

.select2-container--default .select2-selection--multiple .select2-selection__choice {
  color: black;
}

</style>
    
@stop

@section('main_content')

    <div class="row mb-md-5">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2>ارسال SMS گروهی</h2>

            </div>

            <div class="pull-left">

                <a class="btn btn-primary" href="{{ route('sms.showSendForm') }}"> بازگشت</a>

            </div>

        </div>

    </div>


    @if (count($errors) > 0)

        <div class="alert alert-danger">

            <strong>خطا!</strong> در مقادیر ورودی ایراداتی وجود دارد.<br><br>

            <ul>

                @foreach ($errors->all() as $error)

                    <li>{{ $error }}</li>

                @endforeach

            </ul>

        </div>

    @endif
    
    
    
    
    
    <div class="row">    
    <div class="col-lg-12 mb-md-5">
        <form action="{{route('sms.showSendForm')}}" method="GET">
        <div class="row">
	    <div class="col-xs-12 col-sm-12 col-md-2 form-input-container">

		<div class="form-group">

		    <strong>استان:</strong>

		    <select  name='state_id' id='state_id' class='form-control pull-right' onChange="change_list('state_id','city_id','loading_gif_cities','{{route('getCities')}}',['region_id'])"> 
			<option {{(!old('state_id', isset(request()->state_id) ? request()->state_id : '')? 'selected' : '')}} value="">انتخاب کنید</option>
			@foreach($states as $state)
			    <option @if(old('state_id', isset(request()->state_id) ? request()->state_id : '')==$state->id) selected @endif value="{{$state->id}}" >{{$state->unit_name}}</option>
			@endforeach
		    </select>

		</div>

	    </div>
	
	
	    <div class="col-xs-12 col-sm-12 col-md-2 form-input-container">

			<div class="form-group">

			    <strong>شهر:</strong>

			    <select  name='city_id' id='city_id' class='form-control pull-right'>
				@if($cities)
				    <option {{(!old('city_id', isset(request()->city_id) ? request()->city_id : '')? 'selected' : '')}} value="">انتخاب کنید</option>
				    @foreach($cities as $city)
					<option @if(old('city_id', isset(request()->city_id) ? request()->city_id : '')==$city->id) selected @endif value="{{$city->id}}" >{{$city->unit_name}}</option>
				    @endforeach
				@endif
			    </select>

			</div>

	    </div>
	
	
	    <div class="col-xs-12 col-sm-12 col-md-2 form-input-container">

                <div class="form-group">

                    <strong>درجه بندی مشتری:</strong>

                    <select  name='customer_degree' id='customer_degree' class='form-control pull-right'>
                    	 <option {{(!old('customer_degree', isset(request()->customer_degree) ? request()->customer_degree : '')? 'selected' : '')}} value="">انتخاب کنید</option>
                        @foreach($enums['customer_degree'] as $key=>$customerDegree)
                            <option @if(old('customer_degree', isset(request()->customer_degree) ? request()->customer_degree : '')==$key) selected @endif value="{{$key}}" >{{$customerDegree}}</option>
                        @endforeach
                    </select>

                </div>

            </div>
        
        
            <div class="col-xs-12 col-sm-12 col-md-2 form-input-container">

                <div class="form-group">

                    <strong>رده شغلی:</strong>

                    <select  name='business_type_id' id='business_type_id' class='form-control pull-right'>
                        <option {{(!old('business_type_id', isset(request()->business_type_id) ? request()->business_type_id : '')? 'selected' : '')}} value="">انتخاب کنید</option>
                        @foreach($businessTypes as $businessType)
                            <option @if(old('business_type_id', isset(request()->business_type_id) ? request()->business_type_id : '')==$businessType->id) selected @endif value="{{$businessType->id}}" >{{$businessType->business_type_title}}</option>
                        @endforeach
                    </select>

                </div>

            </div>
            
            
            <div class="col-xs-12 col-sm-12 col-md-2 form-input-container">

                <div class="form-group">

                    <strong>نوع جذب مشتری:</strong>

                    <select  name='customer_recruitment_type' id='customer_recruitment_type' class='form-control pull-right'>
                    	 <option {{(!old('customer_recruitment_type', isset(request()->customer_recruitment_type) ? request()->customer_recruitment_type : '')? 'selected' : '')}} value="">انتخاب کنید</option>
                        
                        @foreach($enums['customer_recruitment_type'] as $key=>$customerRecruitmentType)
                            <option @if(old('customer_recruitment_type', isset(request()->customer_recruitment_type) ? request()->customer_recruitment_type : '')==$key) selected @endif value="{{$key}}" >{{$customerRecruitmentType}}</option>
                        @endforeach 
                    </select>

                </div>

            </div>
            
            
            

	
	
        </div>
        
        <div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 form-input-container">
			<div class="pull-left">
			<button type="submit" class="btn btn-success" href="{{ route('businesses.index') }}">فیلتر</button>
			</div>
		</div>
        </div>
        </form>
    </div>
</div>


	<div class="row mb-5">
		<div class="col-xs-12 col-sm-12 col-md-10 form-input-container">

				<div class="form-group">

					<strong>لیست شماره موبایل:</strong>

					<textarea class="form-control" style="height:100px" id="extra_mobile_numbers_list" placeholder="هر شماره موبایل در یک سطر" required>{{ old('extra_mobile_numbers_list') }}</textarea>

				</div>

		</div>
		
		<div class="col-xs-12 col-sm-12 col-md-2 form-input-container">

				<div class="form-group">

					<strong>&nbsp;</strong>


					<button type="button" class="btn btn-default" onclick="addExtraMobileNumbersToList()">افزودن به لیست</button>

				</div>

		</div>
	</div>

    {!! Form::open(array('route' => 'sms.sendSMS','method'=>'POST')) !!}

    <div class="row mb-5">
    
    	
    	<div class="col-xs-12 col-sm-12 col-md-12 form-input-container">

		<div class="form-group">

		    <strong>مخاطبین:</strong>

		
		    
		    
		    <select required  class="js-example-basic-multiple" name="contacts[]" id="contacts" multiple="multiple">
		    
			    @foreach($contactsToSendSMS as $key=>$contact)
			    

			    
				    <option selected value="{{$contact}}" >{{$contact}}</option>
				@endforeach

			</select>

		</div>

        </div>
    
    
	<div class="col-xs-12 col-sm-12 col-md-12 form-input-container">

        	<div class="form-group">

            	<strong>متن SMS:</strong>

            	<textarea class="form-control" style="height:100px" name="sms_text" placeholder="" required>{{ old('sms_text') }}</textarea>

        	</div>

	</div>

        <div class="col-xs-12 col-sm-12 col-md-12 text-center">

            <button type="submit" class="btn btn-primary">ارسال</button>

        </div>

    </div>

    {!! Form::close() !!}
























@endsection





@section('additional_js_bottom')

<script src = "{{asset('select2/js/select2.min.js')}}"></script>


<script type="text/javascript">
	/* global $ */
	$(document).ready(function() {
		
		$(document).ready(function() {
		    $('.js-example-basic-multiple').select2();
		});


	});


	
	
</script>


<script>
function change_list(master_el_id,child_el_id,loading_gif_id,ajax_req_url,depend_element_ids = []){

    var e = document.getElementById(master_el_id);
    r_id = e.options[e.selectedIndex].value;

    if(r_id==""){
        $('#'+child_el_id).html('');
        return;
    }

    $('#'+master_el_id).prop('disabled', true);
    //$('#'+loading_gif_id).css('display', 'block'); //TODO: enable loadings

    token= $('body').find("input[name='_token']").val();

    $.ajax({
        url: ajax_req_url,
        type: 'GET',
        data: {"r_id":r_id, "_token":token },
        success: function(data){

            $('#'+child_el_id).html('');
            html_str='<option value="" >انتخاب کنید</option>';
            $('#'+child_el_id).append(html_str);
            $.each(data, function( index, value ) {
                html_str='<option value="'+value.id+'" >'+value.title+'</option>';
                $('#'+child_el_id).append(html_str);
            });

            depend_element_ids.forEach(function(){
                $('#'+depend_element_ids).empty();
            });

            $('#'+master_el_id).prop('disabled', false);
            //$('#'+loading_gif_id).css('display', 'none');
        },
        error: function(data){
            alert(data.responseText);
            alert('دریافت اطلاعات با خطا مواجه شد!');
            $('#'+master_el_id).prop('disabled', false);
            //$('#'+loading_gif_id).css('display', 'none');
        }
    });

}


function addExtraMobileNumbersToList(){
	
	
	var phoneNumberLines = $('#extra_mobile_numbers_list').val().split('\n');
	var regex = new RegExp("^(\\+98|0)?9\\d{9}$");
	
	for(var i = 0;i < phoneNumberLines.length;i++){
		phoneToAdd = phoneNumberLines[i];
		
		if(!regex.test(phoneToAdd)){
			alert("شماره "+phoneToAdd+" نامعتبر می باشد.");
		}else{
			// Create the DOM option that is pre-selected by default
			var newOptions = new Option(phoneToAdd, phoneToAdd, true, true);
			// Append it to the select
			$("#contacts").append(newOptions).trigger('change');
		}
		
		
		
		
		
	}


	

	
}



</script>


@endsection
