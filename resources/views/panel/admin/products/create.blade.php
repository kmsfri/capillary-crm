@extends('panel.layout.master')
@section('main_content')

    <div class="row">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2>افزودن محصول جدید</h2>

            </div>

            <div class="pull-left">

                <a class="btn btn-primary" href="{{ route('products.index') }}"> بازگشت</a>

            </div>

        </div>

    </div>


    @if ($errors->any())

        <div class="alert alert-danger">

            <strong>خطا!</strong> در مقادیر ورودی ایراداتی وجود دارد.<br><br>

            <ul>

                @foreach ($errors->all() as $error)

                    <li>{{ $error }}</li>

                @endforeach

            </ul>

        </div>

    @endif


    <form action="{{ route('products.store') }}" method="POST">

        @csrf


        <div class="row">
        
        
        
		<div class="col-xs-12 col-sm-12 col-md-6">

		    <div class="form-group">

			<strong>نام محصول:</strong>

			{!! Form::text('product_name', old('product_name'), array('placeholder' => '','class' => 'form-control')) !!}

		    </div>

		</div>
		
		
		<div class="col-xs-12 col-sm-12 col-md-6">

		    <div class="form-group">

			<strong>کد محصول:</strong>

			{!! Form::text('product_code', old('product_code'), array('placeholder' => '','class' => 'form-control')) !!}

		    </div>

		</div>
		
		
		<div class="col-xs-12 col-sm-12 col-md-6">

		    <div class="form-group">

			<strong>تولید کننده:</strong>

			{!! Form::text('manufacturer', old('manufacturer'), array('placeholder' => '','class' => 'form-control')) !!}

		    </div>

		</div>
		
		<div class="col-xs-12 col-sm-12 col-md-6">

		    <div class="form-group">

			<strong>مدل محصول:</strong>

			{!! Form::text('product_model', old('product_model'), array('placeholder' => '','class' => 'form-control')) !!}

		    </div>

		</div>
		
		
		
		<div class="col-xs-12 col-sm-12 col-md-4 form-input-container mb-md-5">

			<div class="form-group">

			    <strong>موجودی(تعداد):</strong>

			    <input type="number" name="stock_count" id="stock_count" value="{{old('stock_count')}}" class="form-control" placeholder="" onKeyPress="return NumbersOnly(event, 'stock_count')">

			</div>

		</div>
		
		
		
		<div class="col-xs-12 col-sm-12 col-md-4 form-input-container mb-md-5">

			<div class="form-group">

			    <strong>قیمت(تومان):</strong>

			    <input type="number" name="price" id="price" value="{{old('price')}}" class="form-control" placeholder="" onKeyPress="return NumbersOnly(event, 'price')">

			</div>

		</div>
		
		
		
		<div class="col-xs-12 col-sm-12 col-md-4">

			<div class="form-group">

			<strong>اولویت فروش:</strong>

				<select  name='has_priority' id='has_priority' class='form-control pull-right' >
					<option @if(old('has_priority')==0) selected @endif value="0" >ندارد</option>
					<option @if(old('has_priority')==1) selected @endif value="1" >دارد</option>
				</select>

			</div>

		</div>
		
		
		
		
		<div class="col-xs-12 col-sm-12 col-md-6 mt-md-2">

			<div class="form-group">

				<strong>توضیحات:</strong>

				<textarea class="form-control" name="descriptions" placeholder="" >{{old('descriptions')}}</textarea>

			</div>

		</div>
		
		
		
		<div class="col-xs-12 col-sm-12 col-md-6 mt-md-2">

			<div class="form-group">

				<strong>جزءیات محصول:</strong>

				<textarea class="form-control" name="details" placeholder="" >{{old('details')}}</textarea>

			</div>

		</div>
		
        
        

            <div class="col-xs-12 col-sm-12 col-md-12 text-center">

                <button type="submit" class="btn btn-primary">ثبت</button>

            </div>

        </div>


    </form>



@endsection


@section('additional_js_bottom')
<script>
    function NumbersOnly(evt, label) {
        var status = document.getElementById(label);
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode == 46) {
            return true;
        }
        if (charCode < 48 || charCode > 57) {
            status.innerHTML = 'Numbers Only Please';
            return false;
        }
        status.innerHTML = '';
        return true;
    }
</script>
@endsection
