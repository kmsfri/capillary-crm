@extends('panel.layout.master')
@section('main_content')


    <div class="row">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2>مدیریت محصولات</h2>

            </div>

            <div class="pull-left">

                @can('product-create')

                    <a class="btn btn-success" href="{{ route('products.create') }}"> ایجاد محصول جدید</a>

                @endcan

            </div>

        </div>

    </div>


    @if ($message = Session::get('success'))

        <div class="alert alert-success">

            <p>{{ $message }}</p>

        </div>

    @endif


    <table class="table table-bordered">

        <tr>

            <th>ردیف</th>

            <th>نام</th>

            <th>کد</th>
            
            <th>مدل</th>
            
            <th>موجودی</th>
            
            <th>اولویت فروش</th>
            

            <th width="280px">عملیات</th>

        </tr>

        @foreach ($products as $product)

            <tr>

                <td>{{ ++$i }}</td>

                <td>{{ $product->product_name }}</td>

                <td>{{ $product->product_code }}</td>
                
                <td>{{ $product->product_model }}</td>
                
                <td>{{ $product->stock_count }}</td>
                
                <td>{{ ($product->has_priority==1)?'دارد':'ندارد' }}</td>

                <td>

                    <form action="{{ route('products.destroy',$product->id) }}" method="POST">

                        <a class="btn btn-info" href="{{ route('products.show',$product->id) }}">نمایش</a>

                        @can('product-edit')

                            <a class="btn btn-primary" href="{{ route('products.edit',$product->id) }}">ویرایش</a>

                        @endcan


                        @csrf

                        @method('DELETE')

                        @can('product-delete')

                            <button type="submit" class="btn btn-danger">حذف</button>

                        @endcan

                    </form>

                </td>

            </tr>

        @endforeach

    </table>


    {!! $products->links() !!}



@endsection
