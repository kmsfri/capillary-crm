@extends('panel.layout.master')
@section('main_content')

    <div class="row">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2>نمایش محصول</h2>

            </div>

            <div class="pull-left">

                <a class="btn btn-primary" href="{{ route('products.index') }}"> بازگشت</a>

            </div>

        </div>

    </div>


    <div class="row">

        <div class="col-xs-12 col-sm-12 col-md-6">

		    <div class="form-group">

			<strong>نام محصول:</strong>

			{{$product->product_name}}

		    </div>

		</div>
		
		
		<div class="col-xs-12 col-sm-12 col-md-6">

		    <div class="form-group">

			<strong>کد محصول:</strong>

			{{$product->product_code}}

		    </div>

		</div>
		
		
		<div class="col-xs-12 col-sm-12 col-md-6">

		    <div class="form-group">

			<strong>تولید کننده:</strong>

			{{$product->manufacturer}}

		    </div>

		</div>
		
		<div class="col-xs-12 col-sm-12 col-md-6">

		    <div class="form-group">

			<strong>مدل محصول:</strong>

			{{$product->product_model}}

		    </div>

		</div>
		
		
		
		<div class="col-xs-12 col-sm-12 col-md-4 form-input-container mb-md-5">

			<div class="form-group">

			    <strong>موجودی(تعداد):</strong>

			    {{$product->stock_count}}
			</div>

		</div>
		
		
		
		<div class="col-xs-12 col-sm-12 col-md-4 form-input-container mb-md-5">

			<div class="form-group">

			    <strong>قیمت(تومان):</strong>

			    {{$product->price}}

			</div>

		</div>
		
		
		
		<div class="col-xs-12 col-sm-12 col-md-4">

			<div class="form-group">

			<strong>اولویت فروش:</strong>

				{{($product->has_priority==1)?'دارد':'ندارد'}}

			</div>

		</div>
		
		
		
		
		<div class="col-xs-12 col-sm-12 col-md-6 mt-md-2">

			<div class="form-group">

				<strong>توضیحات:</strong>

				{!! $product->descriptions !!}

			</div>

		</div>
		
		
		
		<div class="col-xs-12 col-sm-12 col-md-6 mt-md-2">

			<div class="form-group">

				<strong>جزءیات محصول:</strong>

				{!! $product->details !!}


			</div>

		</div>

    </div>

@endsection

