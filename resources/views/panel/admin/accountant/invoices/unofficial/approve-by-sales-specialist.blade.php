@extends('panel.layout.master')
@section('main_content')

    <div class="row mb-md-5">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2>تایید پیش فاکتور غیررسمی با شناسه: 
                    {{$invoice->id}}
                </h2>

            </div>

            <div class="pull-left">

                <a class="btn btn-primary" href="{{ route('invoices.unofficial.index') }}"> بازگشت</a>

            </div>

        </div>

    </div>


    @if (count($errors) > 0)

        <div class="alert alert-danger">

            <strong>خطا!</strong> در مقادیر ورودی ایراداتی وجود دارد.<br><br>

            <ul>

                @foreach ($errors->all() as $error)

                    <li>{{ $error }}</li>

                @endforeach

            </ul>

        </div>

    @endif
    
    <div class="row mb-5">
    	<div class="col-xs-12 col-sm-12 col-md-12">
    		<a href="{{route('invoices.unofficial.edit',$invoice->id)}}" target="_blank">مشاهده اطلاعات پیش فاکتور</a>
    		</br></br>
    		<a href="{{route('invoices.unofficial.get-pdf',['invoice_id'=>$invoice->id])}}" target="_blank">دریافت pdf پیش فاکتور</a>
    		
    		
    	</div>
    </div>


    {!! Form::model($invoice, ['method' => 'POST','route' => ['invoices.unofficial.sales-specialist-approving-invoice', $invoice->id]]) !!}


	<input type="hidden" name="invoice_id" value="{{$invoice->id}}">

    <div class="row">

	<div class="col-xs-12 col-sm-12 col-md-12 form-input-container">

		<div class="form-group">

		    <strong>توضیحات:</strong>

		    <textarea class="form-control" style="height:200px" name="sales_specialist_approving_description" placeholder="" required>{{ old('sales_specialist_approving_description',$invoice->sales_specialist_approving_description) }}</textarea>

		</div>

	</div>



        <div class="col-xs-12 col-sm-12 col-md-4">

            <div class="form-group">

                <strong>وضعیت:</strong>

                <select  name='approved_by_sales_specialist' id='approved_by_sales_specialist' class='form-control pull-right' >
                    <option @if(old('approved_by_sales_specialist', isset($invoice->approved_by_sales_specialist) ? $invoice->approved_by_sales_specialist : '')==1) selected @endif value="1" >تایید</option>
                    <option @if(old('approved_by_sales_specialist', isset($invoice->approved_by_sales_specialist) ? $invoice->approved_by_sales_specialist : '')==2) selected @endif value="2" >عدم تایید</option>
                </select>

            </div>

        </div>


        <div class="col-xs-12 col-sm-12 col-md-12 text-center">

            <button type="submit" class="btn btn-primary">ثبت</button>

        </div>

    </div>

    {!! Form::close() !!}



@endsection
