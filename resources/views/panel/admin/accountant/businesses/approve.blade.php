@extends('panel.layout.master')
@section('main_content')

    <div class="row mb-md-5">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2>تایید اطلاعات کسب و کار: 
                    {{$business->business_name}}
                </h2>

            </div>

            <div class="pull-left">

                <a class="btn btn-primary" href="{{ route('businesses.index') }}"> بازگشت</a>

            </div>

        </div>

    </div>


    @if (count($errors) > 0)

        <div class="alert alert-danger">

            <strong>خطا!</strong> در مقادیر ورودی ایراداتی وجود دارد.<br><br>

            <ul>

                @foreach ($errors->all() as $error)

                    <li>{{ $error }}</li>

                @endforeach

            </ul>

        </div>

    @endif
	
	@if ($message = Session::get('danger'))

        <div class="alert alert-danger">

            <p>{{ $message }}</p>

        </div>

    @endif
    
    <div class="row mb-5">
    	<div class="col-xs-12 col-sm-12 col-md-12">
    		<a href="{{route('businesses.edit',$business->id)}}" target="_blank">مشاهده اطلاعات کسب و کار</a>
    	</div>
    </div>


    {!! Form::model($business, ['method' => 'POST','route' => ['business.accounting-approving-business', $business->id]]) !!}


	<input type="hidden" name="business_id" value="{{$business->id}}">

    <div class="row">

	<div class="col-xs-12 col-sm-12 col-md-12 form-input-container">

		<div class="form-group">

		    <strong>توضیحات:</strong>

		    <textarea class="form-control" style="height:200px" name="accountant_approving_description" placeholder="" required>{{ old('accountant_approving_description',$business->accountant_approving_description) }}</textarea>

		</div>

	</div>



        <div class="col-xs-12 col-sm-12 col-md-4">

            <div class="form-group">

                <strong>وضعیت:</strong>

                <select  name='approved_by_accountant' id='approved_by_accountant' class='form-control pull-right' >
                    <option @if(old('approved_by_accountant', isset($business->approved_by_accountant) ? $business->approved_by_accountant : '')==1) selected @endif value="1" >تایید</option>
                    <option @if(old('approved_by_accountant', isset($business->approved_by_accountant) ? $business->approved_by_accountant : '')==2) selected @endif value="2" >رد</option>
                </select>

            </div>

        </div>


        <div class="col-xs-12 col-sm-12 col-md-12 text-center">

            <button type="submit" class="btn btn-primary">ثبت</button>

        </div>

    </div>

    {!! Form::close() !!}



@endsection
