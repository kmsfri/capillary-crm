@extends('panel.layout.master')
@section('main_content')

    <div class="row mb-md-5">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2>ویرایش برندهای موجود در {{$business->business_name}}</h2>

            </div>

            <div class="pull-left">

                <a class="btn btn-primary" href="{{ route('businesses.index') }}"> بازگشت</a>

            </div>

        </div>

    </div>


    @if (count($errors) > 0)

        <div class="alert alert-danger">

            <strong>خطا!</strong> در مقادیر ورودی ایراداتی وجود دارد.<br><br>

            <ul>

                @foreach ($errors->all() as $error)

                    <li>{{ $error }}</li>

                @endforeach

            </ul>

        </div>

    @endif
	
	@if ($message = Session::get('success'))

        <div class="alert alert-success">

            <p>{{ $message }}</p>

        </div>

    @endif


    {!! Form::model($business, ['method' => 'PATCH','route' => ['brands.updateBusinessBrands', $business->id]]) !!}

	<input type="hidden" name="business_id" value="{{$business->id}}">

    <div class="row">

       
		@foreach($brands as $key=>$value)

        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                
                    <label>
						{{ Form::checkbox('brand_ids['.$value->id.']', $value->id, in_array($value->id, $businessBrandsIDs) ? true : false, array('class' => 'name')) }}

                        {{ $value->brand_name }}
						
						{!! Form::text('brand_price['.$value->id.']', old('brand_price['.$value->id.']',$businessBrands[$value->id]["price"]??''), array('placeholder' => 'قیمت','class' => 'form-control')) !!}
					
					</label>

                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                

            </div>

        </div>
		@endforeach

        <div class="col-xs-12 col-sm-12 col-md-12 text-center">

            <button type="submit" class="btn btn-primary">ثبت</button>

        </div>

    </div>

    {!! Form::close() !!}


@endsection

