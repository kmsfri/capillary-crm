@extends('panel.layout.master')
@section('main_content')

    <div class="row">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2>افزودن برند جدید</h2>

            </div>

            <div class="pull-left">

                <a class="btn btn-primary" href="{{ route('brands.index') }}"> بازگشت</a>

            </div>

        </div>

    </div>


    @if ($errors->any())

        <div class="alert alert-danger">

            <strong>خطا!</strong> در مقادیر ورودی ایراداتی وجود دارد.<br><br>

            <ul>

                @foreach ($errors->all() as $error)

                    <li>{{ $error }}</li>

                @endforeach

            </ul>

        </div>

    @endif


    <form action="{{ route('brands.store') }}" method="POST">

        @csrf


        <div class="row">
        
        
        
		<div class="col-xs-12 col-sm-12 col-md-6">

		    <div class="form-group">

			<strong>نام برند:</strong>

			{!! Form::text('brand_name', old('brand_name'), array('placeholder' => '','class' => 'form-control')) !!}

		    </div>

		</div>
		
	
		
		<div class="col-xs-12 col-sm-12 col-md-6">

		    <div class="form-group">

			<strong>تولید کننده:</strong>

			{!! Form::text('manufacturer', old('manufacturer'), array('placeholder' => '','class' => 'form-control')) !!}

		    </div>

		</div>
		
		
		
		<div class="col-xs-12 col-sm-12 col-md-6 mt-md-2">

			<div class="form-group">

				<strong>توضیحات:</strong>

				<textarea class="form-control" name="descriptions" placeholder="" >{{old('descriptions')}}</textarea>

			</div>

		</div>
		
		
		
	

		<div class="col-xs-12 col-sm-12 col-md-12 text-center">

			<button type="submit" class="btn btn-primary">ثبت</button>

		</div>

        </div>


    </form>



@endsection


