@extends('panel.layout.master')
@section('main_content')


    <div class="row">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2>مدیریت برندها</h2>

            </div>

            <div class="pull-left">

                @can('brand-create')

                    <a class="btn btn-success" href="{{ route('brands.create') }}"> ایجاد برند جدید</a>

                @endcan

            </div>

        </div>

    </div>


    @if ($message = Session::get('success'))

        <div class="alert alert-success">

            <p>{{ $message }}</p>

        </div>

    @endif


    <table class="table table-bordered">

        <tr>

            <th>ردیف</th>

            <th>نام برند</th>

            <th>تولید کننده</th>
			
			<th class="text-center">تعداد موجود</th>
			
			<th class="text-center">میانگین قیمت</th>
			
			<th class="text-center">کمترین قیمت</th>
			
			<th class="text-center">بیشترین قیمت</th>
          

            <th width="280px">عملیات</th>

        </tr>

        @foreach ($brands as $brand)

            <tr>

                <td>{{ ++$i }}</td>

                <td>{{ $brand->brand_name }}</td>

                <td>{{ $brand->manufacturer }}</td>
				
				
				
				<td class="text-center">{{ $brand->businesses()->count() }}</td>
             
			 
				<td class="text-center">{{ round($brand->businesses()->avg('price')) }}</td>
				
				<td class="text-center">{{ round($brand->businesses()->min('price')) }}</td>
				
				<td class="text-center">{{ round($brand->businesses()->max('price')) }}</td>
			 
                <td>

                    <form action="{{ route('brands.destroy',$brand->id) }}" method="POST">

                        <a class="btn btn-info" href="{{ route('brands.show',$brand->id) }}">نمایش</a>

                        @can('brand-edit')

                            <a class="btn btn-primary" href="{{ route('brands.edit',$brand->id) }}">ویرایش</a>

                        @endcan


                        @csrf

                        @method('DELETE')

                        @can('brand-delete')

                            <button type="submit" class="btn btn-danger">حذف</button>

                        @endcan

                    </form>

                </td>

            </tr>

        @endforeach

    </table>


    {!! $brands->links() !!}



@endsection
