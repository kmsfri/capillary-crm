@extends('panel.layout.master')
@section('main_content')

    <div class="row">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2>نمایش برند</h2>

            </div>

            <div class="pull-left">
 
                <a class="btn btn-primary" href="{{ route('brands.index') }}"> بازگشت</a>

            </div>

        </div>

    </div>


    <div class="row">

        <div class="col-xs-12 col-sm-12 col-md-6">

		    <div class="form-group">

			<strong>نام برند:</strong>

			{{$brand->brand_name}}

		    </div>

		</div>
		
		
		
		
		<div class="col-xs-12 col-sm-12 col-md-6">

		    <div class="form-group">

			<strong>تولید کننده:</strong>

			{{$brand->manufacturer}}

		    </div>

		</div>
		

		
		
				
		
		
		<div class="col-xs-12 col-sm-12 col-md-6 mt-md-2">

			<div class="form-group">

				<strong>توضیحات:</strong>

				{!! $brand->descriptions !!}

			</div>

		</div>
		
		


    </div>

@endsection

