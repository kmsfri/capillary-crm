@extends('panel.layout.master')
@section('additional_css')
    <link rel = "stylesheet" href = "{{asset('css/leaflet.css')}}" />
@stop
@section('main_content')

	
    @if ($message = Session::get('warning'))

    <div class="alert alert-warning">

        <p>{{ $message }}</p>

    </div>

    @endif




    <div class="row mb-5">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2>افزودن کسب و کار جدید</h2>

            </div>

            <div class="pull-left">

                <a class="btn btn-primary" href="{{ route('businesses.index') }}"> بازگشت</a>

            </div>

        </div>

    </div>


    @if ($errors->any())

        <div class="alert alert-danger">

            <strong>خطا!</strong> در مقادیر ورودی ایراداتی وجود دارد.<br><br>

            <ul>

                @foreach ($errors->all() as $error)

                    <li>{{ $error }}</li>

                @endforeach

            </ul>

        </div>

    @endif

 
    <form id="business_store_form" action="{{ route('businesses.store') }}" method="POST" enctype="multipart/form-data" onsubmit="return validateForm()" >

        @csrf

	<input type="hidden" name="customer_id" value="<?php echo !empty($customerData['customer_id'])?$customerData['customer_id']:''; ?>">

        <div class="row">
        
        
            <div class="col-xs-12 col-sm-12 col-md-3 form-input-container">

                <div class="form-group">

                    <strong>نوع مشتری:</strong>

                    <select required  name='customer_legal_type' id='customer_legal_type' class='form-control pull-right'>
                        <!--<option {{(!old('customer_legal_type', isset($business->customer_legal_type) ? $business->customer_legal_type : '')? 'selected' : '')}} value="">انتخاب کنید</option>-->
                        @foreach($enums['customer_legal_type'] as $key=>$legalType)
                            <option @if(old('customer_legal_type', isset($business->customer_legal_type) ? $business->customer_legal_type : '')==$key) selected @endif value="{{$key}}" >{{$legalType}}</option>
                        @endforeach
                    </select>

                </div>

            </div>
            
            
            <div class="col-xs-12 col-sm-12 col-md-3 form-input-container">

                <div class="form-group">

                    <strong>درجه بندی مشتری:</strong>

                    <select required  name='customer_degree' id='customer_degree' class='form-control pull-right'>
                        <!--<option {{(!old('customer_degree', isset($business->customer_degree) ? $business->customer_degree : '')? 'selected' : '')}} value="">انتخاب کنید</option>-->
                        @foreach($enums['customer_degree'] as $key=>$customerDegree)
                            <option @if(old('customer_degree', isset($business->customer_degree) ? $business->customer_degree : '')==$key) selected @endif value="{{$key}}" >{{$customerDegree}}</option>
                        @endforeach
                    </select>

                </div>

            </div>
        
        
            <div class="col-xs-12 col-sm-12 col-md-3 form-input-container">

                <div class="form-group">

                    <strong>رده شغلی:</strong>

                    <select required  name='business_type_id' id='business_type_id' class='form-control pull-right'>
                        <!--<option {{(!old('business_type_id', isset($business->business_type_id) ? $business->business_type_id : '')? 'selected' : '')}} value="">انتخاب کنید</option>-->
                        @foreach($businessTypes as $businessType)
                            <option @if(old('business_type_id', isset($business->business_type_id) ? $business->business_type_id : '')==$businessType->id) selected @endif value="{{$businessType->id}}" >{{$businessType->business_type_title}}</option>
                        @endforeach
                    </select>

                </div>

            </div>
            
            
            <div class="col-xs-12 col-sm-12 col-md-3 form-input-container">

                <div class="form-group">

                    <strong>نوع جذب مشتری:</strong>

                    <select required  name='customer_recruitment_type' id='customer_recruitment_type' class='form-control pull-right'>
                        <!--<option {{(!old('customer_recruitment_type', isset($business->customer_recruitment_type) ? $business->customer_recruitment_type : '')? 'selected' : '')}} value="">انتخاب کنید</option>-->
                        @foreach($enums['customer_recruitment_type'] as $key=>$customerRecruitmentType)
                            <option @if(old('customer_recruitment_type', isset($business->customer_recruitment_type) ? $business->customer_recruitment_type : '')==$key) selected @endif value="{{$key}}" >{{$customerRecruitmentType}}</option>
                        @endforeach 
                    </select>

                </div>

            </div>
        
        

            <div class="col-xs-12 col-sm-12 col-md-3 form-input-container"> 

                <div class="form-group">

                    <strong>نام(*):</strong>

                    <input type="text" name="first_name" value="{{old('first_name',    (!empty($customerData['first_name'])?$customerData['first_name']:'')    )}}" class="form-control" placeholder="" required>

                </div>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-3 form-input-container">

                <div class="form-group">

                    <strong>نام خانوادگی(*):</strong>

                    <input type="text" name="last_name" value="{{old('last_name', (!empty($customerData['last_name'])?$customerData['last_name']:'')      )}}" class="form-control" placeholder="" required>

                </div>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-3 form-input-container">

                <div class="form-group">

                    <strong>نام فروشگاه(*):</strong>

                    <input type="text" name="business_name" value="{{old('business_name',   (!empty($customerData['business_name'])?$customerData['business_name']:'')    )}}" class="form-control" placeholder="" required>

                </div>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-3 form-input-container">

                <div class="form-group">

                    <strong>نام باربری:</strong>

                    <input type="text" name="shipping_name" value="{{old('shipping_name')}}" class="form-control" placeholder="">

                </div>

            </div>
            
            
            <div class="col-xs-12 col-sm-12 col-md-3 form-input-container">

                <div class="form-group">

                    <strong>تاریخ تولد:</strong>

                    <input data-jdp type="text" name="birth_date" value="{{old('birth_date')}}" class="form-control" placeholder="">

                </div>

            </div>


            <div class="col-xs-12 col-sm-12 col-md-3 form-input-container">

                <div class="form-group">

                    <strong>استان(*):</strong>

                    <select required  name='state_id' id='state_id' class='form-control pull-right' onChange="change_list('state_id','city_id','loading_gif_cities','{{route('getCities')}}',['region_id'])"> 				<!-- TODO: init vars -->
                        <option {{(!old('state_id', isset($business->state_id) ? $business->state_id : '')? 'selected' : '')}} value="">انتخاب کنید</option>
                        @foreach($states as $state)
                            <option @if(old('state_id', isset($business->state_id) ? $business->state_id : '')==$state->id) selected @endif value="{{$state->id}}" >{{$state->unit_name}}</option>
                        @endforeach
                    </select>

                </div>

            </div>



            <div class="col-xs-12 col-sm-12 col-md-3 form-input-container">

                <div class="form-group">

                    <strong>شهر(*):</strong>

                    <select required  name='city_id' id='city_id' class='form-control pull-right' onChange="change_list('city_id','region_id','loading_gif_regions','{{route('getCities')}}')">
                        @if($cities)
                            <option {{(!old('city_id', isset($business->city_id) ? $business->city_id : '')? 'selected' : '')}} value="">انتخاب کنید</option>
                            @foreach($cities as $city)
                                <option @if(old('city_id', isset($business->city_id) ? $business->city_id : '')==$city->id) selected @endif value="{{$city->id}}" >{{$city->unit_name}}</option>
                            @endforeach
                        @endif
                    </select>

                </div>

            </div>


            <div class="col-xs-12 col-sm-12 col-md-3 form-input-container">

                <div class="form-group">

                    <strong>منطقه:</strong>

                    <select  name='region_id' id='region_id' class='form-control pull-right'>
                        @if($regions)
                            <option {{(!old('region_id', isset($business->region_id) ? $business->region_id : '')? 'selected' : '')}} value="">انتخاب کنید</option>
                            @foreach($regions as $region)
                                <option @if(old('region_id', isset($business->region_id) ? $business->region_id : '')==$region->id) selected @endif value="{{$region->id}}" >{{$region->unit_name}}</option>
                            @endforeach
                        @endif
                    </select>

                </div>

            </div>


            <div class="col-xs-12 col-sm-12 col-md-12 form-input-container">

                <div class="form-group">

                    <strong>آدرس(*):</strong>

                    <textarea class="form-control" style="height:100px" name="address" placeholder="" required>{{old('address'  ,   (!empty($customerData['address'])?$customerData['address']:'')      )}}</textarea>

                </div>

            </div>


            <div class="col-xs-12 col-sm-12 col-md-3 form-input-container">

                <div class="form-group">

                    <strong>کد ملی شخص/کد ملی شرکت:</strong>

                    <input type="text" name="national_code" value="{{old('national_code')}}" class="form-control" placeholder="">

                </div>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-3 form-input-containerform-input-container">

                <div class="form-group">

                    <strong>کد پستی:</strong>

                    <input type="text" name="postal_code" value="{{old('postal_code')}}" class="form-control" placeholder="">

                </div>

            </div>


            <div class="col-xs-12 col-sm-12 col-md-3 form-input-container">

                <div class="form-group">

                    <strong>تلفن همراه(*):</strong>

                    <input type="text" name="mobiles[]" value="{{old('mobiles',     (!empty($customerData['mobiles'])?$customerData['mobiles']:[''])      )[0]}}" class="form-control" placeholder="" required>

                </div>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-3 form-input-container">

                <div class="form-group">

                    <strong>تلفن همراه ۲(سوشال مدیا):</strong>

                    <input type="text" name="mobiles[]" value="{{old('mobiles',['',''])[1]}}" class="form-control" placeholder="">

                </div>

            </div>




            <div class="col-xs-12 col-sm-12 col-md-3 form-input-container">

                <div class="form-group">

                    <strong>نحوه خرید:</strong>

                    <select  name='buy_method' id='buy_method' class='form-control pull-right'>
                        <option value="" >انتخاب کنید</option>
                        <option @if(old('buy_method', isset($business->buy_method) ? $business->buy_method : '')=='0') selected @endif value="0" >نقد</option>
                        <option @if(old('buy_method', isset($business->buy_method) ? $business->buy_method : '')==1) selected @endif value="1" >چکی</option>
                        <option @if(old('buy_method', isset($business->buy_method) ? $business->buy_method : '')==2) selected @endif value="2" >حساب باز(اعتباری)</option>
                    </select>

                </div>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-3 form-input-container">

                <div class="form-group">

                    <strong>مالکیت مغازه:</strong>

                    <select  name='business_ownership' id='business_ownership' class='form-control pull-right'>
                        <option value="" >انتخاب کنید</option>
                        <option @if(old('business_ownership', isset($business->business_ownership) ? $business->business_ownership : '')=='0') selected @endif value="0" >مالک</option>
                        <option @if(old('business_ownership', isset($business->business_ownership) ? $business->business_ownership : '')==1) selected @endif value="1" >اجاره</option>
                    </select>

                </div>

            </div>
            
            

            
            
            
            <div class="col-xs-12 col-sm-12 col-md-3 form-input-container">

                <div class="form-group">

                    <strong>نام و نام خانوادگی شریک:</strong>

                    <input type="text" name="copartner_name" value="{{old('copartner_name')}}" class="form-control" placeholder="">

                </div>

            </div>
            
            
            <div class="col-xs-12 col-sm-12 col-md-3 form-input-container">

                <div class="form-group">

                    <strong>تلفن شریک:</strong>

                    <input type="text" name="copartner_phone" value="{{old('copartner_phone')}}" class="form-control" placeholder="">

                </div>

            </div>


            <div class="col-xs-12 col-sm-12 col-md-3 form-input-container mb-5">

                <div class="form-group">

                    <strong>تصویر سند یا اجاره نامه:</strong>

                    <input type="file" onchange="readURL(this,'ownership_doc_file_preview_container')" name="ownership_doc_file" id="ownership_doc_file" value="{{ old('ownership_doc_file',isset($business->ownership_doc_file) ? $business->ownership_doc_file : '') }}" autocomplete="off" >

                </div>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-3 form-input-container mb-5">

                <div class="form-group">

                    <div id="ownership_doc_file_preview_container"></div>

                </div>

            </div>



            <div class="col-xs-12 col-sm-12 col-md-3 form-input-container mb-5">

                <div class="form-group">

                    <strong>تصویر کارت ویزیت:</strong>

                    <input type="file" onchange="readURL(this,'visit_card_file_preview_container')" name="visit_card_file" id="visit_card_file" value="{{ old('visit_card_file',isset($business->visit_card_file) ? $business->visit_card_file : '') }}" autocomplete="off" >

                </div>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-3 form-input-container mb-5">

                <div class="form-group">

                    <div id="visit_card_file_preview_container"></div>

                </div>

            </div>



            <div class="col-xs-12 col-sm-12 col-md-6 form-input-container mb-5">

                <div class="form-group">

                    <strong>تصویر پروانه کسب:</strong>

                    <input type="file" onchange="readURL(this,'business_license_file_preview_container')" name="business_license_file" id="business_license_file" value="{{ old('business_license_file',isset($business->business_license_file) ? $business->business_license_file : '') }}" autocomplete="off" >

                </div>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-6 form-input-container mb-5">

                <div class="form-group">
                    <div id="business_license_file_preview_container"></div>

                </div>

            </div>


            <div class="col-xs-12 col-sm-12 col-md-3 form-input-container mb-md-5">

                <div class="form-group">

                    <strong>سابقه فعالیت در مکان فعلی:</strong>

                    <input type="text" name="activity_amount" value="{{old('activity_amount')}}" class="form-control" placeholder="" >

                </div>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-3 form-input-container mb-md-5">

                <div class="form-group">

                    <strong>تعداد پرسنل شاغل:</strong>

                    <input type="text" name="working_personnel" value="{{old('working_personnel')}}" class="form-control" placeholder="" >

                </div>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-3 form-input-container mb-md-5">

                <div class="form-group">

                    <strong>برآورد مقدار جنس دپو در فروشگاه(ریالی):</strong>

                    <input type="number" name="assets_estimate_rial" id="assets_estimate_rial" value="{{old('assets_estimate_rial')}}" class="form-control" placeholder="" onKeyPress="return NumbersOnly(event, 'assets_estimate_rial')">

                </div>

            </div>


            <div class="col-xs-12 col-sm-12 col-md-3 form-input-container mb-md-5">

                <div class="form-group">

                    <strong>برآورد مقدار جنس دپو در فروشگاه(کارتنی):</strong>

                    <input type="number" name="assets_estimate_box" id="assets_estimate_box" value="{{old('assets_estimate_box')}}" class="form-control" placeholder="" onKeyPress="return NumbersOnly(event, 'assets_estimate_box')">

                </div>

            </div>


            <div class="col-xs-12 col-sm-12 col-md-12 mb-md-4">
                <h4>آمار کسبه و همسایگان</h4>
            </div>

            <span id="neighbors_container">

            <?php
                $neighborsSampleElements = '
                    <div class="col-xs-12 col-sm-12 col-md-4 mt-md-2">
                        <div class="form-group">
                            <strong>نام و نام خانوادگی:</strong>
                            <input type="text" name="neighbors[{index_placeholder}][name]" class="form-control" placeholder="" value="{name_placeholder}" >
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-4 mt-md-2">
                        <div class="form-group">
                            <strong>تلفن:</strong>
                            <input type="text" name="neighbors[{index_placeholder}][phone]" class="form-control" placeholder="" value="{phone_placeholder}" >
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-4 mt-md-2">
                        <div class="form-group">
                            <strong>آدرس:</strong>
                            <input type="text" name="neighbors[{index_placeholder}][address]" class="form-control" placeholder="" value="{address_placeholder}" >
                        </div>
                    </div>
                ';

                $storedNeighborsCount = 1;
                $searchArray = ['{index_placeholder}','{name_placeholder}','{phone_placeholder}','{address_placeholder}'];

                if(!empty(old('neighbors'))){
                    foreach(old('neighbors') as $neighbor){
                        $replaceArray = [$storedNeighborsCount, $neighbor['name'], $neighbor['phone'], $neighbor['address']];
                        echo str_replace($searchArray,$replaceArray,$neighborsSampleElements);
                        $storedNeighborsCount++;
                    }
                }else{
                    $storedNeighborsCount = 1;
                    $replaceArray = [$storedNeighborsCount, '','',''];
                    echo str_replace($searchArray,$replaceArray,$neighborsSampleElements);
                }
                ?>

            </span>


            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <button type="button" id="addNewNeighborsButton" class="btn btn-success float-left">جدید</button>
                </div>
            </div>







            <div class="col-xs-12 col-sm-12 col-md-12 mb-md-4 mt-md-5">
                <h4>با چه شرکت و افرادی همکاری دارد</h4>
            </div>


            <span id="partners_container">

            <?php
                $partnersSampleElements = '
                    <div class="col-xs-12 col-sm-12 col-md-4 mt-md-2">
                        <div class="form-group">
                            <strong>نام فرد یا شرکت:</strong>
                            <input type="text" name="partners[{index_placeholder}][name]" class="form-control" placeholder=""  value="{name_placeholder}" >
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-4 mt-md-2">
                        <div class="form-group">
                            <strong>محصول یا برند:</strong>
                            <input type="text" name="partners[{index_placeholder}][products]" class="form-control" placeholder=""  value="{products_placeholder}" >
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-4 mt-md-2">
                        <div class="form-group">
                            <strong>تلفن همراه یا ثابت:</strong>
                            <input type="text" name="partners[{index_placeholder}][phones]" class="form-control" placeholder=""  value="{phones_placeholder}" >
                        </div>
                    </div>
                ';

                $storedPartnersCount = 1;
                $searchArray = ['{index_placeholder}','{name_placeholder}','{products_placeholder}','{phones_placeholder}'];

                if(!empty(old('partners'))){
                    foreach(old('partners') as $partner){

                        $replaceArray = [$storedPartnersCount, $partner['name'], $partner['products'], $partner['phones']];
                        echo str_replace($searchArray,$replaceArray,$partnersSampleElements);
                        $storedPartnersCount++;
                    }
                }else{
                    $storedPartnersCount = 1;
                    $replaceArray = [$storedPartnersCount, '','',''];
                    echo str_replace($searchArray,$replaceArray,$partnersSampleElements);
                }
                ?>

            </span>


            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <button type="button" id="addNewPartnersButton" class="btn btn-success float-left">جدید</button>
                </div>
            </div>









            <div class="col-xs-12 col-sm-12 col-md-3 form-input-container mb-md-5 mt-md-5">

                <div class="form-group">

                    <strong>تصویر چک:</strong>

                    <input type="file" onchange="readURL(this,'business_cheque_file_preview_container')" name="business_cheque_file" id="business_cheque_file" value="{{ old('business_cheque_file',isset($business->business_cheque_file) ? $business->business_cheque_file : '') }}" autocomplete="off" >

                </div>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-3 form-input-container mb-md-5 mt-md-5">

                <div class="form-group">

                    <div id="business_cheque_file_preview_container"></div>

                </div>

            </div>



            <div class="col-xs-12 col-sm-12 col-md-3 form-input-container mb-md-5 mt-md-5">

                <div class="form-group">

                    <strong>تصویر فروشگاه:</strong>
                    <input {{(auth()->user()->user_type=="SALES_SPECIALIST")?"required":""}} type="file" multiple onchange="readURL(this,'business_images_preview_container')" name="business_images[]" id="business_images" value="{{ old('business_images',isset($business->business_images) ? $business->business_images : '') }}" autocomplete="off" >

                </div>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-3 form-input-container mb-md-5 mt-md-5">

                <div class="form-group">
                    <div id="business_images_preview_container"></div>

                </div>

            </div>






















            <div class="col-xs-12 col-sm-12 col-md-3 mt-md-2">

                <div class="form-group">

                    <strong>سقف اعتبار پیشنهادی:</strong>

                    <input type="text" name="suggested_credit_limit" value="{{old('suggested_credit_limit')}}" class="form-control" placeholder="" >

                </div>

            </div>


            <div class="col-xs-12 col-sm-12 col-md-3 mt-md-2">

                <div class="form-group">

                    <strong>شماره تلفن ثابت:</strong>

                    <input type="text" name="phone_number" value="{{old('phone_number'     ,     (!empty($customerData['phone_number'])?$customerData['phone_number']:'')      )}}" class="form-control" placeholder="" >

                </div>

            </div>



            <div class="col-xs-12 col-sm-12 col-md-6 mt-md-2">

                <div class="form-group">

                    <strong>توضیحات:</strong>

                    <textarea class="form-control" name="business_description" placeholder="" >{{old('business_description')}}</textarea>

                </div>

            </div>



            <div class="col-xs-12 col-sm-12 col-md-12 mt-md-2">

                <div class="form-group">

                    <input class="checkbox"  type="checkbox" value="1"  name="sell_approved_by_seller" {{ (old('sell_approved_by_seller')==1)?'checked':'' }} autocomplete="off" >&nbsp;از نظر فروشنده مربوطه فروش چکی بالامانع است<br><br>

                </div>

            </div>
            
            <div class="col-xs-12 col-sm-12 col-md-12 mt-md-2">

                <div class="form-group">

                    <input class="checkbox"  type="checkbox" value="1"  name="no_validation_required" {{ (old('no_validation_required')==1)?'checked':'' }} autocomplete="off" >&nbsp;فاقد اعتبارسنجی<br><br>

                </div>

            </div>


            <div class="col-xs-12 col-sm-12 col-md-12 mt-md-3 mb-md-5">

                <div class="form-group">

                    <div id="map"></div>

                </div>

            </div>




            <input type="hidden" name="latitude" id="latitude" value="{{old('latitude')}}" class="form-control"  {{(auth()->user()->user_type=="SALES_SPECIALIST")?"required":""}}>
            <input type="hidden" name="longitude" id="longitude" value="{{old('longitude')}}" class="form-control"  {{(auth()->user()->user_type=="SALES_SPECIALIST")?"required":""}}>







            <div class="col-xs-12 col-sm-12 col-md-12 text-center">

                <button type="submit" class="btn btn-primary">ذخیره</button>

            </div>

        </div>


    </form>


    <script>




        function change_list(master_el_id,child_el_id,loading_gif_id,ajax_req_url,depend_element_ids = []){

            var e = document.getElementById(master_el_id);
            r_id = e.options[e.selectedIndex].value;

            if(r_id==""){
                $('#'+child_el_id).html('');
                return;
            }

            $('#'+master_el_id).prop('disabled', true);
            //$('#'+loading_gif_id).css('display', 'block'); //TODO: enable loadings

            token= $('body').find("input[name='_token']").val();

            $.ajax({
                url: ajax_req_url,
                type: 'GET',
                data: {"r_id":r_id, "_token":token },
                success: function(data){

                    $('#'+child_el_id).html('');
                    html_str='<option value="" >انتخاب کنید</option>';
                    $('#'+child_el_id).append(html_str);
                    $.each(data, function( index, value ) {
                        html_str='<option value="'+value.id+'" >'+value.title+'</option>';
                        $('#'+child_el_id).append(html_str);
                    });

                    depend_element_ids.forEach(function(){
                        $('#'+depend_element_ids).empty();
                    });

                    $('#'+master_el_id).prop('disabled', false);
                    //$('#'+loading_gif_id).css('display', 'none');
                },
                error: function(data){
                    alert(data.responseText);
                    alert('دریافت اطلاعات با خطا مواجه شد!');
                    $('#'+master_el_id).prop('disabled', false);
                    //$('#'+loading_gif_id).css('display', 'none');
                }
            });

        }
    </script>


    <script type="text/javascript">





        function readURL(input,img_container_id) {

            $(input).removeClass('required');

            $('#'+img_container_id).empty();
            for (let i = 0; i < input.files.length; i++) {
                if (input.files && input.files[i]) {
                    var reader = new FileReader();

                    var newImgElementID = img_container_id+"_img_"+i;
                    $('#'+img_container_id).append('<img id="'+newImgElementID+'" class="img img-responsive" />');

                    reader.onload = function (e) {


                        $('#'+newImgElementID)
                            .attr('src', e.target.result)
                            .height(100);
                    };

                    reader.readAsDataURL(input.files[i]);
                    //$('#'+img_id).removeClass('hide');


                }




            }


        }



    </script>

@endsection

@section('additional_js_bottom')
    <script src = "{{asset('js/leaflet.js')}}"></script>

    <script>
    
    function NumbersOnly(evt, label) {
        var status = document.getElementById(label);
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode == 46) {
            return true;
        }
        if (charCode < 48 || charCode > 57) {
            status.innerHTML = 'Numbers Only Please';
            return false;
        }
        status.innerHTML = '';
        return true;
    }



        function escapeRegExp(string) {
            return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
        }

        function replaceAll(str, find, replace) {
            return str.replace(new RegExp(escapeRegExp(find), 'g'), replace);
        }

        $(document).ready(function(){

            neighborsCount = <?php echo $storedNeighborsCount; ?>;

            $("#addNewNeighborsButton").click(function(){
                neighborsCount++;
                var neighborsSampleElements = `<?php echo $neighborsSampleElements; ?>`;
                neighborsSampleElements = replaceAll(neighborsSampleElements,'{index_placeholder}',neighborsCount);
                neighborsSampleElements = replaceAll(neighborsSampleElements,'{name_placeholder}','');
                neighborsSampleElements = replaceAll(neighborsSampleElements,'{phone_placeholder}','');
                neighborsSampleElements = replaceAll(neighborsSampleElements,'{address_placeholder}','');

                $('#neighbors_container').append(neighborsSampleElements);
            });


            partnersCount = <?php echo $storedPartnersCount; ?>;

            $("#addNewPartnersButton").click(function(){
                partnersCount++;
                var partnersSampleElements = `<?php echo $partnersSampleElements; ?>`;
                partnersSampleElements = replaceAll(partnersSampleElements,'{index_placeholder}',partnersCount);
                partnersSampleElements = replaceAll(partnersSampleElements,'{name_placeholder}','');
                partnersSampleElements = replaceAll(partnersSampleElements,'{products_placeholder}','');
                partnersSampleElements = replaceAll(partnersSampleElements,'{phones_placeholder}','');

                $('#partners_container').append(partnersSampleElements);
            });


            // Creating map options



            let mapCenter = {lat:'35.709165', lng:'51.371681'};

            let oldLat = '<?php echo old('latitude'); ?>';
            let oldLng = '<?php echo old('longitude'); ?>';



            if(oldLat && oldLat!=="" && oldLng && oldLng!==""){
                mapCenter.lat = oldLat;
                mapCenter.lng = oldLng;
            }
            
            
            


            var mapOptions = {
                center: [mapCenter.lat,mapCenter.lng ],
                zoom: 11
            }

            // Creating a map object
            var map = new L.map('map', mapOptions);

            // Creating a Layer object
            var layer = new     L.TileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');

            // Adding layer to the map
            map.addLayer(layer);


            var marker;
            map.on('click', function(e) {
                if(marker)
                    map.removeLayer(marker);

                marker = L.marker(e.latlng).addTo(map);

                $('#latitude').val(e.latlng.lat);
                $('#longitude').val(e.latlng.lng)
            });

            if(marker){
                map.removeLayer(marker);
            }
            
            if(oldLat && oldLat!=="" && oldLng && oldLng!==""){
		    let latlng = {lat:'<?php echo old('latitude'); ?>', lng:'<?php echo old('longitude'); ?>'};

		    marker = L.marker(latlng).addTo(map);
            }else {
            
            		
            	    
            
            	    navigator.geolocation.getCurrentPosition(function(location) {
		        mapCenter.lat = location.coords.latitude;
		        mapCenter.lng = location.coords.longitude;
		    
		    	map.panTo([location.coords.latitude, location.coords.longitude]);
		    	map.setZoom(17);

		    });
            }
            

        });



        $(document).ready(function() {
            $('#business_store_form').keydown(function(event){
                if(event.keyCode == 13) {
                    event.preventDefault();
                    return false;
                }
            });
            
            
	
        });
        
        
        function validateForm() {
        	if(neighborsCount<3){
			if($('#buy_method').val()>0){
        			alert('حداقل ۳ مورد همسایه باید وارد شود');
        			return false;
			}
        	}
        	
        	
        	if(partnersCount<3){
			if($('#buy_method').val()>0){
        			alert('حداقل ۳ مورد شرکت باید وارد شود');
        			return false;
			}
        	}
	} 


    </script>
@stop

