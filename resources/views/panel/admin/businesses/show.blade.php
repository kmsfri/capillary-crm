@extends('panel.layout.master')
@section('additional_css')
    <link rel = "stylesheet" href = "{{asset('css/leaflet.css')}}" />
@stop
@section('main_content')






    <div class="row mb-5">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2>مشاهده اطلاعات کسب و کار</h2>

            </div>

            <div class="pull-left">

                <a class="btn btn-primary" href="{{ route('businesses.index') }}"> بازگشت</a>

            </div>

        </div>

    </div>




    <form action="#" method="POST" enctype="multipart/form-data">


        <div class="row">
        
        
            <div class="col-xs-12 col-sm-12 col-md-3 form-input-container">

                <div class="form-group">

                    <strong>نوع مشتری:</strong>

                    <select required  name='customer_legal_type' id='customer_legal_type' class='form-control pull-right' disabled>
                        <!--<option {{(!old('customer_legal_type', isset($business->customer_legal_type) ? $business->customer_legal_type : '')? 'selected' : '')}} value="">انتخاب کنید</option>-->
                        @foreach($enums['customer_legal_type'] as $key=>$legalType)
                            <option @if(old('customer_legal_type', isset($business->customer_legal_type) ? $business->customer_legal_type : '')==$key) selected @endif value="{{$key}}" >{{$legalType}}</option>
                        @endforeach
                    </select>

                </div>

            </div>
            
            
            <div class="col-xs-12 col-sm-12 col-md-3 form-input-container">

                <div class="form-group">

                    <strong>درجه بندی مشتری:</strong>

                    <select required  name='customer_degree' id='customer_degree' class='form-control pull-right' disabled>
                        <!--<option {{(!old('customer_degree', isset($business->customer_degree) ? $business->customer_degree : '')? 'selected' : '')}} value="">انتخاب کنید</option>-->
                        @foreach($enums['customer_degree'] as $key=>$customerDegree)
                            <option @if(old('customer_degree', isset($business->customer_degree) ? $business->customer_degree : '')==$key) selected @endif value="{{$key}}" >{{$customerDegree}}</option>
                        @endforeach
                    </select>

                </div>

            </div>
        
        
            <div class="col-xs-12 col-sm-12 col-md-3 form-input-container">

                <div class="form-group">

                    <strong>رده شغلی:</strong>

                    <select required  name='business_type_id' id='business_type_id' class='form-control pull-right' disabled>
                        <!--<option {{(!old('business_type_id', isset($business->business_type_id) ? $business->business_type_id : '')? 'selected' : '')}} value="">انتخاب کنید</option>-->
                        @foreach($businessTypes as $businessType)
                            <option @if(old('business_type_id', isset($business->business_type_id) ? $business->business_type_id : '')==$businessType->id) selected @endif value="{{$businessType->id}}" >{{$businessType->business_type_title}}</option>
                        @endforeach
                    </select>

                </div>

            </div>
            
            
            <div class="col-xs-12 col-sm-12 col-md-3 form-input-container">

                <div class="form-group">

                    <strong>نوع جذب مشتری:</strong>

                    <select required  name='customer_recruitment_type' id='customer_recruitment_type' class='form-control pull-right' disabled>
                        <!--<option {{(!old('customer_recruitment_type', isset($business->customer_recruitment_type) ? $business->customer_recruitment_type : '')? 'selected' : '')}} value="">انتخاب کنید</option>-->
                        @foreach($enums['customer_recruitment_type'] as $key=>$customerRecruitmentType)
                            <option @if(old('customer_recruitment_type', isset($business->customer_recruitment_type) ? $business->customer_recruitment_type : '')==$key) selected @endif value="{{$key}}" >{{$customerRecruitmentType}}</option>
                        @endforeach
                    </select>

                </div>

            </div>
        

            <div class="col-xs-12 col-sm-12 col-md-3 form-input-container">

                <div class="form-group">

                    <strong>نام:</strong>

                    <input disabled value="{{ $business->first_name }}" type="text" name="first_name" class="form-control" placeholder="">

                </div>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-3 form-input-container">

                <div class="form-group">

                    <strong>نام خانوادگی:</strong>

                    <input disabled value="{{ $business->last_name }}" type="text" name="last_name" class="form-control" placeholder="">

                </div>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-3 form-input-container">

                <div class="form-group">

                    <strong>نام فروشگاه:</strong>

                    <input disabled type="text" name="business_name" value="{{ $business->business_name }}" class="form-control" placeholder="">

                </div>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-3 form-input-container">

                <div class="form-group">

                    <strong>نام باربری:</strong>

                    <input disabled type="text" name="shipping_name" value="{{ $business->shipping_name }}" class="form-control" placeholder="">

                </div>

            </div>

	    
	    <div class="col-xs-12 col-sm-12 col-md-2 form-input-container">

                 <div class="form-group">

                    <strong>تاریخ تولد:</strong>

                    <input disabled type="text" name="birth_date" value="{{ $business->birth_date }}" class="form-control" placeholder="">

                </div>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-4 form-input-container">

                <div class="form-group">

                    <strong>استان - شهر - منطقه:</strong>

                    <input disabled type="text" value="{{ $business->getFullStateCityRegion() }}" class="form-control" placeholder="">

                </div>

            </div>
            
            





            <div class="col-xs-12 col-sm-12 col-md-6 form-input-container">

                <div class="form-group">

                    <strong>آدرس:</strong>

                    <textarea disabled class="form-control" style="height:100px" name="address" placeholder="">{{ $business->address }}</textarea>

                </div>

            </div>


            <div class="col-xs-12 col-sm-12 col-md-3 form-input-container">

                <div class="form-group">

                    <strong>کد ملی شخص/کد ملی شرکت:</strong>

                    <input disabled type="text" name="national_code" value="{{ $business->national_code }}" class="form-control" placeholder="">

                </div>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-3 form-input-containerform-input-container">

                <div class="form-group">

                    <strong>کد پستی:</strong>

                    <input disabled type="text" name="postal_code" value="{{ $business->postal_code }}" class="form-control" placeholder="">

                </div>

            </div>


            <div class="col-xs-12 col-sm-12 col-md-3 form-input-container">

                <div class="form-group">

                    <strong>تلفن همراه:</strong>

                    <input disabled type="text" name="mobiles[]" value="{{ !empty($business->mobiles[0])?$business->mobiles[0]:'' }}" class="form-control" placeholder="">

                </div>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-3 form-input-container">

                <div class="form-group">

                    <strong>تلفن همراه ۲(سوشال مدیا):</strong>

                    <input disabled type="text" name="mobiles[]" value="{{ !empty($business->mobiles[1])?$business->mobiles[1]:'' }}" class="form-control" placeholder="">

                </div>

            </div>




            <div class="col-xs-12 col-sm-12 col-md-3 form-input-container">

                <div class="form-group">

                    <strong>نحوه خرید:</strong>


                    <?php
                    $buyTypeText = '';
                    if($business->buy_method==0){
                        $buyTypeText = "نقد";
                    }elseif($business->buy_method==1){
                        $buyTypeText = "چکی";
                    }elseif($business->buy_method==2){
                        $buyTypeText = "حساب باز(اعتباری)";
                    }
                    ?>
                    <input disabled type="text" value="{{$buyTypeText}}" class="form-control" placeholder="">


                </div>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-3 form-input-container">

                <div class="form-group">

                    <strong>مالکیت مغازه:</strong>


                    <input disabled type="text" value="{{($business->business_ownership==0)?'مالک':'اجاره'}}" class="form-control" placeholder="">



                </div>

            </div>
            
            
            
            <div class="col-xs-12 col-sm-12 col-md-3 form-input-container">

                <div class="form-group">

                    <strong>نام و نام خانوادگی شریک:</strong>

                    <input value="{{ old('copartner_name',$business->copartner_name) }}" type="text" name="copartner_name" class="form-control" placeholder="" disabled>

                </div>

            </div>
            
            
            <div class="col-xs-12 col-sm-12 col-md-3 form-input-container">

                <div class="form-group">

                    <strong>تلفن شریک:</strong>

                    <input value="{{ old('copartner_phone',$business->copartner_phone) }}" type="text" name="copartner_phone" class="form-control" placeholder="" disabled>

                </div>

            </div>


            <div class="col-xs-12 col-sm-12 col-md-6 form-input-container mb-5">

                <div class="form-group">

                    <strong>تصویر سند یا اجاره نامه:</strong>

                    <div id="ownership_doc_file_preview_container">

                        <img class="img img-responsive" height="250" src="{{!empty($business->ownership_doc_file_path) ? $business->ownership_doc_file_path : ''}}" />


                    </div>

                </div>

            </div>


            <div class="col-xs-12 col-sm-12 col-md-6 form-input-container mb-5">

                <div class="form-group">

                    <strong>تصویر کارت ویزیت:</strong>

                    <div id="visit_card_file_preview_container">

                        <img class="img img-responsive" height="250" src="{{!empty($business->visit_card_file_path) ? $business->visit_card_file_path : ''}}" />


                    </div>

                </div>

            </div>




            <div class="col-xs-12 col-sm-12 col-md-12 form-input-container mb-5">

                <div class="form-group">

                    <strong>تصویر پروانه کسب:</strong>

                    <div id="business_license_file_preview_container">

                        <img class="img img-responsive" height="250" src="{{!empty($business->business_license_file_path) ? $business->business_license_file_path : ''}}" />

                    </div>

                </div>

            </div>



            <div class="col-xs-12 col-sm-12 col-md-3 form-input-container mb-md-5">

                <div class="form-group">

                    <strong>سابقه فعالیت در مکان فعلی:</strong>

                    <input disabled type="text" name="activity_amount" value="{{ $business->activity_amount }}" class="form-control" placeholder="">

                </div>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-3 form-input-container mb-md-5">

                <div class="form-group">

                    <strong>تعداد پرسنل شاغل:</strong>

                    <input disabled type="text" name="working_personnel" value="{{ $business->working_personnel }}" class="form-control" placeholder="">

                </div>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-3 form-input-container mb-md-5">

                <div class="form-group">

                    <strong>برآورد مقدار جنس دپو در فروشگاه(ریالی):</strong>

                    <input disabled type="text" name="assets_estimate_rial" value="{{ $business->assets_estimate_rial }}" class="form-control" placeholder="">

                </div>

            </div>


            <div class="col-xs-12 col-sm-12 col-md-3 form-input-container mb-md-5">

                <div class="form-group">

                    <strong>برآورد مقدار جنس دپو در فروشگاه(کارتنی):</strong>

                    <input disabled type="text" name="assets_estimate_box" value="{{ $business->assets_estimate_box }}" class="form-control" placeholder="">

                </div>

            </div>


            <div class="col-xs-12 col-sm-12 col-md-12 mb-md-4">
                <h4>آمار کسبه و همسایگان</h4>
            </div>

            <span id="neighbors_container">

            <?php
                $neighborsSampleElements = '
                    <div class="col-xs-12 col-sm-12 col-md-4 mt-md-2">
                        <div class="form-group">
                            <strong>نام و نام خانوادگی:</strong>
                            <input disabled type="text" name="neighbors[{index_placeholder}][name]" class="form-control" placeholder="" value="{name_placeholder}">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-4 mt-md-2">
                        <div class="form-group">
                            <strong>تلفن:</strong>
                            <input disabled type="text" name="neighbors[{index_placeholder}][phone]" class="form-control" placeholder="" value="{phone_placeholder}">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-4 mt-md-2">
                        <div class="form-group">
                            <strong>آدرس:</strong>
                            <input disabled type="text" name="neighbors[{index_placeholder}][address]" class="form-control" placeholder="" value="{address_placeholder}">
                        </div>
                    </div>
                ';

                $storedNeighborsCount = 1;
                $searchArray = ['{index_placeholder}','{name_placeholder}','{phone_placeholder}','{address_placeholder}'];

                if(!empty($business->neighbors)){
                    foreach($business->neighbors as $neighbor){
                        $replaceArray = [$storedNeighborsCount, $neighbor['name'], $neighbor['phone'], $neighbor['address']];
                        echo str_replace($searchArray,$replaceArray,$neighborsSampleElements);
                        $storedNeighborsCount++;
                    }
                }
                ?>

            </span>





            <div class="col-xs-12 col-sm-12 col-md-12 mb-md-4 mt-md-5">
                <h4>با چه شرکت و افرادی همکاری دارد</h4>
            </div>

            <span id="partners_container">

            <?php
                $partnersSampleElements = '
                    <div class="col-xs-12 col-sm-12 col-md-4 mt-md-2">
                        <div class="form-group">
                            <strong>نام فرد یا شرکت:</strong>
                            <input disabled type="text" name="partners[{index_placeholder}][name]" class="form-control" placeholder=""  value="{name_placeholder}">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-4 mt-md-2">
                        <div class="form-group">
                            <strong>محصول یا برند:</strong>
                            <input disabled type="text" name="partners[{index_placeholder}][products]" class="form-control" placeholder=""  value="{products_placeholder}">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-4 mt-md-2">
                        <div class="form-group">
                            <strong>تلفن همراه یا ثابت:</strong>
                            <input disabled type="text" name="partners[{index_placeholder}][phones]" class="form-control" placeholder=""  value="{phones_placeholder}">
                        </div>
                    </div>
                ';

                $storedPartnersCount = 1;
                $searchArray = ['{index_placeholder}','{name_placeholder}','{products_placeholder}','{phones_placeholder}'];

                if(!empty($business->partners)){
                    foreach($business->partners as $partner){

                        $replaceArray = [$storedPartnersCount, $partner['name'], $partner['products'], $partner['phones']];
                        echo str_replace($searchArray,$replaceArray,$partnersSampleElements);
                        $storedPartnersCount++;
                    }
                }
                ?>

            </span>


            <div class="col-xs-12 col-sm-12 col-md-6 form-input-container mb-md-5 mt-md-5">

                <div class="form-group">

                    <strong>تصویر چک:</strong>

                    <div id="business_cheque_file_preview_container">

                        <img class="img img-responsive" height="250" src="{{!empty($business->business_cheque_file_path) ? $business->business_cheque_file_path : ''}}" />

                    </div>


                </div>

            </div>





            <div class="col-xs-12 col-sm-12 col-md-6 form-input-container mb-md-5 mt-md-5">

                <div class="form-group">

                    <strong>تصویر فروشگاه:</strong>

                    <div id="business_images_preview_container">

                        @if(!empty($business->business_images))

                            @foreach($business->business_images as $businessImage)
                                <img class="img img-responsive" height="250" src="{{!empty($businessImage) ? $businessImage : ''}}" />
                            @endforeach
                        @endif


                    </div>
                </div>

            </div>





            <div class="col-xs-12 col-sm-12 col-md-3 mt-md-2">

                <div class="form-group">

                    <strong>سقف اعتبار پیشنهادی:</strong>

                    <input disabled type="text" name="suggested_credit_limit" value="{{ $business->suggested_credit_limit }}" class="form-control" placeholder="">

                </div>

            </div>


            <div class="col-xs-12 col-sm-12 col-md-3 mt-md-2">

                <div class="form-group">

                    <strong>شماره تلفن ثابت:</strong>

                    <input disabled type="text" name="phone_number" value="{{ $business->phone_number }}" class="form-control" placeholder="">

                </div>

            </div>



            <div class="col-xs-12 col-sm-12 col-md-6 mt-md-2">

                <div class="form-group">

                    <strong>توضیحات:</strong>

                    <textarea disabled class="form-control" name="business_description" placeholder="">{{ $business->business_description }}</textarea>

                </div>

            </div>




            <div class="col-xs-12 col-sm-12 col-md-12 mt-md-2">

                <div class="form-group">

                    <input disabled class="checkbox"  type="checkbox" value="1"  name="sell_approved_by_seller" autocomplete="off" {{ ($business->sell_approved_by_seller==1)?'checked':'' }}>&nbsp;از نظر فروشنده مربوطه فروش چکی بالامانع است<br><br>

                </div>

            </div>


            <div class="col-xs-12 col-sm-12 col-md-12 mt-md-3 mb-md-5">

                <div class="form-group">

                    <div id="map"></div>

                </div>

            </div>





        </div>


    </form>





@endsection

@section('additional_js_bottom')
    <script src = "{{asset('js/leaflet.js')}}"></script>

    <script>


        $(document).ready(function(){


            // Creating map options
            var mapOptions = {
                center: [<?php echo $business->latitude ?>, <?php echo $business->longitude ?>],
                zoom: 11
            }

            // Creating a map object
            var map = new L.map('map', mapOptions);

            // Creating a Layer object
            var layer = new     L.TileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');

            // Adding layer to the map
            map.addLayer(layer);


            var marker;

            if(marker){
                map.removeLayer(marker);
            }
            let latlng = {lat:'<?php echo $business->latitude; ?>', lng:'<?php echo $business->longitude; ?>'};

            marker = L.marker(latlng).addTo(map);


        });






    </script>
@stop
