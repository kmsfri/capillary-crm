@extends('panel.layout.master')
@section('additional_css')


<style>

.create-invoice-col a{
  display:block;
}

</style>
    
@stop
@section('main_content')


    <div class="row mb-md-5">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2>کارتابل کسب و کار: {{$business->business_name."(".$business->first_name." ".$business->last_name.")"}}</h2>

            </div>

            <div class="pull-left">


            </div>

        </div>

    </div>
    
    
    @if ($message = Session::get('success'))

        <div class="alert alert-success">

            <p>{{ $message }}</p>

        </div>

    @endif
    
    @if ($message = Session::get('danger'))

        <div class="alert alert-danger">

            <p>{{ $message }}</p>

        </div>

    @endif
    
    
    
    
    
   
   
   
   
<!-- BEGIN BUSINESS INFORMATION -->


<div class="row mt-3 mb-3">

<div class="col-lg-12">


<div class="card border-success">
<div class="card-header">اطلاعات کلی کسب و کار</div>
<div class="card-body">
				
				
				
				
				
				
				
				
				
				
			
				
<table class="table table-bordered">



<tr>
	<td>
		<strong>نام:</strong>
                {{ $business->first_name }}
	</td>
	<td>
		<strong>نام خانوادگی:</strong>
                {{ $business->last_name }}
	</td>
</tr>

<tr>
	<td>
		<strong>نام فروشگاه:</strong>
                {{ $business->business_name }}
	</td>
	<td>
		<strong>نوع کاربر:</strong>
		{{$business->customer_legal_type=='real'?'حقیقی':'حقوقی'}}
                
	</td>
</tr>


<tr>
	<td colspan="2">
		<strong>آدرس کامل:</strong>
                {{ $business->getFullStateCityRegion() }}
	</td>
</tr>


<tr>
	<td>
		<strong>کد ملی / شناسه ملی:</strong>
                {{ $business->national_code }}
	</td>
	<td>
		<strong>کد پستی:</strong>
                {{ $business->postal_code }}
	</td>
</tr>


<tr>
	<td>
		<strong>تلفن ثابت:</strong>
                {{ $business->phone_number }}
	</td>
	@php
	$buyMethod = '-';
	if($business->buy_method==0){
		$buyMethod = 'نقد';
	}else if($business->buy_method==1){
		$buyMethod = 'چک';
	}else if($business->buy_method==2){
		$buyMethod = 'اعتباری';
	}
	@endphp
	<td>
		<strong>نحوه خرید:</strong>
                {{ $buyMethod }}
	</td>
</tr>

@php
	$mobiles = json_decode($business->mobiles,true);
@endphp
<tr>
	<td>
		<strong>موبایل ۱:</strong>
                {{ !empty($mobiles[0])?$mobiles[0]:'-' }}
	</td>
	<td>
		<strong>موبایل ۲:</strong>
                {{ !empty($mobiles[1])?$mobiles[1]:'-' }}
	</td>
</tr>







<tr>
	<td colspan="2">
		<strong>کاربر ایجاد کننده:</strong>
                {{ $business->creator()->first()->name }}({{ $business->creator()->first()->email }})
	</td>
</tr>


<tr>
	<td>
		<strong>زمان ایجاد:</strong>
                {{\Morilog\Jalali\Jalalian::forge($business->created_at)->format('%A, %d %B %Y در ساعت H:i:s')}}
	</td>
	<td>
		<strong>زمان آخرین ویرایش:</strong>
                {{\Morilog\Jalali\Jalalian::forge($business->updated_at)->format('%A, %d %B %Y در ساعت H:i:s')}}
	</td>
</tr>



</table>
				
							
	
	
</div>
</div>

</div>

</div>


<!-- END CUSTOMER INFORMATION -->
   
   
   
   
   
   
   
    
    
    
    
    
    
    
    
<div class="row mt-3 mb-3">

<div class="col-lg-12">


<div class="card border-default">
<div class="card-header">سوابق فاکتورهای صادر شده</div>
<div class="card-body">
				
				
				
				
				
				
				
				
				
				
@if(count($invoices)==0)
	<p class="card-text">هیچ فاکتوری برای این کسب و کار صادر نشده است</p>
@else				
				
<table class="table table-bordered">

<tr>
    <th>ثبت کننده</th>

    <th>تاریخ ثبت</th>
    
    <th>نوع</th>
    
    <th>ﻋﻤﻠﯿﺎﺕ</th>

</tr>

@foreach ($invoices as $invoice)

    <tr>

        

        <td>{{ $invoice->creator()->first()->name }}</td>
        
        <td>{{ \Morilog\Jalali\Jalalian::forge($invoice->created_at)->format('%A, %d %B %Y در ساعت H:i:s') }}</td>

	@php
		$invoiceGetPDFRoute  = "#";
		$invoiceEditRoute  = "#";
		$invoiceTitle = "";
		if($invoice->invoice_type=="official"){
			$invoiceGetPDFRoute  = route('invoices.get-pdf',['invoice_id'=>$invoice->id]);
			$invoiceEditRoute  = route('invoices.edit',$invoice->id);
			$invoiceTitle = "رسمی";
		}else if($invoice->invoice_type=="unofficial"){
			$invoiceGetPDFRoute  = route('invoices.unofficial.get-pdf',['invoice_id'=>$invoice->id]);
			$invoiceEditRoute  = route('invoices.unofficial.edit',$invoice->id);
			$invoiceTitle = "غیررسمی";
		}
	@endphp
	
	<td>{{ $invoiceTitle }}</td>

        <td>
        
        	
        
		<a target="_brank" class="btn btn-warning" href="{{ $invoiceGetPDFRoute }}">دریافت</a>


		@can('invoice-edit')

		    <a target="_brank" class="btn btn-primary" href="{{ $invoiceEditRoute }}">ﻭﯾﺮاﯾﺶ</a>

		@endcan
        </td>

    </tr>

@endforeach

</table>
				
				
@endif			
				
				
				
	
	
</div>
</div>

</div>

</div>










<!-- BEGIN CREDITS -->








<div class="row mt-3 mb-3">

<div class="col-lg-12">


<div class="card border-success">
<div class="card-header">سوابق اعتبار</div>
<div class="card-body">
				
				
				
				
				
				
				
				
				
				
@if(count($credits)==0)
	<p class="card-text">هیچ اعتباری برای این کسب و کار تنظیم نشده است</p>
@else				
				
<table class="table table-bordered">

<tr>
    <th>مبلغ</th>

    <th>تاریخ</th>
    
    <th>توضیحات</th>
    
    <th>ﻋﻤﻠﯿﺎﺕ</th>

</tr>

@foreach ($credits as $credit)

    <tr>

        <td style="direction: ltr">{{ number_format($credit->credit_amount) }}</td>

        <td>{{ \Morilog\Jalali\Jalalian::forge($credit->created_at)->format('%A, %d %B %Y در ساعت H:i:s') }}</td>
        
        <td>{{ $credit->credit_description }}</td>

	@php
		$invoice = Null;
		if(!empty($credit->invoice_id)){
			$invoice = \App\Models\Invoice::find($credit->invoice_id);
		}
	@endphp
	
        <td>
        
        	
        	@if(!empty($invoice))
        		@php
				$invoiceGetPDFRoute  = "#";
				
				if($invoice->invoice_type=="official"){
					$invoiceGetPDFRoute  = route('invoices.get-pdf',['invoice_id'=>$invoice->id]);
					
				}else if($invoice->invoice_type=="unofficial"){
					$invoiceGetPDFRoute  = route('invoices.unofficial.get-pdf',['invoice_id'=>$invoice->id]);
					
				}
			@endphp
        	
			<a target="_brank" class="btn btn-warning" href="{{ $invoiceGetPDFRoute }}">دریافت فاکتور</a>
		@else
			-
		@endif

        </td>

    </tr>

@endforeach

</table>
				
				
@endif			
				
				
				
	
	
</div>
</div>

</div>

</div>










<!-- BEGIN CALLS HISTORY -->













<div class="row mt-3 mb-3">

<div class="col-lg-12">


<div class="card border-success">
<div class="card-header">سوابق تماسها</div>
<div class="card-body">
				
				
				
				
				
				
				
				
				
				
@if(count($businessCalls)==0)
	<p class="card-text">هیچ تماسی برای این کسب و کار ثبت نشده است</p>
@else				
				
<table class="table table-bordered">

<tr>
    <th>تاریخ تماس</th>
    <th>داخلی تماس گیرنده</th>
    <th>نام تماس گیرنده</th>
    <th>مقصد</th>
    <th>مدت زمان</th>
    <th>وضعیت پاسخ</th>

</tr>



@php
$dispositionValues = [
	'NO ANSWER' => ['title'=>'پاسخ داده نشده','class'=>'warning'],
	'BUSY' => ['title'=>'مشغول','class'=>'info'],
	'ANSWERED' => ['title'=>'پاسخ داده شده','class'=>'success'],
	'CONGESTION' => ['title'=>'ترافیک','class'=>'default'],
	'FAILED' => ['title'=>'ناموفق','class'=>'danger']
];
@endphp


@foreach ($businessCalls as $call)

    <tr>

        <td>{{\Morilog\Jalali\Jalalian::forge($call->calldate)->format('%A, %d %B %Y در ساعت H:i:s')}}</td>
        <td>{{$call->src}}</td>
        <td>
        @php
        $crmUser = \App\Models\User::where('voip_id',$call->src)->first();
        @endphp
        @if(!empty($crmUser->name))
        {{$crmUser->name}}
        @else
        {{$call->src}}
        @endif
        </td>
        <td>
        
        
        @php
        $crmUser = \App\Models\User::where('voip_id',$call->dst)->first();
        @endphp
        @if(!empty($crmUser->name))
        {{$crmUser->name}}
        @else
        {{$call->dst}}
        @endif
        
        </td>
        <td>{{$call->duration}} ثانیه</td>
        <td><label class="badge badge-{{$dispositionValues[$call->disposition]['class']}}">{{$dispositionValues[$call->disposition]['title']}}</label></td>	

    </tr>

@endforeach

</table>
				
				
@endif			
				
				
				
	
	
</div>
</div>

</div>

</div>







<!-- BEGIN ALARMS -->






<div class="row mt-3 mb-3">

<div class="col-lg-12">


<div class="card border-success">
<div class="card-header">سوابق آلارمها</div>
<div class="card-body">
				
				
				
				
				
				
				
				
				
				
@if(count($businessAlarms)==0)
	<p class="card-text">هیچ آلارمی برای این کسب و کار ثبت نشده است</p>
@else				
				
<table class="table table-bordered">

<tr>
	<th>توضیحات</th>
	<td>تاریخ یادآوری</td>
</tr>





@foreach ($businessAlarms as $alarm)

	@php
		$rowClass = "";
		if($alarm->is_seen==1){

			$rowClass = 'success';

		}elseif(\Morilog\Jalali\Jalalian::forge($alarm->alarm_at)->isPast()){
			$rowClass = 'danger';
		}
	@endphp



    <tr class="{{$rowClass}}">



	<td>{{$alarm->alarm_description}}</td>


	<td>{{ \Morilog\Jalali\Jalalian::forge($alarm->alarm_at)->format('%A, %d %B %Y در ساعت H:i:s') }}</td>


    </tr>

@endforeach

</table>
				
				
@endif			
				
				
				
	
	
</div>
</div>

</div>

</div>


<!-- END ALARMS -->






<!-- BEGIN BUSINESS NOTES -->


<div class="row mt-3 mb-3">

<div class="col-lg-12">


<div class="card border-success">
<div class="card-header">یادداشتهای کسب و کار</div>
<div class="card-body">
				
	<form action="{{ route('businesses.addNewNote') }}" method="POST">
        	@csrf
        	<input type="hidden" name="business_id" value="{{$business->id}}">
		<div class="form-group">

			<textarea class="form-control" name="note_text" placeholder="" rows="5">{{old('note_text')}}</textarea>

		</div>
		
		<div class="col-xs-12 col-sm-12 col-md-12 text-center">

			<button type="submit" class="btn btn-primary">ثبت یادداشت</button>

		</div>
		
	</form>
	
	
	
	
	
	
	
	
	
					
	@if(count($businessNotes)==0)
		<p class="card-text text-center">هنوز هیچ یادداشتی برای این کسب و کار ثبت نشده است</p>
	@else				
					
	<table class="table table-bordered mt-5">

	<tr>
		<th>متن یادداشت</th>
		<td>ثبت کننده</td>
		<td>تاریخ ثبت</td>
	</tr>





	@foreach ($businessNotes as $note)


	    <tr>



		<td>{{$note->note_text}}</td>
		
		<td>{{ $note->creator()->first()->name }}</td>


		<td>{{ \Morilog\Jalali\Jalalian::forge($note->created_at)->format('%Y/%m/%d H:i:s') }}</td>


	    </tr>

	@endforeach

	</table>
					
					
	@endif	
	
	
	
	
	
	
	
	
	
	
					
							
	
	
</div>
</div>

</div>

</div>


<!-- END CUSTOMER NOTES -->









@endsection



