@extends('panel.layout.master')
@section('additional_js_top2')

<script src="{{asset('highcharts/highcharts.js')}}"></script>
<script src="{{asset('highcharts/modules/exporting.js')}}"></script>
<script src="{{asset('highcharts/modules/export-data.js')}}"></script>
<script src="{{asset('highcharts/modules/accessibility.js')}}"></script>

@endsection
@section('main_content')
    <div class="row mb-md-5">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2>گزارش جامع آماری کسب و کارها</h2>

            </div>

           

        </div>

    </div>


    @if ($message = Session::get('success'))

        <div class="alert alert-success">

            <p>{{ $message }}</p>

        </div>

    @endif
    
    @if ($message = Session::get('danger'))

        <div class="alert alert-danger">

            <p>{{ $message }}</p>

        </div>

    @endif
    
    
    
    <div class="row">    
    <div class="col-lg-12 mb-md-5">
        <form action="{{route('businesses.showBusinessesReport')}}" method="GET">
        <div class="row">
	    
            
            
            
            <div class="col-xs-12 col-sm-12 col-md-2 form-input-container">

                <div class="form-group">

                    <strong>از تاریخ:</strong>

                    <input data-jdp type="text" name="from_date" id="from_date" value="{{ old('from_date',request()->from_date) }}" class="form-control" placeholder="">

                </div>

            </div>
            
            
            <div class="col-xs-12 col-sm-12 col-md-2 form-input-container">

                <div class="form-group">

                    <strong>تا تاریخ:</strong>
                    
                    <input data-jdp type="text" name="to_date" id="to_date" value="{{ old('to_date',request()->to_date) }}" class="form-control" placeholder="">

                </div>

            </div>
            
			
			
			
			<div class="col-xs-12 col-sm-12 col-md-2 form-input-container">

		<div class="form-group">

		    <strong>استان:</strong>

		    <select  name='state_id' id='state_id' class='form-control pull-right' onChange="change_list('state_id','city_id','loading_gif_cities','{{route('getCities')}}',['region_id'])"> 
			<option {{(!old('state_id', isset(request()->state_id) ? request()->state_id : '')? 'selected' : '')}} value="">انتخاب کنید</option>
			@foreach($states as $state)
			    <option @if(old('state_id', isset(request()->state_id) ? request()->state_id : '')==$state->id) selected @endif value="{{$state->id}}" >{{$state->unit_name}}</option>
			@endforeach
		    </select>

		</div>

	    </div>
	
	
	    <div class="col-xs-12 col-sm-12 col-md-2 form-input-container">

			<div class="form-group">

			    <strong>شهر:</strong>

			    <select  name='city_id' id='city_id' class='form-control pull-right'>
				@if($cities)
				    <option {{(!old('city_id', isset(request()->city_id) ? request()->city_id : '')? 'selected' : '')}} value="">انتخاب کنید</option>
				    @foreach($cities as $city)
					<option @if(old('city_id', isset(request()->city_id) ? request()->city_id : '')==$city->id) selected @endif value="{{$city->id}}" >{{$city->unit_name}}</option>
				    @endforeach
				@endif
			    </select>

			</div>

	    </div>
	
	
	    <div class="col-xs-12 col-sm-12 col-md-2 form-input-container">

			<div class="form-group">

				<strong>درجه بندی مشتری:</strong>

				<select  name='customer_degree' id='customer_degree' class='form-control pull-right'>
					 <option {{(!old('customer_degree', isset(request()->customer_degree) ? request()->customer_degree : '')? 'selected' : '')}} value="">انتخاب کنید</option>
					@foreach($enums['customer_degree'] as $key=>$customerDegree)
						<option @if(old('customer_degree', isset(request()->customer_degree) ? request()->customer_degree : '')==$key) selected @endif value="{{$key}}" >{{$customerDegree}}</option>
					@endforeach
				</select>

			</div>

		</div>
	
	
		<div class="col-xs-12 col-sm-12 col-md-2 form-input-container">

			<div class="form-group">

				<strong>رده شغلی:</strong>

				<select  name='business_type_id' id='business_type_id' class='form-control pull-right'>
					<option {{(!old('business_type_id', isset(request()->business_type_id) ? request()->business_type_id : '')? 'selected' : '')}} value="">انتخاب کنید</option>
					@foreach($businessTypes as $businessType)
						<option @if(old('business_type_id', isset(request()->business_type_id) ? request()->business_type_id : '')==$businessType->id) selected @endif value="{{$businessType->id}}" >{{$businessType->business_type_title}}</option>
					@endforeach
				</select>

			</div>

		</div>
		
		
		<div class="col-xs-12 col-sm-12 col-md-2 form-input-container">

			<div class="form-group">

				<strong>نوع جذب مشتری:</strong>

				<select  name='customer_recruitment_type' id='customer_recruitment_type' class='form-control pull-right'>
					 <option {{(!old('customer_recruitment_type', isset(request()->customer_recruitment_type) ? request()->customer_recruitment_type : '')? 'selected' : '')}} value="">انتخاب کنید</option>
					
					@foreach($enums['customer_recruitment_type'] as $key=>$customerRecruitmentType)
						<option @if(old('customer_recruitment_type', isset(request()->customer_recruitment_type) ? request()->customer_recruitment_type : '')==$key) selected @endif value="{{$key}}" >{{$customerRecruitmentType}}</option>
					@endforeach 
				</select>

			</div>

		</div>
		
		
		<div class="col-xs-12 col-sm-12 col-md-2 form-input-container">

			<div class="form-group">

				<strong>بازه زمانی:</strong>

				<select  name='report_period' id='report_period' class='form-control pull-right' autocomplete='off'>
				 <option @if(old('report_period', isset(request()->report_period) ? request()->report_period : '')==2) selected @endif value="2" >ماهانه</option>
				 <option @if(old('report_period', isset(request()->report_period) ? request()->report_period : '')==1) selected @endif value="1" >روزانه</option>
				</select>

			</div>

		</div>
			
			
			
			
			
			
			
	
        </div>
        
        <div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 form-input-container">
			<div class="pull-left">
			<button type="submit" class="btn btn-success">دریافت گزارش</button>
			</div>
		</div>
        </div>
        </form>
    </div>
</div>


  
<div class="row mt-5">   
    <div class="col-lg-12 mb-md-5">
    	<figure class="highcharts-figure">
		<div id="container"></div>
		
	</figure>
   </div>
</div>

@endsection


@section('additional_js_bottom')
<script>
function change_list(master_el_id,child_el_id,loading_gif_id,ajax_req_url,depend_element_ids = []){

    var e = document.getElementById(master_el_id);
    r_id = e.options[e.selectedIndex].value;

    if(r_id==""){
        $('#'+child_el_id).html('');
        return;
    }

    $('#'+master_el_id).prop('disabled', true);
    //$('#'+loading_gif_id).css('display', 'block'); //TODO: enable loadings

    token= $('body').find("input[name='_token']").val();

    $.ajax({
        url: ajax_req_url,
        type: 'GET',
        data: {"r_id":r_id, "_token":token },
        success: function(data){

            $('#'+child_el_id).html('');
            html_str='<option value="" >انتخاب کنید</option>';
            $('#'+child_el_id).append(html_str);
            $.each(data, function( index, value ) {
                html_str='<option value="'+value.id+'" >'+value.title+'</option>';
                $('#'+child_el_id).append(html_str);
            });

            depend_element_ids.forEach(function(){
                $('#'+depend_element_ids).empty();
            });

            $('#'+master_el_id).prop('disabled', false);
            //$('#'+loading_gif_id).css('display', 'none');
        },
        error: function(data){
            alert(data.responseText);
            alert('دریافت اطلاعات با خطا مواجه شد!');
            $('#'+master_el_id).prop('disabled', false);
            //$('#'+loading_gif_id).css('display', 'none');
        }
    });

}


</script>



<script type="text/javascript">
Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'نمودار تعداد کسب و کارهای ثبت شده'
    },
    xAxis: {
        categories: [
        		"تاریخ"
        		],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'تعداد کسب و کار'
        }
    },
    tooltip: {
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: [
    
    	@foreach($chart1DataArray as $key=>$chartData)
    		{
    			name: '{{$key}}',
    			data: [{{$chartData}}]
    		}
    		@if($key!=array_key_last($chart1DataArray))
    		,
    		@endif
    	@endforeach
    
    ]
});

</script>


@endsection



