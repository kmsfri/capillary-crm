@extends('panel.layout.master')
@section('additional_css')


<style>

.create-invoice-col a{
  display:block;
}

</style>
    
@stop
@section('main_content')


    <div class="row mb-md-5">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2>کسب و کارها</h2>

            </div>

            <div class="pull-left">

                @can('business-create')

                    <a class="btn btn-success" href="{{ route('businesses.create') }}"> ثبت کسب و کار جدید</a>

                @endcan

            </div>
			
			
			

        </div>

    </div>


    @if ($message = Session::get('success'))

        <div class="alert alert-success">

            <p>{{ $message }}</p>

        </div>

    @endif
    
    @if ($message = Session::get('danger'))

        <div class="alert alert-danger">

            <p>{{ $message }}</p>

        </div>

    @endif

<div class="row">    
    <div class="col-lg-12 mb-md-5">
        <form action="{{route('businesses.index')}}" method="GET">
        <div class="row">
	    <div class="col-xs-12 col-sm-12 col-md-2 form-input-container">

		<div class="form-group">

		    <strong>استان:</strong>

		    <select  name='state_id' id='state_id' class='form-control pull-right' onChange="change_list('state_id','city_id','loading_gif_cities','{{route('getCities')}}',['region_id'])"> 
			<option {{(!old('state_id', isset(request()->state_id) ? request()->state_id : '')? 'selected' : '')}} value="">انتخاب کنید</option>
			@foreach($states as $state)
			    <option @if(old('state_id', isset(request()->state_id) ? request()->state_id : '')==$state->id) selected @endif value="{{$state->id}}" >{{$state->unit_name}}</option>
			@endforeach
		    </select>

		</div>

	    </div>
	
	
	    <div class="col-xs-12 col-sm-12 col-md-2 form-input-container">

			<div class="form-group">

			    <strong>شهر:</strong>

			    <select  name='city_id' id='city_id' class='form-control pull-right'>
				@if($cities)
				    <option {{(!old('city_id', isset(request()->city_id) ? request()->city_id : '')? 'selected' : '')}} value="">انتخاب کنید</option>
				    @foreach($cities as $city)
					<option @if(old('city_id', isset(request()->city_id) ? request()->city_id : '')==$city->id) selected @endif value="{{$city->id}}" >{{$city->unit_name}}</option>
				    @endforeach
				@endif
			    </select>

			</div>

	    </div>
	
	
	    <div class="col-xs-12 col-sm-12 col-md-2 form-input-container">

                <div class="form-group">

                    <strong>درجه بندی مشتری:</strong>

                    <select  name='customer_degree' id='customer_degree' class='form-control pull-right'>
                    	 <option {{(!old('customer_degree', isset(request()->customer_degree) ? request()->customer_degree : '')? 'selected' : '')}} value="">انتخاب کنید</option>
                        @foreach($enums['customer_degree'] as $key=>$customerDegree)
                            <option @if(old('customer_degree', isset(request()->customer_degree) ? request()->customer_degree : '')==$key) selected @endif value="{{$key}}" >{{$customerDegree}}</option>
                        @endforeach
                    </select>

                </div>

            </div>
        
        
            <div class="col-xs-12 col-sm-12 col-md-2 form-input-container">

                <div class="form-group">

                    <strong>رده شغلی:</strong>

                    <select  name='business_type_id' id='business_type_id' class='form-control pull-right'>
                        <option {{(!old('business_type_id', isset(request()->business_type_id) ? request()->business_type_id : '')? 'selected' : '')}} value="">انتخاب کنید</option>
                        @foreach($businessTypes as $businessType)
                            <option @if(old('business_type_id', isset(request()->business_type_id) ? request()->business_type_id : '')==$businessType->id) selected @endif value="{{$businessType->id}}" >{{$businessType->business_type_title}}</option>
                        @endforeach
                    </select>

                </div>

            </div>
            
            
            <div class="col-xs-12 col-sm-12 col-md-2 form-input-container">

                <div class="form-group">

                    <strong>نوع جذب مشتری:</strong>

                    <select  name='customer_recruitment_type' id='customer_recruitment_type' class='form-control pull-right'>
                    	 <option {{(!old('customer_recruitment_type', isset(request()->customer_recruitment_type) ? request()->customer_recruitment_type : '')? 'selected' : '')}} value="">انتخاب کنید</option>
                        
                        @foreach($enums['customer_recruitment_type'] as $key=>$customerRecruitmentType)
                            <option @if(old('customer_recruitment_type', isset(request()->customer_recruitment_type) ? request()->customer_recruitment_type : '')==$key) selected @endif value="{{$key}}" >{{$customerRecruitmentType}}</option>
                        @endforeach 
                    </select>

                </div>

            </div>
            
            
            
            
            <div class="col-xs-12 col-sm-12 col-md-2 form-input-container">

                <div class="form-group">

                    <strong>میزان دپو(ریالی):</strong>

                    <input type="text" name="assets_estimate_rial" id="assets_estimate_rial" value="{{ old('assets_estimate_rial',request()->assets_estimate_rial) }}" class="form-control" placeholder="" onKeyPress="return NumbersOnly(event, 'assets_estimate_rial')">

                </div>

            </div>
            
            
            <div class="col-xs-12 col-sm-12 col-md-4 form-input-container">

                <div class="form-group">

                    <strong>عنوان:</strong>

                    <input type="text" name="search_key" id="search_key" value="{{ old('search_key',request()->search_key) }}" class="form-control" placeholder="بخشی از نام، نام خانوادگی، عنوان یا شماره تلفن">

                </div>

            </div>
	
	
        </div>
        
        <div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 form-input-container">
			<div class="pull-left">
			<button type="submit" class="btn btn-success" href="{{ route('businesses.index') }}">فیلتر</button>
			</div>
			
			
			<div class="pull-left ml-3">
				<button type="button" class="btn btn-warning" onclick="exportExcel()">خروجی اکسل</button>
				<script>
					function exportExcel(){
						const urlParams = new URLSearchParams(window.location.search);

						urlParams.set('export_excel', 1);

						window.location.search = urlParams;
					}
				</script>
			</div>
			
			
		</div>
        </div>
        </form>
    </div>
</div>


    <table class="table table-bordered">

        <tr>

            <th  class="text-center">ردیف</th>

            <th>نام و نام خانوادگی</th>

            <th>نام فروشگاه</th>

            <th>استان - شهر - منطقه</th>
            <th  class="text-center">کد مشتری</th>
            
            <th class="text-center">ثبت</th>

	    <th>تایید کارشناس فروش</th>
            
            <th>تایید حسابدار فروش</th>
            
            @can('send-specialist-info-by-sms')
            <th>ارسال اطلاعات</th>
            @endcan
            
            @can('set-alarm-on-customers')
            <th>آلارم</th>
            @endcan
            
            
            <th width="280px" class="text-center">عملیات</th>

        </tr>

        @foreach ($businesses as $business)

            <tr>

                <td  class="text-center">{{ ++$i }}</td>

                <td>
                
                @can('show-business-cartable')
                <a target="_blank" class="" href="{{ route('businesses.showCartable',['business_id'=>$business->id]) }}">
                @endcan
                
                	{{ $business->first_name.' '.$business->last_name }}
                	
                @can('show-business-cartable')
                </a>
                @endcan
                </td>

                <td>{{ $business->business_name }}</td>
                <td>{{ $business->getFullStateCityRegion() }}</td>

                <td  class="text-center">{{ $business->id }}</td>


                <td class="text-center create-invoice-col">
                    @can('invoice-create')

                        <a class="" href="{{ route('invoices.create',['business_id'=>$business->id]) }}">ﺻﺪﻭﺭ پیش فاکتور رسمی</a>

                    @endcan
                    @can('invoice-create')

                        <a class="" href="{{ route('invoices.unofficial.create',['business_id'=>$business->id]) }}">ﺻﺪﻭﺭ پیش فاکتور غیررسمی</a>

                    @endcan
                    
                    @can('invoice-create')

                        <a class="" href="{{ route('invoices.returned.create',['business_id'=>$business->id]) }}">ﺻﺪﻭﺭ فاکتور مرجوعی</a>

                    @endcan
					
					@can('management-business-brands')

                        <a class="" href="{{ route('brands.editBusinessBrands',['business_id'=>$business->id]) }}">ثبت قیمت برندها</a>

                    @endcan
                </td>
                
                 <td class="text-center">
                	@if($business->approved_by_sales_specialist==0)
                		@can('business-sales-specialist-approve')
                		<a class="btn btn-primary" href="{{ route('business.show-sales-specialist-approving-form',['business_id'=>$business->id]) }}">تایید کارشناس فروش</a>
                		@else
                		<label class="badge badge-warning">عدم تعیین وضعیت</label>
                		@endcan
                	@elseif($business->approved_by_sales_specialist==1)
                	<label class="badge badge-success">تایید شده</label>
                	@elseif($business->approved_by_sales_specialist==2)
                	<label class="badge badge-danger">رد شده</label>
                	@endif
                </td>
                
                <td class="text-center">
                	@if($business->approved_by_accountant==0)
                		@can('business-accounting-approve')
                		<a class="btn btn-primary {{($business->approved_by_sales_specialist==0)?'disabled':''}}"
                		{{($business->approved_by_sales_specialist==0)?'disabled':''}}
                		href="{{ ($business->approved_by_sales_specialist==0)?'#':route('business.show-accounting-approving-form',['business_id'=>$business->id]) }}">تایید حسابداری فروش</a>
                		@else
                		<label class="badge badge-warning">عدم تعیین وضعیت</label>
                		@endcan
                	@elseif($business->approved_by_accountant==1)
                	<label class="badge badge-success">تایید شده</label>
                	@elseif($business->approved_by_accountant==2)
                	<label class="badge badge-danger">رد شده</label>
                	@endif
                </td>
                
                @can('send-specialist-info-by-sms')
		<td>
			<a href="{{ route('sms.sendSpecialistInfo',['business_id'=>$business->id]) }}" onclick="return confirm('آیا مطمئنید؟')">پیامک اطلاعات کارشناس</a>
		</td>
		@endcan
                

		@can('set-alarm-on-customers')
		<td>
			<a href="{{ route('specialists.set-alarm-to-specialist-daily-call-target',['business_id'=>$business->id]) }}" onclick="return confirm('آیا مطمئنید؟')">تنظیم آلارم</a>
		</td>
		@endcan


                <td  class="text-center">

                    

                    <form action="{{ route('businesses.destroy',$business->id) }}" method="POST">

                        <a class="btn btn-info" href="{{ route('businesses.show',$business->id) }}">نمایش</a>

                        @can('business-edit')

                            <a class="btn btn-primary" href="{{ route('businesses.edit',$business->id) }}">ویرایش</a>

                        @endcan


                        @csrf

                        @method('DELETE')

                        @can('business-delete')

                            <button type="submit" class="btn btn-danger">حذف</button>

                        @endcan

                    </form>

                </td>

            </tr>

        @endforeach

    </table>


    {!! $businesses->links() !!}



@endsection



@section('additional_js_bottom')
<script>
function change_list(master_el_id,child_el_id,loading_gif_id,ajax_req_url,depend_element_ids = []){

    var e = document.getElementById(master_el_id);
    r_id = e.options[e.selectedIndex].value;

    if(r_id==""){
        $('#'+child_el_id).html('');
        return;
    }

    $('#'+master_el_id).prop('disabled', true);
    //$('#'+loading_gif_id).css('display', 'block'); //TODO: enable loadings

    token= $('body').find("input[name='_token']").val();

    $.ajax({
        url: ajax_req_url,
        type: 'GET',
        data: {"r_id":r_id, "_token":token },
        success: function(data){

            $('#'+child_el_id).html('');
            html_str='<option value="" >انتخاب کنید</option>';
            $('#'+child_el_id).append(html_str);
            $.each(data, function( index, value ) {
                html_str='<option value="'+value.id+'" >'+value.title+'</option>';
                $('#'+child_el_id).append(html_str);
            });

            depend_element_ids.forEach(function(){
                $('#'+depend_element_ids).empty();
            });

            $('#'+master_el_id).prop('disabled', false);
            //$('#'+loading_gif_id).css('display', 'none');
        },
        error: function(data){
            alert(data.responseText);
            alert('دریافت اطلاعات با خطا مواجه شد!');
            $('#'+master_el_id).prop('disabled', false);
            //$('#'+loading_gif_id).css('display', 'none');
        }
    });

}


function NumbersOnly(evt, label) {
	var status = document.getElementById(label);
	evt = (evt) ? evt : window.event;
	var charCode = (evt.which) ? evt.which : evt.keyCode;
	if (charCode == 46) {
	    return true;
	}
	if (charCode < 48 || charCode > 57) {
	    status.innerHTML = 'Numbers Only Please';
	    return false;
	}
	status.innerHTML = '';
	return true;
}

</script>


@endsection
