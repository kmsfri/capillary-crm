@extends('panel.layout.master')
@section('main_content')


    <div class="row">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2>مدیریت صورتجلسه ها</h2>

            </div>

            <div class="pull-left">

                @can('proceeding-create')

                    <a class="btn btn-success" href="{{ route('proceedings.create') }}"> اضافه کردن صورتجلسه جدید</a>

                @endcan

            </div>

        </div>

    </div>


    @if ($message = Session::get('success'))

        <div class="alert alert-success">

            <p>{{ $message }}</p>

        </div>

    @endif


    <table class="table table-bordered">

        <tr>

            <th>ردیف</th>

            <th>عنوان صورتجلسه</th>

            <th>تاریخ برگزاری جلسه</th>
            
            <th>فایل صورتجلسه</th>
            
            <th width="280px">عملیات</th>

        </tr>

        @foreach ($proceedings as $proceeding)

            <tr>

                <td>{{ ++$i }}</td>

                <td>{{ $proceeding->proceeding_title }}</td>

                <td>{{ \Morilog\Jalali\Jalalian::forge($proceeding->hold_at)->format('%A, %d %B %Y') }}</td>
                
                <td><a href="{{ asset($proceeding->file_path) }}" target="_blank">دانلود فایل</a></td>
                
               

                <td>

                    <form action="{{ route('proceedings.destroy',$proceeding->id) }}" method="POST">

                        <a class="btn btn-info" href="{{ route('proceedings.show',$proceeding->id) }}">نمایش</a>

                        @can('proceeding-edit')

                            <a class="btn btn-primary" href="{{ route('proceedings.edit',$proceeding->id) }}">ویرایش</a>

                        @endcan


                        @csrf

                        @method('DELETE')

                        @can('proceeding-delete')

                            <button type="submit" class="btn btn-danger">حذف</button>

                        @endcan

                    </form>

                </td>

            </tr>

        @endforeach

    </table>


    {!! $proceedings->links() !!}



@endsection
