@extends('panel.layout.master')
@section('main_content')

    <div class="row">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2>ویرایش اطلاعات صورتجلسه</h2>

            </div>

            <div class="pull-left">

                <a class="btn btn-primary" href="{{ route('proceedings.index') }}"> بازگشت</a>

            </div>

        </div>

    </div>


    @if ($errors->any())

        <div class="alert alert-danger">

            <strong>خطا!</strong> در مقادیر ورودی ایراداتی وجود دارد.<br><br>

            <ul>

                @foreach ($errors->all() as $error)

                    <li>{{ $error }}</li>

                @endforeach

            </ul>

        </div>

    @endif


    <form action="{{ route('proceedings.update',$proceeding->id) }}" method="POST">

        @csrf

        @method('PUT')


        <div class="row">
		
		
		<div class="col-xs-12 col-sm-12 col-md-6 form-input-containerform-input-container">

			<div class="form-group">

				<strong>عنوان جلسه:</strong>

				<input type="text" name="proceeding_title" value="{{ old('proceeding_title',($proceeding->proceeding_title??'') ) }}" class="form-control" placeholder="">

			</div>

		</div>
		
		</div>
		<div class="row">
	
		
		<div class="col-xs-12 col-sm-12 col-md-6 form-input-container">

			<div class="form-group">

				<strong>تاریخ جلسه:</strong>

				<input data-jdp type="text" name="hold_at" value="{{ old('hold_at',($proceeding->hold_at??'') ) }}" class="form-control" placeholder="">

			</div>

		</div>
		
		</div>
		<div class="row">
	
		<div class="col-xs-12 col-sm-12 col-md-6 form-input-container mb-5">

			<div class="form-group">

				<strong>فایل صورتجلسه:</strong>

				<input type="file" name="hold_file" id="hold_file" value="" autocomplete="off">

			</div>

		</div>
		
		</div>
		<div class="row">
		
		<div class="col-xs-12 col-sm-12 col-md-6 mt-md-2">

			<div class="form-group">

				<strong>توضیحات جلسه:</strong>

				<textarea class="form-control" name="hold_description" placeholder="" >{{old('hold_description',($proceeding->hold_description??'') )}}</textarea>

			</div>
 
		</div>
        
        
      
	
		</div>
		<div class="row">

		<div class="col-xs-12 col-sm-12 col-md-6 text-center">

			<button type="submit" class="btn btn-primary">ثبت تغییرات</button>

		</div>

        </div>

    </form>



@endsection