@extends('panel.layout.master')
@section('additional_css')


<style>

.create-invoice-col a{
  display:block;
}

</style>
    
@stop

@section('additional_js_top2')

<script src="{{asset('highcharts/highcharts.js')}}"></script>
<script src="{{asset('highcharts/modules/exporting.js')}}"></script>
<script src="{{asset('highcharts/modules/export-data.js')}}"></script>
<script src="{{asset('highcharts/modules/accessibility.js')}}"></script>

@endsection
@section('main_content')


    <div class="row mb-md-5">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2>کارتابل فعالیتهای کاربر کارشناس فروش: {{$specialistUser->name."(".$specialistUser->email.")"}}</h2>

            </div>

            <div class="pull-left">


            </div>

        </div>

    </div>
    
    
    @if ($message = Session::get('success'))

        <div class="alert alert-success">

            <p>{{ $message }}</p>

        </div>

    @endif
    
    @if ($message = Session::get('danger'))

        <div class="alert alert-danger">

            <p>{{ $message }}</p>

        </div>

    @endif
    
    
    
    <div class="row">    
    <div class="col-lg-12 mb-md-5">
        <form action="{{route('specialists.show-specialist-report',$specialistUser->id)}}" method="GET">
        <div class="row">
	    
            
            
     
            
            
            <div class="col-xs-12 col-sm-12 col-md-2 form-input-container">

                <div class="form-group">

                    <strong>از تاریخ:</strong>

                    <input data-jdp type="text" name="from_date" id="from_date" value="{{ old('from_date',request()->from_date) }}" class="form-control" placeholder="">

                </div>

            </div>
            
            
            <div class="col-xs-12 col-sm-12 col-md-2 form-input-container">

                <div class="form-group">

                    <strong>تا تاریخ:</strong>
                    
                    <input data-jdp type="text" name="to_date" id="to_date" value="{{ old('to_date',request()->to_date) }}" class="form-control" placeholder="">

                </div>

            </div>
 
            
	
        </div>
        
        <div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 form-input-container">
			<div class="pull-left">
			<button type="submit" class="btn btn-success">فیلتر فعالیت‌ها</button>
			</div>
		</div>
        </div>
        </form>
    </div>
	</div>
    
   
   
   
   
<!-- BEGIN BUSINESS INFORMATION -->


<div class="row mt-3 mb-3">

<div class="col-lg-12">


<div class="card border-success">
<div class="card-header">اطلاعات کلی</div>
<div class="card-body">
				
				
				
				
				
				
				
				
				
				
			
				
<table class="table table-bordered">



<tr>
	<td>
		<strong>نام:</strong>
                {{ $specialistUser->name }}
	</td>
	<td>
		<strong>ایمیل:</strong>
                {{ $specialistUser->email }}
	</td>
</tr>

<tr>
	
	<td>
		<strong>شماره voip:</strong>
		{{!empty($specialistUser->voip_id)?$specialistUser->voip_id:'-'}}
                
	</td>
	
	<td>
		<strong>تعداد ویزیتورها:</strong>
                {{ !empty($subUsersCount)?$subUsersCount:'-' }}
	</td>
</tr>



<tr>
	
	<td>
		<strong>تعداد سرنخ ها:</strong>
		{{!empty($clues)?$clues->total():0}}
                
	</td>
	
	<td>
		<strong>تعداد فاکتورها:</strong>
		{{!empty($invoices)?count($invoices):0}}
                
	</td>
</tr>



<tr>
	
	<td>
		<strong>تعداد کسب و کارها:</strong>
		{{!empty($businesses)?$businesses->total():0}}
                
	</td>
	
	<td>
		<strong>تعداد تماسها:</strong>
		{{!empty($userCalls)?$userCalls->total():0}}
                
	</td>
</tr>

<tr>
	
	<td>
		<strong>تعداد یادداشتها:</strong>
		{{!empty($userNotes)?$userNotes->total():0}}
                
	</td>
	
	<td> 
		<strong>تعداد آلارم‌ها</strong>
		{{!empty($userAlarms)?$userAlarms->total():0}}
	</td>
</tr>


<tr>
	<td>
		<strong>زمان ایجاد کاربر کارشناس فروش:</strong>
                {{\Morilog\Jalali\Jalalian::forge($specialistUser->created_at)->format('%A, %d %B %Y در ساعت H:i:s')}}
	</td>
	<td>
		<strong>زمان آخرین ویرایش کاربر کارشناس فروش:</strong>
                {{\Morilog\Jalali\Jalalian::forge($specialistUser->updated_at)->format('%A, %d %B %Y در ساعت H:i:s')}}
	</td>
</tr>



</table>
				
							
	
	
</div>
</div>

</div>

</div>


<!-- END CUSTOMER INFORMATION -->
   
   
   






<!-- BEGIN CLUES -->




<div class="row mt-3 mb-3">

<div class="col-lg-12">


<div class="card border-default">
<div class="card-header">سوابق سرنخ های ایجاد شده</div>
<div class="card-body">
				
				
				
				
@if(count($clues)==0)
	<p class="card-text">هیچ سرنخی توسط این کارشناس ایجاد نشده است</p>
@else				
				
	<table class="table table-bordered">

        <tr>

            <th>ردیف</th>

            <th>عنوان سرنخ</th>
            <th>وضعیت</th>
			<th>نتیجه</th>
            <th>کاربر ایجاد کننده</th>
			<th>تاریخ ایجاد</th>
			<th>تاریخ آخرین بروزرسانی</th>
            
            
        </tr>
		@php
		$i=0;
		@endphp
        @foreach ($clues as $key => $clue)

            <tr>

                <td>{{ ++$i }}</td>

                <td>
                
                
					<a target="_blank" class="" href="{{ route('clues.showCartable',['clue_id'=>$clue->id]) }}">
					
					
					{{ $clue->clue_title }}
					
					
					</a>
                
                </td>
				
				
				
				<td>
				@php
					
					$clueTitlesArr = [
						'clue' => 'ثبت سرنخ',
						'customer' => 'ثبت مشتری',
						'validation' => 'اعتبارسنجی',
						'sale' => 'فروش',
						'pre_invoice'=>'‍‍پیش فاکتور',
						'invoice'=>'فاکتور',
						'return_from_sale' => 'برگشت از فروش'
					];
					$clueStatusTitle = $clueTitlesArr[$clue->clue_status];
				@endphp
				{{ $clueStatusTitle }}
				</td>
				
				
				
				<td>
				@php
					
					$clueClosedArr = [
						0 => 'باز',
						1 => 'بسته',
					];
					$clueClosedTitle = $clueClosedArr[$clue->clue_closed];
				@endphp
				@php
					$class="badge badge-success";
					if($clue->clue_closed==1){
						$class="badge badge-danger";
					}
				@endphp
				<span class="{{$class}}">
				{{ $clueClosedTitle }}
				</span>
				</td>
				
				
				
				<td>
                {{$clue->creator()->first()->name;}}
                </td>
				
				<td>{{ \Morilog\Jalali\Jalalian::forge($clue->created_at)->format('%A, %d %B %Y در ساعت H:i:s') }}</td>
				
				<td>{{ !empty($clue->updated_at)?(\Morilog\Jalali\Jalalian::forge($clue->updated_at)->format('%A, %d %B %Y در ساعت H:i:s')):'-' }}</td>
				
				
				
                



            </tr>

        @endforeach

    </table>
		{!! $clues->appends(request()->input())->links() !!}				
				
@endif			
				
				
				
	
	
</div>
</div>

</div>

</div>










<!-- END CLUES -->











<!-- BEGIN CLUE CHARTS -->




<div class="row mt-3 mb-3">

<div class="col-lg-12">


<div class="card border-default">
<div class="card-header">گزارش آماری سرنخ های کارشناس</div>
<div class="card-body">
				
				
				
	<div class="row mt-5">   
		<div class="col-lg-12 mb-md-5">
		<figure class="highcharts-figure">
			<div id="container"></div>
			
		</figure>
	   </div>
	</div>


	<div class="row mt-5">   
	   <div class="col-xs-12 col-sm-12 col-md-6">
		<figure class="highcharts-figure">
			<div id="chartClosedClues"></div>
			
		</figure>
	   </div>
	   
	   
	   <div class="col-xs-12 col-sm-12 col-md-6">
		<figure class="highcharts-figure">
			<div id="chartClueStatusGrouped"></div>
			
		</figure>
	   </div>
	   
	   
	   
	   
	</div>			
	
	
</div>
</div>

</div>

</div>










<!-- END CLUE CHARTS -->









<!-- BEGIN USER NOTES -->


<div class="row mt-3 mb-3">

<div class="col-lg-12">


<div class="card border-success">
<div class="card-header">یادداشتهای ثبت شده توسط کارشناس</div>
<div class="card-body">
				
	<form action="{{ route('specialists.addNewNote',$specialistUser->id) }}" method="POST">
        	@csrf
		<div class="form-group">

			<textarea class="form-control" name="note_text" placeholder="" rows="5">{{old('note_text')}}</textarea>

		</div>
		
		<div class="col-xs-12 col-sm-12 col-md-12 text-center">

			<button type="submit" class="btn btn-primary">ثبت یادداشت</button>

		</div>
		
	</form>
	
	
	
	
	
	
	
	
	
					
	@if(count($userNotes)==0)
		<p class="card-text text-center">هنوز هیچ یادداشتی برای این کاربر کارشناس فروش ثبت نشده است</p>
	@else				
					
	<table class="table table-bordered mt-5">

	<tr>
		<th>ردیف</th>
		<th style="width:56%">متن یادداشت</th>
		<td style="width:10%">منبع</td>
		<td style="width:10%">ارجاع توسط</td>
		<td style="width:12%">تاریخ ثبت</td>
		<td style="width:12%">تاریخ آخرین ویرایش</td>
	</tr>




	@php
	$counter = 0;
	@endphp
	@foreach ($userNotes as $note)


	    <tr>

		<td>{{++$counter}}</td>

		<td>
		
		@if($note->creator()->first()->id==auth()->user()->id)
			<div class="form-group">
				<form action="{{route('specialists.updateNote',auth()->user()->id)}}" method="POST">
					@csrf
					<input type="hidden" name="note_id" value="{{$note->id}}">
					<textarea name="cartable_row_note_text" class="form-control">{{$note->note_text}}</textarea>
					<button type="submit" class="btn btn-success btn-small pull-left mt-1">بروزرسانی</button>
				</form>
			</div>
		@else
			{{$note->note_text}}
		@endif
		
		</td>
		
		<td>
		
		@if(!empty($note->clue_id))
			<a href="{{route('clues.showCartable',$note->clue_id)}}" target="_blank">{{$note->clue()->first()->clue_title}}({{$note->clue()->first()->first_name}} {{$note->clue()->first()->last_name}})</a>
		@endif
		
		@if(!empty($note->business_id))
			<a href="{{route('businesses.showCartable',$note->business_id)}}" target="_blank">کارتابل کسب و کار</a>
		@endif
		
		</td>
		
		<td>{{ !empty($note->referencedBy()->first())?$note->referencedBy()->first()->name:
				(
					!empty($note->creator()->first())?$note->creator()->first()->name:'-'
				)}}</td>


		<td>{{ \Morilog\Jalali\Jalalian::forge($note->created_at)->format('%Y/%m/%d H:i:s') }}</td>
		
		<td>{{ \Morilog\Jalali\Jalalian::forge($note->updated_at)->format('%Y/%m/%d H:i:s') }}</td>


	    </tr>

	@endforeach

	</table>
	{!! $userNotes->appends(request()->input())->links() !!}		
					
	@endif	
	
	
	
	
	
	
	
	
	
	
					
							
	
	
</div>
</div>

</div>

</div>


<!-- END CUSTOMER NOTES -->





   
   
<!-- BEGIN ALARMS -->



<div class="row mt-3 mb-3">

<div class="col-lg-12">


<div class="card border-success">
<div class="card-header">سوابق آلارمهای ثبت شده برای کارشناس فروش</div>
<div class="card-body">
				
				
				
				
				
				
				
				
				
				
@if(count($userAlarms)==0)
	<p class="card-text">هیچ آلارمی برای این کسب و کار ثبت نشده است</p>
@else				
				
<table class="table table-bordered">

<tr>
	<th>توضیحات</th>
	<td>تاریخ یادآوری</td>
</tr>





@foreach ($userAlarms as $alarm)

	@php
		$rowClass = "";
		if($alarm->is_seen==1){

			$rowClass = 'success';

		}elseif(\Morilog\Jalali\Jalalian::forge($alarm->alarm_at)->isPast()){
			$rowClass = 'danger';
		}
	@endphp



    <tr class="{{$rowClass}}">



	<td>{{$alarm->alarm_description}}</td>


	<td>{{ \Morilog\Jalali\Jalalian::forge($alarm->alarm_at)->format('%A, %d %B %Y در ساعت H:i:s') }}</td>


    </tr>

@endforeach

</table>
				
	{!! $userAlarms->appends(request()->input())->links() !!}					
@endif			
				
				
				
	
	
</div>
</div>

</div>

</div>


<!-- END ALARMS --> 
   
   
   
   
   
   
   
   
   
<div class="row mt-3 mb-3">

<div class="col-lg-12">


<div class="card border-default">
<div class="card-header">سوابق فاکتورهای صادر شده توسط کاربر کارشناس فروش</div>
<div class="card-body">
				
				
				
				
@if(count($invoices)==0)
	<p class="card-text">این کارشناس فروش تاکنون هیچ فاکتوری صادر نکرده است</p>
@else				
				
<table class="table table-bordered">

<tr>
    <th>ثبت کننده</th>

    <th>تاریخ ثبت</th>
    
    <th>نوع</th>
    
    <th>ﻋﻤﻠﯿﺎﺕ</th>

</tr>

@foreach ($invoices as $invoice)

    <tr>

        

        <td>{{ $invoice->creator()->first()->name }}</td>
        
        <td>{{ \Morilog\Jalali\Jalalian::forge($invoice->created_at)->format('%A, %d %B %Y در ساعت H:i:s') }}</td>

	@php
		$invoiceGetPDFRoute  = "#";
		$invoiceEditRoute  = "#";
		$invoiceTitle = "";
		if($invoice->invoice_type=="official"){
			$invoiceGetPDFRoute  = route('invoices.get-pdf',['invoice_id'=>$invoice->id]);
			$invoiceEditRoute  = route('invoices.edit',$invoice->id);
			$invoiceTitle = "رسمی";
		}else if($invoice->invoice_type=="unofficial"){
			$invoiceGetPDFRoute  = route('invoices.unofficial.get-pdf',['invoice_id'=>$invoice->id]);
			$invoiceEditRoute  = route('invoices.unofficial.edit',$invoice->id);
			$invoiceTitle = "غیررسمی";
		}
	@endphp
	
	<td>{{ $invoiceTitle }}</td>

        <td>
        
        	
        
		<a target="_brank" class="btn btn-warning" href="{{ $invoiceGetPDFRoute }}">دریافت</a>


        </td>

    </tr>

@endforeach

</table>
				
				
@endif			
				
				
				
	
	
</div>
</div>

</div>

</div>






















<div class="row mt-3 mb-3">

<div class="col-lg-12">


<div class="card border-default">
<div class="card-header">سوابق کسب و کارهای ایجاد شده توسط کارشناس</div>
<div class="card-body">
				
				
				
				
@if(count($businesses)==0)
	<p class="card-text">این کارشناس تاکنون هیچ کسب و کاری ایجاد نکرده است</p>
@else				
				
	<table class="table table-bordered">

        <tr>

            <th  class="text-center">ردیف</th>

            <th>نام و نام خانوادگی</th>

            <th>نام فروشگاه</th>

            <th>استان - شهر - منطقه</th>
            <th  class="text-center">کد مشتری</th>
            
			<th>تایید کارشناس فروش</th>
            
            <th>تایید حسابدار فروش</th>


        </tr>

		@php
			$i=0;
		@endphp
        @foreach ($businesses as $business)

            <tr>

                <td  class="text-center">{{ ++$i }}</td>

                <td>
                
                @can('show-business-cartable')
                <a target="_blank" class="" href="{{ route('businesses.showCartable',['business_id'=>$business->id]) }}">
                @endcan
                
                	{{ $business->first_name.' '.$business->last_name }}
                	
                @can('show-business-cartable')
                </a>
                @endcan
                </td>

                <td>{{ $business->business_name }}</td>
                <td>{{ $business->getFullStateCityRegion() }}</td>

                <td  class="text-center">{{ $business->id }}</td>


                
                 <td class="text-center">
                	@if($business->approved_by_sales_specialist==0)
                		<label class="badge badge-warning">عدم تعیین وضعیت</label>
                	@elseif($business->approved_by_sales_specialist==1)
                	<label class="badge badge-success">تایید شده</label>
                	@elseif($business->approved_by_sales_specialist==2)
                	<label class="badge badge-danger">رد شده</label>
                	@endif
                </td>
                
                <td class="text-center">
                	@if($business->approved_by_accountant==0)
                		<label class="badge badge-warning">عدم تعیین وضعیت</label>
                	@elseif($business->approved_by_accountant==1)
                	<label class="badge badge-success">تایید شده</label>
                	@elseif($business->approved_by_accountant==2)
                	<label class="badge badge-danger">رد شده</label>
                	@endif
                </td>
                





            </tr>

        @endforeach

    </table>
	{!! $businesses->appends(request()->input())->links() !!}		
				
@endif			
				
				
				
	
	
</div>
</div>

</div>

</div>



















<!-- BEGIN CALLS HISTORY -->













<div class="row mt-3 mb-3">

<div class="col-lg-12">


<div class="card border-success">
<div class="card-header">سوابق تماسها</div>
<div class="card-body">
				
				
				
				
				
				
				
				
				
				
@if(count($userCalls)==0)
	<p class="card-text">هیچ تماسی برای این کارشناس ثبت نشده است</p>
@else				
				
<table class="table table-bordered">

<tr>
	<th>ردیف</th>
    <th>تاریخ تماس</th>
    <th>داخلی تماس گیرنده</th>
    <th>نام تماس گیرنده</th>
    <th>مقصد</th>
    <th>مدت زمان</th>
    <th>وضعیت پاسخ</th>

</tr>



@php
$dispositionValues = [
	'NO ANSWER' => ['title'=>'پاسخ داده نشده','class'=>'warning'],
	'BUSY' => ['title'=>'مشغول','class'=>'info'],
	'ANSWERED' => ['title'=>'پاسخ داده شده','class'=>'success'],
	'CONGESTION' => ['title'=>'ترافیک','class'=>'default'],
	'FAILED' => ['title'=>'ناموفق','class'=>'danger']
];
@endphp

@php
$i=0;
@endphp


@foreach ($userCalls as $call)

    <tr>
		<td>{{++$i}}</td>
        <td>{{\Morilog\Jalali\Jalalian::forge($call->calldate)->format('%A, %d %B %Y در ساعت H:i:s')}}</td>
        <td>{{$call->src}}</td>
        <td>
        @php
        $crmUser = \App\Models\User::where('voip_id',$call->src)->first();
        @endphp
        @if(!empty($crmUser->name))
        {{$crmUser->name}}
        @else
        {{$call->src}}
        @endif
        </td>
        <td>
        
        
        @php
        $crmUser = \App\Models\User::where('voip_id',$call->dst)->first();
        @endphp
        @if(!empty($crmUser->name))
        {{$crmUser->name}}
        @else
        {{$call->dst}}
        @endif
        
        </td>
        <td>{{$call->duration}} ثانیه</td>
        <td><label class="badge badge-{{$dispositionValues[$call->disposition]['class']}}">{{$dispositionValues[$call->disposition]['title']}}</label></td>	

    </tr>

@endforeach

</table>
	{!! $userCalls->appends(request()->input())->links() !!}			
				
@endif			
				
				
				
	
	
</div>
</div>

</div>

</div>


   
   
   
   
   




@endsection





@section('additional_js_bottom')



<script type="text/javascript">
Highcharts.chart('container', {
    chart: {
        type: 'column',
		plotShadow: true
    },
    title: {
        text: 'نمودار تعداد سرنخ های ثبت شده'
    },
    xAxis: {
        categories: [
        		"تاریخ"
        		],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'تعداد سرنخ ها'
        }
    },
    tooltip: {
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: [
    
    	@foreach($chart1DataArray as $key=>$chartData)
    		{
    			name: '{{$key}}',
    			data: [{{$chartData}}]
    		}
    		@if($key!=array_key_last($chart1DataArray))
    		,
    		@endif
    	@endforeach
    
    ]
});













Highcharts.chart('chartClosedClues', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: 0,
        plotShadow: true
    },
	
    title: {
        text: 'نتیجه سرنخ ها(باز/بسته)',
        align: 'center'
        //verticalAlign: 'middle',
        //y: 60
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    accessibility: {
        point: {
            valueSuffix: '%'
        }
    },
    plotOptions: {
        pie: {
            dataLabels: {
                enabled: true,
                distance: -50,
                style: {
                    fontWeight: 'bold',
                    color: 'white',
					fontSize: "16px"
                }
            },
            startAngle: -90,
            endAngle: 90,
            center: ['50%', '75%'],
            size: '110%'
        }
    },
    series: [{
        type: 'pie',
        name: 'Browser share',
        innerSize: '40%',
        data: [
		
			@foreach($chartOpenClosedCluesDataArray as $key=>$chartData)
    		[
    			'{{$chartData["title"]}}', {{$chartData["count"]}}
    		]
    		@if($key!=array_key_last($chartOpenClosedCluesDataArray))
    		,
    		@endif
			@endforeach
		
	
        ]
    }]
});


















Highcharts.chart('chartClueStatusGrouped', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: true,
        type: 'pie'
    },
    title: {
        text: 'نمودار سرنخ ها بر اساس وضعیت',
        align: 'center'
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    accessibility: {
        point: {
            valueSuffix: '%'
        }
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.percentage:.1f} %',
				style: {
					fontSize: "14px"
                }
            }
        }
    },
    series: [{
        name: 'وضعیت سرنخ ها',
        colorByPoint: true,
        data: [
		
		@foreach($chartStatusGroupedDataArray as $key=>$chartData)
    		{
    			name: '{{$chartData["title"]}}',
    			y: {{$chartData["count"]}}
    		}
    		@if($key!=array_key_last($chartStatusGroupedDataArray))
    		,
    		@endif
    	@endforeach
		
		/*
		{
            name: 'Chrome',
            y: 70.67,
            sliced: true,
            selected: true
        }
		*/]
    }]
});


</script>


@endsection
