@extends('panel.layout.master')
@section('main_content')
    <div class="row mb-md-5">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2>مدیریت کارشناسان فروش</h2>

            </div>

            <div class="pull-left">

                

            </div>

        </div>

    </div>


    @if ($message = Session::get('success'))

        <div class="alert alert-success">

            <p>{{ $message }}</p>

        </div>

    @endif


    <table class="table table-bordered">

        <tr>

            <th>ردیف</th>

            <th>نام و نام خانوادگی</th>

            <th>ایمیل</th>

            <th>نقش</th>
            
            <th>نمایش</th>
			@can('set-monthly-commision')
			<th>کمیسیون ماهانه</th>
			@endcan
			
			@can('show-users-commission-list')
			<th>لیست کمیسیون</th>
			@endcan

            <th width="280px">عملیات</th>

        </tr>

        @foreach ($data as $key => $user)

            <tr>

                <td>{{ ++$i }}</td>

				<td><a href="{{route('specialists.show-specialist-report',$user->id)}}" target="_blank">{{ $user->name }}</a></td>

                <td>{{ $user->email }}</td>

                <td>

                    @if(!empty($user->getRoleNames()))

                        @foreach($user->getRoleNames() as $v)

                            <label class="badge badge-success">{{ $v }}</label>

                        @endforeach

                    @endif

                </td>
                
                
                <td>
                
                @can('user-list')
                    <a class="btn btn-info" href="{{ route('users.show',$user->id) }}">نمایش</a>
                @endcan
                </td>
				@can('set-monthly-commision')
				<td>
				
	             <a class="btn btn-danger" href="{{ route('commission.set-monthly-commision',$user->id) }}">ثبت کمیسیون ماهانه</a>
	            
				</td>
				@endcan
				
				@can('show-users-commission-list')
				<td>
				
	             <a href="{{ route('commission.show-users-commission-months-list',$user->id) }}">لیست کمیسیون ماهانه</a>
	            
				</td>
				@endcan
				
				


                <td>
		     
                    
		     @can('show-call-targets-list')
	             <a class="btn btn-warning" href="{{ route('specialists.show-call-targets-list',$user->id) }}">لیست تارگت تماسها</a>
	             @endcan
				 
				 
	            

                </td>
				
				
            </tr>

        @endforeach

    </table>


    {!! $data->links() !!}



@endsection
