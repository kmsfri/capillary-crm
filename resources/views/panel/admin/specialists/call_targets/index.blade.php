@extends('panel.layout.master')
@section('main_content')
    <div class="row mb-md-5">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2>لیست تارگت تماسهای روزانه برای کاربر: {{$specialistUser->name}}</h2>

            </div>

            <div class="pull-left">

                @can('business-create')

                    <a class="btn btn-success" href="{{ route('specialists.create-call-target',$specialistUser->id) }}"> ثبت لیست تارگت تماس روزانه</a>

                @endcan

            </div>

        </div>

    </div>


    @if ($message = Session::get('success'))

        <div class="alert alert-success">

            <p>{{ $message }}</p>

        </div>

    @endif
    
    
    


    <table class="table table-bordered">

        <tr>

            <th>ردیف</th>

            <th>تاریخ تماس</th>
            
            <th>تعداد کل تماسها</th>
            
            <td>تماسهای گرفته شده</td>

            <th>ایجاد کننده</th>
            
            <th>زمان ایجاد</th>
            
            <th>مشاهده</th>

            <th width="280px">عملیات</th>

        </tr>

        @foreach ($data as $key => $callTarget)

            <tr>

                <td>{{ ++$i }}</td>

                <td>{{ \Morilog\Jalali\Jalalian::forge($callTarget->target_date)->format('%A, %d %B %Y') }}</td>
                
                
                @php
                	$allCallTargets = 0;
                	if(!empty($callTarget->target_calls_list)){
                		$decodedCallTargetsList = json_decode($callTarget->target_calls_list);
                		$allCallTargets = count($decodedCallTargetsList);
                	}
                @endphp
                <td>{{$allCallTargets}}</td>
                
                <td>{{$callTarget->voip_calls_count}}</td>

                <td>{{ $callTarget->creator()->first()->email }}</td>
                
                <td>{{ \Morilog\Jalali\Jalalian::forge($callTarget->created_at)->format('%A, %d %B %Y') }}</td>
                
                <td>
                @can('specialist-show-call-target-details')
	             <a class="btn btn-success" href="{{ route('specialists.show-call-target-details',['call_target_id'=>$callTarget->id]) }}">مشاهده وضعیت</a>
	        @endcan
                </td>

                <td>
                @can('specialist-edit-call-targets')
	             <a class="btn btn-warning" href="{{ route('specialists.create-call-target',['user_id'=>$specialistUser->id,'target_date'=>$callTarget->target_date]) }}">ویرایش</a>
	        @endcan
                </td>

            </tr>

        @endforeach

    </table>


    {!! $data->links() !!}



@endsection
