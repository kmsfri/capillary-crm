@extends('panel.layout.master')
@section('main_content')



    <div class="row mb-md-5">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2>کارتابل آلارم ها</h2>

            </div>

            <div class="pull-left">

		<a class="btn btn-success" href="{{ route('specialists.set-alarm-to-specialist-daily-call-target') }}"> ایجاد آلارم جدید</a>
               
            </div>

        </div>

    </div>


    @if ($message = Session::get('success'))

        <div class="alert alert-success">

            <p>{{ $message }}</p>

        </div>

    @endif
    
    @if ($message = Session::get('danger'))

        <div class="alert alert-danger">

            <p>{{ $message }}</p>

        </div>

    @endif
	
	
	<div class="row">    
		<div class="col-lg-12 mb-md-5">
			<form action="{{route('specialists.showAlarmsList')}}" method="GET">
			<div class="row">
			
				
				
				<div class="col-xs-12 col-sm-12 col-md-2 form-input-container">

					<div class="form-group">

						<strong>از تاریخ:</strong>

						<input data-jdp type="text" name="from_date" id="from_date" value="{{ old('from_date',request()->from_date) }}" class="form-control" placeholder="">

					</div>

				</div>
				
				
				<div class="col-xs-12 col-sm-12 col-md-2 form-input-container">

					<div class="form-group">

						<strong>تا تاریخ:</strong>
						
						<input data-jdp type="text" name="to_date" id="to_date" value="{{ old('to_date',request()->to_date) }}" class="form-control" placeholder="">

					</div>

				</div>
				

				<div class="col-xs-12 col-sm-12 col-md-2 form-input-container">

					<div class="form-group">

					    <strong>دسته بندی:</strong>

					    <select  name='seen_type' id='seen_type' class='form-control pull-right'>
					    	<option {{(!old('seen_type', isset(request()->seen_type) ? request()->seen_type : '')? 'selected' : '')}} value="">انتخاب کنید</option>
						
						<option @if(old('seen_type', isset(request()->seen_type) ? request()->seen_type : '')=='0') selected @endif value="0" >انجام نشده</option>
						
						<option @if(old('seen_type', isset(request()->seen_type) ? request()->seen_type : '')=='1') selected @endif value="1" >انجام شده</option>
					    </select>

					</div>

				</div>
				
				<div class="col-xs-12 col-sm-12 col-md-2 form-input-container">

					<div class="form-group">

					    <strong>سررسید:</strong>

					    <select  name='overdue' id='overdue' class='form-control pull-right'>
					    	<option {{(!old('overdue', isset(request()->overdue) ? request()->overdue : '')? 'selected' : '')}} value="">انتخاب کنید</option>
						
						<option @if(old('overdue', isset(request()->overdue) ? request()->overdue : '')=='1') selected @endif value="1" >سررسید نشده</option>
						
						<option @if(old('overdue', isset(request()->overdue) ? request()->overdue : '')=='2') selected @endif value="2" >سررسید شده</option>
					    </select>

					</div>

				</div>
				
				
				<div class="col-xs-12 col-sm-12 col-md-4 form-input-container">

					<div class="form-group">

						<strong>بخشی از توضیحات:</strong>
						
						<input type="text" name="alarm_description" id="alarm_description" value="{{ old('alarm_description',request()->alarm_description) }}" class="form-control" placeholder="">

					</div>

				</div>
				
		
			</div>
			
			<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 form-input-container">
				<div class="pull-left">
				<button type="submit" class="btn btn-success">فیلتر</button>
				</div>
			</div>
			</div>
			</form>
		</div>
	</div>

    
	<div class="row">    
		<div class="col-lg-12 mb-md-5">
			<h4>آلارم های امروز:</h4>
		</div>
	</div>

    <table class="table table-bordered">

        <tr> 

            <th>ردیف</th>

            <th>مشتری</th>
			
            
            <th>توضیحات</th>
			
            
            <td>تاریخ یادآوری</td>
            
            <td>تاریخ ثبت</td>

			
			<td>عملیات</td>


        </tr>

		@php
		$i = 0;
		@endphp
        @foreach ($todayData as $key => $alarm)

	    @php
	    $rowClass = "";
	    if($alarm->is_seen==1){
	    	
	    	$rowClass = 'success';
	    
	    }elseif(\Morilog\Jalali\Jalalian::forge($alarm->alarm_at)->isPast()){
	    	$rowClass = 'danger';
	    }
	    @endphp

            <tr class="{{$rowClass}}">

                <td>{{ ++$i }}</td>
				
				<td>
					@if(!empty($alarm->clue_id))
						@php
							$clue = $alarm->clue()->first();
						@endphp
					<a target="_blank" href="{{ route('clues.showCartable',['clue_id'=>$alarm['clue_id']]) }}">{{(!empty($alarm->clue_id)&&!empty($clue))?$clue->business_name.'('.$clue->first_name.' '.$clue->last_name.')':'-'}}</a>
					@elseif(!empty($alarm->business_id))
						@php
							$business = $alarm->business()->first();
						@endphp
					<a target="_blank" href="{{ route('businesses.showCartable',['business_id'=>$alarm['business_id']]) }}">{{(!empty($alarm->business_id)&&!empty($business))?$business->business_name.'('.$business->first_name.' '.$business->last_name.')':'-'}}</a>
					@elseif(!empty($alarm->customer_id))
						@php
							$customer = $alarm->customer()->first();
						@endphp
					<a target="_blank" href="{{ route('customers.showCartable',['customer_id'=>$alarm['customer_id']]) }}">{{(!empty($alarm->customer_id)&&!empty($customer))?$customer->business_name.'('.$customer->first_name.' '.$customer->last_name.')':'-'}}</a>
					@else
					-
					@endif
				</td>

				<td>{{$alarm->alarm_description}}</td>
				
				
				<td>{{ \Morilog\Jalali\Jalalian::forge($alarm->alarm_at)->format('%A, %d %B %Y در ساعت H:i:s') }}</td>
				
				<td>{{ \Morilog\Jalali\Jalalian::forge($alarm->created_at)->format('%A, %d %B %Y در ساعت H:i:s') }}</td>
                

				<td><a {{($alarm->is_seen==1)?'disabled':''}} class="btn btn-success {{($alarm->is_seen==1)?'disabled':''}}" href="{{ route('specialists.seenAlarm',$alarm->id) }}">انجام</a></td>

            </tr>

        @endforeach

    </table>
	
	</br>
	</br>
	</br>
	
	
    
    
	<div class="row">    
		<div class="col-lg-12 mb-md-5">
			<h4>همه آلارم ها:</h4>
		</div>
	</div>

    <table class="table table-bordered">

        <tr>

            <th>ردیف</th>

            <th>مشتری</th>
			
            
            <th>توضیحات</th>
			
            
            <td>تاریخ یادآوری</td>
            
            <td>تاریخ ثبت</td>

			
			<td>عملیات</td>


        </tr>

		@php
		$i = 0;
		@endphp
        @foreach ($data as $key => $alarm)

	    @php
	    $rowClass = "";
	    if($alarm->is_seen==1){
	    	
	    	$rowClass = 'success';
	    
	    }elseif(\Morilog\Jalali\Jalalian::forge($alarm->alarm_at)->isPast()){
	    	$rowClass = 'danger';
	    }
	    @endphp

            <tr class="{{$rowClass}}">

                <td>{{ ++$i }}</td>
				
				<td>
					@if(!empty($alarm->clue_id))
						@php
							$clue = $alarm->clue()->first();
						@endphp
					<a target="_blank" href="{{ route('clues.showCartable',['clue_id'=>$alarm['clue_id']]) }}">{{(!empty($alarm->clue_id)&&!empty($clue))?$clue->business_name.'('.$clue->first_name.' '.$clue->last_name.')':'-'}}</a>
					@elseif(!empty($alarm->business_id))
						@php
							$business = $alarm->business()->first();
						@endphp
					<a target="_blank" href="{{ route('businesses.showCartable',['business_id'=>$alarm['business_id']]) }}">{{(!empty($alarm->business_id)&&!empty($business))?$business->business_name.'('.$business->first_name.' '.$business->last_name.')':'-'}}</a>
					@elseif(!empty($alarm->customer_id))
						@php
							$customer = $alarm->customer()->first();
						@endphp
					<a target="_blank" href="{{ route('customers.showCartable',['customer_id'=>$alarm['customer_id']]) }}">{{(!empty($alarm->customer_id)&&!empty($customer))?$customer->business_name.'('.$customer->first_name.' '.$customer->last_name.')':'-'}}</a>
					@else
					-
					@endif
				</td>

				<td>{{$alarm->alarm_description}}</td>
				
				
				<td>{{ \Morilog\Jalali\Jalalian::forge($alarm->alarm_at)->format('%A, %d %B %Y در ساعت H:i:s') }}</td>
				
				<td>{{ \Morilog\Jalali\Jalalian::forge($alarm->created_at)->format('%A, %d %B %Y در ساعت H:i:s') }}</td>
                

				<td><a {{($alarm->is_seen==1)?'disabled':''}} class="btn btn-success {{($alarm->is_seen==1)?'disabled':''}}" href="{{ route('specialists.seenAlarm',$alarm->id) }}">انجام</a></td>

            </tr>

        @endforeach

    </table>


@endsection
