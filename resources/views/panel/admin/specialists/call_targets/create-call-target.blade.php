@extends('panel.layout.master')
@section('additional_js_top')

@endsection
@section('additional_css')


<link href="{{asset('select2/css/select2.min.css')}}" rel="stylesheet" />
    

<style>

.select2-container--default .select2-selection--multiple .select2-selection__choice {
  color: black;
}

</style>
    
@stop
@section('main_content')

@if ($message = Session::get('danger'))

        <div class="alert alert-danger">

            <p>{{ $message }}</p>

        </div>

    @endif

    <div class="row mb-md-5">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2>ثبت لیست تارگت تماس روزانه برای کاربر: {{$specialistUser->name}}</h2>

            </div>

            <div class="pull-left">

                <a class="btn btn-primary" href="{{ route('showDashboard') }}"> بازگشت</a>

            </div>

        </div>

    </div>


    @if (count($errors) > 0)

        <div class="alert alert-danger">

            <strong>خطا!</strong> در مقادیر ورودی ایراداتی وجود دارد.<br><br>

            <ul>

                @foreach ($errors->all() as $error)

                    <li>{{ $error }}</li>

                @endforeach

            </ul>

        </div>

    @endif



    {!! Form::open(array('route' => 'specialists.store-call-target','method'=>'POST','id'=>'mainForm')) !!}


    <input type="hidden" name="specialist_user_id" value="{{$specialistUser->id}}">
    <input type="hidden" name="call_target_id" value="{{!empty($callTarget)?$callTarget->id:''}}">
    <div class="row">
    
    
	<div class="col-xs-12 col-sm-12 col-md-3 form-input-container">

		<div class="form-group">

		    <strong>در تاریخ:</strong>

		    <input data-jdp type="text" name="target_date" {{(!empty($callTargetAt)?'disabled':'')}} id="target_date" value="{{old('target_date',(!empty($callTargetAt)?\Morilog\Jalali\Jalalian::forge($callTargetAt)->format('%Y/%m/%d'):'') )}}" class="form-control" placeholder="">

		</div>

	</div>
    </div>
    <div class="row">
	
	
	
	
	
	
	
	<div class="col-xs-12 col-sm-12 col-md-12 form-input-container">

		<div class="form-group">

		    <strong>کسب و کار:</strong>

		
		    
		    
		    <select required  class="js-example-basic-multiple" name="business_id[]" multiple="multiple">
		    
			    @foreach($businesses as $key=>$business)
			    
				@php
				$selected = "";
			    	if(!empty($callTarget->target_calls_list)){
				    	if(in_array($business->id,$callTarget->target_calls_list)){
				    		$selected = "selected";
				    	}
			    	}
			    	@endphp
			    
				    <option {{$selected}} value="{{$business->id}}" >{{$business->business_name}}</option>
				@endforeach

			</select>

		</div>

        </div>
        

    
<!--form-control-->
        
        
        
        <div class="col-xs-12 col-sm-12 col-md-12 form-input-container">

        	<div class="form-group">

            	<strong>توضیحات:</strong>

            	<textarea class="form-control" style="height:100px" name="target_call_description" placeholder="" required>{{ old('target_call_description',(!empty($callTarget)?$callTarget->target_call_description:'')) }}</textarea>

        	</div>

	</div>

        
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">

            <button id="submitFormBtn" type="button" class="btn btn-primary">ثبت</button>

        </div>

    </div>

    {!! Form::close() !!}



@endsection
@section('additional_js_bottom')

<script src = "{{asset('select2/js/select2.min.js')}}"></script>



    <script type="text/javascript">
		/* global $ */
		$(document).ready(function() {
			
			$(document).ready(function() {
			    $('.js-example-basic-multiple').select2();
			});
			
			$('#submitFormBtn').click(function() {
			
			
				if($('#target_date').val()==""){
					alert('لطفا یک تاریخ را برای انجام تماسهای روزانه انتخاب کنید');
					return;
				}
				
				$('#mainForm').submit();
				//submit #mainForm form
			
			});


		});


		
		
	</script>
	
	
	
	
	
	
@endsection
