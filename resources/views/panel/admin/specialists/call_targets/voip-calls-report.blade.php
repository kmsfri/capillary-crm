@extends('panel.layout.master')
@section('main_content')



    <div class="row mb-md-5">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2>گزارش تماسهای سیستم VOIP</h2>

            </div>

            <div class="pull-left">

               
            </div>

        </div>

    </div>


    @if ($message = Session::get('success'))

        <div class="alert alert-success">

            <p>{{ $message }}</p>

        </div>

    @endif
    
    @if ($message = Session::get('danger'))

        <div class="alert alert-danger">

            <p>{{ $message }}</p>

        </div>

    @endif
	
	
	<div class="row">    
		<div class="col-lg-12 mb-md-5">
			<form action="{{route('specialists.show-voip-calls-report')}}" method="GET">
			<div class="row">
			
				
				
				<div class="col-xs-12 col-sm-12 col-md-2 form-input-container">

					<div class="form-group">

						<strong>از تاریخ:</strong>

						<input data-jdp type="text" name="from_date" id="from_date" value="{{ old('from_date',request()->from_date) }}" class="form-control" placeholder="">

					</div>

				</div>
				
				
				<div class="col-xs-12 col-sm-12 col-md-2 form-input-container">

					<div class="form-group">

						<strong>تا تاریخ:</strong>
						
						<input data-jdp type="text" name="to_date" id="to_date" value="{{ old('to_date',request()->to_date) }}" class="form-control" placeholder="">

					</div>

				</div>
				

				<div class="col-xs-12 col-sm-12 col-md-2 form-input-container">

					<div class="form-group">

					    <strong>تماس گیرنده:</strong>

					    <select  name='src_number' id='src_number' class='form-control pull-right'>
					    	<option {{(!old('src_number', isset(request()->src_number) ? request()->src_number : '')? 'selected' : '')}} value="">انتخاب کنید</option>
					    	
					    	@foreach($voipSpecialistUsers as $srcUser)
					    		<option @if(old('src_number', isset(request()->src_number) ? request()->src_number : '')==$srcUser->voip_id) selected @endif value="{{$srcUser->voip_id}}" >{{$srcUser->name}}</option>
					    	@endforeach
					    	
						
					    </select>

					</div>

				</div>
				
				
				<div class="col-xs-12 col-sm-12 col-md-2 form-input-container">

					<div class="form-group">

						<strong>شماره مقصد:</strong>
						
						<input type="text" name="dst_number" id="dst_number" value="{{ old('dst_number',request()->dst_number) }}" class="form-control" placeholder="شماره مقصد یا بخشی از آن">

					</div>

				</div>
				
				
				<div class="col-xs-12 col-sm-12 col-md-2 form-input-container">

					<div class="form-group">

					    <strong>وضعیت پاسخ:</strong>

					    <select  name='disposition' id='disposition' class='form-control pull-right'>
					    	<option {{(!old('disposition', isset(request()->disposition) ? request()->disposition : '')? 'selected' : '')}} value="">انتخاب کنید</option>
						
						<option @if(old('disposition', isset(request()->disposition) ? request()->disposition : 'NO ANSWER')=='NO ANSWER') selected @endif value="NO ANSWER" >پاسخ داده نشده</option>
						<option @if(old('disposition', isset(request()->disposition) ? request()->disposition : '')=='BUSY') selected @endif value="BUSY" >مشغول</option>
						<option @if(old('disposition', isset(request()->disposition) ? request()->disposition : '')=='ANSWERED') selected @endif value="ANSWERED" >پاسخ داده شده</option>
						<option @if(old('disposition', isset(request()->disposition) ? request()->disposition : '')=='CONGESTION') selected @endif value="CONGESTION" >ترافیک</option>
						<option @if(old('disposition', isset(request()->disposition) ? request()->disposition : '')=='FAILED') selected @endif value="FAILED" >ناموفق</option>

					    </select>

					</div>

				</div>
				
				
		
			</div>
			
			<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 form-input-container">
				<div class="pull-left">
				<button type="submit" class="btn btn-success">فیلتر</button>
				</div>
			</div>
			</div>
			</form>
		</div>
	</div>

    
    
    


    <table class="table table-bordered">

        <tr>

            <th>ردیف</th>

            <th>تاریخ تماس</th>
	    <th>داخلی تماس گیرنده</th>
	    <th>نام تماس گیرنده</th>
	    <th>مقصد</th>
	    <th>مدت زمان</th>
	    <th>وضعیت پاسخ</th>

        </tr>

	@php
	$i = 0;
	@endphp
	
	@php
	$dispositionValues = [
		'NO ANSWER' => ['title'=>'پاسخ داده نشده','class'=>'warning'],
		'BUSY' => ['title'=>'مشغول','class'=>'info'],
		'ANSWERED' => ['title'=>'پاسخ داده شده','class'=>'success'],
		'CONGESTION' => ['title'=>'ترافیک','class'=>'default'],
		'FAILED' => ['title'=>'ناموفق','class'=>'danger']
	];
	@endphp
	
        @foreach ($callsReportData as $key => $call)


            <tr>

                <td>{{ ++$i }}</td>
                <td>{{\Morilog\Jalali\Jalalian::forge($call->calldate)->format('%A, %d %B %Y در ساعت H:i:s')}}</td>
                <td>{{$call->src}}</td>
                <td>
                @php
                $crmUser = \App\Models\User::where('voip_id',$call->src)->first();
                @endphp
                @if(!empty($crmUser->name))
                {{$crmUser->name}}
                @else
                {{$call->src}}
                @endif
                </td>
                <td>
                
                
                @php
                $crmUser = \App\Models\User::where('voip_id',$call->dst)->first();
                @endphp
                @if(!empty($crmUser->name))
                {{$crmUser->name}}
                @else
                {{$call->dst}}
                @endif
                
                </td>
                <td>{{$call->duration}} ثانیه</td>
                <td><label class="badge badge-{{$dispositionValues[$call->disposition]['class']}}">{{$dispositionValues[$call->disposition]['title']}}</label></td>	
				
            </tr>

        @endforeach

    </table>
    
    {!! $callsReportData->links() !!}


@endsection
