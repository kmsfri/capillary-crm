@extends('panel.layout.master')
@section('main_content')
    <div class="row mb-md-5">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2>{{$data['target_title']}}</h2>

            </div>

            <div class="pull-left">

                

            </div>

        </div>

    </div>


    @if ($message = Session::get('success'))

        <div class="alert alert-success">

            <p>{{ $message }}</p>

        </div>

    @endif


    <table class="table table-bordered">

        <tr>

            <th>ردیف</th>

            <th>نام کسب و کار</th>

            <th>وضعیت تماس</th>

            <th>زمان تماس</th>
            
        </tr>

	@php
	$i=0;
	@endphp
        @foreach ($data['target_businesses'] as $key => $business)

            <tr>

                <td>{{ ++$i }}</td>

                <td>{{ $business->business_name }}</td>
                
                
      
                @if($business->voip_sum_call_durations==0)
                	<td><span class="label label-danger badge">تماس گرفته نشده</span></td>
                @else
                	<td><span class="label label-success badge">تماس گرفته شده</span></td>
                @endif
                
                <td>{{ $business->voip_sum_call_durations }} ثانیه</td>
                



            </tr>

        @endforeach

    </table>





@endsection
