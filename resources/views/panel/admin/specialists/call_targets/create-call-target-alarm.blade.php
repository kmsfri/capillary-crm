@extends('panel.layout.master')
@section('additional_js_top')

@endsection
@section('additional_css')


<style>

.select2-container--default .select2-selection--multiple .select2-selection__choice {
  color: black;
}

</style>
    
@stop
@section('main_content')

@if ($message = Session::get('danger'))

        <div class="alert alert-danger">

            <p>{{ $message }}</p>

        </div>

    @endif

    <div class="row mb-md-5">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">
		<h2>{{$pageTitle}}</h2>
            </div>

            <div class="pull-left">

                <a class="btn btn-primary" href="{{ route('specialists.show-specialist-daily-call-targets-list') }}"> بازگشت</a>

            </div>

        </div>

    </div>
	
	
	
	@if(!empty($customer))
		
		<div class="alert alert-danger">

            <strong>اخطار!</strong>
			
			<h5>دسترسی به بخش مشتریان به زودی به طور کامل بسته خواهد شد. لطفا تا حد ممکن عملیات مربوطه را برای سرنخ انجام دهید</h5>
            

        </div>
	


    @endif


    @if (count($errors) > 0)

        <div class="alert alert-danger">

            <strong>خطا!</strong> در مقادیر ورودی ایراداتی وجود دارد.<br><br>

            <ul>

                @foreach ($errors->all() as $error)

                    <li>{{ $error }}</li>

                @endforeach

            </ul>

        </div>

    @endif



    {!! Form::open(array('route' => 'specialists.store-alarm-to-specialist-daily-call-target','method'=>'POST','id'=>'mainForm')) !!}


    @if(!empty($business))
    <input type="hidden" name="business_id" value="{{$business->id}}">
    @endif
    @if(!empty($customer))
    <input type="hidden" name="customer_id" value="{{$customer->id}}">
    @endif
	@if(!empty($clue))
    <input type="hidden" name="clue_id" value="{{$clue->id}}">
    @endif
    @if(!empty($callTargetID))
    <input type="hidden" name="call_target_id" value="{{!empty($callTargetID)?$callTargetID:''}}">
    @endif
    <div class="row">
    
	
	@if(!empty(request()->get('return_url')))
    <input type="hidden" name="return_url" value="{{request()->get('return_url')}}">
    @endif
    
	<div class="col-xs-12 col-sm-12 col-md-3 form-input-container">

		<div class="form-group">

		    <strong>یادآوری در تاریخ :</strong>

		    <input data-jdp type="text" name="alarm_at" id="alarm_at" value="{{old('alarm_at')}}" class="form-control" placeholder="">

		</div>

	</div>
    </div>
    <div class="row">
	
    
<!--form-control-->
        
        
        
        <div class="col-xs-12 col-sm-12 col-md-12 form-input-container">

        	<div class="form-group">

            	<strong>توضیحات یادآوری:</strong>

            	<textarea class="form-control" style="height:100px" name="alarm_description" placeholder="" required>{{ old('alarm_description') }}</textarea>

        	</div>

	</div>

        
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">

            <button id="submitFormBtn" type="button" class="btn btn-primary">ثبت</button>

        </div>

    </div>

    {!! Form::close() !!}



@endsection
@section('additional_js_bottom')



    <script type="text/javascript">
		/* global $ */
		$(document).ready(function() {
			

			$('#submitFormBtn').click(function() {
			
			
				if($('#alarm_at').val()==""){
					alert('لطفا یک تاریخ را انتخاب کنید');
					return;
				}
				
				$('#mainForm').submit();
				//submit #mainForm form
			
			});


		});


		
		
	</script>
	
	
	
	
	
	
	
	
	
@endsection

@section('overwrite_datepicker_js')

<script>
jalaliDatepicker.startWatch({
    minDate: "attr",
    maxDate: "attr",
    time: true,
    changeMonthRotateYear: true,
    showTodayBtn: true,
    showEmptyBtn: true,
    topSpace: 10,
    bottomSpace: 30,
    dayRendering(opt,input){
        return {
            isHollyDay:opt.day==1
        }
    }
});
</script>

@endsection
