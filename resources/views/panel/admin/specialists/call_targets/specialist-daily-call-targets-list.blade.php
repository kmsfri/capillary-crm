@extends('panel.layout.master')
@section('main_content')
    <div class="row mb-md-5">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2>{{$data['target_title']}}</h2>

            </div>

            <div class="pull-left">

                

            </div>

        </div>

    </div>


    @if ($message = Session::get('success'))

        <div class="alert alert-success">

            <p>{{ $message }}</p>

        </div>

    @endif


    @if(!empty($data['target_businesses']))
	    <table class="table table-bordered">

		<tr>

		    <th>ردیف</th>

		    <th>نام کسب و کار</th>

		    <th>وضعیت تماس</th>

		    <th>زمان تماس</th>
		    
		    <th width="280px" class="text-center">عملیات</th>
		    
		</tr>

		@php
		$i=0;
		@endphp
		
		
			
		@foreach ($data['target_businesses'] as $key => $business)

		    <tr>

			<td>{{ ++$i }}</td>

			<td>{{ $business->business_name }}</td>
			
			<td><span class="label label-danger badge">تماس گرفته نشده</span></td>

			<td>-</td>

		     
			<td class="text-center">
			
			     

			    <a class="btn btn-info" href="{{ route('specialists.set-alarm-to-specialist-daily-call-target',['business_id'=>$business->id, 'call_target_id'=>$callTargetID]) }}">تنظیم آلارم</a>

			</td>

		    </tr>

		@endforeach

	    </table>
    @else
    <div class="alert alert-warning">

        <p>برای این تاریخ شما هیچ تارگت تماسی توسط مدیریت مشخص نشده است</p>

    </div>
    @endif




@endsection
