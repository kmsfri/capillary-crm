@extends('panel.layout.master')
@section('main_content')
    <div class="row mb-md-5">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2>مدیریت مشتریان</h2>

            </div>

            <div class="pull-left">

                <a class="btn btn-success" href="{{ route('customers.create') }}"> ایجاد مشتری جدید</a>

            </div>

        </div>

    </div>


    @if ($message = Session::get('success'))

        <div class="alert alert-success">

            <p>{{ $message }}</p>

        </div>

    @endif
    
    @if ($message = Session::get('danger'))

        <div class="alert alert-danger">

            <p>{{ $message }}</p>

        </div>

    @endif
    
    
    
    
    
    <div class="row">    
    <div class="col-lg-12 mb-md-5">
        <form action="{{route('customers.index')}}" method="GET">
        <div class="row">
	    
            
            
            <div class="col-xs-12 col-sm-12 col-md-4 form-input-container">

                <div class="form-group">

                    <strong>عنوان:</strong>

                    <input type="text" name="search_key" id="search_key" value="{{ old('search_key',request()->search_key) }}" class="form-control" placeholder="بخشی از نام، نام خانوادگی، عنوان، آدرس یا شماره تلفن">

                </div>

            </div>
            
            <div class="col-xs-12 col-sm-12 col-md-2 form-input-container">

                <div class="form-group">

                    <strong>استان:</strong>

                    <input type="text" name="state" id="state" value="{{ old('state',request()->state) }}" class="form-control" placeholder="نام استان">

                </div>

            </div>
            
            <div class="col-xs-12 col-sm-12 col-md-2 form-input-container">

                <div class="form-group">

                    <strong>شهر:</strong>

                    <input type="text" name="city" id="city" value="{{ old('city',request()->city) }}" class="form-control" placeholder="نام شهر">

                </div>

            </div>
	
	
        </div>
        
        <div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 form-input-container">
				<div class="pull-left">
					<button type="submit" class="btn btn-success" href="{{ route('customers.index') }}">فیلتر</button>
				</div>
				
				<div class="pull-left ml-3">
					<button type="button" class="btn btn-warning" onclick="exportExcel()">خروجی اکسل</button>
					<script>
						function exportExcel(){
							const urlParams = new URLSearchParams(window.location.search);

							urlParams.set('export_excel', 1);

							window.location.search = urlParams;
						}
					</script>
				</div>
			</div>
        </div>
        </form>
    </div>
</div>
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    


    <table class="table table-bordered">

        <tr>

            <th>ردیف</th>

            <th>نام و نام خانوادگی</th>
            <th>فروشگاه</th>
            <th>شهر</th>
            <th>کاربر ایجاد کننده</th>
            <th>فاکتور</th>
            @can('send-specialist-info-by-sms')
            <th>ارسال اطلاعات</th>
            @endcan
            
            @can('set-alarm-on-customers')
            <th>آلارم</th>
            @endcan
            
            
            <th width="280px">عملیات</th>

        </tr>

        @foreach ($data as $key => $customer)

            <tr>

                <td>{{ ++$i }}</td>

                <td>
                
                @can('show-customer-cartable')
                <a target="_blank" class="" href="{{ route('customers.showCartable',['customer_id'=>$customer->id]) }}">
                @endcan
                
                {{ $customer->first_name }} {{ $customer->last_name }}
                
                @can('show-customer-cartable')
                </a>
                @endcan
                </td>

                <td>
                
                @can('show-customer-cartable')
                <a target="_blank" class="" href="{{ route('customers.showCartable',['customer_id'=>$customer->id]) }}">
                @endcan
                {{ $customer->business_name }}
                
                @can('show-customer-cartable')
                </a>
                @endcan
                
                </td>
                
                
                
                <td>{{ $customer->city }}</td>
                <td>
                @php
                if($customer->is_sepidar_user){
                	echo "<label class='badge badge-warning'>کاربر سپیدار</label>";
                }else{
                	echo $customer->creator()->first()->name;
                }
                @endphp
                </td>

		<td>
			<a class="btn btn-primary" href="{{ route('invoices.create',['customer_id'=>$customer->id]) }}">صدور فاکتور</a>
		</td>
		@can('send-specialist-info-by-sms')
		<td>
			<a href="{{ route('sms.sendSpecialistInfo',['customer_id'=>$customer->id]) }}" onclick="return confirm('آیا مطمئنید؟')">پیامک اطلاعات کارشناس</a>
		</td>
		@endcan
		
		@can('set-alarm-on-customers')
		<td>
			<a href="{{ route('specialists.set-alarm-to-specialist-daily-call-target',['customer_id'=>$customer->id]) }}" onclick="return confirm('آیا مطمئنید؟')">تنظیم آلارم</a>
		</td>
		@endcan
                <td>
                
		     

                    <!--<a class="btn btn-info" href="{{ route('customers.show',$customer->id) }}">نمایش</a>-->

                    <a class="btn btn-primary" href="{{ route('customers.edit',$customer->id) }}">ویرایش</a>

                    {!! Form::open(['method' => 'DELETE','route' => ['customers.destroy', $customer->id],'style'=>'display:inline']) !!}

                    {!! Form::submit('حذف', ['class' => 'btn btn-danger']) !!}

                    {!! Form::close() !!}

                </td>

            </tr>

        @endforeach

    </table>


    {!! $data->appends(Request::all())->render() !!}



@endsection
