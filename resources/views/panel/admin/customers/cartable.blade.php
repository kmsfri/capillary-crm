@extends('panel.layout.master')
@section('additional_css')


<style>

.create-invoice-col a{
  display:block;
}

</style>
    
@stop
@section('main_content')



	@if(!empty($customer))
		
		<div class="alert alert-danger">

            <strong>اخطار!</strong>
			
			<h5>دسترسی به بخش مشتریان به زودی به طور کامل بسته خواهد شد. لطفا تا حد ممکن عملیات مربوطه را برای سرنخ انجام دهید</h5>
            

        </div>
	


    @endif



    <div class="row mb-md-5">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2>کارتابل مشتری : {{$customer->business_name."(".$customer->first_name." ".$customer->last_name.")"}}</h2>

            </div>

            <div class="pull-left">


            </div>

        </div>

    </div>
    
    
    
    @if ($message = Session::get('success'))

        <div class="alert alert-success">

            <p>{{ $message }}</p>

        </div>

    @endif
    
    @if ($message = Session::get('danger'))

        <div class="alert alert-danger">

            <p>{{ $message }}</p>

        </div>

    @endif
    
    
    
    
    
    
<!-- BEGIN CUSTOMER INFORMATION -->


<div class="row mt-3 mb-3">

<div class="col-lg-12">


<div class="card border-success">
<div class="card-header">اطلاعات مشتری</div>
<div class="card-body">
				
				
				
				
				
				
				
				
				
				
			
				
<table class="table table-bordered">



<tr>
	<td>
		<strong>نام:</strong>
                {{ $customer->first_name }}
	</td>
	<td>
		<strong>نام خانوادگی:</strong>
                {{ $customer->last_name }}
	</td>
</tr>

<tr>
	<td>
		<strong>نام فروشگاه:</strong>
                {{ $customer->business_name }}
	</td>
	<td>
		<strong>استان - شهر:</strong>
                {{ $customer->state }} {{ $customer->city }}
	</td>
</tr>


<tr>
	<td colspan="2">
		<strong>آدرس:</strong>
                {{ $customer->address }}
	</td>
</tr>

<tr>
	<td>
		<strong>تلفن ثابت ۱:</strong>
                {{ $customer->phone }}
	</td>
	<td>
		<strong>تلفن ثابت ۲:</strong>
                {{ $customer->phone2 }}
	</td>
</tr>


<tr>
	<td>
		<strong>تلفن ثابت ۳:</strong>
                {{ $customer->phone3 }}
	</td>
	<td>
		<strong>موبایل ۱:</strong>
                {{ $customer->mobile }}
	</td>
</tr>

<tr>
	<td>
		<strong>موبایل ۲:</strong>
                {{ $customer->mobile2 }}
	</td>
	<td>
		<strong>موبایل ۳:</strong>
                {{ $customer->mobile3 }}
	</td>
</tr>


<tr>
	<td>
		<strong>تلفن سپیدار:</strong>
                {{ $customer->sepidar_phone }}
	</td>
	<td>
		<strong>کد سپیدار:</strong>
                {{ $customer->sepidar_code }}
	</td>
</tr>


<tr>
	<td colspan="2">
		<strong>کاربر ایجاد کننده:</strong>
		
		@php
                if($customer->is_sepidar_user){
                	echo "<label class='badge badge-warning'>کاربر سپیدار</label>";
                }else{
                	echo $customer->creator()->first()->name."(".$customer->creator()->first()->email.")";
                }
                @endphp
	</td>
</tr>


<tr>
	<td>
		<strong>زمان ایجاد:</strong>
                {{\Morilog\Jalali\Jalalian::forge($customer->created_at)->format('%A, %d %B %Y در ساعت H:i:s')}}
	</td>
	<td>
		<strong>زمان آخرین ویرایش:</strong>
                {{\Morilog\Jalali\Jalalian::forge($customer->updated_at)->format('%A, %d %B %Y در ساعت H:i:s')}}
	</td>
</tr>



</table>
				
							
	
	
</div>
</div>

</div>

</div>


<!-- END CUSTOMER INFORMATION -->















<!-- BEGIN CUSTOMER DESCRIPTION UPDATE -->


<div class="row mt-3 mb-3">

<div class="col-lg-12">


<div class="card border-success">
<div class="card-header">توضیحات کلی مشتری</div>
<div class="card-body">
				
	<form action="{{ route('customers.updateCustomerDescription') }}" method="POST">
        	@csrf
        	<input type="hidden" name="customer_id" value="{{$customer->id}}">
		<div class="form-group">

			<textarea class="form-control" name="customer_description" placeholder="" rows="8">{{old('customer_description',$customer->customer_description)}}</textarea>

		</div>
		
		<div class="col-xs-12 col-sm-12 col-md-12 text-center">

			<button type="submit" class="btn btn-primary">ثبت تغییرات</button>

		</div>
		
	</form>
					
							
	
	
</div>
</div>

</div>

</div>


<!-- END CUSTOMER DESCRIPTION UPDATE -->











<!-- BEGIN CALLS HISTORY -->













<div class="row mt-3 mb-3">

<div class="col-lg-12">


<div class="card border-success">
<div class="card-header">سوابق تماسها</div>
<div class="card-body">
				
				
				
				
				
				
				
				
				
				
@if(count($customerCalls)==0)
	<p class="card-text">هیچ تماسی برای این مشتری ثبت نشده است</p>
@else				
				
<table class="table table-bordered">

<tr>
    <th>تاریخ تماس</th>
    <th>داخلی تماس گیرنده</th>
    <th>نام تماس گیرنده</th>
    <th>مقصد</th>
    <th>مدت زمان</th>
    <th>وضعیت پاسخ</th>

</tr>



@php
$dispositionValues = [
	'NO ANSWER' => ['title'=>'پاسخ داده نشده','class'=>'warning'],
	'BUSY' => ['title'=>'مشغول','class'=>'info'],
	'ANSWERED' => ['title'=>'پاسخ داده شده','class'=>'success'],
	'CONGESTION' => ['title'=>'ترافیک','class'=>'default'],
	'FAILED' => ['title'=>'ناموفق','class'=>'danger']
];
@endphp


@foreach ($customerCalls as $call)

    <tr>

        <td>{{\Morilog\Jalali\Jalalian::forge($call->calldate)->format('%A, %d %B %Y در ساعت H:i:s')}}</td>
        <td>{{$call->src}}</td>
        <td>
        @php
        $crmUser = \App\Models\User::where('voip_id',$call->src)->first();
        @endphp
        @if(!empty($crmUser->name))
        {{$crmUser->name}}
        @else
        {{$call->src}}
        @endif
        </td>
        <td>
        
        
        @php
        $crmUser = \App\Models\User::where('voip_id',$call->dst)->first();
        @endphp
        @if(!empty($crmUser->name))
        {{$crmUser->name}}
        @else
        {{$call->dst}}
        @endif
        
        </td>
        <td>{{$call->duration}} ثانیه</td>
        <td><label class="badge badge-{{$dispositionValues[$call->disposition]['class']}}">{{$dispositionValues[$call->disposition]['title']}}</label></td>	

    </tr>

@endforeach

</table>
				
				
@endif			
				
				
				
	
	
</div>
</div>

</div>

</div>







<!-- BEGIN ALARMS -->






<div class="row mt-3 mb-3">

<div class="col-lg-12">


<div class="card border-success">
<div class="card-header">سوابق آلارمها</div>
<div class="card-body">
				
				
				
				
				
				
				
				
				
				
@if(count($customerAlarms)==0)
	<p class="card-text">هیچ آلارمی برای این مشتری ثبت نشده است</p>
@else				
				
<table class="table table-bordered">

<tr>
	<th>توضیحات</th>
	<td>تاریخ یادآوری</td>
	<td>تاریخ ثبت</td>
</tr>





@foreach ($customerAlarms as $alarm)

	@php
		$rowClass = "";
		if($alarm->is_seen==1){

			$rowClass = 'success';

		}elseif(\Morilog\Jalali\Jalalian::forge($alarm->alarm_at)->isPast()){
			$rowClass = 'danger';
		}
	@endphp



    <tr class="{{$rowClass}}">



	<td>{{$alarm->alarm_description}}</td>


	<td>{{ \Morilog\Jalali\Jalalian::forge($alarm->alarm_at)->format('%A, %d %B %Y در ساعت H:i:s') }}</td>
	
	
	<td>{{ \Morilog\Jalali\Jalalian::forge($alarm->created_at)->format('%A, %d %B %Y در ساعت H:i:s') }}</td>


    </tr>

@endforeach

</table>
				
				
@endif			
				
				
				
	
	
</div>
</div>

</div>

</div>





<!-- END ALARMS -->




<!-- BEGIN CUSTOMER NOTES -->


<div class="row mt-3 mb-3">

<div class="col-lg-12">


<div class="card border-success">
<div class="card-header">یادداشتهای مشتری</div>
<div class="card-body">
				
	<form action="{{ route('customers.addNewNote') }}" method="POST">
        	@csrf
        	<input type="hidden" name="customer_id" value="{{$customer->id}}">
		<div class="form-group">

			<textarea class="form-control" name="note_text" placeholder="" rows="5">{{old('note_text')}}</textarea>

		</div>
		
		<div class="col-xs-12 col-sm-12 col-md-12 text-center">

			<button type="submit" class="btn btn-primary">ثبت یادداشت</button>

		</div>
		
	</form>
	
	
	
	
	
	
	
	
	
					
	@if(count($customerNotes)==0)
		<p class="card-text text-center">هنوز هیچ یادداشتی برای این مشتری ثبت نشده است</p>
	@else				
					
	<table class="table table-bordered mt-5">

	<tr>
		<th>متن یادداشت</th>
		<td>ثبت کننده</td>
		<td>تاریخ ثبت</td>
	</tr>





	@foreach ($customerNotes as $note)


	    <tr>



		<td>{{$note->note_text}}</td>
		
		<td>{{ $note->creator()->first()->name }}</td>


		<td>{{ \Morilog\Jalali\Jalalian::forge($note->created_at)->format('%Y/%m/%d H:i:s') }}</td>


	    </tr>

	@endforeach

	</table>
					
					
	@endif	
	
	
	
	
	
	
	
	
	
	
					
							
	
	
</div>
</div>

</div>

</div>


<!-- END CUSTOMER NOTES -->




























@endsection



