@extends('panel.layout.master')
@section('main_content')

    <div class="row mb-md-5">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2>ایجاد مشتری جدید</h2>

            </div>

            <div class="pull-left">

                <a class="btn btn-primary" href="{{ route('customers.index') }}"> بازگشت</a>

            </div>

        </div>

    </div>
	
	


    @if (count($errors) > 0)

        <div class="alert alert-danger">

            <strong>خطا!</strong> در مقادیر ورودی ایراداتی وجود دارد.<br><br>

            <ul>

                @foreach ($errors->all() as $error)

                    <li>{{ $error }}</li>

                @endforeach

            </ul>

        </div>

    @endif



    {!! Form::open(array('route' => 'customers.store','method'=>'POST')) !!}

    <div class="row mb-5">

        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>نام:</strong>

                {!! Form::text('first_name', old('first_name'), array('placeholder' => '','class' => 'form-control')) !!}

            </div>

        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>نام خانوادگی:</strong>

                {!! Form::text('last_name', old('last_name'), array('placeholder' => '','class' => 'form-control')) !!}

            </div>

        </div><div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>فروشگاه:</strong>

                {!! Form::text('business_name', old('business_name'), array('placeholder' => '','class' => 'form-control')) !!}

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>استان:</strong>

                {!! Form::text('state', old('state'), array('placeholder' => '','class' => 'form-control')) !!}

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>شهر:</strong>

                {!! Form::text('city', old('city'), array('placeholder' => '','class' => 'form-control')) !!}

            </div>

        </div><div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>آدرس:</strong>

                {!! Form::text('address', old('address'), array('placeholder' => '','class' => 'form-control')) !!}

            </div>

        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>تلفن ۱:</strong>

                {!! Form::text('phone', old('phone'), array('placeholder' => '','class' => 'form-control')) !!}

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>تلفن ۲:</strong>

                {!! Form::text('phone2', old('phone2'), array('placeholder' => '','class' => 'form-control')) !!}

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>تلفن ۳:</strong>

                {!! Form::text('phone3', old('phone3'), array('placeholder' => '','class' => 'form-control')) !!}

            </div>

        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>موبایل ۱:</strong>

                {!! Form::text('mobile', old('mobile'), array('placeholder' => '','class' => 'form-control')) !!}

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>موبایل ۲:</strong>

                {!! Form::text('mobile2', old('mobile2'), array('placeholder' => '','class' => 'form-control')) !!}

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>موبایل ۳:</strong>

                {!! Form::text('mobile3', old('mobile3'), array('placeholder' => '','class' => 'form-control')) !!}

            </div>

        </div>
        
        
        

        
        
        

        
        

        <div class="col-xs-12 col-sm-12 col-md-12 text-center">

            <button type="submit" class="btn btn-primary">ثبت</button>

        </div>

    </div>

    {!! Form::close() !!}















{!! Form::open(array('route' => 'customers.uploadExcelFile','method'=>'POST','enctype'=>'multipart/form-data')) !!}

    <div class="row mt-5">

        <div class="col-xs-12 col-sm-12 col-md-4">

            <div class="form-group">

                <strong>فایل اکسل:</strong>


		@csrf

                {!! Form::file('excel_file', array('class' => 'form-control')) !!}

            </div>

        </div>
    </div>
    <div class="row">

        <div class="col-xs-12 col-sm-12 col-md-4 text-center">

            <button type="submit" class="btn btn-primary">آپلود فایل</button>

        </div>

    </div>

    {!! Form::close() !!}













@endsection
