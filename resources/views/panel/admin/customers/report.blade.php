@extends('panel.layout.master')
@section('additional_js_top2')

<script src="{{asset('highcharts/highcharts.js')}}"></script>
<script src="{{asset('highcharts/modules/exporting.js')}}"></script>
<script src="{{asset('highcharts/modules/export-data.js')}}"></script>
<script src="{{asset('highcharts/modules/accessibility.js')}}"></script>

@endsection
@section('main_content')
    <div class="row mb-md-5">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2>گزارش جامع آماری مشتریان</h2>

            </div>

           

        </div>

    </div>


    @if ($message = Session::get('success'))

        <div class="alert alert-success">

            <p>{{ $message }}</p>

        </div>

    @endif
    
    @if ($message = Session::get('danger'))

        <div class="alert alert-danger">

            <p>{{ $message }}</p>

        </div>

    @endif
    
    
    
	<div class="row">    
		<div class="col-lg-12 mb-md-5">
			<form action="{{route('customers.showCustomersReport')}}" method="GET">
			<div class="row">
			
				
				
				<div class="col-xs-12 col-sm-12 col-md-2 form-input-container">

					<div class="form-group">

						<strong>از تاریخ:</strong>

						<input data-jdp type="text" name="from_date" id="from_date" value="{{ old('from_date',request()->from_date) }}" class="form-control" placeholder="">

					</div>

				</div>
				
				
				<div class="col-xs-12 col-sm-12 col-md-2 form-input-container">

					<div class="form-group">

						<strong>تا تاریخ:</strong>
						
						<input data-jdp type="text" name="to_date" id="to_date" value="{{ old('to_date',request()->to_date) }}" class="form-control" placeholder="">

					</div>

				</div>
				
				
				<div class="col-xs-12 col-sm-12 col-md-2 form-input-container">

					<div class="form-group">

						<strong>بازه زمانی:</strong>

						<select  name='report_period' id='report_period' class='form-control pull-right' autocomplete='off'>
						 <option @if(old('report_period', isset(request()->report_period) ? request()->report_period : '')==2) selected @endif value="2" >ماهانه</option>
						 <option @if(old('report_period', isset(request()->report_period) ? request()->report_period : '')==1) selected @endif value="1" >روزانه</option>
						</select>

					</div>

				</div>
				
		
			</div>
			
			<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 form-input-container">
				<div class="pull-left">
				<button type="submit" class="btn btn-success">دریافت گزارش</button>
				</div>
			</div>
			</div>
			</form>
		</div>
	</div>


  
<div class="row mt-5">   
    <div class="col-lg-12 mb-md-5">
    	<h4>نمودار زمانی</h4>
    </div> 
    <div class="col-lg-12 mb-md-5">
    	<figure class="highcharts-figure">
		<div id="container"></div>
	</figure>
   </div>
</div>



@endsection

@section('additional_js_bottom')

<script type="text/javascript">
Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'نمودار تعداد مشتریان ثبت شده'
    },
    xAxis: {
        categories: [
        		"تاریخ"
        		],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'تعداد مشتری'
        }
    },
    tooltip: {
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: [
    
    	@foreach($chart1DataArray as $key=>$chartData)
    		{
    			name: '{{$key}}',
    			data: [{{$chartData}}]
    		}
    		@if($key!=array_key_last($chart1DataArray))
    		,
    		@endif
    	@endforeach
    
    ]
});

</script>


@endsection
