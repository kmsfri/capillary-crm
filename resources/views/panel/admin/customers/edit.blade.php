@extends('panel.layout.master')
@section('main_content')

    <div class="row mb-md-5">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2>ویرایش مشتری</h2>

            </div>

            <div class="pull-left">

                <a class="btn btn-primary" href="{{ route('customers.index') }}"> بازگشت</a>

            </div>

        </div>

    </div>
	
	
	@if(!empty($customer))
		
		<div class="alert alert-danger">

            <strong>اخطار!</strong>
			
			<h5>دسترسی به بخش مشتریان به زودی به طور کامل بسته خواهد شد. لطفا تا حد ممکن عملیات مربوطه را برای سرنخ انجام دهید</h5>
            

        </div>
	


    @endif


    @if (count($errors) > 0)

        <div class="alert alert-danger">

            <strong>خطا!</strong> در مقادیر ورودی ایراداتی وجود دارد.<br><br>

            <ul>

                @foreach ($errors->all() as $error)

                    <li>{{ $error }}</li>

                @endforeach

            </ul>

        </div>

    @endif


    {!! Form::model($customer, ['method' => 'PATCH','route' => ['customers.update', $customer->id]]) !!}

    <div class="row">

        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>نام:</strong>

                {!! Form::text('first_name', old('first_name',$customer->first_name), array('placeholder' => '','class' => 'form-control')) !!}

            </div>

        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>نام خانوادگی:</strong>

                {!! Form::text('last_name', old('last_name',$customer->last_name), array('placeholder' => '','class' => 'form-control')) !!}

            </div>

        </div><div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>فروشگاه:</strong>

                {!! Form::text('business_name', old('business_name',$customer->business_name), array('placeholder' => '','class' => 'form-control')) !!}

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>استان:</strong>

                {!! Form::text('state', old('state',$customer->state), array('placeholder' => '','class' => 'form-control')) !!}

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>شهر:</strong>

                {!! Form::text('city', old('city',$customer->city), array('placeholder' => '','class' => 'form-control')) !!}

            </div>

        </div><div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>آدرس:</strong>

                {!! Form::text('address', old('address',$customer->address), array('placeholder' => '','class' => 'form-control')) !!}

            </div>

        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>تلفن ۱:</strong>

                {!! Form::text('phone', old('phone',$customer->phone), array('placeholder' => '','class' => 'form-control')) !!}

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>تلفن ۲:</strong>

                {!! Form::text('phone2', old('phone2',$customer->phone2), array('placeholder' => '','class' => 'form-control')) !!}

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>تلفن ۳:</strong>

                {!! Form::text('phone3', old('phone3',$customer->phone3), array('placeholder' => '','class' => 'form-control')) !!}

            </div>

        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>موبایل:</strong>

                {!! Form::text('mobile', old('mobile',$customer->mobile), array('placeholder' => '','class' => 'form-control')) !!}

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>موبایل ۲:</strong>

                {!! Form::text('mobile2', old('mobile2',$customer->mobile2), array('placeholder' => '','class' => 'form-control')) !!}

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>موبایل ۳:</strong>

                {!! Form::text('mobile3', old('mobile3',$customer->mobile3), array('placeholder' => '','class' => 'form-control')) !!}

            </div>

        </div>
        
        
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>تلفن سپیدار:</strong>

                {!! Form::text('sepidar_phone', old('sepidar_phone',$customer->sepidar_phone), array('placeholder' => '','class' => 'form-control')) !!}

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>کد سپیدار:</strong>

                {!! Form::text('sepidar_code', old('sepidar_code',$customer->sepidar_code), array('placeholder' => '','class' => 'form-control')) !!}

            </div>

        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 text-center">

            <button type="submit" class="btn btn-primary">ثبت</button>

        </div>

    </div>

    {!! Form::close() !!}



@endsection
