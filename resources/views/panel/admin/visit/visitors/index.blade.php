@extends('panel.layout.master')
@section('main_content')
    <div class="row mb-md-5">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2>مدیریت ویزیتورها</h2>

            </div>

            <div class="pull-left">

                

            </div>

        </div>

    </div>


    @if ($message = Session::get('success'))

        <div class="alert alert-success">

            <p>{{ $message }}</p>

        </div>

    @endif


    <table class="table table-bordered">

        <tr>

            <th>ردیف</th>

            <th>نام و نام خانوادگی</th>

            <th>ایمیل</th>

            <th>نقش</th>
            
            <th>نمایش</th>
            
            <th>مناطق</th>

            <th width="280px">عملیات</th>

        </tr>

        @foreach ($data as $key => $user)

            <tr>

                <td>{{ ++$i }}</td>

                <td><a href="{{route('visit.show-visitor-report',$user->id)}}" target="_blank">{{ $user->name }}</a></td>

                <td>{{ $user->email }}</td>

                <td>

                    @if(!empty($user->getRoleNames()))

                        @foreach($user->getRoleNames() as $v)

                            <label class="badge badge-success">{{ $v }}</label>

                        @endforeach

                    @endif

                </td>
                
                
                <td>
                
                	<a class="btn btn-info" href="{{ route('visit.show-users-daily-flow-list',$user->id) }}">مسیرهای ویزیت</a>
					<a class="btn btn-danger" href="{{ route('users.showUserWalkedPaths',$user->id) }}">پیمایش ها</a>
                </td>
                
                
                <td>
                	@can('user-edit')
		            <a class="btn btn-info" href="{{ route('visit.visitors.set-specialist',$user->id) }}">ویرایش کارشناس</a>
		        @endcan
                	
                
                	<a class="btn btn-success" href="{{ route('users.edit-user-cities',$user->id) }}">ویرایش مناطق</a>
                </td>
                
                


                <td>
		     @can('user-list')
                    <a class="btn btn-info" href="{{ route('users.show',$user->id) }}">نمایش</a>
                    @endcan
                    
                    
                    		

	             <a class="btn btn-warning" href="{{ route('visit.set-daily-flow',$user->id) }}">تنظیم مسیر روزانه</a>

                </td>

            </tr>

        @endforeach

    </table>


    {!! $data->links() !!}



@endsection
