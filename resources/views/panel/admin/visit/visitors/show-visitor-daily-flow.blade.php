@extends('panel.layout.master')
@section('additional_js_top')

@endsection

@section('additional_css')
<link rel = "stylesheet" href = "{{asset('panel/plugins/jquery-flowchart-master/jquery.flowchart.min.css')}}" />
<link rel = "stylesheet" href = "{{asset('css/leaflet.css')}}" />
    
<style>
.flowchart-container {
	width: 100%;
	min-height: 400px;
	background: white;
	border: 1px solid #BBB;
	margin-bottom: 10px;
}

#chart_wrapper{
	direction: ltr;
	text-align: left;
}

#chart_wrapper .flowchart-operator .flowchart-operator-title{
	text-align: center;
}


.not-visited .flowchart-operator-title{
	background-color: #d90000;
	color: white;
}
.not-visited .flowchart-operator-title a{
	color: white;
}

.visiting .flowchart-operator-title{
	background-color: #6565ff;
	color: white;
}
.visiting .flowchart-operator-title a{
	color: white;
}


.visited .flowchart-operator-title{
	background-color: green;
	color: white;
}
.visited .flowchart-operator-title a{
	color: white;
}

.modal.show .modal-dialog{
	top: 15%;
}
#routeElementOperationsModal .close{
	margin-left: 0;
	margin-right: auto;
}

a.disabled {
  color: grey;
}

#visit_image_container{
	margin-top: 10px;
	margin-bottom: 15px;
}

#flowchartworkspace{
	overflow-x: scroll !important;
}
</style>
    
@stop
@section('main_content')

@if ($message = Session::get('danger'))

        <div class="alert alert-danger">

            <p>{{ $message }}</p>

        </div>

    @endif

    <div class="row mb-md-5">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2>مسیر ویزیت روزانه در تاریخ: 
                {{ \Morilog\Jalali\Jalalian::forge($nowInDate)->format('%A, %d %B %Y') }}
                </h2>

            </div>

            <div class="pull-left">

                <a class="btn btn-primary" href="{{ route('showDashboard') }}"> بازگشت</a>

            </div>

        </div>

    </div>
    


	@if ($message = Session::get('success'))

		<div class="alert alert-success">

		    <p>{{ $message }}</p>

		</div>

	@endif
    


    @if (count($errors) > 0)

        <div class="alert alert-danger">

            <strong>خطا!</strong> در مقادیر ورودی ایراداتی وجود دارد.<br><br>

            <ul>

                @foreach ($errors->all() as $error)

                    <li>{{ $error }}</li>

                @endforeach

            </ul>

        </div>

    @endif



    <div class="row">
	

    

        <div class="col-xs-12 col-sm-12 col-md-12">
		<div id="chart_wrapper">
			<div class="flowchart-container" id="flowchartworkspace"></div>
		</div>
            
        </div>
    </div>
    
    
    
    <div class="row mt-4">

        <div class="col-lg-12">
<h5>
                	مشاهده موقعیت های مکانی بر روی نقشه:
                </h5>


        </div>

    </div>
    
    
    <div class="row">
        
	<div class="col-xs-12 col-sm-12 col-md-12 mt-md-3 mb-md-5">

		<div class="form-group">

		    <div id="map"></div>

		</div>

	</div>

    </div>
    <div class="row">  
        
        <div class="col-xs-12 col-sm-12 col-md-12 form-input-container">

        	<div class="form-group">

            	<strong>توضیحات:</strong>

            	<textarea disabled class="form-control" style="height:100px" placeholder="" required>{{ old('visitRouteDescription',(!empty($visitRouteDescription)?$visitRouteDescription:'')) }}</textarea>

        	</div>

	</div>

        

    </div>
    
    

<div id="routeElementOperationsModal" class="modal" tabindex="-1" role="dialog">

  <form id="routeElementUpdateFrm" action="{{route('visit.update-daily-flow-operator')}}" method="POST">
  	{{csrf_field()}}
  	<input type="hidden" name="visit_route_id" value="{{$visitRouteID}}" autocomplete="off">
  	<input type="hidden" name="operator_id" id="operator_id" value="" autocomplete="off">
  	<input type="hidden" name="request_type" id="request_type" value="" autocomplete="off">
  	<input type="hidden" name="operator_description" id="operator_description" value="" autocomplete="off">
  </form>

  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
	<h5 class="modal-title" id="route_el_modal_title"></h5>
	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	  <span aria-hidden="true">&times;</span>
	</button>
      </div>
      <div class="modal-body text-center">
	<p><a id="setEntryDateTimeBtn" href="#" data-operatorID="">ثبت ورود</a></p>
	<p><a id="setExitDateTimeBtn" href="#" data-operatorID="">ثبت خروج</a></p>
	<p><a id="setInvoiceBtn" href="#" data-operatorID="">صدور فاکتور</a></p>
	<!--before: <p><a id="setDescriptionBtn" href="#" data-operatorID="">ثبت توضیحات</a></p>-->
	<p><a id="setDescriptionBtn" href="#" data-operatorID="">ثبت مشتری</a></p>
	<p><a id="showGeoLocationBtn" href="#" data-operatorID="">مشاهده موقعیت مکانی</a></p>
	<div id="setDescriptionContainer" class="hide mt-3">
		<div id="descriptionFieldsContainer">
			<div class="form-group">
				<label for="route_visit_description">متن توضیحات:</label>
				<textarea class="form-control route-visit-description" rows="5" id="route_visit_description_1" autocomplete="off"></textarea>
			</div>
		</div>
		<div class="form-group text-center">
			<button id="route_visit_description_submit" type="button" class="btn btn-success">ثبت توضیحات</button>
			<button type="button" onclick="addNewDescriptionField()" class="btn btn-link">توضیحات جدید</button>
		</div>
	</div>
	
	
	
	<form id="visit_image_form" action="{{route('visit.update-daily-flow-operator')}}" method="POST" enctype='multipart/form-data'>
		{{csrf_field()}}
		
		
		<input type="hidden" name="visit_route_id" value="{{$visitRouteID}}" autocomplete="off">
	  	<input type="hidden" name="operator_id" id="visit_image_form_operator_id" value="" autocomplete="off">
	  	<input type="hidden" name="request_type" value="store_visit_file" autocomplete="off">
		
		<div class="col-lg-12 text-right" style="margin-top: 20px; padding-top: 20px; border-top: 1px solid #e9ecef">
			<p><input type="file" onchange="readURL(this,'visit_image_container'); $('#upload_visit_image_btn_container').removeClass('hide');" name="visit_image" id="visit_image" value="{{ old('visit_image') }}" autocomplete="off" ></p>
		</div>
		
		<div class="col-lg-12">
			<div id="visit_image_container"></div>
		</div>
		
		<div id="upload_visit_image_btn_container" class="col-lg-12 hide">
			<div class="form-group text-center">
				<button id="upload_visit_image_btn" type="submit" class="btn btn-success">آپلود فایل</button>
			</div>
		</div>
	</form>
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
      </div>
    </div>
  </div>
</div>
    
    


@endsection
@section('additional_js_bottom')
<script src = "{{asset('js/leaflet.js')}}"></script>

<script src = "{{asset('panel/plugins/jquery-flowchart-master/jquery-ui.min.js')}}"></script>
<script src = "{{asset('panel/plugins/jquery-flowchart-master/jquery.flowchart.min.js')}}"></script>

    <script type="text/javascript">
	
		var descriptionFieldCount = 1;
		
	
		/* global $ */
		$(document).ready(function() {
			var $flowchart = $('#flowchartworkspace');
			var $container = $flowchart.parent();


			
			


			function getOperatorData($element) {
				var nbInputs = parseInt($element.data('nb-inputs'), 10);
				var nbOutputs = parseInt($element.data('nb-outputs'), 10);
				var data = {
					properties: {
						title: $element.text(),
						inputs: {},
						outputs: {}
					}
				};

				var i = 0;
				for (i = 0; i < nbInputs; i++) {
					data.properties.inputs['input_' + i] = {
						label: 'Input ' + (i + 1)
					};
				}
				for (i = 0; i < nbOutputs; i++) {
					data.properties.outputs['output_' + i] = {
						label: 'Output ' + (i + 1)
					};
				}

				return data;
			}
			
			
			// Apply the plugin on a standard, empty div...
			
			var defaultFlowData = '';
			if (typeof defaultFlowchartData !== 'undefined') {
				/*
				$flowchart.flowchart({
					data: defaultFlowchartData,
					defaultSelectedLinkColor: '#000055',
					grid: 10,
					multipleLinksOnInput: true,
					multipleLinksOnOutput: true
				});
				*/
				defaultFlowData = defaultFlowchartData;
				
			}



			//-----------------------------------------
			//--- operator and link properties
			//--- start
			var $operatorProperties = $('#operator_properties');
			$operatorProperties.hide();
			var $linkProperties = $('#link_properties');
			$linkProperties.hide();
			var $operatorTitle = $('#operator_title');
			var $linkColor = $('#link_color');

			$flowchart.flowchart({
				data: defaultFlowData,
				onOperatorSelect: function(operatorId) {
					$operatorProperties.show();
					$operatorTitle.val($flowchart.flowchart('getOperatorTitle', operatorId));
					return true;
				},
				onOperatorUnselect: function() {
					$operatorProperties.hide();
					return true;
				},
				onLinkSelect: function(linkId) {
					$linkProperties.show();
					$linkColor.val($flowchart.flowchart('getLinkMainColor', linkId));
					return true;
				},
				onLinkUnselect: function() {
					$linkProperties.hide();
					return true;
				}
			});

			$operatorTitle.keyup(function() {
				var selectedOperatorId = $flowchart.flowchart('getSelectedOperatorId');
				if (selectedOperatorId != null) {
					$flowchart.flowchart('setOperatorTitle', selectedOperatorId, $operatorTitle.val());
				}
			});

			$linkColor.change(function() {
				var selectedLinkId = $flowchart.flowchart('getSelectedLinkId');
				if (selectedLinkId != null) {
					$flowchart.flowchart('setLinkMainColor', selectedLinkId, $linkColor.val());
				}
			});
			//--- end
			//--- operator and link properties
			//-----------------------------------------

			//-----------------------------------------
			//--- delete operator / link button
			//--- start
			$flowchart.parent().siblings('.delete_selected_button').click(function() {
				$flowchart.flowchart('deleteSelected');
			});
			//--- end
			//--- delete operator / link button
			//-----------------------------------------



			//-----------------------------------------
			//--- create operator button
			//--- start
			var operatorI = 0;
			$flowchart.parent().siblings('.create_operator').click(function() {
				var operatorId = 'created_operator_' + operatorI;
				var operatorData = {
					top: ($flowchart.height() / 2) - 30,
					left: ($flowchart.width() / 2) - 100 + (operatorI * 10),
					properties: {
						title: 'Operator ' + (operatorI + 3),
						inputs: {
							input_1: {
								label: 'Input 1',
							}
						},
						outputs: {
							output_1: {
								label: 'Output 1',
							}
						}
					}
				};

				operatorI++;

				$flowchart.flowchart('createOperator', operatorId, operatorData);

			});
			//--- end
			//--- create operator button
			//-----------------------------------------




			//-----------------------------------------
			//--- draggable operators
			//--- start
			//var operatorId = 0;
			var $draggableOperators = $('.draggable_operator');
			$draggableOperators.draggable({
				cursor: "move",
				opacity: 0.7,

				// helper: 'clone',
				appendTo: 'body',
				zIndex: 1000,

				helper: function(e) {
					var $this = $(this);
					var data = getOperatorData($this);
					return $flowchart.flowchart('getOperatorElement', data);
				},
				stop: function(e, ui) {
					var $this = $(this);
					var elOffset = ui.offset;
					var containerOffset = $container.offset();
					if (elOffset.left > containerOffset.left &&
						elOffset.top > containerOffset.top &&
						elOffset.left < containerOffset.left + $container.width() &&
						elOffset.top < containerOffset.top + $container.height()) {

						var flowchartOffset = $flowchart.offset();

						var relativeLeft = elOffset.left - flowchartOffset.left;
						var relativeTop = elOffset.top - flowchartOffset.top;

						var positionRatio = $flowchart.flowchart('getPositionRatio');
						relativeLeft /= positionRatio;
						relativeTop /= positionRatio;

						var data = getOperatorData($this);
						data.left = relativeLeft;
						data.top = relativeTop;

						$flowchart.flowchart('addOperator', data);
					}
				}
			});
			//--- end
			//--- draggable operators
			//-----------------------------------------


			//-----------------------------------------
			//--- save and load
			//--- start
			function Flow2Text() {
				var data = $flowchart.flowchart('getData');
				$('#flowchart_data').val(JSON.stringify(data, null, 2));
			}
			$('#get_data').click(Flow2Text);

			function Text2Flow() {
				var data = JSON.parse($('#flowchart_data').val());
				$flowchart.flowchart('setData', data);
			}
			$('#set_data').click(Text2Flow);

			/*global localStorage*/
			function SaveToLocalStorage() {
				if (typeof localStorage !== 'object') {
					alert('local storage not available');
					return;
				}
				Flow2Text();
				localStorage.setItem("stgLocalFlowChart", $('#flowchart_data').val());
			}
			$('#save_local').click(SaveToLocalStorage);

			function LoadFromLocalStorage() {
				if (typeof localStorage !== 'object') {
					alert('local storage not available');
					return;
				}
				var s = localStorage.getItem("stgLocalFlowChart");
				if (s != null) {
					$('#flowchart_data').val(s);
					Text2Flow();
				}
				else {
					alert('local storage empty');
				}
			}
			$('#load_local').click(LoadFromLocalStorage);
			//--- end
			//--- save and load
			//-----------------------------------------
			
		


		});


		@if(!empty($visitRouteFlow))
			var defaultFlowchartData = {!! json_encode($visitRouteFlow) !!};
			
			
			
			
			if (false) console.log('remove lint unused warning', defaultFlowchartData);
		@endif
		
		
		
		
		
		
		
		
		
		function showRouteElementModal(routeVisitElementMetaData, operatorTitle){
		
		
			$('#descriptionFieldsContainer').html('');
			descriptionFieldCount = 1;

			$('#routeElementOperationsModal #setEntryDateTimeBtn').removeClass('disabled');
			$('#routeElementOperationsModal #setExitDateTimeBtn').removeClass('disabled');
			$('#routeElementOperationsModal #setInvoiceBtn').removeClass('disabled');
			$('#routeElementOperationsModal #showGeoLocationBtn').removeClass('disabled');
			$('#routeElementOperationsModal #upload_visit_image_btn').removeAttr('disabled');
			$('#routeElementOperationsModal #setDescriptionBtn').removeClass('disabled');
			$('#routeElementOperationsModal #setDescriptionContainer #route_visit_description_submit').removeAttr("disabled");
			$('#routeElementOperationsModal #setDescriptionContainer .route-visit-description').removeAttr("disabled");
			$('#routeElementOperationsModal #setDescriptionContainer .route-visit-description').html("");


			if(routeVisitElementMetaData['entry_datetime']==""){
				$('#routeElementOperationsModal #setEntryDateTimeBtn').data('operatorID',routeVisitElementMetaData['chart_operator_id']);
				
				
				
				
			}else{
				$('#routeElementOperationsModal #setEntryDateTimeBtn').data('operatorID',"-1");
				$('#routeElementOperationsModal #setEntryDateTimeBtn').addClass('disabled');
				
			}
			
			
			if(routeVisitElementMetaData['exit_datetime']=="" && routeVisitElementMetaData['entry_datetime']!=""){
				$('#routeElementOperationsModal #setExitDateTimeBtn').data('operatorID',routeVisitElementMetaData['chart_operator_id']);
				
				
			}else if(routeVisitElementMetaData['entry_datetime']==""){
			
				$('#routeElementOperationsModal #setExitDateTimeBtn').data('operatorID',"-1");
				$('#routeElementOperationsModal #setExitDateTimeBtn').addClass('disabled');
				
				
			}else{
				$('#routeElementOperationsModal #setExitDateTimeBtn').data('operatorID',"-2");
				$('#routeElementOperationsModal #setExitDateTimeBtn').addClass('disabled');

			}
			
			
			
			
			if($('#routeElementOperationsModal #setEntryDateTimeBtn').data('operatorID')=="-1" && $('#routeElementOperationsModal #setExitDateTimeBtn').data('operatorID')!="-1" && $('#routeElementOperationsModal #setExitDateTimeBtn').data('operatorID')!="-2"){
			
				$('#routeElementOperationsModal #setInvoiceBtn').data('operatorID',routeVisitElementMetaData['chart_operator_id']);
				$('#routeElementOperationsModal #showGeoLocationBtn').data('operatorID',routeVisitElementMetaData['chart_operator_id']);
				$('#routeElementOperationsModal #setDescriptionBtn').data('operatorID',routeVisitElementMetaData['chart_operator_id']);
				$('#routeElementOperationsModal #upload_visit_image_btn').removeClass('disabled');
				$('#routeElementOperationsModal #visit_image_form #visit_image_form_operator_id').val(routeVisitElementMetaData['chart_operator_id']);
				$('#routeElementOperationsModal #visit_image_form #upload_visit_image_btn_container').removeClass('hide');
			}else{
				$('#routeElementOperationsModal #setInvoiceBtn').data('operatorID',"-1");
				$('#routeElementOperationsModal #setInvoiceBtn').addClass('disabled');
				
				$('#routeElementOperationsModal #showGeoLocationBtn').data('operatorID',"-1");
				$('#routeElementOperationsModal #showGeoLocationBtn').addClass('disabled');
				
				$('#routeElementOperationsModal #setDescriptionBtn').data('operatorID',"-1");
				$('#routeElementOperationsModal #setDescriptionBtn').addClass('disabled');
				
				$('#routeElementOperationsModal #upload_visit_image_btn').addClass('disabled');
				$('#routeElementOperationsModal #visit_image_form #upload_visit_image_btn_container').addClass('hide');
				
				$('#routeElementOperationsModal #setDescriptionContainer #route_visit_description_submit').attr("disabled", true);
				
				$('#routeElementOperationsModal #setDescriptionContainer .route-visit-description').attr("disabled", true);
			}
			
			
			
			
		
			if(routeVisitElementMetaData['visit_description']!=='' || routeVisitElementMetaData['visit_description'].length>0){
				$('#routeElementOperationsModal #setDescriptionContainer').removeClass('hide');
				
				console.log(routeVisitElementMetaData['visit_description']);
				if(Array.isArray(routeVisitElementMetaData['visit_description'])){

					
					var descriptionDataArray = routeVisitElementMetaData['visit_description'];
					
					
					
					descriptionDataArray.forEach(function(rowValue){
						
						
						if(!document.getElementById("route_visit_description_"+descriptionFieldCount)) {
							$('#descriptionFieldsContainer').append('\
										<div class="form-group">\
											<label for="route_visit_description">متن توضیحات:</label>\
											<textarea class="form-control route-visit-description" rows="5" id="route_visit_description_'+descriptionFieldCount+'" autocomplete="off"></textarea>\
										</div>\
							');
						}
												
						
						
						
						$('#routeElementOperationsModal #route_visit_description_'+descriptionFieldCount).html(rowValue['value']);
						
						descriptionFieldCount++;
					});
					
					
					
				}else{
					$('#routeElementOperationsModal #route_visit_description_1').html(routeVisitElementMetaData['visit_description']);
				}
				
				
						
			}else{
				$('#routeElementOperationsModal #setDescriptionContainer').addClass('hide');
				$('#routeElementOperationsModal .route-visit-description').html("");
			}
			
			
			$("#routeElementOperationsModal #visit_image_container").html("");	


			if(isJsonString(routeVisitElementMetaData['visit_image'])){

				var decodedImagesArray = JSON.parse(routeVisitElementMetaData['visit_image']);

				var tempIDCounter = 0;
				decodedImagesArray.forEach(imageURL=>{
					tempIDCounter++;
					$("#routeElementOperationsModal #visit_image_container").append('<img id='+tempIDCounter+' style="max-width: 100%; margin-bottom: 15px;" class="img img-responsive" />');
        	                        $('#routeElementOperationsModal #visit_image_container img').removeClass('hide');
	                                $("#routeElementOperationsModal #visit_image_container #"+tempIDCounter).attr("src", imageURL);
				});

			}else if(routeVisitElementMetaData['visit_image']!==''){
			
				$("#routeElementOperationsModal #visit_image_container").append('<img style="max-width: 100%" class="img img-responsive" />');
				$('#routeElementOperationsModal #visit_image_container img').removeClass('hide');
				$("#routeElementOperationsModal #visit_image_container img").attr("src", routeVisitElementMetaData['visit_image']);	
			}
		
			
			
			
			$('#routeElementOperationsModal #route_el_modal_title').html(operatorTitle);
			
			
			$('#routeElementOperationsModal').modal('show');
		}
		
		
		$('#routeElementOperationsModal #setEntryDateTimeBtn').on('click',function(){
			
			//-1 entry
			//-2 exit
			
			var operatorID = $(this).data('operatorID');
			if(operatorID=="-1" || operatorID=="-2"){
				alert('شما قبلا ورود به این مکان را ثبت کرده اید');
				return;
			}
			
			if (confirm("آیا از ثبت زمان ورود مطمئنید؟") == true) {
			  
				$('#routeElementUpdateFrm #operator_id').val(operatorID);
				
				$('#routeElementUpdateFrm #request_type').val("set_entry_datetime");
				
				$('#routeElementUpdateFrm #operator_description').val("");
				
				$('#routeElementUpdateFrm').submit();
			  
			  
			} else {
				return;
			}
		});
		
		$('#routeElementOperationsModal #setExitDateTimeBtn').on('click',function(){
			
			
			var operatorID = $(this).data('operatorID');
			if(operatorID=="-2"){
				alert('شما قبلا خروج از این مکان را ثبت کرده اید');
				return;
			}else if(operatorID=="-1"){
				alert('شما هنوز ورود خود به این مکان را ثبت نکرده اید');
				return;
			}
			
			if (confirm("آیا از ثبت زمان خروج مطمئنید؟") == true) {
			  
				$('#routeElementUpdateFrm #operator_id').val(operatorID);
				
				$('#routeElementUpdateFrm #request_type').val("set_exit_datetime");
				
				$('#routeElementUpdateFrm #operator_description').val("");
				
				$('#routeElementUpdateFrm').submit();
			  
			  
			} else {
				return;
			}
		});
		
		
		
		
		$('#routeElementOperationsModal #setInvoiceBtn').on('click',function(){
		
			var operatorID = $(this).data('operatorID');
			if(operatorID=="-1"){
				alert('پس از ورود و تا قبل از خروج از موقعیت مکانی میتوانید فاکتور ثبت کنید.');
				return;
			}
			
			
			$('#routeElementUpdateFrm #operator_id').val(operatorID);
				
			$('#routeElementUpdateFrm #request_type').val("redirect_to_create_invoice");
			
			$('#routeElementUpdateFrm #operator_description').val("");
			
			$('#routeElementUpdateFrm').submit();
			
			
		
		});
		
		$('#routeElementOperationsModal #showGeoLocationBtn').on('click',function(){
		
			var operatorID = $(this).data('operatorID');
			if(operatorID=="-1"){
				alert('پس از ورود و تا قبل از خروج از این موقعیت مکانی میتوانید آن را بر روی نقشه مشاهده کنید');
				return;
			}
			
			
			$('#routeElementUpdateFrm #operator_id').val(operatorID);
				
			$('#routeElementUpdateFrm #request_type').val("redirect_to_show_geo_location");
			
			$('#routeElementUpdateFrm #operator_description').val("");
			
			$('#routeElementUpdateFrm').submit();
			
			
		
		});
		
		
		$('#routeElementOperationsModal #setDescriptionBtn').on('click',function(){
		
			var operatorID = $(this).data('operatorID');
			
			
			if(operatorID=="-1"){
				alert('پس از ورود و تا قبل از خروج از موقعیت مکانی میتوانید توضیحات ثبت کنید.');
				return;
			}
			
			
			window.location.href = "{{Route('clues.index')}}";
			
			//before
			//$('#routeElementOperationsModal #setDescriptionContainer').removeClass('hide');
			
			
		
		});


		
		$('#routeElementOperationsModal #setDescriptionContainer #route_visit_description_submit').on('click',function(){
		

		
			//foreach elements with class route-visit-description. and generate a json string
		
			var data = [];
		
			var fields = document.getElementsByClassName("route-visit-description");
			for (var i = 0; i < fields.length; i++) {
				
				var descriptionText = $(fields.item(i)).val();
				if(descriptionText!==""){
					data.push({label: i, value: descriptionText});
				}
		
			}
			 
	
		
			if(data.length==0){
				alert('لطفا توضیحات را وارد کنید');
				return;
			}
			
			var operatorID = $('#routeElementOperationsModal #setDescriptionBtn').data('operatorID');
		
			$('#routeElementUpdateFrm #operator_id').val(operatorID);
				
			$('#routeElementUpdateFrm #request_type').val("set_description");
			
			
			var jsonDescriptions = JSON.stringify(data);
			
			$('#routeElementUpdateFrm #operator_description').val(jsonDescriptions);
			
			$('#routeElementUpdateFrm').submit();
		
		});
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	$(document).ready(function(){

            <?php
            	$mapCenter = [
            		'lat' => '35.709165',
            		'lng' => '51.371681',
            	];
            	
            	
            	if(!empty($visitGeoLocationsForShowOnMap) && !empty($visitGeoLocationsForShowOnMap[0]['latitude']) && !empty($visitGeoLocationsForShowOnMap[0]['longitude'])){
            		$mapCenter = [
		    		'lat' => $visitGeoLocationsForShowOnMap[0]['latitude'],
		    		'lng' => $visitGeoLocationsForShowOnMap[0]['longitude'],
		    	];
            	}
            	
            ?>
            
            
            var visitGeoLocationsForShowOnMap = {!! json_encode($visitGeoLocationsForShowOnMap) !!};


            // Creating map options




            let centerLat = '<?php echo $mapCenter['lat']; ?>';
            let centerLng = '<?php echo $mapCenter['lng']; ?>';



     
            var mapOptions = {
                center: [centerLat,centerLng ],
                zoom: 10
            }

            // Creating a map object
            var map = new L.map('map', mapOptions);

            // Creating a Layer object
            var layer = new     L.TileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');

            // Adding layer to the map
            map.addLayer(layer);


            var marker;
            
            /*
            map.on('click', function(e) {
                if(marker)
                    map.removeLayer(marker);

                marker = L.marker(e.latlng).addTo(map);

                $('#latitude').val(e.latlng.lat);
                $('#longitude').val(e.latlng.lng)
            });

            if(marker){
                map.removeLayer(marker);
            }
            */
            
            
            
            console.log(visitGeoLocationsForShowOnMap);
            
            
	    visitGeoLocationsForShowOnMap.forEach(function(item){
	    
		let latlng = {lat:item.latitude, lng:item.longitude};

	    	marker = L.marker(latlng,{title: item.title}).addTo(map);
	    });
    
            
            
            

        });
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	</script>
	
	
	
	<script type="text/javascript">




		function isJsonString(str) {
			try {
				JSON.parse(str);
			} catch (e) {
				return false;
			}
			return true;
		}



		function readURL(input,img_container_id) {

		    $(input).removeClass('required');

		    $('#'+img_container_id).empty();
		    for (let i = 0; i < input.files.length; i++) {
		        if (input.files && input.files[i]) {
		            var reader = new FileReader();

		            var newImgElementID = img_container_id+"_img_"+i;
		            $('#'+img_container_id).append('<img id="'+newImgElementID+'" class="img img-responsive" />');

		            reader.onload = function (e) {


		                $('#'+newImgElementID)
		                    .attr('src', e.target.result)
		                    .height(100);
		            };

		            reader.readAsDataURL(input.files[i]);
		            //$('#'+img_id).removeClass('hide');


		        }




		    }


		}
		
		
		
		function addNewDescriptionField(){
			
			descriptionFieldCount++;
			
			$('#descriptionFieldsContainer').append('\
						<div class="form-group">\
							<label for="route_visit_description">متن توضیحات:</label>\
							<textarea class="form-control route-visit-description" rows="5" id="route_visit_description_'+descriptionFieldCount+'" autocomplete="off"></textarea>\
						</div>\
			');
			
		}



	    </script>
	
	
@endsection
