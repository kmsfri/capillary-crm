@extends('panel.layout.master')
@section('main_content')

    <div class="row mb-md-5">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2>ویرایش کاربر کارشناس برای کاربر: {{$visitorUser->name}}</h2>

            </div>

            <div class="pull-left">

                <a class="btn btn-primary" href="{{ route('visit.show-visitors') }}"> بازگشت</a>

            </div>

        </div>

    </div>


    @if (count($errors) > 0)

        <div class="alert alert-danger">

            <strong>خطا!</strong> در مقادیر ورودی ایراداتی وجود دارد.<br><br>

            <ul>

                @foreach ($errors->all() as $error)

                    <li>{{ $error }}</li>

                @endforeach

            </ul>

        </div>

    @endif
    
    
    @if ($message = Session::get('success'))

        <div class="alert alert-success">

            <p>{{ $message }}</p>

        </div>

    @endif
    
    @if ($message = Session::get('danger'))

        <div class="alert alert-danger">

            <p>{{ $message }}</p>

        </div>

    @endif
    
    
    
    
    
    <div class="row">    
    <div class="col-lg-12 mb-md-5">
       
        <div class="row">
	    
	    
	    
	    
	    
	    
	    <div class="col-xs-12 col-sm-12 col-md-3 form-input-container">

                    <p>کارشناس فعلی: {{!empty($parentUser)?$parentUser->name:'تعیین نشده'}}</p>
            </div>


	    

	
	
        </div>
        
        <form action="{{route('visit.visitors.update-specialist-user')}}" method="POST">
        <div class="row">
	    
	    
	    
	    
	    
	    
	    @csrf
	    
	    <input type="hidden" name="visitor_user_id" value="{{$visitorUser->id}}">
	    
	    <div class="col-xs-12 col-sm-12 col-md-3 form-input-container">

                <div class="form-group">

                    <strong>کاربر کارشناس(*):</strong>

                    <select required  name='specialist_user_id' id='specialist_user_id' class='form-control pull-right' > 				<!-- TODO: init vars -->
                        <option {{(!old('specialist_user_id', isset($visitorUser->parent_user_id) ? $visitorUser->parent_user_id : '')? 'selected' : '')}} value="">انتخاب کنید</option>
                        @foreach($specialistUsers as $specialistUser)
                            <option @if(old('specialist_user_id', isset($visitorUser->parent_user_id) ? $visitorUser->parent_user_id : '')==$specialistUser->id) selected @endif value="{{$specialistUser->id}}" >{{$specialistUser->name}}</option>
                        @endforeach
                    </select>

                </div>

            </div>



            
            
            <div class="col-xs-12 col-sm-12 col-md-2 form-input-container">

                <div class="form-group">

                    <strong>&nbsp;</strong>

                    <button type="submit" class="btn btn-success">ثبت تغییرات</button>

                </div>

            </div>
	    
	    
	    
	    
	    
	    

	
	
        </div>
        </form>

    </div>
</div>
    
    
   

@endsection
