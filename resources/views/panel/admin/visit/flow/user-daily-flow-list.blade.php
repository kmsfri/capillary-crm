@extends('panel.layout.master')
@section('main_content')
    <div class="row mb-md-5">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2>مسیرهای ویزیت روزانه برای کاربر: {{$visitorUser->name}}</h2>

            </div>

            <div class="pull-left">

                

            </div>

        </div>

    </div>


    @if ($message = Session::get('success'))

        <div class="alert alert-success">

            <p>{{ $message }}</p>

        </div>

    @endif
    
    
    


    <table class="table table-bordered">

        <tr>

            <th>ردیف</th>

            <th>تاریخ ویزیت</th>

            <th>ایجاد کننده</th>
            
            <th>زمان ایجاد</th>
			
			<th>گزارش</th>
	    @can('visit-set-daily-flow')
            <th width="280px">عملیات</th>
            @endcan

        </tr>

        @foreach ($data as $key => $visitRoute)

            <tr>

                <td>{{ ++$i }}</td>

                <td>{{ \Morilog\Jalali\Jalalian::forge($visitRoute->visit_at_date)->format('%A, %d %B %Y') }}</td>

                <td>{{ $visitRoute->creator()->first()->email }}</td>
                
                <td>{{ \Morilog\Jalali\Jalalian::forge($visitRoute->created_at)->format('%A, %d %B %Y') }}</td>
				
		<td>
	             <a class="btn btn-success" href="{{ route('visit.show-users-daily-flow-report',['user_id'=>$visitorUser->id,'visit_at_date'=>$visitRoute->visit_at_date]) }}">گزارش بازدید</a>
                </td>
		 @can('visit-set-daily-flow')
                <td>
	             <a class="btn btn-warning" href="{{ route('visit.set-daily-flow',['user_id'=>$visitorUser->id,'visit_at_date'=>$visitRoute->visit_at_date]) }}">ویرایش</a>
                </td>
                @endcan

            </tr>

        @endforeach

    </table>


    {!! $data->links() !!}



@endsection
