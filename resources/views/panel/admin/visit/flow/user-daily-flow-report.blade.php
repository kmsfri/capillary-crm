@extends('panel.layout.master')
@section('main_content')

<?php
	function isJson($string) {
           json_decode($string);
           return json_last_error() === JSON_ERROR_NONE;
        }
?>

    <div class="row mb-md-5">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2>گزارش ویزیت کاربر: {{$visitorUser->name}} در تاریخ 
				{{ \Morilog\Jalali\Jalalian::forge($visitRoute->visit_at_date)->format('%A, %d %B %Y') }}
				</h2>

            </div>

            <div class="pull-left">

                

            </div>

        </div>

    </div>


    @if ($message = Session::get('success'))

        <div class="alert alert-success">

            <p>{{ $message }}</p>

        </div>

    @endif
    
    
    


    <table class="table table-bordered">

        <tr>

            <th>ردیف</th>

            <th>عنوان</th>

            <th>نوع مکان</th>
            
            <th>دسترسی</th>
			
			<th>زمان ورود</th>
			
			<th>زمان خروج</th>
			
			<th>توضیحات</th>
			
			<th>تصویر ویزیت</th>
			
        </tr>

		@php
			$i=0;
		@endphp
        @foreach ($userVisitRouteReportData as $key => $reportRow)

            <tr>

                <td>{{ ++$i }}</td>
				
				<td>{{ $reportRow['title'] }}</td>
				
				<td>{{ ($reportRow['type']==1)?'کسب و کار':'مکان جغرافیایی' }}</td>
				
				<td>
				<a target="_blank" href="{{($reportRow['type']==1)?route('businesses.show',$reportRow['business_id']):      ('https://maps.google.com/?q='.$reportRow['latitude'].','.$reportRow['longitude'])      }}">
				{{($reportRow['type']==1)?'مشاهده کسب و کار':'مشاهده موقعیت'}}
				</a>
				</td>

                <td>
						{{ 
							!empty($reportRow['entry_datetime'])?
							\Morilog\Jalali\Jalalian::forge($reportRow['entry_datetime'])->format('%A, %d %B %Y - H:i:s'):
							'-'
						}}
				</td>
				
				<td>
						{{ 
							!empty($reportRow['exit_datetime'])?
							\Morilog\Jalali\Jalalian::forge($reportRow['exit_datetime'])->format('%A, %d %B %Y - H:i:s'): 
							'-'
						}}
				</td>

				@php
				
				$visitDescriptionArray = [];
				
				if(is_array($reportRow['visit_description'])){ 
					foreach($reportRow['visit_description'] as $key=>$descRow){
						$visitDescriptionArray[] = $descRow['value'].( (count($reportRow['visit_description'])!=($key+1))?'<hr>':'' );
					}
				}else{
					$visitDescriptionArray[] = $reportRow['visit_description'];
				}
				
				@endphp

				
               <td>
			   @foreach($visitDescriptionArray as $descRow)
			   {!! !empty($descRow)?$descRow:'-' !!}
			   @endforeach
			   
			   </td>
               
               <td>
		<?php
               if(!empty($reportRow['visit_image'])){
			if(isJson($reportRow['visit_image'])){
				$decodedReportVisitRowImages = json_decode($reportRow['visit_image'],true);
				foreach($decodedReportVisitRowImages as $key=>$visitRowImageURL){
		?>
					<a href="{{$visitRowImageURL}}" target="_blank">مشاهده فایل {{$key+1}}</a>
					{!!(count($decodedReportVisitRowImages)!=($key+1))?'<hr>':''!!}
		<?php
				}
			}else{
		?>
		               <a href="{{$reportRow['visit_image']}}" target="_blank">مشاهده فایل</a>
		<?php
			}
               }else{
		?>
               -
		<?php
               }
		?>
               </td>

            </tr>

        @endforeach
 
    </table>



@endsection
