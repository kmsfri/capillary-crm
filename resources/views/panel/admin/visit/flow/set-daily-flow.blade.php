@extends('panel.layout.master')
@section('additional_js_top')

@endsection
@section('additional_css')

<link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css"/>
<link rel="stylesheet" href="https://unpkg.com/leaflet-geosearch@3.1.0/dist/geosearch.css"/>
<script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"></script>
<script src="https://unpkg.com/leaflet-geosearch@3.1.0/dist/bundle.min.js"></script>


<link rel = "stylesheet" href = "{{asset('panel/plugins/jquery-flowchart-master/jquery.flowchart.min.css')}}" />
    
<style>
.flowchart-container {
	width: 100%;
	min-height: 400px;
	background: white;
	border: 1px solid #BBB;
	margin-bottom: 10px;
}

#chart_wrapper{
	direction: ltr;
	text-align: left;
}

#chart_wrapper .flowchart-operator .flowchart-operator-title{
	text-align: center;
}


.route-element-type-container .form-check:first-child{
	margin-right: 0;
	padding-right: 0;
}

#search_box{
	margin-top: 0;
	position: absolute;
	z-index: 9999999;
	
}

#search_suggestion_box{


	background: white;
	z-index: 9999999999;
	position: absolute;
	margin-top: 40px;
	padding: 0 10px 0 10px;
	border-radius: 0 0 5px 5px;
	border: solid 1px #a6a6a6;
	max-height: calc(100% - 56px);
	overflow-y: scroll;

}

#search_suggestion_box ul li{

	list-style-type: none;
	border-bottom: solid 1px #c8c8c8;
	cursor: pointer;
	padding: 7px 0 7px 0;
	
}

#search_suggestion_box ul:last-child li:last-child{

	
	border-bottom: unset;
	
}

</style>
    
@stop
@section('main_content')

@if ($message = Session::get('danger'))

        <div class="alert alert-danger">

            <p>{{ $message }}</p>

        </div>

    @endif

    <div class="row mb-md-5">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2>برنامه ریزی روزانه ویزیت برای ویزیتور: {{$visitorUser->name}}</h2>

            </div>

            <div class="pull-left">

                <a class="btn btn-primary" href="{{ route('showDashboard') }}"> بازگشت</a>

            </div>

        </div>

    </div>


    @if (count($errors) > 0)

        <div class="alert alert-danger">

            <strong>خطا!</strong> در مقادیر ورودی ایراداتی وجود دارد.<br><br>

            <ul>

                @foreach ($errors->all() as $error)

                    <li>{{ $error }}</li>

                @endforeach

            </ul>

        </div>

    @endif



    {!! Form::open(array('route' => 'visit.store-daily-flow','method'=>'POST','id'=>'mainForm')) !!}

    <input id="visit_flowchart_json" name="visit_flowchart_json" type="hidden" value="{{!empty($visitRoute)?$visitRoute->visit_flowchart_json:''}}">
    <input type="hidden" name="visitor_id" value="{{$visitorUser->id}}">
    <input type="hidden" name="visit_route_id" value="{{!empty($visitRoute)?$visitRoute->id:''}}">
    <div class="row">
    
    
	<div class="col-xs-12 col-sm-12 col-md-3 form-input-container">

		<div class="form-group">

		    <strong>در تاریخ:</strong>

		    <input data-jdp type="text" name="visit_flow_date" {{(!empty($visitRouteAt)?'disabled':'')}} id="visit_flow_date" value="{{old('visit_flow_date',(!empty($visitRouteAt)?\Morilog\Jalali\Jalalian::forge($visitRouteAt)->format('%Y/%m/%d'):'') )}}" class="form-control" placeholder="">

		</div>

	</div>
    </div>
    <div class="row">
	
	<div class="col-xs-12 col-sm-12 col-md-12 form-input-container route-element-type-container mb-3">
	
		<div class="form-check form-check-inline">
		  <input class="form-check-input" type="radio" id="inlineRadio1" name="routeElementType" value="1" autocomplete="off">
		  <label class="form-check-label" for="inlineRadio1">&nbsp;افزودن کسب و کار</label>
		</div>
		<div class="form-check form-check-inline">
		  <input class="form-check-input" type="radio" id="inlineRadio2" name="routeElementType" value="2" checked="checked" autocomplete="off">
		  <label class="form-check-label" for="inlineRadio2">&nbsp;افزودن موقعیت مکانی</label>
		</div>
		
	</div>
    </div>
    
    
    <div class="row">
    	<div class="col-xs-12 col-sm-12 col-md-3 form-input-container">

                <div class="form-group">

                    <strong>استان:</strong>

                    <select name='state_id' id='state_id' class='form-control pull-right' onChange="change_list('state_id','city_id','loading_gif_cities','{{route('getCities')}}',['region_id']); panToRegion('state_id');"> 				<!-- TODO: init vars -->
                        <option {{(!old('state_id', isset($business->state_id) ? $business->state_id : '')? 'selected' : '')}} value="">انتخاب کنید</option>
                        @foreach($states as $state)
                            <option data-latitude="{{$state->latitude}}" data-longitude="{{$state->longitude}}" @if(old('state_id', isset($business->state_id) ? $business->state_id : '')==$state->id) selected @endif value="{{$state->id}}" >{{$state->unit_name}}</option>
                        @endforeach
                    </select>

                </div>

            </div>



            <div class="col-xs-12 col-sm-12 col-md-3 form-input-container">

                <div class="form-group">

                    <strong>شهر:</strong>

                    <select name='city_id' id='city_id' class='form-control pull-right' onChange="change_list('city_id','region_id','loading_gif_regions','{{route('getCities')}}'); panToRegion('city_id');">
                        
                    </select>

                </div>

            </div>


            <div class="col-xs-12 col-sm-12 col-md-3 form-input-container">

                <div class="form-group">

                    <strong>منطقه:</strong>

                    <select  name='region_id' id='region_id' class='form-control pull-right' onChange="panToRegion('region_id');">
                        
                    </select>

                </div>

            </div>
    
	</div>    
    
    
	<div id="routeElementType1Container" class="row" style="display:none">
	
		<div class="col-xs-12 col-sm-12 col-md-3 form-input-container">

			<div class="form-group">

			    <strong>کسب و کار:</strong>

			    <select required  name='business_id' id='business_id' class='form-control pull-right'>
				<option value="">انتخاب کنید</option>
				@foreach($businesses as $key=>$business)
				    <option @if(old('business_id')==$business->id) selected @endif value="{{$business->id}}" >{{$business->business_name}}</option>
				@endforeach
			    </select>

			</div>

		</div>
		
		
		<div class="col-xs-12 col-sm-12 col-md-2 form-input-container">

			<div class="form-group">

			    <strong>&nbsp;</strong>

			    <button type="button" id="addBusinessToVisitFlowChartBtn" class="btn btn-primary">افزودن کسب و کار</button>

			</div>

		</div>
		
        </div>
        
        
        <div id="routeElementType2Container" class="row">
        
        	<input type="hidden" id="latitude" value="">
        	<input type="hidden" id="longitude" value="">
        	
        	
        	<div class="col-xs-12 col-sm-12 col-md-9"> 

			<div class="form-group">

			    <strong>عنوان موقعیت مکانی:</strong>

			    <input type="text" id="geo_location_title" placeholder="مثال: نام فروشگاه، شهر، خیابان، یا آدرس..." class="form-control">

			</div>

		</div>
        
		<div class="col-xs-12 col-sm-12 col-md-9 mt-md-3 mb-md-5">

			<div class="form-group">

				<input type="text" id="search_box" placeholder="جستجوی مکان" class="form-control col-sm-12 col-md-7">
				<div id="search_suggestion_box" class="form-control col-sm-12 col-md-7 hide">
					<ul></ul>
				</div>
				
				<!--
				<datalist id="search_suggestion_box">
				</datalist>
				-->

				<div id="map"></div>

			</div>

		</div>
		

		
		
		
		
		<div class="col-xs-12 col-sm-12 col-md-2 form-input-container">

			<div class="form-group">

			    <strong>&nbsp;</strong>

			    <button type="button" id="addLocationToVisitRouteBtn" class="btn btn-primary">افزودن موقعیت مکانی</button>

			</div>

		</div>
    
    	</div>
    	
    	
    	
    <div class="row">

        <div class="col-xs-12 col-sm-12 col-md-12">
		<div id="chart_wrapper">
			<div class="flowchart-container" id="flowchartworkspace"></div>
		</div>
            
        </div>
        
        
        
        <div class="col-xs-12 col-sm-12 col-md-12 form-input-container">

        	<div class="form-group">

            	<strong>توضیحات:</strong>

            	<textarea class="form-control" style="height:100px" name="visit_route_description" placeholder="" required>{{ old('visit_route_description',(!empty($visitRoute)?$visitRoute->visit_route_description:'')) }}</textarea>

        	</div>

	</div>

        
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">

            <button id="submitFormBtn" type="button" class="btn btn-primary">ثبت</button>

        </div>

    </div>

    {!! Form::close() !!}



@endsection
@section('additional_js_bottom')


<script src = "{{asset('panel/plugins/jquery-flowchart-master/jquery-ui.min.js')}}"></script>
<script src = "{{asset('panel/plugins/jquery-flowchart-master/jquery.flowchart.min.js')}}"></script>


<script src = "{{asset('js/leaflet.js')}}"></script>
	
	<script>

	/*
	$('#search_box').on('input', function() {
		var value = $(this).val();
		selectedLat = $('#search_suggestion_box [value="' + value + '"]').data('lat');
		selectedLng = $('#search_suggestion_box [value="' + value + '"]').data('lng');
		


		


		map.panTo([selectedLat, selectedLng]);
		map.setZoom(15);

		
		
		
	});
	*/
	
	$('body').on('click', ' #search_box', function() {
		$('#search_suggestion_box').toggleClass('hide');
	});
	
	$('body').on('click', ' #map', function() {
		$('#search_suggestion_box').addClass('hide');
	});
	
	$('#search_suggestion_box').on('click', ' ul li', function() {
		selectedLat = $(this).data('lat');
		selectedLng = $(this).data('lng');
		
		$('#search_box').val($(this).html());
		$('#search_suggestion_box').addClass('hide');
		
		
		map.panTo([selectedLat, selectedLng]);
		map.setZoom(17);
	});
	
	$('#search_box').on('keyup', function() {
		 if (this.value.length > 3) {
			 
			$('#search_suggestion_box').removeClass('hide');
			 
			var searchKey = $('#search_box').val();
			
			
			 $.ajax({
                url: 'https://crmmap.sepidarang.com/neshan/search.php',
                type: 'GET',
                cache:false,
                data: {"search_key":searchKey },
                success: function(data){

					var response = JSON.parse(data);
					locationsList = response['items'];
					//console.log(locationsList['items']);
					
					appendHtml = "";
					locationsList.forEach(locationItem => {
						appendHtml += '<li data-lat="'+locationItem.location.y+'" data-lng="'+locationItem.location.x+'">'+locationItem.title+'('+locationItem.address+' - '+locationItem.region+')</li>';
					});
					
					$('#search_suggestion_box ul').html(appendHtml);
					

					/*
                    $('#'+child_el_id).html('');
                    html_str='<option value="" >انتخاب کنید</option>';
                    $('#'+child_el_id).append(html_str);
                    $.each(data, function( index, value ) {
                        html_str='<option data-latitude="'+value.latitude+'" data-longitude="'+value.longitude+'" value="'+value.id+'" >'+value.title+'</option>';
                        $('#'+child_el_id).append(html_str);
                    });

                    depend_element_ids.forEach(function(){
                        $('#'+depend_element_ids).empty();
                    });

                    $('#'+master_el_id).prop('disabled', false);
					*/
                    //$('#'+loading_gif_id).css('display', 'none');
                },
                error: function(data){
                    alert(data.responseText);
                    alert('دریافت اطلاعات با خطا مواجه شد!');
                    $('#'+master_el_id).prop('disabled', false);
                    //$('#'+loading_gif_id).css('display', 'none');
                }
            });
			
			
		 }else {
			 $('#search_suggestion_box ul').html('');
			 $('#search_suggestion_box').addClass('hide');
		 }
	});
	</script>


    <script type="text/javascript">
    
    var map;
    
    
		/* global $ */
		$(document).ready(function() {
		
		
			$('input[type=radio][name=routeElementType]').change(function() {
			    if (this.value == '1') {
				$('#routeElementType2Container').hide();
				$('#routeElementType1Container').show();
			    }
			    else if (this.value == '2') {
				$('#routeElementType2Container').show();
				$('#routeElementType1Container').hide();
			    }
			});
			
            

		    // Creating map options



		    let mapCenter = {lat:'35.709165', lng:'51.371681'};



		    var mapOptions = {
		        center: [mapCenter.lat,mapCenter.lng ],
		        zoom: 11
		    }

		    // Creating a map object
		    map = new L.map('map', mapOptions);

		    // Creating a Layer object
		    var layer = new     L.TileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');

		    // Adding layer to the map
		    map.addLayer(layer);


		    var marker;
		    map.on('click', function(e) {
		        if(marker)
		            map.removeLayer(marker);

		        marker = L.marker(e.latlng).addTo(map);

		        $('#latitude').val(e.latlng.lat);
		        $('#longitude').val(e.latlng.lng)
		    });
			
			
			/*
			const provider = new window.GeoSearch.OpenStreetMapProvider();
			const search = new GeoSearch.GeoSearchControl({
			  provider: provider,
			  style: 'bar',
			  updateMap: true,
			  autoClose: true,
			}); // Include the search box with usefull params. Autoclose and updateMap in my case. Provider is a compulsory parameter.

			L.marker([51.0, -0.09]).addTo(map).bindPopup('A pretty CSS3 popup.<br> Easily customizable.'); //Creates a marker at [latitude, longitude] coordinates.
			map.addControl(search); 
			*/
			

		    if(marker){
		        map.removeLayer(marker);
		    }

		    
		    navigator.geolocation.getCurrentPosition(function(location) {
				mapCenter.lat = location.coords.latitude;
				mapCenter.lng = location.coords.longitude;
		    
		    	map.panTo([location.coords.latitude, location.coords.longitude]);
		    	map.setZoom(17);

		    });
            

		
		
		
		
		
		
		
		
			

		
		
		
		
		
		
		
		
		
		
		
		
		
		
			var $flowchart = $('#flowchartworkspace');
			var $container = $flowchart.parent();


			
			


			function getOperatorData($element) {
				var nbInputs = parseInt($element.data('nb-inputs'), 10);
				var nbOutputs = parseInt($element.data('nb-outputs'), 10);
				var data = {
					properties: {
						title: $element.text(),
						inputs: {},
						outputs: {}
					}
				};

				var i = 0;
				for (i = 0; i < nbInputs; i++) {
					data.properties.inputs['input_' + i] = {
						label: 'Input ' + (i + 1)
					};
				}
				for (i = 0; i < nbOutputs; i++) {
					data.properties.outputs['output_' + i] = {
						label: 'Output ' + (i + 1)
					};
				}

				return data;
			}
			
			
			// Apply the plugin on a standard, empty div...
			
			var defaultFlowData = '';
			if (typeof defaultFlowchartData !== 'undefined') {
				/*
				$flowchart.flowchart({
					data: defaultFlowchartData,
					defaultSelectedLinkColor: '#000055',
					grid: 10,
					multipleLinksOnInput: true,
					multipleLinksOnOutput: true
				});
				*/
				defaultFlowData = defaultFlowchartData;
				
			}



			//-----------------------------------------
			//--- operator and link properties
			//--- start
			var $operatorProperties = $('#operator_properties');
			$operatorProperties.hide();
			var $linkProperties = $('#link_properties');
			$linkProperties.hide();
			var $operatorTitle = $('#operator_title');
			var $linkColor = $('#link_color');

			$flowchart.flowchart({
				data: defaultFlowData,
				onOperatorSelect: function(operatorId) {
					$operatorProperties.show();
					$operatorTitle.val($flowchart.flowchart('getOperatorTitle', operatorId));
					return true;
				},
				onOperatorUnselect: function() {
					$operatorProperties.hide();
					return true;
				},
				onLinkSelect: function(linkId) {
					$linkProperties.show();
					$linkColor.val($flowchart.flowchart('getLinkMainColor', linkId));
					return true;
				},
				onLinkUnselect: function() {
					$linkProperties.hide();
					return true;
				}
			});

			$operatorTitle.keyup(function() {
				var selectedOperatorId = $flowchart.flowchart('getSelectedOperatorId');
				if (selectedOperatorId != null) {
					$flowchart.flowchart('setOperatorTitle', selectedOperatorId, $operatorTitle.val());
				}
			});

			$linkColor.change(function() {
				var selectedLinkId = $flowchart.flowchart('getSelectedLinkId');
				if (selectedLinkId != null) {
					$flowchart.flowchart('setLinkMainColor', selectedLinkId, $linkColor.val());
				}
			});
			//--- end
			//--- operator and link properties
			//-----------------------------------------

			//-----------------------------------------
			//--- delete operator / link button
			//--- start
			$flowchart.parent().siblings('.delete_selected_button').click(function() {
				$flowchart.flowchart('deleteSelected');
			});
			//--- end
			//--- delete operator / link button
			//-----------------------------------------



			//-----------------------------------------
			//--- create operator button
			//--- start
			var operatorI = 0;
			$flowchart.parent().siblings('.create_operator').click(function() {
				var operatorId = 'created_operator_' + operatorI;
				var operatorData = {
					top: ($flowchart.height() / 2) - 30,
					left: ($flowchart.width() / 2) - 100 + (operatorI * 10),
					properties: {
						title: 'Operator ' + (operatorI + 3),
						inputs: {
							input_1: {
								label: 'Input 1',
							}
						},
						outputs: {
							output_1: {
								label: 'Output 1',
							}
						}
					}
				};

				operatorI++;

				$flowchart.flowchart('createOperator', operatorId, operatorData);

			});
			//--- end
			//--- create operator button
			//-----------------------------------------




			//-----------------------------------------
			//--- draggable operators
			//--- start
			//var operatorId = 0;
			var $draggableOperators = $('.draggable_operator');
			$draggableOperators.draggable({
				cursor: "move",
				opacity: 0.7,

				// helper: 'clone',
				appendTo: 'body',
				zIndex: 1000,

				helper: function(e) {
					var $this = $(this);
					var data = getOperatorData($this);
					return $flowchart.flowchart('getOperatorElement', data);
				},
				stop: function(e, ui) {
					var $this = $(this);
					var elOffset = ui.offset;
					var containerOffset = $container.offset();
					if (elOffset.left > containerOffset.left &&
						elOffset.top > containerOffset.top &&
						elOffset.left < containerOffset.left + $container.width() &&
						elOffset.top < containerOffset.top + $container.height()) {

						var flowchartOffset = $flowchart.offset();

						var relativeLeft = elOffset.left - flowchartOffset.left;
						var relativeTop = elOffset.top - flowchartOffset.top;

						var positionRatio = $flowchart.flowchart('getPositionRatio');
						relativeLeft /= positionRatio;
						relativeTop /= positionRatio;

						var data = getOperatorData($this);
						data.left = relativeLeft;
						data.top = relativeTop;

						$flowchart.flowchart('addOperator', data);
					}
				}
			});
			//--- end
			//--- draggable operators
			//-----------------------------------------


			//-----------------------------------------
			//--- save and load
			//--- start
			function Flow2Text() {
				var data = $flowchart.flowchart('getData');
				$('#flowchart_data').val(JSON.stringify(data, null, 2));
			}
			$('#get_data').click(Flow2Text);

			function Text2Flow() {
				var data = JSON.parse($('#flowchart_data').val());
				$flowchart.flowchart('setData', data);
			}
			$('#set_data').click(Text2Flow);

			/*global localStorage*/
			function SaveToLocalStorage() {
				if (typeof localStorage !== 'object') {
					alert('local storage not available');
					return;
				}
				Flow2Text();
				localStorage.setItem("stgLocalFlowChart", $('#flowchart_data').val());
			}
			$('#save_local').click(SaveToLocalStorage);

			function LoadFromLocalStorage() {
				if (typeof localStorage !== 'object') {
					alert('local storage not available');
					return;
				}
				var s = localStorage.getItem("stgLocalFlowChart");
				if (s != null) {
					$('#flowchart_data').val(s);
					Text2Flow();
				}
				else {
					alert('local storage empty');
				}
			}
			$('#load_local').click(LoadFromLocalStorage);
			//--- end
			//--- save and load
			//-----------------------------------------
			
			
			
			var operatorI = {{$visitRouteFlowChartOperatorsCount}};
			$('#addBusinessToVisitFlowChartBtn').click(function() {
			
			    var selectedOptionText = $('#business_id').find(":selected").text();
			    
			    var selectedOptionValue = $('#business_id').find(":selected").val();
			    
			    if(selectedOptionValue ==""){
			    	alert('یک کسب و کار را انتخاب کنید');
			    	return;
			    }
			    
			    var operatorId = 'created_operator_' + operatorI;
			      var operatorData = {
				top: 60,
				left: 500,
				properties: {
				  title: selectedOptionText,
				  inputs: {
				    input_1: {
				      label: 'ورود',
				    }
				  },
				  outputs: {
				    output_1: {
				      label: 'خروج',
				    }
				  },
				  meta: {
				      type: 1,
				      id: selectedOptionValue,
				      entry_datetime: '',
				      exit_datetime: '',
				      chart_operator_id: operatorId,
				      visit_description: ''
				  }
				}
			      };
			      
			      operatorI++;
			      
			      $flowchart.flowchart('createOperator', operatorId, operatorData);
			
			
			
			});
			
			
			
			$('#addLocationToVisitRouteBtn').click(function() {
				
				var latitude = $('#latitude').val();
				var longitude = $('#longitude').val();
				var geo_location_title = $('#geo_location_title').val();
				
				if( (latitude!==null && latitude!=="") && (longitude!==null && longitude!=="") && (geo_location_title!==null && geo_location_title!=="") ){

					var operatorId = 'created_operator_' + operatorI;
					var operatorData = {
						top: 60,
						left: 500,
						properties: {
						  title: geo_location_title,
						  inputs: {
						    input_1: {
						      label: 'ورود',
						    }
						  },
						  outputs: {
						    output_1: {
						      label: 'خروج',
						    }
						  },
						  meta: {
						      type: 2,
						      latitude: latitude,
						      longitude: longitude,						      
						      entry_datetime: '',
						      exit_datetime: '',
						      chart_operator_id: operatorId,
						      visit_description: ''
						      
						  }
						}
					};

					operatorI++;

					$flowchart.flowchart('createOperator', operatorId, operatorData);
				
				
				}else{
					alert('وارد کردن عنوان موقعیت مکانی و انتخاب موقعیت مکانی بر روی نقشه اجباری می باشد.');
				}
			
			
			});
			
			
			
			
			
			$('#submitFormBtn').click(function() {
			
			
				if($('#visit_flow_date').val()==""){
					alert('لطفا یک تاریخ برای انجام مسیر ویزیت انتخاب کنید');
					return;
				}
			
				var data = $flowchart.flowchart('getData');
				
				$('#visit_flowchart_json').val(JSON.stringify(data, null, 2));
				
				$('#mainForm').submit();
				//submit #mainForm form
			
			});


		});


		@if(!empty($visitRoute->visit_route))
			var defaultFlowchartData = {!! $visitRoute->visit_route !!};
			
			
			
			
			if (false) console.log('remove lint unused warning', defaultFlowchartData);
		@endif
		
		
	</script>
	
	
	
	
	<script>



	function panToRegion(master_el_id){
	
		var e = document.getElementById(master_el_id);
	
		var latitude = $(e.options[e.selectedIndex]).data('latitude');
		var longitude = $(e.options[e.selectedIndex]).data('longitude');

		if( (latitude!==null && latitude!=="") && (longitude!==null && longitude!=="") ){
			map.panTo([latitude, longitude]);
			
			if(master_el_id=="state_id"){
				map.setZoom(8);
			}else if(master_el_id=="city_id"){
				map.setZoom(12);
			}else{
				map.setZoom(15); //region
			}
			
			
			
		}
	
	}


        function change_list(master_el_id,child_el_id,loading_gif_id,ajax_req_url,depend_element_ids = []){

            var e = document.getElementById(master_el_id);
            r_id = e.options[e.selectedIndex].value;
            
            if(r_id==""){
                $('#'+child_el_id).html('');
                return;
            }

            $('#'+master_el_id).prop('disabled', true);
            //$('#'+loading_gif_id).css('display', 'block'); //TODO: enable loadings

            token= $('body').find("input[name='_token']").val();

            $.ajax({
                url: ajax_req_url,
                type: 'GET',
                cache:false,
                data: {"r_id":r_id, "_token":token },
                success: function(data){

                    $('#'+child_el_id).html('');
                    html_str='<option value="" >انتخاب کنید</option>';
                    $('#'+child_el_id).append(html_str);
                    $.each(data, function( index, value ) {
                        html_str='<option data-latitude="'+value.latitude+'" data-longitude="'+value.longitude+'" value="'+value.id+'" >'+value.title+'</option>';
                        $('#'+child_el_id).append(html_str);
                    });

                    depend_element_ids.forEach(function(){
                        $('#'+depend_element_ids).empty();
                    });

                    $('#'+master_el_id).prop('disabled', false);
                    //$('#'+loading_gif_id).css('display', 'none');
                },
                error: function(data){
                    alert(data.responseText);
                    alert('دریافت اطلاعات با خطا مواجه شد!');
                    $('#'+master_el_id).prop('disabled', false);
                    //$('#'+loading_gif_id).css('display', 'none');
                }
            });

        }
    </script>
	
	
	
	
	
	
@endsection
