@extends('panel.layout.master')
@section('main_content')
    <div class="row mb-md-5">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2>گزارش ویزیت کاربر راننده: {{$visitorUser->name}} در تاریخ 
				{{ \Morilog\Jalali\Jalalian::forge($visitRoute->visit_at_date)->format('%A, %d %B %Y') }}
				</h2>

            </div>

            <div class="pull-left">

                

            </div>

        </div>

    </div>


    @if ($message = Session::get('success'))

        <div class="alert alert-success">

            <p>{{ $message }}</p>

        </div>

    @endif
    
    
    


    <table class="table table-bordered">

        <tr>

            <th>ردیف</th>

            <th>عنوان</th>

            <th>نوع مکان</th>
            
            <th>دسترسی</th>
			
			<th>زمان ورود</th>
			
			<th>زمان خروج</th>
			
			<th>توضیحات</th>
			
        </tr>

		@php
			$i=0;
		@endphp
        @foreach ($userVisitRouteReportData as $key => $reportRow)

            <tr>

                <td>{{ ++$i }}</td>
				
				<td>{{ $reportRow['title'] }}</td>
				
				<td>{{ ($reportRow['type']==1)?'کسب و کار':'مکان جغرافیایی' }}</td>
				
				<td>
				<a target="_blank" href="{{($reportRow['type']==1)?route('businesses.show',$reportRow['business_id']):      ('https://maps.google.com/?q='.$reportRow['latitude'].','.$reportRow['longitude'])      }}">
				{{($reportRow['type']==1)?'مشاهده کسب و کار':'مشاهده موقعیت'}}
				</a>
				</td>

                <td>
						{{ 
							!empty($reportRow['entry_datetime'])?
							\Morilog\Jalali\Jalalian::forge($reportRow['entry_datetime'])->format('%A, %d %B %Y - H:i:s'):
							'-'
						}}
				</td>
				
				<td>
						{{ 
							!empty($reportRow['exit_datetime'])?
							\Morilog\Jalali\Jalalian::forge($reportRow['exit_datetime'])->format('%A, %d %B %Y - H:i:s'): 
							'-'
						}}
				</td>

               <td>{{ !empty($reportRow['visit_description'])?$reportRow['visit_description']:'-' }}</td>

            </tr>

        @endforeach
 
    </table>



@endsection
