@extends('panel.layout.master')
@section('main_content')

    <div class="row">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2>ویرایش تنظیمات قیمتها</h2>

            </div>

            <div class="pull-left">

                <a class="btn btn-primary" href="{{ route('showDashboard') }}"> بازگشت</a>

            </div>

        </div>

    </div>


    @if ($errors->any())

        <div class="alert alert-danger">

            <strong>خطا!</strong> در مقادیر ورودی ایراداتی وجود دارد.<br><br>

            <ul>

                @foreach ($errors->all() as $error)

                    <li>{{ $error }}</li>

                @endforeach

            </ul>

        </div>

    @endif
	
	@if ($message = Session::get('success'))

        <div class="alert alert-success">

            <p>{{ $message }}</p>

        </div>

    @endif
    
    @if ($message = Session::get('danger'))

        <div class="alert alert-danger">

            <p>{{ $message }}</p>

        </div>

    @endif



    <form action="{{ route('configs.update') }}" method="POST">

        @csrf


        <div class="row">

            <div class="col-xs-12 col-sm-12 col-md-6">

				<div class="form-group">

					<strong>حداکثر تاریخ چک(روز):</strong>

					{!! Form::text('max_cheque_days', old('max_cheque_days',$data['max_cheque_days']), array('placeholder' => '','class' => 'form-control', 'onKeyPress'=>"return NumbersOnly(event, 'max_cheque_days')")) !!}

				</div>
			</div>

		</div>
		
		
		<div class="row">

            <div class="col-xs-12 col-sm-12 col-md-6">

				<div class="form-group">

					<strong>حداکثر تخفیف مجاز:</strong>

					{!! Form::text('max_discount_amount', old('max_discount_amount',$data['max_discount_amount']), array('placeholder' => '','class' => 'form-control', 'onKeyPress'=>"return NumbersOnly(event, 'max_discount_amount')")) !!}

				</div>
			
			</div>

		</div>
		
		
		<div class="row">

            <div class="col-xs-12 col-sm-12 col-md-6">

				<div class="form-group">

					<strong>حداکثر قیمت مجاز هر آیتم در فاکتور:</strong>

					{!! Form::text('max_invoice_item_amount', old('max_invoice_item_amount',$data['max_invoice_item_amount']), array('placeholder' => '','class' => 'form-control', 'onKeyPress'=>"return NumbersOnly(event, 'max_invoice_item_amount')")) !!}

				</div>
			
			</div>

		</div>
		
		
		<div class="row">

            <div class="col-xs-12 col-sm-12 col-md-6">

				<div class="form-group">

					<strong>حداکثر جمع مبالغ مجاز هر فاکتور:</strong>

					{!! Form::text('max_sum_of_invoice_items_amount', old('max_sum_of_invoice_items_amount',$data['max_sum_of_invoice_items_amount']), array('placeholder' => '','class' => 'form-control', 'onKeyPress'=>"return NumbersOnly(event, 'max_sum_of_invoice_items_amount')")) !!}

				</div>
			
			</div>

		</div>
		
		<div class="row">

			<div class="col-xs-12 col-sm-12 col-md-6 text-center">

				<button type="submit" class="btn btn-primary">ثبت تغییرات</button>

			</div>
			
		</div>

        </div>


    </form>



@endsection


@section('additional_js_bottom')
<script>
    function NumbersOnly(evt, label) {
        var status = document.getElementById(label);
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode == 46) {
            return true;
        }
        if (charCode < 48 || charCode > 57) {
            status.innerHTML = 'Numbers Only Please';
            return false;
        }
        status.innerHTML = '';
        return true;
    }
</script>
@endsection
