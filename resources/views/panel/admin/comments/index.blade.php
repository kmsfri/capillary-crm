@extends('panel.layout.master')
@section('main_content')
    <div class="row mb-md-5">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2>مدیریت شکایات و پیشنهادات</h2>

            </div>

            <div class="pull-left">

                <a class="btn btn-success" href="{{ route('comments.create') }}"> اضافه کردن شکایت/پیشنهاد</a>

            </div>

        </div>

    </div>


    @if ($message = Session::get('success'))

        <div class="alert alert-success">

            <p>{{ $message }}</p>

        </div>

    @endif
    
    @if ($message = Session::get('danger'))

        <div class="alert alert-danger">

            <p>{{ $message }}</p>

        </div>

    @endif


    <table class="table table-bordered">

        <tr>

            <th>ردیف</th>

            <th>دسته بندی</th>
            <th>مشتری</th>
            <th>شماره تماس</th>
            
            <th width="280px">عملیات</th>

        </tr>

        @foreach ($data as $key => $comment)

            <tr>

                <td>{{ ++$i }}</td>

                <td>{{ $comment->category()->first()->category_text }}</td>

                <td><a href="{{ !empty($comment->customer_id)?route('customers.edit',$comment->customer()->first()->id):'#' }}" target="_blank">{{ !empty($comment->customer_id)?$comment->customer()->first()->business_name:'-' }}</a></td>
                <td>{{ $comment->customer_phone }}</td>
              
		
                <td>
                
		     

                    <a class="btn btn-info" href="{{ route('comments.show',$comment->id) }}">نمایش</a>

                    <a class="btn btn-primary" href="{{ route('comments.edit',$comment->id) }}">ویرایش</a>

                    {!! Form::open(['method' => 'DELETE','route' => ['comments.destroy', $comment->id],'style'=>'display:inline']) !!}

                    {!! Form::submit('حذف', ['class' => 'btn btn-danger']) !!}

                    {!! Form::close() !!}

                </td>

            </tr>

        @endforeach

    </table>


    {!! $data->render() !!}



@endsection
