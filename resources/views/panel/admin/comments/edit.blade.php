@extends('panel.layout.master')
@section('main_content')

    <div class="row">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2>ویرایش شکایت/پیشنهاد</h2>

            </div>

            <div class="pull-left">

                <a class="btn btn-primary" href="{{ route('comments.index') }}"> بازگشت</a>

            </div>

        </div>

    </div>


    @if ($errors->any())

        <div class="alert alert-danger">

            <strong>خطا!</strong> در مقادیر ورودی ایراداتی وجود دارد.<br><br>

            <ul>

                @foreach ($errors->all() as $error)

                    <li>{{ $error }}</li>

                @endforeach

            </ul>

        </div>

    @endif


    <form action="{{ route('comments.update',$comment->id) }}" method="POST">

        @csrf

        @method('PUT')


        <div class="row">
        
        
        	<div class="col-xs-12 col-sm-12 col-md-4">

		<div class="form-group">

		<strong>دسته بندی:</strong>

			<select  name='comment_category_id' id='comment_category_id' class='form-control pull-right' >
				@foreach($commentCategories as $category)
				<option @if(old('comment_category_id', isset($comment->comment_category_id) ? $comment->comment_category_id : '')==$category->id) selected @endif value="{{$category->id}}" >{{$category->category_text}}</option>
				@endforeach
				
			</select>

		</div>

	</div>
    

        <div class="col-xs-12 col-sm-12 col-md-4">

            <div class="form-group">

                <strong>شناسه مشتری:</strong>
                
                <input type="number" name="customer_id" id="customer_id" value="{{old('customer_id',$comment->customer_id)}}" class="form-control" placeholder="" onKeyPress="return NumbersOnly(event, 'customer_id')">

            </div>

        </div>
        
	<div class="col-xs-12 col-sm-12 col-md-4">

		<div class="form-group">

		<strong>تلفن تماس:</strong>

		{!! Form::text('customer_phone', old('customer_phone',$comment->customer_phone), array('placeholder' => '','class' => 'form-control')) !!}

		</div>
	</div>

        
        
	<div class="col-xs-12 col-sm-12 col-md-12 mt-md-2">

		<div class="form-group">

			<strong>متن شکایت/پیشنهاد:</strong>

			<textarea class="form-control" rows="10" name="comment_text" placeholder="" >{{old('comment_text',$comment->comment_text)}}</textarea>

		</div>

	</div>
        
        
        
        
        
        
        
        
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">

                <button type="submit" class="btn btn-primary">ثبت تغییرات</button>

            </div>

        </div>


    </form>



@endsection


@section('additional_js_bottom')
<script>
    function NumbersOnly(evt, label) {
        var status = document.getElementById(label);
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode == 46) {
            return true;
        }
        if (charCode < 48 || charCode > 57) {
            status.innerHTML = 'Numbers Only Please';
            return false;
        }
        status.innerHTML = '';
        return true;
    }
</script>
@endsection
