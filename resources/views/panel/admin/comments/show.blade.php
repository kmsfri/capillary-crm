@extends('panel.layout.master')
@section('main_content')

    <div class="row mb-md-5">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2> نمایش جزئیات شکایت/پیشنهاد</h2>

            </div>

            <div class="pull-left">

                <a class="btn btn-primary" href="{{ route('customers.index') }}"> بازگشت</a>

            </div>

        </div>

    </div>


    <div class="row">

        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>مشتری:</strong>

                <a href="{{ !empty($comment->customer_id)?route('customers.edit',$comment->customer()->first()->id):'#' }}" target="_blank">{{ !empty($comment->customer_id)?$comment->customer()->first()->business_name:'-' }}</a>

            </div>

        </div>


        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>دسته بندی:</strong>

                {{ $comment->category()->first()->category_text }}

            </div>

        </div>


        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>متن شکایت/پیشنهاد:</strong>

                {{ $comment->comment_text }}

            </div>

        </div>


        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>شماره تماس:</strong>

                {{ $comment->customer_phone }}

            </div>

        </div>


        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>کاربر ایجاد کننده:</strong>

                {{ $comment->creator()->first()->name }}({{ $comment->creator()->first()->email }})

            </div>

        </div>


        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>تاریخ و زمان ایجاد:</strong>

                {{ $comment->created_at }}

            </div>

        </div>


        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>تاریخ و زمان آخرین ویرایش:</strong>

                {{ $comment->updated_at }}

            </div>

        </div>

    </div>

@endsection
