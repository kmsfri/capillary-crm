@extends('panel.layout.master')
@section('main_content')


    <div class="row">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2>فهرست بازاریابی</h2>

            </div>

            <div class="pull-left">

                @can('marketing-create')

                    <a class="btn btn-success" href="{{ route('marketing.create') }}"> اضافه کردن آیتم بازاریابی جدید</a>

                @endcan

            </div>

        </div>

    </div>


    @if ($message = Session::get('success'))

        <div class="alert alert-success">

            <p>{{ $message }}</p>

        </div>

    @endif


    <table class="table table-bordered">

        <tr>

            <th>ردیف</th>

            <th>عنوان آیتم بازاریابی</th>

            <th>تاریخ انجام</th> 
            
            <th>فایل ضمیمه</th>
            
            <th width="280px">عملیات</th>

        </tr> 

        @foreach ($marketings as $marketing)

            <tr>

                <td>{{ ++$i }}</td>

                <td>{{ $marketing->marketing_title }}</td>

                <td>{{ \Morilog\Jalali\Jalalian::forge($marketing->do_at)->format('%A, %d %B %Y') }}</td>
                
                <td><a href="{{ asset($marketing->file_path) }}" target="_blank">دانلود فایل ضمیمه</a></td>
                
               

                <td>

                    <form action="{{ route('marketing.destroy',$marketing->id) }}" method="POST">

                        <a class="btn btn-info" href="{{ route('marketing.show',$marketing->id) }}">نمایش</a>

                        @can('marketing-edit')

                            <a class="btn btn-primary" href="{{ route('marketing.edit',$marketing->id) }}">ویرایش</a>

                        @endcan


                        @csrf

                        @method('DELETE')

                        @can('marketing-delete')

                            <button type="submit" class="btn btn-danger">حذف</button>

                        @endcan

                    </form>

                </td>

            </tr>

        @endforeach

    </table>


    {!! $marketings->links() !!}



@endsection
