@extends('panel.layout.master')
@section('main_content')

    <div class="row">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2>ویرایش آیتم بازاریابی</h2>

            </div>

            <div class="pull-left">

                <a class="btn btn-primary" href="{{ route('marketing.index') }}"> بازگشت</a>

            </div>

        </div>

    </div>


    @if ($errors->any())

        <div class="alert alert-danger">

            <strong>خطا!</strong> در مقادیر ورودی ایراداتی وجود دارد.<br><br>

            <ul>

                @foreach ($errors->all() as $error)

                    <li>{{ $error }}</li>

                @endforeach

            </ul>

        </div>

    @endif


    <form action="{{ route('marketing.update',$marketing->id) }}" method="POST">

        @csrf

        @method('PUT')


        <div class="row">
		
		
		<div class="col-xs-12 col-sm-12 col-md-6 form-input-containerform-input-container">

			<div class="form-group">

				<strong>عنوان آیتم بازاریابی:</strong>

				<input type="text" name="marketing_title" value="{{ old('marketing_title',($marketing->marketing_title??'') ) }}" class="form-control" placeholder="">

			</div>

		</div>
		
		</div>
		<div class="row">
	
		
		<div class="col-xs-12 col-sm-12 col-md-6 form-input-container">

			<div class="form-group">

				<strong>تاریخ انجام:</strong>

				<input data-jdp type="text" name="do_at" value="{{ old('do_at',($marketing->do_at??'') ) }}" class="form-control" placeholder="">

			</div>

		</div>
		
		</div>
		<div class="row">
	
		<div class="col-xs-12 col-sm-12 col-md-6 form-input-container mb-5">

			<div class="form-group">

				<strong>فایل ضمیمه:</strong>

				<input type="file" name="marketing_file" id="marketing_file" value="" autocomplete="off">

			</div>

		</div>
		
		</div>
		<div class="row">
		
		<div class="col-xs-12 col-sm-12 col-md-6 mt-md-2">

			<div class="form-group">

				<strong>توضیحات آیتم بازاریابی:</strong>

				<textarea class="form-control" name="marketing_description" placeholder="" >{{old('marketing_description',($marketing->marketing_description??'') )}}</textarea>

			</div>
 
		</div>
        
        
      
	
		</div>
		<div class="row">

		<div class="col-xs-12 col-sm-12 col-md-6 text-center">

			<button type="submit" class="btn btn-primary">ثبت</button>

		</div>

        </div>


    </form>



@endsection