@extends('panel.layout.master')
@section('main_content')


    <div class="row">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2>مدیریت تیم ها</h2>

            </div>

            <div class="pull-left">

                @can('product-create')

                    <a class="btn btn-success" href="{{ route('teams.create') }}"> ایجاد تیم جدید</a>

                @endcan

            </div>

        </div>

    </div>


    @if ($message = Session::get('success'))

        <div class="alert alert-success">

            <p>{{ $message }}</p>

        </div>

    @endif


    <table class="table table-bordered">

        <tr>
        
        





            <th>ردیف</th>

            <th>نام تیم</th>
            
            <th>از تاریخ</th>
            
            <th>تا تاریخ</th>

            <th>تعداد فاکتور</th>
            
            <th>جمع مبالغ</th>
            
            <th>کسب و کار</th>
            
            <th>اعضا</th>
            
            <th>گزارش</th>
            

            <th width="280px">عملیات</th>

        </tr>

        @foreach ($teams as $team)

            <tr>

                <td>{{ ++$i }}</td>

                <td>{{ $team->team_name }}</td>
                
                <td>{{ \Morilog\Jalali\Jalalian::forge($team->date_from)->format('%A, %d %B %Y') }}</td>
                
                <td>{{ \Morilog\Jalali\Jalalian::forge($team->date_to)->format('%A, %d %B %Y') }}</td>

                <td>{{ $team->target_invoices_count }}</td>
                
                <td>{{ $team->target_invoices_amount }}</td>
                
                <td>{{ $team->target_businesses_count }}</td>
                
               <td><a href="{{route('teams.show-createTeamMembers-form',$team->id)}}">{{ $team->members()->count() }}</a></td>

		
		<td><a href="{{route('teams.show-team-report',$team->id)}}">مشاهده گزارش</a></td>


                <td>

                    <form action="{{ route('teams.destroy',$team->id) }}" method="POST">

                        

                        @can('team-management')

                            <a class="btn btn-primary" href="{{ route('teams.edit',$team->id) }}">ویرایش</a>

                        @endcan


                        @csrf

                        @method('DELETE')

                        @can('team-management')

                            <button type="submit" class="btn btn-danger">حذف</button>

                        @endcan

                    </form>

                </td>

            </tr>

        @endforeach

    </table>


    {!! $teams->links() !!}



@endsection
