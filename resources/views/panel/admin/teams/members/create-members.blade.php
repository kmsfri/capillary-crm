@extends('panel.layout.master')
@section('additional_js_top')

@endsection
@section('additional_css')


<link href="{{asset('select2/css/select2.min.css')}}" rel="stylesheet" />
    

<style>

.select2-container--default .select2-selection--multiple .select2-selection__choice {
  color: black;
}

</style>
    
@stop
@section('main_content')

@if ($message = Session::get('danger'))

        <div class="alert alert-danger">

            <p>{{ $message }}</p>

        </div>

    @endif

    <div class="row mb-md-5">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2>ثبت لیست اعضاء تیم: {{$team->team_name}}</h2>
                <h5>
                	از تاریخ:
                	{{ \Morilog\Jalali\Jalalian::forge($team->date_from)->format('%A, %d %B %Y') }}
                	&nbsp;&nbsp;&nbsp;
                	تا تاریخ:
                	{{ \Morilog\Jalali\Jalalian::forge($team->date_to)->format('%A, %d %B %Y') }}
                </h5>

            </div>

            <div class="pull-left">

                <a class="btn btn-primary" href="{{ route('showDashboard') }}"> بازگشت</a>

            </div>

        </div>

    </div>


    @if (count($errors) > 0)

        <div class="alert alert-danger">

            <strong>خطا!</strong> در مقادیر ورودی ایراداتی وجود دارد.<br><br>

            <ul>

                @foreach ($errors->all() as $error)

                    <li>{{ $error }}</li>

                @endforeach

            </ul>

        </div>

    @endif



    {!! Form::open(array('route' => 'teams.store-team-members','method'=>'POST','id'=>'mainForm')) !!}


    <input type="hidden" name="team_id" value="{{$team->id}}">
   

    <div class="row">
	
	
	
	
	
	
	
	<div class="col-xs-12 col-sm-12 col-md-12 form-input-container">

		<div class="form-group">

		    <strong>اعضاء:</strong>

		
		    
		    
		    <select required  class="js-example-basic-multiple" name="member_user_id[]" multiple="multiple">
		    
			    @foreach($users as $key=>$user)
			    
				@php
				$selected = "";
			    	if(!empty($teamMembers)){
				    	if(in_array($user->id,$teamMembers)){
				    		$selected = "selected";
				    	}
			    	}
			    	@endphp
			    
				    <option {{$selected}} value="{{$user->id}}" >{{$user->name}}</option>
				@endforeach

			</select>

		</div>

        </div>
        

    
<!--form-control-->
        
        

        
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">

            <button id="submitFormBtn" type="submit" class="btn btn-primary">ثبت</button>

        </div>

    </div>

    {!! Form::close() !!}



@endsection
@section('additional_js_bottom')

<script src = "{{asset('select2/js/select2.min.js')}}"></script>



    <script type="text/javascript">
		/* global $ */
		$(document).ready(function() {
			
			$(document).ready(function() {
			    $('.js-example-basic-multiple').select2();
			});
			



		});


		
		
	</script>
	
	
	
	
	
	
@endsection
