@extends('panel.layout.master')
@section('main_content')


    <div class="row">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2>گزارش عملکرد تیم: {{$team->team_name}}</h2>
				<h5>
                	از تاریخ:
                	{{ \Morilog\Jalali\Jalalian::forge($team->date_from)->format('%A, %d %B %Y') }}
                	&nbsp;&nbsp;&nbsp;
                	تا تاریخ:
                	{{ \Morilog\Jalali\Jalalian::forge($team->date_to)->format('%A, %d %B %Y') }}
                </h5>

            </div>

            <div class="pull-left">

    
            </div>

        </div>

    </div>


    @if ($message = Session::get('success'))

        <div class="alert alert-success">

            <p>{{ $message }}</p>

        </div>

    @endif


    <table class="table table-bordered mt-5">

        <tr>
        
        





            <th>ردیف</th>

            <th>عنوان تارگت</th>
            
            <th class="text-center">تارگت</th>
            
            <th class="text-center">ثبت شده</th>

            <th class="text-center">درصد</th>
			
			<th class="text-center">وضعیت</th>
            
        </tr>
		
		
		<tr>
			<td>1</td>
			<td>تعداد فاکتورهای ثبت شده</td>
			<td class="text-center">{{$team->target_invoices_count}}</td>
			<td class="text-center">{{$data['registeredInvoicesCount']}}</td>
			@php
			$progressPercent = ((100/$team->target_invoices_count)*$data['registeredInvoicesCount']);
			
			
			$colorClass="";
			if($progressPercent>=100){
				$colorClass="bg-success";
			}else if($progressPercent<100 && $progressPercent>=70){
				$colorClass="bg-info";
			}else if($progressPercent<70 && $progressPercent>=50){
				$colorClass="bg-warning";
			}else{
				$colorClass="bg-danger";
			}
			
			if($progressPercent>100){
				$progressPercent = 100;
			}
			@endphp 
			<td class="text-center">%{{round($progressPercent,2)}}</td>
			<td>
			
				<div class="progress">
				  <div class="progress-bar {{$colorClass}}" role="progressbar" style="width: {{round($progressPercent)}}%" aria-valuenow="{{round($progressPercent)}}" aria-valuemin="0" aria-valuemax="100"></div>
				</div>
							
			</td>
		</tr>
		
		
		<tr>
			<td>2</td>
			<td>جمع مبالغ فاکتورهای ثبت شده</td>
			<td class="text-center">{{$team->target_invoices_amount}}</td>
			<td class="text-center">{{$data['registeredInvoicesAmount']}}</td>
			@php
			$progressPercent = ((100/$team->target_invoices_amount)*$data['registeredInvoicesAmount']);
			
			
			$colorClass="";
			if($progressPercent>=100){
				$colorClass="bg-success";
			}else if($progressPercent<100 && $progressPercent>=70){
				$colorClass="bg-info";
			}else if($progressPercent<70 && $progressPercent>=50){
				$colorClass="bg-warning";
			}else{
				$colorClass="bg-danger";
			}
			
			if($progressPercent>100){
				$progressPercent = 100;
			}
			@endphp 
			<td class="text-center">%{{round($progressPercent,2)}}</td>
			<td>
			
				<div class="progress">
				  <div class="progress-bar {{$colorClass}}" role="progressbar" style="width: {{round($progressPercent)}}%" aria-valuenow="{{round($progressPercent)}}" aria-valuemin="0" aria-valuemax="100"></div>
				</div>
							
			</td>
		</tr>
		
		
		<tr>
			<td>3</td>
			<td>تعداد کسب و کارهای ثبت شده</td>
			<td class="text-center">{{$team->target_businesses_count}}</td>
			<td class="text-center">{{$data['registeredBusinessesCount']}}</td>
			@php
			$progressPercent = ((100/$team->target_businesses_count)*$data['registeredBusinessesCount']);
			
			
			$colorClass="";
			if($progressPercent>=100){
				$colorClass="bg-success";
			}else if($progressPercent<100 && $progressPercent>=70){
				$colorClass="bg-info";
			}else if($progressPercent<70 && $progressPercent>=50){
				$colorClass="bg-warning";
			}else{
				$colorClass="bg-danger";
			}
			
			if($progressPercent>100){
				$progressPercent = 100;
			}
			@endphp 
			<td class="text-center">%{{round($progressPercent,2)}}</td>
			<td>
			
				<div class="progress">
				  <div class="progress-bar {{$colorClass}}" role="progressbar" style="width: {{round($progressPercent)}}%" aria-valuenow="{{round($progressPercent)}}" aria-valuemin="0" aria-valuemax="100"></div>
				</div>
							
			</td>
		</tr>
		
		
		
		
		 

        
    </table>



@endsection
