@extends('panel.layout.master')
@section('main_content')

    <div class="row">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2>ویرایش اطلاعات تیم</h2>

            </div>

            <div class="pull-left">

                <a class="btn btn-primary" href="{{ route('teams.index') }}"> بازگشت</a>

            </div>

        </div>

    </div>


    @if ($errors->any())

        <div class="alert alert-danger">

            <strong>خطا!</strong> در مقادیر ورودی ایراداتی وجود دارد.<br><br>

            <ul>

                @foreach ($errors->all() as $error)

                    <li>{{ $error }}</li>

                @endforeach

            </ul>

        </div>

    @endif


    <form action="{{ route('teams.update',$team->id) }}" method="POST">

        @csrf

        @method('PUT')


        <div class="row">


		<div class="col-xs-12 col-sm-12 col-md-12">

		    <div class="form-group">

			<strong>نام تیم:</strong>

			{!! Form::text('team_name', old('team_name',$team->team_name), array('placeholder' => '','class' => 'form-control')) !!}

		    </div>

		</div>
		
		
		<div class="col-xs-12 col-sm-12 col-md-6 form-input-container">

			<div class="form-group">

			    <strong>از تاریخ:</strong>

			    <input data-jdp type="text" name="date_from" id="date_from" value="{{old('date_from',(!empty($team->date_from)?\Morilog\Jalali\Jalalian::forge($team->date_from)->format('%Y/%m/%d'):'') )}}" class="form-control" placeholder="">

			</div>

		</div>
		
		<div class="col-xs-12 col-sm-12 col-md-6 form-input-container">

			<div class="form-group">

			    <strong>تا تاریخ:</strong>

			    <input data-jdp type="text" name="date_to" id="date_from" value="{{old('date_to',(!empty($team->date_to)?\Morilog\Jalali\Jalalian::forge($team->date_to)->format('%Y/%m/%d'):'') )}}" class="form-control" placeholder="">

			</div>

		</div>
		
		

		
		<div class="col-xs-12 col-sm-12 col-md-4 form-input-container mb-md-5">

			<div class="form-group">

			    <strong>تارگت تعداد فاکتورهای صادر شده:</strong>

			    <input type="number" name="target_invoices_count" id="target_invoices_count" value="{{old('target_invoices_count',$team->target_invoices_count)}}" class="form-control" placeholder="" onKeyPress="return NumbersOnly(event, 'target_invoices_count')">

			</div>

		</div>
		
		
		<div class="col-xs-12 col-sm-12 col-md-4 form-input-container mb-md-5">

			<div class="form-group">

			    <strong>تارگت جمع مبالغ فاکتورها:</strong>

			    <input type="number" name="target_invoices_amount" id="target_invoices_amount" value="{{old('target_invoices_amount',$team->target_invoices_amount)}}" class="form-control" placeholder="" onKeyPress="return NumbersOnly(event, 'target_invoices_amount')">

			</div>

		</div>
		
		<div class="col-xs-12 col-sm-12 col-md-4 form-input-container mb-md-5">

			<div class="form-group">

			    <strong>تارگت تعداد کسب و کار اضافه شده:</strong>

			    <input type="number" name="target_businesses_count" id="target_businesses_count" value="{{old('target_businesses_count',$team->target_businesses_count)}}" class="form-control" placeholder="" onKeyPress="return NumbersOnly(event, 'target_businesses_count')">

			</div>

		</div>
		
		
		
		
		
		<div class="col-xs-12 col-sm-12 col-md-12 mt-md-2">

			<div class="form-group">

				<strong>توضیحات:</strong>

				<textarea class="form-control" name="team_description" placeholder="" >{{old('team_description',$team->team_description)}}</textarea>

			</div>

		</div>






            <div class="col-xs-12 col-sm-12 col-md-12 text-center">

                <button type="submit" class="btn btn-primary">ثبت تغییرات</button>

            </div>

        </div>


    </form>



@endsection


@section('additional_js_bottom')
<script>
    function NumbersOnly(evt, label) {
        var status = document.getElementById(label);
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode == 46) {
            return true;
        }
        if (charCode < 48 || charCode > 57) {
            status.innerHTML = 'Numbers Only Please';
            return false;
        }
        status.innerHTML = '';
        return true;
    }
</script>
@endsection
