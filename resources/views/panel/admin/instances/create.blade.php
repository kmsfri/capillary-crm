@extends('panel.layout.master')
@section('additional_css')

@stop
@section('main_content')






    <div class="row mb-5">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2>ثبت اطلاعات فرم ارسال نمونه برای سرنخ {{$clue->clue_title}}</h2>

            </div>

            <div class="pull-left">

                <a class="btn btn-primary" href="{{ route('clues.showCartable',['clue_id'=>$clue->id]) }}"> ﺑﺎﺯﮔﺸﺖ</a>

            </div>

        </div>

    </div>


    @if ($errors->any())

        <div class="alert alert-danger">

            <strong>ﺧﻄﺎ!</strong> ﺩﺭ ﻣﻘﺎﺩﯾﺮ ﻭﺭﻭﺩﯼ اﯾﺮاﺩاﺗﯽ ﻭﺟﻮﺩ ﺩاﺭﺩ.<br><br>

            <ul>

                @foreach ($errors->all() as $error)

                    <li>{{ $error }}</li>

                @endforeach

            </ul>

        </div>

    @endif
	
	
	@if ($message = Session::get('danger'))

        <div class="alert alert-danger">

            <p>{{ $message }}</p>

        </div>

    @endif


    <form action="{{ route('instances.store') }}" method="POST" enctype="multipart/form-data">

        @csrf

        <input name="clue_id" type="hidden" value="{{$clue->id}}">
		<input name="clue_level" type="hidden" value="{{$clueLevel}}">

        
        <div class="row">

            <div class="col-xs-12 col-sm-12 col-md-12 mb-md-4">
                <h4>ﻣﺸﺨﺼﺎﺕ ﻓﺮﻭﺷﻨﺪﻩ</h4>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-6 form-input-container">

                <div class="form-group">

                    <strong>ﻧﺎﻡ ﻓﺮﻭﺷﻨﺪﻩ:</strong>

                    <input type="text" value="ﺳﭙﯿﺪ ﺁﺭﻧﮓ" class="form-control" placeholder="" disabled>

                </div>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-6 form-input-container">

                <div class="form-group">

                    <strong>ﺁﺩﺭﺱ:</strong>

                    <input type="text" value="۳ ﺧﯿﺎﺑﺎﻥ ﺷﻬﯿﺪ ﺑﻬﺸﺘﯽ، ﺧﯿﺎﺑﺎﻥ ﺧﺎﻟﺪ اﺳﻼﻣﺒﻮﻟﯽ(ﻭﺭﺯاء)، ﮐﻮﭼﻪ ﺩﻫﻢ، ﭘﻼﮎ" class="form-control" placeholder="" disabled>

                </div>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-6 form-input-container">

                <div class="form-group">

                    <strong>ﮐﺪﭘﺴﺘﯽ:</strong>

                    <input type="text" value="1511787813" class="form-control" placeholder="" disabled>

                </div>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-6 form-input-container">

                <div class="form-group">

                    <strong>ﺗﻠﻔﻦ:</strong>

                    <input type="text" value="58347" class="form-control" placeholder="" disabled>

                </div>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 mb-md-4">
                <h4>ﻣﺸﺨﺼﺎﺕ ﺧﺮﯾﺪاﺭ</h4>
            </div>


            <div class="col-xs-12 col-sm-12 col-md-4 form-input-container">

                <div class="form-group">

                    <strong>ﻧﺎﻡ:</strong>

                    <input type="text" value="{{$clue->first_name}}" class="form-control" placeholder="" disabled>

                </div>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-4 form-input-container">

                <div class="form-group">

                    <strong>ﻧﺎﻡ ﺧﺎﻧﻮاﺩﮔﯽ:</strong>

                    <input type="text" value="{{$clue->last_name}}" class="form-control" placeholder="" disabled>

                </div>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-4 form-input-container">

                <div class="form-group">

                    <strong>ﻧﺎﻡ ﻓﺮﻭﺷﮕﺎﻩ:</strong>

                    <input type="text" value="{{$clue->business_name}}" class="form-control" placeholder="" disabled>

                </div>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 form-input-container">

                <div class="form-group">

                    <strong>ﻧﺸﺎﻧﯽ:</strong>

                    <input type="text" value="{{$clue->getFullStateCityRegion()}} - {{$clue->address}}" class="form-control" placeholder="" disabled>

                </div>

            </div>


            <div class="col-xs-12 col-sm-12 col-md-3 form-input-container">

                <div class="form-group">

                    <strong>ﺗﻠﻔﻦ ﺛﺎﺑﺖ:</strong>

                    <input type="text" value="{{$clue->phone}}" class="form-control" placeholder="" disabled>

                </div>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-3 form-input-container">

                <div class="form-group">

                    <strong>ﮐﺪﭘﺴﺘﯽ:</strong>

                    <input type="text" value="-" class="form-control" placeholder="" disabled>

                </div>

            </div>


          




            <div class="col-xs-12 col-sm-12 col-md-12 mb-md-4 mt-5">
                <h4>ﻣﺸﺨﺼﺎﺕ ﮐﺎﻻ ﺑﺎ ﻧﻮﺳﺎﻧﺎﺕ ﻣﻮﺭﺩ ﻣﻌﺎﻣﻠﻪ</h4>
            </div>

            <span id="neighbors_container">

            <?php
            
            	$selectProduct = '
            		<select onChange="loadProductInfo(this)" name="invoice_items[{index_placeholder}][product_id]" class="form-control">
            			<option value="">انتخاب کنید</option>
            	';
            	
            	foreach($products as $product){
            		$selectProduct .= '<option value="'.$product->id.'" data-code="'.$product->product_code.'" data-name="'.$product->product_name.'" data-price="'.$product->price.'">'.$product->product_name.( ($product->has_priority==1)?'(دارای اولویت فروش)':'' ).'</option>';
            	}
            	
            	$selectProduct .= '</select>';
            	
            	
            	$selectStock = '
            		<select name="invoice_items[{index_placeholder}][stock]" class="form-control">
            			<option value="">انتخاب کنید</option>
            	';
            	$selectStock .= '<option value="1">1</option>';
            	$selectStock .= '<option value="2">2</option>';
            	$selectStock .= '</select>';
            
                $invoiceItemsSampleElements = '
                <div class="col-12 mb-2 mt-2 border border-top-0 border-right-0 border-left-0">
                <div class="row">
                
        	    <div class="col-xs-12 col-sm-12 col-md-4 mt-md-2">
                        <div class="form-group">
                            <strong>محصول:</strong>
                            '.$selectProduct.'
                        </div>
                    </div>
                
                    <div class="col-xs-12 col-sm-12 col-md-2 mt-md-2">
                        <div class="form-group">
                            <strong>ﮐﺪ ﮐﺎﻻ:</strong>
                            <input type="text" name="invoice_items[{index_placeholder}][code]" class="form-control invoice-product-code" placeholder="" value="{code_placeholder}" >
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-4 mt-md-2">
                        <div class="form-group">
                            <strong>ﺷﺮﺡ ﮐﺎﻻ:</strong>
                            <input type="text" name="invoice_items[{index_placeholder}][title]" class="form-control invoice-product-title" placeholder="" value="{title_placeholder}" >
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-2 mt-md-2">
                        <div class="form-group">
                            <strong>انبار کالا:</strong>
                            '.$selectStock.'
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-2 mt-md-2">
                        <div class="form-group">
                            <strong>ﺗﻌﺪاﺩ ﻭاﺣﺪ:</strong>
                            <input type="text" name="invoice_items[{index_placeholder}][units_count]" class="form-control invoice-product-units-count" placeholder="" value="{units_count_placeholder}" >
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-2 mt-md-2">
                        <div class="form-group">
                            <strong>ﺗﻌﺪاﺩ ﮐﺎﺭﺗﻦ:</strong>
                            <input type="text" name="invoice_items[{index_placeholder}][cartons_count]" class="form-control invoice-product-cartoons-count" placeholder="" value="{cartons_count_placeholder}" >
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-2 mt-md-2">
                        <div class="form-group">
                            <strong>ﻣﺒﻠﻎ ﻭاﺣﺪ:</strong>
                            <input type="text" name="invoice_items[{index_placeholder}][unit_price]" class="form-control invoice-product-price" placeholder="" value="{unit_price_placeholder}" >
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-2 mt-md-2">
                        <div class="form-group">
                            <strong>ﻣﺎﻟﯿﺎﺕ ﻭ اﺭﺯﺵ اﻓﺰﻭﺩﻩ:</strong>
                            <input type="text" name="invoice_items[{index_placeholder}][tax]" class="form-control invoice-product-tax" placeholder="" value="{tax_placeholder}" >
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-2 mt-md-2">
                        <div class="form-group">
                            <strong>عوارض:</strong>
                            <input type="text" name="invoice_items[{index_placeholder}][toll]" class="form-control invoice-product-toll" placeholder="" value="{toll_placeholder}" >
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-2 mt-md-2">
                        <div class="form-group">
                            <strong>ﺗﺨﻔﯿﻒ ﺭﯾﺎﻟﯽ:</strong>
                            <input type="text" name="invoice_items[{index_placeholder}][discount]" class="form-control invoice-product-discount" placeholder="" value="{discount_placeholder}" >
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-2 mt-md-2">
                        <div class="form-group">
                            <strong>ﻣﺒﻠﻎ ﮐﻞ:</strong>
                            <input type="text" name="invoice_items[{index_placeholder}][item_total_price]" class="form-control invoice-product-total-price" placeholder="" value="{item_total_price_placeholder}" >
                        </div>
                    </div>
                </div>
                </div>
                ';

                $storedInvoiceItemsCount = 1;
                $searchArray = ['{index_placeholder}','{code_placeholder}','{title_placeholder}','{stock_placeholder}','{units_count_placeholder}','{cartons_count_placeholder}','{unit_price_placeholder}','{tax_placeholder}','{toll_placeholder}','{discount_placeholder}','{item_total_price_placeholder}'];

                $storedInvoiceItems = !empty($invoice->invoice_items)?$invoice->invoice_items:[];
                if(!empty( old('invoice_items',$storedInvoiceItems) )){
                    foreach(old('invoice_items',$storedInvoiceItems) as $invoiceItem){
                        $replaceArray = [$storedInvoiceItemsCount, $invoiceItem['code'], $invoiceItem['title'], $invoiceItem['stock'], $invoiceItem['units_count'], $invoiceItem['cartons_count'], $invoiceItem['unit_price'], $invoiceItem['tax'], $invoiceItem['toll'], $invoiceItem['discount'], $invoiceItem['item_total_price']];
                        echo str_replace($searchArray,$replaceArray,$invoiceItemsSampleElements);
                        $storedInvoiceItemsCount++;
                    }
                }else{
                    $storedInvoiceItemsCount = 1;
                    $replaceArray = [$storedInvoiceItemsCount, '','','','','','','','',''];
                    echo str_replace($searchArray,$replaceArray,$invoiceItemsSampleElements);
                }
                ?>

            </span>


            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <button type="button" id="addNewInvoiceItemsButton" class="btn btn-success float-left">ﺟﺪﯾﺪ</button>
                </div>
            </div>

























            <div class="col-xs-12 col-sm-12 col-md-3 form-input-container">

                <div class="form-group">

                    <strong>ﺻﺎﺩﺭﮐﻨﻨﺪﻩ:</strong>

                    <input type="text" value="{{auth()->user()->name}}" class="form-control" placeholder="" disabled>

                </div>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-3 form-input-container">

                <div class="form-group">

                    <strong>ﺛﺒﺖ ﮐﻨﻨﺪﻩ:</strong>

                    <input type="text" value="{{auth()->user()->name}}" class="form-control" placeholder="" disabled>

                </div>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-6">

                <div class="form-group">

                    <strong>ﻣﺤﻞ ﺗﺤﻮﯾﻞ:</strong>

                    <textarea class="form-control" name="delivery_place_address" placeholder="" required>{{old('delivery_place_address',($invoice->delivery_place_address??Null)     )}}</textarea>

                </div>

            </div>



            <div class="col-xs-12 col-sm-12 col-md-6 form-input-container">

                <div class="form-group">

                    <strong>ﻧﺤﻮﻩ ﻓﺮﻭﺵ:</strong>

                    <select  name='sell_method' id='sell_method' class='form-control pull-right' required>
                        <option @if(old('sell_method', isset($invoice->sell_method) ? $invoice->sell_method : '')=='1') selected @endif value="1" >ﻧﻘﺪ</option>
                        <option @if(old('sell_method', isset($invoice->sell_method) ? $invoice->sell_method : '')==2) selected @endif value="2" >ﭼﮏ</option>
                        <option @if(old('sell_method', isset($invoice->sell_method) ? $invoice->sell_method : '')==3) selected @endif value="3" >اﻋﺘﺒﺎﺭﯼ</option>
                    </select>

                </div>

            </div>


            <div class="col-xs-12 col-sm-12 col-md-6 form-input-container mb-md-5">

                <div class="form-group">

                    <strong>ﻣﺪﺕ ﺯﻣﺎﻥ ﭼﮏ(ﺭﻭﺯ):</strong>

                    <input type="number" name="cheque_time" id="cheque_time" value="{{old('cheque_time',$invoice->cheque_time??Null )}}" required class="form-control" placeholder="" onKeyPress="return NumbersOnly(event, 'cheque_time')">

                </div>

            </div>



            <div class="col-xs-12 col-sm-12 col-md-12">

                <div class="form-group">

                    <strong>ﺗﻮﺿﯿﺤﺎﺕ:</strong>

                    <textarea class="form-control" name="invoice_description" placeholder="" required>{{old('invoice_description',$invoice->invoice_description??Null )}}</textarea>

                </div>

            </div>
			
			
			<div class="col-xs-12 col-sm-12 col-md-12 form-input-container mt-5 mb-5">

				<div class="form-group">

					<strong>فایل ضمیمه:</strong>

					<input type="file" name="file_path" id="file_path" value="" autocomplete="off">

				</div>

			</div>
				
            
            <!--
            <div class="col-xs-12 col-sm-12 col-md-6 form-input-container">

                <div class="form-group">

                    <strong>نوع فاکتور:</strong>

                    <select {{!empty($invoice)?'disabled':''}}  name='invoice_type' id='invoice_type' class='form-control pull-right' required>
                        <option @if(old('invoice_type', isset($invoice->invoice_type) ? $invoice->invoice_type : '')=='PRE') selected @endif value="PRE" >پیش فاکتور</option>
                        <option @if(old('invoice_type', isset($invoice->invoice_type) ? $invoice->invoice_type : '')=='RETURNED') selected @endif value="RETURNED" >مرجوعی</option>
                        <option @if(old('invoice_type', isset($invoice->invoice_type) ? $invoice->invoice_type : '')=='DAMAGED') selected @endif value="DAMAGED" >ضایعاتی</option>
                    </select>

                </div>

            </div>


		-->













            <div class="col-xs-12 col-sm-12 col-md-12 text-center">

                <button type="submit" class="btn btn-primary">ﺫﺧﯿﺮﻩ</button>

            </div>

        </div>


    </form>


@endsection

@section('additional_js_bottom')

    <script>
	
	
	
	
	

    function NumbersOnly(evt, label) {
        var status = document.getElementById(label);
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode == 46) {
            return true;
        }
        if (charCode < 48 || charCode > 57) {
            status.innerHTML = 'Numbers Only Please';
            return false;
        }
        status.innerHTML = '';
        return true;
    }



        function escapeRegExp(string) {
            return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
        }

        function replaceAll(str, find, replace) {
            return str.replace(new RegExp(escapeRegExp(find), 'g'), replace);
        }

        $(document).ready(function(){

            invoiceItemsCount = <?php echo $storedInvoiceItemsCount; ?>;

            $("#addNewInvoiceItemsButton").click(function(){
                invoiceItemsCount++;
                var invoiceItemsSampleElements = `<?php echo $invoiceItemsSampleElements; ?>`;
                invoiceItemsSampleElements = replaceAll(invoiceItemsSampleElements,'{index_placeholder}',invoiceItemsCount);
                invoiceItemsSampleElements = replaceAll(invoiceItemsSampleElements,'{code_placeholder}','');
                invoiceItemsSampleElements = replaceAll(invoiceItemsSampleElements,'{title_placeholder}','');
                invoiceItemsSampleElements = replaceAll(invoiceItemsSampleElements,'{stock_placeholder}','');
                invoiceItemsSampleElements = replaceAll(invoiceItemsSampleElements,'{units_count_placeholder}','');
                invoiceItemsSampleElements = replaceAll(invoiceItemsSampleElements,'{cartons_count_placeholder}','');
                invoiceItemsSampleElements = replaceAll(invoiceItemsSampleElements,'{unit_price_placeholder}','');
                invoiceItemsSampleElements = replaceAll(invoiceItemsSampleElements,'{tax_placeholder}','');
                invoiceItemsSampleElements = replaceAll(invoiceItemsSampleElements,'{toll_placeholder}','');
                invoiceItemsSampleElements = replaceAll(invoiceItemsSampleElements,'{discount_placeholder}','');
                invoiceItemsSampleElements = replaceAll(invoiceItemsSampleElements,'{item_total_price_placeholder}','');

                $('#neighbors_container').append(invoiceItemsSampleElements);
            });





        });
        
        function loadProductInfo(obj){
            
            var selectedOption = $(obj).find(":selected");
            
          
            $(obj).closest('.row').find('.invoice-product-title').val(selectedOption.data('name'));
            $(obj).closest('.row').find('.invoice-product-code').val(selectedOption.data('code'));
            $(obj).closest('.row').find('.invoice-product-price').val(selectedOption.data('price'));
            //$(obj).closest('.row').find('.invoice-product-total-price').val(selectedOption.data('price'));
			$(obj).closest('.row').find('.invoice-product-units-count').val(1);
			$(obj).closest('.row').find('.invoice-product-cartoons-count').val(1);
			
			//$(obj).closest('.row').find('.invoice-product-tax').val(0);
			$(obj).closest('.row').find('.invoice-product-toll').val(0);
			$(obj).closest('.row').find('.invoice-product-discount').val(0);
			
			
			
			
			calculateInvoiceItemValues(obj);
			
            
        }
		
		
		 
		
		function calculateInvoiceItemValues(obj, changingElementID = ''){
			
			price = $(obj).closest('.row').find('.invoice-product-price').val();
			
			
			
			if (!price) {
				return true;
			}
			
			
			
			
			unitCounts = $(obj).closest('.row').find('.invoice-product-units-count').val();
			
			
			taxValueForUnitItem = (price/100)*9;
			
			
			
			
			if(changingElementID=="invoice-product-tax"){
				sumTax = $(obj).closest('.row').find('.invoice-product-tax').val();
			}else{
				sumTax = (taxValueForUnitItem*unitCounts);
				$(obj).closest('.row').find('.invoice-product-tax').val(sumTax);
			}
			
			toll= $(obj).closest('.row').find('.invoice-product-toll').val();
			
			
			discount= $(obj).closest('.row').find('.invoice-product-discount').val();
			
			
			sumPrice = ( ((  parseInt(unitCounts)  *  parseInt(price)  )+  parseInt(sumTax)  +  parseInt(toll)  ) - parseInt(discount) )
			
			
			$(obj).closest('.row').find('.invoice-product-total-price').val(sumPrice);
			
			
			
		}

        $(document).ready(function() {
            $('form').keydown(function(event){
                if(event.keyCode == 13) {
                    event.preventDefault();
                    return false;
                }
            });
            
            
            
            	$('form').on('keyup','.invoice-product-units-count',function(event){
            	
		   calculateInvoiceItemValues(this, 'invoice-product-units-count');
		});
		
		$('form').on('keyup','.invoice-product-price',function(event){
            	
		   calculateInvoiceItemValues(this, 'invoice-product-price');
		});
		
		
		$('form').on('keyup','.invoice-product-tax',function(event){
            	
		   calculateInvoiceItemValues(this, 'invoice-product-tax');
		});
		
		
		$('form').on('keyup','.invoice-product-toll',function(event){
            	
		   calculateInvoiceItemValues(this, 'invoice-product-toll');
		});
		
		$('form').on('keyup','.invoice-product-discount',function(event){
            	
		   calculateInvoiceItemValues(this, 'invoice-product-discount');
		});

            



        });


    </script>
@stop

