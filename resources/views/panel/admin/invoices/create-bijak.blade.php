@extends('panel.layout.master')
@section('main_content')

    <div class="row">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2>بارگزاری بیجک</h2>

            </div>

            <div class="pull-left">

                <a class="btn btn-primary" href="{{ route('invoices.show-primary-invoices') }}"> بازگشت</a>

            </div>

        </div>

    </div>


    @if ($errors->any())

        <div class="alert alert-danger">

            <strong>خطا!</strong> در مقادیر ورودی ایراداتی وجود دارد.<br><br>

            <ul>

                @foreach ($errors->all() as $error)

                    <li>{{ $error }}</li>

                @endforeach

            </ul>

        </div>

    @endif


    <form action="{{ route('invoices.upload-bijak') }}" method="POST" enctype="multipart/form-data" >

        @csrf
	
	<input type="hidden" name="invoice_id" value="{{$invoice_id}}">

        <div class="row">
		
		
		<div class="col-xs-12 col-sm-12 col-md-6 form-input-containerform-input-container">

			<div class="form-group">

				<strong>عنوان بیجک:</strong>

				<input type="text" name="bijak_title" value="{{ old('bijak_title',($bijak->bijak_title??'') ) }}" class="form-control" placeholder="">

			</div>

		</div>
		
		</div>
		<div class="row">
	
		
		<div class="col-xs-12 col-sm-12 col-md-6 form-input-container">

			<div class="form-group">

				<strong>تاریخ بیجک:</strong>

				<input data-jdp type="text" name="bijak_date" value="{{ old('bijak_date',($bijak->bijak_date??'') ) }}" class="form-control" placeholder="">

			</div>

		</div>
		
		</div>
		<div class="row">
	
		<div class="col-xs-12 col-sm-12 col-md-6 form-input-container mb-5">

			<div class="form-group">

				<strong>فایل تصویر بیجک:</strong>

				<input type="file" name="bijak_file" id="bijak_file" value="" autocomplete="off">

			</div>

		</div>
		
		</div>
		<div class="row">
		
		<div class="col-xs-12 col-sm-12 col-md-6 mt-md-2">

			<div class="form-group">

				<strong>توضیحات بیجک:</strong>

				<textarea class="form-control" name="bijak_description" placeholder="" >{{old('bijak_description',($bijak->bijak_description??'') )}}</textarea>

			</div>
 
		</div>
        
        
      
	
		</div>
		<div class="row">

		<div class="col-xs-12 col-sm-12 col-md-6 text-center">

			<button type="submit" class="btn btn-primary">ثبت</button>

		</div>

        </div>


    </form>



@endsection
