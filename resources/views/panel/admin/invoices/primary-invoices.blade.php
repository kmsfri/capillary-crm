@extends('panel.layout.master')
@section('main_content')


    <div class="row mb-md-5">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2>
فاکتورهای اصلی</h2>

            </div>

            <div class="pull-left">


            </div>

        </div>

    </div>


    @if ($message = Session::get('success'))

        <div class="alert alert-success">

            <p>{{ $message }}</p>

        </div>

    @endif


    <table class="table table-bordered">

        <tr>

            <th>ﺭﺩﯾﻒ</th>

            <th>کسب و کار</th>

            <th>ثبت کننده</th>

            <th>تاریخ ثبت</th>
            @if(auth()->user()->can('management-bijak') || auth()->user()->can('view-bijak'))
            <th>
            بیجک
            </th>
            @endcan
            
            <th width="280px">ﻋﻤﻠﯿﺎﺕ</th>

        </tr>

        @foreach ($invoices as $invoice)

            <tr>

                <td>{{ ++$i }}</td>

                <td>{{ $invoice->business()->first()->business_name }}</td>


                <td>{{ $invoice->creator()->first()->name }}</td>
                
                <td>{{ \Morilog\Jalali\Jalalian::forge($invoice->created_at)->format('%A, %d %B %Y در ساعت H:i:s') }}</td>
                
                @if(auth()->user()->can('management-bijak') || auth()->user()->can('view-bijak'))
		        <td>
		        @can('management-bijak')
		        
		        <a class="btn btn-info {{($invoice->bijaks()->count()>0)?'disabled':''}}" {{($invoice->bijaks()->count()>0)?'disabled':''}} href="{{($invoice->bijaks()->count()>0)?'':route('invoices.show-create-bijak-form',$invoice->id)}}">آپلود بیجک</a>
		        
		        @endcan
		        
		        @can('view-bijak')
		        
				@if($invoice->bijaks()->count()>0)
				<a class="btn btn-success" href="{{route('invoices.show-bijak',$invoice->bijaks()->first()->id)}}">مشاهده بیجک</a>
				@endif
		        	
		        @endcan
		        </td>
                @endif
                

                <td>
                    <form action="{{ route('invoices.destroy',$invoice->id) }}" method="POST">




			<a class="btn btn-warning" href="{{ route('invoices.get-pdf',['invoice_id'=>$invoice->id]) }}">دریافت</a>


                        @can('invoice-edit')
<!--
                            <a class="btn btn-primary" href="{{ route('invoices.edit',$invoice->id) }}">ﻭﯾﺮاﯾﺶ</a>
                            -->

                        @endcan


                        @csrf

                        @method('DELETE')

                        @can('invoice-delete')
<!--
                            <button type="submit" class="btn btn-danger">ﺣﺬﻑ</button>
-->
                        @endcan

                    </form>
                </td>

            </tr>

        @endforeach

    </table>


    {!! $invoices->links() !!}



@endsection
