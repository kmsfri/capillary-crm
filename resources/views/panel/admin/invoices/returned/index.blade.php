@extends('panel.layout.master')
@section('main_content')


    <div class="row mb-md-5">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2>
فاکتورهای مرجوعی/ضایعاتی درخواست شده</h2>

            </div>

            <div class="pull-left">


            </div>

        </div>

    </div>


    @if ($message = Session::get('success'))

        <div class="alert alert-success">

            <p>{{ $message }}</p>

        </div>

    @endif


    <table class="table table-bordered">

        <tr>

            <th>ﺭﺩﯾﻒ</th>

            <th>کسب و کار</th>

            <th>ثبت کننده</th>

            <th>تاریخ ثبت</th>
            
	    <th>تایید کارشناس فروش</th>
            
	    <th>تایید حسابدار فروش</th>
            
            <th width="280px">ﻋﻤﻠﯿﺎﺕ</th>

        </tr>

        @foreach ($invoices as $invoice)

            <tr>

                <td>{{ ++$i }}</td>

                <td>{{ $invoice->business()->first()->business_name }}</td>


                <td>{{ $invoice->creator()->first()->name }}</td>
                
                <td>{{ \Morilog\Jalali\Jalalian::forge($invoice->created_at)->format('%A, %d %B %Y در ساعت H:i:s') }}</td>
                

		
		
		
		@php
		
		$to = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i',$invoice->created_at);
		$from = \Carbon\Carbon::now()->format('Y-m-d H:s:i');

		$diffInDays = $to->diffInDays($from);
		@endphp
		
		
		<td class="text-center">
			@if($diffInDays>15)
				<label class="badge badge-danger">منقضی شده</label>
			@else
				@if($invoice->approved_by_sales_specialist==0)
					@can('invoice-sales-specialist-approve')
					<a class="btn btn-primary" href="{{ route('invoices.returned.show-sales-specialist-approving-form',['invoice_id'=>$invoice->id]) }}">تایید کارشناس فروش</a>
					@else
					<label class="badge badge-warning">عدم تعیین وضعیت</label>
					@endcan
				@elseif($invoice->approved_by_sales_specialist==1)
				<label class="badge badge-success">تایید شده</label>
				@elseif($invoice->approved_by_sales_specialist==2)
				<label class="badge badge-danger">رد شده</label>
				@endif
			@endif
		</td>
		
		
		
		
						
		<td class="text-center">
			@if($diffInDays>15)
				<label class="badge badge-danger">منقضی شده</label>
			@else
				@if($invoice->approved_by_accountant==0)
					@can('invoice-accounting-approve')
					<a class="btn btn-primary {{($invoice->approved_by_sales_specialist==0)?'disabled':''}}" {{($invoice->approved_by_sales_specialist==0)?'disabled':''}}
						href="{{ ($invoice->approved_by_sales_specialist==0)?'#':route('invoices.returned.show-accounting-approving-form',['invoice_id'=>$invoice->id]) }}">تایید حسابداری فروش</a>
					@else
					<label class="badge badge-warning">عدم تعیین وضعیت</label>
					@endcan
				@elseif($invoice->approved_by_accountant==1)
				<label class="badge badge-success">تایید شده</label>
				@elseif($invoice->approved_by_accountant==2)
				<label class="badge badge-danger">رد شده</label>
				@endif
			@endif

		</td>
		
		

                <td>
                    <form action="{{ route('invoices.returned.destroy',$invoice->id) }}" method="POST">




			<a class="btn btn-warning" href="{{ route('invoices.returned.get-pdf',['invoice_id'=>$invoice->id]) }}">دریافت</a>


                        @can('invoice-edit')

                            <a class="btn btn-primary" href="{{ route('invoices.returned.edit',$invoice->id) }}">ﻭﯾﺮاﯾﺶ</a>

                        @endcan


                        @csrf

                        @method('DELETE')

                        @can('invoice-delete')

                            <button type="submit" class="btn btn-danger">ﺣﺬﻑ</button>

                        @endcan

                    </form>
                </td>

            </tr>

        @endforeach

    </table>


    {!! $invoices->links() !!}



@endsection
