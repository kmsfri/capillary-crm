@extends('panel.layout.master')
@section('main_content')



    <div class="row mb-5">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2>ویرایش اطلاعات فاکتور مرجوعی/ضایعاتی 
                </h2>

            </div>

            <div class="pull-left">

                <a class="btn btn-primary" href="{{ route('invoices.returned.index') }}"> بازگشت</a>

            </div>

        </div>

    </div>


    @if ($errors->any())

        <div class="alert alert-danger">

            <strong>خطا!</strong> در مقادیر ورودی ایراداتی وجود دارد.<br><br>

            <ul>

                @foreach ($errors->all() as $error)

                    <li>{{ $error }}</li>

                @endforeach

            </ul>

        </div>

    @endif


    <form action="{{ route('invoices.returned.update',$invoice->id) }}" method="POST" enctype="multipart/form-data">

        <input name="_method" type="hidden" value="PATCH">
        @csrf


	<div class="row">

            <div class="col-xs-12 col-sm-12 col-md-12 mb-md-4">
                <h4>ﻣﺸﺨﺼﺎﺕ ﻓﺮﻭﺷﻨﺪﻩ</h4>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-6 form-input-container">

                <div class="form-group">

                    <strong>ﻧﺎﻡ ﻓﺮﻭﺷﻨﺪﻩ:</strong>

                    <input type="text" value="ﺳﭙﯿﺪ ﺁﺭﻧﮓ" class="form-control" placeholder="" disabled>

                </div>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-6 form-input-container">

                <div class="form-group">

                    <strong>ﺁﺩﺭﺱ:</strong>

                    <input type="text" value="۳ ﺧﯿﺎﺑﺎﻥ ﺷﻬﯿﺪ ﺑﻬﺸﺘﯽ، ﺧﯿﺎﺑﺎﻥ ﺧﺎﻟﺪ اﺳﻼﻣﺒﻮﻟﯽ(ﻭﺭﺯاء)، ﮐﻮﭼﻪ ﺩﻫﻢ، ﭘﻼﮎ" class="form-control" placeholder="" disabled>

                </div>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-6 form-input-container">

                <div class="form-group">

                    <strong>ﮐﺪﭘﺴﺘﯽ:</strong>

                    <input type="text" value="1511787813" class="form-control" placeholder="" disabled>

                </div>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-6 form-input-container">

                <div class="form-group">

                    <strong>ﺗﻠﻔﻦ:</strong>

                    <input type="text" value="58347" class="form-control" placeholder="" disabled>

                </div>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 mb-md-4">
                <h4>ﻣﺸﺨﺼﺎﺕ ﺧﺮﯾﺪاﺭ</h4>
            </div>


            <div class="col-xs-12 col-sm-12 col-md-4 form-input-container">

                <div class="form-group">

                    <strong>ﻧﺎﻡ:</strong>

                    <input type="text" value="{{$business->first_name}}" class="form-control" placeholder="" disabled>

                </div>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-4 form-input-container">

                <div class="form-group">

                    <strong>ﻧﺎﻡ ﺧﺎﻧﻮاﺩﮔﯽ:</strong>

                    <input type="text" value="{{$business->last_name}}" class="form-control" placeholder="" disabled>

                </div>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-4 form-input-container">

                <div class="form-group">

                    <strong>ﻧﺎﻡ ﻓﺮﻭﺷﮕﺎﻩ:</strong>

                    <input type="text" value="{{$business->business_name}}" class="form-control" placeholder="" disabled>

                </div>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 form-input-container">

                <div class="form-group">

                    <strong>ﻧﺸﺎﻧﯽ:</strong>

                    <input type="text" value="{{$business->getFullStateCityRegion()}} - {{$business->address}}" class="form-control" placeholder="" disabled>

                </div>

            </div>


            <div class="col-xs-12 col-sm-12 col-md-3 form-input-container">

                <div class="form-group">

                    <strong>ﺗﻠﻔﻦ ﺛﺎﺑﺖ:</strong>

                    <input type="text" value="{{$business->phone_number}}" class="form-control" placeholder="" disabled>

                </div>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-3 form-input-container">

                <div class="form-group">

                    <strong>ﮐﺪﭘﺴﺘﯽ:</strong>

                    <input type="text" value="{{$business->postal_code}}" class="form-control" placeholder="" disabled>

                </div>

            </div>


            @php
            $mobilesArray = json_decode($business->mobiles,true);
            @endphp
            <div class="col-xs-12 col-sm-12 col-md-3 form-input-container">

                <div class="form-group">

                    <strong>ﺗﻠﻔﻦ ﻫﻤﺮاﻩ:</strong>

                    <input type="text" value="{{!empty($mobilesArray[0])?$mobilesArray[0]:'-'}}" class="form-control" placeholder="" disabled>

                </div>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-3 form-input-container">

                <div class="form-group">

                    <strong>ﺗﻠﻔﻦ ﻫﻤﺮاﻩ ﺳﻮﺷﺎﻝ:</strong>

                    <input type="text" value="{{!empty($mobilesArray[1])?$mobilesArray[1]:'-'}}" class="form-control" placeholder="" disabled>

                </div>

            </div>




            <div class="col-xs-12 col-sm-12 col-md-12 mb-md-4 mt-5">
                <h4>ﻣﺸﺨﺼﺎﺕ ﮐﺎﻻ ﺑﺎ ﻧﻮﺳﺎﻧﺎﺕ ﻣﻮﺭﺩ ﻣﻌﺎﻣﻠﻪ</h4>
            </div>

            <span id="neighbors_container">

            <?php
            
            	$selectProduct = '
            		<select name="invoice_items[{index_placeholder}][product_id]" class="form-control">
            			<option value="">انتخاب کنید</option>
            	';
            	
            	foreach($products as $product){
            		$selectProduct .= '<option {selected_invoice_item_product_id_'.$product->id.'} value="'.$product->id.'" data-code="'.$product->product_code.'" data-name="'.$product->product_name.'" data-price="'.$product->price.'">'.$product->product_name.( ($product->has_priority==1)?'(دارای اولویت فروش)':'' ).'</option>';
            	}
            	
            	$selectProduct .= '</select>';
            	
            	
            	$selectStock = '
            		<select name="invoice_items[{index_placeholder}][stock]" class="form-control">
            			<option value="">انتخاب کنید</option>
            	';
            	$selectStock .= '<option {selected_invoice_item_stock_1} value="1">1</option>';
            	$selectStock .= '<option {selected_invoice_item_stock_2} value="2">2</option>';
            	$selectStock .= '</select>';
            
            
                $invoiceItemsSampleElements = '
                <div class="col-12 mb-2 mt-2 border border-top-0 border-right-0 border-left-0">
                <div class="row">
                
                    <div class="col-xs-12 col-sm-12 col-md-4 mt-md-2">
                        <div class="form-group">
                            <strong>محصول:</strong>
                            '.$selectProduct.'
                        </div>
                    </div>
                
                    <div class="col-xs-12 col-sm-12 col-md-2 mt-md-2">
                        <div class="form-group">
                            <strong>ﮐﺪ ﮐﺎﻻ:</strong>
                            <input type="text" name="invoice_items[{index_placeholder}][code]" class="form-control" placeholder="" value="{code_placeholder}" >
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-4 mt-md-2">
                        <div class="form-group">
                            <strong>ﺷﺮﺡ ﮐﺎﻻ:</strong>
                            <input type="text" name="invoice_items[{index_placeholder}][title]" class="form-control" placeholder="" value="{title_placeholder}" >
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-2 mt-md-2">
                        <div class="form-group">
                            <strong>انبار کالا:</strong>
                            '.$selectStock.'
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-2 mt-md-2">
                        <div class="form-group">
                            <strong>ﺗﻌﺪاﺩ ﻭاﺣﺪ:</strong>
                            <input type="text" name="invoice_items[{index_placeholder}][units_count]" class="form-control" placeholder="" value="{units_count_placeholder}" >
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-2 mt-md-2">
                        <div class="form-group">
                            <strong>ﺗﻌﺪاﺩ ﮐﺎﺭﺗﻦ:</strong>
                            <input type="text" name="invoice_items[{index_placeholder}][cartons_count]" class="form-control" placeholder="" value="{cartons_count_placeholder}" >
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-2 mt-md-2">
                        <div class="form-group">
                            <strong>ﻣﺒﻠﻎ ﻭاﺣﺪ:</strong>
                            <input type="text" name="invoice_items[{index_placeholder}][unit_price]" class="form-control" placeholder="" value="{unit_price_placeholder}" >
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-2 mt-md-2">
                        <div class="form-group">
                            <strong>ﻣﺎﻟﯿﺎﺕ ﻭ اﺭﺯﺵ اﻓﺰﻭﺩﻩ:</strong>
                            <input type="text" name="invoice_items[{index_placeholder}][tax]" class="form-control" placeholder="" value="{tax_placeholder}" >
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-2 mt-md-2">
                        <div class="form-group">
                            <strong>عوارض:</strong>
                            <input type="text" name="invoice_items[{index_placeholder}][toll]" class="form-control" placeholder="" value="{toll_placeholder}" >
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-2 mt-md-2">
                        <div class="form-group">
                            <strong>ﺗﺨﻔﯿﻒ ﺭﯾﺎﻟﯽ:</strong>
                            <input type="text" name="invoice_items[{index_placeholder}][discount]" class="form-control" placeholder="" value="{discount_placeholder}" >
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-2 mt-md-2">
                        <div class="form-group">
                            <strong>ﻣﺒﻠﻎ ﮐﻞ:</strong>
                            <input type="text" name="invoice_items[{index_placeholder}][item_total_price]" class="form-control" placeholder="" value="{item_total_price_placeholder}" >
                        </div>
                    </div>
                </div>
                </div>
                ';

                $storedInvoiceItemsCount = 1;
                $searchArray = ['{index_placeholder}','{code_placeholder}','{title_placeholder}','{stock_placeholder}','{units_count_placeholder}','{cartons_count_placeholder}','{unit_price_placeholder}','{tax_placeholder}','{toll_placeholder}','{discount_placeholder}','{item_total_price_placeholder}'];

                $storedInvoiceItems = !empty($invoice->invoice_items)?$invoice->invoice_items:[];
                if(!empty( old('invoice_items',$storedInvoiceItems) )){
                    foreach(old('invoice_items',$storedInvoiceItems) as $invoiceItem){
                        $replaceArray = [$storedInvoiceItemsCount, $invoiceItem['code'], $invoiceItem['title'], $invoiceItem['stock'], $invoiceItem['units_count'], $invoiceItem['cartons_count'], $invoiceItem['unit_price'], $invoiceItem['tax'], $invoiceItem['toll'], $invoiceItem['discount'], $invoiceItem['item_total_price']];
                        
                        
                        foreach($products as $product){
                        	$selected = "";
                        	if($invoiceItem['product_id']==$product->id){
                        		$selected = "selected";
                        	}
                        	
                        	$searchArray[] = '{selected_invoice_item_product_id_'.$product->id.'}';
                        	$replaceArray[] = $selected;
                        }
                        
                        
                        
                        for($iCounter=1; $iCounter<=2; $iCounter++){
                        	
                        	$selected = "";
                        	if($invoiceItem['stock']==$iCounter){
                        		$selected = "selected";
                        	}
                        	
                        	$searchArray[] = '{selected_invoice_item_stock_'.$iCounter.'}';
                        	$replaceArray[] = $selected;
                        
                        }
                        
                        
                        echo str_replace($searchArray,$replaceArray,$invoiceItemsSampleElements);
                        $storedInvoiceItemsCount++;
                    }
                }else{
                    $storedInvoiceItemsCount = 1;
                    $replaceArray = [$storedInvoiceItemsCount, '','','','','','','','',''];
                    echo str_replace($searchArray,$replaceArray,$invoiceItemsSampleElements);
                }
                ?>

            </span>


            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <button type="button" id="addNewInvoiceItemsButton" class="btn btn-success float-left">ﺟﺪﯾﺪ</button>
                </div>
            </div>

























            <div class="col-xs-12 col-sm-12 col-md-3 form-input-container">

                <div class="form-group">

                    <strong>ﺻﺎﺩﺭﮐﻨﻨﺪﻩ:</strong>

                    <input type="text" value="{{auth()->user()->name}}" class="form-control" placeholder="" disabled>

                </div>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-3 form-input-container">

                <div class="form-group">

                    <strong>ﺛﺒﺖ ﮐﻨﻨﺪﻩ:</strong>

                    <input type="text" value="{{auth()->user()->name}}" class="form-control" placeholder="" disabled>

                </div>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-6">

                <div class="form-group">

                    <strong>ﻣﺤﻞ ﺗﺤﻮﯾﻞ:</strong>

                    <textarea class="form-control" name="delivery_place_address" placeholder="" required>{{old('delivery_place_address',($invoice->delivery_place_address??Null)     )}}</textarea>

                </div>

            </div>



            <div class="col-xs-12 col-sm-12 col-md-6 form-input-container">

                <div class="form-group">

                    <strong>ﻧﺤﻮﻩ ﻓﺮﻭﺵ:</strong>

                    <select  name='sell_method' id='sell_method' class='form-control pull-right' required>
                        <option @if(old('sell_method', isset($invoice->sell_method) ? $invoice->sell_method : '')=='0') selected @endif value="0" >ﻧﻘﺪ</option>
                        <option @if(old('sell_method', isset($invoice->sell_method) ? $invoice->sell_method : '')==1) selected @endif value="1" >ﭼﮏ</option>
                        <option @if(old('sell_method', isset($invoice->sell_method) ? $invoice->sell_method : '')==2) selected @endif value="2" >اﻋﺘﺒﺎﺭﯼ</option>
                    </select>

                </div>

            </div>


            <div class="col-xs-12 col-sm-12 col-md-6 form-input-container mb-md-5">

                <div class="form-group">

                    <strong>ﻣﺪﺕ ﺯﻣﺎﻥ ﭼﮏ(ﺭﻭﺯ):</strong>

                    <input type="number" name="cheque_time" id="cheque_time" value="{{old('cheque',$invoice->cheque_time??Null )}}" required class="form-control" placeholder="" onKeyPress="return NumbersOnly(event, 'cheque_time')">

                </div>

            </div>



            <div class="col-xs-12 col-sm-12 col-md-12">

                <div class="form-group">

                    <strong>ﺗﻮﺿﯿﺤﺎﺕ:</strong>

                    <textarea class="form-control" name="invoice_description" placeholder="" required>{{old('invoice_description',$invoice->invoice_description??Null )}}</textarea>

                </div>

            </div>














            <div class="col-xs-12 col-sm-12 col-md-12 text-center">

                <button type="submit" class="btn btn-primary">ﺫﺧﯿﺮﻩ</button>

            </div>

        </div>


    </form>


    <script>




        function change_list(master_el_id,child_el_id,loading_gif_id,ajax_req_url,depend_element_ids = []){

            var e = document.getElementById(master_el_id);
            r_id = e.options[e.selectedIndex].value;

            if(r_id==""){
                $('#'+child_el_id).html('');
                return;
            }

            $('#'+master_el_id).prop('disabled', true);
            //$('#'+loading_gif_id).css('display', 'block'); //TODO: enable loadings

            token= $('body').find("input[name='_token']").val();

            $.ajax({
                url: ajax_req_url,
                type: 'GET',
                data: {"r_id":r_id, "_token":token },
                success: function(data){

                    $('#'+child_el_id).html('');
                    html_str='<option value="" >انتخاب کنید</option>';
                    $('#'+child_el_id).append(html_str);
                    $.each(data, function( index, value ) {
                        html_str='<option value="'+value.id+'" >'+value.title+'</option>';
                        $('#'+child_el_id).append(html_str);
                    });

                    depend_element_ids.forEach(function(){
                        $('#'+depend_element_ids).empty();
                    });

                    $('#'+master_el_id).prop('disabled', false);
                    //$('#'+loading_gif_id).css('display', 'none');
                },
                error: function(data){
                    alert(data.responseText);
                    alert('دریافت اطلاعات با خطا مواجه شد!');
                    $('#'+master_el_id).prop('disabled', false);
                    //$('#'+loading_gif_id).css('display', 'none');
                }
            });

        }
    </script>


    <script type="text/javascript">





        function readURL(input,img_container_id) {

            $('#'+img_container_id).empty();
            for (let i = 0; i < input.files.length; i++) {
                if (input.files && input.files[i]) {
                    var reader = new FileReader();

                    var newImgElementID = img_container_id+"_img_"+i;
                    $('#'+img_container_id).append('<img id="'+newImgElementID+'" class="img img-responsive" />');

                    reader.onload = function (e) {


                        $('#'+newImgElementID)
                            .attr('src', e.target.result)
                            .height(100);
                    };

                    reader.readAsDataURL(input.files[i]);
                    //$('#'+img_id).removeClass('hide');


                }




            }


        }



    </script>

@endsection

@section('additional_js_bottom')

    <script>

        function escapeRegExp(string) {
            return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
        }

        function replaceAll(str, find, replace) {
            return str.replace(new RegExp(escapeRegExp(find), 'g'), replace);
        }

        $(document).ready(function(){
        
        
		invoiceItemsCount = <?php echo $storedInvoiceItemsCount; ?>;

		$("#addNewInvoiceItemsButton").click(function(){
			invoiceItemsCount++;
			var invoiceItemsSampleElements = `<?php echo $invoiceItemsSampleElements; ?>`;
			invoiceItemsSampleElements = replaceAll(invoiceItemsSampleElements,'{index_placeholder}',invoiceItemsCount);
			invoiceItemsSampleElements = replaceAll(invoiceItemsSampleElements,'{code_placeholder}','');
			invoiceItemsSampleElements = replaceAll(invoiceItemsSampleElements,'{title_placeholder}','');
			invoiceItemsSampleElements = replaceAll(invoiceItemsSampleElements,'{stock_placeholder}','');
			invoiceItemsSampleElements = replaceAll(invoiceItemsSampleElements,'{units_count_placeholder}','');
			invoiceItemsSampleElements = replaceAll(invoiceItemsSampleElements,'{cartons_count_placeholder}','');
			invoiceItemsSampleElements = replaceAll(invoiceItemsSampleElements,'{unit_price_placeholder}','');
			invoiceItemsSampleElements = replaceAll(invoiceItemsSampleElements,'{tax_placeholder}','');
			invoiceItemsSampleElements = replaceAll(invoiceItemsSampleElements,'{toll_placeholder}','');
			invoiceItemsSampleElements = replaceAll(invoiceItemsSampleElements,'{discount_placeholder}','');
			invoiceItemsSampleElements = replaceAll(invoiceItemsSampleElements,'{item_total_price_placeholder}','');

			$('#neighbors_container').append(invoiceItemsSampleElements);
		});
        

            
        });






    </script>
@stop
