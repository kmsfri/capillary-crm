@extends('panel.layout.master')
@section('main_content')

    <div class="row">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2>مشاهده بیجک</h2>

            </div>

            <div class="pull-left">

                <a class="btn btn-primary" href="{{ route('invoices.show-primary-invoices') }}"> بازگشت</a>

            </div>

        </div>

    </div>


    @if ($errors->any())

        <div class="alert alert-danger">

            <strong>خطا!</strong> در مقادیر ورودی ایراداتی وجود دارد.<br><br>

            <ul>

                @foreach ($errors->all() as $error)

                    <li>{{ $error }}</li>

                @endforeach

            </ul>

        </div>

    @endif


    

        <div class="row">
		
		
		<div class="col-xs-12 col-sm-12 col-md-8 form-input-containerform-input-container">

			<div class="form-group">

				<strong>عنوان بیجک:</strong>

				<input type="text" disabled value="{{ $bijak->bijak_title }}" class="form-control" placeholder="">

			</div>

		</div>
		
		</div>
		<div class="row">
	
		
		<div class="col-xs-12 col-sm-12 col-md-8 form-input-container">

			<div class="form-group">

				<strong>تاریخ بیجک:</strong>

				<input type="text" disabled value="{{\Morilog\Jalali\Jalalian::forge($bijak->bijak_date)->format('%A, %d %B %Y')}}" class="form-control">

			</div>

		</div>
		
		</div>
		
		<div class="row">
	
		
		<div class="col-xs-12 col-sm-12 col-md-8 form-input-container">

			<div class="form-group">

				<strong>تاریخ ثبت:</strong>

				<input type="text" disabled value="{{\Morilog\Jalali\Jalalian::forge($bijak->created_at)->format('%A, %d %B %Y در ساعت H:i:s')}}" class="form-control">

			</div>

		</div>
		
		</div>
		
		<div class="row">
		
		<div class="col-xs-12 col-sm-12 col-md-8 mt-md-2">

			<div class="form-group">

				<strong>توضیحات بیجک:</strong>

				<textarea rows="10" class="form-control" disabled >{{$bijak->bijak_description}}</textarea>

			</div>
 
		</div>
        
        
      
	
		</div>
		
		
		<div class="row">
	
		
		<div class="col-xs-12 col-sm-12 col-md-8 form-input-container">

			<div class="form-group">

				<strong>ثبت کننده:</strong>

				<input type="text" disabled value="{{$bijak->creator()->first()->name}}" class="form-control">

			</div>

		</div>
		
		</div>
		
		
		<div class="row">
	
		<div class="col-xs-12 col-sm-12 col-md-8 form-input-container mb-5">

			<div class="form-group">

				<strong>فایل تصویر بیجک:</strong>

				<div class="bijak-image-container">
					<img src="{{asset($bijak->file_path)}}" class="img" style="max-width:100%">
				</div>

			</div>

		</div>
		
		</div>
		




@endsection
