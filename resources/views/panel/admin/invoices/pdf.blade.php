<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>فاکتور سپیدآرنگ</title>
    <style>
        body {
            font-family: 'vazir';
            direction: rtl;
            font-size: 11px;
            
        }
        
        .text-center{
        	text-align:center;
        	
        }
        table {
	  width: 100%; 
	  border: solid thin black;
	  border-collapse: separate; 
	  border-spacing: 0 0px;
	}

	td.border-bottom{
	  border-bottom: 1px solid black; 
	  border-collapse: separate; 
	  border-spacing: 5px 5px;
	} 


	.table2{
		margin-top: 25px;
	}
	.table2 td{
		border: 1px solid black; 
		border-collapse: separate; 
		border-spacing: 5px 5px;
	}
    </style>
</head>

<body>


	<table>
		<thead>
			<tr>
				<td class="text-center"><h3>{{ $data['title'] }}</h3></td>
			</tr>
			<tr>
				<td>
					شماره سریال:
					{{$data['serial_number']}}
				</td>
			</tr>
			
			<tr>
				<td class="border-bottom" style="padding-bottom: 25px;">
					تاریخ صدور:
					{{$data['created_at']}}
				</td>
			</tr>
			

			
			
		</thead>
		<tbody>
			<tr>
			
				<td style="padding-top: 10px">
				نام فروشنده: سپید آرنگ
				</td>
			
			</tr>
			<tr>
			
				<td style="padding-top: 10px">
				نشانی: خیابان شهید بهشتی، خیابان خالد اسلامبولی(وزراء)، کوچه دهم، پلاک ۳
				</td>
			
			</tr>
			<tr>
			
				<td style="padding-top: 10px; padding-bottom: 20px" class="border-bottom">
				کدپستی: ۱۵۱۱۷۸۷۸۱۳        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;        تلفن: ۵۸۳۴۷
				</td>
			
			</tr>
			
			
			<tr>
				<td class="text-center border-bottom" style="padding-top: 10px; padding-bottom: 10px">
					<h3>مشخصات خریدار</h3>
				</td>
			</tr>
			
			
			<tr>
			
				<td style="padding-top: 10px">
				نام خریدار: 
				{{$data['business_first_name']}}
				        &nbsp;&nbsp;&nbsp;&nbsp;        نام خانوادگی: 
				        {{$data['business_last_name']}}
				             &nbsp;&nbsp;&nbsp;&nbsp;       نام فروشگاه: 
				             {{$data['business_name']}}
				</td>
			
			</tr>
			
			<tr>
			
				<td style="padding-top: 10px">
				نشانی: 
				{{$data['business_address']}}
				</td>
			
			</tr>
			
			<tr>
			
				<td style="padding-top: 10px">
				کدپستی: 
				{{$data['business_postal_code']}}
				</td>
			
			</tr>
			
			<tr>
			
				<td style="padding-top: 10px">
				تلفن ثابت: 
				{{$data['business_phone_number']}}
				      &nbsp;&nbsp;&nbsp;&nbsp;    تلفن همراه: 
				      {{$data['business_mobile1']}}
				               &nbsp;&nbsp;&nbsp;&nbsp;    تلفن همراه سوشال: 
				               {{$data['business_mobile2']}}
				</td>
			
			</tr>
			
			
			
			
		</tbody>
	</table>
	<table class="table2">
		
		<tbody>
		
			<tr>
				<td colspan="10" class="text-center border-bottom" style="padding-top: 10px; padding-bottom: 10px">
				<h3>مشخصات کالا با نوسانات مورد معامله</h3>
				</td>
			</tr>

			<tr>
				<td class="text-center">ردیف</td>
				<td class="text-center">کد کالا</td>
				<td class="text-center">شرح کالا</td>
				<td class="text-center">انبار کالا</td>
				<td class="text-center">تعداد واحد</td>
				<td class="text-center">تعداد کارتن</td>
				<td class="text-center">مبلغ واحد</td>
				<td class="text-center">مالیات و ارزش افزوده</td>
				<td class="text-center">تخفیف ریالی</td>
				<td class="text-center">مبلغ کل</td>

			</tr>
			@php
				$sumTotalPrice = 0;
			@endphp
			@foreach($data['invoice_items'] as $key=>$invoiceItem)
			<tr>
				<td class="text-center">{{$key+1}}</td>
				<td class="text-center">{{$invoiceItem['code']}}</td>
				<td class="text-center">{{$invoiceItem['title']}}</td>
				<td class="text-center">{{\Helpers::convertFaToEn($invoiceItem['stock'])}}</td>
				<td class="text-center">{{\Helpers::convertFaToEn($invoiceItem['units_count'])}}</td>
				<td class="text-center">{{\Helpers::convertFaToEn($invoiceItem['cartons_count'])}}</td>
				<td class="text-center">{{\Helpers::convertFaToEn($invoiceItem['unit_price'])}}</td>
				<td class="text-center">{{\Helpers::convertFaToEn($invoiceItem['tax'])}}</td>
				<td class="text-center">{{\Helpers::convertFaToEn($invoiceItem['discount'])}}</td>
				<td class="text-center">{{\Helpers::convertFaToEn($invoiceItem['item_total_price'])}}</td>
				@php 
					$sumTotalPrice = $sumTotalPrice+\Helpers::convertFaToEn($invoiceItem['item_total_price']);
				@endphp

			</tr>
			@endforeach
			
	
			
			
			<tr>
				<td colspan="9">جمع کل:</td>
				<td colspan="1" class="text-center">{{$sumTotalPrice}}</td>
			</tr>



		</tbody>
		
		
		
	</table>
	
	
	<table style="margin-top: 25px">
		<tr>
			<td>
				صادر کننده:
				{{$data['invoice_creator']}}
			</td>
		</tr>
		
		<tr>
			<td>
				ثبت کننده اطلاعات:
				{{$data['invoice_registerar']}}
			</td>
		</tr>
		
		<tr>
			<td>
				محل تحویل:
				{{$data['invoice_delivery_place_address']}}
			</td>
		</tr>
	</table>
	
	
	
	
	
	
	
	
	
	
	<table style="margin-top: 25px; margin-bottom: 20px">
		<tbody>
			<tr>
			
				<td style="padding-top: 10px" class="border-bottom">
				شرایط و نحوه فروش نقدی:
				{{$data['invoice_sell_method']}}
				</td>
			
			</tr>
			<tr>
			
				<td style="padding-top: 10px" class="border-bottom">
				مدت زمان چک:
				{{$data['invoice_cheque_time']}}
				</td>
			
			</tr>
			
			<tr>
			
				<td style="padding-top: 10px" class="border-bottom">
				توضیحات:
				{{$data['invoice_description']}}
				</td>
			
			</tr>
			
			
			
		</tbody>
	</table>
	
	
	
	<p>
		لطفا مبلغ فاکتور را به شماره حساب 0226334552001 یا شماره کارت 6037991775159803 یا شماره شبا 281700000002263345520003 بنام آقای یاشار نظری نزد بانک ملی واریز و فیش آن را برای کارشناس مربوط ارسال کنید. اجناس فوق تا زمان تسویه فاکتور و وصول چک نزد خریدار به صورت امانی و عندالمطالبه می باشد.
	</p>
	
	

    
</body>

</html>
