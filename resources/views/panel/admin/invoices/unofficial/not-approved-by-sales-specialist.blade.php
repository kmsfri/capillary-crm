@extends('panel.layout.master')
@section('additional_css')

@stop
@section('main_content')






    <div class="row mb-5">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2>ﺛﺒﺖ اﻃﻼﻋﺎﺕ پیش فاکتور غیررسمی ﺑﺮاﯼ {{$business->business_name}}</h2>

            </div>

            <div class="pull-left">

                <a class="btn btn-primary" href="{{ route('invoices.unofficial.index') }}"> ﺑﺎﺯﮔﺸﺖ</a>

            </div>

        </div>

    </div>


    @if ($errors->any())

        <div class="alert alert-danger">

            <strong>ﺧﻄﺎ!</strong> ﺩﺭ ﻣﻘﺎﺩﯾﺮ ﻭﺭﻭﺩﯼ اﯾﺮاﺩاﺗﯽ ﻭﺟﻮﺩ ﺩاﺭﺩ.<br><br>

            <ul>

                @foreach ($errors->all() as $error)

                    <li>{{ $error }}</li>

                @endforeach

            </ul>

        </div>

    @endif
    
    
    
    <div class="row mb-5">

        <div class="col-lg-12 margin-tb">

		<div class="alert alert-danger" role="alert">
			صدور فاکتور غیررسمی قابل انجام نیست. اطلاعات این کسب و کار توسط کارشناس فروش تایید نشده است.
		</div>

        </div>

    </div>
    
    
    
    <div class="row mb-5">

        <div class="col-lg-12 margin-tb">

		<div class="alert alert-light" role="alert">
			<h5>توضیحات کارشناس فروش:</h5>
			<p>{{$business->sales_specialist_approving_description}}</p>
		</div>

        </div>

    </div>


@endsection
