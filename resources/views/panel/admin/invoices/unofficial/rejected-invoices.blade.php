@extends('panel.layout.master')
@section('main_content')


    <div class="row mb-md-5">

        <div class="col-lg-12 margin-tb">

            <div class="pull-right">

                <h2>پیش فاکتورهای غیررسمی تایید نشده</h2>

            </div>

            <div class="pull-left">


            </div>

        </div>

    </div>


    @if ($message = Session::get('success'))

        <div class="alert alert-success">

            <p>{{ $message }}</p>

        </div>

    @endif


    <table class="table table-bordered">

        <tr>

            <th>ﺭﺩﯾﻒ</th>

            <th>کسب و کار</th>

            <th>ثبت کننده</th>

            <th>تاریخ ثبت</th>
            
            <th width="280px">ﻋﻤﻠﯿﺎﺕ</th>

        </tr>

        @foreach ($invoices as $invoice)

            <tr>

                <td>{{ ++$i }}</td>

                <td>{{ $invoice->business()->first()->business_name }}</td>


                <td>{{ $invoice->creator()->first()->name }}</td>
                
                <td>{{ \Morilog\Jalali\Jalalian::forge($invoice->created_at)->format('%A, %d %B %Y در ساعت H:i:s') }}</td>
                

                <td>
                    <form action="{{ route('invoices.unofficial.destroy',$invoice->id) }}" method="POST">




			<a class="btn btn-warning" href="{{ route('invoices.unofficial.get-pdf',['invoice_id'=>$invoice->id]) }}">دریافت</a>


                        @can('invoice-edit')

                            <a class="btn btn-primary" href="{{ route('invoices.unofficial.edit',$invoice->id) }}">ﻭﯾﺮاﯾﺶ</a>

                        @endcan


                        @csrf

                        @method('DELETE')

                        @can('invoice-delete')

                            <button type="submit" class="btn btn-danger">ﺣﺬﻑ</button>

                        @endcan

                    </form>
                </td>

            </tr>

        @endforeach

    </table>


    {!! $invoices->links() !!}



@endsection
