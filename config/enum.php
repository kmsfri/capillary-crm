<?php


return [
	'customer_degree' => [
		'degree1' => 'درجه ۱ بنکداران اصلی',
		'degree2' => 'درجه ۲ بنکداران خرد',
		'degree3' => 'درجه ۳ خرده فروشی',
		'degree4' => 'درجه ۴ شرکتها و مصرف کننده ها',
	],
	
	'customer_legal_type' => [
		'real' => 'حقیقی',
		'legal' => 'حقوقی'
	],
	
	'customer_recruitment_type' => [
		'old_user' => 'مشتری قدیمی',
		'new_user' => 'مشتری جدید',
		'social_user' => 'مشتری اینستاگرام',
		'internet_user' => 'مشتری اینترنتی',
		'phone_user' => 'تماس ورودی به شرکت',
	],
	
	'sell_method' => [
		'CASH' => 'نقدی',
		'CHEQUE' => 'چک',
		'CREDIT' => 'اعتباری'
	]
];
