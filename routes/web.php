<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::group(['middleware'=>['auth']],function() {
    Route::get('/', function () {
        return view('panel.pages.dashboard');
    })->name('showDashboard');
});
*/

Route::group(['middleware'=>['auth','init_shared_data']],function() {
	Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('showDashboard');
});

Auth::routes(['register' => false]);

Route::group(['middleware'=>['auth','init_shared_data']],function() {
	Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
});


Route::get('products/update-all', [\App\Http\Controllers\Admin\ProductController::class, 'updateAllProducts'])->name('products.updateAllProducts');

Route::get('customers/update-all', [\App\Http\Controllers\Admin\CustomerController::class, 'updateAllCustomers'])->name('customers.updateAllCustomers');


Route::group(['middleware' => ['auth', 'init_shared_data']], function() {
	
	
	Route::get('users/toggle-walking-status', [\App\Http\Controllers\Admin\UserController::class, 'toggleWalkingStatus'])->name('users.toggleWalkingStatus');
	
	Route::get('users/{user_id}/geo-update', [\App\Http\Controllers\Admin\UserController::class, 'updateGeoPath'])->name('users.updateGeoPath');
	Route::get('users/{user_id}/walked-paths', [\App\Http\Controllers\Admin\UserController::class, 'showUserWalkedPaths'])->name('users.showUserWalkedPaths');
	
	Route::get('users/{path_id}/show-walked-path', [\App\Http\Controllers\Admin\UserController::class, 'showWalkedPath'])->name('users.showWalkedPath');
	
	
	Route::get('clues/report', [\App\Http\Controllers\Admin\ClueController::class, 'showCluesReport'])->name('clues.showCluesReport');
	
	Route::post('clues/merge', [\App\Http\Controllers\Admin\ClueController::class, 'mergeToAnotherClue'])->name('clues.merge');
	
	Route::post('clues/merge/confirm', [\App\Http\Controllers\Admin\ClueController::class, 'mergeConfirmToAnotherClue'])->name('clues.merge.confirm');
	
	Route::get('clues/{clue_id}/cartable', [\App\Http\Controllers\Admin\ClueController::class, 'showCartable'])->name('clues.showCartable');
	Route::post('clues/add-new-note', [\App\Http\Controllers\Admin\ClueController::class, 'addNewNote'])->name('clues.addNewNote');
	Route::post('clues/add-new-report-process', [\App\Http\Controllers\Admin\ClueController::class, 'addNewReportProcess'])->name('clues.addNewReportProcess');
	Route::post('clues/update-clue-customer-status', [\App\Http\Controllers\Admin\ClueController::class, 'updateClueCustomerStatus'])->name('clues.updateClueCustomerStatus');
	Route::post('clues/update-clue-business-status', [\App\Http\Controllers\Admin\ClueController::class, 'updateClueBusinessStatus'])->name('clues.updateClueBusinessStatus');
	Route::post('clues/update-clue-pre-invoice-status', [\App\Http\Controllers\Admin\ClueController::class, 'updateCluePreInvoiceStatus'])->name('clues.updateCluePreInvoiceStatus');
	Route::post('clues/update-clue-invoice-status', [\App\Http\Controllers\Admin\ClueController::class, 'updateClueInvoiceStatus'])->name('clues.updateClueInvoiceStatus');
	
	
	Route::post('clues/update-clue-description', [\App\Http\Controllers\Admin\ClueController::class, 'updateClueDescription'])->name('clues.updateClueDescription');
	
	
	Route::post('clues/update-clue-products', [\App\Http\Controllers\Admin\ClueController::class, 'updateClueProducts'])->name('clues.updateClueProducts');
	
	Route::post('clues/update-clue-marketing-list', [\App\Http\Controllers\Admin\ClueController::class, 'updateClueMarketingList'])->name('clues.updateClueMarketingList');
	
	
	
	
	Route::resource('clues', \App\Http\Controllers\Admin\ClueController::class);
	
	
	Route::get('competitors/report', [\App\Http\Controllers\Admin\CompetitorController::class, 'showCompetitorsReport'])->name('competitors.showCompetitorsReport');
	
	Route::resource('competitors', \App\Http\Controllers\Admin\CompetitorController::class);
	
	 
	

    Route::resource('roles', \App\Http\Controllers\Admin\RoleController::class);
    
    Route::get('users/cities/edit/{user_id}', [\App\Http\Controllers\Admin\UserController::class, 'editUserCities'])->name('users.edit-user-cities');
    Route::get('users/cities/add', [\App\Http\Controllers\Admin\UserController::class, 'addCityToUser'])->name('users.add-city-to-user');
    Route::delete('users/cities/remove', [\App\Http\Controllers\Admin\UserController::class, 'removeUserCity'])->name('users.remove-user-city');
    Route::delete('users/cities/remove-mass', [\App\Http\Controllers\Admin\UserController::class, 'removeUserCityMass'])->name('users.remove-user-city-mass');

    Route::resource('users', \App\Http\Controllers\Admin\UserController::class);
    
    
	Route::get('campaigns/{campaign_id}/report', [\App\Http\Controllers\Admin\CampaignController::class, 'showCampaignReport'])->name('campaigns.showCampaignsReport');
	Route::resource('campaigns', \App\Http\Controllers\Admin\CampaignController::class);
	
	
    Route::resource('products', \App\Http\Controllers\Admin\ProductController::class);

	Route::get('business/{business_id}/cartable', [\App\Http\Controllers\Admin\BusinessController::class, 'showCartable'])->name('businesses.showCartable');
	
	
	Route::get('business/brands/edit/{business_id}', [\App\Http\Controllers\Admin\BrandController::class, 'editBusinessBrands'])->name('brands.editBusinessBrands');
	Route::patch('business/brands/update', [\App\Http\Controllers\Admin\BrandController::class, 'updateBusinessBrands'])->name('brands.updateBusinessBrands');
	
	
	//Route::get('business/brands/show/{business_id}', [\App\Http\Controllers\Admin\BrandController::class, 'showBusinessBrands'])->name('brands.showBusinessBrands');
	
	
	Route::resource('brands', \App\Http\Controllers\Admin\BrandController::class);
	
	Route::resource('proceedings', \App\Http\Controllers\Admin\ProceedingController::class);
	
	Route::resource('marketing', \App\Http\Controllers\Admin\MarketingController::class);
    
	Route::get('configs/edit', [\App\Http\Controllers\Admin\ConfigController::class, 'edit'])->name('configs.edit');
    Route::post('configs/update', [\App\Http\Controllers\Admin\ConfigController::class, 'update'])->name('configs.update');
    
	Route::get('teams/members/{team_id}', [\App\Http\Controllers\Admin\TeamController::class, 'createTeamMembers'])->name('teams.show-createTeamMembers-form');
	Route::post('teams/members/store-team-members', [\App\Http\Controllers\Admin\TeamController::class, 'storeTeamMembers'])->name('teams.store-team-members');
    
	Route::get('teams/report/{team_id}', [\App\Http\Controllers\Admin\TeamController::class, 'showTeamReport'])->name('teams.show-team-report'); 
	
    Route::resource('teams', \App\Http\Controllers\Admin\TeamController::class);
	
	Route::get('businesses/report', [\App\Http\Controllers\Admin\BusinessController::class, 'showBusinessesReport'])->name('businesses.showBusinessesReport');

    Route::resource('businesses', \App\Http\Controllers\Admin\BusinessController::class);
    
    Route::post('businesses/add-new-note', [\App\Http\Controllers\Admin\BusinessController::class, 'addNewNote'])->name('businesses.addNewNote');
    
    Route::get('accountant/business/approve/{business_id}', [\App\Http\Controllers\Admin\BusinessController::class, 'showAccountantApprovingForm'])->name('business.show-accounting-approving-form');
    
    Route::post('accountant/business/approve/{business_id}', [\App\Http\Controllers\Admin\BusinessController::class, 'accountantApprovingBusiness'])->name('business.accounting-approving-business');
    
    Route::get('sales-specialist/business/approve/{business_id}', [\App\Http\Controllers\Admin\BusinessController::class, 'showSalesSpecialistApprovingForm'])->name('business.show-sales-specialist-approving-form');
    
    Route::post('sales-specialist/business/approve/{business_id}', [\App\Http\Controllers\Admin\BusinessController::class, 'salesSpecialistApprovingBusiness'])->name('business.sales-specialist-approving-business');

    Route::resource('cities', \App\Http\Controllers\Admin\CityController::class);


    Route::get('comments/report', [\App\Http\Controllers\Admin\CommentController::class, 'showCommentsReport'])->name('comments.showCommentsReport');

    Route::resource('comments', \App\Http\Controllers\Admin\CommentController::class);
    
    
    Route::get('customers/{customer_id}/cartable', [\App\Http\Controllers\Admin\CustomerController::class, 'showCartable'])->name('customers.showCartable');
   
	Route::get('customers/report', [\App\Http\Controllers\Admin\CustomerController::class, 'showCustomersReport'])->name('customers.showCustomersReport');

    Route::resource('customers', \App\Http\Controllers\Admin\CustomerController::class);
    
    

    Route::get('sms/history', [\App\Http\Controllers\Admin\SmsController::class, 'history'])->name('sms.history');
    Route::get('sms/send/send-specialist-info', [\App\Http\Controllers\Admin\SmsController::class, 'sendSpecialistInfo'])->name('sms.sendSpecialistInfo');
    Route::get('sms/send', [\App\Http\Controllers\Admin\SmsController::class, 'showSendForm'])->name('sms.showSendForm');
    Route::post('sms/send', [\App\Http\Controllers\Admin\SmsController::class, 'sendSMS'])->name('sms.sendSMS');
    
    //Route::resource('sms', \App\Http\Controllers\Admin\SmsController::class);
    
    Route::post('customers/upload-excel-file', [\App\Http\Controllers\Admin\CustomerController::class, 'uploadExcelFile'])->name('customers.uploadExcelFile');
    
    Route::post('customers/update-customer-description', [\App\Http\Controllers\Admin\CustomerController::class, 'updateCustomerDescription'])->name('customers.updateCustomerDescription');
    Route::post('customers/add-new-note', [\App\Http\Controllers\Admin\CustomerController::class, 'addNewNote'])->name('customers.addNewNote');
	
	


    
    
    
    
    //unofficial
    Route::get('invoices/unofficial/get-pdf', [\App\Http\Controllers\Admin\UnofficialInvoiceController::class, 'getPdf'])->name('invoices.unofficial.get-pdf');

    Route::resource('invoices/unofficial', \App\Http\Controllers\Admin\UnofficialInvoiceController::class, ['as' => 'invoices']);
    
    Route::get('accountant/invoices/unofficial/primary/index', [\App\Http\Controllers\Admin\UnofficialInvoiceController::class, 'primaryInvoices'])->name('invoices.unofficial.show-primary-invoices');
    
    Route::get('accountant/invoices/unofficial/rejected/index', [\App\Http\Controllers\Admin\UnofficialInvoiceController::class, 'rejectedInvoices'])->name('invoices.unofficial.show-rejected-invoices');
    
    Route::get('accountant/invoices/unofficial/approve/{invoice_id}', [\App\Http\Controllers\Admin\UnofficialInvoiceController::class, 'showAccountantApprovingForm'])->name('invoices.unofficial.show-accounting-approving-form');
    
    Route::post('accountant/invoices/unofficial/approve/{invoice_id}', [\App\Http\Controllers\Admin\UnofficialInvoiceController::class, 'accountantApprovingInvoice'])->name('invoices.unofficial.accounting-approving-invoice');
    
    Route::get('sales-specialist/invoices/unofficial/approve/{invoice_id}', [\App\Http\Controllers\Admin\UnofficialInvoiceController::class, 'showSalesSpecialistApprovingForm'])->name('invoices.unofficial.show-sales-specialist-approving-form');
    
    Route::post('sales-specialist/invoices/unofficial/approve/{invoice_id}', [\App\Http\Controllers\Admin\UnofficialInvoiceController::class, 'salesSpecialistApprovingInvoice'])->name('invoices.unofficial.sales-specialist-approving-invoice');
    
 
    //returned invoices
    Route::get('invoices/returned/get-pdf', [\App\Http\Controllers\Admin\ReturnedInvoiceController::class, 'getPdf'])->name('invoices.returned.get-pdf');

    Route::resource('invoices/returned', \App\Http\Controllers\Admin\ReturnedInvoiceController::class, ['as' => 'invoices']);
    
    Route::get('accountant/invoices/returned/primary/index', [\App\Http\Controllers\Admin\ReturnedInvoiceController::class, 'primaryInvoices'])->name('invoices.returned.show-primary-invoices');
    
    Route::get('accountant/invoices/returned/rejected/index', [\App\Http\Controllers\Admin\ReturnedInvoiceController::class, 'rejectedInvoices'])->name('invoices.returned.show-rejected-invoices');
    
    Route::get('accountant/invoices/returned/approve/{invoice_id}', [\App\Http\Controllers\Admin\ReturnedInvoiceController::class, 'showAccountantApprovingForm'])->name('invoices.returned.show-accounting-approving-form');
    
    Route::post('accountant/invoices/returned/approve/{invoice_id}', [\App\Http\Controllers\Admin\ReturnedInvoiceController::class, 'accountantApprovingInvoice'])->name('invoices.returned.accounting-approving-invoice');
    
    Route::get('sales-specialist/invoices/returned/approve/{invoice_id}', [\App\Http\Controllers\Admin\ReturnedInvoiceController::class, 'showSalesSpecialistApprovingForm'])->name('invoices.returned.show-sales-specialist-approving-form');
    
    Route::post('sales-specialist/invoices/returned/approve/{invoice_id}', [\App\Http\Controllers\Admin\ReturnedInvoiceController::class, 'salesSpecialistApprovingInvoice'])->name('invoices.returned.sales-specialist-approving-invoice');
    
    
    //officials
    Route::get('invoices/get-pdf', [\App\Http\Controllers\Admin\InvoiceController::class, 'getPdf'])->name('invoices.get-pdf');

    Route::resource('invoices', \App\Http\Controllers\Admin\InvoiceController::class);
	
	Route::get('instances/get-pdf', [\App\Http\Controllers\Admin\InstanceController::class, 'getPdf'])->name('instances.get-pdf');
	
	Route::resource('instances', \App\Http\Controllers\Admin\InstanceController::class);
    
    Route::get('accountant/invoices/primary/index', [\App\Http\Controllers\Admin\InvoiceController::class, 'primaryInvoices'])->name('invoices.show-primary-invoices');
    
    Route::get('accountant/invoices/bijak/{bijak_id}/show', [\App\Http\Controllers\Admin\InvoiceController::class, 'showBijak'])->name('invoices.show-bijak');
    
    Route::get('accountant/invoices/bijak/show-create-form/{invoice_id}', [\App\Http\Controllers\Admin\InvoiceController::class, 'showCreateBijakform'])->name('invoices.show-create-bijak-form');
    
    Route::post('accountant/invoices/bijak/upload', [\App\Http\Controllers\Admin\InvoiceController::class, 'uploadBijak'])->name('invoices.upload-bijak');
    
    Route::get('accountant/invoices/rejected/index', [\App\Http\Controllers\Admin\InvoiceController::class, 'rejectedInvoices'])->name('invoices.show-rejected-invoices');
    
    Route::get('accountant/invoices/approve/{invoice_id}', [\App\Http\Controllers\Admin\InvoiceController::class, 'showAccountantApprovingForm'])->name('invoices.show-accounting-approving-form');
    
    Route::post('accountant/invoices/approve/{invoice_id}', [\App\Http\Controllers\Admin\InvoiceController::class, 'accountantApprovingInvoice'])->name('invoices.accounting-approving-invoice');
    
    Route::get('sales-specialist/invoices/approve/{invoice_id}', [\App\Http\Controllers\Admin\InvoiceController::class, 'showSalesSpecialistApprovingForm'])->name('invoices.show-sales-specialist-approving-form');
    
    Route::post('sales-specialist/invoices/approve/{invoice_id}', [\App\Http\Controllers\Admin\InvoiceController::class, 'salesSpecialistApprovingInvoice'])->name('invoices.sales-specialist-approving-invoice');
    
   
    
	Route::get('commission/{commission_id}/edit', [\App\Http\Controllers\Admin\CommissionController::class, 'editCommission'])->name('commission.edit-commission');
	
	Route::post('commission/monthly', [\App\Http\Controllers\Admin\CommissionController::class, 'storeMonthlyCommission'])->name('commission.store-monthly-commission');
    
    Route::get('commission/months/{user_id}/list', [\App\Http\Controllers\Admin\CommissionController::class, 'userCommissionsMonthsList'])->name('commission.show-users-commission-months-list');
    Route::get('commission/monthly/{commission_month_id}/list', [\App\Http\Controllers\Admin\CommissionController::class, 'userMonthCommissionsList'])->name('commission.show-users-month-commission-list');
	
    Route::get('commission/monthly/{user_id}', [\App\Http\Controllers\Admin\CommissionController::class, 'setMonthlyCommission'])->name('commission.set-monthly-commision');
	
    Route::delete('commission/{commission_id}/destroy', [\App\Http\Controllers\Admin\CommissionController::class, 'destroy'])->name('commission.destroy');
	
    
	
	
    
    
    Route::post('visit/daily-flow/update-flow-operator', [\App\Http\Controllers\Admin\VisitController::class, 'updateFlowOperator'])->name('visit.update-daily-flow-operator');
    
    Route::get('visit/daily-flow/{user_id}/list', [\App\Http\Controllers\Admin\VisitController::class, 'userDailyFlowList'])->name('visit.show-users-daily-flow-list');
	
	Route::get('visit/daily-flow/{user_id}/report', [\App\Http\Controllers\Admin\VisitController::class, 'showUserDailyFlowReport'])->name('visit.show-users-daily-flow-report'); 
    
    Route::get('visit/daily-flow/{user_id}', [\App\Http\Controllers\Admin\VisitController::class, 'setDailyFlow'])->name('visit.set-daily-flow');
    
    
    
    
    //BEGIN DRIVERS
    
    Route::get('visit/driver/daily-flow/{user_id}/list', [\App\Http\Controllers\Admin\VisitController::class, 'showDriverUserDailyFlowList'])->name('visit.show-driver-users-daily-flow-list');
    
    Route::get('visit/drivers', [\App\Http\Controllers\Admin\VisitController::class, 'showDrivers'])->name('visit.show-drivers');
    
    Route::get('visit/driver/daily-flow/{user_id}', [\App\Http\Controllers\Admin\VisitController::class, 'setDriverDailyFlow'])->name('visit.set-driver-daily-flow');
    
    Route::post('visit/drivers/daily-flow', [\App\Http\Controllers\Admin\VisitController::class, 'storeDriverDailyFlow'])->name('visit.store-driver-daily-flow');
    
    Route::get('visit/drivers/daily-flow/{user_id}/report', [\App\Http\Controllers\Admin\VisitController::class, 'showDriverUserDailyFlowReport'])->name('visit.show-driver-users-daily-flow-report'); 
    
    Route::get('visit/drivers/vistitor-daily-flow', [\App\Http\Controllers\Admin\VisitController::class, 'showDriverVisitorDailyFlow'])->name('visit.driver-visitor.show-daily-flow');
    
    Route::post('visit/drivers/daily-flow/update-flow-operator', [\App\Http\Controllers\Admin\VisitController::class, 'updateDriverFlowOperator'])->name('visit.update-driver-daily-flow-operator');
    //END DRIVERS
    
    
    
    
    
    
    Route::get('visit/visitors', [\App\Http\Controllers\Admin\VisitController::class, 'showVisitors'])->name('visit.show-visitors');
	
	Route::get('visit/visitors/{visitor_user_id}/report', [\App\Http\Controllers\Admin\VisitController::class, 'showVisitorReport'])->name('visit.show-visitor-report');
	
	Route::post('visit/visitors/{visitor_user_id}/add-new-note', [\App\Http\Controllers\Admin\VisitController::class, 'addNewNoteToVisitor'])->name('visit.addNewNote');
    
    Route::get('visit/visitors/set-specialist/{user_id}', [\App\Http\Controllers\Admin\VisitController::class, 'showSetSpecialistForm'])->name('visit.visitors.set-specialist');
    
    Route::post('visit/visitors/update-specialist-user', [\App\Http\Controllers\Admin\VisitController::class, 'updateSpecialistUser'])->name('visit.visitors.update-specialist-user');
    
    
    Route::post('visit/daily-flow', [\App\Http\Controllers\Admin\VisitController::class, 'storeDailyFlow'])->name('visit.store-daily-flow');
    
    Route::get('visit/vistitor-daily-flow', [\App\Http\Controllers\Admin\VisitController::class, 'showVisitorDailyFlow'])->name('visit.visitor.show-daily-flow');
    
    
    Route::get('specialists/specialist-daily-call-targets', [\App\Http\Controllers\Admin\SpecialistController::class, 'showSpecialistDailyCallTargetsList'])->name('specialists.show-specialist-daily-call-targets-list');
    
    Route::get('specialists/specialist-daily-call-targets/set-alarm', [\App\Http\Controllers\Admin\SpecialistController::class, 'showSetAlarmFormForCustomerCallTarget'])->name('specialists.set-alarm-to-specialist-daily-call-target');
	
	Route::get('specialists/alarms/seen/{alarm_id}', [\App\Http\Controllers\Admin\SpecialistController::class, 'seenAlarm'])->name('specialists.seenAlarm');
	
	Route::get('specialists/alarms/seen_in_notifications', [\App\Http\Controllers\Admin\SpecialistController::class, 'seenAlarmInNotifications'])->name('specialists.seenAlarmInNotifications');
	
	Route::get('specialists/alarms/index', [\App\Http\Controllers\Admin\SpecialistController::class, 'showAlarmsList'])->name('specialists.showAlarmsList');
    
    
    Route::post('specialists/specialist-daily-call-targets/store-alarm', [\App\Http\Controllers\Admin\SpecialistController::class, 'storeAlarmForCustomerCallTarget'])->name('specialists.store-alarm-to-specialist-daily-call-target');
   

 Route::get('specialists/voip/report', [\App\Http\Controllers\Admin\SpecialistController::class, 'showVoipCallsReport'])->name('specialists.show-voip-calls-report');

 
	Route::get('specialists/{specialist_user_id}/report', [\App\Http\Controllers\Admin\SpecialistController::class, 'showSpecialistReport'])->name('specialists.show-specialist-report');
	
	Route::post('specialists/{specialist_user_id}/add-new-note', [\App\Http\Controllers\Admin\SpecialistController::class, 'addNewNoteToSpecialist'])->name('specialists.addNewNote');
	
	Route::post('specialists/{specialist_user_id}/update-note', [\App\Http\Controllers\Admin\SpecialistController::class, 'updateNote'])->name('specialists.updateNote');
	
	Route::post('clues/{clue_id}/update-clue-process-report', [\App\Http\Controllers\Admin\ClueController::class, 'updateClueProcessReport'])->name('clues.updateClueProcessReport');
	
	
    Route::resource('specialists', \App\Http\Controllers\Admin\SpecialistController::class);
    
    
    Route::get('specialists/call-targets/list/{user_id}', [\App\Http\Controllers\Admin\SpecialistController::class, 'showCallTargetsList'])->name('specialists.show-call-targets-list');
    
    Route::get('specialists/call-targets/create/{user_id}', [\App\Http\Controllers\Admin\SpecialistController::class, 'createCallTarget'])->name('specialists.create-call-target');
    
    Route::post('specialists/call-targets/store', [\App\Http\Controllers\Admin\SpecialistController::class, 'storeCallTarget'])->name('specialists.store-call-target');
    
    Route::get('specialists/call-targets/details/{call_target_id}', [\App\Http\Controllers\Admin\SpecialistController::class, 'showCallTargetDetails'])->name('specialists.show-call-target-details');
    
   
    
    //Route::get('specialists/call-targets/edit/{user_id}', [\App\Http\Controllers\Admin\SpecialistController::class, 'editCallTargets'])->name('specialists.edit-call-targets');
    
    
    //Route::get('specialists/call-targets/update', [\App\Http\Controllers\Admin\SpecialistController::class, 'updateCallTargets'])->name('specialists.update-call-targets');
    
    

});


Route::get('/ajax/get_cities', [\App\Http\Controllers\Admin\CityController::class,'getCities'])->name('getCities');
